﻿import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';

export interface ConfirmModel {
    title: string;
    message: string;
    includeCancel: boolean;
}

@Component({
    selector: 'app-confirm',
    template: `<div class="modal-dialog">
                <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" (click)="close()" >&times;</button>
                     <h4 class="modal-title">{{title || 'Confirm'}}</h4>
                   </div>
                   <div class="modal-body">
                     <p>{{message || 'Are you sure?'}}</p>
                   </div>
                   <div class="modal-footer">
                     <button id="confirm-button" type="button" class="btn btn-primary" (click)="confirm()"><span>OK</span></button>
                     <span id="cancel" *ngIf="includeCancel === undefined
                     || includeCancel === true" type="button" class="accent-paragraph pointerCursor"
                     (click)="cancel()"><span>Cancel</span></span>
                   </div>
                 </div>
                </div>`,
    styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
    title: string;
    message: string;
    includeCancel: boolean;
    constructor(dialogService: DialogService) {
        super(dialogService);
    }
    confirm() {
        // on click on confirm button we set dialog result as true,
        // ten we can get dialog result from caller code
        this.result = true;
        this.close();
    }
    cancel() {
        this.result = false;
        this.close();
    }
}
