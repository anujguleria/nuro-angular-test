import { Component, OnInit, Input } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { TaskService } from '../shared/task.service';
import { DataService } from '../../Services/data.service';
import { PipelineService } from '../../Services/pipeline.service';
import * as _ from 'underscore';
import { Observable } from 'rxjs/Rx';
export interface TaskModel {
    title: string;
    message: string;
    pipelineID: number;
    sequence: number;
    pipelineTaskID: number;
    projectID: number;
}

@Component({
    selector: 'app-new-task',
    templateUrl: './new-task.component.html',
    styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent extends DialogComponent<TaskModel, boolean> implements TaskModel, OnInit {
    title: string;
    message: string;
    validationClass: string;
    validationText: string;
    failedValidation = false;
    moduleGroups: {};
    previousTaskParameters: {} = {};
    modules: any[];
    groupKeys: any[];
    selectedModuleID: number;
    selectedModule: any;
    parameterValues: {} = {};
    parameterTaskHooks: {} = {};
    pipelineID: number;
    sequence = 0;
    pipelineTaskID = 0;
    projectID = 0;
    currentDBTask: any;
    nextSequence = 0;

    validationError: string;
    errorEnabled: boolean;
    isErorrOpened: boolean;

    modelContentHeight: string;

    taskname: string;
    constructor(dialogService: DialogService,
        private taskService: TaskService,
        private dataService: DataService,
        private pipelineService: PipelineService
    ) {
        super(dialogService);
        this.taskname = undefined;
    }

    ngOnInit() {
        let screenHeight: number;
        if (typeof (window.innerWidth) === 'number') {
            screenHeight = window.innerHeight;
        } else {
            if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                screenHeight = document.documentElement.clientHeight;
            } else {
                if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                    screenHeight = document.body.clientHeight;
                }
            }
        }
        if (this.sequence === 0) {
            this.sequence = 1;
        }
        this.modelContentHeight = screenHeight * .55 + 'px';
        let taskPromise;
        if (this.pipelineTaskID) {
            taskPromise = this.taskService.getTask(this.pipelineTaskID);
        }
        const pipelineTasksPromise = this.pipelineService.getPipelineTasks(this.pipelineID);
        this.taskService.getModules().subscribe(res => {
            this.modules = res;
            this.moduleGroups = _.groupBy(res, function (module) { return module['ModuleGroup']['Name']; });
            this.groupKeys = _.keys(this.moduleGroups);
            if (taskPromise) {
                taskPromise.subscribe(task => {
                    this.selectedModuleID = task.ModuleID;
                    this.selectedModule = this.modules.find(m => m.ModuleID === task.ModuleID); // this.selectedModuleID);
                    this.taskname = task.Name;
                    for (const userInputValue of task.UserInputValues) {
                        this.parameterValues[userInputValue.ParameterID] = userInputValue.InputValue;
                    }
                    this.currentDBTask = task;
                    this.pipelineTaskSubscribe(pipelineTasksPromise, task);
                });
            } else {
                this.pipelineTaskSubscribe(pipelineTasksPromise);
            }
        }, error => { });
    }
    pipelineTaskSubscribe(pipelineTasksPromise, task?) {
        pipelineTasksPromise.subscribe(res => {
            this.previousTaskParameters = {};
            for (const innerTask of res) {
                if ((innerTask.SequencePosition + 1) > this.nextSequence && task) {
                    this.nextSequence = task.SequencePosition + 1;
                }

                if ((task && task.SequencePosition > innerTask.SequencePosition) ||
                    innerTask.SequencePosition < this.sequence || this.sequence === undefined) {

                    this.taskService.getModule(innerTask.ModuleID).subscribe(res2 => {
                        const module = res2;
                        for (const parameter of module.Parameters) {
                            if (parameter.Direction.toUpperCase() !== 'IN') {
                                if (!this.previousTaskParameters[parameter.ParameterType.ParameterTypeID]) {
                                    this.previousTaskParameters[parameter.ParameterType.ParameterTypeID] = [];
                                }
                                const taskName = innerTask.Name.replace('System', 'Pipeline');
                                const option = {
                                    name: taskName + ' - ' + parameter.Name,
                                    pipelineTaskID: innerTask.PipelineTaskID,
                                    parameterID: parameter.ParameterID,
                                    id: innerTask.PipelineTaskID + '~' + parameter.ParameterID,
                                    pipelineID: innerTask.PipeLineID
                                };
                                if (task) {
                                    for (const taskInputValue of task.PipelineTaskInputValues) {
                                        if (taskInputValue.PriorPipelineTaskID === innerTask.PipelineTaskID
                                            && taskInputValue.PriorPipeLineTaskParameterID === parameter.ParameterID) {
                                            this.parameterTaskHooks[taskInputValue.ParameterID] = option;
                                        }
                                    }
                                }
                                this.previousTaskParameters[parameter.ParameterType.ParameterTypeID].push(option);
                            }
                        }
                    });
                }
            }
        });
    }
    onClick(moduleID: number): void {
        this.parameterValues = {};
        this.parameterTaskHooks = {};
        this.selectedModule = this.modules.find(m => m.ModuleID === moduleID);
        this.resetErrorMessage();
    }

    confirm() {
        // we set dialog result as true on click on confirm button,
        // then we can get dialog result from caller code

        // this.taskname;

        if (this.taskname === undefined || this.taskname.trim() === '') {
            this.failValidation('Please enter a Task Name.');

        } else {
            const regexp = new RegExp(this.dataService.num_letter_spaces_underscore_rejexExp);
            const test: boolean = regexp.test(this.taskname);
            if (!test) {
                this.failValidation('Task Name only supports numbers, letters, spaces and underscores.');
                return;
            }
            const context = {
                ProjectID: this.projectID,
                PipelineID: this.pipelineID

            }
            if (this.sequence === undefined) {
                this.sequence = this.nextSequence;
            }
            if (this.pipelineTaskID === undefined && this.sequence === 0) {
                this.sequence = 1;
            }
            const task: any = {
                Name: this.taskname,
                PipelineID: this.pipelineID,
                SequencePosition: this.sequence,
                UserInputValues: [],
                ModuleID: this.selectedModuleID,
                PipelineTaskInputValues: []
            };
            if (!task.ModuleID) {
                this.failValidation('Please select a module.');
                return;
            }
            for (const parameterID of _.keys(this.parameterValues)) {
                task.UserInputValues.push({ ParameterID: parameterID, InputValue: this.parameterValues[parameterID] });
            }
            for (const hook of _.keys(this.parameterTaskHooks)) {
                if (this.parameterTaskHooks[hook].name !== 'None') {
                    task.PipelineTaskInputValues.push({
                        ParameterID: hook, PriorPipelineTaskID: this.parameterTaskHooks[hook].id.split('~')[0],
                        PriorPipeLineTaskParameterID: this.parameterTaskHooks[hook].id.split('~')[1],
                        PriorPipelineID: this.parameterTaskHooks[hook].pipelineID
                    });
                }
            }
            let tastPresentPromis: Observable<any>;
            tastPresentPromis = this.taskService.getTaskByName(this.taskname, context);
            if (this.pipelineTaskID) {
                task.PipelineTaskID = this.pipelineTaskID;
                this.taskService.updateTask(task).subscribe(res => {
                    this.taskSaveSuccess(task, res);
                }, error => {
                    this.failValidation('Task save failed.');
                });
            } else {
                tastPresentPromis.subscribe(res => {
                    this.failValidation('Task Name in use, please enter another name.');
                    return;
                }, error => {
                    if (error.status === 404) {
                        this.taskService.createTask(task).subscribe(res => {
                            this.taskSaveSuccess(task, res);
                        }, error2 => {
                            this.failValidation('Task save failed.');
                        });
                    }
                });
            }
        }
    }
    taskSaveSuccess(task, res) {
        if (!this.pipelineTaskID) {
            task.PipelineTaskID = parseInt(res._body, undefined);
        }
        this.result = task;
        this.close();
    }
    failValidation(Message: string) {
        this.validationText = Message;
        this.errorEnabled = true;
    }
    collapsAll(elements, parameter) {
        elements.forEach(element => {
            if (element.isExpanded && element.isExpanded === true) {
                if (element.ParameterID !== parameter.ParameterID) {
                    element.isExpanded = false;
                }
            }
        });
        parameter.isExpanded = !parameter.isExpanded;
    }

    resetErrorMessage() {
        this.validationText = undefined;
        this.errorEnabled = false;
        this.isErorrOpened = false;
    }
    setNoneValue(id) {
        this.parameterTaskHooks[id] = { 'name': 'None' };
    }
}



