﻿import { Injectable, OnInit, EventEmitter } from '@angular/core';
import { ApiService } from '../../Services/api.service';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class TaskService implements OnInit {
    url = environment.apiUrl + 'api/PipelineTasks/';
    moduleUrl = environment.apiUrl + 'api/Modules/';
    newTaskCreated = new EventEmitter<string>();
    ngOnInit() {

    }

    constructor(private apiService: ApiService) { }
    getTasks(): Observable<any> {
        return this.apiService.get(this.url);
    }
    getTask(id): Observable<any> {
        return this.apiService.get(this.url + id);
    }
    createTask(data): Observable<any> {
        return this.apiService.post(this.url, data);
    }
    updateTask(data): Observable<any> {
        return this.apiService.put(this.url + data.PipelineTaskID, data);
    }
    deleteTask(data): Observable<any> {
        return this.apiService.delete(this.url + data.ID);
    }
    getModules(): Observable<any> {
        return this.apiService.get(this.moduleUrl);
    }
    getModule(id): Observable<any> {
        return this.apiService.get(this.moduleUrl + id);
    }
    getTaskByName(name, context): Observable<any> {
        return this.apiService.post(this.url + 'ByName/' + name, context);
    }
    updateModuleParameters(data, id): Observable<any> {
        return this.apiService.put(this.moduleUrl + id + '/Parameters', data);
    }
    getModuleParameters(id) {
        return this.apiService.get(this.moduleUrl + id + '/Parameters');
    }
}
