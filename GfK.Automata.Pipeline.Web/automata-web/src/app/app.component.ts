﻿import { Component, Inject } from '@angular/core';
import { DataService } from './Services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent {
  title = 'app';
  show = false;
  clientHeight: number;

  constructor(private dataservice: DataService) {
    this.dataservice.showHideMask.subscribe((value: boolean) => {
      this.show = value;
    });
    this.clientHeight = window.innerHeight;
  }
}
