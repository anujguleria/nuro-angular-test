﻿import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderComponent } from '../header/header.component';

import { UserLoginService } from '../Services/user-login.service';
import { DataService } from '../Services/data.service';

@Component({
    selector: 'app-login-box',
    templateUrl: './login-box.component.html',
    styleUrls: ['./login-box.component.css'],
    providers: [UserLoginService]
})
export class LoginBoxComponent implements OnInit {
    creds: any;
    username: any;
    password: any;
    showpassword = false;
    typeforPassword = 'password';
    placeholderText = 'Username';
    UsernamevalidationClass = '';
    PasswordvalidationClass = '';
    showjspassword = '';
    rememberMe = false;
    usernameMessage = '';
    passwordMessage = '';
    Usernamevalidationfalse = false;
    Passwordvalidationfalse = false;
    showUsernameMessage = false;
    showPasswordMessage = false;
    clientHeight = 0;
    constructor(private UserService: UserLoginService, private router: Router) { }

    ngOnInit() {
        if (localStorage.getItem('creds') === undefined || localStorage.getItem('creds') === null) {
            this.rememberMe = false;
        } else {
            this.rememberMe = true;
            const credData = localStorage.getItem('creds').split(';');

            this.username = atob(credData[0]);
            this.password = atob(credData[1]);
        }
        this.clientHeight = window.innerHeight - 107;
    }

    login() {
        if (typeof this.username !== 'undefined' && this.username && typeof this.password !== 'undefined' && this.password) {
            this.UserService.login(this.username, this.password).subscribe((response) => {
                if (this.rememberMe) {
                    localStorage.setItem('creds', btoa(this.username) + ';' + btoa(this.password));
                } else {
                    localStorage.removeItem('creds');
                }
                localStorage.setItem('username', this.username);
                localStorage.setItem('authToken', JSON.parse(response['_body'])['access_token']);
                this.router.navigate(['/projectscontainer']);
            }, (errorData) => {
                // this.failValidation('Username or Password incorrect.')
                this.failValidation('username', 'Username or Password incorrect.');
            });
        } else {
            if (typeof this.username === 'undefined' || this.username === '') {
                this.failValidation('username', undefined);
            } else if (typeof this.password === 'undefined' || this.password === '') {
                this.failValidation('password', undefined);
            }
        }
    }

    togglePasswordView() {
        if (this.typeforPassword === 'password') {
            this.typeforPassword = 'text';
            if (this.PasswordvalidationClass.indexOf('js-password-show') === -1) {
                this.PasswordvalidationClass += ' js-password-show';
            }
        } else {
            this.typeforPassword = 'password'
            this.PasswordvalidationClass = this.PasswordvalidationClass.replace('js-password-show', '');

        }
    }

    failValidation(field: string, customMessage) {
        // this.username = undefined;
        // this.validationClass = 'validation-error js-form-is-collapsable js-form-message-open'
        // this.placeholderText = message;

        if (field === 'username') {
            this.usernameMessage = customMessage === undefined ? 'Username cannot be empty.' : customMessage;
            this.Usernamevalidationfalse = true;
            this.UsernamevalidationClass = 'validation-error js-form-is-collapsable';


        } else {
            this.passwordMessage = 'Password cannot be empty.';
            this.Passwordvalidationfalse = true;

            this.PasswordvalidationClass = 'validation-error js-form-is-collapsable';
            if (this.typeforPassword === 'text' && this.PasswordvalidationClass.indexOf('js-password-show') === -1) {
                this.PasswordvalidationClass += 'js-password-show';
            }
        }

    }

    showError(field: string) {
        if (field === 'username') {
            if (this.showUsernameMessage) {
                this.UsernamevalidationClass = 'validation-error js-form-is-collapsable';
            } else {
                this.UsernamevalidationClass += ' js-form-message-open';
            }

            this.showUsernameMessage = !this.showUsernameMessage;
        } else {
            if (this.showPasswordMessage) {
                this.PasswordvalidationClass = 'validation-error js-form-is-collapsable';

            } else {
                this.PasswordvalidationClass += ' js-form-message-open';
            }
            if (this.typeforPassword === 'text' && this.PasswordvalidationClass.indexOf('js-password-show') === -1) {
                this.PasswordvalidationClass += 'js-password-show';
            }

            this.showPasswordMessage = !this.showPasswordMessage;
        }

    }
    resetField(field: string) {
        if (field === 'username') {
            this.UsernamevalidationClass = '';
            this.usernameMessage = '';
            this.Usernamevalidationfalse = false;
        } else {
            if (this.typeforPassword === 'text') {
                this.PasswordvalidationClass = 'js-password-show';
            } else {
                this.PasswordvalidationClass = '';
            }

            this.PasswordvalidationClass = '';
            this.Passwordvalidationfalse = false;
            this.passwordMessage = '';
        }
    }
    @HostListener('document:keydown', ['$event'])
    keyDownFunction(event: KeyboardEvent) {
        if (event.keyCode === 13) { this.login(); }
    }

}
