export interface ConfirmModel {
  title: string;
  message: string;
  projectId: string;
  parentId: string;
}
