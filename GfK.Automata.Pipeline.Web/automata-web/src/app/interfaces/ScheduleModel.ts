﻿export class ScheduleModel {
    PipelineID: number;
    StartTime: any;
    Enabled: boolean;
    Recurrence: any;
    TimeZoneID: any;
    DailyRecurrenceType: any;
    RecurEvery: number;
    OnMonday: boolean;
    OnTuesday: boolean;
    OnWednesday: boolean;
    OnThursday: boolean;
    OnFriday: boolean;
    OnSaturday: boolean;
    OnSunday: boolean;
    MonthlyRecurrenceType: any;
    DayOfWeekOffset: any;
    DayOfMonth: any;
    YearlyRecurrenceType: any;
    MonthOfYear: any;
    NextFireTime: any;
}
