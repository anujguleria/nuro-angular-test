﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {ProjectsService} from '../Services/projects.service';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private projectsService: ProjectsService) { }

    canActivate() {
        return new Promise((resolve) => {
            this.projectsService.getProjects().toPromise().then(() => {
                return resolve(true);
            }).catch(() => {
                return resolve(false);
            });
        });
    }
}
