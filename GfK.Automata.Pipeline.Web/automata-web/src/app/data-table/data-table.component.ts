import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ApiService } from '../Services/api.service';
import { DataService } from '../Services/data.service';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { environment } from 'environments/environment';
import { UpdateGroupComponent } from '../admin/update-group/update-group.component';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
  animations: [
      trigger('slideInOut', [
          state('in', style({

              opacity: 1.0,
              transform: 'translate3d(0, 0, 0)'
          })),
          state('out', style({

              opacity: 0.0,
              transform: 'translate3d(100%, 0, 0)'
          })),
          transition('in => out', animate('400ms ease-in-out')),
          transition('out => in', animate('400ms ease-in-out'))
      ]),
  ]
})
export class DataTableComponent implements OnInit {
  @Input()
  data;
  @Input()
  columnconfig;
  @Input()
  events;
  @Input()
  settings;
  @Input()
  enableDropdown;
  @Input()
  suggestionData;
  @Input()
  SelectedRowInfo;
  @Input()
  menuState;
  @Output()
  close = new EventEmitter<any>();

  dropDownEnabled: false;
  selectedOption: any = {};
  page = 1;
  search = '';
  textValue = '';
  selectedSuggestion = undefined;
  DataObject = {};

  constructor(private apiService: ApiService, private dataService: DataService, private dialogService: DialogService) {

  }
  initialize() {
    if (this.settings !== undefined && this.settings.TableName === 'UserAccess') {
      this.selectedOption = { name: 'Execute', id: 1 };
    }
  }
  ngOnInit() {
    this.initialize();
  }
  checkdisable(row, column) {
    if (column['dependency'].length === 0) {
      return false;
    } else {
      return row[column['dependency'][column['dependency'].length - 1]];
    }
  }
  customEvent($event, row) {
    if (this.events.customEvent(this.settings.TableName, row)) {
      this.events.customEvents(this.settings.TableName, row);
    }
  }
  delete($event, row) {

    this.data.splice(this.data.indexOf(row), 1);
    let Key = '';
    if (this.settings.TableName === 'toGroup') {
      Key = 'Email';
    } else if (this.settings.TableName === 'toUsers') {
      Key = 'Name';
    }
    this.sortAndSaveData(Key);
  }
  deleteRow($event, row) {
    this.events.deleteRow(row);
  }
  addEvent($event) {
    let allow = false;
    this.DataObject = {};
    this.DataObject['name'] = this.textValue;
    if (this.settings.TableName === 'UserAccess') {
      if (this.selectedOption.id === 2) {
        this.DataObject['Access'] = '1,2,3';
      } else if (this.selectedOption.id === 3) {
        this.DataObject['Access'] = '1,3';
      } else {
        this.DataObject['Access'] = this.selectedOption.id;
      }
      allow = true;
      this.textValue = '';
    } else if (this.settings.TableName === 'toGroup') {
      if (this.selectedSuggestion !== undefined) {
        this.DataObject = {};
        if (this.data === null) {
          this.data = [];
        }
        this.data.push(this.selectedSuggestion);
        this.sortAndSaveData('Email');
      }
    } else if (this.settings.TableName === 'Group Access') {
      allow = true;
      this.textValue = '';
    } else if (this.settings.TableName === 'toUsers') {
      if (this.selectedSuggestion !== undefined) {
        this.DataObject = {};
        if (this.data === null) {
          this.data = [];
        }
        this.data.push(this.selectedSuggestion);
        this.sortAndSaveData('Name');
      }
    }

    if (allow) {
      if (this.events.addEvent(this.settings.TableName, this.DataObject)) {
        this.events.addEvent(this.settings.TableName, this.DataObject);
      }
    }
  }
  resetSelectedSuggestion() {
    if (this.selectedSuggestion !== undefined) {
      this.selectedSuggestion = undefined;
    }
  }
  dialogSave() {
    if (this.events.dialogSave) {
      this.closeDialog();
      this.events.dialogSave(this.settings.TableName, this.DataObject);
    }
  }
  closeDialog() {
    this.close.next();
  }

  sortAndSaveData(key: string) {
    this.data.sort(function (a, b) {
      return a[key].localeCompare(b[key], undefined /* Ignore language */, { sensitivity: 'base' })
    });
    this.DataObject['currentDataSet'] = this.data;
    this.DataObject['parentRow'] = this.SelectedRowInfo;

    this.textValue = '';
    this.selectedSuggestion = undefined;
  }
  cellClicked($event, row, col) {
    if (this.events.rowClicked($event, row, col)) {
      this.events.rowClicked($event, row, col);
    }
  }

  buttonDisabled() {
    return this.textValue === undefined || this.textValue === '' || this.textValue === null;
  }

  toggleGroupSettings(index: number) {

      // 1-line if statement that toggles the value:
      this.menuState[index] = this.menuState[index] === 'out' ? 'in' : 'out';
  }
  deleteGroup(event) {

      const targetId = event.currentTarget.id;
      const row = targetId.split('group-delete-')[1];
      let toSend: any = {};

      toSend = this.data[row];
      this.dataService.setGroupData(this.data);
      this.dataService.showHideMask.emit(true);
      this.dialogService.addDialog(UpdateGroupComponent, {
          title: 'Delete Group',
          message: toSend['Name'],
          parentId: row
      }).subscribe(response => {
          this.dataService.showHideMask.emit(false);
          if (response) {
            this.data.splice(row, 1);
          }
      });

      this.menuState[row] = this.menuState[row] === 'out' ? 'in' : 'out';
  }
  renameGroup(event) {

      const targetId = event.currentTarget.id;
      const row = targetId.split('group-rename-')[1];
      let toSend: any = {};

      toSend = this.data[row];
      this.dataService.setGroupData(this.data);
      this.dataService.showHideMask.emit(true);
      this.dialogService.addDialog(UpdateGroupComponent, {
          title: 'Rename Group',
          message: toSend['Name'],
          parentId: row
      }).subscribe(response => {
          this.dataService.showHideMask.emit(false);
          if (response) {

          }
      });

      this.menuState[row] = this.menuState[row] === 'out' ? 'in' : 'out';
  }
}
