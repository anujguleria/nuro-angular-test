﻿import { Component, OnInit, Output, EventEmitter } from '@angular/core';
// import { Router } from '@angular/router';
import { ConfirmModel } from '../../interfaces/ConfirmModel';
import { DialogService } from 'ng2-bootstrap-modal';
import { DataService } from '../../Services/data.service'
import { NotificationsService } from '../../Services/notifications.service';
import { Resolve } from '@angular/router';
import { SignalR, ISignalRConnection, BroadcastEventListener } from 'ng2-signalr';
import { Injectable } from '@angular/core';
import * as _ from 'underscore';
declare var $: any;

// import { HeaderComponent } from './header/header.component';
// import { DataService } from './Services/data.service';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.css']
    // providers: [UserLoginService],
    // host: { '(window:keydown)': 'keyDownFunction($event)' }
})
export class NotificationsComponent implements ConfirmModel, OnInit {
    @Output() toggleNotifications = new EventEmitter();
    @Output() updateNotifications = new EventEmitter();
    title: string;
    message: string;
    parentId: string;
    projectId: string;
    notificationCount: number;
    notificationCollection: any[];
    signalRConnection: ISignalRConnection;
    notificationsOpen: boolean;

    constructor(
        private dialogService: DialogService,
        private dataservice: DataService,
        private notificationsService: NotificationsService,
        private _signalR: SignalR) { }
    ngOnInit() {
        $.signalR.ajaxDefaults.headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' +
            localStorage.getItem('authToken'),  // accessToken contain bearer value.,
        };
        this.notificationsOpen = false;
        this.loadNotifications();
        const userInfo = this.dataservice.getUserInfo();
        this.signalRConnection = this._signalR.createConnection({
            hubName: 'NotificationHub',
            qs: 'id=' + ''

        });


        this.signalRConnection.start().then(results => {
            // 1.create a listener object
            const onMessageSent$ = new BroadcastEventListener<string>('newNotification');
            // 2.register the listener
            this.signalRConnection.listen(onMessageSent$);

            // 3.subscribe for incoming messages
            onMessageSent$.subscribe((notification: any) => {
                // append to notification list and re-sort
                this.notificationCollection.push(notification);
                console.log(notification);
                // increment notification count
                this.notificationCount++;
            });

            // 4.if we had to send a message
            /*this.signalRConnection.invoke('Echo', 'huh').then((data: string) => {
                console.log(data);
            });*/
        });


    }
    loadNotifications() {
        this.notificationsService.getNotifications().subscribe(res => {
            this.notificationCollection = res;
            this.notificationCount = this.notificationCollection.length;
        });

    }

    showNotifications() {
        this.notificationCount = 0;
        this.notificationsOpen = !this.notificationsOpen;

    }
    getCount() {
        return this.notificationCount;
    }
    formatDate(date: any) {
        return this.dataservice.formatDate(date);
    }
    removeNotification(notification) {
        // delete notification
        this.notificationsService.deleteNotification(notification.NotificationID).subscribe(res => {
            this.notificationCollection = this.notificationCollection.filter(x => x !== notification);
        });
    }

    removeAll() {
        // delete all notifications
        const deleteList = this.notificationCollection,
            notificationService = this.notificationsService;
        _.each(deleteList, function (notification) {
            notificationService.deleteNotification(notification.NotificationID).subscribe(res => {

            });
        });
        this.notificationCollection = [];
        this.notificationCount = 0;
    }
}
