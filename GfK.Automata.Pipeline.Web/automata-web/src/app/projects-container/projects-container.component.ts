import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ProjectsService } from '../Services/projects.service';
import { DataService } from '../Services/data.service';
import { ApiService } from '../Services/api.service';
import { TaskService } from '../task/shared/task.service';
import { environment } from '../../environments/environment'
import * as _ from 'underscore';
import * as moment from 'moment';
import { SignalR, ISignalRConnection, BroadcastEventListener } from 'ng2-signalr';
import { DxTabPanelModule, DxTemplateModule, DxTabPanelComponent } from 'devextreme-angular';

@Component({
    selector: 'app-projects-container',
    templateUrl: './projects-container.component.html',
    styleUrls: ['./projects-container.component.css'],
    animations: [
        trigger('slideInOut', [
            state('in', style({
                position: 'fixed',
                opacity: 1.0,
                transform: 'translate3d(0, 0, 0)'
            })),
            state('out', style({
                position: 'fixed',
                opacity: 0.0,
                transform: 'translate3d(100%, 0, 0)'
            })),
            transition('in => out', animate('400ms ease-in-out')),
            transition('out => in', animate('400ms ease-in-out'))
        ]),
    ]
})
export class ProjectscontainerComponent implements OnInit {
    projectsCollection: any[];
    tabsCollection: any[];
    isProjectPanelOpen: boolean;
    clientHeight: number;
    selectedTab = 0;

    activeAdmin = false;
    enableAdmin = false;
    menuState = 'out';
    nothing = 'nothing';
    auto = 'auto';
    allModules: any[];
    signalRConnection: ISignalRConnection;
    @ViewChild('tabPanel') TabPanel: DxTabPanelComponent;
    toggleMenu() {
        // 1-line if statement that toggles the value:
        this.menuState = this.menuState === 'out' ? 'in' : 'out';
    }
    constructor(private projectsService: ProjectsService,
        private dataservice: DataService,
        private apiservice: ApiService,
        private taskService: TaskService,
        private _signalR: SignalR) {
        this.isProjectPanelOpen = true;
        this.dataservice.newProjectCreated.subscribe(res => {
            const newProjectToOpen = res;
            this.projectsService.updateProjectLastAccess(newProjectToOpen.ProjectID).subscribe(response => {
                let timestamp = response._body;
                // parse out double quotes
                if (timestamp.length > 0 && timestamp.substr(0, 1) === '"') {
                    timestamp = timestamp.substr(1, timestamp.length - 2);
                }
                const projectUsers = [];
                projectUsers.push({ 'LastAccess': timestamp });
                newProjectToOpen['projectUsers'] = projectUsers;
                newProjectToOpen['isTabed'] = true;
                newProjectToOpen['isSelectedInView'] = true;
                this.selectProject(newProjectToOpen);
                this.projectsCollection.push(newProjectToOpen);
            });

        });

        this.dataservice.ActivateAdminPanel.subscribe((value: boolean) => {
            const adminObj = { 'Name': 'Admin' };
            const adminProj = _.findWhere(this.tabsCollection, adminObj);
            if (adminProj === undefined) {
                this.tabsCollection.push(adminObj);
                this.selectedTab = this.tabsCollection.length - 1;
                this.changeIndex();
            }
        });
        this.apiservice.get(environment.apiUrl + 'api/Users/GetCurrentLoggedInUser').subscribe((response) => {
            this.dataservice.setUserInfo(response);
        });

    }

    ngOnInit() {
        this.loadProjects();
        this.tabsCollection = [];
        this.clientHeight = window.innerHeight - 108; // headerHeight+footerHeight=115
        this.taskService.getModules().subscribe(res => {
            this.allModules = res;
        });
    }
    loadProjects() {
        this.projectsService.getProjectsWithPipelines().subscribe(res => {
            this.projectsCollection = res;
            this.projectsCollection = _.sortBy(this.projectsCollection, function (item) {
                return (item.projectUsers && item.projectUsers.length ?
                    Date.parse(item.projectUsers[0].LastAccess) : Date.parse('1900-01-01'));
            }).reverse();
            this.tabsCollection = [];
            this.dataservice.ShowProjectButton.emit(true);
            this.dataservice.subscribeNotifications();
        });

    }
    closeTab(project) {
        const indexOfClosedProject = this.tabsCollection.indexOf(project);
        const tempSelected = this.selectedTab;
        this.tabsCollection.splice(indexOfClosedProject, 1);
        if (indexOfClosedProject <= this.selectedTab) {
            this.selectedTab -= 1;
        } else {
            this.selectedTab = tempSelected;
        }
        this.TabPanel['selectedIndex'] = this.selectedTab;

    }

    selectProject(project) {
        this.projectsService.updateProjectLastAccess(project.ProjectID).subscribe(response => {

            let timestamp = response._body;
            // parse out double quotes
            if (timestamp.length > 0 && timestamp.substr(0, 1) === '"') {
                timestamp = timestamp.substr(1, timestamp.length - 2);
            }
            if (!project.projectUsers[0]) {
                const projectUsers = [];
                projectUsers.push({ 'LastAccess': timestamp });
                project['projectUsers'] = projectUsers;
            } else {
                project.projectUsers[0].LastAccess = timestamp;
            }
            this.updateTabCollection(project);
        });
        this.activeAdmin = false;
    }

    updateTabCollection(project) {
        this.tabsCollection.forEach(innerProject => {
            innerProject['viewEnabled'] = false;
        });
        project['viewEnabled'] = true;
        //  update the project in projectsCollection and then reorder tabsCollection
        if (this.tabsCollection.indexOf(project) === -1) {
            this.tabsCollection.push(project);
        }
        // going to try waiting a second to change this
        const currentTab = this.tabsCollection.indexOf(project);
        this.selectedTab = currentTab;
        this.changeIndex();
    }

    changeIndex() {
        setTimeout(this.applyIndex.bind(this), 500);
    }

    applyIndex() {
        if (this.TabPanel) {
            this.TabPanel['selectedIndex'] = this.selectedTab;
        }
    }
    preventFocusShift($event) {

        // $event.element[0].onmousedown= function(ea){

        //     if (ea.target.name != 'input-2' &&
        //         ea.target.name != 'input-3' &&
        //         ea.target.name != 'input-4' &&
        //         ea.target.name != 'input-5' &
        //         ea.target.name != 'input-8' &&
        //         ea.target.name != 'input-6' &&
        //         ea.target.name != 'input-7' &&
        //         typeof( ea.target.className)==='string' &&
        //         ea.target.className.indexOf('ui-inputtext ui-widget ui-state-default ui-corner-all')===-1) {
        //         ea.preventDefault();
        //     }
        // }
    }
    @HostListener('document:keydown', ['$event'])
    hotkeys($event: KeyboardEvent) {
        if ($event.ctrlKey && $event.keyCode === 39) {
            // right
            if (this.selectedTab !== this.tabsCollection.length - 1) {
                this.selectedTab++;
                this.TabPanel['selectedIndex'] = this.selectedTab;
            }
        } else if ($event.ctrlKey && $event.keyCode === 37) {
            // left
            if (this.selectedTab !== 0) {
                this.selectedTab--;
                this.TabPanel['selectedIndex'] = this.selectedTab;
            }
        }
    }
}
