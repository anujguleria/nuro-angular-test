import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProjectsService } from '../../Services/projects.service';

@Component({
  selector: 'app-projects-panel',
  templateUrl: './projects-panel.component.html',
  styleUrls: ['./projects-panel.component.css']
})
export class ProjectsPanelComponent implements OnInit {
  @Input() public projectsCollection: any[];
  @Output() viewOpened = new EventEmitter<object>();
  @Input() isAdminActive: boolean;
  @Output() isAdminActiveChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  isProjectPanelOpen: boolean;
  constructor(private projectsService: ProjectsService) {
    this.isProjectPanelOpen = true;
  }

  ngOnInit() {
  }
  selectProject(project) {
    this.projectsService.updateProjectLastAccess(project.ProjectID).subscribe(response => {
      let timestamp = response._body;
      // parse out double quotes
      if (timestamp.length > 0 && timestamp.substr(0, 1) === '"') {
        timestamp = timestamp.substr(1, timestamp.length - 2);
      }
      if (!project.projectUsers[0]) {
        const projectUsers = [];
        projectUsers.push({ 'LastAccess': timestamp });
        project['projectUsers'] = projectUsers;
      } else {
        project.projectUsers[0].LastAccess = timestamp;
      }
    });
    this.viewOpened.emit(project);
    this.isAdminActive = false;
    this.isAdminActiveChange.emit(this.isAdminActive);
  }
  getHeight(proj: any) {
    let numberOfExpendedChilren = 0;
    if (proj.isExpended && proj.pipeLines) {
      numberOfExpendedChilren = proj.pipeLines.length;
      for (let i = 0; i < proj.pipeLines.length; i++) {
        numberOfExpendedChilren = numberOfExpendedChilren + this.getNumberOFExpandedChildren(proj.pipeLines[i]);
      }
    }
    return { 'height': numberOfExpendedChilren * 24 + 'px' }
  }
  getNumberOFExpandedChildren(pipe): number {
    if (pipe.Pipelines) {
      let numberOfExpendedChilren = pipe.Pipelines.length;
      for (let i = 0; i < pipe.Pipelines.length; i++) {
        numberOfExpendedChilren = numberOfExpendedChilren + this.getNumberOFExpandedChildren(pipe.Pipelines[i]);
      }
      return numberOfExpendedChilren;
    } else {
      return 0;
    }
  }

}



