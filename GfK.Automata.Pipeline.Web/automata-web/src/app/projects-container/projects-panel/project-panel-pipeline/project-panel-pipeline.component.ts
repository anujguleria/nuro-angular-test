import { Component, Input } from '@angular/core';
import { DataService } from '../../../Services/data.service';
@Component({
  selector: 'app-proejct-panel-pipeline',
  templateUrl: './project-panel-pipeline.component.html',
  styleUrls: ['./project-panel-pipeline.component.css']
})
export class ProjectPanelPipelineComponent {
  @Input() public pipeline: any;
  @Input() public showChildren: any;
  dateService: DataService;

  constructor(private dataService: DataService) {
    this.dataService = dataService;
  }
  getPipelineTooltip(pipeline) {
    if (pipeline.LastRun && pipeline.LastStatus) {
      return this.dataService.formatDate(pipeline.LastRun) + ' / ' + pipeline.LastStatus;
    }
    return '';
  }
  getHeight(pipe: any) {
    let numberOfExpendedChilren = 1;
    numberOfExpendedChilren = numberOfExpendedChilren + this.getNumberOFExpandedChildren(pipe);
    return { 'height': numberOfExpendedChilren * 24 + 'px' }
  }
  getNumberOFExpandedChildren(pipe): number {
    if (pipe.Pipelines) {
      let numberOfExpendedChilren = pipe.Pipelines.length;
      for (let i = 0; i < pipe.Pipelines.length; i++) {
        numberOfExpendedChilren = numberOfExpendedChilren + this.getNumberOFExpandedChildren(pipe.Pipelines[i]);
      }
      return numberOfExpendedChilren;
    } else {
      return 0;
    }
  }

}
