import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectPanelPipelineComponent } from './project-panel-pipeline.component';

describe('ProjectPanelPipelineComponent', () => {
  let component: ProjectPanelPipelineComponent;
  let fixture: ComponentFixture<ProjectPanelPipelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectPanelPipelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPanelPipelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
