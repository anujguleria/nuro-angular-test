﻿import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { ConfirmModel } from '../../../interfaces/ConfirmModel';
import { PipelineService } from '../../../Services/pipeline.service';
import { DataService } from '../../../Services/data.service';
@Component({
  selector: 'app-add-new-pipeline',
  templateUrl: './add-new-pipeline.component.html',
  styleUrls: ['./add-new-pipeline.component.css']
})
export class AddNewPipelineComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
  title: string;
  message: string;
  projectId: string;
  parentId: string;
  pipelineName: string;
  validationError: string;
  errorEnabled: boolean;
  isErorrOpened: boolean;
  constructor(dialogService: DialogService, private pipelineSevice: PipelineService, private dataService: DataService) {
    super(dialogService);
  }
  ngOnInit() {
  }
  showError(message) {
    this.validationError = message;
    this.errorEnabled = true;
  }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code

    if (this.pipelineName === undefined || (this.pipelineName.trim() === '')) {
      this.showError('Please enter a Pipeline Name.');
    } else {
      const regexp = new RegExp(this.dataService.num_letter_spaces_underscore_rejexExp);
      const test: boolean = regexp.test(this.pipelineName);
      if (!test) {
        this.showError('Pipeline Name only supports numbers, letters, spaces and underscores.');
      } else {
        const context = {
          'ProjectID': this.projectId,
          'ParentID': this.parentId
        };
        this.pipelineSevice.getPipelineByName(this.pipelineName, context).subscribe(res => {
          this.showError('Pipeline Name in use, please enter another name.');
        }, error => {
          const pipelineObj = {};
          pipelineObj['ProjectID'] = this.projectId;
          pipelineObj['ParentID'] = this.parentId;
          pipelineObj['Name'] = this.pipelineName;
          this.pipelineSevice.createPipeline(pipelineObj).subscribe((pipelineId) => {
            this.result = true;
            this.close();
          })
        });

      }
    }
  }
}
