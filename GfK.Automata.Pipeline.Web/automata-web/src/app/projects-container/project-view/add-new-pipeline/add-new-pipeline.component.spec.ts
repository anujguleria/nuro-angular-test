import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewPipelineComponent } from './add-new-pipeline.component';

describe('AddNewPipelineComponent', () => {
  let component: AddNewPipelineComponent;
  let fixture: ComponentFixture<AddNewPipelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewPipelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewPipelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
