﻿import { Component, Input, OnInit } from '@angular/core';
import { DialogService } from 'ng2-bootstrap-modal';
import { DataService } from '../../Services/data.service'
import { ProjectsService } from '../../Services/projects.service';
import { ConfirmModel } from '../../interfaces/ConfirmModel';
import { AddNewPipelineComponent } from '../project-view/add-new-pipeline/add-new-pipeline.component';
import * as _ from 'underscore';
@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.css']
})
export class ProjectViewComponent implements OnInit, ConfirmModel {
  title: string;
  message: string;
  projectId: string;
  parentId: string;
  @Input() project: any;
  @Input() allModules: any[];

  constructor(private dialogService: DialogService, private dataservice: DataService, private projectsService: ProjectsService) {
    const self = this;
    this.dataservice.pipelineDeleted.subscribe(res => {
      this.projectsService.getPipelinesOfProject(this.project.ProjectID).subscribe(res2 => {
        this.project.pipeLines = res2;
      });
    });
  }

  ngOnInit() {

  }

  addPipeLine(project) {
    this.dataservice.showHideMask.emit(true);
    this.dialogService.addDialog(AddNewPipelineComponent, {
      title: 'New Pipeline',
      message: undefined,
      parentId: null,
      projectId: project.ProjectID + '',
    })
      .subscribe((isConfirmed) => {
        if (isConfirmed) {
          this.projectsService.getPipelinesOfProject(project.ProjectID).subscribe(res => {
            this.project.pipeLines = res;
          })
        }
        this.dataservice.showHideMask.emit(false);
      });
  }

}
