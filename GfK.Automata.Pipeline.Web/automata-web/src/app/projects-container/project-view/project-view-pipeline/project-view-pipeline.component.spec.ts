import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectViewPipelineComponent } from './project-view-pipeline.component';

describe('ProjectViewPipelineComponent', () => {
  let component: ProjectViewPipelineComponent;
  let fixture: ComponentFixture<ProjectViewPipelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectViewPipelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectViewPipelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
