﻿import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { ConfirmModel } from '../../../../interfaces/ConfirmModel';
import { PipelineService } from '../../../../Services/pipeline.service';
import { DataService } from '../../../../Services/data.service';
import { UserService } from '../../../../Services/user.service';
import * as _ from 'underscore';
@Component({
  selector: 'app-pipeline-settings',
  templateUrl: './pipeline-settings.component.html',
  styleUrls: ['./pipeline-settings.component.css']
})

export class PipelineSettingsComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
  title: string;
  message: string;
  projectId: string;
  parentId: string;
  validationError: string;
  errorEnabled: boolean;
  isErorrOpened: boolean;
  pipelineData: any;
  disable = false;
  errorMessage = undefined;
  usersArray: any;
  manipulatedPipelineSettingData = [];
  pipelineNotificationSettings: any;
  textValue = '';
  suggestionData = [];
  selectedSuggestion: {};
  dropDownEnabled: boolean;
  selectedOption = {};
  settingData = {}
  buttonText: string;
  settings = {
    dropDown: [{ name: 'All Notifications', id: 3 }, { name: 'On Success', id: 1 }, { name: 'On Failure', id: 2 }],
  }
  SearchKey = 'Email';
  gridData = {
    data: this.manipulatedPipelineSettingData,
    columnconfig: [
      { prop: 'userName', as: 'Assigned Users', cssClass: 'equalWidth', cellTemplate: 2, dependency: [] },
      { prop: undefined, as: 'Notify:', cssClass: 'equalWidth', cellTemplate: 0, dependency: [] },
      { prop: 'pipelineNotificationOnSuccess', as: 'On Success', cssClass: 'equalWidth', cellTemplate: 1, dependency: [] },
      { prop: 'pipelineNotificationOnFailure', as: 'On Failure', cssClass: 'equalWidth', cellTemplate: 1, dependency: [] },
      {
        prop: undefined, as: '', cssClass: 'equalWidth',
        template: '<div class="glyphicon glyphicon-remove show-hand"></div>', cellTemplate: 4, dependency: []
      }
    ]
  }
  constructor(dialogService: DialogService, private pipelineService: PipelineService, private dataService: DataService,
    private userService: UserService) {
    super(dialogService);
  }
  notificationTypeSettingUpdate(obj, row, col) {
    alert('Nofication update clicked ' + row);

  }

  deleteUser(row) {
    console.log(row);
  }

  ngOnInit() {
    switch (this.title) {
      case 'Delete Pipeline':
        this.pipelineData = this.dataService.getPipelineData();
        this.buttonText = 'OK';
        break;
      case 'Edit Notification Settings':
        this.buttonText = 'Save';
        this.pipelineData = this.dataService.getPipelineData();
        this.selectedOption = { name: 'All Notifications', id: 3 };
        this.pipelineService.getNoficationSettings(this.pipelineData['PipeLineID']).subscribe(res => {
          this.pipelineNotificationSettings = res;
          this.userService.getUsers().subscribe(response => {
            this.suggestionData = response;
            this.usersArray = this.manipulateUsersData(response);
            this.manipulatePipelineSettingData();

          })
        });
        break;
    }
  }

  manipulateUsersData(usersObject) {
    const user = {};
    for (let i = 0; i < usersObject.length; i++) {
      user[usersObject[i].UserID] = usersObject[i].Email;
    }
    return user;
  }

  manipulatePipelineSettingData() {
    this.manipulatedPipelineSettingData = [];
    for (let setting = 0; setting < this.pipelineNotificationSettings.length; setting++) {
      this.settingData = {};
      this.settingData['UserID'] = this.pipelineNotificationSettings[setting].UserID;
      this.settingData['userName'] = this.usersArray[this.pipelineNotificationSettings[setting].UserID];
      const settingTypeValue = this.pipelineNotificationSettings[setting].PipelineNotificationSettingType;
      this.settingData['pipelineNotificationOnSuccess'] = settingTypeValue === 1 || settingTypeValue === 3 ? true : false;
      this.settingData['pipelineNotificationOnFailure'] = settingTypeValue === 2 || settingTypeValue === 3 ? true : false;
      this.manipulatedPipelineSettingData.push(this.settingData);
    }
    this.updateGridData();
  }

  updateGridData() {
    this.gridData.data = _.sortBy(this.manipulatedPipelineSettingData, 'userName');
  }

  confirm() {
    switch (this.title) {
      case 'Delete Pipeline':
        this.DeletePipeline();
        break;
      case 'Edit Notification Settings':
        this.SavePipelineSettings();
        break;
    }
  }

  SavePipelineSettings() {
    const pipelineSettingsNewData = [];
    for (let data = 0; data < this.manipulatedPipelineSettingData.length; data++) {
      const pipelineSetting = {};
      pipelineSetting['Email'] = this.manipulatedPipelineSettingData[data]['userName'];
      pipelineSetting['PipelineID'] = this.pipelineData['PipeLineID'];
      pipelineSetting['UserID'] = this.manipulatedPipelineSettingData[data]['UserID'];
      if (this.manipulatedPipelineSettingData[data].pipelineNotificationOnSuccess &&
        this.manipulatedPipelineSettingData[data].pipelineNotificationOnFailure) {
        pipelineSetting['PipelineNotificationSettingType'] = 3;
      } else if (this.manipulatedPipelineSettingData[data].pipelineNotificationOnSuccess) {
        pipelineSetting['PipelineNotificationSettingType'] = 1;
      } else if (this.manipulatedPipelineSettingData[data].pipelineNotificationOnFailure) {
        pipelineSetting['PipelineNotificationSettingType'] = 2;
      }
      pipelineSettingsNewData.push(pipelineSetting);
    }
    this.pipelineService.updateNotificationSettings(pipelineSettingsNewData, this.pipelineData['PipeLineID']).subscribe(res => {
      this.close();
    }, (error) => {
      this.disable = true;
      this.errorMessage = error._body;
    });
  }

  DeletePipeline() {
    if (this.pipelineData['Pipelines'] && this.pipelineData['Pipelines'].length > 0) {
      this.disable = true;
      this.errorMessage = 'Error occurred while deleting the pipeline, please make sure this pipeline has no children.';
    } else if (this.pipelineData['Schedule'] && this.pipelineData['Schedule']['Enabled'] && this.pipelineData['Schedule']['NextFireTime']) {
      if (new Date(this.pipelineData['Schedule']['NextFireTime']) > new Date()) {
        this.disable = true;
        this.errorMessage = 'Error occurred while deleting the pipeline, please make sure this pipeline isn\'t scheduled to run.';
      }
    } else {
      this.pipelineService.deletePipeline(this.pipelineData['PipeLineID']).subscribe((result) => {
        if (result.status === 200) {
          this.result = true;
          this.close();
        }
      }, (error) => {
        this.disable = true;
        this.errorMessage = error._body;
      });
    }

  }

  buttonDisabled() {
    return this.textValue === undefined || this.textValue === '' || this.textValue === null;
  }

  addUser() {
    const listOfUsernames = _.pluck(this.manipulatedPipelineSettingData, 'userName');
    const indexOfUsername = listOfUsernames.indexOf(this.selectedSuggestion['Email']);
    if (indexOfUsername !== -1) {
      this.manipulatedPipelineSettingData.splice(indexOfUsername, 1);
    }
    const pipelineSettingObject = {};
    this.settingData = {};
    this.settingData['userName'] = this.selectedSuggestion['Email'];
    this.settingData['pipelineNotificationOnSuccess'] = this.selectedOption['id'] === 1 || this.selectedOption['id'] === 3 ? true : false;
    this.settingData['pipelineNotificationOnFailure'] = this.selectedOption['id'] === 2 || this.selectedOption['id'] === 3 ? true : false;
    this.settingData['UserID'] = this.selectedSuggestion['UserID'];
    this.manipulatedPipelineSettingData.push(this.settingData);
    this.updateGridData();
    this.textValue = '';
  }

  resetSelectedSuggestion() {
    if (this.selectedSuggestion !== undefined && _.keys(this.selectedSuggestion).length > 0) {
      this.selectedSuggestion = {};
    }
  }

  isSelectedSuggestionDefined() {
    if (this.selectedSuggestion !== undefined && _.keys(this.selectedSuggestion).length > 0) {
      return false;
    } else {
      return true;
    }
  }



}
