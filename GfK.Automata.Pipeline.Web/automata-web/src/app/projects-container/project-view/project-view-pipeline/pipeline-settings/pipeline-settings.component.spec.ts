import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipelineSettingsComponent } from './pipeline-settings.component';

describe('PipelineSettingsComponent', () => {
  let component: PipelineSettingsComponent;
  let fixture: ComponentFixture<PipelineSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipelineSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipelineSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
