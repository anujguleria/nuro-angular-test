﻿import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { ConfirmModel } from '../../../../interfaces/ConfirmModel';
import { PipelineService } from '../../../../Services/pipeline.service';
import { DataService } from '../../../../Services/data.service';
@Component({
    selector: 'app-rename-pipeline',
    templateUrl: './rename-pipeline.component.html',
    styleUrls: ['./rename-pipeline.component.css']
})
export class RenamePipelineComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
    title: string;
    message: string;
    projectId: string;
    parentId: string;
    pipelineId: string;
    validationError: string;
    errorEnabled: boolean;
    isErorrOpened: boolean;
    pipelineName: string;
    pipeline: any;
    constructor(dialogService: DialogService, private pipelineService: PipelineService, private dataService: DataService) {
        super(dialogService);
    }


    ngOnInit() {
        this.pipelineId = this.parentId;
        this.pipelineService.getPipeline(this.pipelineId).subscribe(res => {
            this.pipeline = res;
            this.pipelineName = this.pipeline.Name;
        });
    }
    showError(message) {
        this.validationError = message;
        this.errorEnabled = true;
    }
    confirm() {

        // we set dialog result as true on click on confirm button,
        // then we can get dialog result from caller code

        if (this.pipelineName === undefined || (this.pipelineName.trim() === '')) {
            this.showError('Please enter a Pipeline Name.');
        } else {
            const regexp = new RegExp(this.dataService.num_letter_spaces_underscore_rejexExp);
            const test: boolean = regexp.test(this.pipelineName);
            if (!test) {
                this.showError('Pipeline Name only supports numbers, letters, spaces and underscores.');
            } else {
                const context = {
                    'ProjectID': this.pipeline.ProjectID,
                    'ParentID': this.pipeline.ParentID
                };
                this.pipelineService.getPipelineByName(this.pipelineName, context).subscribe(res => {
                    this.showError('Pipeline Name in use, please enter another name.');
                }, error => {
                    this.pipeline['Name'] = this.pipelineName;
                    this.pipelineService.updatePipeline(this.pipeline, this.pipelineId).subscribe(res => {
                        this.result = true;
                        this.close();
                    }, error2 => {
                        this.showError('Error saving new name.');
                    });
                });
            }
        }
    }
}
