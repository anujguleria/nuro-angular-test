import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { PipelineService } from '../../../Services/pipeline.service';
import { DataService } from '../../../Services/data.service';
import { ConfirmModel } from '../../../interfaces/ConfirmModel';
import { PipelineInstanceComponent } from '../project-view-pipeline/pipeline-instance/pipeline-instance.component';
import { NewTaskComponent, TaskModel } from '../../../task/new-task/new-task.component';
import { AddNewPipelineComponent } from '../../project-view/add-new-pipeline/add-new-pipeline.component';
import { RunPipelineComponent } from '../project-view-pipeline/run-pipeline/run-pipeline.component';
import { RenamePipelineComponent } from '../project-view-pipeline/rename-pipeline/rename-pipeline.component';
import { ConfirmComponent } from '../../../shared/confirm-dialog.component';
import { WarningComponent } from '../../../shared/warning-dialog.component';
import { ClickOutsideDirective } from '../../../directives/click-outside.directive';
import { TaskService } from '../../../task/shared/task.service';
import { SignalR, ISignalRConnection, BroadcastEventListener } from 'ng2-signalr';
import { ManageParametersComponent, ParametersModel } from '../project-view-pipeline/pipeline-parameters/manage-parameters.component';
declare var $: any;
import { trigger, state, style, transition, animate } from '@angular/animations';
import { PipelineSettingsComponent } from '../project-view-pipeline/pipeline-settings/pipeline-settings.component';

import * as _ from 'underscore';
export interface PipelineInstanceModel {
    Title: string;
    PipelineTaskInstances: any[];
    PipelineInstanceId: number;
    PipelineId: number;
    Pipeline: null;
    Started: string;
    Ended: string;
    Status: string;
    Message: string;
}
@Component({
    selector: 'app-project-view-pipeline',
    templateUrl: './project-view-pipeline.component.html',
    styleUrls: ['./project-view-pipeline.component.css'],
    animations: [
        trigger('slideInOut', [
            state('in', style({

                opacity: 1.0,
                transform: 'translate3d(0, 0, 0)'
            })),
            state('out', style({

                opacity: 0.0,
                transform: 'translate3d(100%, 0, 0)'
            })),
            transition('in => out', animate('400ms ease-in-out')),
            transition('out => in', animate('400ms ease-in-out'))
        ]),
    ]
})
export class ProjectViewPipelineComponent implements OnInit, PipelineInstanceModel, ConfirmModel, OnDestroy {
    @Input() pipeLine;
    @Input() project;
    @Input() allModules: any[];
    Title: string;
    PipelineTaskInstances: any[];
    PipelineInstanceId: number;
    PipelineId: number;
    Pipeline: null;
    Started: string;
    Ended: string;
    Status: string;
    Message: string;
    instancePanelCollapsed = false;
    pipelineInstances: any[];
    tasks: any[] = [];
    dataService: DataService;
    showSchedular = false;
    showInstances = true;
    PipelineIDforSchedule: number;
    taskTypeIDImageMap = {
        2: 'Datasource_63.jpg',
        3: 'cleaning_63.png',
        6: 'Scripts_63.png',
        4: 'Transform_63.png',
        1: 'tools_63.jpg',
        5: 'Tables_63.png'
    };
    showContextMenu = false;
    x = '0px';
    y = '0px';
    selectedTask: any;
    selectedIndex: number;
    selectedAction: string;
    showControl = {};
    enableSubMenu = false;
    enableSubMenu2 = false;
    enableLeft = true;
    enableRight = true;
    enableAddLeft = true;
    enableAddRight = true;
    enableParameters = false;
    enableMove = true;
    enableEdit = true;
    enableDelete = true;
    startTask = {};
    endTask = {};
    // Confirm model props
    title: string;
    message: string;
    projectId: string;
    parentId: string;
    TaskIdNameMap: any;
    IntervelInstance: any;
    signalRConnection: ISignalRConnection;
    menuState = 'out';
    isAdminOrConfigure = false;

    constructor(private dialogService: DialogService, private dataservice: DataService, private pipelineService: PipelineService
        , private taskService: TaskService, private _signalR: SignalR
    ) {
        this.dataService = dataservice;
        this.showSchedular = false;
        this.showInstances = true;
    }

    ngOnInit() {
        this.dataservice.newNotifications.subscribe(res => {
            if (res['type'] === 'Pipeline' && res['pipelineId'] === this.pipeLine.PipeLineID) {
                this.pipelineService.getPipeline(res['pipelineId']).subscribe(response => {
                    this.pipeLine = response;
                });
                this.pipelineService.getPipelineInstances(res['pipelineId']).subscribe(response2 => {
                    this.pipelineInstances = _.sortBy(response2, 'Started').reverse().slice(0, 3);
                    this.dataService.pipelineInstanceDataRefreshed.emit(this.pipelineInstances);
                });
            }
        });
        this.PipelineIDforSchedule = this.pipeLine.PipeLineID;
        this.loadPipelineInstances();
        // this.IntervelInstance=setInterval(this.loadPipelineInstances.bind(this),60000);
        this.pipelineService.getPipelineTasks(this.pipeLine.PipeLineID).subscribe(res => {
            this.tasks = _.sortBy(res, 'SequencePosition');
            this.TaskIdNameMap = {};
            for (let i = 0; i < this.tasks.length; i++) {
                this.TaskIdNameMap[this.tasks[i]['PipelineTaskID']] = this.tasks[i]['Name'];
            }
            this.startTask = _.find(this.tasks, function (param) {
                return param.Name === 'System Start';
            });
            this.endTask = _.find(this.tasks, function (param) {
                return param.Name === 'System End';
            });
            this.tasks = _.filter(this.tasks, function (param) {
                return param.Name !== 'System Start' && param.Name !== 'System End'
            });

        });
        this.showSchedular = false;
        const userInfo = this.dataservice.getUserInfo();
        if (userInfo != null && userInfo['GapRoleUsers'] != null) {
            let i = 0; // 2 is for admin and 3 is configure
            while (!this.isAdminOrConfigure && i < userInfo['GapRoleUsers'].length) {
                if (userInfo['GapRoleUsers'][i]['GapRole']['GapRoleID'] === 2
                    || userInfo['GapRoleUsers'][i]['GapRole']['GapRoleID'] === 3) {
                    this.isAdminOrConfigure = true;
                }
                i++;
            }
        }
    }
    ngOnDestroy() {
        clearInterval(this.IntervelInstance);
    }
    loadPipelineInstances() {
        if (this.project['viewEnabled']) {
            this.pipelineService.getPipelineInstances(this.pipeLine.PipeLineID).subscribe(res => {
                this.pipelineInstances = _.sortBy(res, 'Started').reverse().slice(0, 3);
                this.dataService.pipelineInstanceDataRefreshed.emit(this.pipelineInstances);
            });
        }
    }
    showInstanceDetails(instance) {
        instance.Title = 'Pipeline Instance';
        instance.Pipeline = this.pipeLine;
        instance.TaskIdNameMap = this.TaskIdNameMap;
        this.dataservice.showHideMask.emit(true);
        this.dialogService.addDialog(PipelineInstanceComponent, instance).subscribe(response => {
            this.dataservice.showHideMask.emit(false);
        });
    }
    toggelSchedular($event) {
        if ($event.currentTarget.id === 'schedulerpanel') {
            this.showSchedular = !this.showSchedular;
            if (this.showSchedular) {
                this.showInstances = false;
            }
        } else {
            this.showInstances = !this.showInstances;
            if (this.showInstances) {
                this.showSchedular = false;
            }
        }
        // this.instancePanelCollapsed = !this.instancePanelCollapsed;

    }
    deleteTask(index: number, taskID: number, taskName: string) {
        const disposable = this.dialogService.addDialog(ConfirmComponent, {
            title: 'Delete Task',
            message: this.message === undefined ? 'Are you sure you want to delete task - ' + taskName + '?' : this.message
        })
            .subscribe((isConfirmed) => {
                // We get dialog result
                this.dataservice.showHideMask.emit(false);
                if (isConfirmed) {
                    this.pipelineService.deletePipelineTask(taskID).subscribe(() => {
                        this.tasks.splice(index, 1);
                        // update sequences of everything after
                        if (index < this.tasks.length) {
                            for (const task of this.tasks.slice(index)) {
                                task.SequncePosition--;
                            }
                        }
                        this.selectedTask = {};
                        this.selectedIndex = undefined;
                        this.selectedAction = undefined;
                        this.showControl = {};
                    }, (error) => {
                        if (error.status === 409) {
                            this.dataservice.showHideMask.emit(true);
                            this.dialogService.addDialog(WarningComponent, {
                                title: 'Warning',
                                message:
                                    'Error occurred while deleting the task, please make sure this task is not referenced by another task'
                            }).subscribe(() => {
                                this.dataservice.showHideMask.emit(false);
                            });
                        }
                    });
                }
            });
    }
    editTask(index: number, taskID: number) {
        this.dataservice.showHideMask.emit(true);
        // if adding a new task, need to increment index
        if (taskID === undefined) {
            index++;
        }

        const disposable = this.dialogService.addDialog(NewTaskComponent, {
            title: 'Configure Task',
            message: undefined,
            pipelineID: parseInt(this.pipeLine.PipeLineID, undefined), // TODO - remove hardcoding here
            sequence: index,
            pipelineTaskID: taskID,
            projectID: parseInt(this.project.ProjectID, undefined)
        })
            .subscribe((task) => {
                // We get dialog result
                if (task) {
                    // reload tasks
                    this.pipelineService.getPipelineTasks(this.pipeLine.PipeLineID).subscribe(res => {
                        this.tasks = _.sortBy(res, 'SequencePosition');
                        this.TaskIdNameMap = {};
                        for (let i = 0; i < this.tasks.length; i++) {
                            this.TaskIdNameMap[this.tasks[i]['PipelineTaskID']] = this.tasks[i]['Name'];
                        }
                        this.startTask = _.find(this.tasks, function (param) {
                            return param.Name === 'System Start';
                        });
                        this.endTask = _.find(this.tasks, function (param) {
                            return param.Name === 'System End';
                        });
                        this.tasks = _.filter(this.tasks, function (param) {
                            return param.Name !== 'System Start' && param.Name !== 'System End'
                        });

                    });
                }
                this.dataservice.showHideMask.emit(false);
            });
    }
    moveTask(taskID: number, index: number, count: number) {
        this.pipelineService.movePipelineTask(taskID, count).subscribe(() => {
            const oldIndex = index, newIndex = index + count;
            if (newIndex >= this.tasks.length) {
                let k = newIndex - this.tasks.length;
                while ((k--) + 1) {
                    this.tasks.push(undefined);
                }
            }
            this.tasks.splice(newIndex, 0, this.tasks.splice(oldIndex, 1)[0]);
            // update the actual sequence, probably don't need to do this, but just in case
            if (count < 0) {
                for (const task of this.tasks.slice(index + 1, index + count)) {
                    task.SequncePosition++;
                }
                this.selectedIndex -= 1;
            } else {
                for (const task of this.tasks.slice(index - count, index - 1)) {
                    task.SequncePosition--;
                }
                this.selectedIndex += 1;

            }
        });
    }
    addChildPipeLine() {
        this.dataservice.showHideMask.emit(true);
        this.dialogService.addDialog(AddNewPipelineComponent, {
            title: 'New Child Pipeline',
            message: undefined,
            projectId: this.project.ProjectID + '',
            parentId: this.pipeLine.PipeLineID + ''
        })
            .subscribe((isConfirmed) => {
                if (isConfirmed) {
                    const context = {
                        ProjectID: this.project.ProjectID,
                        ParentID: this.pipeLine.ParentID
                    };
                    this.pipelineService.getPipelineByName(this.pipeLine.Name, context).subscribe(res => {
                        const pipeObj = JSON.parse(res._body);
                        this.updateProjectPipeLine(pipeObj, this.project);
                        this.pipeLine = pipeObj;
                    })
                }
                this.dataservice.showHideMask.emit(false);
            });

    }

    imagePath(moduleId) {
        const obj = _.findWhere(this.allModules, { 'ModuleID': moduleId })
        return obj !== undefined ? '../../../../../assets/img/' + this.taskTypeIDImageMap[obj['ModuleGroupID']] : '';
    }
    onRightClick(task, $event, index) {

        $event.preventDefault();
        const referer = document.getElementById('refer');
        const parent = document.getElementById('tabsContainer');
        const tempX = $event.pageX - parent.offsetLeft;
        const tempY = $event.pageY - parent.offsetTop - 41;
        this.showContextMenu = true;
        this.x = tempX + 'px';
        let contextMenuHeight = 0;
        if (!(task === 'start' || task === 'end') && this.tasks.length > 1) {
            contextMenuHeight = 157.73; // Full context menu
        } else if (!(task === 'start' || task === 'end') && this.tasks.length === 1) {
            contextMenuHeight = 118.3; // height of context menu without move option
        }
        if (task === 'start' || task === 'end') {
            contextMenuHeight = 78.23; // Hieght of context menu with two options
        }

        /*this.y = (referer.offsetTop - tempY< contextMenuHeight ?
            tempY - (contextMenuHeight - 50) : tempY) + 'px';
            // ($event.pageY > 1600 ? $event.layerY - (this.tasks.length > 1 ? 160 : 120) : $event.layerY) + 'px';
        */
        if (referer.offsetTop - $event.pageY < contextMenuHeight) {
            this.y = (tempY - contextMenuHeight) + 'px';

        } else {
            this.y = tempY + 'px';
        }
        this.selectedTask = task;
        this.selectedIndex = index;
        if ((task === 'start' && _.isEmpty(this.startTask)) ||
            (task === 'end' && _.isEmpty(this.endTask))) {
            this.showContextMenu = false;
        }
        if (task === 'start' || task === 'end') {
            this.enableParameters = true;
            this.enableEdit = false;
            this.enableMove = false;
            this.enableDelete = false;
            if (task === 'start') {
                this.enableAddRight = true;
                this.enableAddLeft = false;
                // this.selectedIndex = 0;
            } else {
                this.enableAddRight = false;
                this.enableAddLeft = true;
            }
        } else {
            if (index === 0) {
                this.enableLeft = false;
                this.enableRight = true;
            } else if (index === this.tasks.length - 1) {
                this.enableRight = false;
                this.enableLeft = true;
            } else {
                this.enableLeft = true;
                this.enableRight = true;
            }
            this.enableEdit = true;
            this.enableMove = true;
            this.enableDelete = true;
            this.enableParameters = false;
            this.enableAddLeft = true;
            this.enableAddRight = true;
        }
    }
    onContextMenuSelection(action: string) {

        this.closeContext();
        this.selectedAction = action;
        switch (action) {
            case 'edit':
                this.editTask(this.selectedIndex + 1, this.selectedTask.PipelineTaskID)
                break;
            case 'move':
            case 'add':
                break;
            case 'delete':
                this.deleteTask(this.selectedIndex, this.selectedTask.PipelineTaskID, this.selectedTask.Name)
                break;
            case 'parameters':
                this.manageParameters(this.selectedTask)
                break;
        }
    }
    arrowMovement(movementDirection, action) {
        this.selectedAction = action;
        switch (this.selectedAction) {
            case 'add':
                const increment = movementDirection === 'prev' ? 0 : 1;
                let index = this.selectedIndex;
                if (index === undefined) {
                    index = movementDirection === 'prev' ? this.tasks.length : 0;
                } else {
                    index = index + increment;
                }
                this.editTask(index, undefined);
                break;
            case 'move':
                const thirdParameter = movementDirection === 'prev' ? -1 : 1;
                this.moveTask(this.selectedTask.PipelineTaskID, this.selectedIndex, thirdParameter);
                break;
            default:
                const firstParameter = movementDirection === 'prev' ? this.tasks.length : 0;
                this.editTask(firstParameter, undefined);
                break;
        }
        this.closeContext();
    }
    closeContext() {
        this.showContextMenu = false;
        this.enableSubMenu = false;
        this.enableSubMenu2 = false;

    }
    updateProjectPipeLine(pipeline, project) {
        for (let i = 0; i < project.pipeLines.length; i++) {
            if (project.pipeLines[i].PipeLineID === pipeline.PipeLineID) {
                project.pipeLines[i] = pipeline;
                break;
            } else {
                project.pipeLines[i] = this.updatePipeLine(project.pipeLines[i], pipeline)
            }
        }
    }
    updatePipeLine(dest, value) {
        if (dest.PipeLineID === value.PipeLineID) {
            return value;
        } else {
            for (let i = 0; i < dest.Pipelines.length; i++) {
                dest.Pipelines[i] = this.updatePipeLine(dest.Pipelines[i], value)
            }
        }
        return dest;
    }


    getModuleName(moduleId) {
        const obj = _.findWhere(this.allModules, { 'ModuleID': moduleId })
        return obj !== undefined ? obj['Name'] : '';
    }

    confirmRunPipeline() {
        this.dataservice.setPipelineData(this.pipeLine);
        this.dataservice.showHideMask.emit(true);
        this.dialogService.addDialog(RunPipelineComponent, {
            title: 'Run Pipeline',
            message: this.pipeLine.Name,
            projectId: this.project.ProjectID + '',
            parentId: this.pipeLine.PipeLineID + ''
        }).subscribe(response => {
            this.dataservice.showHideMask.emit(false);
            if (response) {
                // refresh instances
                this.pipelineService.getPipelineInstances(this.pipeLine.PipeLineID).subscribe(res => {
                    this.pipelineInstances = _.sortBy(res, 'Started').reverse().slice(0, 3);
                });
            }
        });

    }
    trackPipeline(index, pipeline) {
        return pipeline ? pipeline.PipeLineID : undefined;
    }
    manageParameters(taskName: string) {
        const task = taskName === 'start' ? this.startTask : this.endTask;

        this.dataservice.showHideMask.emit(true);
        const disposable = this.dialogService.addDialog(ManageParametersComponent, {
            title: 'Manage Parameters',
            message: undefined,
            pipelineID: parseInt(this.pipeLine.PipeLineID, undefined),
            pipelineTaskID: parseInt(task['PipelineTaskID'], undefined),
            sequence: undefined,
            taskName: taskName,
            parameterName: undefined,
            projectID: parseInt(this.project.ProjectID, undefined)
        })
            .subscribe((task2) => {
                // We get dialog result
                if (task2) {

                }
                this.dataservice.showHideMask.emit(false);
            });
    }
    togglePipelineSettings() {
        // 1-line if statement that toggles the value:
        this.menuState = this.menuState === 'out' ? 'in' : 'out';

    }

    DeletePipeline() {

        this.dataservice.setPipelineData(this.pipeLine);
        this.dataservice.showHideMask.emit(true);
        this.dialogService.addDialog(PipelineSettingsComponent, {
            title: 'Delete Pipeline',
            message: this.pipeLine.Name
        }).subscribe(response => {
            this.dataservice.showHideMask.emit(false);
            if (response) {
                this.dataService.pipelineDeleted.emit(this.pipeLine['PipeLineID']);
            }
        });
        this.togglePipelineSettings();
    }

    updateScheduleInPipeline(schedule) {
        this.pipeLine.Schedule = schedule;
    }
    renamePipeline() {

        this.dataservice.setPipelineData(this.pipeLine);
        this.dataservice.showHideMask.emit(true);
        this.dialogService.addDialog(RenamePipelineComponent, {
            title: 'Rename Pipeline',
            message: this.pipeLine.Name,
            projectId: this.project.ProjectID + '',
            parentId: this.pipeLine.PipeLineID + ''
        }).subscribe(response => {

            this.dataservice.showHideMask.emit(false);
            if (response) {
                this.pipelineService.getPipeline(this.pipeLine.PipeLineID).subscribe(res => {
                    this.pipeLine = res;
                });
            }
        });
        this.togglePipelineSettings();
    }

    ShowpipelineNotificationSettings() {
        this.dataservice.setPipelineData(this.pipeLine);
        this.dataservice.showHideMask.emit(true);
        this.dialogService.addDialog(PipelineSettingsComponent, {
            title: 'Edit Notification Settings',
            message: this.pipeLine.Name
        }).subscribe(response => {
            this.dataservice.showHideMask.emit(false);
        });
        this.togglePipelineSettings();
    }
}

