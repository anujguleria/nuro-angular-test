import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { PipelineInstanceModel } from '../../project-view-pipeline/project-view-pipeline.component';
import { DataService } from '../../../../Services/data.service';
import { PipelineService } from '../../../../Services/pipeline.service';
import * as _ from 'underscore';
import { SignalR, ISignalRConnection, BroadcastEventListener } from 'ng2-signalr';
declare var $: any;
@Component({
  selector: 'app-pipeline-instance',
  templateUrl: './pipeline-instance.component.html',
  styleUrls: ['./pipeline-instance.component.css']
})
export class PipelineInstanceComponent extends DialogComponent<PipelineInstanceModel, boolean> implements OnInit, PipelineInstanceModel {
  Title: string;
  PipelineTaskInstances: any[];
  TaskIdNameMap: any;
  PipelineInstanceId: number;
  PipelineId: number;
  Pipeline: any = {};
  Started: string;
  Ended: string;
  Status: string;
  Message: string;
  noOfCharsInSmallMessage: number;
  signalRConnection: ISignalRConnection;
  constructor(dialogService: DialogService, private dataservice: DataService
    , private _signalR: SignalR, private pipelineService: PipelineService) {
    super(dialogService);
    this.dataservice = dataservice;
    this.dataservice.pipelineInstanceDataRefreshed.subscribe(res => {
      const instance = _.findWhere(res, { 'PipelineInstanceId': this.PipelineInstanceId });
      this.updateInstance(instance);
    });
  }
  updateInstance(instance) {
    if (instance) {
      this.updateInstanceWithID();
    }
  }
  updateInstanceWithID() {
    this.pipelineService.getPipelineInstanceWithLogs(this.PipelineInstanceId).subscribe(res => {
      this.Started = res['Started'];
      this.Ended = res['Ended'];
      this.Status = res['Status'];
      this.Message = res['Message'];
      const tempArr = res['PipelineTaskInstances'];
      for (let i = 0; i < tempArr.length; i++) {
        const taskInstance = _.findWhere(this.PipelineTaskInstances, { 'PipelineInstanceId': tempArr[i]['PipelineInstanceId'] })
        if (taskInstance) {
          tempArr[i]['isExpanded'] = taskInstance['isExpanded'];
          tempArr[i]['showErorrs'] = taskInstance['showErorrs'];
          tempArr[i]['showWarnings'] = taskInstance['showWarnings'];
        }
      }
      this.PipelineTaskInstances = res['PipelineTaskInstances'];
    });
  }
  ngOnInit() {
    this.dataservice.newNotifications.subscribe(res => {
      if (res['type'] === 'PipelineInstance' && res['instanceId'] === this.PipelineInstanceId) {
        this.pipelineService.getPipelineInstance(res['instanceId']).subscribe(response => {
          this.updateInstance(response);
        });
      }
    });
    for (let i = 0; i < this.PipelineTaskInstances.length; i++) {
      this.PipelineTaskInstances[i].showErorrs = false;
      this.PipelineTaskInstances[i].showWarnings = false;
    }
    this.PipelineTaskInstances = _.sortBy(this.PipelineTaskInstances, function (item) {
      return Date.parse(item.Started);
    });
    this.noOfCharsInSmallMessage = 72;
    this.updateInstanceWithID();
  }
  confirm() {
    this.result = true;
    this.close();
  }

  formatDate(date: any) {
    return this.dataservice.formatDate(date);
  }
  showHideLogs(instance) {
    instance.isExpanded = !instance.isExpanded;
    for (let i = 0; i < this.PipelineTaskInstances.length; i++) {
      if (this.PipelineTaskInstances[i].PipelineInstanceId !== instance.PipelineInstanceId) {
        this.PipelineTaskInstances[i].isExpanded = false;
      }
    }
  }

}
