import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipelineInstanceComponent } from './pipeline-instance.component';

describe('PipelineInstanceComponent', () => {
  let component: PipelineInstanceComponent;
  let fixture: ComponentFixture<PipelineInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipelineInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipelineInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
