import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerPanelComponent } from './scheduler-panel.component';

describe('SchedulerPanelComponent', () => {
  let component: SchedulerPanelComponent;
  let fixture: ComponentFixture<SchedulerPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulerPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
