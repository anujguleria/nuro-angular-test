import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { environment } from '../../../../../environments/environment';
// import * as momentz from 'moment-timezone';
import * as moment from 'moment';
import { ScheduleModel } from '../../../../interfaces/ScheduleModel';
import { ApiService } from '../../../../Services/api.service';
import { AngularKalendarModule } from 'angular-kalendar';
import { CalendarModule } from 'primeng/primeng';
import { ConfirmComponent } from '../../../../shared/confirm-dialog.component';
import { ConfirmModel } from '../../../../interfaces/ConfirmModel';
import { DataService } from '../../../../Services/data.service';
import * as _ from 'underscore';

let numInstances = 0;

@Component({
    selector: 'app-scheduler-panel',
    templateUrl: './scheduler-panel.component.html',
    styleUrls: ['./scheduler-panel.component.css']
})
export class SchedulerPanelComponent implements OnInit, ConfirmModel {
    @Input() showSchedular;
    @Input() pipelineID;
    @Input() pipelineName;
    @Output() toggleScheduler = new EventEmitter<object>();
    @Output() ScheduleUpdated = new EventEmitter<any>();
    instanceId: number;
    ScheduleObject: ScheduleModel = new ScheduleModel();
    abbrAndOffset = [];
    zone: any;
    firstCall = true;
    parameter = false;
    MonthOfYearDropFirst = false;
    MonthOfYearDropSecond = false;
    MonthOfYearSelected: any = undefined;
    recurr = false;
    monthDate = false;
    dateTime: any;
    startDate = new Date();
    minDate = new Date();
    timeZoneObjectSelected: any;
    defaultTimeZoneID: string;
    DayOfWeekOffsetDrop = false;
    DayOfWeekOffsetSelected: any = undefined;
    DayOfWeekOffset = [
        { 'name': 'First', 'id': 1 },
        { 'name': 'Second', 'id': 2 },
        { 'name': 'Third', 'id': 3 },
        { 'name': 'Fourth', 'id': 4 },
        { 'name': 'Last', 'id': 5 }
    ];

    DayOfWeek = [
        { 'name': 'Sun', 'id': 1 },
        { 'name': 'Mon', 'id': 2 },
        { 'name': 'Tue', 'id': 3 },
        { 'name': 'Wed', 'id': 4 },
        { 'name': 'Thu', 'id': 5 },
        { 'name': 'Fri', 'id': 6 },
        { 'name': 'Sat', 'id': 7 }
    ];
    DayOfWeekDrop = false;
    DayOfWeekSelected: any = undefined;
    MonthOfYear = [
        { 'name': 'Jan', 'id': 1 },
        { 'name': 'Feb', 'id': 2 },
        { 'name': 'Mar', 'id': 3 },
        { 'name': 'Apr', 'id': 4 },
        { 'name': 'May', 'id': 5 },
        { 'name': 'Jun', 'id': 6 },
        { 'name': 'Jul', 'id': 7 },
        { 'name': 'Aug', 'id': 8 },
        { 'name': 'Sep', 'id': 9 },
        { 'name': 'Oct', 'id': 10 },
        { 'name': 'Nov', 'id': 11 },
        { 'name': 'Dec', 'id': 12 }
    ];
    showError = false;

    MonthlyYearlyModels = {
        '3': {
            '1': {
                'DayOfMonth': ''
            },
            '2': {
                'DayOfWeekOffsetSelected': {},
                'DayOfWeekSelected': {}
            }

        },
        '4': {
            '1': {
                'MonthOfYearSelected': {},
                'DayOfMonth': ''
            },
            '2': {
                'DayOfWeekOffsetSelected': {},
                'DayOfWeekSelected': {},
                'MonthOfYearSelected': {}
            }
        }
    }
    notEnoughSpaceToRender = false;

    // Not used now
    unpackeddata = [];

    // Confirm model props
    title: string;
    message: string;
    projectId: string;
    parentId: string;
    calendarOpen = false;
    @ViewChild('owl') CalendarMod;
    constructor(private apiService: ApiService, private dialogService: DialogService, private dataservice: DataService) {
        // this.processpackedData();
        // this.abbrAndOffset = environment.offsetWithAbbr;
        // this.zone = Object.keys(this.abbrAndOffset);
        // this.currentTimeZone();
        this.instanceId = numInstances++;

    }
    recurrr() {
        this.showError = false;
        this.recurr = !(this.ScheduleObject.RecurEvery >= 0 && this.ScheduleObject.RecurEvery <= 99);

    }
    dateCheck() {
        this.showError = false;
        this.monthDate = !(this.ScheduleObject.DayOfMonth >= 1 && this.ScheduleObject.DayOfMonth <= 31);

    }
    ngOnInit() {
        this.showError = false;
        this.getSchedule();
        this.minDate = new Date();
    }

    saveSchedule() {
        this.showError = false;
        if (this.ScheduleObject.Recurrence === 2 || this.ScheduleObject.Recurrence === 3 || this.ScheduleObject.Recurrence === 4) {
            this.ScheduleObject.RecurEvery = 1;
        }

        this.ScheduleObject.TimeZoneID = this.timeZoneObjectSelected['Id'];
        const d = new Date(this.startDate);
        // convert this into an object that looks like UTC

        this.ScheduleObject.StartTime = new Date(Date.UTC(this.startDate.getFullYear(), this.startDate.getMonth(), this.startDate.getDate(),
            this.startDate.getHours(), this.startDate.getMinutes(), this.startDate.getSeconds()));
        // if (this.DayOfWeekOffsetSelected !== undefined)
        //     this.ScheduleObject.DayOfWeekOffset = this.DayOfWeekOffsetSelected['id'];
        // if (this.DayOfWeekSelected !== undefined)
        //     this.ScheduleObject.DayOfMonth = this.DayOfWeekSelected['id'];
        // if (this.MonthOfYearSelected !== undefined)
        //     this.ScheduleObject.MonthOfYear = this.MonthOfYearSelected['id'];
        this.setMonthlyYearlySchedules();

        this.PostSchedule(true);

    }

    InitScheduleModel() {
        this.showError = false;
        this.ScheduleObject.DailyRecurrenceType = undefined;
        this.ScheduleObject.RecurEvery = 1;
        this.ScheduleObject.OnMonday = false;
        this.ScheduleObject.OnTuesday = false;
        this.ScheduleObject.OnWednesday = false;
        this.ScheduleObject.OnThursday = false;
        this.ScheduleObject.OnFriday = false;
        this.ScheduleObject.OnSaturday = false;
        this.ScheduleObject.OnSunday = false;
        this.ScheduleObject.MonthlyRecurrenceType = undefined;
        this.ScheduleObject.DayOfMonth = moment().date();
        this.ScheduleObject.YearlyRecurrenceType = undefined;
        this.ScheduleObject.MonthOfYear = undefined;
        this.ScheduleObject.DayOfWeekOffset = undefined;

        this.resetDropDownAndSelections();

    }
    toggelSchedular($event) {
        if ($event.target.id === 'schedulerpanel' || $event.target.id === 'scheduler-title' || $event.target.id === 'errorMessage') {
            this.showSchedular = !this.showSchedular;
            this.toggleScheduler.next($event);
        }
    }

    getSchedule() {

        let defaultTimeZone;
        const userTimeZone = (Intl.DateTimeFormat().resolvedOptions().timeZone);
        this.apiService.get(environment.apiUrl + 'api/Pipelines/Schedules/GetTimezones').subscribe((response) => {
            this.abbrAndOffset = response;

            this.apiService.post(environment.apiUrl + 'api/Schedules/Timezone', '"' + userTimeZone + '"').subscribe((response2) => {
                const TMZinfo = response2._body;
                if (TMZinfo.length > 0 && TMZinfo.substr(0, 1) === '"') {
                    defaultTimeZone = TMZinfo.substr(1, TMZinfo.length - 2);
                    const timeZone = _.filter(this.abbrAndOffset, function (param) {
                        return param.Id === defaultTimeZone;
                    });
                    if (timeZone !== undefined && timeZone[0] !== undefined) {
                        this.timeZoneObjectSelected = timeZone[0];
                    }
                }


                this.apiService.get(environment.apiUrl + 'api/Pipelines/' + this.pipelineID + '/Schedule').subscribe((schedule) => {

                    if (schedule !== null) {
                        this.ScheduleObject = schedule;
                        this.startDate = new Date(this.ScheduleObject.StartTime);
                        // this.currentTimeZone('Id',this.ScheduleObject.TimeZoneID);
                        if (this.ScheduleObject.TimeZoneID === undefined || this.ScheduleObject.TimeZoneID === '') {
                            this.ScheduleObject.TimeZoneID = defaultTimeZone;
                        }
                        const timeZoneID = this.ScheduleObject.TimeZoneID;
                        const timeZone = _.filter(this.abbrAndOffset, function (param) {
                            return param.Id === timeZoneID;
                        });
                        if (timeZone !== undefined && timeZone[0] !== undefined) {
                            this.timeZoneObjectSelected = timeZone[0];
                        }
                        if (this.ScheduleObject.Recurrence === undefined || this.ScheduleObject.Recurrence === '') {
                            this.ScheduleObject.Recurrence = '0';
                        } else {
                            this.ScheduleObject.Recurrence = this.ScheduleObject.Recurrence.toString();
                        }
                        if (this.ScheduleObject.MonthlyRecurrenceType !== undefined) {
                            this.ScheduleObject.MonthlyRecurrenceType = this.ScheduleObject.MonthlyRecurrenceType.toString();
                        }
                        if (this.ScheduleObject.YearlyRecurrenceType !== undefined) {
                            this.ScheduleObject.YearlyRecurrenceType = this.ScheduleObject.YearlyRecurrenceType.toString();
                        }
                        if (this.ScheduleObject.DailyRecurrenceType !== undefined) {
                            this.ScheduleObject.DailyRecurrenceType = this.ScheduleObject.DailyRecurrenceType.toString();
                        }

                        this.setMonthlyYearlyModels();

                    } else {
                        this.ScheduleObject = new ScheduleModel();
                        this.ScheduleObject.Recurrence = '0';
                        this.ScheduleObject.TimeZoneID = defaultTimeZone;

                    }
                });
            });
        });
    }
    currentTimeZone(key: string, value: string) {
        for (let i = 0; i < this.abbrAndOffset.length; i++) {
            if (this.abbrAndOffset[i][key] === value) {
                this.timeZoneObjectSelected = this.abbrAndOffset[i];
            }
        }
    }

    setSelectedObject(dataObject: any[], id: any) {
        for (let i = 0; i < dataObject.length; i++) {
            if (dataObject[i]['id'] === id) {
                return dataObject[i];
            }
        }

    }
    pad(value) {
        return value < 10 ? '0' + value : value;
    }
    createOffset(off) {
        const sign = (off > 0) ? '-' : '+';
        const offset = Math.abs(off);
        const hours = this.pad(Math.floor(offset / 60));
        const minutes = this.pad(offset % 60);
        return sign + hours + ':' + this.pad(Math.round(minutes));
    }


    calculateUTCDiff() {
        const date = new Date();
        return date.toString().split('(')[1].split(')')[0];
    }

    PostSchedule(showUpdatedDialog: boolean) {
        if (this.ScheduleObject.StartTime === undefined) {
            this.showError = true;
            this.ScheduleObject['Enabled'] = false;
        } else {
            this.apiService.post(environment.apiUrl + 'api/Pipelines/' + this.pipelineID + '/Schedule',
                this.ScheduleObject).subscribe((response) => {
                    if (showUpdatedDialog) {
                        console.log(response);
                        this.dataservice.showHideMask.emit(true);
                        const disposable = this.dialogService.addDialog(ConfirmComponent, {
                            title: 'Scheduler Notification',
                            message: this.message === undefined ? 'Schedule has been updated for pipeline - ' +
                                this.pipelineName : this.message,
                            includeCancel: false
                        }).subscribe((task) => {
                            // We get dialog result
                            this.dataservice.showHideMask.emit(false);
                        });
                    }
                    if (response.status === 200) {
                        this.ScheduleUpdated.emit(this.ScheduleObject);
                    }
                });
        }
    }

    resetDropDownAndSelections() {
        this.DayOfWeekOffsetDrop = false;
        // this.DayOfWeekOffsetSelected = undefined;
        this.DayOfWeekDrop = false;
        // this.DayOfWeekSelected = undefined;
        this.MonthOfYearDropFirst = false;
        this.MonthOfYearDropSecond = false;
        // this.MonthOfYearSelected = undefined;
        this.MonthlyYearlyModels[3][1]['DayOfMonth'] = undefined;
        this.MonthlyYearlyModels[3][2]['DayOfWeekOffsetSelected'] = undefined;
        this.MonthlyYearlyModels[3][2]['DayOfWeekSelected'] = undefined;

        this.MonthlyYearlyModels[4][1]['DayOfMonth'] = undefined;
        this.MonthlyYearlyModels[4][1]['MonthOfYearSelected'] = undefined;
        this.MonthlyYearlyModels[4][2]['DayOfWeekOffsetSelected'] = undefined;
        this.MonthlyYearlyModels[4][2]['DayOfWeekSelected'] = undefined;
        this.MonthlyYearlyModels[4][2]['MonthOfYearSelected'] = undefined;

    }

    setMonthlyYearlySchedules() {
        if (this.ScheduleObject.Recurrence === 3) {
            if (this.ScheduleObject.MonthlyRecurrenceType === 1) {
                this.ScheduleObject.DayOfMonth = this.MonthlyYearlyModels[3][1]['DayOfMonth'];
            } else {
                if (this.MonthlyYearlyModels[3][2]['DayOfWeekOffsetSelected'] !== undefined) {
                    this.ScheduleObject.DayOfWeekOffset = this.MonthlyYearlyModels[3][2]['DayOfWeekOffsetSelected']['id'];
                } if (this.MonthlyYearlyModels[3][2]['DayOfWeekSelected'] !== undefined) {
                    this.ScheduleObject.DayOfMonth = this.MonthlyYearlyModels[3][2]['DayOfWeekSelected']['id'];
                }
            }
        } else if (this.ScheduleObject.Recurrence === 4) {
            if (this.ScheduleObject.YearlyRecurrenceType === 1) {
                this.ScheduleObject.DayOfMonth = this.MonthlyYearlyModels[4][1]['DayOfMonth'];
                if (this.MonthlyYearlyModels[4][1]['MonthOfYearSelected'] !== undefined) {
                    this.ScheduleObject.MonthOfYear = this.MonthlyYearlyModels[4][1]['MonthOfYearSelected']['id'];
                }
            } else {
                if (this.MonthlyYearlyModels[4][2]['DayOfWeekOffsetSelected'] !== undefined) {
                    this.ScheduleObject.DayOfWeekOffset = this.MonthlyYearlyModels[4][2]['DayOfWeekOffsetSelected']['id'];
                }
                if (this.MonthlyYearlyModels[4][2]['DayOfWeekSelected'] !== undefined) {
                    this.ScheduleObject.DayOfMonth = this.MonthlyYearlyModels[4][2]['DayOfWeekSelected']['id'];
                }
                if (this.MonthlyYearlyModels[4][2]['MonthOfYearSelected'] !== undefined) {
                    this.ScheduleObject.MonthOfYear = this.MonthlyYearlyModels[4][2]['MonthOfYearSelected']['id'];
                }

            }
        }
    }

    setMonthlyYearlyModels() {
        if (this.ScheduleObject.Recurrence === 3) {
            if (this.ScheduleObject.MonthlyRecurrenceType === 1) {
                this.MonthlyYearlyModels[3][1]['DayOfMonth'] = this.ScheduleObject.DayOfMonth;
            } else {
                if (this.ScheduleObject.DayOfWeekOffset !== undefined && this.ScheduleObject.DayOfWeekOffset !== 0) {
                    this.MonthlyYearlyModels[3][2]['DayOfWeekOffsetSelected'] =
                        this.setSelectedObject(this.DayOfWeekOffset, this.ScheduleObject.DayOfWeekOffset);
                }
                if (this.ScheduleObject.DayOfMonth !== undefined && this.ScheduleObject.DayOfMonth !== 0) {
                    this.MonthlyYearlyModels[3][2]['DayOfWeekSelected'] =
                        this.setSelectedObject(this.DayOfWeek, this.ScheduleObject.DayOfMonth);
                }
            }
        } else if (this.ScheduleObject.Recurrence === 4) {
            if (this.ScheduleObject.YearlyRecurrenceType === 1) {
                this.MonthlyYearlyModels[4][1]['DayOfMonth'] = this.ScheduleObject.DayOfMonth;
                if (this.ScheduleObject.MonthOfYear !== undefined && this.ScheduleObject.MonthOfYear !== 0) {
                    this.MonthlyYearlyModels[4][1]['MonthOfYearSelected'] =
                        this.setSelectedObject(this.MonthOfYear, this.ScheduleObject.MonthOfYear);
                }
            } else {
                if (this.ScheduleObject.DayOfWeekOffset !== undefined && this.ScheduleObject.DayOfWeekOffset !== 0) {
                    this.MonthlyYearlyModels[4][2]['DayOfWeekOffsetSelected'] =
                        this.setSelectedObject(this.DayOfWeekOffset, this.ScheduleObject.DayOfWeekOffset);
                }
                if (this.ScheduleObject.DayOfMonth !== undefined && this.ScheduleObject.DayOfMonth !== 0) {
                    this.MonthlyYearlyModels[4][2]['DayOfWeekSelected'] =
                        this.setSelectedObject(this.DayOfWeek, this.ScheduleObject.DayOfMonth);
                }
                if (this.ScheduleObject.MonthOfYear !== undefined && this.ScheduleObject.MonthOfYear !== 0) {
                    this.MonthlyYearlyModels[4][2]['MonthOfYearSelected'] =
                        this.setSelectedObject(this.MonthOfYear, this.ScheduleObject.MonthOfYear);
                }
            }
        }
    }
    checkBottomofPage($event, heightOfelement: number) {
        if ($event.currentTarget.nextElementSibling.clientHeight === 0) {
            const referer = document.getElementById('refer');
            if (referer.offsetTop - $event.pageY < heightOfelement + 50) {
                this.notEnoughSpaceToRender = true;
            } else {
                this.notEnoughSpaceToRender = false;
            }
        }
    }

    formatDateToView() {
        if (this.startDate) {
            return this.startDate.toLocaleDateString() + ' ' +
                (this.startDate.getHours() < 10 ? '0' + this.startDate.getHours() : this.startDate.getHours())
                + ':' +
                (this.startDate.getMinutes() < 10 ? '0' + this.startDate.getMinutes() : this.startDate.getMinutes());

        }
    }
    ToggleCalendar(event) {
        if (this.calendarOpen) {
            document.getElementById('dateText').click();
            this.calendarOpen = false;
            event.stopPropagation();
        } else {
            this.calendarOpen = true;
            const referer = document.getElementById('refer');
            if (referer.offsetTop - event.pageY < 285) {
                this.CalendarMod['dialogElm']['nativeElement']['classList'].add('force-Calendar-up');
            }
        }
    }
}
