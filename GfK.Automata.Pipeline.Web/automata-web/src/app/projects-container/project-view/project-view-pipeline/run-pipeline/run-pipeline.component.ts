﻿import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { ConfirmModel } from '../../../../interfaces/ConfirmModel';
import { PipelineService } from '../../../../Services/pipeline.service';
import { DataService } from '../../../../Services/data.service';
@Component({
  selector: 'app-run-pipeline',
  templateUrl: './run-pipeline.component.html',
  styleUrls: ['./run-pipeline.component.css']
})
export class RunPipelineComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
  title: string;
  message: string;
  projectId: string;
  parentId: string;
  validationError: string;
  errorEnabled: boolean;
  isErorrOpened: boolean;
  RunType = '0';
  pipelineData: any;
  constructor(dialogService: DialogService, private pipelineService: PipelineService, private dataService: DataService) {
    super(dialogService);
  }


  ngOnInit() {
    this.pipelineData = this.dataService.getPipelineData();
    if (!this.pipelineData['ParentID']) {
      this.RunType = 'NONE';
    } else {
      this.RunType = 'PARENT';
    }
  }
  showError(message) {

  }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    const context = {
      ProjectID: this.projectId,
      ParentID: this.parentId,
      RunPipelineContext: this.RunType
    };
    this.pipelineService.runPipeline(this.parentId, context).subscribe(res => {
      this.dataService.showHideMask.emit(false);
      this.result = true;
      this.close();
    }, error => {

    });
  }
}
