﻿import { Component, OnInit, Input } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { TaskService } from '../../../../task/shared/task.service';
import { DataService } from '../../../../Services/data.service';
import { PipelineService } from '../../../../Services/pipeline.service';
import { WarningComponent } from '../../../../shared/warning-dialog.component';
import * as _ from 'underscore';
import { Observable } from 'rxjs/Rx';
export interface ParametersModel {
    title: string;
    message: string;
    pipelineID: number;
    sequence: number;
    pipelineTaskID: number;
    projectID: number;
    parameterName: string;
    taskName: string;
}

@Component({
    selector: 'app-manage-parameters',
    templateUrl: './manage-parameters.component.html',
    styleUrls: ['./manage-parameters.component.css']
})
export class ManageParametersComponent extends DialogComponent<ParametersModel, boolean> implements ParametersModel, OnInit {
    title: string;
    message: string;
    validationClass: string;
    validationText: string;
    failedValidation = false;
    moduleGroups: {};
    previousTaskParameters: any[] = [];
    groupKeys: any[];
    selectedModuleID: number;
    selectedModule: any;
    parameterValues: {} = {};
    parameterTaskHooks: {} = {};
    pipelineID: number;
    sequence = 0;
    pipelineTaskID = 0;
    projectID = 0;
    currentDBTask: any;
    nextSequence = 0;
    inputParameters: {} = {};
    newParameters: any[] = [];
    newParameterIndex = 0;
    existingParameters: any[] = [];
    parametersToDelete: any[] = [];

    validationError: string;
    errorEnabled = false;
    isErrorOpened = false;
    showNewParameters = false;

    modelContentHeight: string;

    taskName: string;
    parameterName: string;
    inputOutput: string;
    constructor(dialogService: DialogService,
        private taskService: TaskService,
        private dataService: DataService,
        private pipelineService: PipelineService
    ) {
        super(dialogService);
        this.parameterName = undefined;
    }

    ngOnInit() {
        this.inputOutput = this.taskName === 'start' ? 'Input' : 'Output';
        let screenHeight: number;
        if (typeof (window.innerWidth) === 'number') {
            screenHeight = window.innerHeight;
        } else {
            if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                screenHeight = document.documentElement.clientHeight;
            } else {
                if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                    screenHeight = document.body.clientHeight;
                }
            }
        }
        if (this.nextSequence = 0) {
            this.nextSequence++;
        }
        this.modelContentHeight = screenHeight * .55 + 'px';
        let taskPromise; // = undefined;
        if (this.pipelineTaskID) {
            taskPromise = this.taskService.getTask(this.pipelineTaskID);
        }
        const pipelineTasksPromise = this.pipelineService.getPipelineTasks(this.pipelineID);

        const pipelineParametersPromise = this.pipelineService.getPipelineParameters(this.pipelineID, 'INPUT');
        if (taskPromise) {
            taskPromise.subscribe(task => {

                this.selectedModuleID = task.ModuleID;
                this.taskService.getModule(this.selectedModuleID).subscribe(res => {
                    this.selectedModule = res;
                    for (const userInputValue of task.UserInputValues) {
                        this.parameterValues[userInputValue.ParameterID] = userInputValue.InputValue;
                    }
                    this.currentDBTask = task;
                    this.sequence = task.SequencePosition;
                    this.pipelineParametersSubscribe(pipelineParametersPromise, pipelineTasksPromise, task);


                    if (this.selectedModule.Parameters.length === 0) {
                        this.newParameterIndex++;
                        const newParam = { index: this.newParameterIndex, Name: '', value: '', error: false, errorOpen: false };
                        this.newParameters.push(newParam);
                        this.showNewParameters = true;
                    } else {
                        const parameters = this.selectedModule.Parameters;
                        this.existingParameters = parameters.sort(function (obj1, obj2) {
                            return obj1.SequencePosition - obj2.SequencePosition;
                        });
                        _.each(this.existingParameters, function (parm) {
                            parm['error'] = false;
                            parm['errorOpen'] = false;
                        });

                    }

                });
            });
        } else {
            this.pipelineParametersSubscribe(pipelineParametersPromise, pipelineTasksPromise);
        }

    }
    pipelineParametersSubscribe(pipelineParametersPromise, pipelineTasksPromise, task?) {
        if (this.inputOutput === 'Input') {
            pipelineParametersPromise.subscribe(tasks => {
                this.previousTaskParameters = [];
                const pipelineID = this.pipelineID;
                for (const innerTask of tasks) {
                    this.taskService.getModule(innerTask.ModuleID).subscribe(res => {
                        const thisModule = res;
                        this.pipelineService.getPipeline(innerTask.PipeLineID).subscribe(pipeline => {
                            for (const inputValue of innerTask.UserInputValues) {
                                const thisParameter = _.find(thisModule.Parameters, function (parm) {
                                    return parm['ParameterID'] === inputValue.ParameterID;
                                });
                                const option = {
                                    name: pipeline.Name + ' - ' + thisParameter['Name'],
                                    pipelineTaskID: innerTask.PipelineTaskID,
                                    parameterID: inputValue.ParameterID,
                                    id: innerTask.PipelineTaskID + '~' + inputValue.ParameterID,
                                    pipelineID: innerTask.PipeLineID
                                };

                                if (task) {
                                    for (const taskInputValue of task.PipelineTaskInputValues) {
                                        if (
                                            taskInputValue.PriorPipelineTaskID === innerTask.PipelineTaskID
                                            && taskInputValue.PriorPipeLineTaskParameterID === inputValue.ParameterID) {
                                            this.parameterTaskHooks[taskInputValue.ParameterID] = option;
                                        }
                                    }
                                }
                                this.previousTaskParameters.push(option);
                            }
                            for (const inputValue of innerTask.PipelineTaskInputValues) {
                                const thisParameter = _.find(thisModule.Parameters, function (parm) {
                                    return parm['ParameterID'] === inputValue.ParameterID;
                                });
                                const option = {
                                    name: pipeline.Name + ' - ' + thisParameter['Name'],
                                    pipelineTaskID: innerTask.PipelineTaskID,
                                    parameterID: inputValue.ParameterID,
                                    id: innerTask.PipelineTaskID + '~' + inputValue.ParameterID,
                                    pipelineID: innerTask.PipeLineID
                                };

                                if (task) {
                                    for (const taskInputValue of task.PipelineTaskInputValues) {
                                        if (
                                            taskInputValue.PriorPipelineTaskID === innerTask.PipelineTaskID
                                            && taskInputValue.PriorPipeLineTaskParameterID === inputValue.ParameterID) {
                                            this.parameterTaskHooks[taskInputValue.ParameterID] = option;
                                        }
                                    }
                                }
                                this.previousTaskParameters.push(option);
                            }
                        });
                    });
                }
                this.pipelineTaskSubscribe(pipelineTasksPromise, task);
            });
        } else {
            this.pipelineTaskSubscribe(pipelineTasksPromise, task);
        }
    }
    pipelineTaskSubscribe(pipelineTasksPromise, task?) {
        if (this.inputOutput !== 'Input') {
            pipelineTasksPromise.subscribe(res => {
                for (const innerTask of res) {
                    if ((innerTask.SequencePosition + 1) > this.nextSequence && task) {
                        this.nextSequence = task.SequencePosition + 1;
                    }
                    if (innerTask.PipelineTaskID !== this.pipelineTaskID &&
                        (innerTask.SequencePosition < this.sequence || this.sequence === undefined)) {
                        this.taskService.getModule(innerTask.ModuleID).subscribe(modules => {
                            const module = modules;
                            if (module !== undefined) {
                                for (const parameter of module.Parameters) {
                                    if (parameter.Direction.toUpperCase() !== 'IN') {

                                        const taskName = innerTask.Name.replace('System', 'Pipeline');
                                        const option = {
                                            name: taskName + ' - ' + parameter.Name,
                                            pipelineTaskID: innerTask.PipelineTaskID,
                                            parameterID: parameter.ParameterID,
                                            id: innerTask.PipelineTaskID + '~' + parameter.ParameterID,
                                            pipelineID: innerTask.PipeLineID
                                        };
                                        if (task) {
                                            for (const taskInputValue of task.PipelineTaskInputValues) {
                                                if (
                                                    taskInputValue.PriorPipelineTaskID === innerTask.PipelineTaskID
                                                    && taskInputValue.PriorPipeLineTaskParameterID === parameter.ParameterID) {
                                                    this.parameterTaskHooks[taskInputValue.ParameterID] = option;
                                                }
                                            }
                                        }
                                        this.previousTaskParameters.push(option);
                                    }
                                }
                            }
                        });


                    }
                }
            });
        }
    }

    confirm() {
        this.isErrorOpened = false;
        this.errorEnabled = false;
        // we set dialog result as true on click on confirm button,
        // then we can get dialog result from caller code
        const self = this,
            validate = this.validate;
        _.each(this.existingParameters, function (param) {
            param['error'] = false;
            validate(param, self);
        });
        if (this.showNewParameters) {
            _.each(this.newParameters, function (param) {
                param['error'] = false;
                validate(param, self);
            });
        }

        if (!this.errorEnabled) {
            const newParametersArray: any[] = [],
                updateParametersArray: any[] = [],
                deleteParametersArray: any[] = [];
            let sequence: number = this.existingParameters.length;

            // build add module request
            _.each(this.newParameters, function (param) {
                sequence++;
                const newParameter: any = {
                    Name: param.Name,
                    Description: param.Name,
                    Direction: 'INOUT',
                    SequencePosition: sequence,
                    ParameterTypeID: 4
                };
                newParametersArray.push(newParameter);
            });
            // build update module request
            _.each(this.existingParameters, function (param) {
                sequence++;
                const updateParameter: any = {
                    ParameterID: param.ParameterID,
                    Name: param.Name,
                    Description: param.Name,
                    Direction: param.Direction,
                    SequencePosition: param.SequencePosition,
                    ParameterTypeID: param.ParameterTypeID
                };
                updateParametersArray.push(updateParameter);
            });
            // build delete module request
            _.each(this.parametersToDelete, function (parameterID) {
                sequence++;
                const deleteParameter: any = {
                    ParameterID: parameterID
                };
                deleteParametersArray.push(deleteParameter);
            });

            const data: any = {
                modifications: [
                    {
                        Action: 'Add',
                        Parameters: newParametersArray
                    },
                    {
                        Action: 'Delete',
                        Parameters: deleteParametersArray
                    },
                    {
                        Action: 'Update',
                        Parameters: updateParametersArray
                    }]
            };
            this.taskService.updateModuleParameters(data, this.selectedModuleID).subscribe(res => {
                // if module update was successful, update parameter values
                // first reload module to get parameter id's for parameters that were just added
                this.taskService.getModule(this.selectedModuleID).subscribe(selectModule => {
                    this.selectedModule = selectModule;
                    const moduleParameters = this.selectedModule.Parameters,
                        newParameters = this.newParameters,
                        parameterValues = this.parameterValues,
                        parameterHooks = this.parameterTaskHooks,
                        pipelineTaskInputValues = [];

                    const newlyAdded = _(newParameters).chain().map(function (moduleItem) {
                        return _.find(moduleParameters, function (newItem) {
                            return newItem['Name'] === moduleItem['Name'];
                        });
                    }).compact().value();
                    const userInputValues = [];
                    _.each(this.existingParameters, function (item) {
                        if (parameterValues[item.ParameterID] !== undefined) {
                            userInputValues.push({ ParameterID: item.ParameterID, InputValue: parameterValues[item.ParameterID] });
                        }
                        for (const hook of _.keys(parameterHooks)) {
                            if (hook === item.ParameterID && parameterHooks[hook].name !== 'None') {
                                pipelineTaskInputValues.push({
                                    ParameterID: hook, PriorPipelineTaskID: parameterHooks[hook].id.split('~')[0],
                                    PriorPipeLineTaskParameterID: parameterHooks[hook].id.split('~')[1],
                                    PriorPipelineID: parameterHooks[hook].pipelineID
                                });
                            }
                        }
                    });
                    _.each(this.newParameters, function (item) {
                        const addedItem = _.find(newlyAdded, function (newItem) {
                            return newItem['Name'] === item['Name'];
                        });
                        const itemKey = 'new-' + item['index'];
                        if (parameterValues[itemKey] !== undefined) {
                            userInputValues.push({ ParameterID: addedItem['ParameterID'], InputValue: parameterValues[itemKey] });
                        }
                        for (const hook of _.keys(parameterHooks)) {
                            if (hook === itemKey && parameterHooks[hook].name !== 'None') {
                                pipelineTaskInputValues.push({
                                    ParameterID: addedItem['ParameterID'], PriorPipelineTaskID: parameterHooks[hook].id.split('~')[0],
                                    PriorPipeLineTaskParameterID: parameterHooks[hook].id.split('~')[1],
                                    PriorPipelineID: parameterHooks[hook].pipelineID
                                });
                            }
                        }
                    });
                    this.selectedModule.UserInputValues = userInputValues;
                    this.taskService.getTask(this.pipelineTaskID).subscribe(tasks => {
                        const task = tasks;
                        task.UserInputValues = userInputValues;
                        task.PipelineTaskInputValues = pipelineTaskInputValues;
                        // update parameter values
                        this.taskService.updateTask(task).subscribe(response => {
                            this.taskSaveSuccess(task, response);
                        }, error => {
                            // this.failValidation('Task save failed.');
                        });

                        this.close();
                    });
                });


            }, error => {

            });

        }

    }

    validate(parameter, self) {

        if (parameter.Name === undefined || parameter.Name.trim() === '') {
            self.failValidation('Please enter a Parameter Name.', parameter);

        } else {
            const regexp = new RegExp(self.dataService.num_letter_spaces_underscore_rejexExp);
            const test: boolean = regexp.test(parameter.Name);
            if (!test) {
                self.failValidation('Parameter Name only supports numbers, letters, spaces and underscores.', parameter);
                return;
            }
        }
        // check for duplicate parameter name
        _.each(self.newParameters, function (param) {
            if (param['index'] !== parameter.index && param['Name'] === parameter.Name) {
                self.failValidation('Parameter Name in use, please enter another name.', parameter);
                return;
            }
        });
        _.each(self.existingParameters, function (param) {
            if (param['ParameterID'] !== parameter.ParameterID && param['Name'] === parameter.Name) {
                self.failValidation('Parameter Name in use, please enter another name.', parameter);
                return;
            }
        });


    }
    taskSaveSuccess(task, res) {
        if (!this.pipelineTaskID) {
            task.PipelineTaskID = parseInt(res._body, undefined);
        }
        this.result = task;
        this.close();
    }
    failValidation(Message: string, parameter: any) {
        parameter['error'] = true;
        this.validationText = Message;
        this.errorEnabled = true;
    }
    collapsAll(elements, parameter) {
        elements.forEach(element => {
            if (element.isExpanded && element.isExpanded === true) {
                if (element.ParameterID !== parameter.ParameterID) {
                    element.isExpanded = false;
                }
            }
        });
        parameter.isExpanded = !parameter.isExpanded;
    }
    removeParameter(parameter) {
        // if existing parameter, mark for deletion
        if (parameter.ParameterID !== undefined) {
            // get parameters that are in use
            this.taskService.getModuleParameters(this.selectedModuleID).subscribe(res => {

                // check if parameter is in use
                const foundParameter = _.find(res, function (item) {
                    return item['ParameterID'] === parameter.ParameterID;
                });
                if (foundParameter !== undefined && foundParameter['PipelineTaskInputValues'] !== null &&
                    foundParameter['PipelineTaskInputValues'].length > 0) {
                    this.dataService.showHideMask.emit(true);
                    this.dialogService.addDialog(WarningComponent, {
                        title: 'Warning',
                        message: 'Error occurred while deleting the task, please make sure this task is not referenced by another task'
                    }).subscribe(() => {
                        this.dataService.showHideMask.emit(false);
                    });

                } else {

                    this.parametersToDelete.push(parameter.ParameterID);
                    this.existingParameters = _.filter(this.existingParameters, function (param) {
                        return param.ParameterID !== parameter.ParameterID;
                    });
                }

            });
        } else {

            // if new parameter, just remove it from the dialog
            if (parameter.index !== undefined) {
                this.newParameters = _.filter(this.newParameters, function (param) {
                    return param.index !== parameter.index;
                });
            }
        }
    }
    addParameter() {
        this.newParameterIndex++;
        const newParam = { index: this.newParameterIndex, Name: '', value: '' };
        this.newParameters.push(newParam);
        this.showNewParameters = true;
    }
    setNoneValue(id) {
        this.parameterTaskHooks[id] = { 'name': 'None' };
    }
}



