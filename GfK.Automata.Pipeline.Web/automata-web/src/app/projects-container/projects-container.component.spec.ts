import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectscontainerComponent } from './projects-container.component';

describe('ProjectscontainerComponent', () => {
  let component: ProjectscontainerComponent;
  let fixture: ComponentFixture<ProjectscontainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectscontainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectscontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
