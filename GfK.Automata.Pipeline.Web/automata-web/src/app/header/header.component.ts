﻿import { Component, OnInit } from '@angular/core';
import { DataService } from '../Services/data.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    show = false;
    isAdmin = false;
    constructor(private dataservice: DataService, private router: Router) {
        this.dataservice.ShowProjectButton.subscribe((value: boolean) => {
            this.show = value;
        });
        this.dataservice.userChange$.subscribe((value) => {
            if (value != null && value['GapRoleUsers'] != null) {
                let i = 0;
                while (!this.isAdmin && i < value['GapRoleUsers'].length) {
                    if (value['GapRoleUsers'][i]['GapRole']['GapRoleID'] === 2) {
                        this.isAdmin = true;
                    }
                    i++;
                }
            }
        });
    };
    ngOnInit() {
        this.show = false;
        this.isAdmin = false;
    }
    logout() {
        localStorage.removeItem('authToken');
        this.show = false;
        this.isAdmin = false;
        this.router.navigate(['/login']);
    }
    openAdminPanel() {
        this.dataservice.ActivateAdminPanel.emit(true);
    }
}
