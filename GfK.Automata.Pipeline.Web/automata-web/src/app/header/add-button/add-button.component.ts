﻿import { Component } from '@angular/core';
import { NewProjectComponent } from '../new-project/new-project.component';
import { ConfirmModel } from '../../interfaces/ConfirmModel';
import { DialogService } from 'ng2-bootstrap-modal';
import { DataService } from '../../Services/data.service'

@Component({
    selector: 'app-add-button',
    templateUrl: './add-button.component.html',
    styleUrls: ['./add-button.component.css']
})
export class AddButtonComponent implements ConfirmModel {
    title: string;
    message: string;
    parentId: string;
    projectId: string;

    constructor(private dialogService: DialogService, private dataservice: DataService) { }
    showConfirm() {
        this.dataservice.showHideMask.emit(true);
        const disposable = this.dialogService.addDialog(NewProjectComponent, {
            title: 'New Project',
            message: undefined,
            projectId: undefined,
            parentId: undefined
        })
            .subscribe((isConfirmed) => {
                /*We get dialog result
                if(isConfirmed) {
                }
                else {

                } */
                this.dataservice.showHideMask.emit(false);
            });
        // We can close dialog calling disposable.unsubscribe();
        // If dialog was not closed manually close it by timeout
        // setTimeout(()=>{
        //     disposable.unsubscribe();
        // },10000);
    }

}
