﻿import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { ProjectsService } from '../../Services/projects.service';
import { DataService } from '../../Services/data.service';
import { ConfirmModel } from '../../interfaces/ConfirmModel';
import { ApiService } from '../../Services/api.service';
import { environment } from 'environments/environment';
import * as _ from 'underscore';
@Component({
    selector: 'app-new-project',
    templateUrl: './new-project.component.html',
    styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
    title: string;
    message: string;
    projectId: string;
    parentId: string;
    validationClass: string;
    validationText: string;
    failedValidation = false;
    showFailedValidation = false;
    rememberMe = false;
    projectname: string;
    dropDownEnabled = false;
    copyDropDownEnabled = false;
    selectedOption = {};
    dropDown = [];
    copyDropDown = [];
    selectedCopyOption = {};

    constructor(dialogService: DialogService,
        private projectsService: ProjectsService,
        private dataService: DataService,
        private apiService: ApiService) {
        super(dialogService);
        const obj = this.dataService.getUserInfo();
        if (obj !== undefined && obj['GapGroupUsers'].length > 0) {
            for (let i = 0; i < obj['GapGroupUsers'].length; i++) {
                this.dropDown.push(obj['GapGroupUsers'][i]['GapGroup']);
            }
            this.selectedOption = this.dropDown[0];
        }
        this.projectsService.getProjectsWithPipelines().subscribe(res => {
            this.copyDropDown = res;
            this.copyDropDown = _.sortBy(this.copyDropDown, function (item) {
                return (item.Name);
            });
            const defaultNone: any = { Name: 'None' };
            this.copyDropDown.unshift(defaultNone);
            // this.selectedCopyOption = defaultNone;
        });
    }

    confirm(event: Event) {
        // we set dialog result as true on click on confirm button,
        // then we can get dialog result from caller code
        // this.projectname;
        if (this.projectname === undefined || this.projectname === '') {
            this.failValidation('Please enter a Project Name.');
            return false;
        } else {
            const regexp = new RegExp('^([a-zA-Z0-9_]+)$');
            const test: boolean = regexp.test(this.projectname);
            if (!test) {
                this.failValidation('Project Name only supports numbers, letters, and underscores.');
                return false;
            } else {
                this.projectsService.getPrjectByName(this.projectname).subscribe(res => {
                    this.failValidation('Project Name in use, please enter another name.');
                }, error => {
                    const obj = {};
                    obj['Name'] = this.projectname;
                    if (_.isEmpty(this.selectedOption)) {
                        obj['GapGroupID'] = 1;
                    } else {
                        obj['GapGroupID'] = this.selectedOption['GapGroupID'];
                    }

                    if (_.isEmpty(this.selectedCopyOption) || this.selectedCopyOption['Name'] === 'None') {

                        this.projectsService.createProject(obj).subscribe((projectID) => {
                            this.result = true;
                            this.close();
                            const eventObj = {
                                'Name': this.projectname,
                                'ProjectID': parseInt(projectID._body, undefined),
                                'pipeLines': []
                            }
                            this.dataService.newProjectCreated.emit(eventObj);
                        });
                    } else {
                        obj['Group'] = this.selectedOption['Name'];
                        this.projectsService.cloneProject(this.selectedCopyOption['ProjectID'], obj).subscribe((projectID) => {
                            this.result = true;
                            this.close();
                            this.projectsService.getPrjectByName(this.projectname).subscribe(res => {
                                const eventObj = res;
                                this.projectsService.getPipelinesOfProject(eventObj.ProjectID).subscribe(pipelines => {
                                    eventObj.pipeLines = pipelines;
                                    this.dataService.newProjectCreated.emit(eventObj);
                                })

                            });
                        });

                    }
                });

            }
        }

    }

    failValidation(Message: string) {
        this.validationClass = 'validation-error js-form-is-collapsable';
        this.validationText = Message;
        this.failedValidation = true;
    }

    showError() {

        if (this.showFailedValidation) {
            this.validationClass = 'validation-error js-form-is-collapsable';
        } else {
            this.validationClass += ' js-form-message-open';
        }
        this.showFailedValidation = !this.showFailedValidation;
    }

    resetField() {
        this.validationClass = '';
        this.validationText = '';
        this.failedValidation = false;
    }

}

interface Project {
    [key: string]: string;
}
