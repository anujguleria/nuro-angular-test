﻿import { Injectable, OnInit } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class ProjectsService implements OnInit {
  url = environment.apiUrl + 'api/projects/';
  ngOnInit() {

  }

  constructor(private apiService: ApiService) { }
  getProjects(): Observable<any> {
    return this.apiService.get(this.url);
  }
  getProjectsWithPipelines(): Observable<any> {
    return this.apiService.get(this.url + 'pipelines/')
  }
  getPipelinesOfProject(projectId): Observable<any> {
    return this.apiService.get(this.url + projectId + '/pipelines');
  }
  getPrjectByName(name): Observable<any> {
    return this.apiService.get(this.url + 'ByName/' + name);
  }
  createProject(data): Observable<any> {
    return this.apiService.post(this.url, data);
  }
  updateProject(data): Observable<any> {
    return this.apiService.put(this.url + data.ID, data);
  }
  deleteProject(data): Observable<any> {
    return this.apiService.delete(this.url + data.ID);
  }
  updateProjectLastAccess(id: number) {
    return this.apiService.post(environment.apiUrl + 'api/users/LastAccess/' + id, {});
  }
  cloneProject(projectId, data): Observable<any> {
    return this.apiService.post(this.url + projectId + '/Clone', data);
  }
}
