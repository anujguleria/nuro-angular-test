﻿import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { environment } from '../../environments/environment';

@Injectable()
export class UserLoginService {
    constructor(private http: Http) { }

    login(username, password): Observable<any> {
        const data = 'grant_type=password&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);
        const headers = new Headers();

        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(environment.apiUrl + 'token', data, {
            headers: headers
        });
    }
}
