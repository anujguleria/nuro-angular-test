﻿import { Injectable, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { SignalR, ISignalRConnection, BroadcastEventListener } from 'ng2-signalr';


@Injectable()
export class DataService {
    public num_letter_spaces_underscore_rejexExp = '^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$';
    private userInfo: any;
    PipeData: any;
    userChange$: Observable<any>;
    private _userObserver: Observer<any>;
    signalRConnection: ISignalRConnection;
    groupData: any;
    ShowProjectButton = new EventEmitter<boolean>();
    showHideMask = new EventEmitter<boolean>();
    newProjectCreated = new EventEmitter<object>();
    newPipelineCreated = new EventEmitter<string>();
    ActivateAdminPanel = new EventEmitter<boolean>();
    dialogBoxData = new EventEmitter<any>();
    notificationEventEmitter = new EventEmitter<any>();
    newNotifications = new EventEmitter();
    pipelineInstanceDataRefreshed = new EventEmitter<object>();
    pipelineDeleted = new EventEmitter<string>();

    constructor(private _signalR: SignalR) {
        this.userChange$ = new Observable(observer =>
            this._userObserver = observer);
    }
    subscribeNotifications() {
        this.signalRConnection = this._signalR.createConnection({
            hubName: 'GenericNotification'
        });
        this.signalRConnection.start().then(result => {
            console.log('Connected');
            // 1.create a listener object
            const onPipelineUpdated$ = new BroadcastEventListener<string>('newNotification');
            // 2.register the listener
            this.signalRConnection.listen(onPipelineUpdated$);
            // 3.subscribe for updated pipeline instances
            onPipelineUpdated$.subscribe((res) => {
                this.newNotifications.next(res);
            });
        });
    }
    formatDate(date) {
        if (date) {
            // var newDate=new Date(date);
            // return moment(newDate).format('YYYY-MM-DD HH:mm:ss');
            let localDate;
            if (date.indexOf('Z') > -1) {
                localDate = date;
            } else {
                localDate = date + 'Z';
            }
            return new Date(localDate).toLocaleString().replace(',', '');
        } else {
            return '';
        }
    }
    setUserInfo(data) {
        this.userInfo = data;
        this._userObserver.next(data);
    }
    getUserInfo() {
        return this.userInfo;
    }

    setPipelineData(pipedata) {
        this.PipeData = pipedata;
    }
    getPipelineData() {
        return this.PipeData;
    }

    setGroupData(groupData) {
        this.groupData = groupData;
    }
    getGroupData() {
        return this.groupData;
    }
}
