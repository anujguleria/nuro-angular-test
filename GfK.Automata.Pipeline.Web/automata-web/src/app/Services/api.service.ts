﻿import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';


@Injectable()
export class ApiService {
    constructor(private http: Http, private router: Router) {
    }
    createAuthorizationHeader(headers: Headers, context?: any) {
        headers.append('Authorization', 'Bearer ' +
            localStorage.getItem('authToken'));
        headers.append('Content-Type', 'application/json');
        if (context) {
            headers.append('context', context);
        }
        // headers.append('Access-Control-Allow-Origin', '*');
    }
    get(url, context?): Observable<any> {
        const headers = new Headers();
        this.createAuthorizationHeader(headers, context);
        return this.http.get(url, {
            headers: headers
        }).map(res => {
            return res.json();
        }).catch((response) => {
            if (response.status === 401) {
                this.router.navigate(['/login']);
                return Observable.throw(response.json());
            } else {
                return Observable.throw(response.json());
            }
        });
    }

    post(url, data) {
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.post(url, data, {
            headers: headers
        }).catch((response) => {
            if (response.status === 401) {
                this.router.navigate(['/login']);
                return Observable.throw(response);
            } else {
                return Observable.throw(response);
            }
        });
    }

    put(url, data) {
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.put(url, data, {
            headers: headers
        }).catch((response) => {
            if (response.status === 401) {
                this.router.navigate(['/login']);
                return Observable.throw(response.json());
            } else {
                return Observable.throw(response.json());
            }
        });
    }
    delete(url) {
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        return this.http.delete(url, {
            headers: headers
        }).catch((response) => {
            if (response.status === 401) {
                this.router.navigate(['/login']);
                return Observable.throw(response);
            } else {
                return Observable.throw(response);
            }
        });
    }


}
