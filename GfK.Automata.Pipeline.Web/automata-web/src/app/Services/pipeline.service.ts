import { Injectable, OnInit } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class PipelineService implements OnInit {
    url = environment.apiUrl + 'api/pipelines/';
    ngOnInit() {

    }

    constructor(private apiService: ApiService) { }
    getPipeline(pipelineId): Observable<any> {
        return this.apiService.get(this.url + pipelineId);
    }
    getPipelineInstance(instanceId): Observable<any> {
        return this.apiService.get(this.url + '/PipelineInstances/' + instanceId);
    }
    getPipelineInstanceWithLogs(instanceId): Observable<any> {
        return this.apiService.get(this.url + '/PipelineInstances/' + instanceId + '/Logs');
    }
    getPipelineInstances(pipelineId): Observable<any> {
        return this.apiService.get(this.url + pipelineId + '/PipelineInstances');
    }
    getPipelineByName(pipelineName, context): Observable<any> {
        return this.apiService.post(this.url + 'ByName/' + pipelineName, context);
    }
    createPipeline(pipelineObj): Observable<any> {
        return this.apiService.post(this.url, pipelineObj);
    }
    getPipelineTasks(pipelineId): Observable<any> {
        return this.apiService.get(this.url + pipelineId + '/PipelineTasks');
    }
    movePipelineTask(pipelineTaskID: number, count: number) {
        return this.apiService.post(environment.apiUrl + 'api/PipelineTasks/Move/' + pipelineTaskID, count);
    }
    deletePipelineTask(pipelineTaskID: number) {
        return this.apiService.delete(environment.apiUrl + 'api/PipelineTasks/' + pipelineTaskID);
    }
    runPipeline(pipelineId, context): Observable<any> {
        return this.apiService.post(this.url + pipelineId + '/Run', context);
    }
    getPipelineParameters(pipelineId, direction) {
        return this.apiService.get(this.url + pipelineId + /Parameters/ + direction);
    }
    deletePipeline(pipelineID: number) {
        return this.apiService.delete(environment.apiUrl + 'api/Pipelines/' + pipelineID);

    }
    updatePipeline(data, id): Observable<any> {
        return this.apiService.put(this.url + id, data);
    }
    getNoficationSettings(pipelineId): Observable<any> {
        return this.apiService.get(environment.apiUrl + 'api/Pipelines/' + pipelineId + '/PipelineNotificationSettings');
    }
    updateNotificationSettings(data, pipelineID): Observable<any> {
        return this.apiService.put(this.url + pipelineID + '/PipelineNotificationSettings', data);
    }
}
