﻿import { Injectable, OnInit } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class NotificationsService implements OnInit {
    url = environment.apiUrl + '/api/Users/LoggedInUser/Notifications/';
    ngOnInit() {

    }

    constructor(private apiService: ApiService) { }

    getNotifications(): Observable<any> {
        return this.apiService.get(this.url);
    }

    deleteNotification(notificationID: number) {
        return this.apiService.delete(this.url + notificationID);
    }
}
