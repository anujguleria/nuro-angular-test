import { Injectable, OnInit } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class UserService implements OnInit {
    url = environment.apiUrl + 'api/Users';
    ngOnInit() {

    }

    constructor(private apiService: ApiService) { }
    getUsers(): Observable<any> {
        return this.apiService.get(this.url);
    }
}
