﻿/* tslint:disable:max-line-length */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularKalendarModule } from 'angular-kalendar';
import { DxTabPanelModule, DxTemplateModule } from 'devextreme-angular';

import { CalendarModule } from 'primeng/primeng';
import { SignalRModule } from 'ng2-signalr';
import { SignalRConfiguration } from 'ng2-signalr';

import { Routes, RouterModule } from '@angular/router';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ApiService } from './Services/api.service';
import { ProjectsService } from './Services/projects.service';
import { TaskService } from './task/shared/task.service';
import { DataService } from './Services/data.service';
import { PipelineService } from './Services/pipeline.service';
import { NotificationsService } from './Services/notifications.service';
import { environment } from '../environments/environment';
import { UserService } from './Services/user.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NewProjectComponent } from './header/new-project/new-project.component';
import { NewTaskComponent } from './task/new-task/new-task.component';
import { AddButtonComponent } from './header/add-button/add-button.component';
import { FooterComponent } from './footer/footer.component';
import { ConfirmComponent } from './shared/confirm-dialog.component';
import { NotificationsComponent } from './projects-container/notifications/notifications.component';
import { WarningComponent } from './shared/warning-dialog.component';

import { LoginBoxComponent } from './login-box/login-box.component';
import { ProjectscontainerComponent } from './projects-container/projects-container.component';
import { ProjectsPanelComponent } from './projects-container/projects-panel/projects-panel.component';
import { ProjectPanelPipelineComponent } from './projects-container/projects-panel/project-panel-pipeline/project-panel-pipeline.component';
import { ProjectViewComponent } from './projects-container/project-view/project-view.component';
import { AddNewPipelineComponent } from './projects-container/project-view/add-new-pipeline/add-new-pipeline.component';
import { AuthGuard } from './_guards/auth.guard';
import { RunPipelineComponent } from './projects-container/project-view/project-view-pipeline/run-pipeline/run-pipeline.component';
import { RenamePipelineComponent } from './projects-container/project-view/project-view-pipeline/rename-pipeline/rename-pipeline.component';

import { MaskComponent } from './mask/mask.component';
import { ProjectViewPipelineComponent } from './projects-container/project-view/project-view-pipeline/project-view-pipeline.component';
import { PipelineInstanceComponent } from './projects-container/project-view/project-view-pipeline/pipeline-instance/pipeline-instance.component';
import { ManageParametersComponent } from './projects-container/project-view/project-view-pipeline/pipeline-parameters/manage-parameters.component';
import { AdminComponent } from './admin/admin.component';
import { DataTableComponent } from './data-table/data-table.component';
import { SchedulerPanelComponent } from './projects-container/project-view/project-view-pipeline/scheduler-panel/scheduler-panel.component';
import { DialogBoxComponent } from './admin/dialog-box/dialog-box.component';
import { SearchFilterPipe } from './utilities/pipes';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { PipelineSettingsComponent } from './projects-container/project-view/project-view-pipeline/pipeline-settings/pipeline-settings.component';
import { UpdateGroupComponent } from './admin/update-group/update-group.component';
import { GridDataTableComponent } from './common/grid-data-table/grid-data-table.component';
import { DeleteGroupWarningComponent } from './admin/update-group/delete-group-warning/delete-group-warning.component';
/* tslint:enable:max-line-length */

const appRoutes: Routes = [
    { path: 'login', component: LoginBoxComponent },
    { path: 'projectscontainer', component: ProjectscontainerComponent, canActivate: [AuthGuard] },
    { path: '', component: ProjectscontainerComponent, canActivate: [AuthGuard] }

];

export function createConfig(): SignalRConfiguration {
    const c = new SignalRConfiguration();
    c.hubName = 'NotificationHub';
    c.url = environment.apiUrl;
    c.logging = true;
    return c;
}

@NgModule({
    declarations: [
        AppComponent,
        ConfirmComponent,
        WarningComponent,
        HeaderComponent,
        FooterComponent,
        LoginBoxComponent,
        ProjectscontainerComponent,
        ProjectsPanelComponent,
        ProjectViewComponent,
        ProjectPanelPipelineComponent,
        NewProjectComponent,
        AddButtonComponent,
        MaskComponent,
        ProjectViewPipelineComponent,
        AddNewPipelineComponent,
        PipelineInstanceComponent,
        NewTaskComponent,
        ManageParametersComponent,
        AdminComponent,
        DataTableComponent,
        SchedulerPanelComponent,
        NotificationsComponent,
        DialogBoxComponent,
        SearchFilterPipe,
        ClickOutsideDirective,
        RunPipelineComponent,
        PipelineSettingsComponent,
        RenamePipelineComponent,
        UpdateGroupComponent,
        GridDataTableComponent,
        DeleteGroupWarningComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),
        BootstrapModalModule,
        // NguiDatetimePickerModule,
        AngularKalendarModule,
        DxTabPanelModule,
        DxTemplateModule,
        CalendarModule,
        SignalRModule.forRoot(createConfig),
        DateTimePickerModule
    ],
    entryComponents: [
        AddNewPipelineComponent,
        ConfirmComponent,
        WarningComponent,
        PipelineInstanceComponent,
        ProjectscontainerComponent,
        ProjectsPanelComponent,
        ProjectViewComponent,
        ProjectPanelPipelineComponent,
        NewProjectComponent,
        NewTaskComponent,
        ManageParametersComponent,
        DialogBoxComponent,
        NotificationsComponent,
        RunPipelineComponent,
        PipelineSettingsComponent,
        RenamePipelineComponent,
        UpdateGroupComponent,
        DeleteGroupWarningComponent
    ],
    providers: [
        AuthGuard,
        ApiService,
        DataService,
        NotificationsService,
        PipelineService,
        ProjectsService,
        TaskService,
        UserService,
        { provide: LocationStrategy, useClass: HashLocationStrategy }],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class AppModule { }
