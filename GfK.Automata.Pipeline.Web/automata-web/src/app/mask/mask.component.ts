﻿import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-mask',
  templateUrl: './mask.component.html',
  styleUrls: ['./mask.component.css']
})
export class MaskComponent implements OnInit, OnDestroy {

  constructor( @Inject(DOCUMENT) private document: Document) {
  }

  ngOnInit() {
    this.document.body.classList.add('overflowHidden');
  }
  ngOnDestroy() {
    this.document.body.classList.remove('overflowHidden');
  }

}
