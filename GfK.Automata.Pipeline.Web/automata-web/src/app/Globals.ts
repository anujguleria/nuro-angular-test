﻿/// <reference path="../environments/environment.prod.ts" />
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class AppScope {
    data: any;
    isAuthenticated: boolean;

    constructor() {
    }

    setData(data: any) {
        this.data = data;
    }
}
