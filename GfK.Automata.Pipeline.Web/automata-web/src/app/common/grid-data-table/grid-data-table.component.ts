import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-grid-data-table',
  templateUrl: './grid-data-table.component.html',
  styleUrls: ['./grid-data-table.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in', style({

        opacity: 1.0,
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({

        opacity: 0.0,
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})
export class GridDataTableComponent implements OnInit {
  @Input()
  data;
  @Input()
  columnconfig;
  @Input()
  settings;
  constructor() { }

  ngOnInit() {
  }
  checkdisable(row, column) {
    if (column['dependency'].length === 0) { return false } else {
      return row[column['dependency'][column['dependency'].length - 1]];
    }
  }

  delete($event, row) {
    this.data.splice(this.data.indexOf(row), 1);
  }
}
