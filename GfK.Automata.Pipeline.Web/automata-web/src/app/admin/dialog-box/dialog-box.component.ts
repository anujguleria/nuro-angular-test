﻿import { Component, OnInit, Input } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { DataService } from '../../Services/data.service';
import { ConfirmModel } from '../../interfaces/ConfirmModel';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
  title: string;
  message: string;
  projectId: string;
  parentId: string;
  gridData: any = {};
  constructor(dialogService: DialogService, dataservice: DataService) {
    super(dialogService);

    dataservice.dialogBoxData.subscribe((data) => {
      this.gridData = data;
    });
  }

  ngOnInit() {
  }
  confirm(event: Event) {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.result = true;
    this.close();

  }
}
