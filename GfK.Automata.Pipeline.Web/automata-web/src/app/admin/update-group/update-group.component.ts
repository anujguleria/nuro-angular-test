import { Component, OnInit, Input } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { ConfirmModel } from '../../interfaces/ConfirmModel';
import { DataService } from '../../Services/data.service';
import { ApiService } from '../../Services/api.service';
import { environment } from 'environments/environment';
import { WarningComponent } from '../../shared/warning-dialog.component';
import { DeleteGroupWarningComponent } from '../update-group/delete-group-warning/delete-group-warning.component';
import * as _ from 'underscore';

@Component({
  selector: 'app-update-group',
  templateUrl: './update-group.component.html',
  styleUrls: ['./update-group.component.css']
})

export class UpdateGroupComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
    @Input()
    data;
    title: string;
    message: string;
    projectId: string;
    parentId: string;
    validationError: string;
    errorEnabled: boolean;
    isErorrOpened: boolean;
    pipelineData: any;
    disable = false;
    errorMessage = undefined;
    groupName: string;
    group: any = {};
    groupData: any;
    saveButton = 'Save';

    constructor(dialogService: DialogService, private dataService: DataService, private apiService: ApiService) {
      super(dialogService);
    }


    ngOnInit() {
      this.groupData = this.dataService.getGroupData();
      this.group = this.groupData[this.parentId];
      this.groupName = this.message;
      if (this.title === 'Delete Group') {
          this.saveButton = 'OK';
      }
  }

    confirm() {
    switch (this.title) {
        case 'Delete Group':
            this.deleteGroup();
            break;
        case 'Rename Group':
            this.renameGroup();
            break;
    }
  }

  deleteGroup() {

      const control = 'api/Groups/',
          id = this.group['GapGroupID'];
      this.apiService.delete(environment.apiUrl + control + id).subscribe((response) => {
          this.result = true;
          this.close();
      }, error => {
          if (error.status === 403) {
              // display warning listing projects that use this group
            const inProjectList = JSON.parse(error._body);
            const projectList = [];
            _.each(inProjectList, function (project) {
              const name = project.toString().split('Name = ');
              if (name[1]) {
                projectList.push({ Name: name[1].replace(' }', '') });
              }
            });
              const msg = 'The following projects are associated to this group, please move/delete them prior to deleting this group: ';
              if (projectList.length > 0) {

                  this.dataService.showHideMask.emit(true);
                  this.dialogService.addDialog(DeleteGroupWarningComponent, {
                      title: 'Warning',
                      message: msg,
                      msgData: projectList
                  }).subscribe(() => {
                      this.close();
                      this.dataService.showHideMask.emit(false);
                  });
              }
          }
      });

  }

  renameGroup() {
      if (this.groupName === undefined || (this.groupName.trim() === '')) {
          this.showError('Please enter a Group Name.');
      } else {
          const regexp = new RegExp(this.dataService.num_letter_spaces_underscore_rejexExp);
          const test: boolean = regexp.test(this.groupName);
          if (!test) {
              this.showError('Group Name only supports numbers, letters, spaces and underscores.');
          } else {
              // check if name is already in use
            const groupName = this.groupName;
            let nameError = false;

              if (this.groupData !== undefined) {
                  _.each(this.groupData, function (group) {
                      if (group['Name'] === groupName) {
                          nameError = true;
                      }
                  });
              }
              if (nameError) {
                  this.showError('Group Name in use, please enter another name.');
              } else {
                  const toSend = this.groupData[this.parentId];
                  toSend['Name'] = this.groupName;
                  const control = 'api/Groups/',
                      id = this.group['GapGroupID'];
                  this.apiService.put(environment.apiUrl + control + id, toSend).subscribe((response) => {
                      this.result = true;
                      this.close();
                  }, error => {
                      this.showError('Error saving new name.');
                  });
              }
          }
      }
  }

  showError(message) {
      this.validationError = message;
      this.errorEnabled = true;
  }

}
