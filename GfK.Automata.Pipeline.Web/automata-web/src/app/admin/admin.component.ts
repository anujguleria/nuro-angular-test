import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { environment } from 'environments/environment';
import { DialogBoxComponent } from '../admin/dialog-box/dialog-box.component';
import { ConfirmModel } from '../interfaces/ConfirmModel';
import { DataService } from '../Services/data.service';
import { DialogService } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from '../shared/confirm-dialog.component';
import * as _ from 'underscore';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']

})
export class AdminComponent implements OnInit, ConfirmModel {
  title: string;
  message: string;
  parentId: string;
  projectId: string;
  roleMapping = {
    1: 'Execute',
    2: 'Admin',
    3: 'Configure'
  }
  menuState: any[] = [];
  gridData = {
    data: []
    ,
    columnconfig: [
      { prop: 'Email', as: 'Name', cssClass: 'equalWidth', cellTemplate: 2, dependency: [] },
      { prop: 'Admin', as: 'Admin', cssClass: 'equalWidth', cellTemplate: 1, dependency: [] },
      { prop: 'Configure', as: 'Configure', cssClass: 'equalWidth', cellTemplate: 1, dependency: ['Admin'] },
      { prop: 'Execute', as: 'Execute', cssClass: 'equalWidth', cellTemplate: 1, dependency: ['Admin', 'Configure'] },
      {
        prop: undefined, as: 'Add to Group', noData: true, template:
          '<div (click)="column.click" class="glyphicon glyphicon-plus show-hand"></div>', cssClass: 'equalWidth', cellTemplate: 3
      },
      {
        prop: undefined, as: 'Delete', cssClass: 'equalWidth', template:
          '<div (click)="column.click" class="glyphicon glyphicon-remove show-hand"></div>', cellTemplate: 6, dependency: []
      }
    ],
    events: {
      rowClicked: this.permissionUpdated.bind(this),
      customEvent: this.customevent.bind(this),
      addEvent: this.addtoDB.bind(this),
      deleteRow: this.deleteUser.bind(this)
    },
    settings: {
      search: false,
      custom: true,
      enableDropDown: true,
      dropDown: [{ name: 'Admin', id: 2 }, { name: 'Configure', id: 3 }, { name: 'Execute', id: 1 }],
      TableName: 'UserAccess',
      dropDownPlaceholder: 'Add User',
      showHeader: true,
      colspan: 3,
      Secondcolspan: 3

    }
  };
  gridData2 = {
    data: [],
    columnconfig: [
      { prop: 'Name', as: 'Name', cssClass: 'equalWidth', cellTemplate: 2, dependency: [] },
      {
        prop: undefined, as: 'Edit Group', noData: true,
        template: '<div (click)="column.click" class="glyphicon glyphicon-plus show-hand"></div>',
        cssClass: 'equalWidth', cellTemplate: 3
      },
      {
        prop: undefined, as: '', noData: true,
        template: '<div class="glyphicon glyphicon-cog pull-right pointerCursor"></div>',
        cssClass: 'equalWidth', cellTemplate: 5
      }
    ],
    events: {
      rowClicked: this.permissionUpdated.bind(this),
      customEvent: this.customevent.bind(this),
      addEvent: this.addtoDB.bind(this)

    },
    settings: {
      search: false,
      custom: true,
      enableDropDown: false,
      dropDownPlaceholder: 'Add Group',
      TableName: 'Group Access',
      showHeader: true,
      colspan: 2

    }
  };
  UserOptionsData = {
    data: [],
    columnconfig: [
      { prop: 'Name', as: '', cssClass: 'equalWidth', cellTemplate: 2, dependency: [] },
      {
        prop: undefined, as: '', noData: true,
        template: '<div class="glyphicon glyphicon-remove show-hand"></div>', cssClass: 'equalWidth', cellTemplate: 4
      }
    ],
    events: {
      rowClicked: this.permissionUpdated.bind(this),
      customEvent: this.customevent.bind(this),
      addEvent: this.addtoDB.bind(this),
      dialogSave: this.dialogSave.bind(this)
    },
    settings: {
      search: false,
      custom: true,
      enableDropDown: false,
      dropDownPlaceholder: 'Add Groups',
      TableName: 'toUsers',
      showHeader: false,
      CustomRow: 'Assigned Groups',
      showConfirm: true,
      SearchKey: 'Name',
      colspan: 1
    },
    suggestionData: []
  };
  GroupOptionsData = {
    data: [],
    columnconfig: [
      { prop: 'Email', as: '', cssClass: 'equalWidth', cellTemplate: 2, dependency: [] },
      {
        prop: undefined, as: '', noData: true,
        template: '<div class="glyphicon glyphicon-remove show-hand"></div>', cssClass: 'equalWidth', cellTemplate: 4
      }
    ],
    events: {
      rowClicked: this.permissionUpdated.bind(this),
      customEvent: this.customevent.bind(this),
      addEvent: this.addtoDB.bind(this),
      dialogSave: this.dialogSave.bind(this)
    },
    settings: {
      search: false,
      custom: true,
      enableDropDown: false,
      dropDownPlaceholder: 'Add Users',
      TableName: 'toGroup',
      showHeader: false,
      CustomRow: 'Assigned Users',
      showConfirm: true,
      SearchKey: 'Email',
      colspan: 1

    },
    suggestionData: []
  };

  constructor(private apiService: ApiService, private dialogService: DialogService, private dataservice: DataService) {
    this.updateUsers();
    this.updateGroups();
  }
  ngOnInit() {
  }

  deleteUser(user) {
    this.dataservice.showHideMask.emit(true);
    const userName = user.Email.substring(0, user.Email.indexOf('@'));
    const disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: 'Delete User',
      message: this.message === undefined ? 'Are you sure you want to delete user - ' + userName + '?' : this.message
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.apiService.delete(environment.apiUrl + 'api/users/' + user.UserID).subscribe(res => {
          const index = this.gridData.data.indexOf(user);
          this.gridData.data.splice(index, 1);
          this.dataservice.showHideMask.emit(false);

        });
      } else {
        this.dataservice.showHideMask.emit(false);
      }
    }, (error) => {
      this.dataservice.showHideMask.emit(false);
    });
  }
  dialogSave(tableName, DataObject) {
    let toSend = {}
    let control = '';
    let id = '';
    toSend = DataObject['parentRow'];
    toSend['GapGroupUsers'] = [];

    switch (tableName) {
      case 'toGroup':
        for (let i = 0; i < DataObject['currentDataSet'].length; i++) {
          toSend['GapGroupUsers'].push({
            'GapGroupID': DataObject['parentRow']['GapGroupID'],
            'UserID': DataObject['currentDataSet'][i]['UserID']
          });
        }
        control = 'api/Groups/';
        id = toSend['GapGroupID'];
        break;

      case 'toUsers':
        for (let i = 0; i < DataObject['currentDataSet'].length; i++) {
          toSend['GapGroupUsers'].push({
            'GapGroupID': DataObject['currentDataSet'][i]['GapGroupID'],
            'UserID': DataObject['parentRow']['UserID']
          });
        }
        control = 'api/Users/';
        id = toSend['UserID'];
        break;
    }

    this.apiService.put(environment.apiUrl + control + id, toSend).subscribe((response) => {
      console.log(response);
      this.updateUsers();
      this.updateGroups();
      this.updateCurrentUserInfo();
    });

  }
  permissionUpdated(obj, row, col) {
    const toSend = {};
    Object.assign(toSend, row);
    const index = this.gridData.data.indexOf(row);
    const changedval = !row[col.prop];
    switch (col.prop) {
      case 'Admin':
        row[col.prop] = changedval;
        row['Configure'] = changedval;
        row['Execute'] = changedval;
        toSend['GapRoleUsers'] = [];
        if (changedval) {
          toSend['GapRoleUsers'].push({ 'GapRoleID': 1, 'UserID': row['UserID'] });
          toSend['GapRoleUsers'].push({ 'GapRoleID': 2, 'UserID': row['UserID'] });
          toSend['GapRoleUsers'].push({ 'GapRoleID': 3, 'UserID': row['UserID'] });
        }
        break;
      case 'Configure':
        row[col.prop] = changedval;
        row['Execute'] = changedval;
        toSend['GapRoleUsers'] = [];
        if (changedval) {
          toSend['GapRoleUsers'].push({ 'GapRoleID': 1, 'UserID': row['UserID'] });
          toSend['GapRoleUsers'].push({ 'GapRoleID': 3, 'UserID': row['UserID'] });
        }
        break;
      default:
        row[col.prop] = !row[col.prop];
        toSend['GapRoleUsers'] = [];

        if (changedval) {
          toSend['GapRoleUsers'].push({ 'GapRoleID': 1, 'UserID': row['UserID'] });
        }
        break;
    }
    this.gridData.data[index] = row;

    delete toSend['Admin'];
    delete toSend['Configure'];
    delete toSend['Execute'];
    this.apiService.put(environment.apiUrl + 'api/Users/' + toSend['UserID'], toSend).subscribe((response) => {
      console.log(response);
    });
  }
  customevent(TableName, row) {
    let title = '';
    if (TableName === 'Group Access') {
      title = 'Edit Group';
    } else if (TableName === 'UserAccess') {
      title = 'Add Groups';
    }
    this.showConfirm(title, row);
  }
  addtoDB(tableName, DataObject) {
    const toSend = {};
    let controller = '';
    if (tableName === 'UserAccess') {

      toSend['Email'] = DataObject['name'];
      toSend['GapRoleUsers'] = [];
      const split = DataObject['Access'].toString().split(',');
      for (let i = 0; i < split.length; i++) {
        toSend['GapRoleUsers'].push({ 'GapRoleID': split[i] });
      }
      controller = 'api/Users';
    } else if (tableName === 'Group Access') {
      toSend['Name'] = DataObject['name'];
      controller = 'api/Groups';
    } else if (tableName === 'toGroup') {
      const GapGroupID = DataObject['parentRow']; // GapGroupID
      const selectedUser = DataObject['selectedUser'];
    }

    this.apiService.post(environment.apiUrl + controller, toSend).subscribe((response) => {
      if (controller.indexOf('Groups') > -1) {
        if (response['status'] === 201) {
          this.updateGroups();
        }
      } else if (controller.indexOf('Users') > -1) {
        this.updateUsers();
      }
      console.log(response);
    });
  }
  showConfirm(title: string, row) {
    this.dataservice.showHideMask.emit(true);

    const disposable = this.dialogService.addDialog(DialogBoxComponent, {
      title: title,
      message: undefined,
      projectId: undefined,
      parentId: undefined

    })
      .subscribe((isConfirmed) => {
        this.dataservice.showHideMask.emit(false);
      });
    if (title === 'Add Groups') {
      this.UserOptionsData.data = [];
      this.UserOptionsData['SelectedRowInfo'] = row;
      for (let i = 0; i < row['GapGroupUsers'].length; i++) {
        this.UserOptionsData.data.push(row['GapGroupUsers'][i]['GapGroup']);
      }
      this.UserOptionsData.data.sort(function (a, b) {
        return a['Name'].localeCompare(b['Name'], undefined /* Ignore language */, { sensitivity: 'base' })
      });

      this.dataservice.dialogBoxData.emit(this.UserOptionsData);
    } else if (title === 'Edit Group') {
      this.GroupOptionsData.data = [];
      this.apiService.get(environment.apiUrl + 'api/Groups/' + row['GapGroupID'] + '/Users').subscribe((response) => {
        this.GroupOptionsData['SelectedRowInfo'] = response;
        console.log(response);
        for (let i = 0; i < response['GapGroupUsers'].length; i++) {
          this.GroupOptionsData.data.push(response['GapGroupUsers'][i]['User']);
        }
        this.GroupOptionsData.data.sort(function (a, b) {
          return a['Email'].localeCompare(b['Email'], undefined /* Ignore language */, { sensitivity: 'base' })
        });
        this.dataservice.dialogBoxData.emit(this.GroupOptionsData);

      });

    }
  }
  updateGroups() {
    this.apiService.get(environment.apiUrl + 'api/Groups').subscribe((response) => {
      console.log(response);
      this.gridData2.data = response;
      this.UserOptionsData.suggestionData = response;

      for (let a = 0; a < this.gridData2.data.length; a++) {
        if (this.menuState !== undefined && this.menuState[a] !== undefined) {
          this.menuState[a] = 'out';
        } else {
          this.menuState.push('out');
        }
      }
    });
  }
  updateUsers() {
    this.apiService.get(environment.apiUrl + 'api/Users').subscribe((response) => {
      for (let i = 0; i < response.length; i++) {
        for (let j = 0; j < response[i]['GapRoleUsers'].length; j++) {
          response[i][this.roleMapping[response[i]['GapRoleUsers'][j]['GapRoleID']]] = true;
        }
      }
      console.log(response);
      this.gridData.data = response;
      this.GroupOptionsData.suggestionData = response;
    });
  }

  updateCurrentUserInfo() {
    this.apiService.get(environment.apiUrl + 'api/Users/GetCurrentLoggedInUser').subscribe((response) => {
      this.dataservice.setUserInfo(response);
    });
  }
}
