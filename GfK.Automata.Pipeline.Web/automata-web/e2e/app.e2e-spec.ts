import { AutomataWebPage } from './app.po';

describe('automata-web App', () => {
  let page: AutomataWebPage;

  beforeEach(() => {
    page = new AutomataWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
