using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("GfK.Automata.Pipeline.Test.Functional")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GfK NA")]
[assembly: AssemblyProduct("GfK.Automata.Pipeline.Test.Functional")]
[assembly: AssemblyCopyright("Copyright © GfK NA 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("dca70431-87bb-441a-9bfa-cfe958100bfb")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
