﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GfK.Automata.Pipeline.GapModule.Core;
using GfK.Automata.Pipeline.GapModule;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using GfK.Automata.Pipeline.Model.Domains;
using System.Collections.Generic;

namespace GfK.Automata.Pipeline.Test.Functional.Modules
{
    [TestClass, Ignore]
    public class CoreModules
    {
        private TestLogger logger;
        [TestInitialize]
        public void Initialize()
        {
            logger = new TestLogger();
        }
        [TestMethod]
        public void DimensionsModule()
        {
            DimensionsModule dimensions = new DimensionsModule();
            dimensions.Logger = logger;
            IParameterValueCollection parameters = new ParameterValueCollection();
            parameters.AddParameter(new ParameterString("DimensionsModule", "SurveyName")
            {
                Value = "USCDAP508039_TN152"
            }).Wait();
            parameters.AddParameter(new ParameterString("DimensionsModule", "Version")
            {
                Value = ""
            }).Wait();
            parameters.AddParameter(new ParameterString("DimensionsModule", "Cluster")
            {
                Value = "70_PROD"
            }).Wait();
            Task<IParameterValueCollection> task = dimensions.RunTask(parameters);
            task.Wait();
            IParameterValueCollection returnParameters = task.Result;
            Assert.IsNotNull(returnParameters.GetParameter("DimensionsDataFile"));
            Task<IParameter> taskParam = returnParameters.GetParameter("DimensionsDataFile");
            taskParam.Wait();
            IParameterDimensionsCapable dimParam = (IParameterDimensionsCapable)taskParam.Result;
            Assert.IsNotNull(dimParam.DataFilePath);
            Assert.IsTrue(dimParam.DataFilePath.ToLower().Contains(".ddf"));
            Assert.IsNotNull(dimParam.MDDPath);
            Assert.IsTrue(dimParam.MDDPath.ToLower().Contains(".mdd"));
            Assert.AreEqual(dimParam.DimensionDataFileType, DimensionDataFileType.DDF);
        }
        [TestMethod]
        public void FileInputModule()
        {
            FileInputModule fileInput = new FileInputModule();
            fileInput.Logger = logger;
            IParameterValueCollection parameters = new ParameterValueCollection();
            String testFile = Path.GetTempFileName();
            FileInfo testFileInfo = new FileInfo(testFile);
            parameters.AddParameter(new ParameterString("FileInputModule", "InputFilePath")
            {
                Value = testFile
            }).Wait();
            Task<IParameterValueCollection> task = fileInput.RunTask(parameters);
            task.Wait();
            IParameterValueCollection returnParameters = task.Result;
            Task<IParameter> parameter = (Task<IParameter>)returnParameters.GetParameter("OutputFilePath");
            parameter.Wait();
            string filePath = ((IParameterString)parameter.Result).Value;
            Assert.IsTrue(File.Exists(filePath));
            FileInfo filePathInfo = new FileInfo(filePath);
            Assert.AreEqual(testFileInfo.Name, filePathInfo.Name);
            File.Delete(testFile);
        }
        [TestMethod]
        public void FileOutputModule()
        {
            FileOutputModule fileOutput = new FileOutputModule();
            fileOutput.Logger = logger;
            IParameterValueCollection parameters = new ParameterValueCollection();
            String testFile = Path.GetTempFileName();
            FileInfo testFileInfo = new FileInfo(testFile);
            parameters.AddParameter(new ParameterString("FileOutputModule", "InputFilePath")
            {
                Value = testFile
            }).Wait();
            string guid = Guid.NewGuid().ToString();
            string outputDirectory = Path.GetTempPath() + "\\" + guid;
            Directory.CreateDirectory(outputDirectory);
            parameters.AddParameter(new ParameterString("FileOutputModule", "OutputPath")
            {
                Value = outputDirectory
            }).Wait();
            Task<IParameterValueCollection> task = fileOutput.RunTask(parameters);
            task.Wait();
            IParameterValueCollection returnParameters = task.Result;
            Task<IParameter> parameter = (Task<IParameter>)returnParameters.GetParameter("OutputFilePath");
            parameter.Wait();
            string filePath = ((IParameterString)parameter.Result).Value;
            Assert.IsTrue(File.Exists(filePath));
            FileInfo filePathInfo = new FileInfo(filePath);
            Assert.AreEqual(testFileInfo.Name, filePathInfo.Name);
            File.Delete(testFile);
            Directory.Delete(outputDirectory, true);
        }
        [TestMethod]
        public void EXEModule()
        {
            EXEModule EXE = new EXEModule();
            EXE.Logger = logger;
            IParameterValueCollection parameters = new ParameterValueCollection();
            parameters.AddParameter(new ParameterString("EXEModule", "Command")
            {
                Value = "ping espn.com"
            }).Wait();
            Task<IParameterValueCollection> task = EXE.RunTask(parameters);
            task.Wait();
            IParameterValueCollection returnParameters = task.Result;
        }
        [TestMethod]
        public void MRSModule()
        {
            string mrsScript = "#define MDAT \"C:\\Windows\\TEMP\\tmpE782.tmp\\USCDAP508039_TN152.sav\"";
            MRSModule MRS = new MRSModule();
            MRS.Logger = logger;
            IParameterValueCollection parameters = new ParameterValueCollection();
            parameters.AddParameter(new ParameterString("MRSModule", "Script")
            {
                Value = mrsScript
            }).Wait();
            Task<IParameterValueCollection> task = MRS.RunTask(parameters);
            task.Wait();
            IParameterValueCollection returnParameters = task.Result;
        }
        [TestMethod]
        public void DMSModule()
        {
            StringBuilder template = new StringBuilder();
            string crlf = "\r\n";
            template.Append("#define MySelectQuery \"Select * from vdata\"" + crlf);
            template.Append("InputDatasource(myInputDataSource, \"RDBInput.dms\")" + crlf);
            template.Append("   ConnectionString = \"Provider = mrOleDB.Provider.2; Data Source = mrRdbDsc2; Initial Catalog = C:\\Users\\brady.brau\\AppData\\Local\\Temp\\396a2093-64d1-4126-828c-5fb3f7d71bb4\\1d13c054-9017-4832-b3ad-87194e5d426c\\USCDAP508039_TN152_in.mdd; Location = 'Provider=SQLOLEDB.1;Data Source=NUEW-SQAC70.EXT.GFK;UID=pmswebuser;PWD=Webuserpm5;Initial Catalog=USCDAP508039_TN152'; MR Init MDM Version = {..}; MR Init Project = USCDAP508039_TN152\"" + crlf);
            template.Append("   SelectQuery = MySelectQuery" + crlf);
            template.Append("End InputDatasource" + crlf);
            template.Append("" + crlf);
            template.Append("OutputDataSource(SavOutput)" + crlf);
            template.Append("  n  ConnectionString = \"Provider = mrOleDB.Provider.2; _" + crlf);
            template.Append("       Data Source = mrSavDsc; _" + crlf);
            template.Append("       Location = 'USCDAP508039_TN152.sav'; _" + crlf);
            template.Append("   MR Init Custom = SavMaxVarNameLen = 8\"" + crlf);
            template.Append("   MetaDataOutputName = \"USCDAP508039_TN152.pinney.mdd\"" + crlf);
            template.Append("End OutputDataSource" + crlf);
            DMSModule dms = new GapModule.Core.DMSModule();
            dms.Logger = logger;
            IParameterValueCollection parameters = new ParameterValueCollection();
            parameters.AddParameter(new ParameterString("DMSModule", "DMSScript")
            {
                Value = template.ToString()
            }).Wait();
            Task<IParameterValueCollection> task = dms.RunTask(parameters);
            task.Wait();
        }
    }
    public class TestLogger : ILogger<ModuleLogMessageType>
    {
        public TestLogger()
        {
            messages = new List<Tuple<ModuleLogMessageType, string>>();
            PipelineTaskInstanceID = 1;
        }
        public List<Tuple<ModuleLogMessageType, string>> messages;
        public int PipelineTaskInstanceID { get; set; }

        public void Log(ModuleLogMessageType level, string message)
        {
            messages.Add(new Tuple<ModuleLogMessageType, string>(level, message));
        }

        public async Task LogAsync(ModuleLogMessageType level, string message)
        {
            messages.Add(new Tuple<ModuleLogMessageType, string>(level, message));
        }
    }
}
