﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional
{
    class ConfigureTask
    {
        public ConfigureTask()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "startPipe")]
        public IWebElement btnStartPipeline { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li/div[2]/div/div/div/div[2]")]
        public IWebElement btnStartPipeline2 { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[1]/div[2]")]
        public IWebElement btnStartPipeline3 { get; set; }

        [FindsBy(How = How.Id, Using = "endPipe")]
        public IWebElement btnEndPipeline { get; set; }

        [FindsBy(How = How.Id, Using = "save-task")]
        public IWebElement btnSaveTask { get; set; }

        [FindsBy(How = How.Id, Using = "cancel")]
        public IWebElement btnCloseWindow { get; set; }

        [FindsBy(How = How.ClassName, Using = "icon-warning_30px")]
        public IWebElement btnWarning { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-new-task/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/p")]
        public IWebElement txtErrorMsg { get; set; }

        [FindsBy(How = How.Id, Using = "input-field-info-4")]
        public IWebElement txtTaskName { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-new-task/div/div/div[2]/div/div/div[2]/div/div[2]/div[1]/div[2]/label/font")]
        public IWebElement btnDimensionsModule { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-new-task/div/div/div[2]/div/div/div[2]/div/div[2]/div[3]/div[2]/label/font")]
        public IWebElement btnEXEModule { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-new-task/div/div/div[2]/div/div/div[1]/div/div[2]/div[2]/textarea")]
        public IWebElement txtEXEModule { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[2]/img")]
        public IWebElement btnFirstTask { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[3]/div[2]/img")]
        public IWebElement btnSecondTask { get; set; }

        [FindsBy(How = How.Id, Using = "confirm-button")]
        public IWebElement btnDeleteConfirm { get; set; }

        [FindsBy(How = How.Id, Using = "run-pipeline")]
        public IWebElement btnRunPipeline { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-run-pipeline/div/div/div[3]/button")]
        public IWebElement btnOk { get; set; }


        public void AddTask(string taskName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));

            //Create a task
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            //System.Threading.Thread.Sleep(500);
            Actions action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();

            txtTaskName.EnterText(taskName);
            btnEXEModule.Clicks();
            txtEXEModule.EnterText("echo Hello World");
            //Save
            btnSaveTask.Clicks();
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[1]"), taskName));
        }
        public void TaskRightClickControls(string taskName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));

            //Edit Task Test
            //Create a task            
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            Actions action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();
            txtTaskName.EnterText(taskName);
            btnEXEModule.Clicks();
            txtEXEModule.EnterText("echo Hello World");
            //Save
            btnSaveTask.Clicks();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//dx-tab-panel")));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[1]"), taskName));

            action = new Actions(PropertiesCollection.driver).ContextClick(btnFirstTask);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-edit")).Clicks();

            //Confirm task is populated correctly
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-new-task/div/div/div[1]/h4/b"), "Configure Task"));
            Assert.AreEqual("echo Hello World", SeleniumMethods.GetText(txtEXEModule));

            //Adjust Task Name
            taskName = "Task 2";
            txtTaskName.Clear();
            txtTaskName.EnterText(taskName);
            btnSaveTask.Clicks();
            //Confirm Task Name Change
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[1]"), taskName));
            Assert.AreEqual(taskName, PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[1]")).Text);

            
            //Add Task to the right
            action = new Actions(PropertiesCollection.driver).ContextClick(btnFirstTask);            
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-right")).Clicks();
            taskName = "Task 3";
            txtTaskName.EnterText(taskName);
            btnEXEModule.Clicks();
            txtEXEModule.EnterText("echo Second Task");
            //Save
            btnSaveTask.Clicks();
            //Confirm Task is added
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[3]/div[1]"), taskName));
            Assert.AreEqual(taskName, PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[3]/div[1]")).Text);


            //Add Task to the left
            action = new Actions(PropertiesCollection.driver).ContextClick(btnSecondTask);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();
            taskName = "Task 4";
            txtTaskName.EnterText(taskName);
            btnEXEModule.Clicks();
            txtEXEModule.EnterText("echo Third Task in second position");
            //Save
            btnSaveTask.Clicks();
            //Confirm Task is added
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[3]/div[1]"), taskName));
            Assert.AreEqual(taskName, PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[3]/div[1]")).Text);


            //Move Task to the right
            action = new Actions(PropertiesCollection.driver).ContextClick(btnSecondTask);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-move")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("move-right")).Clicks();
            //Confirm Task is moved
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[3]/div[1]"), "Task 3"));
            Assert.AreEqual("Task 3", PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[3]/div[1]")).Text);


            //Move Task to the left
            action = new Actions(PropertiesCollection.driver).ContextClick(btnSecondTask);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-move")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("move-left")).Clicks();
            //Confirm Task is moved
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[2]/div[1]"), "Task 3"));
            Assert.AreEqual("Task 3", PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[2]/div[1]")).Text);


            //Delete Task
            action = new Actions(PropertiesCollection.driver).ContextClick(btnSecondTask);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-delete")).Clicks();
            btnDeleteConfirm.Clicks();
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[3]/div[1]"), "Task 4"));
            action = new Actions(PropertiesCollection.driver).ContextClick(btnFirstTask);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-delete")).Clicks();
            btnDeleteConfirm.Clicks();
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[2]/div[1]"), "Task 4"));
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-delete")).Clicks();
            btnDeleteConfirm.Clicks();
            //Confirm that all tasks were deleted
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[2]/div[1]"), "Pipeline End"));
            Assert.AreEqual("Pipeline End", PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[2]/div[1]")).Text);

        }
        public void AddTaskStartPIpeline()
        {
            //Open New Task Window
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            Actions action = new Actions(PropertiesCollection.driver).ContextClick(btnStartPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-right")).Clicks();

            //Confirm that dialog is opend
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-new-task/div/div/div[1]/h4/b"), "Configure Task"));
            Assert.AreEqual("Configure Task", PropertiesCollection.driver.FindElement(By.XPath("//app-new-task/div/div/div[1]/h4/b")).Text);
        }
        public void AddTaskEndPIpeline()
        {
            //Open New Task Window
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("pipeline-name"), "Automated Pipeline 1"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("endPipe_label"), "Pipeline End"));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            Actions action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();

            //Confirm that dialog is opend
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-new-task/div/div/div[1]/h4/b"), "Configure Task"));
            Assert.AreEqual("Configure Task", PropertiesCollection.driver.FindElement(By.XPath("//app-new-task/div/div/div[1]/h4/b")).Text);
        }
        public void IncorrectTaskName(string taskName, string projectName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));

            //Save with no name
            //Open New Task Window
            Console.WriteLine("1");
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("pipeline-name"), "Automated Pipeline 1"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("endPipe_label"), "Pipeline End"));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            Actions action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();
            //Save
            btnSaveTask.Clicks();
            //Open Error Message
            btnWarning.Click();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-new-task/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/p"), "Please enter a Task Name."));
            Assert.AreEqual("Please enter a Task Name.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Incorrect Task Name Format
            //Select Project from PLP
            ProjectListPanel pagePLP = new ProjectListPanel();
            pagePLP.SelectProject(projectName);
            //Open Configure Task window and enter erroneous task name
            Console.WriteLine("2");
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("pipeline-name"), "Automated Pipeline 1"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("endPipe_label"), "Pipeline End"));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();
            wait.Until(ExpectedConditions.ElementToBeClickable(txtTaskName));
            txtTaskName.EnterText(taskName);
            //Save
            btnSaveTask.Clicks();
            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message            
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-new-task/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/p"), "Task Name only supports numbers, letters, spaces and underscores."));
            Assert.AreEqual("Task Name only supports numbers, letters, spaces and underscores.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();
                        
            //Select Project
            pagePLP.SelectProject(projectName);
            //Open Configure Task window and enter duplicate task name
            //Create a task
            taskName = "Task 1";
            Console.WriteLine("3");
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("pipeline-name"), "Automated Pipeline 1"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("endPipe_label"), "Pipeline End"));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();
            wait.Until(ExpectedConditions.ElementToBeClickable(txtTaskName));
            txtTaskName.EnterText(taskName);
            //Save
            btnSaveTask.Clicks();
            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message            
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-new-task/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/p"), "Please select a module."));
            Assert.AreEqual("Please select a module.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Duplicate Task Name
            //Select Project
            pagePLP.SelectProject(projectName);
            //Open Configure Task window and enter duplicate task name
            //Create a task
            taskName = "Task 1";
            Console.WriteLine("4");
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("pipeline-name"), "Automated Pipeline 1"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("endPipe_label"), "Pipeline End"));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();
            wait.Until(ExpectedConditions.ElementToBeClickable(txtTaskName));
            txtTaskName.EnterText(taskName);
            btnDimensionsModule.Clicks();
            //Save
            btnSaveTask.Clicks();

            //Confirm New Project Dialog is gone before advancing
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//dx-tab-panel")));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[1]"), taskName));
            PropertiesCollection.driver.Navigate().Refresh();

            //Select Project
            pagePLP.SelectProject(projectName);
            //Open Configure Task window and enter duplicate task name
            Console.WriteLine("5");
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("task-" + taskName + "-Automated Pipeline 1"), taskName));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("pipeline-name"), "Automated Pipeline 1"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("endPipe_label"), "Pipeline End"));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnEndPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();
            wait.Until(ExpectedConditions.ElementToBeClickable(txtTaskName));
            txtTaskName.EnterText(taskName);
            btnDimensionsModule.Clicks();
            //Save
            btnSaveTask.Clicks();
            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-new-task/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/p"), "Task Name in use, please enter another name."));
            Assert.AreEqual("Task Name in use, please enter another name.", txtErrorMsg.Text);
        }
        public void AddTaskTest(string taskName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));

            //Create a task
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("pipeline-name"), "Automated Pipeline 1"));
            wait.Until(ExpectedConditions.ElementToBeClickable(PropertiesCollection.driver.FindElement(By.Id("add-dependent-pipeline"))));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("startPipe_label"), "Pipeline Start"));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("endPipe_label"), "Pipeline End"));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnRunPipeline));
            System.Threading.Thread.Sleep(1000);
            Actions action = new Actions(PropertiesCollection.driver).ContextClick(btnEndPipeline);
            action.Build().Perform();
            PropertiesCollection.driver.FindElement(By.Id("task-add")).Clicks();
            PropertiesCollection.driver.FindElement(By.Id("add-left")).Clicks();

            txtTaskName.EnterText(taskName);
            btnDimensionsModule.Clicks();
            //Save
            btnSaveTask.Clicks();

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//dx-tab-panel")));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[1]"), taskName));
            Assert.AreEqual(taskName, PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[1]")).Text);
        }
        public void PipelineRun(string taskName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));


            //We should create 3 levels of pipeline and then do the following tests
            //Run each pipeline individually
            //Run parent in all children
            //Run middle and parent
            //Run middle and child
            //Run bottom and all parents

            //Create a task
            btnStartPipeline.Clicks();
            txtTaskName.EnterText(taskName);
            btnEXEModule.Clicks();
            txtEXEModule.EnterText("echo Hello World");
            //Save
            btnSaveTask.Clicks();
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[1]/div[1]/div[2]/div[1]"), taskName));
            
            //Add a Child Pipeline
            NewPipeline pageNPL = new NewPipeline();
            pageNPL.AddLowerLevelofPipeline(0, "Automated Pipeline Child");

            //Create a task for 2nd level
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline2));
            btnStartPipeline2.Clicks();
            txtTaskName.EnterText(taskName);
            btnEXEModule.Clicks();
            txtEXEModule.EnterText("echo Hello World");
            //Save
            btnSaveTask.Clicks();
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[2]/div[1]"), taskName));

            //Add a 2nd level Child Pipeline
            pageNPL.AddLowerLevelofPipeline(1, "Automated Pipeline Child 1");

            //Move to the 3rd pipeline
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline3));
            Actions action = new Actions(PropertiesCollection.driver);
            action.SendKeys(Keys.PageDown).Perform();
            wait.Until(ExpectedConditions.ElementToBeClickable(btnStartPipeline3));

            //IWebElement startPipe = PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline"));
            IList<IWebElement> startPipeList = PropertiesCollection.driver.FindElements(By.Id("startPipe"));
            //int iRowsCount = startPipe.FindElements(By.Id("startPipe")).Count;


            //Add Task to the 3rd pipeline
            //btnStartPipeline3.Clicks();
            startPipeList[2].Clicks();
            txtTaskName.EnterText(taskName);
            btnEXEModule.Clicks();
            txtEXEModule.EnterText("echo Hello World");
            ////Save
            //btnSaveTask.Clicks();
            //wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li/div[2]/div[1]/div[1]/div[2]/div[1]"), taskName));


            ////Click Run Pipeline
            //btnRunPipeline.Clicks();
            ////Verify popup
            //wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-run-pipeline/div/div/div[1]/h4/b"), "Run Pipeline"));
            //Assert.AreEqual("Run Pipeline", PropertiesCollection.driver.FindElement(By.XPath("//app-run-pipeline/div/div/div[1]/h4/b")).Text);
            ////Click Ok
            //btnOk.Clicks();

            ////Wait till pipeline runs and confirm success
            //string CurrentTimeStamp = DateTime.Now.ToString("M/d/yyyy H:mm:ss tt");
            //Console.WriteLine(CurrentTimeStamp);

            //var wait2 = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(5));
            //wait2.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[2]/div/div/div[2]/div/table/tbody[1]/tr[1]/td[3]"), "Success"));
            //Assert.AreEqual("Success", PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li[1]/div[2]/div[2]/div/div/div[2]/div/table/tbody[1]/tr[1]/td[3]")).Text);
        }
    }
}
