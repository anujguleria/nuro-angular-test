﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional
{
    class Header
    {
        public Header()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "Log Out")]
        public IWebElement btnLogOut { get; set; }

        [FindsBy(How = How.LinkText, Using = "Admin")]
        public IWebElement btnAdmin { get; set; }

        public void Logout()
        {
            btnLogOut.Clicks();
        }
        public void OpenAdmin()
        {
            //Set Wait Value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));

            //Open Admin Tab
            btnAdmin.Clicks();

            //Confirm Admin Tab is open
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-admin/div/h4[1]"), "User Access"));
            Assert.AreEqual("User Access", PropertiesCollection.driver.FindElement(By.XPath("//app-admin/div/h4[1]")).Text);
        }
    }
}
