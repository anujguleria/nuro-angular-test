﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional
{
    public class CleanupSQL
    {
        public void SeedSQL()
        {
            //Construct Query to check if user exists, if not add it
            string queryString = "SELECT COUNT(*) FROM [dbo].[User] where Email = 'nue.tango.proxy.svc@gfk.com'";
            int queryCount = (ReturnSQL(queryString));
            if (queryCount == 0)
            {
                queryString = "INSERT INTO [dbo].[User] (Email) VALUES ('nue.tango.proxy.svc@gfk.com')";
                ExecuteSQL(queryString);
            }

            //Query to get the UserID
            queryString = "SELECT UserID FROM [dbo].[User] where Email = 'nue.tango.proxy.svc@gfk.com'";
            int userID = (ReturnSQL(queryString));
            //Query GapRoleUser to see if user exists, if not add them as an admin
            queryString = "SELECT COUNT(*) FROM [dbo].[GapRoleUser] where UserID = '" + userID + "'"; ;
            queryCount = (ReturnSQL(queryString));
            if (queryCount == 0)
            {
                queryString = "INSERT INTO [dbo].[GapRoleUser] (GapRoleID, UserID) VALUES (2,'" + userID + "')";
                ExecuteSQL(queryString);                
            }

            //Query to see if there is an Automated Testing group, if not then create one
            queryString = "SELECT COUNT(*) FROM [dbo].[GapGroup] where Name = 'Automated Testing'";
            queryCount = (ReturnSQL(queryString));
            if (queryCount == 0)
            {
                queryString = "INSERT INTO [dbo].[GapGroup] (Name) VALUES ('Automated Testing');";
                ExecuteSQL(queryString);
            }

            //Query to get the GapGroupID
            queryString = "SELECT GapGroupID FROM [dbo].[GapGroup] where Name = 'Automated Testing'";
            int gapgroupID = (ReturnSQL(queryString));
            //Query to see if our user has the Automated Testing Group, if not add it
            queryString = "SELECT COUNT(*) FROM [dbo].[GapGroupUser] where UserID = '" + userID + "' AND GapGroupID = '" + gapgroupID + "'";
            queryCount = (ReturnSQL(queryString));
            if (queryCount == 0)
            {
                queryString = "INSERT INTO [dbo].[GapGroupUser] (GapGroupID, UserID) VALUES ('" + gapgroupID + "','" + userID + "')";
                ExecuteSQL(queryString);
            }
        }
        public void CleanPipeLine(string pipeLineName)
        {
            string queryString = "DELETE FROM [dbo].[Pipeline] WHERE Name = '" + pipeLineName + "'";
            ExecuteSQL(queryString);
        }
        public void CleanProject(string projectName)
        {
            //Delete Pipelines from Project
            //string queryString = "DELETE FROM [dbo].[Pipeline] WHERE (ProjectID in (Select ProjectID FROM [dbo].[Project] WHERE Name = '" + projectName + "')) OR (ParentID in (SELECT [PipeLineID] FROM [dbo].[Pipeline] WHERE ProjectID in (SELECT ProjectID FROM [dbo].[Project] WHERE Name like '" + projectName + "')))";
            string queryString = "DELETE FROM [dbo].[Pipeline] where (ProjectID = (Select projectID FROM [dbo].[Project] where Name like '" + projectName + "')) or (ParentID in (SELECT [PipeLineID] FROM [dbo].[Pipeline] where ProjectID = (Select ProjectID FROM [dbo].[Project] where Name like '" + projectName + "'))) or (ParentID in (SELECT PipelineID FROM [dbo].[Pipeline] where (ParentID in (SELECT [PipeLineID] FROM [dbo].[Pipeline] where ProjectID = (Select ProjectID FROM [dbo].[Project] where Name like '" + projectName + "')))))";
            ExecuteSQL(queryString);
            //Delete Project
            queryString = "DELETE FROM [dbo].[Project] WHERE Name = '" + projectName + "'";
            ExecuteSQL(queryString);
        }

        public void ExecuteSQL(string queryString)
        {
            //string connectionString = "Data Source=nuew-sqaa45.ext.gfk;Initial Catalog=GAP_QAT;Integrated Security=True;Connection Timeout=0;";
            string connectionString = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
            
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("SampleTransaction");

                // Must assign both transaction object and connection
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    command.CommandText = queryString;
                    command.ExecuteNonQuery();

                    // Attempt to commit the transaction.
                    transaction.Commit();

                    Console.WriteLine("Transaction successful!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    Console.WriteLine("  Message: {0}", ex.Message);

                    // Attempt to roll back the transaction.
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                }
            }
        }
        public int ReturnSQL(string queryString)
        {
            //The function will Query a SQL DB and return a single integer value.  Best used to return a count or an ID
            int resultSQL = 0;
            string connectionString = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Must assign connection to Command object for a pending local query
                SqlCommand command = connection.CreateCommand();
                command.Connection = connection;

                try
                {
                    command.CommandText = queryString;
                    resultSQL = (int) command.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    Console.WriteLine("Message: {0}", ex.Message);
                }
                //Console.WriteLine(resultSQL);
                return resultSQL;
            }
        }
    }
}
