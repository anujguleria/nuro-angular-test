﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional
{
    class NewPipeline
    {
        public NewPipeline()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//app-project-view/div/button/span")]
        public IWebElement btnAddPipeline { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-add-new-pipeline/div/div/div[3]/button")]
        public IWebElement btnSavePipeline { get; set; }

        [FindsBy(How = How.ClassName, Using = "icon-warning_30px")]
        public IWebElement btnWarning { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-add-new-pipeline/div/div/div[2]/div/div")]
        public IWebElement txtErrorMsg { get; set; }

        [FindsBy(How = How.Id, Using = "input-field-info-4")]
        public IWebElement txtPipelineName { get; set; }

        public void AddPipeline(string pipelineName)
        {
            //Create a pipeline
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            btnSavePipeline.Clicks();
        }
        public void IncorrectPipelineName(string pipelineName, string projectName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            //Empty Pipeline Name
            //Open Add Pipeline window and save a pipeline with no name
            btnAddPipeline.Clicks();
            btnSavePipeline.Clicks();
            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div")));
            Assert.AreEqual("Please enter a Pipeline Name.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Incorrect Pipeline Name Format
            //Open project
            ProjectListPanel pagePLP = new ProjectListPanel();
            pagePLP.SelectProject(projectName);
            //Open Add Project window and enter erroneous project name
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            btnSavePipeline.Clicks();
            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div")));
            Assert.AreEqual("Pipeline Name only supports numbers, letters,spaces and underscores.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Duplicate Pipeline Name
            //Open project
            pagePLP.SelectProject(projectName);
            //Open Add Pipeline window and enter duplicate project name
            //Create a pipeline
            pipelineName = "Pipeline 1";
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            btnSavePipeline.Clicks();

            //Confirm Add Pipeline Dialog is gone before advancing
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//app-add-new-pipeline")));

            //Enter duplicate name
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//app-project-view/div/button/span")));
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            btnSavePipeline.Clicks();
            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div")));
            Assert.AreEqual("Pipeline Name in use, please enter another name.", txtErrorMsg.Text);
        }
        public void NewPipelineTest(string pipelineName)
        {
            //Create a pipeline
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            btnSavePipeline.Clicks();

            //Confirm Add Pipeline Dialog is gone before advancing
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//app-add-new-pipeline")));

            //Check new Pipeline exists
            //wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-project-view-pipeline/ul/li/div[1]/div[1]")));
            string label = PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/ul/li/div[1]/div[1]")).Text;
            Assert.AreEqual("Pipeline 1", label);
        }
    }
}
