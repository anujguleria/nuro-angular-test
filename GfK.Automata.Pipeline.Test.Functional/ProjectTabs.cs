﻿using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional
{
    class ProjectTabs
    {
        public ProjectTabs()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }
    }
}
