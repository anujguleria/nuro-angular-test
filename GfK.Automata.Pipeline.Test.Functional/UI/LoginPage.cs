﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional.UI
{
    class LoginPage
    {
        public LoginPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "input-field-info-1")]
        public IWebElement txtUserName { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement txtPassword { get; set; }

        [FindsBy(How = How.Id, Using = "view-password-button")]
        public IWebElement clkEye { get; set; }

        [FindsBy(How = How.Id, Using = "remember-me-check")]
        public IWebElement chkRememberMe { get; set; }

        [FindsBy(How = How.LinkText, Using = "Login")]
        public IWebElement btnLogin { get; set; }

        [FindsBy(How = How.Id, Using = "warning-icon")]
        public IWebElement btnWarning { get; set; }

        [FindsBy(How = How.Id, Using = "error-message")]
        public IWebElement txtErrorMsg { get; set; }


        public void Login(string userName, string password)
        {
                //UserName
                txtUserName.EnterText(userName);
                //Password
                txtPassword.EnterText(password);
                //Submit
                btnLogin.Clicks();
        }
        public void PasswordTest(string password)
        {
            //Set password
            txtPassword.EnterText(password);

            //Check to see if password is masked
            Assert.AreEqual("password", txtPassword.GetAttribute("type"));
            
            //Click Eye to show password
            clkEye.Clicks();

            //Check to see if password isn't hidden
            Assert.AreEqual("text", txtPassword.GetAttribute("type"));

            //Click Eye to mask password
            clkEye.Clicks();

            //Check to see if password is masked
            Assert.AreEqual("password", txtPassword.GetAttribute("type"));
        }
        public void IncorrectUserAccount(string userName, string password)
        {
            // we have no way to test this local with bypass auth = on because we cant check password and currently
            // create a user for usernames that do not exist 
            if (ConfigurationManager.AppSettings["TestURL"] == "http://localhost:4200")
            {
                return;
            }
            //Login with no credentials
            txtUserName.Clear();
            txtPassword.Clear();
            btnLogin.Clicks();

            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message
            Assert.AreEqual("Username cannot be empty.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Incorrect username/password test
            //UserName
            txtUserName.EnterText(userName);
            //Password
            txtPassword.EnterText(password);
            //Submit
            btnLogin.Clicks();

            //Open Error Message
            btnWarning.Clicks();
            //Check for errors message for incorrect credentials
            Assert.AreEqual("Username or Password incorrect.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();
            
            //Incorrect password testing
            //Set userName to a correct value
            userName = "steven.gaa";
            //Clear Fields
            txtUserName.Clear();
            txtPassword.Clear();
            //UserName
            txtUserName.EnterText(userName);
            //Password
            txtPassword.EnterText(password);
            //Submit
            btnLogin.Clicks();

            //Open Error Message
            btnWarning.Clicks();
            //Check for errors message for incorrect credentials
            Assert.AreEqual("Username or Password incorrect.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();
            
            //Missing password test
            //Set userName to a correct value
            userName = "steven.gaa";
            //Clear Fields
            txtUserName.Clear();
            txtPassword.Clear();
            //UserName
            txtUserName.EnterText(userName);
            //Submit
            btnLogin.Clicks();

            //Open Error Message
            btnWarning.Clicks();
            //Check for errors message for incorrect credentials
            Assert.AreEqual("Password cannot be empty.", txtErrorMsg.Text);
         }
        public void RememberMeTest(string userName, string password)
        {
            //Check to see if Remember Me is selected, if so then unselect
            if (SeleniumMethods.GetText(txtUserName) != "")
            {
                chkRememberMe.Click();
            }
            //UserName
            txtUserName.EnterText(userName);
            //Password
            txtPassword.EnterText(password);
            //Select Remember Me
            chkRememberMe.Click();

            //Submit
            btnLogin.Clicks();

            //Log out of portal
            Header HeaderSection = new Header();
            HeaderSection.Logout();

            //Verify that Username and Password is remembered
            Assert.AreEqual(password, SeleniumMethods.GetText(txtPassword));
            Assert.AreEqual(userName, SeleniumMethods.GetText(txtUserName));
        }
        public void AutoLogin(string userName, string password)
        {
            //UserName
            txtUserName.EnterText(userName);
            //Password
            txtPassword.EnterText(password);
            //Remember Me
            //chkRememberMe.Click();
            //Submit
            btnLogin.Clicks();

            ProjectListPanel pagePLP = new ProjectListPanel();
            pagePLP.Collapse();

            //Navigate to another page
            PropertiesCollection.driver.Navigate().GoToUrl("http://www.google.com");

            //Navigate back to GAP Portal
            PropertiesCollection.driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["TestURL"]);

            //Confirm user is automatically logged back into portal
            Assert.AreEqual("Projects", PropertiesCollection.driver.FindElement(By.Id("projects-title")).Text);

        }
    }

    
}
