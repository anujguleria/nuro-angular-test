﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Configuration;
using GfK.Automata.Pipeline.Test.Functional.UI;
using GfK.Automata.Pipeline.Test.Functional;
using OpenQA.Selenium.Firefox;

namespace GfK.Automata.Pipeline.Test.UI
{
    [TestClass]
    public class UnitTest2
    {
        public void LoginOrProjectOrPipeline(List<string> list)  //Basic steps for most automated test
        {
            //Seed user and group into the DB if they don't exist
            CleanupSQL cleanSQL = new CleanupSQL();
            cleanSQL.SeedSQL();

            //Log into portal
            LoginPage pageLogin = new LoginPage();
            pageLogin.Login(list[0], list[1]);

            if (list.Count > 2)
            {
                //Create new project
                NewProject pageNP = new NewProject();
                pageNP.AddProject(list[2]);

            }

            if (list.Count > 3)
            {
                //Add new pipeline
                NewPipeline pagePipeline = new NewPipeline();
                pagePipeline.AddPipeline(list[3]);
            }
        }
        [TestMethod]
        public void Login_HiddenPassword()  //Verify EYE button can be used to toggle hidden password
        {
            LoginPage pageLogin = new LoginPage();
            pageLogin.PasswordTest("test");
        }
        [TestMethod]
        public void Login_IncorrectUserAccount()  //Verify that incorrect logins give the correct user errors
        {
            LoginPage pageLogin = new LoginPage();
            pageLogin.IncorrectUserAccount("Wrong.Password", "test");
        }
        [TestMethod]
        public void Login_RememberMeTest()  //Verify that Remember Me option on login page works
        {
            //Seed user and group into the DB if they don't exist
            CleanupSQL cleanSQL = new CleanupSQL();
            cleanSQL.SeedSQL();

            LoginPage pageLogin = new LoginPage();
            pageLogin.RememberMeTest("nue.tango.proxy.svc", "Tpqj'fpt");
        }
        [TestMethod]
        public void Login_AutoLogin()  //Verify that if user is already logged in, that portal doesn't force them to log in again
        {
            if (ConfigurationManager.AppSettings["Browser"] != "IE")
            {
                LoginPage pageLogin = new LoginPage();
                pageLogin.AutoLogin("nue.tango.proxy.svc", "Tpqj'fpt");
            }
        }
        [TestMethod]
        public void NewProject_OpenWindow()   //Verify that user can open a new project window
        {
            //Log into portal
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Confirm New Project window can be opened
            NewProject pageNP = new NewProject();
            pageNP.OpenNewProjectWindow();
        }
        [TestMethod]
        public void NewProject_IncorrectName()  //Verify that new project window correctly flags incorrect names
        {
            //Log into portal
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Opens New Project window and test incorrect names
            NewProject pageNP = new NewProject();
            pageNP.IncorrectProjectName("Seed Project 1");
        }
        [TestMethod]
        public void NewProject_AddProject()  //Adds a new project to the portal
        {
            //Log into portal
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Create new project
            NewProject pageNP = new NewProject();
            pageNP.AddNewProject("Automated_Test_Project");
        }
        [TestMethod]
        public void NewProject_Copy()  //Creating a new project as a copy of an existing project
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Copy Project Test
            NewProject pageNP = new NewProject();            
            pageNP.CopyProject("Automated_Test_Project_Copy");
        }
        [TestMethod]
        public void AddPipeline_IncorrectName()  //Test Add Pipeline name format
        {
            //Log into portal then create a project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add new pipeline
            NewPipeline pagePipeline = new NewPipeline();
            pagePipeline.IncorrectPipelineName("Pipeline !", "Automated_Test_Project");
        }
        [TestMethod]
        public void AddDependentPipeline_IncorrectName()  //Test dependent pipeline name format
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);
            
            //Test dependent pipeline name format
            NewPipeline pagePipeline = new NewPipeline();            
            pagePipeline.IncorrectDependentPipelineName("Pipeline !", "Automated_Test_Project");
        }
        [TestMethod]
        public void AddDependentPipeline_New()  //Test adding a dependent pipeline
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Test dependent pipeline 
            NewPipeline pagePipeline = new NewPipeline();
            pagePipeline.NewDependentPipelineTest("Automated Child Pipeline 1");
        }
        [TestMethod]
        public void AddPipeline_New()  //Adds a new pipeline to the portal
        {
            //Log into portal then create a project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add new pipeline
            NewPipeline pagePipeline = new NewPipeline();
            pagePipeline.NewPipelineTest("Automated Pipeline 1");
        }
        [TestMethod]
        public void ConfigureTask_StartPipelineAdd()  //Verify that user can add a task from the Start Pipeline icon
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add Task using Start Icon
            ConfigureTask pageTask = new ConfigureTask();
            pageTask.AddTaskStartPIpeline();
        }
        [TestMethod]
        public void ConfigureTask_EndPipelineAdd()  //Verify that user can add a task from the End Pipeline icon
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add Task using Start Icon
            ConfigureTask pageTask = new ConfigureTask();
            pageTask.AddTaskEndPIpeline();
        }
        [TestMethod, Ignore]
        public void ConfigureTask_IncorrectName()  //Verify that the Configure Task dialog checks name format
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add Task using Start Icon
            ConfigureTask pageTask = new ConfigureTask();
            pageTask.IncorrectTaskName("Task !", "Automated_Test_Project");
        }
        [TestMethod, Ignore]
        public void ConfigureTask_AddTask()  //Verify that the Configure Task dialog checks name format
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add Task using Start Icon
            ConfigureTask pageTask = new ConfigureTask();
            pageTask.AddTaskTest("Task 1");
        }
        [TestMethod, Ignore]
        public void ConfigureTask_RightClickControls()  //Verify that the Configure Task dialog checks name format
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add Task using Start Icon
            ConfigureTask pageTask = new ConfigureTask();
            pageTask.TaskRightClickControls("Task 1");
        }
        [TestMethod, Ignore]
        public void RunPipeline()  //Verify that the Configure Task dialog checks name format
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add Task using Start Icon
            ConfigureTask pageTask = new ConfigureTask();
            pageTask.PipelineRun("Task 1");
        }
        [TestMethod]
        public void AdminWindow_Open()  //Verify that the Configure Task dialog checks name format
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Add Task using Start Icon
            Header pageHDR = new Header();
            pageHDR.OpenAdmin();
        }
        [TestMethod]
        public void PLP_ExpandPanel()  //Verify that user can collapse and expand the Project List Panel
        {
            //Log into portal
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Collapse and Expand the Project List Panel
            ProjectListPanel pagePLP = new ProjectListPanel();
            pagePLP.ExpandCollapseTest();
        }
        [TestMethod]
        public void PLP_ExpandPipelines()  //Verify that user can collapse and expand the Project's pipelines
        {
            //Log into portal then create a project and pipeline within that project
            List<string> theInfo = new List<string>()
            {
                "nue.tango.proxy.svc", "Tpqj'fpt", "Automated_Test_Project", "Automated Pipeline 1"
            };

            LoginOrProjectOrPipeline(theInfo);

            //Collapse and Expand the Project List Panel
            ProjectListPanel pagePLP = new ProjectListPanel();
            pagePLP.ExpandCollapseProjectListTest("Automated_Test_Project", "Automated Pipeline 1");
        }


        [TestInitialize]
        public void Initialize()
        {
            //Cleanup Test Projects
            CleanupSQL cleanSQL = new CleanupSQL();
            cleanSQL.CleanProject("Automated_Test_Project");
            cleanSQL.CleanProject("Automated_Test_Project_Copy");

            // todo - we should probably move this to a base class and figure out a way to pull browser type and url from config file
            switch (ConfigurationManager.AppSettings["Browser"])
            {
                case "Chrome":
                    PropertiesCollection.driver = new ChromeDriver();
                    break;
                case "IE":
                    PropertiesCollection.driver = new InternetExplorerDriver();                    
                    break;
            }
            PropertiesCollection.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            PropertiesCollection.driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["TestURL"]);
            PropertiesCollection.driver.Manage().Window.Maximize();

            //Check how many DIVs are in the header to determine if the user is logged in or not
            IWebElement HeaderDiv = PropertiesCollection.driver.FindElement(By.XPath("//app-header/div/header"));
            int HeaderDivCount = HeaderDiv.FindElements(By.XPath("//app-header/div/header/div")).Count;

            //Check to see if user is logged into the portal already (based on DIV count)
            if (HeaderDivCount > 3)
            {
                //Log out of portal
                Header pageHDR = new Header();
                pageHDR.Logout();
            }
        }
        [TestCleanup]
        public void Cleanup()
        {
            PropertiesCollection.driver.Close();
            PropertiesCollection.driver.Quit();

            //Cleanup Test Projects
            CleanupSQL cleanSQL = new CleanupSQL();
            cleanSQL.CleanProject("Automated_Test_Project");
            cleanSQL.CleanProject("Automated_Test_Project_Copy");
        }

   


    }

}
