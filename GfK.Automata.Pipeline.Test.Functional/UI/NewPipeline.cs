﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional
{
    class NewPipeline
    {
        public NewPipeline()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "add-pipeline-button")]
        public IWebElement btnAddPipeline { get; set; }

        [FindsBy(How = How.Id, Using = "add-dependent-pipeline")]
        public IWebElement btnAddDependentPipeline { get; set; }

        [FindsBy(How = How.XPath, Using = "//app-projects-container/div/div/div/div[2]/div[1]/div/dx-tab-panel/div[2]/div/div/div/div/div/app-project-view/div/ul/app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li/div/button")]
        public IWebElement btnAddDependentPipeline2 { get; set; }

        [FindsBy(How = How.Id, Using = "save-button")]
        public IWebElement btnSavePipeline { get; set; }

        [FindsBy(How = How.Id, Using = "warning-icon")]
        public IWebElement btnWarning { get; set; }

        [FindsBy(How = How.Id, Using = "warning-message")]
        public IWebElement txtErrorMsg { get; set; }

        [FindsBy(How = How.Id, Using = "input-field-info-4")]
        public IWebElement txtPipelineName { get; set; }

        public void AddPipeline(string pipelineName)
        {
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            //Add a pipeline
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddPipeline));
            // todo - something is blocking this button sometime right away, going to add a second of wait to work around it for now
            System.Threading.Thread.Sleep(1000);
            btnAddPipeline.Clicks();
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            txtPipelineName.EnterText(pipelineName);
            //Save
            btnSavePipeline.Clicks();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-project-view-pipeline")));
        }
        public void IncorrectPipelineName(string pipelineName, string projectName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            //Empty Pipeline Name
            //Open Add Pipeline window and save a pipeline with no name
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddPipeline));
            // todo - something is blocking this button sometime right away, going to add a second of wait to work around it for now
            System.Threading.Thread.Sleep(1000);
            btnAddPipeline.Clicks();
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();
            //Open Error Message
            wait.Until(ExpectedConditions.ElementToBeClickable(btnWarning));
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div"), "Please enter a Pipeline Name."));
            Assert.AreEqual("Please enter a Pipeline Name.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Incorrect Pipeline Name Format
            //Open project
            ProjectListPanel pagePLP = new ProjectListPanel();
            pagePLP.SelectProject(projectName);
            //Open Add Project window and enter erroneous project name
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddPipeline));
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();
            //Open Error Message
            wait.Until(ExpectedConditions.ElementToBeClickable(btnWarning));
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div"), "Pipeline Name only supports numbers, letters, spaces and underscores."));
            Assert.AreEqual("Pipeline Name only supports numbers, letters, spaces and underscores.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Duplicate Pipeline Name
            //Open project
            pagePLP.SelectProject(projectName);
            //Open Add Pipeline window and enter duplicate project name
            //Create a pipeline
            pipelineName = "Automated Pipeline 1";
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddPipeline));
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();

            //Confirm Add Pipeline Dialog is gone before advancing
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("pipeline-name"), pipelineName));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("add-pipeline-button")));
            PropertiesCollection.driver.Navigate().Refresh();
            pagePLP.SelectProject(projectName);

            //Enter duplicate name
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddPipeline));
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();
            //Open Error Message
            wait.Until(ExpectedConditions.ElementToBeClickable(btnWarning));
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div"), "Pipeline Name in use, please enter another name."));
            Assert.AreEqual("Pipeline Name in use, please enter another name.", txtErrorMsg.Text);
        }
        public void NewPipelineTest(string pipelineName)
        {
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30));
            //Create a pipeline

            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddPipeline));
            btnAddPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();

            //Confirm Add Pipeline Dialog is gone before advancing
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-project-view-pipeline")));

            //Check new Pipeline exists
            //Console.WriteLine(PropertiesCollection.driver.FindElement(By.Id("pipeline-name")).Text);
            Assert.AreEqual(pipelineName, PropertiesCollection.driver.FindElement(By.Id("pipeline-name")).Text);
        }
        public void IncorrectDependentPipelineName(string pipelineName, string projectName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            //Empty Pipeline Name
            //Open Add Pipeline window and save a pipeline with no name
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddDependentPipeline));
            btnAddDependentPipeline.Clicks();
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();
            //Open Error Message
            wait.Until(ExpectedConditions.ElementToBeClickable(btnWarning));
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div"), "Please enter a Pipeline Name."));
            Assert.AreEqual("Please enter a Pipeline Name.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Incorrect Pipeline Name Format
            //Open project
            ProjectListPanel pagePLP = new ProjectListPanel();
            pagePLP.SelectProject(projectName);
            //Open Add Project window and enter erroneous project name
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddDependentPipeline));
            btnAddDependentPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();
            //Open Error Message
            wait.Until(ExpectedConditions.ElementToBeClickable(btnWarning));
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div"), "Pipeline Name only supports numbers, letters, spaces and underscores."));
            Assert.AreEqual("Pipeline Name only supports numbers, letters, spaces and underscores.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Duplicate Pipeline Name
            //Open project
            pagePLP.SelectProject(projectName);
            //Open Add Pipeline window and enter duplicate project name
            //Create a pipeline
            pipelineName = "Automated Child Pipeline 1";
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddDependentPipeline));
            btnAddDependentPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();

            //Confirm Add Pipeline Dialog is gone before advancing
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li/div[1]"), pipelineName));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("add-pipeline-button")));
            PropertiesCollection.driver.Navigate().Refresh();
            pagePLP.SelectProject(projectName);

            //Enter duplicate name
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddDependentPipeline));
            btnAddDependentPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();
            //Open Error Message
            wait.Until(ExpectedConditions.ElementToBeClickable(btnWarning));
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//app-add-new-pipeline/div/div/div[2]/div/div"), "Pipeline Name in use, please enter another name."));
            Assert.AreEqual("Pipeline Name in use, please enter another name.", txtErrorMsg.Text);
        }
        public void NewDependentPipelineTest(string pipelineName)
        {
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30));
            //Create a pipeline
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddPipeline));
            btnAddDependentPipeline.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();

            //Confirm Add Pipeline Dialog is gone before advancing
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-project-view-pipeline")));

            //Check new Pipeline exists
            Assert.AreEqual(pipelineName, PropertiesCollection.driver.FindElement(By.XPath("//app-project-view-pipeline/li[2]/ul/app-project-view-pipeline/li/div[1]/div[1]")).Text);
        }
        public void AddLowerLevelofPipeline(int pipeLevel, string pipelineName)
        {
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30));

            //Create a child pipeline
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddPipeline));
            IList<IWebElement> childPipelineList = PropertiesCollection.driver.FindElements(By.Id("add-dependent-pipeline"));
            childPipelineList[pipeLevel].Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-project-view-pipeline")));
        }
        public void Add3rdLevelofPipeline(string pipelineName)
        {
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30));
            //Add Second Child
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-project-view-pipeline")));
            wait.Until(ExpectedConditions.ElementToBeClickable(btnAddDependentPipeline2));
            btnAddDependentPipeline2.Clicks();
            txtPipelineName.EnterText(pipelineName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSavePipeline));
            btnSavePipeline.Clicks();

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-project-view-pipeline")));
        }
    }
    }
