﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional
{
    class ProjectListPanel
    {
        public ProjectListPanel()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "collapse-button")]
        public IWebElement btnCollapse { get; set; }

        [FindsBy(How = How.Id, Using = "expand-button")]
        public IWebElement btnExpand { get; set; }

        [FindsBy(How = How.Id, Using = "projects-title")]
        public IWebElement txtPLPLabel { get; set; }



        public void Collapse()
        {
            //Collapse Project Panel
            btnCollapse.Clicks();
        }
        public void SelectProject(string projectName)
        {
            //Select project from Project List Panel
            //Read PLP into the PLPList
            IWebElement PLPTable = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul"));
            //IList<IWebElement> PLPList = PLPTable.FindElements(By.ClassName("pull-left"));
            int iRowsCount = PLPTable.FindElements(By.XPath("//app-projects-panel/ul/li/div")).Count;

            //Create count variable
            int number = 1;

            //Create variable and set it  to the first item on the list
            String PLPLabel = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul/li[" + number + "]/div[2]")).Text;

            while (number <= iRowsCount)
            {
                //Compare List item against new project name.  If they match quit the loop
                if (PLPLabel == projectName)
                {
                    break;
                }
                // Add one to number.
                number = number+2;
                PLPLabel = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul/li[" + number + "]/div[2]")).Text;
            }

            IWebElement openProject = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul/li[" + number + "]/div[2]"));
            openProject.Clicks();

            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("project-" + projectName), projectName));

        }
        public void ExpandCollapseTest() //Confirms that user can expand and collapse the PLP

        {
            //Collapse Project Panel
            btnCollapse.Clicks();
            //Confirm that panel is collapsed by looking for Panel Label
            Assert.AreEqual("", txtPLPLabel.Text);

            //Expand Project Panel
            btnExpand.Clicks();

            //Confirm that Project Panel is expanded by looking for Panel label
            Assert.AreEqual("Projects", txtPLPLabel.Text);
        }

        public void ExpandCollapseProjectListTest(string projectName,string pipeName)

        {

            //Select project from Project List Panel
            //Read PLP into the PLPList
            IWebElement PLPTable = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul"));
            //IList<IWebElement> PLPList = PLPTable.FindElements(By.ClassName("pull-left"));
            int iRowsCount = PLPTable.FindElements(By.XPath("//app-projects-panel/ul/li/div")).Count;

            for (int i = 1; i <= iRowsCount; i+=2)
            {
                int j = i + 1;
                String PLPLabel = "";
                PLPLabel = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul/li[" + i + "]/div[2]")).Text;
                //Compare List item against new project name.  If they match quit the loop
                if (PLPLabel == projectName)
                {
                    IWebElement PLP_Folder = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul/li[" + i + "]/div[1]"));

                    //Collapse Project Panel
                    PLP_Folder.Clicks();
                    string PLP_PipeName = PropertiesCollection.driver.FindElement(By.XPath("/html/body/app-root/div/div/app-projects-container/div/div/div/div[1]/div[2]/app-projects-panel/ul/li[" + j + "]/ul/li/app-proejct-panel-pipeline/ul/li/div[2]")).Text;
                    Assert.AreEqual(pipeName, PLP_PipeName);
                    break;
                }
            }
        }
    }
}

