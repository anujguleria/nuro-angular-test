﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GfK.Automata.Pipeline.Test.Functional
{
    public static class SeleniumMethods
    {
        ///<summary>
        ///Extended method for enterting text into a control
        ///<param name="element"></param>
        ///<param name="value"></param>
        ///</summary>
        ///Enter Text
        public static void EnterText(this IWebElement element, string value)
        {
            element.Clear();
            element.SendKeys(value);
        }

        ///<summary>
        ///Click into a button, options, or checkbox
        ///<param name="element"></param>
        ///</summary>
        public static void Clicks(this IWebElement element)
        {
            //Add wait to all button clicks
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
            element.Click();
        }

        ///<summary>
        ///Selecting a dropdown control
        ///<param name="element"></param>
        ///<param name="value"></param>
        ///</summary>
        public static void SelectDropdown(this IWebElement element, string value)
        {
            new SelectElement(element).SelectByText(value);
        }

        //Returns text from a textfield
        public static string GetText(IWebElement element)
        {
            return element.GetAttribute("value");
        }

        //Returns value from a dropdown
        public static string GetTextDD(IWebElement element)
        {
            return new SelectElement(element).AllSelectedOptions.SingleOrDefault().Text;
        }
        public static String GetTimestamp(this DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssfff");
        }
    }
}
