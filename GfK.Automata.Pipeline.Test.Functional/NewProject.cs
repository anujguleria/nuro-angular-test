﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Functional
{
    class NewProject
    {
        public NewProject()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "new-project-button")]
        public IWebElement btnNewProject { get; set; }

        [FindsBy(How = How.Id, Using = "copy-dropdown")]
        public IWebElement ddpCopy { get; set; }

        [FindsBy(How = How.Id, Using = "save-project-button")]
        public IWebElement btnSaveProject { get; set; }

        [FindsBy(How = How.Id, Using = "warning-icon")]
        public IWebElement btnWarning { get; set; }

        [FindsBy(How = How.Id, Using = "error-message")]
        public IWebElement txtErrorMsg { get; set; }

        [FindsBy(How = How.Id, Using = "input-field-info-4")]
        public IWebElement txtProjectName { get; set; }

        public void AddProject(string projectName)
        {
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30));
            //Open New Project Windoww
            wait.Until(ExpectedConditions.ElementToBeClickable(btnNewProject));
            btnNewProject.Clicks();
            //Enter project name
            txtProjectName.EnterText(projectName);
            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSaveProject));
            btnSaveProject.Clicks();
        }
        public void OpenNewProjectWindow()
        {
            //Open New Project window
            btnNewProject.Clicks();

            //Confirm New Project window is open
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-new-project/div/div/div[1]/h4/b")));
            Assert.AreEqual("New Project", PropertiesCollection.driver.FindElement(By.XPath("//app-new-project/div/div/div[1]/h4/b")).Text);
        }
        public void IncorrectProjectName(string projectName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            //Empty Project Name
            //Open Add Project window and save a project with no name
            btnNewProject.Clicks();
            btnSaveProject.Clicks();
            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("error-message"), "Please enter a Project Name."));
            Assert.AreEqual("Please enter a Project Name.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Incorrect Project Name Format
            //Open Add Project window and enter erroneous project name
            btnNewProject.Clicks();
            txtProjectName.EnterText(projectName);
            //Save
            btnSaveProject.Clicks();
            //Open Error Message
            btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("error-message"), "Project Name only supports numbers, letters, and underscores."));
            Assert.AreEqual("Project Name only supports numbers, letters, and underscores.", txtErrorMsg.Text);
            PropertiesCollection.driver.Navigate().Refresh();

            //Duplicate Project Name
            //Open Add Project window and enter duplicate project name
            //Create a project
            projectName = "Automated_Test_Project";
            btnNewProject.Clicks();
            txtProjectName.EnterText(projectName);
            //Save
            btnSaveProject.Clicks();

            //Confirm New Project Dialog is gone before advancing
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//dx-tab-panel")));
            PropertiesCollection.driver.Navigate().Refresh();

            //Enter duplicate name
            //wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("btn-primary")));
            btnNewProject.Clicks();
            txtProjectName.EnterText(projectName);
            //Save
            btnSaveProject.Clicks();
            //Open Error Message
             btnWarning.Clicks();
            //Check for missing credentials error message
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("error-message"), "Project Name in use, please enter another name."));
            Assert.AreEqual("Project Name in use, please enter another name.", txtErrorMsg.Text);
        }
        public void AddNewProject(string projectName)
        {
            //Open New Project window
            btnNewProject.Clicks();
            //Enter Project Name
            txtProjectName.EnterText(projectName);
            //Save
            btnSaveProject.Clicks();

            //Read PLP into the PLPList
            IWebElement PLPTable = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul"));
            int iRowsCount = PLPTable.FindElements(By.XPath("//app-projects-panel/ul/li/div")).Count;

            //Create count variable
            int number = 1;

            //Create variable and set it  to the first item on the list
            String PLPLabel = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul/li[" + number + "]/div[2]")).Text;

            while (number <= iRowsCount)
            {
                //Compare List item against new project name.  If they match quit the loop
                if (PLPLabel == projectName)
                {
                    Console.WriteLine(PLPLabel + " " + number + " " + projectName);
                    break;
                }
                // Add one to number.
                number = number+2;
                PLPLabel = PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul/li[" + number + "]/div[2]")).Text;
            }
            //Confirm New Project tab is open
            Assert.AreEqual(projectName, PropertiesCollection.driver.FindElement(By.XPath("//dx-tab-panel/div/div/div/div/div")).Text);
            //Confirm New Project is in Project List Panel
            Assert.AreEqual(projectName, PropertiesCollection.driver.FindElement(By.XPath("//app-projects-panel/ul/li[" + number + "]/div[2]")).Text);
        }
        public void CopyProject(string projectName)
        {
            //Set wait value
            var wait = new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromMinutes(1));
            PropertiesCollection.driver.Navigate().Refresh();
            //Open New Project window
            wait.Until(ExpectedConditions.ElementToBeClickable(btnNewProject));
            // todo - something is blocking this button sometime right away, going to add a second of wait to work around it for now
            System.Threading.Thread.Sleep(1000);
            btnNewProject.Clicks();
            //Enter Project Name
            txtProjectName.EnterText(projectName);
            //Select Copy Option
            ddpCopy.Clicks();

            

            //Read PLP into the PLPList
            IWebElement PLPTable = PropertiesCollection.driver.FindElement(By.XPath("//*[@id='copy-dropdown']/div/ul"));
            int iRowsCount = PLPTable.FindElements(By.XPath("//*[@id='copy-dropdown']/div/ul/li")).Count;
            
            //Create count variable
            int number = 1;

            //Create variable and set it  to the first item on the list
            String PLPLabel = PropertiesCollection.driver.FindElement(By.XPath("//*[@id='copy-dropdown']/div/ul/li[" + number + "]/a/span")).Text;

            while (number <= iRowsCount)
            {
                //Compare List item against new project name.  If they match quit the loop
                if (PLPLabel == "Automated_Test_Project")
                {
                    break;
                }
                // Add one to number.
                number++;
                PLPLabel = PropertiesCollection.driver.FindElement(By.XPath("//*[@id='copy-dropdown']/div/ul/li[" + number + "]/a/span")).Text;
            }
            //Click Copy Option
            PropertiesCollection.driver.FindElement(By.XPath("//*[@id='copy-dropdown']/div/ul/li[" + number + "]/a/span")).Clicks();

            Console.WriteLine(ddpCopy.Text);

            //Save
            wait.Until(ExpectedConditions.ElementToBeClickable(btnSaveProject));
            System.Threading.Thread.Sleep(1000);
            btnSaveProject.Clicks();

            //Confirm New Project tab is open
            //Assert.AreEqual(projectName, PropertiesCollection.driver.FindElement(By.XPath("//dx-tab-panel/div/div/div/div[2]/div")).Text);
            Assert.AreEqual(projectName, PropertiesCollection.driver.FindElement(By.XPath("//dx-tab-panel/div/div/div/div/div")).Text);
            //Confirm New Project has copied pipeline
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//app-project-view-pipeline")));            
            Assert.AreEqual("Automated Pipeline 1", PropertiesCollection.driver.FindElement(By.Id("pipeline-name")).Text);
        }

    }
}
