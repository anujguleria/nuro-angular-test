﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class ModuleGroup
    {
        public int ModuleGroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
