﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    [Serializable(), JsonObject]
    public class PipelineTaskInputValue
    {
        public int PipelineTaskInputValueID { get; set; }
        public int PriorPipelineTaskID { get; set; }
        public int PriorPipeLineTaskParameterID { get; set; }
        public int ParameterID { get; set;}
        public int PipelineTaskID { get; set; }
        public int PipelineID { get; set; }
        public int PriorPipelineID { get; set; }
    }
}
