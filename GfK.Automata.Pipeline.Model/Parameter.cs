﻿using GfK.Automata.Pipeline.Model.Domains;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    [Serializable(), JsonObject]
    public class Parameter
    {
        private ParameterDirection _parameterDirection;

        public int ParameterID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Direction
        {
            get
            {
                return char.ToUpper(this._parameterDirection.ToString()[0]) + this._parameterDirection.ToString().Substring(1).ToLower();
            }
            set
            {
                if (!Enum.TryParse(value.ToUpper(), out _parameterDirection))
                {
                    throw new InvalidCastException(value.ToString() + " is not a valid parameter direction.");
                }
            }
        }
        public int SequencePosition { get; set; }
        public int ParameterTypeID { get; set; }
        public virtual ParameterType ParameterType { get; set; }
        public ICollection<UserInputValue> UserInputValues { get; set; }
        public ICollection<PipelineTaskInputValue> PipelineTaskInputValues { get; set; }
        public int ModuleID { get; set; }
    }
}
