﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class PipelineRunRequest
    {
        private RunPipelineContext _runPipelineContext;

        public int PipelineRunRequestID { get; set; }
        public int PipelineID { get; set; }
        public string RunPipelineContext
        {
            get
            {
                return char.ToUpper(this._runPipelineContext.ToString()[0]) + this._runPipelineContext.ToString().Substring(1).ToLower();
            }
            set
            {
                if (!Enum.TryParse(value.ToUpper(), out _runPipelineContext))
                {
                    throw new InvalidCastException(value.ToString() + " is not a valid run pipeline context.");
                }
            }
        }
        public DateTime RunRequestDate { get; set; }
    }
}
