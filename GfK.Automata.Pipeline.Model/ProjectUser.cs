﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class ProjectUser
    {
        public int ProjectID { get; set; }
        public int UserID { get; set; }
        public DateTime LastAccess { get; set; }
        public User User { get; set; }
    }
}
