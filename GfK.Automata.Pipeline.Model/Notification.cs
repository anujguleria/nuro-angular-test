﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class Notification
    {
        public int NotificationID { get; set; }
        public Pipeline Pipeline { get; set; }
        public int PipelineID { get; set; }
        public string Message { get; set; }
        public int UserID { get; set; }
        public User user { get; set; }
        public DateTime Timestamp { get; set; }
        public string ProjectName { get; set; }
        public string PipelineName { get; set; }
    }
}
