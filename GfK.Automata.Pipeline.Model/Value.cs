﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class Value
    {
        public int ValueID { get; set; }
        public string InputValue { get; set; }
        public int? InputPriorTaskValueID { get; set; }
        public int ParameterID { get; set; }
        public int PipelineTaskID { get; set; }
    }
}
