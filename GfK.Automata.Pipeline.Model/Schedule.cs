﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class Schedule
    {
        public int ScheduleID { get; set; }
        public virtual Pipeline Pipeline { get; set; }
        public DateTime StartTime { get; set; }
        public bool Enabled { get; set; }
        public ScheduleRecurrence Recurrence { get; set; }
        public string TimezoneAbbreviation { get; set; }
        public int TimezoneOffset { get; set; }
        public string TimeZoneID { get; set; }
        public DailyRecurrenceType DailyRecurrenceType { get; set; }
        public int RecurEvery { get; set; }
        public bool OnMonday { get; set; }
        public bool OnTuesday { get; set; }
        public bool OnWednesday { get; set; }
        public bool OnThursday { get; set; }
        public bool OnFriday { get; set; }
        public bool OnSaturday { get; set; }
        public bool OnSunday { get; set; }
        public MonthlyRecurrenceType MonthlyRecurrenceType { get; set; }
        public byte DayOfWeekOffset { get; set; }
        public byte DayOfMonth { get; set; }
        public YearlyRecurrenceType YearlyRecurrenceType { get; set; }
        public byte MonthOfYear { get; set; }
        public DateTime? NextFireTime { get; set; }
    }
    public enum ScheduleRecurrence : byte
    {
        None = 0,
        Daily = 1,
        Weekly = 2,
        Monthly = 3,
        Yearly = 4
    }
    public enum DailyRecurrenceType : byte
    {
        None = 0,
        EveryXDays = 1,
        EveryWeekday = 2
    }
    public enum MonthlyRecurrenceType : byte
    {
        None = 0,
        SpecificDayOfMonth = 1,
        XDayOfWeekOfMonth = 2
    }
    public enum YearlyRecurrenceType : byte
    {
        None = 0,
        SpecificDayOfMonth = 1,
        XDayOfWeekOfMonth = 2
    }
    public enum DayOfWeek : byte
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7
    }
}
