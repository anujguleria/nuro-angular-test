﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class Module
    {
        private ModuleType _moduleType;
        public int ModuleID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AssemblyName { get; set; }
        public string TypeName { get; set; }
        public string ModuleType
        {
            get
            {
                return char.ToUpper(this._moduleType.ToString()[0]) + this._moduleType.ToString().Substring(1).ToLower();
            }
            set
            {
                if (!Enum.TryParse(value.ToUpper(), out _moduleType))
                {
                    throw new InvalidCastException(value.ToString() + " is not a valid module type.");
                }
            }
        }
        public int ModuleGroupID { get; set; }
        public virtual ModuleGroup ModuleGroup { get; set; }
        public virtual ICollection<Parameter> Parameters { get; set; }
        public ICollection<PipelineTask> PipelineTasks { get; set; }
    }
}
