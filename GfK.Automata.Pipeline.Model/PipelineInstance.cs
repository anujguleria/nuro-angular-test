﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model.Domains;

namespace GfK.Automata.Pipeline.Model
{
    public class PipelineInstance : EntityInstanceStatus
    {
        public PipelineInstance()   :base(" is not a valid Pipeline Instance status.")
        {

        }

        public int PipelineInstanceId { get; set; }
        public int PipelineId { get; set; }
        public Model.Pipeline Pipeline { get; set; }
        public virtual ICollection<PipelineTaskInstance> PipelineTaskInstances { get; set; }
    }
}
