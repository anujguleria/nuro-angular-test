﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model.Domains
{
    public enum EntityInstanceStatus
    {
        COMPLETED,
        SUCCESS,
        FAILED,
        RUNNING,
        CANCELED
    }
}
