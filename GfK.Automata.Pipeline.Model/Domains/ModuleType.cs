﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model.Domains
{
    public enum ModuleType
    {
        BUILTIN,
        CUSTOM,
        SYSTEM,
        PIPELINE
    }

    public enum PipelineNotificationSettingsType
    {
        SUCCESS = 1 ,
        FAILURE = 2,
        BOTH = 3
    }
}
