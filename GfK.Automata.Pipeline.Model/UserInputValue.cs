﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    [Serializable(), JsonObject]
    public class UserInputValue
    {
        public int UserInputValueID { get; set; }
        public string InputValue { get; set; }
        public int ParameterID { get; set; }
        public int PipelineTaskID { get; set; }
    }
}
