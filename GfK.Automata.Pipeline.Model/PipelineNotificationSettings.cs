﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class PipelineNotificationSetting
    {
        public int PipelineNotificationSettingID { get; set; }
        public int PipelineID { get; set; }
        public int UserID { get; set; }
        public PipelineNotificationSettingsType PipelineNotificationSettingType { get; set; }
        public virtual User User { get; set; }
        public virtual Pipeline Pipeline { get; set; }
    }
}
