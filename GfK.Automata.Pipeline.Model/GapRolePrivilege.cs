﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class GapRolePrivilege
    {
        public string Name { get; set; }
        public int GapRolePrivilegeID { get; set; }
        public virtual ICollection<GapRole> GapRoles { get; set; }
    }
}
