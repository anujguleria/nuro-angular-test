﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class User
    {
        public User()
        {
            this.projectUsers = new List<ProjectUser>(); // When no project users = empty list vs. null
        }
        public int UserID { get; set; }
        public string Email { get; set; }

        public ICollection<ProjectUser> projectUsers { get; set; }
        public ICollection<GapGroupUser> GapGroupUsers { get; set; }
        public ICollection<GapRoleUser> GapRoleUsers { get; set; }
        public ICollection<Notification> notifications { get; set; }
    }
}
