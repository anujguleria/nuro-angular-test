﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class PipelineTaskInstance : EntityInstanceStatus
    {
        public PipelineTaskInstance() : base(" is not a valid Pipeline Task Instance status.")
        {

        }
        public int PipelineTaskInstanceId { get; set; }
        public int PipelineTaskID { get; set; }
        public PipelineTask PipelineTask { get; set; }
        public int PipelineInstanceId { get; set; }
        public PipelineInstance PipelineInstance { get; set; }
        public ICollection<PipelineTaskInstanceModuleLog> PipelineTaskInstanceModuleLogs { get; set; }
    }
}
