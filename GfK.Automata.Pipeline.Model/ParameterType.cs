﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    [Serializable(), JsonObject]
    public class ParameterType
    {
        public int ParameterTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //public ICollection<Parameter> Parameters { get; set; }
    }
}
