﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model.Views
{
    public class PipelineHierarchy
    {
        public string Name { get; set; }
        public int PipelineID { get; set; }
        public int HierarchyPipelineID { get; set; }
        public int? Schedule_ScheduleID { get; set; }
        public int TreeLevel { get; set; }
    }
}
