﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model.Views
{
    public class ProjectPipelineHierarchy
    {
        public int PipeLineID { get; set; }
        public string Name { get; set; }
        public int? ProjectID { get; set; }
        public int? ParentID { get; set; }
    }
}
