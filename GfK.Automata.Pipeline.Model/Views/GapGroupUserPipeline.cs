﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model.Views
{
    public class GapGroupUserPipeline
    {
        public int UserID { get; set; }
        public int PipeLineID { get; set; }
        public int GapGroupID { get; set; }
        public int ProjectID { get; set; }
    }
}
