﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class GapRole
    {
        public string Name { get; set; }
        public int GapRoleID { get; set; }
        public ICollection<GapRoleUser> GapRoleUsers { get; set; }
        public ICollection<GapRolePrivilege> GapRolePrivileges { get; set; }
    }
}
