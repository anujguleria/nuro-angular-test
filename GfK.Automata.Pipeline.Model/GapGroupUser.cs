﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class GapGroupUser
    {
        public int GapGroupID { get; set; }
        public int UserID { get; set; }
        public virtual GapGroup GapGroup { get; set; }
        public virtual User User { get; set; }
    }
}
