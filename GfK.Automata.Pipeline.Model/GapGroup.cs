﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class GapGroup
    {
        public string Name { get; set; }
        public int GapGroupID { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<GapGroupUser> GapGroupUsers { get; set; }
    }
}