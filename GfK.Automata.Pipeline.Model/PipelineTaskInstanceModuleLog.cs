﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class PipelineTaskInstanceModuleLog
    {
        private Domains.ModuleLogMessageType _logMessageType;

        public int PipelineTaskInstanceModuleLogID { get; set; }
        public DateTime LogDate { get; set; }
        public string LogMessageType
        {
            get
            {
                return char.ToUpper(this._logMessageType.ToString()[0]) + this._logMessageType.ToString().Substring(1).ToLower();
            }
            set
            {
                if (!Enum.TryParse(value.ToUpper(), out _logMessageType))
                {
                    throw new InvalidCastException(value.ToString() + " is not a valid message type.");
                }
            }
        }
        public string LogMessage { get; set; }
        public int PipelineTaskInstanceId { get; set; }
    }
}
