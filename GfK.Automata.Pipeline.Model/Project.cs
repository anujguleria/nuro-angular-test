﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class Project
    {
        public Project()
        {
            this.projectUsers = new List<ProjectUser>(); // When no project users = empty list vs. null
        }
        public string Name { get; set; }
        public int ProjectID { get; set; }
        public int GapGroupID { get; set; }
        public GapGroup GapGroup { get; set; }
        public ICollection<ProjectUser> projectUsers { get; set; }
        public ICollection<Model.Pipeline> pipeLines { get; set; }
    }
}
