﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public abstract class EntityInstanceStatus
    {
        private readonly string _statusMessageSuffix;

        public EntityInstanceStatus(string statusMessageSuffix  = " is not a valid status.")
        {
            _statusMessageSuffix = statusMessageSuffix;
        }

        public DateTime Started { get; set; }
        public DateTime? Ended { get; set; }

        private Domains.EntityInstanceStatus _status;
        public string Status
        {
            get
            {
                return char.ToUpper(this._status.ToString()[0]) + this._status.ToString().Substring(1).ToLower();
            }
            set
            {
                if (!Enum.TryParse(value.ToUpper(), out _status))
                {
                    throw new InvalidCastException(value.ToString() + _statusMessageSuffix);
                }
            }
        }
        public string Message { get; set; }
    }
}
