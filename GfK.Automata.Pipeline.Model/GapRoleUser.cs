﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class GapRoleUser
    {
        public int GapRoleID { get; set; }
        public int UserID { get; set; }
        public virtual GapRole GapRole { get; set; }
        public virtual User User { get; set; }
    }
}
