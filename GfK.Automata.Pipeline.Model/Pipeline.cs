﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class Pipeline
    {
        public int PipeLineID { get; set; }
        public string Name { get; set; }
        public int? ProjectID { get; set; }
        public int? ParentID { get; set; }
        public int? ModuleID { get; set; }
        public int ParentToPipelineID { get; set; }
        public DateTime? LastRun { get; set; }
        public string LastStatus { get; set; }
        public Project project { get; set; }
        public Module Module { get; set; }
        public ICollection<PipelineInstance> PipelineInstances { get; set; }
        [ForeignKey("ParentID")]
        public virtual ICollection<Pipeline> Pipelines { get; set; }
        public virtual Schedule Schedule { get; set; }
        public ICollection<Notification> notifications { get; set; }
        public ICollection<Parameter> Parameters { get; set; }
        public ICollection<PipelineNotificationSetting> PipelineNotificationSettings { get; set; }
    }
}
