﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Model
{
    public class PipelineTask
    {
        public int PipelineTaskID { get; set; }
        public string Name { get; set; }
        public int SequencePosition { get; set; }
        public int PipeLineID { get; set; }
        public Model.Pipeline Pipeline { get; set; }
        public int ModuleID { get; set; }
        public virtual ICollection<UserInputValue> UserInputValues { get; set; }
        public virtual ICollection<PipelineTaskInputValue> PipelineTaskInputValues { get; set; }    
        public ICollection<PipelineTaskInstance> PipelineTaskInstances { get; set; }
    }
}
