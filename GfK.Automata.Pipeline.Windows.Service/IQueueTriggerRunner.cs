﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Messaging;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public interface IQueueTriggerRunner<T> where T : class
    {
        void MessageAvailable(object source, T result);
        Task MessageAvailableAsync(object source, T result);
    }
}
