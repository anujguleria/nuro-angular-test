﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Data.Service.Messaging;
using System.Configuration;
using GfK.Automata.Pipeline.GapModule;
using GfK.Automata.Pipeline.Model.Domains;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public static class GapQueueStartup
    {
        public static IPipelineQueueRunner InitializeQueueRunner(IScheduleLogger fileLogger)
        {
            Task<IPipelineQueueRunner> queueRunnerRequest = InitializeQueueRunnerAsync(fileLogger);
            queueRunnerRequest.Wait();

            return queueRunnerRequest.Result;
        }

        public static IGapQueueTriggerRunner InitializeGapQueueTriggerRunner(IPipelineQueueRunner pipelineQueueRunner, int jobLimit)
        {
            Task<IGapQueueTriggerRunner> gapQueueTriggerRunnerRequest = InitializeGapQueueTriggerRunnerAsync(pipelineQueueRunner, jobLimit);
            gapQueueTriggerRunnerRequest.Wait();

            return gapQueueTriggerRunnerRequest.Result;
        }

        public static async Task<IGapQueueTriggerRunner> InitializeGapQueueTriggerRunnerAsync(IPipelineQueueRunner pipelineQueueRunner, int jobLimit)
        {
            QueueAttributes queueAttributes = new QueueAttributes();
            queueAttributes.QueueName = ConfigurationManager.AppSettings.Get("PipelineQueueName");
            MsmqQueue queue = new MsmqQueue(queueAttributes);

            IGapQueueTriggerRunner gapQueueTriggerRunner = new GapQueueTriggerRunner(pipelineQueueRunner, queue, jobLimit);
            return gapQueueTriggerRunner;
        }

        public static async Task<IPipelineQueueRunner> InitializeQueueRunnerAsync(IScheduleLogger fileLogger)
        {
            QueueAttributes queueAttributes = new QueueAttributes { QueueName = ConfigurationManager.AppSettings.Get("PipelineQueueName") };
            IGapQueue pipelineQueue = new MsmqQueue(queueAttributes);

            IPipelineTaskService pipelineTaskService = InitializePipelineTaskService();

            IGapMessageQueue pipelineMessageQueue = new PipelineMessageQueue(pipelineQueue, queueAttributes);
            IGapMessageQueue pipelineTaskMessageQueue = new PipelineTaskMessageQueue(pipelineQueue, queueAttributes);

            IGapMessageRunner messageRunner = new GapMessageRunner();
            IGapMessageQueue scheduleUpdateQueue = new ScheduleUpdateMessageQueue(pipelineQueue, queueAttributes);

           
            IPipelineNotificationSettingsService pipelineNotificationSettingsService = InitializePipelineNotificationService();
            INotificationService notificationService = InitializeNotificationService();
            PipelineQueueRunner pipelineQueueRunner = new PipelineQueueRunner(pipelineMessageQueue, pipelineTaskMessageQueue, messageRunner, fileLogger
                ,scheduleUpdateQueue, pipelineNotificationSettingsService, notificationService);

            return pipelineQueueRunner;
        }

        private static INotificationService InitializeNotificationService()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            INotificationRepository notificationRepository = new NotificationRepository(dbFactory);
            IPipelineRepository pipelineRepository = new PipelineRepository(dbFactory);
            IProjectRepository projectRepository = new ProjectRepository(dbFactory);
            INotificationService notificationService = new NotificationService(notificationRepository,unitOfWork, pipelineRepository, projectRepository);
            return notificationService;
        }

        private static IPipelineNotificationSettingsService InitializePipelineNotificationService()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            IPipelineNotificationSettingsRepository pipelineNotificationSettingsRepository = new PipelineNotificationSettingsRepository(dbFactory);
            IPipelineNotificationSettingsService pipelineNotificationSettingsService = new PipelineNotificationSettingsService(unitOfWork, pipelineNotificationSettingsRepository);
            return pipelineNotificationSettingsService;
        }

        public static async Task<IPipelineHierarchyService> InitializePiplineHierarchyService()
        {
            IDbFactory dbFactory = new DBFactory();
            IPipelineHierarchyRepository pipelineHierarchyRepository = new PipelineHierarchyRepository(dbFactory);
            IPipelineHierarchyService pipelineHierarchyService = new PipelineHierarchyService(pipelineHierarchyRepository);

            return pipelineHierarchyService;
        }

        public static async Task<IPipelineRunRequestService> InitializePipelineRunRequestService()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);

            IPipelineRunRequestRepository pipelineRunRequestRepository = new PipelineRunRequestRepository(dbFactory);
            IPipelineRunRequestService pipelineRunRequestService = new PipelineRunRequestService(pipelineRunRequestRepository, unitOfWork);

            return pipelineRunRequestService;
        }

        public static IScheduleService InitializeScheduleService(IGapMessageQueue scheduleUpdateQueue)
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            IScheduleRepository scheduleRepository = new ScheduleRepository(dbFactory);
            IQuartzService quartzService = new QuartzService();
            IScheduleService scheduleService = new ScheduleService(scheduleRepository, unitOfWork, quartzService);
            return scheduleService;
        }

        public static IPipelineTaskService InitializePipelineTaskService()
        {
            Task<IPipelineTaskService> taskServiceRequest = InitializePipelineTaskServiceAsync();
            taskServiceRequest.Wait();

            return taskServiceRequest.Result;
        }

        public static async Task<IPipelineService> InitializePipelineServiceAsync()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            IPipelineInstanceRepository pipelineInstanceRepository = new PipelineInstanceRepository(dbFactory);
            IPipelineInstanceService pipelineInstanceService = new PipelineInstanceService(pipelineInstanceRepository, unitOfWork);

            dbFactory = new DBFactory();
            IProjectPipelineHierarchyRepository projectPipelineHierarchyRepository = new ProjectPipelineHierarchyRepository(dbFactory);
            IProjectPipelineHierarchyService projectPipelineHierarchyService = new ProjectPipelineHierarchyService(projectPipelineHierarchyRepository);

            dbFactory = new DBFactory();
            IGapGroupUserProjectRepository gapGroupUserProjectRepository = new GapGroupUserProjectRepository(dbFactory);
            IGapGroupUserProjectService gapGroupUserProjectService = new GapGroupUserProjectService(gapGroupUserProjectRepository);
            
            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IGapGroupUserRepository gapGroupUserRepository = new GapGroupUserRepository(dbFactory);
            IGapGroupUserService gapGroupUserService = new GapGroupUserService(gapGroupUserRepository, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IGapRoleUserRepository gapRoleUserRepository = new GapRoleUserRepository(dbFactory);
            IGapRoleUserService gapRoleUserService = new GapRoleUserService(gapRoleUserRepository, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IUserRepository userRepository = new UserRepository(dbFactory);
            IUserService userService = new UserService(userRepository, gapRoleUserService, gapGroupUserService, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IProjectUserRepository projectUserRepository = new ProjectUserRepository(dbFactory);
            IProjectUserService projectUserService = new ProjectUserService(projectUserRepository, unitOfWork, userService);

            dbFactory = new DBFactory();
            IGapGroupUserPipelineRepository gapGroupUserPipelineRepository = new GapGroupUserPipelineRepository(dbFactory);
            IGapGroupUserPipelineService gapGroupUserPipelineService = new GapGroupUserPipelineService(gapGroupUserPipelineRepository);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IModuleRepository moduleRepository = new ModuleRepository(dbFactory);
            IModuleService moduleService = new ModuleService(moduleRepository, null, unitOfWork, null);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IPipelineRepository pipelineRepository = new PipelineRepository(dbFactory);
            IPipelineService pipelineService = new PipelineService(pipelineRepository, unitOfWork, pipelineInstanceService, projectPipelineHierarchyService
                ,projectUserService, gapGroupUserPipelineService, gapGroupUserProjectService, moduleService, await InitializePipelineTaskServiceAsync(), null, null);

            return pipelineService;
        }

        public static async Task<IPipelineTaskService> InitializePipelineTaskServiceAsync()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            IPipelineTaskInputValueRepository pipelineTaskInputValueRepository = new PipelineTaskInputValueRepository(dbFactory);
            IPipelineTaskInputValueService pipelineTaskInputValueService = new PipelineTaskInputValueService(pipelineTaskInputValueRepository, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IUserInputValueRepository userInputValueRepository = new UserInputValueRepository(dbFactory);
            IUserInputValueService userInputValueService = new UserInputValueService(userInputValueRepository, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IGapRoleUserRepository gapRoleUserRepository = new GapRoleUserRepository(dbFactory);
            IGapRoleUserService gapRoleUserService = new GapRoleUserService(gapRoleUserRepository, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IGapGroupUserRepository gapGroupUserRepository = new GapGroupUserRepository(dbFactory);
            IGapGroupUserService gapGroupUserService = new GapGroupUserService(gapGroupUserRepository, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IUserRepository userRepository = new UserRepository(dbFactory);
            IUserService userService = new UserService(userRepository, gapRoleUserService, gapGroupUserService, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IProjectUserRepository projectUserRepository = new ProjectUserRepository(dbFactory);
            IProjectUserService projectUserService = new ProjectUserService(projectUserRepository, unitOfWork, userService);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IModuleRepository moduleRepository = new ModuleRepository(dbFactory);
            IModuleService moduleService = new ModuleService(moduleRepository, null, unitOfWork, null);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IPipelineTaskRepository pipelineTaskRepository = new PipelineTaskRepository(dbFactory);
            IPipelineTaskService pipelineTaskService = new PipelineTaskService(pipelineTaskRepository, unitOfWork, pipelineTaskInputValueService, userInputValueService
                , projectUserService);

            return pipelineTaskService;
        }

        public static async Task<IGapPipelineTaskService> InitializeGapPipelineTaskServiceAsync()
        {
            IDbFactory dbFactory = new DBFactory();
            IPipelineTaskRepository pipelineTaskRepository = new PipelineTaskRepository(dbFactory);

            dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            IModuleRepository moduleRepository = new ModuleRepository(dbFactory);
            IModuleService moduleService = new ModuleService(moduleRepository, null, unitOfWork, null);

            IGapPipelineTaskService gapPipelineTaskService = new GapPipelineTaskService(pipelineTaskRepository, moduleService);
            return gapPipelineTaskService;
        }

        public static IPipelineInstanceWriterService InitializePipelineWriter()
        {
            Task<IPipelineInstanceWriterService> instanceWriterServiceRequest = InitializePipelineWriterAsync();
            instanceWriterServiceRequest.Wait();

            return instanceWriterServiceRequest.Result;
        }

        public static async Task<IPipelineInstanceWriterService> InitializePipelineWriterAsync()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);

            IPipelineInstanceRepository pipelineInstanceRepository = new PipelineInstanceRepository(dbFactory);
            IPipelineInstanceService pipelineInstanceService = new PipelineInstanceService(pipelineInstanceRepository, unitOfWork);

            IPipelineInstanceWriterService pipelineInstanceWriter = new PipelineInstanceWriterService(pipelineInstanceService);

            return pipelineInstanceWriter;
        }

        public static IPipelineTaskInstanceWriterService InitializePipelineTaskWriter()
        {
            Task<IPipelineTaskInstanceWriterService> instanceWriterServiceRequest = InitializePipelineTaskWriterAsync();
            instanceWriterServiceRequest.Wait();

            return instanceWriterServiceRequest.Result;
        }

        public static async Task<IPipelineTaskInstanceWriterService> InitializePipelineTaskWriterAsync()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);

            IPipelineTaskInstanceRepository pipelineTaskInstanceRepository = new PipelineTaskInstanceRepository(dbFactory);
            IPipelineTaskInstanceService pipelineTaskInstanceService = new PipelineTaskInstanceService(pipelineTaskInstanceRepository, unitOfWork, null);

            IPipelineTaskInstanceWriterService pipelineTaskInstanceWriter = new PipelineTaskInstanceWriterService(pipelineTaskInstanceService);

            return pipelineTaskInstanceWriter;
        }

        public static async Task<ILogger<ModuleLogMessageType>> GetModuleLogger()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);

            IPipelineTaskInstanceModuleLogRespository pipelineTaskInstanceModuleLogRepository = new PipelineTaskInstanceModuleLogRepository(dbFactory);
            IPipelineTaskInstanceModuleLogService pipelineTaskInstanceModuleLogService = new PipelineTaskInstanceModuleLogService(pipelineTaskInstanceModuleLogRepository, unitOfWork);

            ILogger<ModuleLogMessageType> databaseLogger = new DatabaseLogger(pipelineTaskInstanceModuleLogService);
            return databaseLogger;
        }
    }
}
