﻿using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.GapModule;
using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public class DatabaseLogger : DatabaseLoggerBase, ILogger<ModuleLogMessageType>
    {
        public DatabaseLogger(IPipelineTaskInstanceModuleLogService pipelineTaskInstanceModuleLogService)
            : base(pipelineTaskInstanceModuleLogService) { }
    }
}
