﻿using GfK.Automata.Pipeline.Data.Service.Messaging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public class GapQueueTriggerRunner : IGapQueueTriggerRunner
    {
        private readonly IPipelineQueueRunner _pipelineQueueRunner;
        private readonly MsmqQueue _msmqQueue;
        private readonly SemaphoreSlim _jobLimit;

        public GapQueueTriggerRunner(IPipelineQueueRunner pipelineQueueRunner, MsmqQueue msmqQueue, int jobLimit)
        {
            _pipelineQueueRunner = pipelineQueueRunner;
            _msmqQueue = msmqQueue;

            _msmqQueue.InitializeListener(MessageAvailable).Wait();
            _msmqQueue.StartListening().Wait();

            _jobLimit = new SemaphoreSlim(jobLimit, jobLimit);
        }

        public void MessageAvailable(object source, ReceiveCompletedEventArgs result)
        {
            try
            {
                MessageAvailableAsync(source, result).Wait();
            }
            finally
            {
                _msmqQueue.StartListening().Wait();
            }
        }
        public async Task MessageAvailableAsync(object source, ReceiveCompletedEventArgs result)
        {
            JObject message = await _msmqQueue.RetrieveMessageBodyAsJson(((MessageQueue)source).EndReceive(result.AsyncResult));

            if (await GapQueueMessage.GetMessageType(message) == GapMessageType.PipelineRun)
            {
                await _pipelineQueueRunner.ProcessPendingPipelineRunMessage(message);
            }
            else if (await GapQueueMessage.GetMessageType(message) == GapMessageType.PipelineTaskRun)
            {
                GapQueueTriggerContext runContext = new GapQueueTriggerContext()
                {
                    Message = message,
                    JobLimit = _jobLimit
                };

                ThreadPool.QueueUserWorkItem(new WaitCallback(_pipelineQueueRunner.ProcessPendingPipelineTaskRunMessage), runContext);
            }
            else if (await GapQueueMessage.GetMessageType(message) == GapMessageType.PipelineTaskComplete)
            {
                await _pipelineQueueRunner.ProcessPendingPipelineTaskCompletedMessage(message);
            }
            else if (await GapQueueMessage.GetMessageType(message) == GapMessageType.ScheduleUpdate)
            {
                await _pipelineQueueRunner.ProcessPendingScheduleUpdateMessage(message);
            }
        }
    }
}
