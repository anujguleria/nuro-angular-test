﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.GapModule;
using System.Configuration;
using System.IO;
using GfK.Automata.Pipeline.Model.Domains;
using System.Runtime.Serialization.Formatters.Binary;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public class GapMessageRunner : IGapMessageRunner
    {
        public GapMessageRunner()
        {
        }

        public int TimesRun { get; set; } = 1;
        public bool ThrowErrors { get; set; } = false;

        public async Task<PipelineTask> GetMessagePipelineTask(JObject message)
        {
            IPipelineTaskService pipelineTaskService = await GapQueueStartup.InitializePipelineTaskServiceAsync();
            PipelineTask pipelineTask = await pipelineTaskService.GetPipelineTaskWithParameters(int.Parse(message.Property("ID").Value.ToString()));

            return pipelineTask;
        }

        public async Task SerializeMappedOutputParameters(JObject message, Module taskModule, IParameterValueCollection parameters, string filePrefix)
        {
            string taskParameterPath = ConfigurationManager.AppSettings.Get("PipelineTaskParameterDirectory");

            foreach (KeyValuePair<string, IParameter> parameter in parameters.Parameters)
            {
                string parameterName = parameter.Key;
                Parameter outputParameter = taskModule.Parameters.ToList()
                    .Where(p => p.Name.ToUpper() == parameterName.ToUpper() && p.ModuleID == taskModule.ModuleID && (p.Direction.ToUpper() == "OUT" || p.Direction.ToUpper() == "INOUT")).First();

                if (outputParameter != null)
                {
                    string taskParameterFile = taskParameterPath + filePrefix + "_" + message.Property("ID").Value.ToString() + "_" + outputParameter.ParameterID.ToString() + ".bin";
                    await ((IParameterSerialize)parameters.Parameters[parameterName]).Serialize(taskParameterFile);
                }
            }
        }

        public async Task SerializeBuiltinOutputParameters(JObject message, Module taskModule, IParameterValueCollection parameters)
        {
            await SerializeMappedOutputParameters(message, taskModule, parameters, message.Property("UniqueStamp").Value.ToString());

            PipelineTask pipelineTask = await GetMessagePipelineTask(message);
            string taskParameterPath = ConfigurationManager.AppSettings.Get("PipelineTaskParameterDirectory");

            // TODO - we should probably merge these vs. relying on the previous task to
            // dynamic parameters
            string dynamicParametersFile = taskParameterPath + message.Property("UniqueStamp").Value.ToString() + "_DynamicParameters_" +
                pipelineTask.PipeLineID + ".bin";
            await Serialize(dynamicParametersFile, parameters.DynamicParameters);
        }

        public async Task SerializeSystemEndModuleParameters(JObject message, Module taskModule, IParameterValueCollection parameters)
        {
            IGapPipelineTaskService gapPipelineTaskService = await GapQueueStartup.InitializeGapPipelineTaskServiceAsync();
            Module customTaskModule = await gapPipelineTaskService.GetPipelineTaskModule(int.Parse(message.Property("ResumePipelineTaskID").Value.ToString()));

            message.Property("ID").Value = message.Property("ResumePipelineTaskID").Value;
            message.Property("ParentID").Value = message.Property("ResumePipelineID").Value;

            message.Remove("ResumePipelineTaskID");
            message.Remove("ResumePipelineID");

            await SerializeBuiltinOutputParameters(message, customTaskModule, parameters);
        }

        public async Task SerializeCustomModuleStartTaskParameters(JObject message, Module taskModule, IParameterValueCollection parameters)
        {
            // Serialize the inputs to the custom module/task in the 'primary' pipeline as if emitted by the System Start module/task 
            // in the pipeline implementing the custom module. Then the first 'real' task in the custom module will run next and
            // to extent parameters from System Start are mapped it will find the parameter files as it expects.

            IPipelineService pipelineService = await GapQueueStartup.InitializePipelineServiceAsync();  
            Model.Pipeline customModulePipelineImplementation = await pipelineService.GetModulePipeline(taskModule.ModuleID);

            IPipelineTaskService piplineTaskService = await GapQueueStartup.InitializePipelineTaskServiceAsync();
            int? firstPipelineTaskIDInCustomPipeline = await piplineTaskService.GetFirstPipelineTaskId(customModulePipelineImplementation.PipeLineID);

            IGapPipelineTaskService gapPipelineTaskService = await GapQueueStartup.InitializeGapPipelineTaskServiceAsync();
            Module customTaskModule = await gapPipelineTaskService.GetPipelineTaskModule((int)firstPipelineTaskIDInCustomPipeline);

            if (firstPipelineTaskIDInCustomPipeline != null)
            {
                message.Add("ResumePipelineID", message.Property("ParentID").Value);
                message.Add("ResumePipelineTaskID", message.Property("ID").Value);

                message.Property("ID").Value = (int)firstPipelineTaskIDInCustomPipeline;
                message.Property("ParentID").Value = customModulePipelineImplementation.PipeLineID;
            }

            await SerializeBuiltinOutputParameters(message, customTaskModule, parameters);
        }

        public async Task SerializeOutputParameters(JObject message, Module taskModule, IParameterValueCollection parameters)
        {
            if (taskModule.ModuleType.ToUpper() == ModuleType.BUILTIN.ToString().ToUpper())
            {
                await SerializeBuiltinOutputParameters(message, taskModule, parameters);
            }
            else if (taskModule.ModuleType.ToUpper() == ModuleType.PIPELINE.ToString().ToUpper())
            {
                await SerializeBuiltinOutputParameters(message, taskModule, parameters);
            }
            else if (taskModule.ModuleType.ToUpper() == ModuleType.SYSTEM.ToString().ToUpper())
            {
                await SerializeSystemEndModuleParameters(message, taskModule, parameters);
            }
            else if (taskModule.ModuleType.ToUpper() == ModuleType.CUSTOM.ToString().ToUpper())
            {
                await SerializeCustomModuleStartTaskParameters(message, taskModule, parameters);
            }
        }

        public async Task<IParameterValueCollection> DeserializeMappedInputParameters(PipelineTask pipelineTask, JObject message, Module taskModule, string filePrefix)
        {
            IParameterValueCollection parameters = new ParameterValueCollection();

            string taskParameterPath = ConfigurationManager.AppSettings.Get("PipelineTaskParameterDirectory");
            foreach (PipelineTaskInputValue inputParameter in pipelineTask.PipelineTaskInputValues)
            {
                string taskParameterFile = taskParameterPath + filePrefix + "_" + inputParameter.PriorPipelineTaskID + "_" 
                    + inputParameter.PriorPipeLineTaskParameterID + ".bin";

                if (File.Exists(taskParameterFile))
                {
                    Parameter parameter = taskModule.Parameters.ToList().Where(p => p.ParameterID == inputParameter.ParameterID).First();
                    IParameter inputParameterValue = null;

                    if (String.Equals(parameter.ParameterType.Name, "String", StringComparison.OrdinalIgnoreCase))
                    {
                        inputParameterValue = (IParameter)(await (new ParameterString("", "")).Deserialize(taskParameterFile));
                    }

                    if (String.Equals(parameter.ParameterType.Name, "Script", StringComparison.OrdinalIgnoreCase))
                    {
                        inputParameterValue = (IParameter)(await (new ParameterScript("", "")).Deserialize(taskParameterFile));
                    }

                    if (String.Equals(parameter.ParameterType.Name, "Metadata", StringComparison.OrdinalIgnoreCase))
                    {
                        inputParameterValue = (IParameter)(await (new ParameterMetadata("", "")).Deserialize(taskParameterFile));
                    }
                    if (inputParameterValue != null)
                    {
                        inputParameterValue.Definition = parameter;
                        await parameters.AddParameter(inputParameterValue);
                    }
                }
            }

            return parameters;
        }

        public async Task<int> GetPipelineTaskProjectID(PipelineTask pipelineTask)
        {
            IPipelineService pipelineService = await GapQueueStartup.InitializePipelineServiceAsync();

            return (int)(await pipelineService.GetPipelineProjectID(pipelineTask.PipeLineID));
        }

        public async Task<SystemParameters> GetSystemParameters(string taskParameterPath, PipelineTask pipelineTask, JObject message)
        {
            SystemParameters systemParameters = new SystemParameters();

            string systemParameterFile = taskParameterPath + message.Property("UniqueStamp").Value.ToString() + "_SystemParameters_" +
                pipelineTask.PipeLineID + ".bin";

            if (File.Exists(systemParameterFile))
            {
                systemParameters = (SystemParameters)(await Deserialize(systemParameterFile));
            }
            else
            {
                systemParameters.ProjectID = await GetPipelineTaskProjectID(pipelineTask);

                string systemParametersFile = taskParameterPath + message.Property("UniqueStamp").Value.ToString() + "_SystemParameters_" +
                    pipelineTask.PipeLineID + ".bin";
                await Serialize(systemParametersFile, systemParameters);
            }

            return systemParameters;
        }

        public async Task<IParameterValueCollection> DeserializeInputParameters(JObject message, Module taskModule, string filePrefix)
        {            
            PipelineTask pipelineTask = await GetMessagePipelineTask(message);
            string taskParameterPath = ConfigurationManager.AppSettings.Get("PipelineTaskParameterDirectory");

            IParameterValueCollection parameters = await DeserializeMappedInputParameters(pipelineTask, message, taskModule, filePrefix);
            parameters.SystemParameters = await GetSystemParameters(taskParameterPath, pipelineTask, message);

            // dynamic parameters
            string dynamicParametersFile = taskParameterPath + message.Property("UniqueStamp").Value.ToString() + "_DynamicParameters_" +
                pipelineTask.PipeLineID + ".bin";
            if (File.Exists(dynamicParametersFile))
            {
                parameters.DynamicParameters = (Dictionary<string, string>)(await Deserialize(dynamicParametersFile));
            }
            else
            {
                parameters.DynamicParameters = new Dictionary<string, string>();
            }

            return parameters;
        }
        private async Task Serialize(string path, object value)
        {
            try
            {
                using (Stream serializerStream = File.Open(path, FileMode.Create))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(serializerStream, value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private async Task<object> Deserialize(string path)
        {
            try
            {
                using (Stream deserializerStream = File.Open(path, FileMode.Open))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    object deserialized = formatter.Deserialize(deserializerStream);

                    return deserialized;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IParameterValueCollection> GetUserInputParameterValues(JObject message, Module taskModule)
        {
            IParameterValueCollection parameters = new ParameterValueCollection();

            PipelineTask pipelineTask = await GetMessagePipelineTask(message);
            foreach (UserInputValue inputParameter in pipelineTask.UserInputValues)
            {   
                Parameter parameter = taskModule.Parameters.ToList().Where(p => p.ParameterID == inputParameter.ParameterID).First();
                await parameters.AddParameterValue(taskModule.Name, parameter.Name, parameter.ParameterType.Name, inputParameter.InputValue);
            }

            return parameters;
        }

        public async Task<IParameterValueCollection> GetInputParameters(JObject message, Module taskModule, string filePrefix)
        {
            IParameterValueCollection inputParameters = await GetUserInputParameterValues(message, taskModule);
            IParameterValueCollection mappedParameters = await DeserializeInputParameters(message, taskModule, filePrefix);

            if (inputParameters.Parameters != null && inputParameters.Parameters.Count > 0)
            {
                foreach (IParameter parameter in mappedParameters.Parameters.Values)
                {
                    await inputParameters.AddParameter(parameter);
                }

                inputParameters.SystemParameters = mappedParameters.SystemParameters;
            }
            else
            {
                inputParameters = mappedParameters;
            }

            return inputParameters;
        }

        public async Task<IModule> GetModuleInstance(Module taskModule, JObject message)
        {
            IModule moduleInstance = (IModule)Activator.CreateInstance(taskModule.AssemblyName, taskModule.TypeName).Unwrap();

            ILogger<ModuleLogMessageType> logger = await GapQueueStartup.GetModuleLogger();
            logger.PipelineTaskInstanceID = int.Parse(message.Property("PipelineTaskInstanceID").Value.ToString());            
            moduleInstance.Logger = logger;

            return moduleInstance;
        }

        public async Task<bool> IsModuleCustom(Module taskModule)
        {
            return String.Compare(taskModule.ModuleType, ModuleType.CUSTOM.ToString(), true) == 0 || String.Compare(taskModule.ModuleType, ModuleType.SYSTEM.ToString(), true) == 0;
        }

        public async Task<bool> IsModuleSystemStart(Module taskModule)
        {
            return String.Compare(taskModule.ModuleType, ModuleType.PIPELINE.ToString(), true) == 0 && taskModule.Name.EndsWith("_Start");
        }

        public async Task<bool> IsModuleSystemEnd(Module taskModule)
        {
            return String.Compare(taskModule.ModuleType, ModuleType.PIPELINE.ToString(), true) == 0 && taskModule.Name.EndsWith("_End");
        }

        public async Task<bool> RunMessage(JObject message)
        {
            IGapPipelineTaskService gapPipelineTaskService = await GapQueueStartup.InitializeGapPipelineTaskServiceAsync();
            Module taskModule = await gapPipelineTaskService.GetPipelineTaskModule(int.Parse(message.Property("ID").Value.ToString()));

            string filePrefix = (await IsModuleSystemStart(taskModule) ? "Pipeline" : message.Property("UniqueStamp").Value.ToString());
            IParameterValueCollection inputParameters = await GetInputParameters(message, taskModule, filePrefix);

            if (await IsModuleCustom(taskModule) || (await IsModuleSystemStart(taskModule)))
            {
                await SerializeOutputParameters(message, taskModule, inputParameters);
            }
            else if (await IsModuleSystemEnd(taskModule))
            {
                await SerializeMappedOutputParameters(message, taskModule, inputParameters, "Pipeline");
            }
            else
            {
                IModule moduleInstance = await GetModuleInstance(taskModule, message);                
                IParameterValueCollection outputParameters = await moduleInstance.RunTask(inputParameters);
                await SerializeOutputParameters(message, taskModule, outputParameters);
            }

            return true;
        }
    }
}
