﻿using GfK.Automata.Pipeline.Data.Service.Messaging;
using GfK.Automata.Pipeline.Model.Domains;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public interface IQueueRunner<T> where T : class
    {
        Task AddInstanceEndEntries(T message, string status, string addlMessage = null);
        Task<Dictionary<string, int?>> AddInstanceStartEntries(T message);
        Task<string> GetExceptionMessage(Exception ex);
        Task<int?> GetFirstTaskID(T message);
        Task<int?> GetIDFromMessage(T message);
        Task<int> GetMessageNumericValue(T message, string idPropertyName);
        Task<string> GetErrorMessagePrefix(T message);
        string GetQueueName();
        Task<IPipelineRunStack> GetPipelineHierarchy(int pipelineID, IPipelineRunStack pipelineRunStack, bool towardsParent = true);
        Task<IPipelineRunStack> ToPipelineStack(JArray runStack);
        Task<bool> IsLastTaskInPipeline(int taskToBeRunID, int pipelineID, T message = null);
        Task LogPipelineTaskProcessMessageError(T message, Exception ex, string messageType);
        Task RemoveFailedMessageFromQueue(T message);
        Task RemoveParameterFilesForCompletedPipeline(T message);
        Task ProcessPendingPipelineRunMessage(T message);
        Task ProcessPendingPipelineTaskCompletedMessage(T message);
        void ProcessPendingPipelineTaskRunMessage(object threadMessage);
        Task ProcessPendingScheduleUpdateMessage(T message);
        Task SendCompletedMessage(int ID, string recipientType, int pipelineInstanceID, string uniqueStamp, JArray runStack, int? parentID = default(int?)
            ,int? resumePipelineID = null, int? resumePipelineTaskID = null);
        Task SendCompletedPipelineMessage(T message);
        Task SendCompletedPipelineTaskMessage(T message, Dictionary<string, int?> newlyAssignedInstanceIds);
        Task SendPipelineTaskRunMessage(int pipelineTaskID, int ParentID, int pipelineInstanceID, string uniqueStamp, JArray runStack, bool isFirst = false, bool isLast = false
            ,int? resumePipelineID = null, int? resumePipelineTaskID = null);
        Task SendStartPipelineMessage(int pipelineID, IPipelineRunStack pipelineRunStack = null, string runParentPipelines = "Parent");
    }
}
