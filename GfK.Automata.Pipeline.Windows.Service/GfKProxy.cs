﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public class GfKProxy : IWebProxy
    {
        public ICredentials Credentials
        {
            get { return new NetworkCredential(ConfigurationManager.AppSettings["ProxyUsername"], ConfigurationManager.AppSettings["ProxyPassword"]); }
            //or get { return new NetworkCredential("user", "password","domain"); }
            set { }
        }

        public Uri GetProxy(Uri destination)
        {
            return new Uri(ConfigurationManager.AppSettings["ProxyUrl"]);
        }

        public bool IsBypassed(Uri host)
        {
            return false;
        }
    }
}
