﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public class ScheduleLogger : IScheduleLogger
    {
        private IServiceLogger _logger;
        const string messageRoot = "Automata Scheduler Service:";

        public ScheduleLogger(IServiceLogger logger)
        {
            _logger = logger;
        }
        public void LogMessage(string message, string segmentIndicatorPrefix = "")
        {
            Task logMessageRequest = LogMessageAsync(message, segmentIndicatorPrefix);
            logMessageRequest.Wait();
        }
        public void LogStartStopMessage(string message)
        {
            LogMessage(message, "**** " );
        }
        public async Task LogMessageAsync(string message, string segmentIndicatorPrefix = "")
        {
            string minute = DateTime.UtcNow.Minute.ToString();

            await _logger.WriteAsync(segmentIndicatorPrefix + messageRoot + " - " + DateTime.UtcNow.ToShortDateString() + " " + DateTime.UtcNow.Hour.ToString()
                + ":" + (minute.Length == 1 ? "0" + minute : minute) + ":" + DateTime.UtcNow.Second.ToString() + " UTC " + message);
        }
    }
}
