﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public class GapQueueTriggerContext
    {
        public JObject Message { get; set; }
        public SemaphoreSlim JobLimit { get; set; }
    }
}
