﻿using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Data.Service.Messaging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model.Views;
using GfK.Automata.Pipeline.Model.Domains;
using System.Net;
using System.Collections.Specialized;
using System.Web;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public class PipelineQueueRunner : IPipelineQueueRunner
    {
        private readonly IGapMessageQueue _pipelineQueueManager;
        private readonly IGapMessageQueue _pipelineTaskQueueManager;
        private readonly IGapMessageQueue _scheduleUpdateQueueManager;
        private readonly IScheduleLogger _fileLogger;
        private readonly IGapMessageRunner _messageRunner;
        private IScheduleService _scheduleService;
        private IPipelineNotificationSettingsService _pipelineNotificationSettingsService;
        private INotificationService _notificationService;


        public PipelineQueueRunner(IGapMessageQueue pipelineQueueManager, IGapMessageQueue pipelineTaskQueueManager, IGapMessageRunner messageRunner
            , IScheduleLogger fileLogger, IGapMessageQueue scheduleUpdateQueueManager, IPipelineNotificationSettingsService pipelineNotificationSettingsService,
            INotificationService notificationService)
        {
            _pipelineQueueManager = pipelineQueueManager;
            _pipelineTaskQueueManager = pipelineTaskQueueManager;
            _fileLogger = fileLogger;
            _messageRunner = messageRunner;
            _scheduleUpdateQueueManager = scheduleUpdateQueueManager;
            _pipelineNotificationSettingsService = pipelineNotificationSettingsService;
            _notificationService = notificationService;
        }
        public IScheduleService InitializeScheduleService()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            IScheduleRepository scheduleRepository = new ScheduleRepository(dbFactory);
            IQuartzService quartzService = new QuartzService();
            IScheduleService scheduleService = new ScheduleService(scheduleRepository, unitOfWork, quartzService);
            scheduleService.ScheduleFired += new EventHandler<int>(async (s, e) => await SendStartPipelineMessage(e));
            return scheduleService;
        }


        public async Task<int> GetMessageNumericValue(JObject message, string idPropertyName)
        {
            return int.Parse(message.Property(idPropertyName).Value.ToString());
        }

        public async Task<string> GetExceptionMessage(Exception ex)
        {
            return (ex.InnerException == null ? ex.Message + " : " + ex.StackTrace : ex.InnerException.Message + " : " + ex.InnerException.StackTrace);
        }

        public async Task<string> GetErrorMessagePrefix(JObject message)
        {
            string prefix = "** ERROR - " + (message.Property("UniqueStamp") != null ? message.Property("UniqueStamp").Value.ToString() + " - " : "");

            return prefix;
        }

        public async Task<int?> GetIDFromMessage(JObject message)
        {
            if (message.Property("ID") != null)
            {
                int? ID = int.Parse(message.Property("ID").Value.ToString());
                return ID;
            }
            else
            {
                return null;
            }
        }

        public string GetQueueName()
        {
            Task<string> queueNameRequest = _pipelineQueueManager.GetQueueName();
            queueNameRequest.Wait();

            return queueNameRequest.Result;
        }

        public async Task RemoveParameterFilesForCompletedPipeline(JObject message)
        {
            if (bool.Parse(ConfigurationManager.AppSettings.Get("DeleteParameterFiles")))
            {
                string taskParameterPath = ConfigurationManager.AppSettings.Get("PipelineTaskParameterDirectory");
                string taskParameterFile = taskParameterPath + message.Property("UniqueStamp").Value.ToString() + "*.*";

                string[] parameterFilesToDelete = Directory.GetFiles(taskParameterPath, message.Property("UniqueStamp").Value.ToString() + "*.*");
                foreach (string parameterFile in parameterFilesToDelete)
                {
                    File.Delete(parameterFile);
                }
            }
        }
        public async Task<int?> GetFirstTaskID(JObject message)
        {
            IPipelineTaskService _pipelineTaskService = GapQueueStartup.InitializePipelineTaskService();
            int? pipelineTaskId = await _pipelineTaskService.GetFirstPipelineTaskId(await GetMessageNumericValue(message, "ID"));

            if (pipelineTaskId != null)
            {
                await _fileLogger.LogMessageAsync("First piplineTaskID for Pipeline " + (await GetIDFromMessage(message)).ToString()
                    + " found - " + pipelineTaskId.ToString());
            }

            return pipelineTaskId;
        }

        public async Task<bool> IsLastTaskInPipeline(int taskToBeRunID, int pipelineID, JObject message = null)
        {
            IPipelineTaskService _pipelineTaskService = GapQueueStartup.InitializePipelineTaskService();
            int? taskToBeRunSequencePosition = await _pipelineTaskService.GetPipelineTaskSequenceNumber(taskToBeRunID);
            int? nextTaskID = (taskToBeRunSequencePosition == null ? null : await _pipelineTaskService.GetNextPipelineTaskID(pipelineID, (int)taskToBeRunSequencePosition));

            if (nextTaskID == null && message != null && message.Property("ResumePipelineTaskID") != null)
            {
                taskToBeRunSequencePosition = await _pipelineTaskService.GetPipelineTaskSequenceNumber(int.Parse(message.Property("ResumePipelineTaskID").Value.ToString()));
                nextTaskID = (taskToBeRunSequencePosition == null ? null : await _pipelineTaskService.GetNextPipelineTaskID(int.Parse(message.Property("ResumePipelineID").Value.ToString())
                    , (int)taskToBeRunSequencePosition));
            }

            return nextTaskID == null;
        }

        public async Task<Dictionary<string, int?>> AddInstanceStartEntries(JObject message)
        {
            Dictionary<string, int?> newlyAssignedInstanceIds = new Dictionary<string, int?>();
            IPipelineInstanceWriterService pipelineInstanceWriter = await GapQueueStartup.InitializePipelineWriterAsync();
            IPipelineTaskInstanceWriterService pipelineTaskInstanceWriter = await GapQueueStartup.InitializePipelineTaskWriterAsync();

            try
            {
                string baseUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                string apiUrl = string.Empty;
                newlyAssignedInstanceIds.Add("PipelineInstanceID", await pipelineInstanceWriter.AddInstanceStartEntry(message));
                if (newlyAssignedInstanceIds["PipelineInstanceID"] != null) // then created pipeline instance and this first task
                {
                    message.Property("PipelineInstanceID").Value = newlyAssignedInstanceIds["PipelineInstanceID"];
                }
                newlyAssignedInstanceIds.Add("PipelineTaskInstanceID", await pipelineTaskInstanceWriter.AddInstanceStartEntry(message));
                apiUrl = baseUrl + "/api/Pipelines/Instances/" + newlyAssignedInstanceIds["PipelineInstanceID"] + "/Events";
                NotifyloggedInClients(apiUrl);
                message.Add("PipelineTaskInstanceID", newlyAssignedInstanceIds["PipelineTaskInstanceID"]);

                return newlyAssignedInstanceIds;
            }
            catch (Exception ex)
            {
                await _fileLogger.LogMessageAsync(await GetErrorMessagePrefix(message) + "Possible database issue : "
                    + await GetExceptionMessage(ex));
                return newlyAssignedInstanceIds;
            }
        }

        private void NotifyloggedInClients(string apiUrl)
        {
            try
            {
                WebClient client = new WebClient();

                // Add a user agent header in case the 
                // requested URI contains a query.

                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                Stream data = client.OpenRead(apiUrl);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                Console.WriteLine(s);
                data.Close();
                reader.Close();
            }
            catch (Exception e)
            {
                _fileLogger.LogMessage("exception notifying user of status change - " + e.ToString() + " - url - " + apiUrl);
            }
        }

        public async Task AddInstanceEndEntries(JObject message, string status, string addlMessage = null)
        {
            IPipelineInstanceWriterService pipelineInstanceWriter = await GapQueueStartup.InitializePipelineWriterAsync();
            IPipelineTaskInstanceWriterService pipelineTaskInstanceWriter = await GapQueueStartup.InitializePipelineTaskWriterAsync();

            try
            {
                string baseUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                string apiUrl = string.Empty;
                await pipelineInstanceWriter.AddInstanceEndEntry(message, status, addlMessage);
                await pipelineTaskInstanceWriter.AddInstanceEndEntry(message, status, addlMessage);
                apiUrl = baseUrl + "/api/Pipelines/Instances/" + message.Property("PipelineInstanceID").Value.ToString() + "/Events";
                NotifyloggedInClients(apiUrl);
            }
            catch (Exception ex)
            {
                await _fileLogger.LogMessageAsync(await GetErrorMessagePrefix(message) + "Possible database issue : "
                    + await GetExceptionMessage(ex));
            }
        }

        public async Task LogPipelineTaskProcessMessageError(JObject message, Exception ex, string messageType)
        {
            await _fileLogger.LogMessageAsync(await GetErrorMessagePrefix(message) + "Failed to process Pipeline Task "
                + messageType + " message for Pipeline Task " + (await GetIDFromMessage(message)).ToString() + ", Pipeline "
                + (await GetMessageNumericValue(message, "ParentID")).ToString() + " : " + await GetExceptionMessage(ex));

            string messageText = await GetExceptionMessage(ex);
            if (messageType.ToUpper() == "COMPLETED")
            {
                messageText = " Error processing Completed messages - " + await GetExceptionMessage(ex);
            }
            await AddInstanceEndEntries(message, "Failed", messageText);
        }

        public async Task RemoveFailedMessageFromQueue(JObject message)
        {
            if (bool.Parse(ConfigurationManager.AppSettings.Get("RemoveMessageReportingFailure")))
            {
                await _pipelineQueueManager.RemoveMessageFromQueue(message);
            }
        }

        public async Task<IPipelineRunStack> GetPipelineHierarchy(int pipelineID, IPipelineRunStack pipelineRunStack, bool towardsParent = true)
        {
            IPipelineRunStack newPipelineRunStack = (pipelineRunStack == null ? new PipelineRunStack() { RunStack = new Stack<int>() } : pipelineRunStack);

            if (pipelineRunStack != null)
            {
                newPipelineRunStack.RunStack.Push(pipelineID);
            }
            else
            {
                IPipelineHierarchyService hierarchyService = await GapQueueStartup.InitializePiplineHierarchyService();

                IEnumerable<PipelineHierarchy> pipelineHierarchy = await hierarchyService.GetPipelineHierarchy(pipelineID, towardsParent);
                foreach (PipelineHierarchy pipelineHiearchyLevel in pipelineHierarchy)
                {
                    newPipelineRunStack.RunStack.Push((towardsParent ? pipelineHiearchyLevel.HierarchyPipelineID : pipelineHiearchyLevel.PipelineID));
                }
            }

            return newPipelineRunStack;
        }

        public async Task<IPipelineRunStack> ToPipelineStack(JArray runStack)
        {
            Stack<int> pipelineIDStack = new Stack<int>();

            if (runStack != null)
            {
                foreach (JToken pipelineID in runStack.Reverse())
                {
                    pipelineIDStack.Push(int.Parse(pipelineID.ToString()));
                }
            }

            IPipelineRunStack pipelineRunStack = new PipelineRunStack() { RunStack = pipelineIDStack };
            return pipelineRunStack;
        }

        public async Task SendPipelineTaskRunMessage(int pipelineTaskID, int ParentID, int pipelineInstanceID, string uniqueStamp, JArray runStack
            , bool isFirst = false, bool isLast = false, int? resumePipelineID = null, int? resumePipelineTaskID = null)
        {
            GapQueueMessage gapQueueMessage = new GapQueueMessage();
            gapQueueMessage.ID = pipelineTaskID;
            gapQueueMessage.ParentID = ParentID;
            gapQueueMessage.PipelineInstanceID = pipelineInstanceID;
            gapQueueMessage.IsFirst = isFirst;
            gapQueueMessage.IsLast = isLast;
            gapQueueMessage.RecipientType = "Pipelinetask";
            gapQueueMessage.Action = "Run";
            gapQueueMessage.UniqueStamp = uniqueStamp;
            gapQueueMessage.PipelineRunStack = await ToPipelineStack(runStack);
            gapQueueMessage.ResumePipelineID = resumePipelineID;
            gapQueueMessage.ResumePipelineTaskID = resumePipelineTaskID;

            await _pipelineTaskQueueManager.SendMessageAsync(gapQueueMessage);
            await _fileLogger.LogMessageAsync("Submitted Pipeline Task Run message for pipeline task " + pipelineTaskID.ToString() + " belonging to pipeline " + ParentID.ToString());
        }

        public async Task SendCompletedPipelineMessage(JObject message)
        {
            string messagePrefix = "Completed Pipeline ";
            await SendCompletedMessage(await GetMessageNumericValue(message, "ParentID"), "Pipeline", await GetMessageNumericValue(message, "PipelineInstanceID")
                , message.Property("UniqueStamp").Value.ToString(), null);
            messagePrefix = "Submitted Completed message for Pipeline ";

            await _fileLogger.LogMessageAsync(messagePrefix + await GetMessageNumericValue(message, "ParentID") + ".");
        }
        public async Task SendStartPipelineMessage(int pipelineID, IPipelineRunStack pipelineRunStack = null, string runParentPipelines = "Parent")
        {
            GapQueueMessage gapQueueMessage = new GapQueueMessage();

            if (runParentPipelines.ToUpper() == "NONE")
            {
                gapQueueMessage.ID = pipelineID;
            }
            else
            {
                IPipelineRunStack updatedRunStack = await GetPipelineHierarchy(pipelineID, pipelineRunStack, runParentPipelines.ToUpper() == "PARENT");
                gapQueueMessage.ID = updatedRunStack.RunStack.Pop();
                gapQueueMessage.PipelineRunStack = updatedRunStack;
            }

            gapQueueMessage.RecipientType = "Pipeline";
            gapQueueMessage.Action = "Run";
            gapQueueMessage.UniqueStamp = await Messager.GenerateUniqueStampAsync();
            gapQueueMessage.Label = "Run Pipeline Request";

            await _pipelineTaskQueueManager.SendMessageAsync(gapQueueMessage);
            await _fileLogger.LogMessageAsync("Submitted Pipeline Run message for pipeline " + gapQueueMessage.ID.ToString());
        }

        public async Task SendCompletedPipelineTaskMessage(JObject message, Dictionary<string, int?> newlyAssignedInstanceIds)
        {
            int? pipelineInstanceID = newlyAssignedInstanceIds["PipelineInstanceID"];
            await SendCompletedMessage(await GetMessageNumericValue(message, "ID"), "Pipelinetask"
                , (pipelineInstanceID != null ? (int)pipelineInstanceID : await GetMessageNumericValue(message, "PipelineInstanceID"))
                , message.Property("UniqueStamp").Value.ToString(), JArray.Parse(message.Property("RunStack").Value.ToString())
                , await GetMessageNumericValue(message, "ParentID")
                , (message.Property("ResumePipelineID") == null ? (int?)null : int.Parse(message.Property("ResumePipelineID").Value.ToString()))
                , (message.Property("ResumePipelineTaskID") == null ? (int?)null : int.Parse(message.Property("ResumePipelineTaskID").Value.ToString())));

            await _fileLogger.LogMessageAsync("Submitted Completed message for Pipeline Task " + (await GetMessageNumericValue(message, "ID")).ToString()
                + " for Pipeline " + (await GetMessageNumericValue(message, "ParentID")).ToString() + ".");
        }

        public async Task SendCompletedMessage(int ID, string recipientType, int pipelineInstanceID, string uniqueStamp, JArray runStack, int? parentID = null
            , int? resumePipelineID = null, int? resumePipelineTaskID = null)
        {
            GapQueueMessage gapQueueMessage = new GapQueueMessage();
            gapQueueMessage.ID = ID;
            gapQueueMessage.ParentID = parentID;
            gapQueueMessage.PipelineInstanceID = pipelineInstanceID;
            gapQueueMessage.UniqueStamp = uniqueStamp;
            gapQueueMessage.RecipientType = recipientType;
            gapQueueMessage.Action = "Completed";
            gapQueueMessage.PipelineRunStack = await ToPipelineStack(runStack);
            gapQueueMessage.ResumePipelineID = resumePipelineID;
            gapQueueMessage.ResumePipelineTaskID = resumePipelineTaskID;

            await _pipelineQueueManager.SendMessageAsync(gapQueueMessage);
        }

        public async Task ProcessPendingPipelineRunMessage(JObject message)
        {
            try
            {
                int? pipelineTaskId = await GetFirstTaskID(message);
                int unAssignedPipelineInstanceIDPlaceholder = -1;

                if (pipelineTaskId != null)
                {
                    bool thisAlsoIsLastTask = await IsLastTaskInPipeline((int)pipelineTaskId, await GetMessageNumericValue(message, "ID"));
                    await SendPipelineTaskRunMessage((int)pipelineTaskId, await GetMessageNumericValue(message, "ID"), unAssignedPipelineInstanceIDPlaceholder
                        , message.Property("UniqueStamp").Value.ToString(), JArray.Parse(message.Property("RunStack").Value.ToString()), true, thisAlsoIsLastTask);
                }
            }
            catch (Exception ex)
            {
                await RemoveParameterFilesForCompletedPipeline(message);
                await _fileLogger.LogMessageAsync(await GetErrorMessagePrefix(message) + "Failed to process Pipeline Run message for Pipeline "
                    + (await GetIDFromMessage(message)).ToString() + " : " + await GetExceptionMessage(ex));
            }
        }

        public void ProcessPendingPipelineTaskRunMessage(object threadMessage)
        {
            string baseUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            string apiUrl = string.Empty;
            GapQueueTriggerContext runContext = threadMessage as GapQueueTriggerContext;
            JObject pendingRunMessage = runContext.Message;

            runContext.JobLimit.Wait();

            Task<Dictionary<string, int?>> idTask = AddInstanceStartEntries(pendingRunMessage);
            idTask.Wait();
            Dictionary<string, int?> newlyAssignedInstanceIds = idTask.Result;
            if (bool.Parse(pendingRunMessage.Property("IsFirst").Value.ToString()))
            {
                apiUrl = baseUrl + "/api/Pipelines/" + pendingRunMessage.Property("ParentID").Value.ToString() + "/Events";
                NotifyloggedInClients(apiUrl);
            }
            try
            {
                Task<bool> runMessagetask = _messageRunner.RunMessage(pendingRunMessage);
                runMessagetask.Wait();
                Task<int> taskID = GetMessageNumericValue(pendingRunMessage, "ParentID");
                taskID.Wait();
                Task<int> taskParentID = GetMessageNumericValue(pendingRunMessage, "ParentID");
                taskParentID.Wait();
                string id = taskID.Result.ToString();
                string parentID = taskParentID.Result.ToString(); ;
                if (runMessagetask.Result && newlyAssignedInstanceIds.Count > 0)
                {
                    AddInstanceEndEntries(pendingRunMessage, "Success").Wait();
                    SendCompletedPipelineTaskMessage(pendingRunMessage, newlyAssignedInstanceIds).Wait();
                    if (bool.Parse(pendingRunMessage.Property("IsLast").Value.ToString()))
                    {
                        apiUrl = baseUrl + "/api/Pipelines/" + parentID + "/Events";
                        NotifyloggedInClients(apiUrl);
                        SendNotification(parentID, 1);
                    }
                }
                else
                {
                    // TODO - these async calls should be wait() ed
                    string exceptionMessage = "Task module reported it failed to run properly, PipelineTaskInstanceModuleLog table may have details. "
                        + "(Pipeline: " + parentID
                    + " Pipeline Task: " + id + ")";
                    Exception runException = new Exception(exceptionMessage);
                    apiUrl = baseUrl + "/api/Pipelines/" + parentID + "/Events";
                    NotifyloggedInClients(apiUrl);
                    SendNotification(parentID, 2);
                    throw runException;
                }
            }
            catch (Exception ex)
            {
                RemoveFailedMessageFromQueue(pendingRunMessage).Wait();
                RemoveParameterFilesForCompletedPipeline(pendingRunMessage).Wait();
                LogPipelineTaskProcessMessageError(pendingRunMessage, ex, "Run").Wait();
                apiUrl = baseUrl + "/api/Pipelines/" + pendingRunMessage.Property("ParentID").Value.ToString() + "/Events";
                NotifyloggedInClients(apiUrl);
            }
            finally
            {
                runContext.JobLimit.Release();
            }
        }

        private async Task SendNotification(string pipelineID, int status)
        {
            try
            {
                int pipelineIDInt = Convert.ToInt32(pipelineID);
                IEnumerable<PipelineNotificationSetting> pipelineNotificationSettings = await _pipelineNotificationSettingsService.GetNotificationSettings(pipelineIDInt);
                NameValueCollection UserIDNotificationIDMap = new NameValueCollection();
                foreach (PipelineNotificationSetting pipelineNotificationSetting in pipelineNotificationSettings)
                {
                    if ((int)pipelineNotificationSetting.PipelineNotificationSettingType == status || (int)pipelineNotificationSetting.PipelineNotificationSettingType == 3)
                    {
                        string messageStatus = "Failure";
                        Notification notification = new Notification();
                        notification.Timestamp = DateTime.Now;
                        notification.PipelineID = pipelineIDInt;
                        notification.UserID = pipelineNotificationSetting.UserID;
                        if (status == 1)
                            messageStatus = "Completed";
                        notification = await _notificationService.CreateNotification(notification, messageStatus);
                        UserIDNotificationIDMap.Add(notification.NotificationID.ToString(), notification.UserID.ToString());
                    }
                }
                var parameters = new StringBuilder();

                foreach (string key in UserIDNotificationIDMap.Keys)
                {
                    parameters.AppendFormat("{0}={1}&",
                        HttpUtility.UrlEncode(key),
                        HttpUtility.UrlEncode(UserIDNotificationIDMap[key]));
                }

                parameters.Length -= 1;
                string baseUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                string apiUrl = baseUrl + "/api/Users/PipelineNotifications";
                var request = (HttpWebRequest)HttpWebRequest.Create(apiUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(parameters.ToString());    
                }
                
                request.GetResponse();
 
            }
            catch (Exception e)
            {
                _fileLogger.LogMessage("exception sending notification to user of success/failure of Pipeline - " + e.ToString());
            }
        }

        public async Task SendDependentPipelineRunMessage(JObject message)
        {
            if (message.Property("RunStack") != null)
            {
                JArray runStackMembers = JArray.Parse(message.Property("RunStack").Value.ToString());
                if (runStackMembers.Count > 0)
                {
                    int dependentPipelineID = int.Parse(runStackMembers.First.ToString());
                    runStackMembers.Remove(runStackMembers.First);

                    IPipelineRunStack runStack = new PipelineRunStack() { RunStack = new Stack<int>() };
                    if (runStackMembers.Count > 0)
                    {
                        foreach (JToken pipelineID in runStackMembers.Reverse())
                        {
                            runStack.RunStack.Push((int.Parse(pipelineID.ToString())));
                        }
                    }

                    await SendStartPipelineMessage(dependentPipelineID, runStack, "Parent");
                }
            }
        }

        public async Task ProcessPendingPipelineTaskCompletedMessage(JObject message)
        {
            try
            {
                IPipelineTaskService _pipelineTaskService = GapQueueStartup.InitializePipelineTaskService();
                int? completedTaskSequencePosition = await _pipelineTaskService.GetPipelineTaskSequenceNumber(await GetMessageNumericValue(message, "ID"));
                int? nextTaskID = await _pipelineTaskService.GetNextPipelineTaskID(await GetMessageNumericValue(message, "ParentID"), (int)completedTaskSequencePosition);

                if (nextTaskID == null)
                {
                    await RemoveParameterFilesForCompletedPipeline(message);
                    await SendDependentPipelineRunMessage(message);
                }
                else
                {
                    bool thisIsLastTask = await IsLastTaskInPipeline((int)nextTaskID, await GetMessageNumericValue(message, "ParentID"), message);
                    await SendPipelineTaskRunMessage((int)nextTaskID, await GetMessageNumericValue(message, "ParentID")
                        , await GetMessageNumericValue(message, "PipelineInstanceID"), message.Property("UniqueStamp").Value.ToString()
                        , JArray.Parse(message.Property("RunStack").Value.ToString()), false, thisIsLastTask
                        , (message.Property("ResumePipelineID") == null ? (int?)null : int.Parse(message.Property("ResumePipelineID").Value.ToString()))
                        , (message.Property("ResumePipelineTaskID") == null ? (int?)null : int.Parse(message.Property("ResumePipelineTaskID").Value.ToString())));
                }
            }
            catch (Exception ex)
            {
                await RemoveFailedMessageFromQueue(message);
                await RemoveParameterFilesForCompletedPipeline(message);
                await LogPipelineTaskProcessMessageError(message, ex, "Completed");
            }
        }

        private async Task LogScheduleUpdateProcessMessageError(JObject message, Exception ex, string messageType)
        {
            await _fileLogger.LogMessageAsync(await GetErrorMessagePrefix(message) + "Failed to process Schedule Update "
                + messageType + " message for pipeline " + (await GetIDFromMessage(message)).ToString() + " : " + await GetExceptionMessage(ex));

            string messageText = await GetExceptionMessage(ex);
        }

        public async Task ProcessPendingScheduleUpdateMessage(JObject message)
        {
            _scheduleService = InitializeScheduleService();
            int scheduleID = await GetMessageNumericValue(message, "ID");
            string action = message.Property("Action").ToString();
            // TODO - un-hack this
            _scheduleService = InitializeScheduleService();
            if (action.ToUpper() == "REMOVE")
            {
                await _scheduleService.RemoveQuartzSchedule(scheduleID);
            }
            else
            {
                await _scheduleService.LoadQuartzSchedule(scheduleID);
            }
        }
    }
}
