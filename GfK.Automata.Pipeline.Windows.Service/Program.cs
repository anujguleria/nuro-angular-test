﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Data.Service.Messaging;
using GfK.Automata.Pipeline.GapModule.PoC;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static string GetFileName(string[] args)
        {
            if (args.Length >= 2 && args[0].Trim().Length > 0)
            {
                return args[0].Trim();
            }
            else
            {
                return ConfigurationManager.AppSettings.Get("ServiceLogFile");
            }
        }
        public static int GetInterval(string[] args)
        {
            int interval;

            if (args.Length >= 3 && int.TryParse(args[0].Trim(), out interval))
            {
                return interval;
            }
            else
            {
                return int.Parse(ConfigurationManager.AppSettings.Get("ServicePollSeconds")) * 1000;
            }
        }
        public static int GetSemaphoreLimit(string[] args)
        {
            int limit;

            if (args.Length >= 1 && int.TryParse(args[0].Trim(), out limit))
            {
                return limit;
            }
            else
            {
                return int.Parse(ConfigurationManager.AppSettings.Get("ServiceJobLimit"));
            }
        }
        public static IScheduleService InitializeScheduleService()
        {
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            IScheduleRepository scheduleRepository = new ScheduleRepository(dbFactory);
            IQuartzService quartzService = new QuartzService();
            IScheduleService scheduleService = new ScheduleService(scheduleRepository, unitOfWork, quartzService);
            return scheduleService;
        }
        public static void Main(string[] args)
        {
            // HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Automata Scheduler
            try
            {
                string logFileName = GetFileName(args);
                int interval = GetInterval(args);
                int jobLimit = GetSemaphoreLimit(args);
    
                IScheduleLogger fileLogger = new ScheduleLogger(new FileLogger(logFileName));
                IPipelineQueueRunner pipelineQueueRunner = GapQueueStartup.InitializeQueueRunner(fileLogger);
                IGapQueueTriggerRunner triggerRunner = GapQueueStartup.InitializeGapQueueTriggerRunner(pipelineQueueRunner, jobLimit);
                IScheduleService scheduleService = InitializeScheduleService();

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new Scheduler(interval, pipelineQueueRunner, fileLogger, scheduleService)
                };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                using (StreamWriter writer = new StreamWriter(Path.GetDirectoryName(ConfigurationManager.AppSettings.Get("ServiceLogFile")) + "\\" + "MainServiceProgramError.txt"))
                {
                    string minute = DateTime.UtcNow.Minute.ToString();

                    writer.WriteLine(DateTime.UtcNow.ToShortDateString() + " " + DateTime.UtcNow.Hour.ToString()
                        + ":" + (minute.Length == 1 ? "0" + minute : minute) + ":" + DateTime.UtcNow.Second.ToString() + " UTC : " + ex.Message.ToString());
                    writer.WriteLine(ex.StackTrace.ToString());
                }
            }
            
        }
    }
}
