﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public interface IScheduleLogger
    {
        void LogMessage(string message, string segmentIndicatorPrefix = "");
        Task LogMessageAsync(string message, string segmentIndicatorPrefix = "");
        void LogStartStopMessage(string message);
    }
}
