﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public interface IServiceLogger
    {
        void Write(string message);
        Task WriteAsync(string message);
    }
}
