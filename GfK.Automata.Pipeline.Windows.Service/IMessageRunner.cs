﻿using GfK.Automata.Pipeline.GapModule;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public interface IMessageRunner<T> where T : class
    {
        Task<IParameterValueCollection> DeserializeInputParameters(T message, Module taskModule, string filePrefix);
        Task<IParameterValueCollection> DeserializeMappedInputParameters(PipelineTask pipelineTask, T message, Module taskModule, string filePrefix);
        Task<IParameterValueCollection> GetInputParameters(T message, Module taskModule, string filePrefix);
        Task<PipelineTask> GetMessagePipelineTask(T message);
        Task<IModule> GetModuleInstance(Module taskModule, T message);
        Task<IParameterValueCollection> GetUserInputParameterValues(T message, Module taskModule);
        Task<bool> RunMessage(T message);
        Task SerializeOutputParameters(T message, Module taskModule, IParameterValueCollection parameters);
        Task SerializeBuiltinOutputParameters(T message, Module taskModule, IParameterValueCollection parameters);
        Task SerializeMappedOutputParameters(T message, Module taskModule, IParameterValueCollection parameters, string filePrefix);
        Task SerializeCustomModuleStartTaskParameters(T message, Module taskModule, IParameterValueCollection parameters);
        Task SerializeSystemEndModuleParameters(T message, Module taskModule, IParameterValueCollection parameters);
    }
}
