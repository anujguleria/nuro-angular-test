﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public class FileLogger: IServiceLogger
    {
        public static SemaphoreSlim fileWriteLimit = new SemaphoreSlim(1, 1);

        public FileLogger(string fileName)
        {
            LogFileName = fileName;
        }
        public String LogFileName
        {
            get;
            set;
        }
        public void Write(string message)
        {
            Task writeRequest = WriteAsync(message);
            writeRequest.Wait();
        }
        public async Task WriteAsync(string message)
        {
            await fileWriteLimit.WaitAsync().ConfigureAwait(false);

            try
            {
                using (StreamWriter logWriter = new StreamWriter(LogFileName, append: true))
                {
                    logWriter.WriteLine(message);
                }
            }
            finally
            {
                fileWriteLimit.Release();
            }

        }
    }
}
