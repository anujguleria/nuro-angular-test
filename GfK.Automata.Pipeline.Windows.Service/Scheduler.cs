﻿using GfK.Automata.Pipeline.Data.Service.Messaging;
using GfK.Automata.Pipeline.Data.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace GfK.Automata.Pipeline.Windows.Service
{ 
    public partial class Scheduler : ServiceBase
    {
        private Timer _checkForWorkTimer = null;
        private IScheduleLogger _fileLogger;
        private long _interval;
        private IPipelineQueueRunner _queue;
        private IScheduleService _scheduleService;

        public Scheduler(int interval, IPipelineQueueRunner queueManager, IScheduleLogger fileLogger, IScheduleService scheduleService)
        {
            InitializeComponent();

            _queue = queueManager;
            _fileLogger = fileLogger;
            _interval = interval;
            _scheduleService = scheduleService;
            _scheduleService.ScheduleFired += new EventHandler<int>(async (s, e) => await _queue.SendStartPipelineMessage(e));
        }
        public void LogStartMessage()
        {
            _fileLogger.LogStartStopMessage("Starting Monitoring Queue - " + _queue.GetQueueName());
        }
        public void LogStopMessage()
        {
            _fileLogger.LogStartStopMessage("Stopping Monitoring Queue - " + _queue.GetQueueName());
        }
        protected override void OnStart(string[] args)
        {
            LogStartMessage();
            _scheduleService.LoadQuartzSchedules().Wait();
        }
        public void Run()
        {
            LogStartMessage();
            _scheduleService.LoadQuartzSchedules().Wait();
        }

        protected override void OnStop()
        {
            LogStopMessage();
            _scheduleService.StopQuartzSchedules().Wait();
        }
    }
}
