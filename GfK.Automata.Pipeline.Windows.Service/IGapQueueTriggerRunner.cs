﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Windows.Service
{
    public interface IGapQueueTriggerRunner : IQueueTriggerRunner<ReceiveCompletedEventArgs>
    {
    }
}
