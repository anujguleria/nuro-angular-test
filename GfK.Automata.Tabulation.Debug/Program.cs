﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;
using GfK.Automata.Tabulation.Service;
using GfK.Automata.Pipeline.GapModule;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using GfK.Automata.Pipeline.Model.Domains;

namespace GfK.Automata.Tabulation.Debug
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0 && args[0] == "RUNMODULE")
            {
                foreach (string arg in args)
                {
                    Console.WriteLine(arg);
                }
                try
                {
                    Console.WriteLine("int - " + IntPtr.Size);
                    DimTabulationModule tabModule = new DimTabulationModule();
                    tabModule.Logger = new DebugLogger();
                    tabModule.consoleRan = true;
                    // serialized parameters will be put in args[1]
                    Task<IParameterValueCollection> parametersTask = GetParametersFromArguments(args);
                    parametersTask.Wait();
                    IParameterValueCollection parameters = parametersTask.Result;
                    Task<IParameterValueCollection> returnParametersTask = tabModule.RunTask(parameters);
                    returnParametersTask.Wait();
                    IParameterValueCollection returnParameters = returnParametersTask.Result;
                    Console.WriteLine("RETURNMRS~" + GetAsyncParamValue(returnParameters, "MrsPath"));
                    Console.WriteLine("RETURNMTD~" + GetAsyncParamValue(returnParameters, "MtdPath"));
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR - " + e.ToString());
                    Console.WriteLine("ERROR - " + e.StackTrace);
                    throw;
                }
                return;
            }
            Dictionary<string, Dictionary<string, string>> filesDict = _filesDict();
            Tester T = new Tester();

            Console.WriteLine("Process entire tab plan? (Y/N)");
            string input = Console.ReadKey().Key.ToString();
            if (input == "Y")
            {
                
                foreach (KeyValuePair<string,Dictionary<string,string>> k_dict in filesDict)
                {
                    string k = k_dict.Key;
                    Dictionary<string, string> itm = filesDict[k];
                    Console.WriteLine("\nProcess " + k + " \n" + itm["path"] + itm["mdd"] + "?  Y/N\n");
                    input = Console.ReadKey().Key.ToString();
                    if (input == "Y")
                    {
                        T.Test(itm, true, false, false);
                        Console.WriteLine("Finished " + itm["mdd"] + "\n");
                        break;
                    }
                    
                }
            }
            else
            {

                string fieldName = "BRIA_M_GRID"; //AE_Loop|ABAW_GRID|FAM_GRID|TOT_CON_GRID|SBAW1|SBAW1_FirstMention|SBAW1_AllOtherMentions|SBAW1_OrderOfMention
                Console.WriteLine("Processing only [" + fieldName + "]");
                T = new Tester();
                T.Test(filesDict["Integration - FMCG"], false, false, false);
                T.TestField(fieldName);
            }
            /*
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            */

        }
        private static string GetAsyncParamValue(IParameterValueCollection parameters, string key)
        {
            Task<IParameter> parameter = parameters.GetParameter(key);
            parameter.Wait();
            return parameter.Result.Value;
        }

        private static async Task<IParameterValueCollection> GetParametersFromArguments(string[] args)
        {
            IParameterValueCollection parameters = new ParameterValueCollection();
            await parameters.AddParameter(new ParameterString("DimTabulationModule", "Language")
            {
                Value = args[1]
            });
            await parameters.AddParameter(new ParameterString("DimTabulationModule", "Context")
            {
                Value = args[2]
            });
            await parameters.AddParameter(new ParameterString("DimTabulationModule", "MrsPath")
            {
                Value = args[3]
            });
            await parameters.AddParameter(new ParameterString("DimTabulationModule", "MddPath")
            {
                Value = args[4]
            });
            await parameters.AddParameter(new ParameterString("DimTabulationModule", "Version")
            {
                Value = args[5]
            });
            await parameters.AddParameter(new ParameterString("DimTabulationModule", "DdfPath")
            {
                Value = args[6]
            });
            await parameters.AddParameter(new ParameterString("DimTabulationModule", "BannersMddPath")
            {
                Value = args[7]
            });
            return parameters;
        }

        private static Dictionary<string, Dictionary<string, string>> _filesDict()
        {
            const string
                mdd = "mdd",
                ddf = "ddf",
                path = "path",
                outname = "outname";

            Dictionary<string, Dictionary<string, string>> filesDict = new Dictionary<string, Dictionary<string, string>>();
            filesDict.Add("Integration - FMCG", new Dictionary<string, string>
            {
                { path, @"C:\Debug\Integration\" },
                { mdd, "USCBVMODTEST1 - funnel" },
                { ddf, "USCBVMODTEST1 - funnel.ddf" },
                { outname, "fmcg.mtd" }

            });
            filesDict.Add("Integration - TECH", new Dictionary<string, string>
            {
                { path, @"C:\Debug\Integration\" },
                { mdd, "USCBVMODTEST2 - funnel" },
                { ddf, "USCBVMODTEST2 - funnel.ddf" },
                { outname, "tech.mtd" }

            });

            return filesDict;
        }

        private void _notes()
        { 
            /*
            DimDetailTable t = new DimDetailTable("ENU","ANA","My Table");
            t.TableDoc = "TableDoc";
            //t.Name = "Table1";
            t.Side = "axis({base(), unweightedbase(), 'Top Three Box (Net)' Net({ _1, _2, _3 }), _4, _5, _6, _7, total()})";
            t.Top = "q1";
            t.Description = "Dummy Table";
            t.FilterName = "_flt1";
            t.FilterExpression = "q1 = {Yes}";
            t.FilterDescription = "Answered Yes to Q1";

            Console.WriteLine(t.GetScript(ScriptType.stDimensions));

            string path = @"C:\Debug\Bagat-10\";
            string file = "USC_BV_MOD_qtype";
            string mdd = path + file + ".mdd";
            string json = path + file + ".txt";
            string mrs = path + file + ".mrs";
            MetadataDocument d = MDMReader.Read(mdd, "{..}", MetadataSourceType.stDimensionsMDD);
            d.Log(json);
            ProcessorConfiguration c = new ProcessorConfiguration();
            c.Add("Document", d);
            c.Add("Language", "ENU");
            c.Add("Context", "Analysis");
            c.Add("MRSPath", mrs);
            c.Add("MDMPath", path);
            c.Add("MDSC", "mrScriptMDSC");
            c.Add("MetadataVersion", "{..}");
            c.Add("CaseDataPath", "not-a-real-path.ddf");
            c.Add("CDSC", "mrDataFileDSC");
            c.Add("Project", "");
            c.Add("MTDPath", "also-not-a-real-path.mtd");
            c.Add("TableDoc", "TableDoc");
            DimTabulationProcessor.Process(c);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            */
        }
    }
    public class DebugLogger : ILogger<ModuleLogMessageType>
    {
        public int PipelineTaskInstanceID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void Log(ModuleLogMessageType level, string message)
        {
            Console.WriteLine(level.ToString() + " - " + message);
        }

        public async Task LogAsync(ModuleLogMessageType level, string message)
        {
            Console.WriteLine(level.ToString() + " - " + message);
        }
    }
}
