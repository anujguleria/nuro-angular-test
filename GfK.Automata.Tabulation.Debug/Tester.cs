﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Debug
{
    public class Tester
    {
        string Path;
        string File;
        string CaseData;
        string Mdd;
        string Json;
        string Mrs;
        string language;
        string context;
        string mrsPath;
        DimBanners B;
        MetadataDocument D;
        ProcessorConfiguration C;
        DimTabPlan TP;
        public Tester()
        {
            

        }
        public void Test(Dictionary<string,string> fileDict, bool testConfig, bool testOleDb, bool testTabPlan)
        {
            
            Path = fileDict["path"];      
            File = fileDict["mdd"];       
            CaseData = fileDict["ddf"];   
            Mdd = System.IO.Path.Combine(Path, File + ".mdd");
            Json = System.IO.Path.Combine(Path, File + ".txt");
            Mrs = System.IO.Path.Combine(Path, File + ".mrs");
            D = Service.MDMReader.Read(Mdd, "{..}", MetadataSourceType.stDimensionsMDD);
            D.Log(Json);
            C = new Model.ProcessorConfiguration();
            B = new DimBanners();
            B.Add("MONTHLY_BANNER", "banMonthly");
            B.Add("QUARTER_BANNER", "banQuarterly");
            C.Add("BANNERNAMES", B.Banners);

            C.Add("Document", D);
            C.Add("Language", "ENU");
            C.Add("Context", "Analysis");
            C.Add("MRSPath", Mrs);
            C.Add("MDMPath", Mdd);
            C.Add("MDSC", "mrScriptMDSC");
            C.Add("MetadataVersion", "{..}");
            C.Add("CaseDataPath", System.IO.Path.Combine(Path, CaseData));
            C.Add("CDSC", "mrDataFileDSC");
            C.Add("Project", "");
            C.Add("MTDPath", System.IO.Path.Combine(Path, fileDict["outname"]));
            C.Add("TableDoc", "TableDoc");
            // query data for the banner variable creation
            DimOLEDbConnection.SetConnectionString(language, context, Mdd, Path + CaseData, DimOLEDbConnection.dbCategoryReturnType.dbValues);
            DateTime d = DimOLEDbConnection.GetMaxValue("DataCollection.FinishTime");
            dynamic dt = DimOLEDbConnection.GetMaxDate("DataCollection.FinishTime");
            string dateString = String.Join("/", new List<int>(){ d.Month, d.Day, d.Year });

            C.Add("MAXDATE", DimOLEDbConnection.GetMaxValue("DataCollection.FinishTime").ToString());
            C.Add("DATEVAR", "DataCollection.FinishTime");
            C.Add("MDAT_BANNERS", System.IO.Path.Combine(Path, fileDict["mdd"] + "_banners.mdd"));

            TP = new DimTabPlan(C);
            DimOLEDbConnection.SetConnectionString(language, context, Mdd, Path + CaseData, DimOLEDbConnection.dbCategoryReturnType.dbNames);
            C.Add("ProjectType", DimQueryData.GetProjectType());
            if (testOleDb == true)
            {
                _TestOleDB();
                return;
            }

            language = (string)C["Language"];
            context = (string)C["Context"];
            mrsPath = (string)C["MRSPath"];

            if (testConfig == true)
            {
                TestConfiguration();
            }
            else if (testTabPlan == true)
            {
                TestTabPlan(TP, language, context, mrsPath);
            }

            if (testConfig == true || testTabPlan == true)
            {
                WriteOut(mrsPath);
            }

        }

        private void _TestOleDB()
        {
            string VarName = null;
            // https://www.ibm.com/support/knowledgecenter/en/SSLVQG_7.0.0/com.spss.ddl/mroledb_ref_conprops.htm

            DimOLEDbConnection.SetConnectionString(language, context, Mdd, Path + CaseData, DimOLEDbConnection.dbCategoryReturnType.dbValues);
            VarName = "SPEND[1].slice";
            Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetPercentileRange(VarName, 0, 0.2)));
            //// Max & Min
            DimOLEDbConnection.SetConnectionString(language, context, Mdd, Path + CaseData, DimOLEDbConnection.dbCategoryReturnType.dbNames);
            VarName = "AGE";
            Console.WriteLine(String.Format("{0} Max : " + DimOLEDbConnection.GetMaxValue(VarName).ToString() + " Min : " + DimOLEDbConnection.GetMinValue(VarName).ToString(), VarName));
            // GetPercentileRange
            VarName = "AGE";
            Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetPercentileRange(VarName, 0, 0.33)));
            Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetPercentileValues(VarName, .33, .50)));
            Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetPercentileRange(VarName, .33, 0.50)));
            //// Distinct values
            //VarName = "DataCollection.FinishTime";
            //Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetDistinctValues(VarName)));
            //// All values
            //VarName = "OCC1";
            //Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetValues(VarName)));
            //Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetDistinctValues("SEX")));

            // Top 5 values
            VarName = "OCC2";
            Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetDistinctValues(VarName)));
            Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetFreqSortedValues(VarName)));

            DimOLEDbConnection.SetConnectionString(language, context, Mdd, Path + CaseData, DimOLEDbConnection.dbCategoryReturnType.dbValues);
            Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetDistinctValues(VarName)));
            Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetFreqSortedValues(VarName)));


            Console.WriteLine("Press any key to exit...");


            Console.ReadKey();

        }


        public void WriteOut(string path)
        {
            using (System.IO.StreamWriter sw = new StreamWriter(path, false, Encoding.UTF8))
            {
                sw.WriteLine(TP.GetScript(ScriptType.stDimensions));
            }

            Console.WriteLine("Output MRS available at: " + path);

        }

        public void TestField(string fieldName)
        {
            DimCustomerHarmonicsController.Init(D);
            // sets the initial connection string in case need to use it later
            DimOLEDbConnection.SetConnectionString(language, context, Mdd, Path + CaseData, DimOLEDbConnection.dbCategoryReturnType.dbNames);
            List<MetadataObject> propertyFields = D.Items.FindAll(o => o.Properties.ContainsKey(GfKProperties.GfKQType) && o.Properties[GfKProperties.GfKQType] == "ADCONX3_M").ToList();
            List<MetadataObject> fields = new List<MetadataObject>();
            foreach(string name in fieldName.Split('|'))
            {
                MetadataObject f = (MetadataObject)D.Find(name);
                if (f!=null)
                { fields.Add(f); }
                else
                {
                    Console.WriteLine(String.Format("{0} is not a valid field in the MDD", fieldName));
                }

            }
            if (fields.Count != 0)
            {
                string mrsPath = (string)C["MRSPath"];
                Service.DimTabulationProcessor.Process(fields, TP, language, context, null, C);
                string path = mrsPath.Replace(".mrs", "_tables.mrs");
                WriteOut(path);
            }
            else
            {
                Console.WriteLine("No valid fields given in " + String.Join(", ", fields));
            }
            
        }

        public void TestConfiguration()
        {

            Service.DimTabulationProcessor.Process(C);
        }
        public void TestTabPlan(DimTabPlan tp, string language, string context, string mrsPath)
        {
            // processes everything.
            // sets the initial connection string in case need to use it later
            DimOLEDbConnection.SetConnectionString(language, context, Mdd, Path + CaseData, DimOLEDbConnection.dbCategoryReturnType.dbNames);
            Service.DimTabulationProcessor.Process(C);
            //Service.DimTabulationProcessor.Process(tp, D, language, context, mrsPath);
        }

    }
}
