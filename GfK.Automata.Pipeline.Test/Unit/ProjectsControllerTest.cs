﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GfK.Automata.Pipeline.Test.Mock;
using GfK.Automata.Pipeline.Data.Service;
using System.Web.Http.Results;
using System.Collections.Generic;
using GfK.Automata.Pipeline.Model;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.API.Identity;
using GfK.Automata.Pipeline.API.Controllers;

namespace GfK.Automata.Pipeline.Test.Unit
{
    [TestClass, Ignore]
    public class ProjectsControllerTest
    {
        private readonly IProjectService _projectService;
        private readonly IProjectUserService _projectUserService;
        private readonly IPipelineService _pipelineService;
        private readonly IUserService _userService;
        private readonly ProjectsController _projectsController;
        private readonly IAuthenticationService _authenticationService;

        public ProjectsControllerTest()
        {
            _projectService = new ProjectServiceMock();
            _projectUserService = new ProjectUserServiceMock();
            _pipelineService = new PipelineServiceMock();
            _userService = new UserServiceMock();
            _authenticationService = new AuthenticationService();

            _projectsController = new ProjectsController(_projectService, _pipelineService, _projectUserService, _userService, _authenticationService, null);
            _userService = new UserServiceMock();
            _projectsController = new ProjectsController(_projectService, _pipelineService, _projectUserService, _userService, _authenticationService, null); 
        }
        [TestMethod]
        public async Task GetProjectsAll_OK()
        {
            OkNegotiatedContentResult<IEnumerable<Project>> result = await _projectsController.Get() 
                as OkNegotiatedContentResult<IEnumerable<Project>>;

            Assert.IsNotNull(result);
            List<Project> projects = new List<Project>(result.Content);
            Assert.IsTrue(projects.Count == 1 && projects[0].Name == "Mock Project");
        }

        [TestMethod]
        [Ignore]
        public async Task GetProjectsWithPipelines_OK()
        {
            OkNegotiatedContentResult<IEnumerable<Project>> result = await _projectsController.GetWithPipelines() 
                as OkNegotiatedContentResult<IEnumerable<Project>>;

            Assert.IsNotNull(result);
            List<Project> projects = new List<Project>(result.Content);
            Assert.IsTrue(projects.Count == 1 && projects[0].Name == "Mock Project");
            Assert.IsTrue(projects[0].pipeLines.Count == 1);
        }

        [TestMethod]
        [Ignore]
        public async Task GetPipelinesArePipelines_OK()
        {
            OkNegotiatedContentResult<IEnumerable<Model.Pipeline>> result = await _projectsController.GetPipelines(1) 
                as OkNegotiatedContentResult<IEnumerable<Model.Pipeline>>;

            Assert.IsNotNull(result);
            List<Model.Pipeline> pipelines = new List<Model.Pipeline>(result.Content);
            Assert.IsTrue(pipelines.Count == 1);
        }

        [TestMethod]
        [Ignore]
        public async Task GetPipelinesAreNoPipelines_OK()
        {
            OkNegotiatedContentResult<IEnumerable<Model.Pipeline>> result = await _projectsController.GetPipelines(2)
                as OkNegotiatedContentResult<IEnumerable<Model.Pipeline>>;

            Assert.IsNotNull(result);
            List<Model.Pipeline> pipelines = new List<Model.Pipeline>(result.Content);
            Assert.IsTrue(pipelines.Count == 0);
        }

        [TestMethod]
        [Ignore]
        public async Task GetProjectByNameExists_OK()
        {
            OkNegotiatedContentResult<Project> result = await _projectsController.GetByName("Existing Project")
                as OkNegotiatedContentResult<Project>;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Content.Name == "Existing Project");
        }

        [TestMethod]
        public async Task GetProjectByNameNotExists_OK()
        {
            OkNegotiatedContentResult<Project> result = await _projectsController.GetByName("NonExisting Project")
                as OkNegotiatedContentResult<Project>;

            Assert.IsNull(result);
        }
        // currently throwing not implemented exception because of project user mock
        [TestMethod, Ignore]
        public async Task CreateProject_OK()
        {
            Project project = new Project()
            {
                Name = "new project"
            };

            CreatedNegotiatedContentResult<int> result = await _projectsController.Post(project) as CreatedNegotiatedContentResult<int>;
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Content == 1);
            Assert.IsTrue(result.Location.ToString() == "api/Projects/1");
        }

        //[TestMethod]
        //public async Task CreateProjectAlreadyExists_OK()
        //{
        //    Project project = new Project()
        //    {
        //        Name = "Existing Project"
        //    };
        //    try
        //    {
        //        var result = await _projectsController.Post(project);
        //    }
        //    catch (Exception ex)
        //    {
        //        int x = 1;
        //    }

        //    //Assert.IsNotNull(result);

        //}
    }
}
