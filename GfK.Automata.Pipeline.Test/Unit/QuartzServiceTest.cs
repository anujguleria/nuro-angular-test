﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using System.Collections.ObjectModel;
using Quartz.Impl;
using System.Threading;

namespace GfK.Automata.Pipeline.Test
{
    [TestClass()]
    public class QuartzServiceTest
    {
        private IQuartzService _quartzService;
        private IScheduler scheduler;
        public QuartzServiceTest()
        {
            _quartzService = new QuartzService();
        }
        // TODO - test monthly recur > 1
        [TestMethod()]
        public void GetYearlyTriggerTestXDayOfWeek()
        {
            Model.Schedule schedule = new Model.Schedule
            {
                Recurrence = Model.ScheduleRecurrence.Yearly,
                YearlyRecurrenceType = Model.YearlyRecurrenceType.XDayOfWeekOfMonth,
                DayOfWeekOffset = 1,
                DayOfMonth = 1,
                TimeZoneID = "Central Standard Time",
                ScheduleID = 1,
                RecurEvery = 1
            };
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Subtract(new TimeSpan(3, 0, 0));
            schedule.MonthOfYear = (byte)schedule.StartTime.Month;

            ITrigger trigger = _quartzService.GetYearlyTrigger(schedule);

            DateTimeOffset nextFire = ScheduleTrigger(trigger, schedule).Value;
            nextFire = TimeZoneInfo.ConvertTimeToUtc(nextFire.DateTime, tz);
            DateTime utc = DateTime.UtcNow;

            DateTime expected = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime, tz).ToUniversalTime();
            // check the first Sunday of this month
            DateTime firstSundayOfMonth = new DateTime(utc.Year, utc.Month, 1, expected.Hour, expected.Minute, expected.Second);
            if (firstSundayOfMonth.DayOfWeek != DayOfWeek.Sunday)
            {
                firstSundayOfMonth = firstSundayOfMonth.AddDays(((int)(DayOfWeek.Sunday) - (int)firstSundayOfMonth.DayOfWeek + 7) % 7);
            }
            // if it's less than or equal to today, should run next year
            if (firstSundayOfMonth.Day <= utc.Day)
            {
                firstSundayOfMonth = new DateTime(utc.Year + 1, utc.Month, 1, expected.Hour, expected.Minute, expected.Second); ;
                if (firstSundayOfMonth.DayOfWeek != DayOfWeek.Sunday)
                {
                    firstSundayOfMonth = firstSundayOfMonth.AddDays(((int)(DayOfWeek.Sunday) - (int)firstSundayOfMonth.DayOfWeek + 7) % 7);
                }
            }
            expected = firstSundayOfMonth;
            Assert.AreEqual(expected.Year, nextFire.Year);
            Assert.AreEqual(expected.Month, nextFire.Month);
            Assert.AreEqual(expected.Hour, nextFire.Hour);
            Assert.AreEqual(expected.Day, nextFire.Day);
            // just test to the minute
            Assert.AreEqual(expected.Minute, nextFire.Minute);
        }

        [TestMethod()]
        public void GetYearlyTriggerTestSpecificDay()
        {
            Model.Schedule schedule = new Model.Schedule
            {
                Recurrence = Model.ScheduleRecurrence.Yearly,
                YearlyRecurrenceType = Model.YearlyRecurrenceType.SpecificDayOfMonth,
                TimeZoneID = "Central Standard Time",
                ScheduleID = 1,
                RecurEvery = 1
            };
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Subtract(new TimeSpan(3, 0, 0));
            schedule.DayOfMonth = (byte)schedule.StartTime.Day;
            schedule.MonthOfYear = (byte)schedule.StartTime.Month;
            ITrigger trigger = _quartzService.GetYearlyTrigger(schedule);

            DateTimeOffset nextFire = ScheduleTrigger(trigger, schedule).Value;
            nextFire = TimeZoneInfo.ConvertTimeToUtc(nextFire.DateTime, tz);
            DateTime utc = DateTime.UtcNow;

            DateTime expected = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime.AddYears(1), tz);
            Assert.AreEqual(expected.Year, nextFire.Year);
            Assert.AreEqual(expected.Month, nextFire.Month);
            Assert.AreEqual(expected.Day, nextFire.Day);
            Assert.AreEqual(expected.Hour, nextFire.Hour);
            // just test to the minute
            Assert.AreEqual(expected.Minute, nextFire.Minute);
        }
        // TODO - test monthly recur > 1
        [TestMethod()]
        public void GetMonthlyTriggerTestXDayOfWeek()
        {
            Model.Schedule schedule = new Model.Schedule
            {
                Recurrence = Model.ScheduleRecurrence.Monthly,
                MonthlyRecurrenceType = Model.MonthlyRecurrenceType.XDayOfWeekOfMonth,
                DayOfWeekOffset = 1,
                OnSunday = true,
                // TODO - right now this is saved as a 1 based Sunday to Saturday
                DayOfMonth = 1,
                TimeZoneID = "Central Standard Time",
                ScheduleID = 1,
                RecurEvery = 1
            };
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Subtract(new TimeSpan(3, 0, 0));

            ITrigger trigger = _quartzService.GetMonthlyTrigger(schedule);

            DateTimeOffset nextFire = ScheduleTrigger(trigger, schedule).Value;
            nextFire = TimeZoneInfo.ConvertTimeToUtc(nextFire.DateTime, tz);
            DateTime utc = DateTime.UtcNow;

            DateTime expected = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime, tz);
            // check the first Sunday of this month
            DateTime firstSundayOfMonth = new DateTime(utc.Year, utc.Month, 1, expected.Hour, expected.Minute, expected.Second);
            if (firstSundayOfMonth.DayOfWeek != DayOfWeek.Sunday)
            {
                firstSundayOfMonth = firstSundayOfMonth.AddDays(((int)(DayOfWeek.Sunday) - (int)firstSundayOfMonth.DayOfWeek + 7) % 7);
            }
            // if it's less than or equal to today, should run next month
            if (firstSundayOfMonth.Day <= utc.Day)
            {
                firstSundayOfMonth = new DateTime((utc.Month == 12 ? utc.Year + 1 : utc.Year), (utc.Month == 12 ? 1 : utc.Month + 1), 1, expected.Hour, expected.Minute, expected.Second); ;
                if (firstSundayOfMonth.DayOfWeek != DayOfWeek.Sunday)
                {
                    firstSundayOfMonth = firstSundayOfMonth.AddDays(((int)(DayOfWeek.Sunday) - (int)firstSundayOfMonth.DayOfWeek + 7) % 7);
                }
            }
            expected = firstSundayOfMonth;
            Assert.AreEqual(expected.Year, nextFire.Year);
            Assert.AreEqual(expected.Month, nextFire.Month);
            Assert.AreEqual(expected.Hour, nextFire.Hour);
            Assert.AreEqual(expected.Day, nextFire.Day);
            // just test to the minute
            Assert.AreEqual(expected.Minute, nextFire.Minute);
        }

        [TestMethod()]
        public void GetMonthlyTriggerTestSpecificDay()
        {
            Model.Schedule schedule = new Model.Schedule
            {
                Recurrence = Model.ScheduleRecurrence.Monthly,
                MonthlyRecurrenceType = Model.MonthlyRecurrenceType.SpecificDayOfMonth,
                TimeZoneID = "Central Standard Time",
                ScheduleID = 1,
                RecurEvery = 1
            };
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Subtract(new TimeSpan(3, 0, 0));
            schedule.DayOfMonth = (byte)schedule.StartTime.Day;
            // if it's 28, just make it the second and we'll add a month to the expected
            if (schedule.StartTime.Day > 28)
            {
                schedule.DayOfMonth = 2;
            }
            ITrigger trigger = _quartzService.GetMonthlyTrigger(schedule);

            DateTimeOffset nextFire = ScheduleTrigger(trigger, schedule).Value;
            nextFire = TimeZoneInfo.ConvertTimeToUtc(nextFire.DateTime, tz);
            DateTime utc = DateTime.UtcNow;

            // should fire same day next month
            // if it's 28, just add 4 days so we don't have to deal with the next month not having that date
            DateTime expected = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime, tz);
            expected = expected.AddMonths(1);
            if (schedule.StartTime.Day > 28)
            {
                expected = new DateTime(expected.Year, expected.Month, 2, expected.Hour, expected.Minute, expected.Second);
            }
            Assert.AreEqual(expected.Year, nextFire.Year);
            Assert.AreEqual(expected.Month, nextFire.Month);
            Assert.AreEqual(expected.Day, nextFire.Day);
            Assert.AreEqual(expected.Hour, nextFire.Hour);
            // just test to the minute
            Assert.AreEqual(expected.Minute, nextFire.Minute);
        }

        [TestMethod()]
        public void GetWeeklyTriggerTest()
        {
            Model.Schedule schedule = new Model.Schedule
            {
                Recurrence = Model.ScheduleRecurrence.Weekly,
                TimeZoneID = "Central Standard Time",
                ScheduleID = 1,
                OnMonday = true,
                OnFriday = true,
                RecurEvery = 1
            };
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Subtract(new TimeSpan(3, 0, 0));
            ITrigger trigger = _quartzService.GetWeeklyTrigger(schedule);

            DateTimeOffset nextFire = ScheduleTrigger(trigger, schedule).Value;
            nextFire = TimeZoneInfo.ConvertTimeToUtc(nextFire.DateTime, tz);
            DateTime utc = DateTime.UtcNow;
            // this should return either Monday or Friday correctly
            DateTime expected = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime.AddDays(((int)(
                (utc.DayOfWeek >= DayOfWeek.Monday && utc.DayOfWeek < DayOfWeek.Friday ? DayOfWeek.Friday : DayOfWeek.Monday) - (int)utc.DayOfWeek + 7) % 7)), tz);
            Assert.AreEqual(expected.Year, nextFire.Year);
            Assert.AreEqual(expected.Month, nextFire.Month);
            Assert.AreEqual(expected.Day, nextFire.Day);
            Assert.AreEqual(expected.Hour, nextFire.Hour);
            // just test to the minute
            Assert.AreEqual(expected.Minute, nextFire.Minute);
        }
        [TestInitialize]
        public void Initialize()
        {
            scheduler = StdSchedulerFactory.GetDefaultScheduler();
            if (!scheduler.IsStarted)
            {
                scheduler.Start();
            }
        }
        [TestCleanup]
        public void Cleanup()
        {
            scheduler.Clear();
        }

        [TestMethod]
        public void GetDailyTriggerTestEveryWeekday()
        {
            Model.Schedule schedule = new Model.Schedule
            {
                Recurrence = Model.ScheduleRecurrence.Daily,
                DailyRecurrenceType = Model.DailyRecurrenceType.EveryWeekday,
                TimeZoneID = "Central Standard Time",
                ScheduleID = 1
            };
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Subtract(new TimeSpan(3, 0, 0));
            // we'll just make it Saturday and test that the next run will be Monday
            if (schedule.StartTime.DayOfWeek == DayOfWeek.Sunday)
            {
                schedule.StartTime = schedule.StartTime.AddDays(-1);
            }
            else
            {
                schedule.StartTime = schedule.StartTime.AddDays(6 - (int)schedule.StartTime.DayOfWeek);
            }

            ITrigger trigger = _quartzService.GetDailyTrigger(schedule);

            DateTimeOffset nextFire = ScheduleTrigger(trigger, schedule).Value;
            nextFire = TimeZoneInfo.ConvertTimeToUtc(nextFire.DateTime, tz);

            DateTime expected = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime.AddDays(2), tz);
            Assert.AreEqual(expected.Year, nextFire.Year);
            Assert.AreEqual(expected.Month, nextFire.Month);
            Assert.AreEqual(expected.Day, nextFire.Day);
            Assert.AreEqual(expected.Hour, nextFire.Hour);
            // just test to the minute
            Assert.AreEqual(expected.Minute, nextFire.Minute);
        }

        [TestMethod()]
        public void GetDailyTriggerTestXDays()
        {
            Model.Schedule schedule = new Model.Schedule
            {
                Recurrence = Model.ScheduleRecurrence.Daily,
                DailyRecurrenceType = Model.DailyRecurrenceType.EveryXDays,
                RecurEvery = 3,
                TimeZoneID = "Central Standard Time",
                ScheduleID = 1
            };
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Subtract(new TimeSpan(3, 0, 0));

            // set it to start 3 hours ago, then check to see if it's next fire time is 3 days from 3 hours ago
            ITrigger trigger = _quartzService.GetDailyTrigger(schedule);

            DateTimeOffset nextFire = ScheduleTrigger(trigger, schedule).Value.ToUniversalTime();

            DateTime expected = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime.AddDays(3), tz).ToUniversalTime();
            Assert.AreEqual(expected.Year, nextFire.Year);
            Assert.AreEqual(expected.Month, nextFire.Month);
            Assert.AreEqual(expected.Day, nextFire.Day);
            Assert.AreEqual(expected.Hour, nextFire.Hour);
            // just test to the minute
            Assert.AreEqual(expected.Minute, nextFire.Minute);
        }

        [TestMethod()]
        public void GetNoneTriggerTest()
        {
            Model.Schedule schedule = new Model.Schedule
            {
                Recurrence = Model.ScheduleRecurrence.None,
                TimeZoneID = "Central Standard Time",
                ScheduleID = 1
            };
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz).Add(new TimeSpan(3, 0, 0));

            // set it to start 3 hours ago, then check to see if it's next fire time is 3 days from 3 hours ago
            ITrigger trigger = _quartzService.GetNoneTrigger(schedule);

            DateTimeOffset nextFire = ScheduleTrigger(trigger, schedule).Value;
            nextFire = TimeZoneInfo.ConvertTimeToUtc(nextFire.DateTime, tz); ;

            DateTime expected = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime, tz);
            Assert.AreEqual(expected.Year, nextFire.Year);
            Assert.AreEqual(expected.Month, nextFire.Month);
            Assert.AreEqual(expected.Day, nextFire.Day);
            Assert.AreEqual(expected.Hour, nextFire.Hour);
            // just test to the minute
            Assert.AreEqual(expected.Minute, nextFire.Minute);
        }
        private DateTimeOffset? GetNextFireTime(ITrigger trigger, Model.Schedule schedule, DateTime startLookingTime)
        {
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            DateTime startTimeUTC = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime, timezone);
            TriggerKey triggerKey = new TriggerKey(schedule.ScheduleID.ToString());
            ITrigger scheduledTrigger = scheduler.GetTrigger(triggerKey);
            int justInCase = 0;
            // get fire time after has some weird behavior that we normalize back out
            DateTimeOffset? nextFire = scheduledTrigger.GetFireTimeAfter(startLookingTime.AddMinutes(1)).Value;
            // sometimes it returns as the timezone time
            if (nextFire.Value.Hour != schedule.StartTime.Hour)
            {
                if (nextFire.Value.Hour != startTimeUTC.Hour)
                {
                    // if not, let's assume it's in local time with right time of timezone, so adjust
                    nextFire = nextFire.Value.LocalDateTime;
                    // for whatever reason it returns the next fire time as local time instead of the timezone it's in, we'l just adjust minutes based on offset
                    double offsetMinutes = TimeZoneInfo.Local.BaseUtcOffset.TotalMinutes - timezone.BaseUtcOffset.TotalMinutes;
                    nextFire = nextFire.Value.AddMinutes(offsetMinutes);
                }
                else
                {
                    // sometimes it returns as UTC
                    nextFire = TimeZoneInfo.ConvertTimeFromUtc(nextFire.Value.DateTime, timezone);
                }
            }
            return nextFire;
        }
        private DateTimeOffset? ScheduleTrigger(ITrigger trigger, Model.Schedule schedule)
        {

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<TestJob2>()
                .WithIdentity("testJob", "testGroup")
                .Build();

            // Tell quartz to schedule the job using our trigger
            scheduler.ScheduleJob(job, trigger);
            // for whatever reason a single trigger job doesn't leave trigger in scheduler
            if (schedule.Recurrence == Model.ScheduleRecurrence.None)
            {
                TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
                return TimeZoneInfo.ConvertTimeFromUtc(trigger.GetNextFireTimeUtc().Value.DateTime, timezone);
            }
            else
            {
                return GetNextFireTime(trigger, schedule, DateTime.UtcNow);
            }
        }
    }

    public class TestJob2 : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var x = 5;
        }
    }
}
