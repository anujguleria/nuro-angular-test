﻿using GfK.Automata.Pipeline.API.Controllers;
using GfK.Automata.Pipeline.API.Identity;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Test.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace GfK.Automata.Pipeline.Test.Unit
{
    [TestClass]
    public class GapGroupsControllerTest
    {
        private readonly IGapGroupService _gapGroupService;
        private readonly IUserService _userService;
        private readonly IAuthenticationService _authenticationService;
        private readonly GroupsController _gapGroupsController;

        public GapGroupsControllerTest()
        {
            _gapGroupService = new GapGroupServiceMock();
            _userService = new UserServiceMock();
            _authenticationService = new AuthenticationServiceMock();

            _gapGroupsController = new GroupsController(_gapGroupService, _authenticationService, _userService);
        }

        [TestMethod]
        public async Task GetGapGroupsAll_OK()
        {
            //OkNegotiatedContentResult<IEnumerable<GapGroup>> result = await _gapGroupsController.Get()
            //    as OkNegotiatedContentResult<IEnumerable<GapGroup>>;

            //Assert.IsNotNull(result);
            //List<GapGroup> groups = new List<GapGroup>(result.Content);
            //Assert.IsTrue(groups.Count == 2 && groups[0].Name == "GapGoup1");

            Assert.IsTrue(true);
        }
    }
}
