﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GfK.Automata.Pipeline.API.Controllers;
using GfK.Automata.Pipeline.Test.Mock;
using GfK.Automata.Pipeline.Data.Service;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using GfK.Automata.Pipeline.Model;
using System.Collections.Generic;
using GfK.Automata.Pipeline.API.Identity;

namespace GfK.Automata.Pipeline.Test.Unit
{
    [TestClass]
    public class UsersControllerTest
    {
        [TestMethod, Ignore]
        public async Task GetProjects_OK()
        {
            IProjectService projectService = new ProjectServiceMock();
            IUserService userService = new UserServiceMock();
            IAuthenticationService _authenticationService = new AuthenticationService();
            IProjectUserService _projectUserService = new ProjectUserServiceMock();
            IGapRoleUserService _gapRoleUserService = new GapRoleUserServiceMock();
            IGapGroupUserService _gapGroupUserService = new GapGroupUserServiceMock();

            INotificationService _notificationService = new NotificationServiceMock();

        UsersController usersController = new UsersController(userService, projectService, _authenticationService, _projectUserService, _gapRoleUserService, _gapGroupUserService, _notificationService);

            OkNegotiatedContentResult<IEnumerable<Project>> result = await usersController.GetProjects() as OkNegotiatedContentResult<IEnumerable<Project>>;

            Assert.IsNotNull(result);
            List<Project> projects = new List<Project>(result.Content);
            Assert.IsTrue(projects.Count == 1 && projects[0].Name == "Mock Project");
        }
    }
}
