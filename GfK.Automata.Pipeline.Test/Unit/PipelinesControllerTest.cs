﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

using GfK.Automata.Pipeline.API.Controllers;
using GfK.Automata.Pipeline.API.Identity;
using GfK.Automata.Pipeline.Data.Migrations;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Test.Mock;
using System.Web.Http;
using GfK.Automata.Pipeline.API.HTTPStatusExtensions;
using Newtonsoft.Json.Linq;

namespace GfK.Automata.Pipeline.Test.Unit
{
    [TestClass, Ignore]
    public class PipelinesControllerTest
    {
        private readonly IPipelineService _pipelineService;
        private readonly IPipelineInstanceService _pipelineInstanceService;
        private readonly IPipelineTaskService _pipelineTaskService;
        private readonly PipelinesController _pipelinesController;
        private readonly IUserService _userService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IScheduleService _scheduleService;
        private readonly IGapGroupUserPipelineService _gapGroupUserPipelineService;
        private readonly IPipelineRunRequestService _pipelineRunRequestService;

        public PipelinesControllerTest()
        {
            _pipelineService = new PipelineServiceMock();
            _pipelineInstanceService = new PipelineInstanceServiceMock();
            _userService = new UserServiceMock();
            _pipelineTaskService = new PipelineTaskServiceMock();
            _authenticationService = new AuthenticationService();
            _scheduleService = new SchedulerServiceMock();
            _gapGroupUserPipelineService = new GapGroupUserPipelineServiceMock();
            _pipelineRunRequestService = new PipelineRunRequestServiceMock();
           
            _pipelinesController = new PipelinesController(_pipelineService, _pipelineInstanceService, _userService, _pipelineTaskService
                ,_authenticationService, _scheduleService, _gapGroupUserPipelineService, _pipelineRunRequestService, null, null);
        }

        [TestMethod]
        public async Task GetPipelinesAll_OK()
        {
            OkNegotiatedContentResult<IEnumerable<Model.Pipeline>> result = await _pipelinesController.Get()
                as OkNegotiatedContentResult<IEnumerable<Model.Pipeline>>;

            Assert.IsNotNull(result);
            List<Model.Pipeline> pipelines = new List<Model.Pipeline>(result.Content);
            Assert.IsTrue(pipelines.Count == 1 && pipelines[0].Name == "New pipeline");
        }

        //[TestMethod]
        //public async Task GetPipelineInstancesPresent_OK()
        //{
        //    OkNegotiatedContentResult<IEnumerable<Model.PipelineInstance>> result = await _pipelinesController.GetPipelineInstances(1)
        //        as OkNegotiatedContentResult<IEnumerable<Model.PipelineInstance>>;

        //    Assert.IsTrue(result.Content.Count() == 1);
        //}

        //[TestMethod]
        //public async Task GetPipelineInstancesNonePresent_OK()
        //{
        //    OkNegotiatedContentResult<IEnumerable<Model.PipelineInstance>> result = await _pipelinesController.GetPipelineInstances(2)
        //        as OkNegotiatedContentResult<IEnumerable<Model.PipelineInstance>>;

        //    Assert.IsTrue(result.Content.Count() == 0);
        //}

        [TestMethod]
        public async Task GetPipelineByNameExists()
        {
            JObject context = JObject.Parse("{ProjectID : 22}");

            OkNegotiatedContentResult<Model.Pipeline> result = await _pipelinesController.PostByName("exists", context)
                as OkNegotiatedContentResult<Model.Pipeline>;

            Assert.IsTrue(result.Content.Name == "exists");
        }

        [TestMethod]
        public async Task GetPipelineByNameNotExists()
        {
            JObject context = JObject.Parse("{ProjectID : 22}");

            IHttpActionResult result = await _pipelinesController.PostByName("notExist", context);

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task GetPipelineByNameBadRequest()
        {
            JObject context = JObject.Parse("{AAA : 22}");

            IHttpActionResult result = await _pipelinesController.PostByName("notExist", context);

            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public async Task CreatePipeline_Ok()
        {
            Model.Pipeline pipeline = new Model.Pipeline { Name = "new", ProjectID = 10, PipeLineID = 1 };

            CreatedNegotiatedContentResult<int> result = await _pipelinesController.Post(pipeline)
                as CreatedNegotiatedContentResult<int>;

            Assert.IsTrue(result.Content == 1);
        }

        [TestMethod]
        public async Task CreatePipelineNameInUse()
        {
            Model.Pipeline pipeline = new Model.Pipeline { Name = "new", ProjectID = 2, PipeLineID = 1 };
            _pipelinesController.Request = new HttpRequestMessage();

            ConflictActionResult result = await _pipelinesController.Post(pipeline) as ConflictActionResult;

            Assert.IsInstanceOfType(result, typeof(ConflictActionResult));
            Assert.IsTrue(result.Message == "Pipeline name already in use.");
        }
    }
}
