﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using GfK.Automata.Pipeline.API.Identity;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class AuthenticationServiceMock : IAuthenticationService
    {
        public string GetCurrentlyLoggedInUserName(IPrincipal user)
        {
            return "surveytestor.gfk.com";
        }

        public async Task<string> GetCurrentlyLoggedInUserNameAsync(IPrincipal user)
        {
            return "surveytestor.gfk.com";
        }
    }
}