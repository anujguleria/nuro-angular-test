﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service.Dto;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class PipelineServiceMock : IPipelineService
    {
        public User CurrentlyLoggedInUser { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Task AddLastRun(IEnumerable<Model.Pipeline> pipelines)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddModulePipeline(string name, int moduleID)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddPipeline(Model.Pipeline pipeline)
        {
            throw new NotImplementedException();
        }

        public async Task<int> AddPipeline(Model.Pipeline pipeline, User currentlyLoggedInUser)
        {
            if (pipeline.ProjectID == 1)
            {
                return -1;
            }
            else if (pipeline.ProjectID == 2)
            {
                return -2;
            }
            else
            {
                return 1;
            }
        }

        public Task AddPipelineLastRun(Model.Pipeline pipeline)
        {
            throw new NotImplementedException();
        }

        public Task DeletePipline(int pipelineID)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Model.Pipeline>> FilterToUserAccessible(IEnumerable<Model.Pipeline> pipelinesToFilter)
        {
            throw new NotImplementedException();
        }

        public Task<Model.Pipeline> GetModulePipeline(int moduleID)
        {
            throw new NotImplementedException();
        }

        public Task<Model.Pipeline> GetPipeline(int pipelineId)
        {
            throw new NotImplementedException();
        }

        public Task<Model.Pipeline> GetPipeline(string pipelineName)
        {
            throw new NotImplementedException();
        }

        public async Task<Model.Pipeline> GetPipeline(string pipelineName, User currentlyLoggedInUser, int projectId, int? parentId)
        {
            if (pipelineName == "notExist")
            {
                return null;
            }
            else
            {
                return new Model.Pipeline { Name = "exists", PipeLineID = 1, ProjectID = 22 };
            }
        }

        public async Task<Model.Pipeline> GetPipeline(string pipelineName, User currentlyLoggedInUser, dynamic request)
        {
            if (pipelineName == "notExist")
            {
                return null;
            }
            else
            {
                return new Model.Pipeline()
                {
                    Name = pipelineName,
                    PipeLineID = 1
                };
            }
        }

        public Task<Model.Pipeline> GetPipeline(string pipelineName, int projectId, int? parentId)
        {
            throw new NotImplementedException();
        }

        public Task<Model.Pipeline> GetPipeline(string pipelineName, dynamic request)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PipelineTask>> GetPipelineAvailableParameters(int pipelineID, string direction)
        {
            throw new NotImplementedException();
        }

        public Task<Model.Pipeline> GetPipelineNoTracking(int id)
        {
            throw new NotImplementedException();
        }

        public Task<int?> GetPipelineProjectID(int pipelineID)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Model.Pipeline>> GetPipelines()
        {
            List<Model.Pipeline> pipelines = new List<Model.Pipeline>();

            pipelines.Add(
                new Model.Pipeline()
                {
                    Name = "New pipeline",
                    PipeLineID = 1
                }
            );

            return pipelines.AsEnumerable<Model.Pipeline>();
        }

        public async Task<IEnumerable<Model.Pipeline>> GetPipelinesByProject(int projectId)
        {
            List<Model.Pipeline> pipelines = new List<Model.Pipeline>();

            if (projectId == 1)
            {
                Model.Pipeline pipeline = new Model.Pipeline()
                {
                    PipeLineID = 1,
                    Name = "Mock Pipeline",
                    ProjectID = 1
                };

                pipelines.Add(pipeline);
            }

            return pipelines.AsEnumerable<Model.Pipeline>();
        }

        public Task<IEnumerable<Model.Pipeline>> GetPipelinesByProject(int projectId, User user)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Model.Pipeline>> GetPipelinesByProjectWithLastRun(int projectId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Model.Pipeline>> GetPipelinesByProjectWithLastRun(int projectId, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsPipelineNameInUse(Model.Pipeline pipeline)
        {
            throw new NotImplementedException();
        }

        public Task<bool> NewPipelineParentIsPipeline(dynamic newPipeline)
        {
            throw new NotImplementedException();
        }

        public Task<bool> NewPipelineParentIsProject(dynamic newPipeline)
        {
            throw new NotImplementedException();
        }

        public Task RunPipeline(int pipelineID, dynamic runContext)
        {
            throw new NotImplementedException();
        }

        public Task RunPipeline(int pipelineID, PipelineRunRequestContext runContext)
        {
            throw new NotImplementedException();
        }

        public Task SavePipeline()
        {
            throw new NotImplementedException();
        }

        public Task UpdatePipeline(Model.Pipeline pipeline)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessPipeline(int pipelineID)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessPipeline(Model.Pipeline pipeline)
        {
            throw new NotImplementedException();
        }
    }
}
