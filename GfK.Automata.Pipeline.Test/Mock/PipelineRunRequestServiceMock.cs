﻿using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class PipelineRunRequestServiceMock : IPipelineRunRequestService
    {
        public Task<int> AddPipelineRunRequest(int piplineID)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddPipelineRunRequest(int piplineID, dynamic context)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PipelineRunRequest>> GetPipelineRunRequests()
        {
            throw new NotImplementedException();
        }

        public Task<string> GetRunContext(dynamic context)
        {
            throw new NotImplementedException();
        }

        public Task RemovePipelineRunRequest(PipelineRunRequest pipelineRunRequest)
        {
            throw new NotImplementedException();
        }

        public Task SavePipelineRunRequest()
        {
            throw new NotImplementedException();
        }
    }
}
