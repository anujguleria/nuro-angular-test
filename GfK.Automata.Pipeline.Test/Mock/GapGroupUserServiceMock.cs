﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Test.Mock
{
    class GapGroupUserServiceMock : IGapGroupUserService
    {
        private readonly IGapGroupUserRepository _gapGroupUserRepository;
        private readonly IUnitOfWork _unitOfWork;

        public Task AddGroupUsers(IEnumerable<GapGroupUser> users)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<GapGroupUser>> GetGroupUsers(int gapGroupID)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<GapGroupUser>> GetUserGroups(int userID)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<GapGroupUser>> GetUserGroupsBrief(int userID)
        {
            throw new NotImplementedException();
        }

        public Task RemoveGroupsFromUser(int userID)
        {
            throw new NotImplementedException();
        }

        public Task RemoveUsersFromGroup(int gapGroupID)
        {
            throw new NotImplementedException();
        }

        public Task SaveGapGroupUser()
        {
            throw new NotImplementedException();
        }

        public Task UpdateGroupsUsers(int gapGroupID, IEnumerable<GapGroupUser> usersToAssign)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUsersGroups(int userID, IEnumerable<GapGroupUser> groupsToAssign)
        {
            throw new NotImplementedException();
        }
    }
}
