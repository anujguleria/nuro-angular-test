﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class GapGroupUserPipelineServiceMock : IGapGroupUserPipelineService
    {
        public Task<IEnumerable<int>> GetUserPipelineIds(User user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessPipeline(Model.Pipeline pipeline, User user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessPipeline(int pipelineID, User user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessPipelineProject(int projectID, User user)
        {
            throw new NotImplementedException();
        }
    }
}
