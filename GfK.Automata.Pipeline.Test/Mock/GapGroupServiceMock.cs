﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class GapGroupServiceMock : IGapGroupService
    {
        public Task<int> AddGapGroup(GapGroup gapGroup)
        {
            throw new NotImplementedException();
        }

        public Task DeleteGroup(GapGroup gapGroup)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GapGroupHasProjects(GapGroup gapGroup)
        {
            throw new NotImplementedException();
        }

        public Task<GapGroup> GetGapGroup(int id)
        {
            throw new NotImplementedException();
        }

        public Task<GapGroup> GetGapGroup(string gapName)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<string>> GetGapGroupProjectNames(GapGroup gapGroup)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<GapGroup>> GetGapGroups()
        {
            ICollection<GapGroup> gapGroups = new List<GapGroup>();

            gapGroups.Add(new GapGroup() { GapGroupID = 1, Name = "GapGoup1" });
            gapGroups.Add(new GapGroup() { GapGroupID = 2, Name = "GapGoup2" });

            return gapGroups.AsEnumerable();
        }

        public Task<IEnumerable<GapGroup>> GetGapGroupsForUser(int userID)
        {
            throw new NotImplementedException();
        }

        public Task<GapGroup> GetGapGroupWithUsers(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsGapGroupNameInUse(GapGroup gapGroup)
        {
            throw new NotImplementedException();
        }

        public async Task SaveGapGroup()
        {
            
        }

        public Task UpdateGapGroup(GapGroup gapGroup)
        {
            throw new NotImplementedException();
        }
    }
}
