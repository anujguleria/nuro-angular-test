﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Test.Mock
{
    class GapRoleUserServiceMock : IGapRoleUserService
    {
        private readonly IGapRoleUserRepository _gapRoleUserRepository;
        private readonly IUnitOfWork _unitOfWork;

        public async Task RemoveRolesFromUser(int userID)
        {
            await _gapRoleUserRepository.Delete(p => p.UserID == userID);
        }

        public async Task<IEnumerable<GapRoleUser>> GetUserRoles(int userID)
        {
            IEnumerable<GapRoleUser> gapRoles = await _gapRoleUserRepository.GetMany(p => p.UserID == userID);

            // The above is pulling in large trees of data for GapRoleUsers, will only pull on demand
            foreach (GapRoleUser gapRoleUser in gapRoles)
            {
                gapRoleUser.GapRole.GapRoleUsers = null;
            }

            return gapRoles;
        }

        public async Task SaveGapRoleUser()
        {
            await _unitOfWork.Commit();
        }

        public Task AddUserRoles(int userID, IEnumerable<GapRoleUser> roles)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<GapRoleUser>> IGapRoleUserService.GetUserRoles(int userID)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUsersRoles(int userID, IEnumerable<GapRoleUser> rolesToAssign)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsUserRole(int UserID, string role)
        {
            throw new NotImplementedException();
        }
    }
}
