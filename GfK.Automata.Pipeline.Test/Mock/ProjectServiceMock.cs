﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using System.Linq.Expressions;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class ProjectServiceMock : IProjectService
    {
        public User CurrentlyLoggedInUser { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public async Task<int> CreateProject(Project project)
        {
            return 1;
        }

        public Task DeleteProject(Project project)
        {
            throw new NotImplementedException();
        }

        public Task DeleteProject(Expression<Func<Project, bool>> where)
        {
            throw new NotImplementedException();
        }

        public Task DeleteProject(Project project, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task DeleteProject(Expression<Func<Project, bool>> where, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<string>> GetGroupProjectNames(int groupID)
        {
            throw new NotImplementedException();
        }

        public Task<Project> GetProject(int projectId)
        {
            throw new NotImplementedException();
        }

        public async Task<Project> GetProject(string projectName)
        {
            if (projectName == "Existing Project")
            {
                return new Project()
                {
                    Name = "Existing Project"
                };
            }
            else
            {
                return null;
            }
        }

        public Task<Project> GetProject(Expression<Func<Project, bool>> where)
        {
            throw new NotImplementedException();
        }

        public Task<Project> GetProject(string projectName, User user)
        {
            throw new NotImplementedException();
        }

        public Task<Project> GetProject(int projectID, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<Project> GetProject(Expression<Func<Project, bool>> where, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<List<Model.Pipeline>> GetProjectPipelines(int projectID, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<List<Model.Pipeline>> GetProjectPipelines(int projectID)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            List<Project> projects = new List<Project>();

            Project project = new Project()
            {
                ProjectID = 1,
                Name = "Mock Project"
            };

            projects.Add(project);
            return projects.AsEnumerable<Project>();
        }

        public Task<IEnumerable<Project>> GetProjects(Expression<Func<Project, bool>> where)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Project>> GetProjects(Expression<Func<Project, bool>> where, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Project>> GetProjectsWithPipelines()
        {
            List<Project> projects = new List<Project>();

            Model.Pipeline pipeline = new Model.Pipeline()
            {
                Name = "Mock Pipeline",
                PipeLineID = 1,
                ProjectID = 1
            };

            Project project = new Project()
            {
                ProjectID = 1,
                Name = "Mock Project",
                pipeLines = new List<Model.Pipeline>() { pipeline }
            };

            projects.Add(project);
            return projects.AsEnumerable<Project>();
        }

        public Task<IEnumerable<Project>> GetProjectsWithPipelines(User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<User>> GetProjectUsers(string projectName)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Project>> GetUserProjects(int userId)
        {
            List<Project> projects = new List<Project>();

            Project project = new Project()
            {
                ProjectID = 1,
                Name = "Mock Project"
            };

            projects.Add(project);
            return projects.AsEnumerable<Project>();
        }

        public async Task<IEnumerable<Project>> GetUserProjects(User user)
        {
            List<Project> projects = new List<Project>();

            Project project = new Project()
            {
                ProjectID = 1,
                Name = "Mock Project"
            };

            projects.Add(project);
            return projects.AsEnumerable<Project>();
        }

        public Task<IEnumerable<Project>> GetUserProjects()
        {
            throw new NotImplementedException();
        }

        public Task<bool> ProjectExists(string projectName)
        {
            throw new NotImplementedException();
        }

        public Task SaveProject()
        {
            throw new NotImplementedException();
        }

        public Task UpdateProject(Project project)
        {
            throw new NotImplementedException();
        }

        public Task UpdateProject(Project project, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessProject(int projectID, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessProject(Project project, User currentlyLoggedInUser)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessProject(int projectID)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UserCanAccessProject(Project project)
        {
            throw new NotImplementedException();
        }
    }
}
