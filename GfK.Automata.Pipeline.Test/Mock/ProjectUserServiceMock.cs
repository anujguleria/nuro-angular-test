﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class ProjectUserServiceMock : IProjectUserService
    {
        public Task AddProjectToUser(ProjectUser projectUser)
        {
            throw new NotImplementedException();
        }

        public async Task AddProjectToUser(string userEmail, int projectId)
        {
            
        }

        public Task AddProjectToUser(string userEmail, int projectId, DateTime lastAccess)
        {
            throw new NotImplementedException();
        }

        public Task AddProjectUsers(IEnumerable<ProjectUser> projectUsers)
        {
            throw new NotImplementedException();
        }

        public Task AddUserToProject(ProjectUser projectUser)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<int>> GetProjectsIdsByUser(User user)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<int>> GetProjectsIdsByUserId(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<ProjectUser> GetProjectUser(int projectId, User user)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<int>> GetUserIdsByProjectId(int projectID)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ProjectBelongsToUser(int projectId, User user)
        {
            throw new NotImplementedException();
        }

        public Task RemoveUserFromProject(ProjectUser projectUser)
        {
            throw new NotImplementedException();
        }

        public Task SaveProjectUser()
        {
            throw new NotImplementedException();
        }

        public Task UpdateProjectUser(string userEmail, int projectId)
        {
            throw new NotImplementedException();
        }

        Task<DateTime> IProjectUserService.UpdateProjectUser(string userEmail, int projectId)
        {
            throw new NotImplementedException();
        }
    }
}
