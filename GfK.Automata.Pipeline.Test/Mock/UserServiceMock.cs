﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using System.Linq.Expressions;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class UserServiceMock : IUserService
    {
        public Task<int> CreateUser(User User)
        {
            throw new NotImplementedException();
        }

        public async Task<int> CreateUser(string email)
        {
            return 1;
        }

        public Task DeleteUser(User User)
        {
            throw new NotImplementedException();
        }

        public Task DeleteUser(Expression<Func<User, bool>> where)
        {
            throw new NotImplementedException();
        }

        public async Task<User> GetUser(int UserID)
        {
            User user = new User()
            {
                UserID = 1,
                Email = "mock@gfk.com"
            };

            return user;
        }

        public async Task<User> GetUser(string email)
        {
            User user = new User()
            {
                UserID = 1,
                Email = "mock@gfk.com"
            };

            return user;
        }

        public Task<User> GetUser(Expression<Func<User, bool>> where)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<User>> GetUsers()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<User>> GetUsers(Expression<Func<User, bool>> where)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<User>> GetUsersWithRoles()
        {
            throw new NotImplementedException();
        }

        public Task<User> GetUserWithGroups(int UserID)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsUserAdmin(int UserID)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsUsernameInUse(User user)
        {
            throw new NotImplementedException();
        }

        public Task SaveUser()
        {
            throw new NotImplementedException();
        }

        public Task UpdateCurrentUser(User currentUser, User updatedUserValues)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUser(User User)
        {
            throw new NotImplementedException();
        }
    }
}
