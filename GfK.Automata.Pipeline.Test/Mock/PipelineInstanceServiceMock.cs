﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class PipelineInstanceServiceMock : IPipelineInstanceService
    {
        public Task<int> AddPipelineInstance(PipelineInstance pipelineInstance)
        {
            throw new NotImplementedException();
        }
        public Task<PipelineInstance> GetPipelineInstanceWithLogs(int pipelineInstanceId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<PipelineInstance>> GetInstancesForPipeline(int pipelineId)
        {
            List<PipelineInstance> instances = new List<PipelineInstance>();

            if (pipelineId == 1)
            {
                instances.Add
                (
                    new PipelineInstance()
                    {
                        PipelineInstanceId = 1
                    }
                );
            }

            return instances.AsEnumerable<PipelineInstance>();
        }

        public Task<PipelineInstance> GetPipelineInstance(int pipelineInstanceId)
        {
            throw new NotImplementedException();
        }

        public Task<PipelineInstance> GetPipelineInstanceLastRun(int pipelineId)
        {
            throw new NotImplementedException();
        }

        public Task SavePipelineInstance()
        {
            throw new NotImplementedException();
        }

        public Task UpdatePipelineInstance(PipelineInstance pipelineInstance)
        {
            throw new NotImplementedException();
        }
    }
}
