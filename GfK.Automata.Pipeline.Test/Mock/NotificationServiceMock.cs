﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class NotificationServiceMock : INotificationService
    {
        public Task<Notification> CreateNotification(Notification notification, string status= null)
        {
            throw new NotImplementedException();
        }

        public Task DeleteNotification(int notificationID)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Notification>> GetNotifications(int userID)
        {
            throw new NotImplementedException();
        }

        public Task<Notification> GetNotificationByID(int NotificationID)
        {
            throw new NotImplementedException();

        }
    }
}
