﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using System.Linq.Expressions;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class SchedulerServiceMock : IScheduleService
    {
        public event EventHandler<int> ScheduleFired;

        public Task<int> CreateSchedule(Schedule schedule)
        {
            throw new NotImplementedException();
        }

        public Task<Schedule> GetSchedule(int projectId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Schedule>> GetSchedules(Expression<Func<Schedule, bool>> where)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsScheduledToRunAfter(Schedule schedule, DateTime afterDate)
        {
            throw new NotImplementedException();
        }

        public Task LoadQuartzSchedule(int scheduleID)
        {
            throw new NotImplementedException();
        }

        public Task LoadQuartzSchedule(Schedule schedule)
        {
            throw new NotImplementedException();
        }

        public Task LoadQuartzSchedules()
        {
            throw new NotImplementedException();
        }

        public Task RemoveQuartzSchedule(int scheduleID)
        {
            throw new NotImplementedException();
        }

        public Task RemoveQuartzSchedule(Schedule schedule)
        {
            throw new NotImplementedException();
        }

        public Task ResetReload(int scheduleID)
        {
            throw new NotImplementedException();
        }

        public Task StopQuartzSchedules()
        {
            throw new NotImplementedException();
        }

        public Task UpdateSchedule(Schedule schedule)
        {
            throw new NotImplementedException();
        }
    }
}
