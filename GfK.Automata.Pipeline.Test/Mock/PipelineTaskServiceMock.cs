﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Model.Views;

namespace GfK.Automata.Pipeline.Test.Mock
{
    public class PipelineTaskServiceMock : IPipelineTaskService
    {
        public async Task<int> AddPipelineTask(PipelineTask pipelineTask)
        {
            throw new NotImplementedException();
        }

        public Task DeletePipelineTask(int pipelineTaskID)
        {
            throw new NotImplementedException();
        }

        public Task<PipelineTask> GetPipelineTask(int pipelineID)
        {
            throw new NotImplementedException();
        }

        public Task<PipelineTask> GetPipelineTaskByName(int pipeLineID, string pipelineTaskName)
        {
            throw new NotImplementedException();
        }
        public Task<Model.PipelineTask> GetPipelineTask(string pipelineName, User currentlyLoggedInUser, dynamic request)
        {
            throw new NotImplementedException();
        }

        public Task<PipelineTask> GetPipelineTaskByPosition(int pipeLineID, int afterSequencePosition)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<PipelineTask>> GetPipelineTasks(int pipeineId)
        {
            throw new NotImplementedException();
        }

        public Task MovePipelineTask(int pipelineTaskID, int count)
        {
            throw new NotImplementedException();
        }

        public Task SavePipelineTask()
        {
            throw new NotImplementedException();
        }

        public Task UpdatePipelineTask(PipelineTask pipelineTask)
        {
            throw new NotImplementedException();
        }

        public Task<int?> GetPipelineTaskSequenceNumber(int pipeineTaskID)
        {
            throw new NotImplementedException();
        }

        public Task<int?> GetNextPipelineTaskID(int pipelineID, int afterSequencePosition)
        {
            throw new NotImplementedException();
        }

        public Task<int?> GetFirstPipelineTaskId(int pipelineID)
        {
            throw new NotImplementedException();
        }

        public Task<PipelineTask> GetPipelineTaskWithParameters(int pipelineTaskID)
        {
            throw new NotImplementedException();
        }

        public Task AddSystemPipelineTasks(int pipelineID, int startModuleID, int endModuleID)
        {
            throw new NotImplementedException();
        }

        public Task<PipelineTask> CreatePipelineTaskSystem(string name, int pipelineID, int moduleID, int sequencePosition)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PipelineTask>> GetAvailableParameters(IEnumerable<PipelineHierarchy> pipelineHierarchy, string direction)
        {
            throw new NotImplementedException();
        }
    }
}
