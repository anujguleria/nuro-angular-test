﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;

namespace GfK.Automata.Pipeline.Test
{
    public class PipelineInstanceTestEntities
    {
        public PipelineInstanceTestEntities(IDbFactory dbfactory)
        {
            UnitOfWork = new UnitOfWork(dbfactory);

            PipelineInstanceRepository = new PipelineInstanceRepository(dbfactory);
            PipelineInstanceService = new PipelineInstanceService(PipelineInstanceRepository, UnitOfWork);
        }
        public IUnitOfWork UnitOfWork { get; set; }
        public IPipelineInstanceRepository PipelineInstanceRepository { get; set; }
        public IPipelineInstanceService PipelineInstanceService { get; set; }
    }
}
