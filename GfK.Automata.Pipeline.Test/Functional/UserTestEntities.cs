﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;

namespace GfK.Automata.Pipeline.Test
{
    public class UserTestEntities
    {
        public UserTestEntities(IDbFactory dbfactory)
        {
            UnitOfWork = new UnitOfWork(dbfactory);

            GapRoleUserRepository = new GapRoleUserRepository(dbfactory);
            GapRoleUserService = new GapRoleUserService(GapRoleUserRepository, UnitOfWork);

            GapGroupUserRepository = new GapGroupUserRepository(dbfactory);
            GapGroupUserService = new GapGroupUserService(GapGroupUserRepository, UnitOfWork);

            UserRespository = new UserRepository(dbfactory);
            UserService = new UserService(UserRespository, GapRoleUserService, GapGroupUserService, UnitOfWork);

            ProjectUserRepository = new ProjectUserRepository(dbfactory);
            ProjectUserService = new ProjectUserService(ProjectUserRepository, UnitOfWork, UserService);
        }

        public IUnitOfWork UnitOfWork { get; set; }
        GapGroupUserRepository GapGroupUserRepository { get; set; }
        GapGroupUserService GapGroupUserService { get; set; }
        public GapRoleUserRepository GapRoleUserRepository { get; set; }
        public GapRoleUserService GapRoleUserService { get; set; }
        public IUserRepository UserRespository { get; set; }
        public IUserService UserService { get; set; }
        public IProjectUserRepository ProjectUserRepository { get; set; }
        public IProjectUserService ProjectUserService { get; set; }
    }
}
