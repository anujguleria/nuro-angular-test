﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;

namespace GfK.Automata.Pipeline.Test
{
    public class ProjectUserTestEntities
    {
        public ProjectUserTestEntities(IDbFactory dbfactory)
        {
            UnitOfWork = new UnitOfWork(dbfactory);

            GapRoleUserRepository = new GapRoleUserRepository(dbfactory);
            GapRoleUserService = new GapRoleUserService(GapRoleUserRepository, UnitOfWork);

            IGapGroupUserRepository gapGroupUserRepository = new GapGroupUserRepository(dbfactory);
            IGapGroupUserService gapGroupUserService = new GapGroupUserService(gapGroupUserRepository, UnitOfWork);

            UserRepository = new UserRepository(dbfactory);
            UserService = new UserService(UserRepository, GapRoleUserService, gapGroupUserService, UnitOfWork);

            ProjectUserRepository = new ProjectUserRepository(dbfactory);
            ProjectUserService = new ProjectUserService(ProjectUserRepository, UnitOfWork, UserService);
        }

        public IUnitOfWork UnitOfWork { get; set; }
        public GapRoleUserRepository GapRoleUserRepository { get; set; }
        public GapRoleUserService GapRoleUserService { get; set; }
        public IProjectUserRepository ProjectUserRepository { get; set; }
        public IProjectUserService ProjectUserService { get; set; }
        public IUserRepository UserRepository { get; set; }
        public IUserService UserService { get; set; }
    }
}
