﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;

namespace GfK.Automata.Pipeline.Test
{
    public class PipelineTestEntities
    {
        public PipelineTestEntities(IDbFactory dbfactory)
        {
            UnitOfWork = new UnitOfWork(dbfactory);

            PipelineInstanceRepository = new PipelineInstanceRepository(dbfactory);
            PipelineInstanceService = new PipelineInstanceService(PipelineInstanceRepository, UnitOfWork);

            ProjectPipelineHierarchyRepository = new ProjectPipelineHierarchyRepository(dbfactory);
            ProjectPipelineHierarchyService = new ProjectPipelineHierarchyService(ProjectPipelineHierarchyRepository);

            GapRoleUserRepository = new GapRoleUserRepository(dbfactory);
            GapRoleUserService = new GapRoleUserService(GapRoleUserRepository, UnitOfWork);

            IGapGroupUserRepository gapGroupUserRepository = new GapGroupUserRepository(dbfactory);
            IGapGroupUserService gapGroupUserService = new GapGroupUserService(gapGroupUserRepository, UnitOfWork);

            UserRepository = new UserRepository(dbfactory);
            UserService = new UserService(UserRepository, GapRoleUserService, gapGroupUserService, UnitOfWork);

            ProjectUserRepository = new ProjectUserRepository(dbfactory);
            ProjectUserService = new ProjectUserService(ProjectUserRepository, UnitOfWork, UserService);

            PipelineRepository = new PipelineRepository(dbfactory);
            PipelineService = new PipelineService(PipelineRepository, UnitOfWork, PipelineInstanceService, ProjectPipelineHierarchyService, ProjectUserService, null, null, null, null, null, null);
        }
        public IUnitOfWork UnitOfWork { get; set; }
        public GapRoleUserRepository GapRoleUserRepository { get; set; }
        public GapRoleUserService GapRoleUserService { get; set; }
        public IPipelineRepository PipelineRepository { get; set; }
        public IPipelineService PipelineService { get; set; }
        public IPipelineInstanceRepository PipelineInstanceRepository { get; set; }
        public IPipelineInstanceService PipelineInstanceService { get; set; }
        public IProjectPipelineHierarchyRepository ProjectPipelineHierarchyRepository { get; set; }
        public IProjectPipelineHierarchyService ProjectPipelineHierarchyService { get; set; }
        public IUserRepository UserRepository { get; set; }
        public IUserService UserService { get; set; }
        public IProjectUserRepository ProjectUserRepository { get; set;}
        public IProjectUserService ProjectUserService { get; set; }

    }
}
