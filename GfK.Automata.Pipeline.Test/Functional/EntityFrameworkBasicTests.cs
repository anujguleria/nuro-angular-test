﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Data.Service;
using System.Collections.Generic;
using System.Linq;

namespace GfK.Automata.Pipeline.Test
{
    [TestClass, Ignore]
    public class EntityFrameworkBasicTests
    {
        [TestMethod]
        public async Task TestBasicEFWiring()
        {
            try
            {
                Project project = new Project();
                project.Name = "first test mike2";

                GAPEntities pipe = new GAPEntities();
                Project newProject = pipe.Projects.Add(project);
                await pipe.SaveChangesAsync();

                Assert.IsTrue(true);
            }
            catch (Exception e)
            {
                Assert.Fail();
            }
        }
        [TestMethod]
        public async Task TestBasicEFRetrieve()
        {
            try
            {
                GAPEntities pipe = new GAPEntities();
                Project project = await pipe.Projects.FindAsync(4);

                Assert.IsTrue(project != null);
            }
            catch (Exception e)
            {
                Assert.Fail();
            }
        }
        [TestMethod]
        public async Task TestBasicEFRetrieveSortedAsc()
        {
            try
            {
                GAPEntities pipe = new GAPEntities();
                List<ProjectUser> projectUser = pipe.ProjectUsers.Where(p => p.LastAccess < DateTime.Now).OrderBy(p => p.LastAccess).ToList();

                Assert.IsTrue(projectUser != null);
            }
            catch (Exception e)
            {
                Assert.Fail();
            }
        }
        [TestMethod]
        public async Task TestBasicEFRetrieveSortedDesc()
        {
            try
            {
                GAPEntities pipe = new GAPEntities();
                List<ProjectUser> projectUser = pipe.ProjectUsers.Where(p => p.LastAccess < DateTime.Now).OrderByDescending(p => p.LastAccess).ToList(); ;

                Assert.IsTrue(projectUser != null);
            }
            catch (Exception e)
            {
                Assert.Fail();
            }
        }
        [TestMethod]
        public async Task TestCreateProject()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                Project project = new Project();
                project.Name = "Test new format another1";

                int projectID = await testEntities.ProjectService.CreateProject(project);
            }
        }

        [TestMethod]
        public async Task TestGetProjectByID()
        {

            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                Project project = await testEntities.ProjectService.GetProject(4);

                Assert.IsTrue(project != null);
            }
        }

        [TestMethod]
        public async Task TestGetProjectByName()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                Project project = await testEntities.ProjectService.GetProject(1);

                Assert.IsTrue(project != null);
            }
        }

        [TestMethod]
        public async Task TestGetProjectByWhereSuccess()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                Project project = await testEntities.ProjectService.GetProject((x => x.Name.Length > 5));

                Assert.IsTrue(project != null);
            }
        }

        [TestMethod]
        public async Task TestGetProjectByWhereFail()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                Project project = await testEntities.ProjectService.GetProject((x => x.Name.Length > 75));

                Assert.IsTrue(project == null);
            }
        }

        [TestMethod]
        public async Task TestGetProjects()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                IEnumerable<Project> projects = null; // await testEntities.ProjectService.GetProjects(new User());

                Assert.IsTrue(projects != null);
            }
        }

        [TestMethod]
        public async Task TestGetProjectsWhereSuccess()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                IEnumerable<Project> projects = await testEntities.ProjectService.GetProjects((x => x.Name.Trim().Length == 11));

                Assert.IsTrue(projects.Count() > 0);
            }
        }

        [TestMethod]
        public async Task TestGetProjectsWhereFail()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                IEnumerable<Project> projects = await testEntities.ProjectService.GetProjects((x => x.Name.Trim().Length == 12));

                Assert.IsTrue(projects.Count() == 0);
            }
        }

        [TestMethod]
        public async Task TestUpdateProject()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);
                Project project = new Project();
                project.Name = "updated name2";
                project.ProjectID = 3;

                try
                {
                    await testEntities.ProjectService.UpdateProject(project);
                    await testEntities.ProjectService.SaveProject();
                }
                catch (Exception e)
                {
                    int x = 12;
                }


                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public async Task TestDeleteProject()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);

                Project project = new Project();
                project.ProjectID = 8;

                await testEntities.ProjectService.DeleteProject(project);
                await testEntities.ProjectService.SaveProject();

                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public async Task TestDeleteProjectWhereSuccess()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);

                await testEntities.ProjectService.DeleteProject((x => x.ProjectID > 10));
                await testEntities.ProjectService.SaveProject();

                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public async Task TestDeleteProjectWhereFail()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);

                await testEntities.ProjectService.DeleteProject((x => x.ProjectID < 3));
                await testEntities.ProjectService.SaveProject();

                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public async Task TestCreateUser()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                UserTestEntities testEntities = new UserTestEntities(dbfactory);
                User user = new User();
                user.Email = "email3&gfk.com";

                int newUserID = await testEntities.UserService.CreateUser(user);
            }

            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task TestAddProjectUser()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectUserTestEntities testEntities = new ProjectUserTestEntities(dbfactory);

                ProjectUser newProjectUser = new ProjectUser();
                newProjectUser.ProjectID = 3;
                newProjectUser.UserID = 1;
                newProjectUser.LastAccess = DateTime.Now;

                await testEntities.ProjectUserService.AddProjectToUser(newProjectUser);
            }
        }

        [TestMethod]
        public async Task TestGetProjectIdsForUser()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectUserTestEntities testEntities = new ProjectUserTestEntities(dbfactory);

                IEnumerable<int> projectsForUser = await testEntities.ProjectUserService.GetProjectsIdsByUserId(1);
            }
        }

        [TestMethod]
        public async Task TestGetProjectsForUser()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                ProjectTestEntities testEntities = new ProjectTestEntities(dbfactory);

                List<Project> projects = (await testEntities.ProjectService.GetUserProjects()).ToList();    
            }
        }

        [TestMethod]
        public async Task TestAddPipeline()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                PipelineTestEntities testEntities = new PipelineTestEntities(dbfactory);

                Model.Pipeline pipeline = new Model.Pipeline();
                pipeline.Name = "test pipeline format";
                pipeline.ProjectID = 4;

                int pipelineId = await testEntities.PipelineService.AddPipeline(pipeline);
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public async Task TestGetPipeline()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                PipelineTestEntities testEntities = new PipelineTestEntities(dbfactory);

                Model.Pipeline pipeline = await testEntities.PipelineService.GetPipeline(1);
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public async Task TestGetPipelineProjectId()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                PipelineTestEntities testEntities = new PipelineTestEntities(dbfactory);

                IEnumerable<Model.Pipeline> pipelines = await testEntities.PipelineService.GetPipelinesByProject(4);
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public async Task TestAddPipelineInstance()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                PipelineInstanceTestEntities testEntities = new PipelineInstanceTestEntities(dbfactory);

                PipelineInstance pipelineInstance = new PipelineInstance();
                pipelineInstance.PipelineId = 1;
                pipelineInstance.Started = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                int pipelineInstanceId = await testEntities.PipelineInstanceService.AddPipelineInstance(pipelineInstance);            
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public async Task TestGetPipelineInstancesForPipeline()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                PipelineInstanceTestEntities testEntities = new PipelineInstanceTestEntities(dbfactory);

                IEnumerable<PipelineInstance> pipelineInstances = await testEntities.PipelineInstanceService.GetInstancesForPipeline(1);
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public async Task TestUpdatePipelineInstance()
        {
            using (IDbFactory dbfactory = new DBFactory())
            {
                PipelineInstanceTestEntities testEntities = new PipelineInstanceTestEntities(dbfactory);

                PipelineInstance targetPipelineinstance = await testEntities.PipelineInstanceService.GetPipelineInstance(2);
                targetPipelineinstance.Ended = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                await testEntities.PipelineInstanceService.UpdatePipelineInstance(targetPipelineinstance);
                Assert.IsTrue(true);
            }
        }
    }
}