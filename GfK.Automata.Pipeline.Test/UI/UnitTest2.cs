﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace GfK.Automata.Pipeline.Test.UI
{
    [TestClass, Ignore]
    public class UnitTest2
    {
        IWebDriver driver;
        [TestMethod]
        public void TestMethod2()
        {
            Assert.AreEqual("Hello World!", driver.FindElement(By.XPath("//body/span")).Text);
        }
        [TestInitialize]
        public void Initialize()
        {
            // todo - we should probably move this to a base class and figure out a way to pull browser type and url from config file
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://localhost:5344/");
        }
        [TestCleanup]
        public void Cleanup()
        {
            driver.Close();
        }
    }

}
