REM - create database deploy folder if it doesn't exist
IF EXIST %1\DatabaseDeploy GOTO DIRCREATED

mkdir %1\DatabaseDeploy

:DIRCREATED

REM - copy migrate exe
%systemroot%\System32\xcopy %1\packages\EntityFramework.6.1.3\tools\Migrate.exe %1\DatabaseDeploy /s /f /y /i

REM - copy all dlls from build directory of data project
%systemroot%\System32\xcopy %1\GfK.Automata.Pipeline.Data\bin\%2 %1\DatabaseDeploy /s /f /y /i

REM - run db migrations
%1\DatabaseDeploy\Migrate.exe GfK.Automata.Pipeline.Data.dll /connectionString="Data Source=nuew-sqaa45.ext.gfk;Initial Catalog=GAP_%2;Integrated Security=SSPI" /connectionProviderName="System.Data.SqlClient" /startupConfigurationFile=%1\DatabaseDeploy\GfK.Automata.Pipeline.Data.dll.config

REM - not sure if we need this but just in case
if %errorlevel% neq 0 exit /b %errorlevel%