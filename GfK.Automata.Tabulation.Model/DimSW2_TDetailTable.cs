﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSW2_TDetailTable: DimDetailTable
    {
        // All of the tables below need to be computed for each Priority Brand that is mentioned in PURLAST_T.
        public DimSW2_TDetailTable(string language, string context, string desc, Category brand, BoundField f, Container c = null ) : base(language, context, desc, f, c, null)
        {
            //  Standard table. 
            //  Net: "Switched from another[CATEGORY]"[All numbered brands in list.Include "other"(97) but exclude "I did not switch"(99)]
            //  desc = [CATEGORY] Switched from Before a Customer of[BRAND FROM PURLAST_T]
            Elements = new DimAxisElements();
            Side = SideAxis(brand);
            FormatDescription("[BRAND FROM PURLAST_T]", brand);
        }
        public string SideAxis(Category brand)
        {
            List<Category> cats = ((BoundField)Field).Categories();
            foreach (Category cat in cats)
            {
                _AddAxisElement(cat);
            }
            Elements.Add(new DimAxisElement()
            {
                ID = "netSwitch",
                Label = "Switched from another [CATEGORY]",
                Expression = "Not " + Field.Name + "={" + brand.Name + "}"
            });

            Elements.AddBaseElement(Base);
            string side = "axis({" + Elements.ToScript() + "})";
            
            return side;
        }
        private void _AddAxisElement(Category cat)
        {
            DimAxisElement e = new DimAxisElement()
            {
                ID = cat.Name,
                Label = cat.Label(Context, Language, cat.Document, true),
                Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, cat)
            };
            switch (cat.Name)
            {
                case "OtherS":
                    break;
                case "DidNot":
                    e.AddProperty("IncludeInBase", "False");
                    e.Properties[DimAxisElement.pIsHidden] = true;
                    break;
            }
            Elements.Add(e);
        }
    }
}
