﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAE5_MDetailTable : DimAggregatedTable
    {
        private bool _hideCats = true;
        private List<string> _CatList { get; set; }
        public DimAE5_MDetailTable(string language, string context, string description, BoundField f, Container c) : base(language, context, description, f, c, null)
        {
            _setCatLst();
            Base = "Exposed to ad and recall ad";
            Side = SideAxis();
            // AE5, AE7:
            FormatDescription("Attribute", c);
            FormatDescription("Ad", (Container)c.Document.Find("AE_Loop"));

        }

        
        public string SideAxis()
        {
            DimAxisElement e = null;
            DimAxisElements elements = new DimAxisElements();
            elements.AddBaseElement("Total Respondents");
            string[] varParts = Field.Name.Split('.');
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(Field.Items, true);
            string sideAxis = Field.Name + "{";
            foreach(Category c in allCats)
            {
                e = new DimAxisElement(c.Name);
                e.Label = _CatList[((BoundField)Field).Categories().IndexOf(c)];
                e.Type = ExpressionType.etNone;
                elements.Add(e);
            }
            e = new DimAxisElement("N1", "Net Somewhat/Strongly Agree (4,3)");
            e.Type = ExpressionType.etExpression;
            e.Expression = DimAxisHelper.ContainsAnyExpression("[" + varParts.Last() + "]", allCats.Take(2));
            elements.Add(e);

            e = new DimAxisElement("N2", "Net Somewhat/Strongly Disagree (1,2)");
            e.Type = ExpressionType.etExpression;
            e.Expression = DimAxisHelper.ContainsAnyExpression("[" + varParts.Last() + "]", allCats.Skip(2));
            elements.Add(e);

            sideAxis += elements.ToScript() + "}";
            
            return sideAxis;
        }
        private void _setCatLst()
        {
            // AE5 & AE7 both use same scale here.
            // 4 = Strongly agree; 3 = Somewhat agree; 2 = Somewhat disagree; 1 = Strongly disagree
            _CatList = new List<string>();
            _CatList.Add("Strongly disagree");        //_1
            _CatList.Add("Somewhat disagree");
            _CatList.Add("Somewhat agree");
            _CatList.Add("Strongly agree");          //_4
        }
    }
}
