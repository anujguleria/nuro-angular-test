﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimDefaultDetailTable: DimDetailTable
    {
        public DimDefaultDetailTable(string language, string context, string desc, BoundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            Side = _DefaultContainerSideAxis();
        }
        public DimDefaultDetailTable(string language, string context, string desc, UnboundField f, DimFilter filter) : base(language, context, desc, f, null, filter)
        {
            Side = base.NumericSideAxis((UnboundField)Field);
        }
        public DimDefaultDetailTable(string language, string context, string desc, BoundField f, DimFilter filter) : base(language, context, desc, f, null, filter)
        {
            switch (Field.DataType)
            {
                case DataType.dtBoolean:
                    Side = base.BooleanSideAxis();
                    break;
                case DataType.dtCategorical:
                default:
                    Side = base.CategorySideAxis(0, false, false);
                    break;
            }
        }

        private string _DefaultContainerSideAxis()
        {
            // returns a table for the individual grid slice
            Elements = CreateElements((BoundField)Field);
            string sideAxis = "axis({" + Elements.ToScript() + "})";
            return sideAxis;
        }

    }
}
