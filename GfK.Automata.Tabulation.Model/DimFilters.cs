﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimFilters 
    {
        // Using this class to instantiate "global" filters that may need to be re-used
        // across several table types


        public Dictionary<string, DimFilter> Items { get; protected set; } 
        public MetadataDocument Document { get; protected set; }
        public DimFilters(MetadataDocument doc)
        {
            Document = doc;
            Items = new Dictionary<string, DimFilter>();
            Items.Add("Version1",
                        new DimFilter("Version1", "FMCG (Version =1)", DimFilterExpressionBuilder.VariableContains("HIDDEN_VERSION", "_1", Document)));

            Items.Add("Version2",
                        new DimFilter("Version2", "Tech (Version =2)", DimFilterExpressionBuilder.VariableContains("HIDDEN_VERSION", "_13", Document)));

            
        }
    }
}
