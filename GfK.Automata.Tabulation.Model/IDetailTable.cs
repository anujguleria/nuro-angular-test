﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    interface IDetailTable
    {
        string CategorySideAxis(Int32 topCount = 0, bool hideCats = false, bool sortDescending = false);
        string CategorySideAxis(Container container, Int32 topCount = 0, bool hideCats = false, bool sortDescending =false);
        string NumericSideAxis(UnboundField f);
    }
}
