﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public class ProjectType
    {
        public const string
            FMCG = "{_1}",
            TECH = "{_13}",
            NONE = "";
        public ProjectType()
        {
            
        }
        
    }
}
