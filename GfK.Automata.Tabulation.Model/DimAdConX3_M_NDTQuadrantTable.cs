﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAdConX3_M_NDTQuadrantTable: DimQuadrantTable
    {
        public DimAdConX3_M_NDTQuadrantTable(string desc, Category brand, Int32 x, Int32 y, Container c, DimFilter filter) : base(desc, brand, x, y, c, filter)
        {
            Base = "Exposed to ad";
            CreateElements(brand);
            Side = SideAxis();
            // FormatDescription("[BRAND]", DimAxisHelper.CleanText(brand.Label(ProcessorConfiguration.Context, ProcessorConfiguration.Language, Container.Document)));
            Annotations.Add(new DimAnnotation(AnnotationType.atFooter, "Ad ConX Score = [% Quadrant 2 (Memorable & Positive) - % Quadrant 1 (Memorable & Negative)]"));
        }
        public void CreateElements(Category ad)
        {

            Elements = new DimAxisElements();
            string xName = DimAxisHelper.GetQuadrantField((Container)Container.Items[0], Container.CategoryMap[0], "X").Name;
            string yName = DimAxisHelper.GetQuadrantField((Container)Container.Items[0], Container.CategoryMap[0], "Y").Name;

            Elements.Add(new DimAxisElement(hide:false)
            {
                ID = "b_" + ad.Name,
                Label = "Base: " + DimAxisHelper.GetLabel(DimAdConXController.Container, ad),
                Type = ExpressionType.etBase,
                Expression = DimAxisHelper.ContainsAnyExpression(DimAdConXController.Iterator, ad)
            });

            Elements.Add(new DimAxisElement()
            {
                ID = "q1" + ad.Name,
                Label = "Quadrant 1 (Vivid & Neg.)",
                Expression = String.Format("{0} <= {1} And {2} > {3}", xName, X, yName, Y)
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "q2" + ad.Name,
                Label = "Quadrant 2 (Vivid & Pos.)",
                Expression = String.Format("{0} > {1} And {2} > {3}", xName, X, yName, Y)
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "q3" + ad.Name,
                Label = "Quadrant 3 (Forgettable & Neg.)",
                Expression = String.Format("{0} > {1} And {2} <= {2}", xName, X, yName, Y)
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "q4" + ad.Name,
                Label = "Quadrant 4 (Forgettable & Pos.)",
                Expression = String.Format("{0} <= {1} And {2} <= {3}", xName, X, yName, Y)
            });
            Elements.Add(new DimAxisElement(decimals: 1)
            {
                ID = "adConX" + ad.Name,
                Label = "Ad ConX Score - " + DimAxisHelper.GetLabel(Container, ad),
                Type = ExpressionType.etDerived,
                Expression = String.Format(" q2{0} - q1{0} ", ad.Name)
            });

        }
    }
}
