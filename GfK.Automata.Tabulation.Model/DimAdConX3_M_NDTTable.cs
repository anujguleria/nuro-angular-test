﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAdConX3_M_NDTTable: DimSummaryTable
    {
        public BoundField Iterator { get; set; }
        Container TopContainer { get; set; }
        Category AE_LoopAd { get; set; }
        public DimAdConX3_M_NDTTable(string description, Category ad, BoundField iterator, Container top, Container c, DimFilter filter): base(description,c,top,filter)
        {
            AE_LoopAd = ad;
            Iterator = iterator;
            TopContainer = top;
            Base = "Respondents who rated brand";
            CreateElements();
            Side = SideAxis(AxisType.atAxis);

        }
        public void CreateElements()
        {
            Elements = new DimAxisElements();
            //IEnumerable < Category > = Container.CategoryMap.Where(o => DimRBXController.Iterator.Categories().Contains(o));
            List<Category> cats = Iterator.Categories();
            foreach (Category ad in cats)
            {
                // IEnumerable<UnboundField> xFields = (from i in brandFields where (i.Name.EndsWith("PercentX")) select (UnboundField)i);

                // AE_Loop[{Ad1}].ADCONX3_M.ConX_Loop[{Ad}].ConX[{_2}].PercentX

                Category LevelCategory = TopContainer.CategoryMap[TopContainer.CategoryMap.IndexOf(AE_LoopAd)];
                
                UnboundField xField = DimAxisHelper.GetQuadrantField((Container)Container.Items[0], ad, LevelCategory, "X"); // xFields.ToList().First();
                UnboundField yField = DimAxisHelper.GetQuadrantField((Container)Container.Items[0], ad, LevelCategory, "Y");
                Elements.Add(BrandBaseElement(Iterator, ad));
                Elements.Add(XMeanElement(ad, xField));
                Elements.Add(YMeanElement(ad, yField));
            }
            
        }

        public DimAxisElement BrandBaseElement(BoundField Iterator, Category ad)
        {
            string name = ad.Name;
            string label = ad.Name + " " + DimAxisHelper.GetLabel(Iterator, ad);
            string[] parts = DimAxisHelper.GetStripVarParts(Iterator);

            string iteration = String.Format("{0}[{1}]", parts[0], AE_LoopAd.Name) + "." + parts[1];
            DimAxisElement e = new DimAxisElement()
            {
                ID = "b_" + name,
                Label = "Base: "+ label,
                Type = ExpressionType.etBase,
                Expression = "^.^.^." + DimAxisHelper.ContainsAnyExpression(iteration, ad)
            };
            return e;
        }
        public DimAxisElement XMeanElement(Category ad, UnboundField f)
        {
            DimAxisElement e = new DimAxisElement()
            {
                ID = "_mx" + ad.Name,
                Label = DimAxisHelper.GetLabel(DimAdConXController.Container, ad) + " Mean Impression (x)",
                Type = ExpressionType.etMean,
                Expression = f.Name
            };
            return e;
        }
        public DimAxisElement YMeanElement(Category ad, UnboundField f)
        {
            DimAxisElement e = new DimAxisElement()
            {
                ID = "_my" + ad.Name,
                Label = DimAxisHelper.GetLabel(DimAdConXController.Container, ad) + " Mean Vividness (y)",
                Type = ExpressionType.etMean,
                Expression = f.Name
            };
            return e;
        }
    }
}
