﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSBAWMeanDetailTable: DimDetailTable
    {
        
        public DimSBAWMeanDetailTable(string language,string context, string description, BoundField SBAW1codes, Container container, DimFilter filter) : base(language, context, description, (BoundField)null, container, filter)
        {
            Base = "All respondents";
            QTypeModifier = "MEAN";
            CreateElements();
            Side = SideAxis();

        }
        public DimSBAWMeanDetailTable(string language, string context, string description, Container container, DimFilter filter) : base(language, context, description, (BoundField)null, container, filter)
        {
            // overload which is borrowed by SBAWBenchmarkTable
            Base = "All respondents";
            QTypeModifier = "MEAN";
            CreateElements();
        }
        public string SideAxis()
        {
            return Container.Name + "{" + Elements.ToScript() + "}";
        }
        public void CreateElements()
        {
            Elements = new DimAxisElements();
            foreach(UnboundField f in Container.Items)
            {

                Category brand = Container.CategoryMap[Container.IndexOf(f)];
                if (InvalidBrandNames.Contains(brand)) { continue; }
                if (brand.IsOtherCategory()) { continue; }
                string brandLabel = brand.Label(Context, Language, Container.Document, true);
                Elements.Add(new DimAxisElement()
                {
                    ID = "e"+brand.Name,
                    Label = brandLabel,
                    Type = ExpressionType.etMean,
                    Expression = f.Name
                });
            }
            Elements.AddBaseElement(Base);
        }
    }
}
