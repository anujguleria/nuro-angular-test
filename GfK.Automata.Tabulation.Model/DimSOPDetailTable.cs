﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSOPDetailTable : DimDetailTable
    {
        private Int32 _MinVal { get; set; }
        private Int32 _MaxVal { get; set; }
        private List<string> _CatList { get; set; }
        private List<string> _bandLabels { get; set; }
        public DimSOPDetailTable(string language, string context, string desc, UnboundField f, Container c, List<string> bandLabels, List<Int32>bands, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            Base = "Total purchases";
            _setCatLst();
            AddQTypeMod("SEG");
            Side = BuyerSegmentDetailTable(c, f, bandLabels, bands);
            FormatDescription("Brand", c);
            
        }
        public DimSOPDetailTable(string language, string context, string desc, UnboundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            Base = "Total purchases";
            _MinVal = f.MinValue;
            _MaxVal = f.MaxValue;
            Side = SOPDistributionBandSideAxis();
            FormatDescription("Brand", c);
        }
        public DimSOPDetailTable(string language, string context, string desc, string verb, UnboundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            Base = "Total purchases";
            AddQTypeMod("SUM");
            Side = SOPMeanMultiplier(f, 10);
            FormatDescription("Brand", c);
        }

        public string SOPDistributionBandSideAxis()
        {
            
            DimAxisElements elements = new DimAxisElements();
            string[] varParts = Field.Name.Split('.');
            // create individual elements from 1 to 10
            // separate logic for 0 element
            for (Int32 i= 1; i <= _MaxVal; i++)
            {
                string exp = "^." + Field.Name + "=" + i.ToString();
                string lab = String.Format("{0}%", (i*10));
                DimAxisElement e = new DimAxisElement()
                {
                    ID = "e" + elements.Items.Count.ToString(),
                    Label = lab,
                    Expression = exp
                };
                e.AddProperty(DimAxisElement.pMultiplier, Field.Name);
                elements.Add(e);
            }
            // net ranges.
            DimAxisElements netElements = new DimAxisElements();
            IEnumerable < DimAxisElement > netItems = elements.Take(3);
            string itemExp = String.Join(",", (from e in netItems select e.ToScript()));
            DimAxisElement s = new DimAxisElement();
            s.Type = ExpressionType.etNet;
            s.ID = "subnet1";
            s.Label = "Subnet: 10% - 30%";
            s.Expression = itemExp;
            s.AddProperty(DimAxisElement.pMultiplier, Field.Name);
            netElements.Add(s);

            netItems = elements.Skip(3).Take(3);
            itemExp = String.Join(",", (from e in netItems select e.ToScript()));
            s = new DimAxisElement();
            s.Type = ExpressionType.etNet;
            s.ID = "subnet2";
            s.Label = "Subnet: 40% - 60%";
            s.Expression = itemExp;
            s.AddProperty(DimAxisElement.pMultiplier, Field.Name);
            netElements.Add(s);


            netItems = elements.Skip(6);
            itemExp = String.Join(",", (from e in netItems select e.ToScript()));
            s = new DimAxisElement();
            s.Type = ExpressionType.etNet;
            s.ID = "subnet3";
            s.Label = "Subnet: 70% - 100%";
            s.Expression = itemExp;
            s.AddProperty(DimAxisElement.pMultiplier, Field.Name);
            netElements.Add(s);

            // Insert Net ANY at top of table
            itemExp = String.Join(",", (from e in netElements select e.ToScript()));
            DimAxisElement net1 = new DimAxisElement();
            net1.Type = ExpressionType.etNet;
            net1.ID = "_net1";
            net1.Label = "Net: Any purchases";
            net1.Expression = itemExp;
            net1.AddProperty(DimAxisElement.pMultiplier, Field.Name);
            Elements.Add(net1);

            // Zero element
            DimAxisElement zero = new DimAxisElement()
            {
                ID = "_zero",
                Label = "No purchases",
                Type = ExpressionType.etDerived,
                Expression = "b - " + net1.ID
            };
            Elements.Add(zero);

            // Base element
            Elements.AddBaseElement(Base);
            string baseMultiplier = Container.Name + "." + varParts.Last();
            Elements.Items[0].AddProperty(DimAxisElement.pMultiplier, baseMultiplier);

            string sideAxis = baseMultiplier + "{";
            sideAxis += Elements.ToScript();
            sideAxis += "}";
            return sideAxis;
        }

        public string SOPMeanMultiplier(UnboundField f, Int32 multiplyBy)
        {

            //SOP_GRID[{_1}].SOP{'Base: All respondents' base(),  m 'Mean' mean(SOP_GRID[{_1}].SOP) [Decimals=2] [IsHidden=True], eDerived 'Derived category' derived('m * 10')}
            string sideExp = Field.Name + "{";
            DimAxisElements elements = new DimAxisElements();
            elements.AddBaseElement(Base);

            DimAxisElement e = new DimAxisElement(id:"m", lab:"Mean", hide:true);
            e.Type = ExpressionType.etMean;
            e.Expression = Field.Name;
            e.Properties[DimAxisElement.pDecimals] = 2;
            elements.Add(e);

            e = new DimAxisElement(id: "eDerived", lab: "Mean Percentage");
            e.Type = ExpressionType.etDerived;
            e.Expression = "m * " + multiplyBy.ToString();
            elements.Add(e);

            sideExp += elements.ToScript() + "}";
            return sideExp;
        }

        public string BuyerSegmentDetailTable(Container c, UnboundField f, List<string> labels, List<Int32> bands)
        {
            string[] varParts = DimAxisHelper.GetVarParts(f);
            string gridPart = varParts[1];
            string sideExp = _bandExpression(gridPart, labels, bands);
            return sideExp;
        }
        private string _bandExpression(string fName, List<string> bandLabels, List<Int32> bands)
        {
            /*
             * takes a List of Int32 and creates banded expression
             * example: List<Int32> bands = new List<Int32>(new Int32[] { 0, 2, 4, 6, 8, 10 });
             * 
             * output: _0 '0 to < 2' expression('[SOP] >= 0 AND [SOP] < 2'), _1 '2 to < 4' expression('[SOP] >= 2 AND [SOP] < 4'), _2 '4 to < 6' expression('[SOP] >= 4 AND [SOP] < 6'), _3 '6 to < 8' expression('[SOP] >= 6 AND [SOP] < 8'), _4 '8 to < 10' expression('[SOP] >= 8 AND [SOP] < 10')
             * 
             * this expression is then appended to an axis expression
             */
            if (bands == null)
            {
                bands = new List<Int32>(new Int32[] { 0, 3, 5, 7 });
            }
            string var = Field.Name.Split('.').Last();
            List<string> expressions = new List<string>();

            DimAxisElements elements = new DimAxisElements();
            // base
            elements.AddBaseElement(Base);
            string exp = "";
            // non-buyers
            exp = "[" + var + "]=" + bands[0].ToString();
            elements.Add(new DimAxisElement("_non", _CatList[0], exp));


            GlobalFilter fltr = new GlobalFilter("Buyer_Segment_1", ProjectType.FMCG);
            fltr.Description = "Non-buyers";
            fltr.Expression = "SUM(" + Container.Name + ".(" + var + "=0))";
            ProcessorConfiguration.GlobalFilters.Add(fltr);
            
            for (Int32 i=1;i< bands.Count; i++)
            {
                DimAxisElement e = new DimAxisElement();
                // non-buyer
                e.ID = "_" + i.ToString();
                e.Label = bandLabels[i];
                string filterExp = "";
                
                if (bands[i] != bands.Last())
                {
                    exp = "[{0}] > {1} AND [{0}] <= {2}";
                    exp = String.Format(exp, var, bands[i-1], bands[i]);
                    filterExp = "SUM(" + Container.Name + ".(" + var + " > " + bands[i - 1].ToString() + "))";
                    filterExp += " AND ";
                    filterExp += "SUM(" + Container.Name + ".(" + var + " <=" + bands[i].ToString() + "))";
                }
                else
                {
                    exp = "[{0}] >= {1}";
                    exp = String.Format(exp, var, bands[i]);
                    filterExp = "SUM(" + Container.Name + ".(" + var + " > " + bands[i].ToString() + "))";
                }
                fltr = (new GlobalFilter("Buyer_Segment_" + (i+1).ToString(), ProjectType.FMCG)
                {
                    Description = e.Label,
                    Expression = filterExp
                });
                ProcessorConfiguration.GlobalFilters.Add(fltr);
                e.Expression = exp;
                elements.Add(e);

            }
            Elements = elements;
            string side = Field.Name + "{" + Elements.ToScript() + "}";
            return side;
        }

        private void _setCatLst()
        {
            _CatList = new List<string>();
            _CatList.Add("Non-Buyers");
            _CatList.Add("Light buyers");        
            _CatList.Add("Medium buyers");
            _CatList.Add("Heavy buyers");
                    
        }

    }
}
