﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class ProcessorConfiguration : Dictionary<string, dynamic>
    {
        public static string Language { get; set; } = "ENU";
        public static string Context { get; set; } = "Analysis";

        public static GlobalFilters GlobalFilters { get; set; } = new GlobalFilters();
        public static MetadataDocument Document { get; set; } = null;


    }
}
