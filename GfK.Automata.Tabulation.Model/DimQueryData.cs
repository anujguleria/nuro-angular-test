﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimQueryData
    {
        static DimQueryData()
        {

        }
        public static Int32 GetMin(UnboundField f, string where = "")
        {
            dynamic min = DimOLEDbConnection.GetMinValue(f.Name, @where);
            if (min is null)
            { return f.MinValue; }
            return min;

        }
        public static Int32 GetMax(UnboundField f, string where = "")
        {
            dynamic max = DimOLEDbConnection.GetMaxValue(f.Name, @where);
            if (max is null)
            { return f.MaxValue; }
            return max;
        }
        public static List<Category> GetTopCategories(BoundField f, Int32 numCats = 5, string where = "")
        {
            //if (f.EffectiveMaxValue != null && f.EffectiveMaxValue > 1)
            //{
            //    // return ALL categories, sorted. The calling procedure will determine what to do with them.
            //    return SortedCategories(f, where);
            //}
            //else
            //{
            return _TopCategories(f, numCats, where);
            //}
        }
        public static List<KeyValuePair<Category, Int32>> SortedCategories(BoundField f, string where)
        {
            // returns frequency sorted dict<Category,(string)count>

            // build query
            Dictionary<Category,string> catFreq = DimOLEDbConnection.CategoryFrequencies(f, where);
            //List<Int32> counts = (from v in catFreq.Values select Convert.ToInt32(v)).ToList();
            //counts.Sort();
            //counts.Reverse();
            
            var sorted = (from itm in catFreq orderby Convert.ToInt32(itm.Value) descending select itm);
            List<KeyValuePair<Category, Int32>> cats = new List<KeyValuePair<Category, Int32>>();
            //(from itm in catFreq orderby itm.Value descending select itm).ToList()
            
            foreach (KeyValuePair<Category,string> itm in sorted)
            {
                Int32 n = Convert.ToInt32(itm.Value);
                Category c = itm.Key;
                // Console.WriteLine(itm.Key.Name + " " + itm.Value);
                
                cats.Add(new KeyValuePair<Category, Int32>(itm.Key, n));
            }

            return cats;
        }
        public static List<Category> _TopCategories(BoundField f, Int32 numCats, string where)
        {
            // returns a list of category NAMES from querying OleDbConnection
            if (numCats == 0)
            {
                numCats = f.Categories().Count;
            }
            List<String> catNames = DimOLEDbConnection.GetFreqSortedValues(f.Name, @where, order: DimOLEDbConnection.OrderDesc);
            catNames = _ParseCategories(f, numCats, catNames);
            catNames = catNames.Take(numCats).ToList();
            List<Category> topCats = new List<Category>();
            foreach(string c in catNames)
            {
                foreach (Category cat in f.Categories())
                {
                    if ("{" + cat.Name + "}" == c)
                    {
                        topCats.Add(cat);
                        break;
                    }
                }
            }

            return topCats;
        }

        private static List<String> _ParseCategories(BoundField f, Int32 numCats, List<string>catNames)
        {
            if (f.EffectiveMaxValue != null && f.EffectiveMaxValue > 1)
            {
                // this is a multiple response question
                // currently the query returns not individual responses, but frequency based on total responses to the MR Set.
                // e.g., 
                //  catNames[0] = "{_17,_5,_10,_14,_11}"
                //  catNames[1] = "{_15,_5,_7,_3,_19,_12,_20,_11,_8,_4,_6,_13,_2,_9,_16,_1,_18,_10,_17,_14}"
                //  catNames[2] = "{_1,_3,_2}"
                // etc...
                List<string> parsedCats = new List<string>();
                foreach (string cSet in catNames)
                {
                    List<string> cList = cSet.Replace("{", String.Empty).Replace("}", String.Empty).Split(',').ToList();
                    parsedCats.AddRange((from c in cList select String.Format("{{{0}}}", c)));
                }
                return parsedCats.Distinct().ToList();
            }
            else
            {
                return catNames;
            }
        }

        public static List<Int32> GetValues(UnboundField f, string where= "")
        {
            List<Int32> values = new List<Int32>();
            List<dynamic> vals = (from v in DimOLEDbConnection.GetValues(f.Name, @where).Where(v => !(v is System.DBNull)) select v).ToList();
            //List<Int32> values = (from v in DimOLEDbConnection.GetValues(f.Name, @where) select (Int32)v).ToList();
            return values;
        }
        public static string GetProjectType()
        {
            List<string> vals = GetDistinctValues("HIDDEN_VERSION", "");
            // there should only be ONE value in this variable, as a project will be either one type of project or another
            if (vals.Count() == 1)
            {
                return vals.First();
            }
            return Model.ProjectType.NONE;
        }
        public static bool ColumnIsEmpty(Field f)
        {

            bool ret = DimOLEDbConnection.ColumnIsEmpty(f);
            return ret;
        }
        public static List<string> GetDistinctTextValues(UnboundField f, string where="")
        {
            List<string> values = (from v in DimOLEDbConnection.GetDistinctValues(f.Name, @where).Where(v => !(v is System.DBNull)) select (string)v).ToList();
            return values;
        }
        public static List<string> GetDistinctValues(string columnName, string where="")
        {
            List<string> values = (from v in DimOLEDbConnection.GetDistinctValues(columnName, @where).Where(v => !(v is System.DBNull)) select (string)v).ToList();
            return values;
        }
        public static List<dynamic> GetDistinctValues(Field f, string where="")
        {
            List<dynamic> values = (from v in DimOLEDbConnection.GetDistinctValues(f.Name, @where).Where(v => !(v is System.DBNull)) select v).ToList();
            return values;
        }
        public static List<Int32> GetDistinctValues(UnboundField f, string where="")
        {
            List<Int32> values = (from v in DimOLEDbConnection.GetDistinctValues(f.Name, @where).Where(v => !(v is System.DBNull)) select (Int32)v).ToList();
            return values;
        }
        public static List<string> GetDistinctValues(BoundField f, string where = "")
        {
            List<string> values = (from v in DimOLEDbConnection.GetDistinctValues(f.Name, @where).Where(v => !(v is System.DBNull)) select (string)Convert.ToString(v) ).ToList();
            return values;
        }
        
        public static List<Int32> GetDistinctValues(Container c, string where="")
        {
            List<Int32> values = (from v in DimOLEDbConnection.GetDistinctValues(c, @where).Where(v => !(v is System.DBNull)) select (Int32)v).ToList();
            return values;
        }

        public static string MostRecentMonth(string endDate)
        {
            // returns a 'where' expression based on most recent month backwards from endDate
            // e.g., if endDate = June 16, 2017: clause will contain May 17 through June 16
            DateTime startDate = Convert.ToDateTime(endDate).AddMonths(-1);
            string end = Convert.ToString(endDate);
            string start = Convert.ToString(startDate);
            //"\"" + columnName + "\""
            string exp = "DataCollection.FinishTime >= " + "'" + start + "'" + " AND DataCollection.FinishTime <= " + "'" + end + "'";
            return exp;
        }
    }
}
