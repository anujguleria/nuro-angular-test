﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimDCBR2_M_RDetailTable: DimDetailTable
    {
        public DimDCBR2_M_RDetailTable(string description, Category brand, BoundField field, Container container, DimFilter filter): base(description, brand, field, container, filter)
        {
            Base = "Respondents who were asked the brand";
            CreateElements();
            Side = SideAxis(AxisType.atContainer);
            CreateFilter();
            FormatDescription("[BRAND]", DimAxisHelper.CleanText(brand.Label(Context, Language, Container.Document, true)));
        }
        public void CreateFilter()
        {
            // creates a filter expression for this brand
            // AddFilter(new DimFilter("brandFilter", "", "CBR=" + Brand.Name));
            string[] varParts = Field.Name.Split('.');
            string varName = "[" +varParts.Last() + "]";
            string filterExp = DimAxisHelper.ContainsAnyExpression(varName, Brand);
            AddFilter(new DimFilter("brandFilter", "Brand filter", filterExp, Container.Name));
        }
        public void CreateElements()
        {
            
            /*
            DCBR_M.DCBR2_M{b base(),   
                _weak   'Weak'      expression('LevelID.ContainsAny({Statement1,Statement2,Statement3}) '),
                _strong 'Strong'    expression('LevelID.ContainsAny({Statement4,Statement5,Statement6}) '),
                _atrisk 'At Risk'   expression('LevelID.ContainsAny({Statement7,Statement8,Statement9}) ')
                 } 
            Filter expression: [CBR].ContainsAny({ OldSpice })

            */
            Elements = new DimAxisElements();
            IEnumerable<Category> cats = null;
            string[] varParts = Field.Name.Split('.');
            string varName = "["+varParts.Last()+"]";

            cats = Container.CategoryMap.Take(3);
            Elements.Add(CreateNetElement(cats, "Net: Weak"));
            
            cats = Container.CategoryMap.Skip(3).Take(3);
            Elements.Add(CreateNetElement(cats, "Net: Strong"));
            
            cats = Container.CategoryMap.Skip(6).Take(3);
            Elements.Add(CreateNetElement(cats, "Net: At Risk"));

            Elements.AddBaseElement(Base);
            //Elements.Items[0].Properties[DimAxisElement.pIsHidden] = "false";
        }
        public DimAxisElement CreateNetElement(IEnumerable<Category> cats, string label)
        {
            DimAxisElements elements = new DimAxisElements();
            DimAxisElement netElement = new DimAxisElement();
            // Individual elements deriving the Net, above:
            foreach (Category c in cats)
            {
                elements.Add(new DimAxisElement()
                {
                    ID = "_" + c.Name,
                    Label = c.Label(Context, Language, Container.Document),
                    Expression = DimAxisHelper.ContainsAnyExpression("LevelID",c)
                });
            }
            // the "Net" element:

            netElement = new DimAxisElement()
            {
                ID = "_" + label.Replace(":", String.Empty).Replace(" ",String.Empty),
                Label = label,
                Type = ExpressionType.etNet,
                Expression = String.Join(", ", (from e in elements select e.ToScript()))
            };
            return netElement;
        }
    }
}
