﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimAxisHelper
    {
        public static string Context { get; set; }
        public static string Language { get; set; }
        public static MetadataDocument Document { get; set; }

        static DimAxisHelper()
        {

        }
        public enum FactorType
        {
            ftNone,
            ftFrequency,
            ftIncrement
        }
        static public string GetLabel(Category cat)
        {
            return CleanText(cat.Label(ProcessorConfiguration.Context, ProcessorConfiguration.Language, ProcessorConfiguration.Document));
        }
        static public string GetLabel(MetadataObject obj, Category cat)
        {
            return CleanText(cat.Label(ProcessorConfiguration.Context, ProcessorConfiguration.Language, obj.Document));
        }
        static public UnboundField GetQuadrantField(Container Container, Category brand, string axis)
        {
            IEnumerable<Field> brandFields = (from i in Container.Items where (i.Name.Contains(brand.Name)) select (Field)i);
            switch (axis.ToUpper())
            {
                case "X":
                    IEnumerable<UnboundField> xFields = (from i in brandFields where (i.Name.EndsWith("PercentX")) select (UnboundField)i);
                    return xFields.First();

                case "Y":
                    IEnumerable<UnboundField> yFields = (from i in brandFields where (i.Name.EndsWith("PercentY")) select (UnboundField)i);
                    return yFields.First();
                default:
                    throw new NotImplementedException();
            }

        }
        static public string[] GetVarParts(MetadataObject obj)
        {
            return obj.Name.Replace("..", String.Empty).Split('.');
        }
        static public string[] GetVarParts(MetadataObject obj, bool includeBrackets)
        {
            string[] parts = GetVarParts(obj);
            if (includeBrackets) { return parts; }

            // get rid of square brackets, if any
            parts = (from p in parts select p.Replace("[", String.Empty).Replace("]", String.Empty)).ToArray();
            return parts;
        }
        static public string[] GetStripVarParts(MetadataObject obj)
        {
            Regex re = new Regex(@"\[{.*?}\].");
            return re.Split(obj.Name);
        }
        static public UnboundField GetQuadrantField(Container Container, Category ad, Category level, string axis)
        {
            IEnumerable<Field> adFields = (from i in Container.Items where (i.Name.Contains(level.Name)) select (Field)i);
            string[] cParts = GetVarParts(Container);
            string endsWith = cParts.Last() + "[{" + ad.Name + "}].Percent" + axis.ToUpper();

            IEnumerable<UnboundField> fields = (from i in adFields where (i.Name.EndsWith(endsWith)) select (UnboundField)i);
            return fields.First();
        }
        static public IEnumerable<Category> GetCategories(List<MetadataObject> f, bool sortDesc)
        {
            IEnumerable<Category> allCats = (from GfK.Automata.Model.Category c in f select c).ToList();
            if (sortDesc == true)
            {
                allCats = allCats.OrderByDescending(s => s.Name.Replace("_", ""));
            }
            return allCats;
        }
        static public IEnumerable<Category> GetCategories(List<Category> cats, bool sortDesc)
        {
            IEnumerable<Category> allCats = (from GfK.Automata.Model.Category c in cats select c).ToList();
            if (sortDesc == true)
            {
                allCats = allCats.OrderByDescending(s => s.Name.Replace("_", ""));
            }

            return allCats;
        }
        static public List<string> GetCategoryNames(Container c)
        {

            return GetCategoryNames(c.CategoryMap.ToList());
        }
        static public List<string> GetCategoryNames(BoundField f, bool sortDesc)
        {
            List<Category> cats = GetCategories(f.Categories().ToList(), sortDesc).ToList();
            return GetCategoryNames(cats);
        }
        static public List<string> GetCategoryNames(List<Category> cats)
        {
            return (from Category c in cats select c.Name).ToList();

        }
        static public List<string> GetCategoryNames(IEnumerable<Category> cats)
        {
            return GetCategoryNames(cats.ToList());
        }
        static public string AddTotal()
        {

            return " 'Total' total() ";
        }
        static public string NotAwareExpression(Field f, Container c, string varName)
        {
            Field helperField = (Field)f.Document.Find(varName);
            Category brand = c.CategoryMap[c.IndexOf(f)];
            return "NOT " + ContainsAnyExpression(helperField.Name, brand);
        }
        static public string BenchmarkAvgExpression(List<Category> categories, string label, bool divideByBase = false)
        {
            // Benchmark "Average" computes the arithmetic mean of other categories in this variable
            // e.g.:
            //      derived('(_1+_2+_3)/3')
            //

            // if parameter 'divideByBase' = false, then the derived expression will not take an average from categories.Count
            // but rather from the base count, e.g.:
            // { b base(), _1,_2,_3,'Mean' derived('(_1+_2+_3)/ b ')}

            string exp = "'" + label + "' derived('(";
            for (int i = 1; i <= categories.Count(); i++)
            {
                exp += "@" + i.ToString() + "+";
            }
            // explicit category naming doesn't work if cats are hidden, use @ notation relative.
            // exp += String.Join("+", (from Category c in categories select c.Name).ToList());
            exp = exp.Substring(0, exp.Length - 1);
            exp += ")/";
            if (!divideByBase)
            {
                exp += categories.Count().ToString();
                exp += "')";
            }
            else
            {
                exp += " b ";  // this should be the NAME for base element in calling procedure
                exp += "') [Decimals=5,CountsOnly=True]";
            }

            return exp;
        }

        static public string BenchmarkAvgExpression(List<Category> categories, string label, bool divideByBase, bool inclZeroCounts = false)
        {

            return "";
        }
        static public string ContainsAnyExpression(string varName, IEnumerable<Category> cats)
        {
            string exp = ContainsAnyExpression(varName, GetCategoryNames(cats.ToList()));

            return exp;
        }
        static public string ContainsAnyExpression(string varName, Category cat)
        {
            return ContainsAnyExpression(varName, new List<Category>() { cat });
        }
        static public string ContainsAnyExpression(string varName, IEnumerable<string>cats)
        {
            string exp = "";
            exp = varName + ".ContainsAny({" + String.Join(",", cats) + "})";
            return exp;
        }
        static public string ContainsAnyExpression(BoundField var, Category cat)
        {
            return ContainsAnyExpression(var.Name, cat);
        }
        static public string ContainsAnyExpression(BoundField var, IEnumerable<Category> cats)
        {
            return ContainsAnyExpression(var.Name, cats);
        }
        static public string NetExpression(DimAggregatedTable tbl, string label, IEnumerable<Category> cats, bool hideNetCats = false)
        {
            _SetProperties(tbl); // in case we need to translate labels, etc.
            return _NetExpression(label, cats, hideNetCats);

        }
        static private string _NetExpression(string label, IEnumerable<Category> cats, bool hideNetCats = false)
        {
            string netExp = "'" + label + "' net({";
            foreach (Category cat in cats)
            {
                netExp += cat.Name + " [IsHidden=" + hideNetCats.ToString() + "], ";
            }
            netExp = netExp.Substring(0, netExp.Count() - 2);
            
            return netExp;
        }

        static public string TopBoxNetExp(DimAggregatedTable tbl, IEnumerable<Category> allCats, Int32 topCount, bool hideNetCats = false, string label = "")
        {
            // accepts tbl as base class DimAggregatedTable
            // reads properties from tbl for use in applying labels
            _SetProperties(tbl);
            return _TopBoxNetExp(allCats, topCount, hideNetCats, label);
        }
        static private string _TopBoxNetExp(IEnumerable<Category> allCats, Int32 topCount, bool hideNetCats = false, string label = "")
        {
            // string netExp = "'Net: Top " + topCount.ToString() + " Box' net({";
            if (label.Trim() == "")
            {
                label = "Net: Top {0} Box";
                label = String.Format(label, topCount);
            }
            IEnumerable<Category> otherCats = allCats.Skip(topCount);
            IEnumerable<Category> topBoxCats = allCats.Take(topCount);
            string netExp = _NetExpression(label, topBoxCats, hideNetCats);
            return netExp += "}) ";
        }

        static public string OtherNetExp(IEnumerable<Category> allCats, IEnumerable<Category>topCats, bool hideNetCats = false)
        {
            // returns string representation of 'net' expression for categories IN allCats but NOT IN excludeCats
            string otherExp = "'Other' net({";
            foreach (Category cat in allCats)
            {
                if (!topCats.Contains(cat))
                {
                    otherExp += cat.Name + " [IsHidden=" + hideNetCats.ToString() + "], ";
                }
            }
            otherExp = otherExp.Substring(0, otherExp.Count() - 2);
            return otherExp += "}) "; 
        }

        static public List<DimAxisElement> CategoryExpressions(DimAggregatedTable tbl, IEnumerable<Category>cats, bool hideCats)
        {

            DimAxisElements elements = new DimAxisElements();
            foreach(Category c in cats)
            {
                DimAxisElement ele = new DimAxisElement(hide:hideCats);
                ele.ID = c.Name;
                ele.Label = c.Label(tbl.Context, tbl.Language, c.Document, true);
                elements.Add(ele);
            }
            return elements.Items;

        }
        static public string ScriptCategories(IEnumerable<Category> cats, Int32 topCount, bool hideCats, FactorType ft = FactorType.ftNone)
        {
            // builds formula string assigning a "1" factor value for counting occurrence
            // or increments factor by 1
            string s = "";
            Int32 i = 0;
            foreach (Category cat in cats)
            {
                s += ", " + cat.Name + " [IsHidden = " + hideCats.ToString();
                if (ft == FactorType.ftIncrement)
                {
                    i++;
                }
                s += SetFactorProperty(ft, i);
                s += "]";
            }
            return s;
        }
        static public string SetFactorProperty(FactorType ft, Int32 i)
        {
            string s = "";

            switch (ft)
            {
                case FactorType.ftFrequency:
                    s += ", Factor = " + (i).ToString();
                    break;
                case FactorType.ftIncrement:
                    i++;
                    s += ", Factor = " + (i).ToString();
                    break;
                case FactorType.ftNone:
                default:
                    break;
            }
            return "";
        }
        static public string TopAxis(Field f)
        {
            string topAxis = "{0}{{'Total respondents' base(), {1}}}";

            //IEnumerable<string> catNames = (from GfK.Automata.Model.Field c in (MetadataObject)f.Items[0] select c.Name).ToList();

            //string cats = String.Join(", ", catNames);

            return topAxis;
        }

        static public string CleanText(string s, string replacement= " ")
        {
            // this pattern may also work:
            // @"\<[^\>]*\>"
            s = Regex.Replace(s, @"<[^>]*(>|$)", " ");
            s = s.Replace("  ", " ").Trim();
            return s;
        }


        static private void _SetProperties(DimAggregatedTable tbl)
        {
            Context = tbl.Context;
            Language = tbl.Language;
            Document = tbl.Container.Document;
        }

    }
}
