﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    
    public class DimFamSummaryTable : DimSummaryTable
    {
        private Int32 _topCount = 2;
        private bool _hideCats = true;
        private bool _sortDesc = true;
        
        public DimFamSummaryTable(string language, string context, string desc, Field f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {

            Side = SummarySideAxis(c, _topCount, _hideCats, _sortDesc);
            AppendToFunnel(FunnelRows, "Familiarity");
        }

    }
}
