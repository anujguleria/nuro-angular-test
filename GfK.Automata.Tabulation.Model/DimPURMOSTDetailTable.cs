﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimPURMOSTDetailTable: DimDetailTable
    {
        public DimPURMOSTDetailTable(string language, string context, string description, BoundField field, Container container, DimFilter filter): base(language, context, description, field, container, filter)
        {
            // Side Axis is set in the base class
            DimFunnelTable.GetFunnelRows(this, field);
        }

    }
}
