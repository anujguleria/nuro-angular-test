﻿using System;
using System.Collections.Generic;
using System.Linq;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Service
{
    public class DimTOT_CONSummaryTable : DimSummaryTable
    {
        public DimTOT_CONSummaryTable(string language, string context, string desc, Field f, Container c, Int32 topCats) : base(language, context, desc, f, c, null)
        {
            FunnelRows = new Dictionary<Category, DimAxisElement>();
            Base = "OCC1 & OCC2 is asked - All respondents";
            Side = TOT_CONSummarySideAxis(topCats);
            
        }
        public string TOT_CONSummarySideAxis(Int32 topCats)
        {
            string label = "";
            if (topCats ==3)
            {
                label = "Net: Consider(1, 2, 3)";
            }
            else if(topCats ==2)
            {
                label = "Net: First / Second Choice)";
            }
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(Field.Items, false);
            string netExp = String.Join(",", (from c in allCats.Take(topCats) select c.Name).ToList());

            //List<string> elements = new List<string>();
            DimAxisElements elements = new DimAxisElements();
            foreach(BoundField f in Container.Items)
            {
                DimAxisElement e = new DimAxisElement();
                string brandName = Container.CategoryMap[Container.Items.IndexOf(f)].Name;
                if (InvalidBrandNames.Contains(brandName)) { continue; }
                string brandLabel = Container.CategoryMap[Container.Items.IndexOf(f)].Label(Context, Language, Container.Document);
                e.ID = brandName;
                e.Label = brandLabel;
                e.Expression = f.Name + ".ContainsAny({" + netExp + "})";
                elements.Add(e);
                // to be used later
                FunnelRows.Add(Container.CategoryMap[Container.Items.IndexOf(f)], e);

            }
            elements.Add(new DimAxisElement("badj", "Total Respondents", "comp = {comp}", true));

            elements.AddBaseElement(Base);
            Elements = elements;
            string sideAxis = "axis({";
            sideAxis += elements.ToScript();
            sideAxis += "}) " + "\"\"" + label + "\"\"";
            return sideAxis;
        }
    }
}