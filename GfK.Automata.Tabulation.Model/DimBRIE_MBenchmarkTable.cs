﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimBRIE_MBenchmarkTable: DimBenchmarkTable
    {
        Category Attribute { get; set; }
        Container Parent { get; set; }
        public DimBRIE_MBenchmarkTable (string description, Category cat, Container parent, Container c, DimFilter filter): base(description, c, filter)
        {
            Attribute = cat;
            Parent = parent;
            Field = (Field)Container.Items.First();
            SetElements();
            Side = SideAxis(AxisType.atAxis);
            Description += ": " + DimAxisHelper.CleanText(Attribute.Label(ProcessorConfiguration.Context, ProcessorConfiguration.Language, Container.Document,true));
        }
        public void SetElements()
        {
            // use DimAxisHelper.GetStripVarParts(field) to produce the 3 level items without brackets/curly.
            // parent.CategoryMap -- 12 attributes
            // ((Container)parent.Items[0]).CategoryMap  -- 20 brands
            string[] parts = DimAxisHelper.GetStripVarParts(Field);
            string itemExp = "{0}[{{{1}}}].{2}[{{{3}}}].{4}";
            foreach(Category brand in Container.CategoryMap)
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                string varExp = String.Format(itemExp, parts[0], Attribute.Name, parts[1], brand.Name, parts[2]);
                string brandAttExp = DimAxisHelper.ContainsAnyExpression(varExp,((BoundField)Field).Categories().Skip(2));

                // brand base element:
                DimAxisElement brandBaseElement = new DimAxisElement(hide: true, countsOnly:true)
                {
                    ID = "base_" + brand.Name,
                    Label = "base: " + brand.Name,
                    Expression = varExp + " is not null"

                };
                // frequency for this brand, this attribute:
                DimAxisElement brandFreqElement =new DimAxisElement(hide: true, countsOnly: true)
                {
                    ID = brand.Name,
                    Label = brand.Name + "top 2 frequency",
                    Expression = brandAttExp
                };
                // derived % for THIS brand:
                DimAxisElement brandDerivedElement = new DimAxisElement(hide: true, countsOnly: true)
                {
                    ID = "d_"+ brand.Name,
                    Label = brand.Name + "top 2 %",
                    Type = ExpressionType.etDerived,
                    Expression = "100 * (" + brandFreqElement.ID + "/" + brandBaseElement.ID + ")"
                };
                Elements.Add(brandBaseElement);
                Elements.Add(brandFreqElement);
                Elements.Add(brandDerivedElement);

            }
            List<string> brandNames = DimAxisHelper.GetCategoryNames(Container);
            string brandCount = Elements.Items.Where(e => e.Type == ExpressionType.etDerived).Count().ToString();
            string benchmarkExp = "(" + String.Join("+", brandNames) + ") / " + brandCount;
            Elements.Add(new DimAxisElement(countsOnly:true)
            {
                ID = "benchmark",
                Label = "Benchmark average % top 2 box",
                Type = ExpressionType.etDerived,
                Expression = benchmarkExp
            });
            Elements.AddBaseElement(Base);

        }
    }
}
