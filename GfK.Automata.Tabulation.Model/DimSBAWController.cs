﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimSBAWController
    {
        static List<IScriptableItem> _Tables { get; set; } = new List<IScriptableItem>();
        static string _Lang { get; set; } = ProcessorConfiguration.Language;
        static string _Context { get; set; } = ProcessorConfiguration.Context;
        static MetadataDocument _Doc { get; set; } = ProcessorConfiguration.Document;
        static UnboundField SBAW1 { get; set; }
        static Container SBAW2 { get; set; }
        public static BoundField SBAW1codes { get; set; } // coded data
        static BoundField SBAW2codes { get; set; } // coded data
        static Container SBAW1_Order { get; set; }
        static bool CreatedSBAWTables { get; set; } = false;
        public static Dictionary<string, Container> FunnelContainers { get; set; }
        public static Dictionary<string, Field> RequiredFields { get; set; }

        static DimSBAWController()
        {

        }
        public static void CreateSBAW_Tables(Field f, Dictionary<string, DimFilter> filters)
        {
            AddField(f);
        }

        public static List<IScriptableItem> SBAW1DetailTables()
        {
            DimSBAW1DetailTable table = null;
            if (!(_ReadyToProcess())) { return null; };
            List<IScriptableItem> tables = new List<IScriptableItem>();
            //List<string> order = _orderBrands();

            if (!(SBAW1codes == null))
            {
                table = new DimSBAW1DetailTable(_Lang, _Context, "Spontaneous Brand Awareness – Top of Mind", null, SBAW1codes, null, null);
                tables.Add(table);
            }

            // Core 4: the middle box...
            if (!(SBAW2codes == null))
            {
                table = new DimSBAW1DetailTable(_Lang, _Context, "Spontaneous Brand Awareness – Other Mentions", null, SBAW2codes, null, null);
                tables.Add(table);
            }
            _Tables.AddRange(tables);
            return tables;
            
        }
        public static List<IScriptableItem> SBAWTOTTable()
        {
            DimSBAWTOTTable table = null;
            if (!(_ReadyToProcess())) { return null; };
            List<IScriptableItem> tables = new List<IScriptableItem>();
            //List<string> order = _orderBrands();
            if (!(SBAW2codes is null) && (!(SBAW1codes is null)))
            {
                List<BoundField> fields = new List<BoundField>() { SBAW1codes, SBAW2codes };
                table = new DimSBAWTOTTable(_Lang, _Context, "Spontaneous Brand Awareness – Total(Top of Mind and Other)", fields, null, null);
                table.AppendToFunnel((table).FunnelRows, "Unaided Awareness");
                tables.Add(table);
            }
            _Tables.AddRange(tables);
            return tables;
        }
        public static List<IScriptableItem> SBAWOrderTables()
        {
            IScriptableItem table = null;
            if (!(_ReadyToProcess())) { return null; };
            table = (DimSBAWOrderDetailTable)table;
            List<IScriptableItem> tables = new List<IScriptableItem>();
            if (!(SBAW1codes is null) && (!(SBAW1_Order is null)))
            {

                // for each brand, create detail table from SBAW1_OrderOfMention
                //foreach (string brand in _orderBrands())
                foreach (UnboundField brandField in SBAW1_Order.Items)
                {
                    table = new DimSBAWOrderDetailTable(_Lang, _Context, "Order of mention in Unaided Brand Awareness - [BRAND]", brandField, SBAW1_Order, null);
                    if (_Tables.Any(o => o.GetType() == table.GetType()))
                    { break; }
                    tables.Add(table);
                    table = null;
                }

                table = new DimSBAWSummaryTable(_Lang, _Context, "Mean Order of mention in Unaided Brand Awareness", SBAW1_Order, null);
                if (!(_Tables.Any(o => o.GetType() == table.GetType())))
                { tables.Add(table); }

                table = new DimSBAWBenchmarkTable(_Lang, _Context, "Unaided Awareness -- Benchmark (Priority Brands)", SBAW1_Order, null);
                if (!(_Tables.Any(o => o.GetType() == table.GetType())))
                { tables.Add(table); }

            }
            _Tables.AddRange(tables);
            return tables;
        }
        
        private static void _SetRequiredFields()
        {
            SBAW1 = (UnboundField)_Doc.Find("SBAW1");
            SBAW1codes = (BoundField)_Doc.Find("SBAW1_FirstMention");
            SBAW2codes = (BoundField)_Doc.Find("SBAW1_AllOtherMentions");
            RequiredFields = new Dictionary<string, Field>()
            {
                { "SBAW1", SBAW1 },
                { "SBAW1_FirstMention", SBAW1codes },
                { "SBAW1_AllOtherMentions", SBAW2codes }
            };
            // not required per se, but may be needed to derive funnel levels.
            SBAW2 = (Container)_Doc.Find("SBAW2_GRID");
            SBAW1_Order = (Container)_Doc.Find("SBAW1_OrderOfMention");
            FunnelContainers = new Dictionary<string, Container>()
            {
                {"SBAW2_GRID",SBAW2 },
                {"SBAW1_OrderOfMention",SBAW1_Order }
            };
        }
        private static bool _ReadyToProcess()
        {

            if (RequiredFields.Any(i => (i.Value is null)))
            { return false; }
            return true;
            

        }

        public static void AddField(Field f)
        {
            if (_Doc is null) { _Doc = f.Document; };
            _SetRequiredFields();
            switch (f.DataType)
            {
                case DataType.dtCategorical:
                    AddField((BoundField)f);
                    break;
                case DataType.dtText:
                case DataType.dtLong:
                    AddField((UnboundField)f);
                    break;
            }
        }
        public static void AddField(BoundField f)
        {
            RequiredFields[f.Name] = f;
        }
        public static void AddField(UnboundField f)
        {
            RequiredFields[f.Name] = f;
        }
        public static string FunnelLevel()
        {
            Int32 i;
            List<Int32> levels = new List<Int32>();

            List<Field> fields = new List<Field>();
            fields.AddRange(RequiredFields.Values);
            fields.Add((Field)SBAW2.Items[0]);

            foreach (Field f in fields)
            {
                try
                {
                    string s = f.Properties[GfKProperties.GfKFunnelLevel];
                    if (s != String.Empty)
                    { levels.Add(Convert.ToInt32(s)); }
                }
                catch (KeyNotFoundException e)
                {
                    // property doesn't exist, so ignore
                }

            }
            if ( levels.Count == 0) { throw new KeyNotFoundException(); }
            string val = levels.Min().ToString();
            return val;
        }
    }
}
