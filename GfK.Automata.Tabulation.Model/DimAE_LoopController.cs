﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimAE_LoopController
    {

        private static Container _Container { get; set; }
        private static Container _Parent { get; set; }
        private static List<IScriptableItem> _Tables { get; set; }
        private static string _lang { get; set; }
        private static string _context { get; set; }
        private static List<string>_ControlledItems {get; set;}
        public static bool CreatedAELoopTables { get; set; }
        public static BoundField Iterator { get; set; }
        
        static DimAE_LoopController()
        {
            _lang = ProcessorConfiguration.Language;
            _context = ProcessorConfiguration.Context;

        }
        public static List<IScriptableItem> CreateAE_LoopTables(Container c)
        {

            _Parent = c;
            _Tables = new List<IScriptableItem>();
            _ControlledItems = new List<string>();
            _ProcessItem((MetadataObject)_Parent);

            if(_Tables.Count > 0)
            { CreatedAELoopTables = true; }
            return _Tables;
        }

        #region ProcessItems

        private static void _ProcessItem(Container itm)
        {
            _Container = (Container)itm;
            foreach (MetadataObject obj in itm.Items)
            {
                //Console.WriteLine(obj.Name);
                _ProcessItem(obj);
            }
        }
        private static void _ProcessItem(MetadataObject obj)
        {
            //Console.WriteLine(obj.Name);
            switch (obj.ObjectType)
            {
                case ObjectType.otCollection:
                    _Container = (Container)obj;
                    _ProcessItem((Container)obj);
                    break;
                case ObjectType.otField:
                    _ProcessItem((Field)obj);
                    break;
            }
        }

        private static void _ProcessItem(Field field)
        {
            {
                if (field.IsSystem == false)
                {
                    switch (field.DataType)
                    {

                        case DataType.dtBoolean:
                        case DataType.dtCategorical:
                            _ProcessItem((BoundField)field);
                            break;
                        case DataType.dtDate:
                        case DataType.dtDouble:
                        case DataType.dtLong:
                            _ProcessItem((UnboundField)field);
                            break;
                        case DataType.dtNone:
                        case DataType.dtObject:
                        case DataType.dtText:
                            // Do nothing to tabulate these (may change text later)
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }
            }
        }
        private static void _ProcessItem(UnboundField field)
        {
            IScriptableItem table = null;
            string propVal = DimTabPlan.GetQType(field);

            switch (propVal.ToUpper())
            {
                default:
                    Console.WriteLine("Not implemented for UnboundField at: " + field.Name);
                    break;
            }

        }

        private static void _ProcessItem(BoundField field)
        {
            IScriptableItem table = null;
            string propVal = DimTabPlan.GetQType(field);
            
            switch (propVal.ToUpper())
            {
                case "AE1":

                    Int32 ae1DetailCount = _Tables.Where(t => (t is DimAE1DetailTable)).Count();
                    if (ae1DetailCount <= _Parent.CategoryMap.Count)
                    {
                        //table = new DimDetailTable(_lang, _context, "Ad Recall - [AD]", field, _Container, null);
                        //foreach(Category ad in _Parent.CategoryMap)
                        //{
                            //BoundField adField = (BoundField)_Parent.Items[_Parent.CategoryMap.IndexOf(ad)];
                            
                            table = new DimAE1DetailTable("Ad Recall - [AD]", ae1DetailCount, field, _Container, null);
                            _Tables.Add(table);
                            table = null;
                        //}
                    }

                    BoundField ae2 = DimAE1AE2DetailTable.HelperVariables(_Container, field)["AE2"];
                    BoundField ae2Brand = DimAE1AE2DetailTable.HelperVariables(_Container, field)["AE2B_BrandName"];
                    if (ae2 is null || ae2Brand is null) { break; }

                    if (_Tables.Where(t => (t is DimAE1AE2DetailTable)).Count() <= _Parent.CategoryMap.Count)
                    {

                        
                        //BoundField itm = (BoundField)_Container.Items[i];
                        table = new DimAE1AE2DetailTable(_lang, _context, "Ad Recall and Branding - [AD]", field, ae2, ae2Brand, _Container, null);
                        _Tables.Add(table);
                        table = null;
                    }
                    if(_Tables.Where(t => (t is DimAE1AE2DetailTable)).Count() == _Parent.CategoryMap.Count &&  _Tables.Where(t => (t is DimAE1BenchmarkTable)).Count() == 0)
                    {
                        // Benchmark table should only be produced once, AFTER the combined AE1/AE2 tables are produced
                        table = new DimAE1BenchmarkTable(_lang, _context, "Total Recognition", field, _Container, null);
                        _Tables.Add(table);
                        table = null;
                    }

                    break;
                case "AE2":
                    if (_Tables.Where(t => (t is DimAE2BenchmarkTable)).Count() == 0)
                    {
                        table = new DimAE2BenchmarkTable(_lang, _context, "Correct Brand Recall", field, _Container, null);
                        _Tables.Add(table);
                        table = null;
                    }
                    break;

                case "AE5_M":
                    // Detail Table per attribute with full scale, net top 2, net bottom 2
                    // similar to DimCL_BE1DetailTable
                    table = new DimAE5_MDetailTable(_lang, _context, "Ad Diagnostics - [AD] - [ATTRIBUTE]", field, _Container);
                    _Tables.Add(table);

                    for (Int32 i = 0; i < _Parent.Items.Count; i++)
                    {
                        if (_Parent.Items[i].Items.Last().Equals(field))
                        {

                            // Summary for ALL attribute items only after we've processed EACH attribute item
                            // two summary tables, 
                            //      first contains _Container.Items[21].Items[0 through 5]
                            //      second contains _Container.Items[21].Items[6 through 11]
                            //
                            field = (BoundField)_Parent.Items[i].Items.First();
                            table = new DimAE5_MSummaryTable(_lang, _context, "Ad Diagnostics = [AD] - Agree", field, (Container)_Parent.Items[i], _Container);
                            _Tables.Add(table);

                            //Benchmark
                            table = new DimBenchmarkTable(_lang, _context, "Ad Diagnostics - Top 2 box Benchmarks", field, (Container)_Parent.Items[i], _Parent, "Ad Diagnostics - Top 2 box Benchmarks", null);
                            _Tables.Add(table);
                        }
                    }

                    break;
                case "AE6_S_M":
                    // similar to AE5_M, possibly reuse
                    table = new DimAE6_S_MDetailTable(_lang, _context, "Social Media Action (After Ad Exposure) - [AD] - [ATTRIBUTE]", field, _Container);
                    _Tables.Add(table);

                    //for (Int32 i = 0; i < _Container.Items.Count; i++)
                    //{
                    if (_Container.Items.Last().Equals(field))
                    {
                        // create summary table for each ad, Learn/Talk/Do 
                        foreach (Category ad in _Parent.CategoryMap)
                        {
                            Console.WriteLine("create summary table for each ad, Learn/Talk/Do " + field.Name);
                            table = new DimAE6_S_MSummaryTable(_lang, _context, "Learn, Talk & Do - [AD]", field, _Container, _Parent, ad);
                            _Tables.Add(table);
                        }

                        // L/T/D Benchmark
                        table = new DimBenchmarkTable(_lang, _context, "Learn, Talk & Do", _Container, _Parent, null);
                        _Tables.Add(table);
                        
                    }
                    //}
                    table = null;
                    break;
                case "AE7_M":
                    // similar structure to AE5_M; should possibly be able to reuse that code.
                    table = new DimAE5_MDetailTable(_lang, _context, "Product Image (After Ad Exposure) - [AD] - [ATTRIBUTE]", field, _Container);
                    _Tables.Add(table);

                    for (Int32 i = 0; i < _Parent.Items.Count; i++)
                    {
                        if (_Parent.Items[i].Items.Last().Equals(field))
                        {
                            field = (BoundField)_Parent.Items[i].Items.First();
                            table = new DimAE5_MSummaryTable(_lang, _context, "Product Image (After Ad Exposure) - [AD] - Agree", field, (Container)_Parent.Items[i], _Container);
                            _Tables.Add(table);

                            //Benchmark
                            table = new DimBenchmarkTable(_lang, _context, "Product Image (After Ad Exposure) - Top 2 box benchmarks", field, (Container)_Parent.Items[i], _Parent, "Product Image (After Ad Exposure) - Top 2 box Benchmarks", null);
                            _Tables.Add(table);
                        }
                    }

                    break;
                default:
                    break;
            }

        }


        #endregion ProcessItems

    }
}
