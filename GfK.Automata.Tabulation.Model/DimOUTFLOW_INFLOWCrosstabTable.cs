﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimOUTFLOW_INFLOWCrosstabTable: DimCrosstabTable
    {
        public DimOUTFLOW_INFLOWCrosstabTable(string description, Category brand, BoundField Field, BoundField sideField, BoundField topField, DimFilter filter) : base(description, brand, Field, sideField, topField, filter)
        {
            Base = "Total Respondents";
            Side = SideAxis();
            Top = TopAxis();
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, brand.Document, true));
        }
        public DimOUTFLOW_INFLOWCrosstabTable(string description, Category brand, BoundField Field, DimAxisElements sideElements, DimAxisElements topElements, DimFilter filter) : base(description, brand, Field, null, null, filter)
        {
            Base = "Total Respondents";
            Elements = sideElements;
            TopElements = topElements;
            Side = "axis({" + sideElements.ToScript() + "})";
            Top = "axis({" + TopElements.ToScript() + "})";
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, brand.Document, true));
        }
    }
}
