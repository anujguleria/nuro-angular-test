﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSummaryTable : DimAggregatedTable , ISummaryTable
    {
        public DimSummaryTable(string language, string context, string desc, Field f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            this.Base = "Base: All respondents";
            this.Description = " Summary Table: " + Description; //+ "</br>" + this.Base;
            
        }
        public string SummarySideAxis(Container container, Int32 topCount = 0, bool hideCats = false, bool sortDescending = false)
        {
            return SummarySideAxis(container, topCount, hideCats, sortDescending, "");
        }
        public DimSummaryTable(string desc, Container c, Container top, DimFilter filter): base(desc, c, top, filter)
        {

        }


        public string SummarySideAxis(Container container, Int32 topCount = 0, bool hideCats = false, bool sortDescending = false, string label="")
        {
            // For Grid Summary Tables...
            // this seems like a hack but without a good way to identify 
            //      the "FAM_GRID[{..}].FAM" (specifically the ".FAM")
            if (Base == "") { Base = "Total respondents"; };
            if (label == "") { label = String.Format("Top {0} box", topCount); };

            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(container.Items[0].Items, sortDescending);
            if (topCount != 0)
            {
                foreach (BoundField f in Container.Items)
                {
                    Category brandCat = Container.CategoryMap[Container.IndexOf(f)];
                    string brandLabel = brandCat.Label(Context, Language, Container.Document);
                    brandLabel = DimAxisHelper.CleanText(brandLabel);
                    DimAxisElement e = new DimAxisElement()
                    {
                        ID = "e" + Elements.Items.Count().ToString(),
                        Label = brandLabel,
                        Expression = DimAxisHelper.ContainsAnyExpression(f.Name, allCats.Take(topCount))
                    };
                    if (!(InvalidBrandNames.Contains(brandCat.Name)))
                    {
                        Elements.Add(e);
                        FunnelRows.Add(brandCat, e);
                    }
                }
            }
            else
            {
                foreach (Category cat in allCats)
                {
                    string lab = cat.Label(Context, Language, Container.Document, true);
                    DimAxisElement e =  new DimAxisElement()
                    {
                        ID = cat.Name,
                        Label = DimAxisHelper.CleanText(lab),
                        Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, cat)
                    };
                    Elements.Add(e);
                    FunnelRows.Add(cat, e);
                }
            }
            Elements.AddBaseElement(Base);
            string sideAxis = "axis({";
            sideAxis += Elements.ToScript();
            sideAxis += "})";
            return sideAxis;
        }
        public string SummaryNumericSideAxis(Container container)
        {
            return "";
        }

        private string CustomSummaryAxisExpression(Container container, Int32 topCount = 0, bool hideCats = false, bool sortDescending = false, string label = "")
        {
            // possibly need revision to:
            /*
                axis({
                    'Base' base(), 
                    b1 'Brand 1 Top 2 Box' expression('FAM_GRID[{Brand1}].FAM.ContainsAny({_4,_3})'),
                    b2 'Brand 2 Top 2 Box' expression('FAM_GRID[{Brand2}].FAM.ContainsAny({_4,_3})'),
                    b3 'Brand 3 Top 2 Box' expression('FAM_GRID[{Brand3}].FAM.ContainsAny({_4,_3})'),
                    b4 'Brand 4 Top 2 Box' expression('FAM_GRID[{Brand4}].FAM.ContainsAny({_4,_3})'),
                    b5 'Brand 5 Top 2 Box' expression('FAM_GRID[{Brand5}].FAM.ContainsAny({_4,_3})'),
                    b6 'Brand 6 Top 2 Box' expression('FAM_GRID[{Brand6}].FAM.ContainsAny({_4,_3})'),
                    b7 'Brand 7 Top 2 Box' expression('FAM_GRID[{Brand7}].FAM.ContainsAny({_4,_3})'),
                    b8 'Brand 8 Top 2 Box' expression('FAM_GRID[{Brand8}].FAM.ContainsAny({_4,_3})'),
                    b9 'Brand 9 Top 2 Box' expression('FAM_GRID[{Brand9}].FAM.ContainsAny({_4,_3})'),
                    b10 'Brand 10 Top 2 Box' expression('FAM_GRID[{Brand10}].FAM.ContainsAny({_4,_3})')
                    })
            */

            string[] varParts = Field.Name.Split('.');
            string exp = "axis({ '" + this.Base + "' base() ";

            Int32 i = -1;
            List<Category> allCats = ((BoundField)Field).Categories().ToList();
            List<Category> topCats = allCats.Skip(allCats.Count - topCount).ToList();
            List<string> catNames = topCats.Select(itm => itm.Name).ToList();
            foreach (Category cat in container.CategoryMap)
            {
                i += 1;
                /*cat.Label(Context, Language, Field.Document) -- needs to be cleaned of HTML for this */
                string cExp = "b" + i.ToString() + " '" + cat.Name + "' expression(";
                cExp += "'" + container.Name + "[{" + cat.Name + "}]." + varParts[1] + ".ContainsAny({";
                cExp += String.Join(", ", catNames) + "})')";

                exp += ", " + cExp;

            }
            exp += "})";
            // add title for the table
            if (label.Trim() != "")
            {
                exp += "'" + label + "'";
            }

            return "";
        }

    }
}
