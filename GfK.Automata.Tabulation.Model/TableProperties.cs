﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public class TableProperties: Dictionary<string,string>
    {
        public const string
            AdjustRounding = "\"AdjustRounding\"";

        public TableProperties()
        {

        }

        public void SetProperty(string k, string v)
        {
            if (ContainsKey(k))
            {
                this[k] = v;
            }
            else { this.Add(k, v);  }
        }
    }
}
