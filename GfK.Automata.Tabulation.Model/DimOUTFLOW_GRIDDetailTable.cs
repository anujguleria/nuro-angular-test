﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimOUTFLOW_GRIDDetailTable: DimSW4_PDetailTable
    {
        
        public DimOUTFLOW_GRIDDetailTable(string language, string context, string desc, Category brand, BoundField field, DimFilter filter): base(language, context, desc, brand, field, filter)
        {
            QTypeModifier = "OUTFLOW_GRID";
            // create an SW4 table , and then just add the appropriate Top Axis
            Top = Field.Name;    // DURSEG'

        }

    }
}
