﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    class DimAE6_S_MDetailTable: DimAggregatedTable
    {
        private bool _hideCats = true;

        public DimAE6_S_MDetailTable(string language, string context, string description, BoundField f, Container c) : base(language, context, description, f, c, null)
        {
            Base = "Exposed to ad and recall ad";
            Side = SideAxis();

            FormatDescription("Attribute", c);
            FormatDescription("Ad", (Container) c.Document.Find("AE_Loop"));
            
        }

        public string SideAxis()
        {
            string sideAxis = "";

            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(Field.Items, true);
            sideAxis = "{0}{{'Total respondents' base() ";
            sideAxis = String.Format(sideAxis, Field.Name);
            sideAxis += DimAxisHelper.ScriptCategories(allCats, 0, false) + ", ";
            sideAxis += "N1 " + DimAxisHelper.NetExpression(this, " Net Somewhat/Strongly Agree (5,4)", allCats.Take(2), _hideCats);
            sideAxis += "})";
            sideAxis += ", ";
            sideAxis += "N2 " + DimAxisHelper.NetExpression(this, " Net Somewhat/Strongly Disagree (1,2)", allCats.Skip(3), _hideCats);
            sideAxis += "})";
            sideAxis += "}";
            return sideAxis;
        }

    }
}
