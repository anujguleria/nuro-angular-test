﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Service
{
    public class DimPIDetailTable : DimDetailTable
    {
        private Int32 _topCount = 2;
        private bool _sortDesc = true;
        private bool _hideCats = false;
        private List<string> _CatList { get; set; }
        public DimPIDetailTable(string language, string context, string desc, BoundField f, Container c = null) : base(language, context, desc, f, c, null)
        {
            _setCatLst();
            Base = "Aware of brand";
            CreateElementsTop2SideAxis(_CatList, _topCount);
            Side = SideAxis(AxisType.atAxis);
            // custom Variable description appends to the side axis expression
            string s = Container.CategoryMap[Container.IndexOf(Field)].Label(Context, Language, Field.Document);
            Side += " '" + DimAxisHelper.CleanText(s) + "'";
            FormatDescription("Brand", c);

        }
        private void _setCatLst()
        {
            _CatList = new List<string>();
            _CatList.Add("Very likely to purchase");        //_4
            _CatList.Add("Somewhat likely to purchase");
            _CatList.Add("Somewhat unlikely to purchase");
            _CatList.Add("Very unlikely to purchase");          //_1
        }

    }
}