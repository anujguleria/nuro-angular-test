﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    class DimSW2_TSummaryTable : DimSummaryTable
    {
        // All of the tables below need to be computed for each Priority Brand that is mentioned in PURLAST_T.  
        public DimSW2_TSummaryTable(string language, string context, string desc, BoundField f, Container c) : base(language, context, desc, f, c, null)
        {
            //  Standard table. 
            //  Net: "Switched from another[CATEGORY]"[All numbered brands in list.Include "other"(97) but exclude "I did not switch"(99)]
            //  desc = [CATEGORY] Switched from Before a Customer of[BRAND FROM PURLAST_T]

        }
    }
}
