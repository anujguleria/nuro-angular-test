﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class GlobalFilter: IScriptableItem
    {
        // GlobalFilter class which is an object appending to MRS output metadata
        // not to be used on Table as table filter; use DimFilter for that.
        public string Name { get; set; }
        public string Description { get; set; }
        public string Expression { get; set; }
        public string ProjectType { get; set; }
        public GlobalFilter(string name, string projectType )
        {
            ProjectType = projectType;
            Name = name;
        }
        
        public string GetScript(ScriptType scriptType)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("    ' Define the filter for " + Description);
            sb.AppendLine("    .Global.Filters.AddNew(\"" + Name + "\", \"" + Expression + "\", \"" + Description + "\" , \"HData\" )");
            return sb.ToString();
        }

        public string CreateFilterInMDD(ScriptType scriptType)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("    ' Define the filter for " + Description);
            sb.AppendLine("    With .DataSet");
            sb.AppendLine("        .MdmDocument.Fields.AddScript(\"!");
            sb.AppendLine("            " + Name);
            sb.AppendLine("            \"" + Description + "\"");
            sb.AppendLine("            [ IsUserDefined = true ]");
            sb.AppendLine("            boolean");
            sb.AppendLine("            expression(\"" + Expression + "\") ");
            sb.AppendLine("            axis(\"{eExpression Expression('" + Expression + "')) } \");");
            sb.AppendLine("            !\")");
            sb.AppendLine("        Set globalFilter = .MdmDocument.Fields[\"" + Name + "\"]");
            sb.AppendLine("        .Variables.Add(globalFilter)");
            sb.AppendLine("    End With");
            sb.AppendLine();

            return sb.ToString();
        }
    }

    
}
