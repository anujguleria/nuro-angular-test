﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimCON_1SummaryTable: DimSummaryTable
    {
        public DimCON_1SummaryTable(string language, string context, string desc, Field f, Container c, Int32 topCats) : base(language, context, desc, f, c, null)
        {
            FunnelRows = new Dictionary<Category, DimAxisElement>();
            Base = "All respondents";
            Side = CON_1SummarySideAxis();

        }

        public string CON_1SummarySideAxis()
        {
            /*
             * axis({
                b 'All respondents' base(),
                b1 'Brand 1' expression('CON_1_GRID[_1].CON = {Brand1} OR CON_1_GRID[_2].CON = {Brand1}'),
                ...
                })  
            */
            string[] varParts = Field.Name.Split('.');
            List<string> fieldNames = (from f in Container.Items select f.Name).ToList();
            DimAxisElements elements = new DimAxisElements();
            foreach (Category c in ((BoundField)Field).Categories())
            {
                string exp = String.Join(" OR ", (from f in fieldNames select f + "={" + c.Name + "}"));
                DimAxisElement e = new DimAxisElement()
                {
                    ID = c.Name,
                    Label = DimAxisHelper.CleanText(c.Label(Context, Language, Container.Document, true)),
                    Expression = exp
                };
                if (!(InvalidBrandNames.Contains(c.Name)))
                {
                    elements.Add(e);
                    FunnelRows.Add(c, e);
                }
            }
            elements.AddBaseElement(Base);
            string side = "axis({";
            side += elements.ToScript();
            side += "})";
                
            return side;
        }
    }
}
