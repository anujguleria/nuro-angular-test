﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimPI_OCCController
    {
        private static List<IScriptableItem> _Tables { get; set; }
        private static string 
            PI_OCC1_Description = "Purchase Intention for last occasion of [INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC2]",
            _IterName = "OCC2";
        static DimPI_OCCController()
        {

        }
        public static List<IScriptableItem> CreatePI_OCCTables(string language, string context, Container container, DimFilter filter, string endDate)
        {
            _Tables = new List<IScriptableItem>();
            
            BoundField filterVar = (BoundField)container.Document.Find(_IterName);
            IScriptableItem table = null;
            Category brand = null;
            string where = DimQueryData.MostRecentMonth(endDate);
            
            // One detail table per Occasion (20) per brand (10)
            foreach (BoundField itm in container.Items)
            {
                brand = container.CategoryMap[container.Items.IndexOf(itm)];
                if (InvalidBrandNames.Contains(brand.Name)) { continue; };
                if (filterVar == null)
                {
                    table = new DimPI_OCCDetailTable(language, context, PI_OCC1_Description, itm, container, filter);
                    _Tables.Add(table);
                    table = null;
                }
                else
                {
                    foreach (Category occ in filterVar.Categories())
                    {
                        // Detail each brand per occasion
                        table = new DimPI_OCCDetailTable(language, context, PI_OCC1_Description, itm, container, filterVar, occ, filter);
                        _Tables.Add(table);
                        table = null;
                    }
                }
            }
            // one summary table per occasion
            if (filterVar != null)
            {
                foreach (Category itm in filterVar.Categories())
                {
                    
                    table = new DimPI_OCCSummaryTable(language, context, PI_OCC1_Description, (BoundField)container.Items.First(), container, filterVar, itm);
                    _Tables.Add(table);
                }
            }
            return _Tables;
        }
    }
}
