﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSW4_PDetailTable: DimDetailTable
    {
        public DimSW4_PDetailTable(string language, string context, string description, Category brand, BoundField field, DimFilter filter): base(language, context, description, field, filter)
        {
            // this creates the default table 
            string throwaway = CategorySideAxis(0, false, false);
            // now we need to add the "not asked" category
            AddNotAsked();
            Side = SideAxis();
            Description += " " + DimAxisHelper.CleanText(brand.Label("Analysis", "ENU", brand.Document, true));


        }
        public void AddNotAsked()
        {
            Elements.Add( new DimAxisElement()
                {
                    ID = "notAsked",
                    Label = "Not asked " + Field.Name,
                    Expression = Field.Name + " Is Null"
                });

        }
        public string SideAxis()
        { return "axis({" + Elements.ToScript() + "})"; }
    }
}
