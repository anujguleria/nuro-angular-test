﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimCL_BE1DetailTable : DimDetailTable
    {
        private bool _hideCats = false;
        private string _topParent = "CL_BE1_Loop";

        public DimCL_BE1DetailTable(string language, string context, string desc, BoundField f, Container c) : base(language, context, desc, f, c, null)
        {
            AddQTypeMod("_1");
            Side = SideAxis(f, _hideCats);
            FormatDescription("Brand", c);
            Container top = null;
            try
            {
                top = (Container)c.Document.Find(_topParent);
                FormatDescription("Item", top);

            }
            catch (Exception)
            {
                Console.WriteLine("Unable to find top-level " + _topParent + ", ITEM name not replaced in Description");
            }
           
        }
        public string SideAxis(BoundField f, bool hideCats = false)
        {
            string sideAxis = "";

            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(f.Items, true);
            sideAxis = "{0}{{'Total respondents' base(), ";
            sideAxis = String.Format(sideAxis, f.Name);
            sideAxis += "N1 " + DimAxisHelper.NetExpression(this, " Net Somewhat/Strongly Agree (4,5)", allCats.Take(2), hideCats);
            sideAxis += "})";
            sideAxis += ", ";
            sideAxis += "N2 " + DimAxisHelper.NetExpression(this, " Net Somewhat/Strongly Disagree (1,2)", allCats.Skip(3), hideCats);
            sideAxis += "})";
            sideAxis += "," + DimAxisHelper.AddTotal() + "}";
            return sideAxis;
        }

    }
}
