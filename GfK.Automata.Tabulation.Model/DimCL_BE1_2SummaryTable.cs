﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimCL_BE1_2SummaryTable: DimSummaryTable
    {
        private Char _delimiter = '.';
        private Int32 _topCount = 2;
        private bool _sortDesc = true;
        private bool _hideNetCats = true;
        private string _topParent = "CL_BE1_Loop";
        private Container _topContainer = null;
        public DimCL_BE1_2SummaryTable(string language, string context, string desc, BoundField f, Container parent) : base(language, context, desc, f, parent, null)
        {
            AddQTypeMod("_2");
            _topContainer = (Container)Field.Document.Find(_topParent);
            FormatDescription("Brand", parent);
            Side = CL_BE1_2SummaryBrandSideAxis();
            
        }

        public string CL_BE1_2SummaryBrandSideAxis()
        {

            /*  Same As CL_BE1_3SummaryTable but WITHOUT the derived category and not hidden rows.
                axis({
                b 'base' base(), 
                e1 'Attribute1 ' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5}) '), 
                e2 'Attribute2 ' expression(' CL_BE1_Loop[{_2}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5}) '), 
                e3 'Attribute3 ' expression(' CL_BE1_Loop[{_3}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5}) '), 
                e4 'Attribute4 ' expression(' CL_BE1_Loop[{_4}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5}) '), 
                }) *banMonthly
                */
            List<string> expressions = new List<string>();
            string[] cParts = DimAxisHelper.GetVarParts(Container);
            string[] varParts = DimAxisHelper.GetVarParts(Field);
            string brand = Container.CategoryMap[Container.IndexOf(Field)].Name;
            string brandLabel = Container.CategoryMap[Container.IndexOf(Field)].Label(Context, Language, Field.Document);

            List<string> catLst = (from c in DimAxisHelper.GetCategories(Field.Items, _sortDesc).Take(_topCount).ToList() select c.Name).ToList();

            foreach (Category cat in _topContainer.CategoryMap)
            {
                string attribute = cat.Label(Context, Language, Field.Document);
                string exp = "e" + expressions.Count.ToString();
                exp += " '" + attribute + "' expression('" + _topContainer.Name + "[{" + cat.Name + "}].";
                
                exp += cParts[1] + "[{" + brand + "}]." + varParts[2] + ".ContainsAny({" + String.Join(",", catLst) + "}) ') ";
                expressions.Add(exp);
            }

            //string derived = "";
            //derived = "d 'Benchmark Brand Equity: " + brandLabel + "' derived('";
            //derived += "(" + String.Join("+", (from v in expressions select v.Substring(0, v.IndexOf(" ")))) + ")";
            //derived += " / " + expressions.Count.ToString();
            //derived += "')";
            //expressions.Add(derived);
            expressions.Insert(0, " b 'Base' base() ");
            string side = "axis({" + String.Join(", ", expressions) + "}) ";
            side += "\"\"" + Description + "\"\"";
            return side;

        }

    }
}
