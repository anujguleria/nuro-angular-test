﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSW3_MSummaryTable: DimSummaryTable
    {
        public DimSW3_MSummaryTable(string language, string context, string description, Int32 top, Category brand, Container container, DimFilter filter): base(language, context, description, null,container, filter)
        {
            //SummarySideAxis(Container container, Int32 topCount = 0, bool hideCats = false, bool sortDescending = false, string label="")
            Side = SideAxis(top);
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, Container.Document, true));
        }

        public string SideAxis(Int32 top)
        {

            string[] varParts = Container.Items[0].Name.Split('.');
            string var = "[" + varParts.Last() + "]";
            IEnumerable<Category> topCats = Container.CategoryMap.Take(top);
            string lvlExp = DimAxisHelper.ContainsAnyExpression("LevelID", topCats);
            Elements.AddBaseElement(Base, lvlExp, false);
            foreach(Category cat in Container.Items[0].Items)
            {
                string catLab = cat.Label(Context, Language, Container.Document, true);
                catLab = DimAxisHelper.CleanText(catLab);
                Elements.Add(new DimAxisElement()
                {
                    ID = cat.Name,
                    Label = catLab,
                    Expression = DimAxisHelper.ContainsAnyExpression(var, cat)
                });
            }
            string sideAxis = Container.Name + "." + varParts.Last();
            sideAxis += "{" + Elements.ToScript() + "}";
            return sideAxis;
        }
    }
}
