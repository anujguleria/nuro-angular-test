﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{

    public static class DimSOPTables
    {
        private static List<IScriptableItem> _Tables { get; set; }
        private static string _lang { get; set; }
        private static string _context { get; set; }
        private static UnboundField _Field { get; set; }
        private static List<string> _bandLabels { get; set; }
        private static List<Int32> _bands { get; set; }
        private static Container _Container { get; set; }
        private static Int32 _min { get; set; }
        private static Int32 _max { get; set; }
        public static GlobalFilters GlobalFilters { get; set; } = new GlobalFilters();
        static DimSOPTables()
        {
            
            _Tables = new List<IScriptableItem>();
            _bandLabels = new List<string>(new string[] { "Non-buyers", "Light buyers", "Medium buyers", "Heavy buyers" });
            _bands = new List<Int32>();

        }
        private static void _getValuesFromDB(string ddfPath)
        {
            // open connection, get range of values
            // brute force modular division of the range, based on number of categories

            DimOLEDbConnection.SetConnectionString(_lang,_context, _Field.Document.Path, ddfPath, DimOLEDbConnection.dbCategoryReturnType.dbValues, DimOLEDbConnection.dbTableName.dbVDATA);
            List<Int32> vals = DimQueryData.GetDistinctValues(_Container, "");
            //GetDistinctValues
            _mapBandsLabels(vals);
        }

        public static List<IScriptableItem> CreateSOPTables(Container container, Field field, string language, string context, DimFilter filter = null)
        {
            _lang = language;
            _context = context;
            _Field = (UnboundField)field;
            _Container = container;
            IScriptableItem table = null;

            foreach (UnboundField itm in _Container.Items)
            {
                //((DimDetailTable)table).Apply(field, pattern, language, context);

                // Share of purchase [Brand]
                // SOP 
                table = new DimSOPDetailTable(language, context, "Share of Purchases - [BRAND]", itm, _Container, filter);
                _Tables.Add(table);
                table = null;

                // SOP Percent
                //table = new DimSOPDetailTable(language, context, "Share of Purchases - Percentages - [BRAND]", "percent", itm, _Container, filter);
                //_Tables.Add(table);
                //table = null;

                // Buyer segment
                // SOP.SEG table
                // only needed for Client's brand
                List<Int32> _bands = null;
                if (_Container.Items.First() == itm)
                {
                    table = new DimSOPDetailTable(language, context, "Buyer Segment - [BRAND]", itm, _Container, _bandLabels, _bands, filter);
                    _Tables.Add(table);
                    
                    table = null;
                }
            }

            // SOP Percentage summary table 
            // SOP.SUM
            table = new DimSOPSummaryTable(language, context, "Share of Purchases - Percentages", field, _Container, null);
            _Tables.Add(table);
            table = null;

            return _Tables;
        }
        private static void _mapBandsLabels(List<Int32> values)
        {
            Int32 minVal = _Field.MinValue;
            Int32 maxVal = values.Max();
            _bands.Add(_Field.MinValue);

            for (Int32 i = 1; i <= _bandLabels.Count; i++)
            {
                Int32 indx = (i / values.Count);
                if (_bands.Contains(values[indx]))
                {
                    Console.WriteLine("!");
                }
                else
                {
                    _bands.Add(values[indx]);
                }

            }

        }
        private static void _getMinAndMax(Container c)
        {
            foreach (UnboundField f in c.Items)
            {
                Int32 min = DimQueryData.GetMin((UnboundField)f);
                if (min < _min)
                { _min = min; }

                Int32 max = DimQueryData.GetMax((UnboundField)f);
                if (max > _max)
                { _max = max; }
            }

        }
    }
}
