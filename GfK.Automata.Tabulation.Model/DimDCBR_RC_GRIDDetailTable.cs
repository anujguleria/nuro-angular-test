﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimDCBR_RC_GRIDDetailTable: DimDetailTable
    {
        public DimDCBR_RC_GRIDDetailTable(string description, BoundField field, Container container, DimFilter filter) : base(description, field, container, filter)
        {
            Base = "Respondents who were asked the brand";
            Brand = Container.CategoryMap[Container.IndexOf(Field)];
            CreateElements();
            Side = SideAxis();
            FormatDescription("BRAND", DimAxisHelper.CleanText(Brand.Label(Context, Language, Container.Document, true)));
        }
        public string SideAxis()
        {
            // this variable has been created in DMS script and could be used simply as Field.Name, e.g.
            //      DCBR_RC_GRID[{Brand1}].RelationshipClass
            // return Field.Name;
            return Field.Name + "{" + Elements.ToScript() + "}";
            
        }
        public void CreateElements()
        {
            Elements = new DimAxisElements();
            foreach(Category cat in ((BoundField)Field).Categories())
            {
                Elements.Add(new DimAxisElement()
                {
                    ID = cat.Name,
                    Label = cat.Label(Context, Language, Container.Document, true)
                });
            }
            Elements.AddBaseElement(Base,null,hidden:false);
        }
    }
}
