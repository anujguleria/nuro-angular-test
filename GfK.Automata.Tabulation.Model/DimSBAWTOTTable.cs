﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSBAWTOTTable: DimDetailTable
    {
        public BoundField SBAW1 { get; set; }
        public BoundField SBAW2 { get; set; }
        public DimSBAWTOTTable(string language, string context, string desc, List<BoundField> fields, List<string>orderedBrands, DimFilter filter): base(language, context, desc, fields[0], null, filter)
        {
            
            SBAW1 = fields[0];
            SBAW2 = fields[1];
            SBAW1.Properties[GfKProperties.GfKQType] = "SBAW1_SBAW2";
            QTypeModifier = "UNAIDEDTOT";
            Base = "All respondents";
            Side = SBAWTOTSideAxis();
            // from testing only
            // Field = (Field)SBAW1.Document.Find("SBAW1");
            // need this to avoid error in DimTabPlan as the PRIORITY_BRAND_LIST 
            // doesn't have a QType or a Container parent
            
        }

        public string SBAWTOTSideAxis()
        {
            string side = "axis({";
            Elements = new DimAxisElements();

            // Assumes the _orderedDict method contains all brands needed and that they exist in the Field variable.

            foreach (Category cat in ((BoundField)Field).Categories())
            {
                //Category cat = SBAW1.Category(itm, true);
                Category cat2 = SBAW2.Category(cat.Name, true);
                if (!(cat2 == null))
                {
                    string catLabel = cat.Label(Context, Language, Field.Document, true);
                    DimAxisElement e = new DimAxisElement()
                    {
                        ID = cat.Name,
                        Label = catLabel,
                        Expression = DimAxisHelper.ContainsAnyExpression(SBAW1.Name, cat) + " OR " + DimAxisHelper.ContainsAnyExpression(SBAW2.Name, cat2)
                    };
                    
                    Elements.Add(e);
                    FunnelRows.Add(cat, e);
                }
                
            }

            Elements.AddBaseElement(Base);
            side += Elements.ToScript();
            side += "})";

            return side;
        }

    }
}
