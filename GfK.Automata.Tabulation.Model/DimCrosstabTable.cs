﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimCrosstabTable: DimAggregatedTable
    {
        public BoundField SideField { get; set; }
        public BoundField TopField { get; set; }
        public DimAxisElements TopElements { get; set; } = new DimAxisElements();
        public DimCrosstabTable(string description, Category brand, BoundField field, BoundField sideField, BoundField topField, DimFilter filter): base(description, brand, field, filter)
        {
            Language = ProcessorConfiguration.Language;
            Context = ProcessorConfiguration.Context;
            SideField = sideField;
            TopField = topField;

        }
        public string SideAxis()
        {
            Elements = CreateElements(SideField);
            // returns all category elements from SideField
            string sideAxis = SideField.Name + "{" + Elements.ToScript() + "}";
            return sideAxis;
        }
        public string TopAxis()
        {
            TopElements = CreateElements(TopField);
            // returns all category elements from TopField
            string topAxis = TopField.Name + "{" + TopElements.ToScript() + "}";
            return topAxis;
        }
    }
}
