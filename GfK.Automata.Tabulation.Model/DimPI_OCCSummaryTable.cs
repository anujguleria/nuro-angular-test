﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimPI_OCCSummaryTable : DimSummaryTable
    {
        private Int32 _topCount = 2;
        private bool _hideCats = true;
        private bool _sortDesc = true;
        public DimPI_OCCSummaryTable(string language, string context, string desc, Field f, Container c, BoundField filterVar, Category itm) : base(language, context, desc, f, c, null)
        {
            Base = "OCC1 & OCC2 asked (Aware of brand)";
            Side = SummarySideAxis(c, _topCount, _hideCats, _sortDesc);
            string lab = filterVar.Name + "_" + itm.Name;
            AddFilter(lab, lab, DimFilterExpressionBuilder.VariableContains(filterVar, itm));
            FormatDescription("[INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC2]", itm);

        }
    }
}