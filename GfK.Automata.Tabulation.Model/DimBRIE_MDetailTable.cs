﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimBRIE_MDetailTable: DimDetailTable
    {
        private List<string> _CatList { get; set; }
        public DimBRIE_MDetailTable(string language, string context, string desc, string attributeName, BoundField f, Container c = null) : base(language, context, desc, f, c, null)
        {
            AddQTypeMod(attributeName);
            _SetCatList();
            SetElements();
            Side = SideAxis(AxisType.atField);
            FormatDescription("Brand", c);
            string[] parts = DimAxisHelper.GetVarParts(Container,false);
            string parent = parts.First();
            FormatDescription("Attribute", ((Container)c.Document.Find(parent)));

            //((Container)c.Document.Find("BRIE_M_Loop")).CategoryMap.ToList()
        }
        public void SetElements()
        {
            DimAxisElements top2 = new DimAxisElements();
            top2.Add(((BoundField)Field).Categories().Skip(2), true);
            DimAxisElement net1 = new DimAxisElement()
            {
                ID = "N1",
                Label = "Net top 2",
                Type = ExpressionType.etNet,
                Expression = top2.ToScript()
            };
            DimAxisElements bottom2 = new DimAxisElements();
            bottom2.Add(((BoundField)Field).Categories().Take(2),true);
            DimAxisElement net2 = new DimAxisElement()
            {
                ID = "N2",
                Label = "Net bottom 2",
                Type = ExpressionType.etNet,
                Expression = bottom2.ToScript()
            };
            DimAxisElement total = new DimAxisElement()
            {
                Type = ExpressionType.etDerived,
                Label = "Total",
                ID = "d",
                Expression = String.Join("+", DimAxisHelper.GetCategoryNames((BoundField)Field, false))
            };
            Elements.Add(net1);
            Elements.Add(net2);
            
            foreach (Category c in ((BoundField)Field).Categories())
            {
                Int32 i = ((BoundField)Field).Categories().IndexOf(c);
                Elements.Add(new DimAxisElement()
                {
                    ID = c.Name,
                    Label = _CatList[i]
                });
            }
            Elements.Add(total);
            Elements.AddBaseElement(Base);

        }
        private void _SetCatList()
        {
            _CatList = new List<string>();
            _CatList.Add("Strongly disagree");        //_1
            _CatList.Add("Somewhat disagree");
            _CatList.Add("Somewhat agree");
            _CatList.Add("Strongly agree");          //_4
        }

    }
}
