﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSTPUL_T_MDetailTable: DimDetailTable
    {
        public DimSTPUL_T_MDetailTable(string language, string context, string description, Category brand, BoundField field, Container container, DimFilter filter): base(language, context, description, field, filter)
        {
            Base = "Total Respondents";
            Side = SideAxis((BoundField)Field);
            Description += " - " + container.CategoryMap[container.IndexOf(Field)].Label(context, language, Field.Document, true);
            FormatDescription("{#INS_PURLAST_T}", DimAxisHelper.CleanText(brand.Label("Analysis", "ENU", brand.Document, true)));
            
        }

        public string SideAxis(BoundField itm)
        {

            IEnumerable<Category> cats = itm.Categories();
            string top2 = DimAxisHelper.ContainsAnyExpression(itm, cats.Skip(2));
            string bottom2 = DimAxisHelper.ContainsAnyExpression(itm, cats.Take(2));
            foreach (Category cat in cats)
            {
                Elements.Add(new DimAxisElement()
                {
                    ID = cat.Name,
                    Label = cat.Label(Context, Language, cat.Document, true),
                    Expression = DimAxisHelper.ContainsAnyExpression(DimCustomerHarmonicsController.Iterator, cat)
                });
            }
            // Net top 2
            Elements.Add(new DimAxisElement()
            {
                ID = "top2",
                Label = "Net top 2",
                Expression = top2
            });
            // Net bottom 2
            Elements.Add(new DimAxisElement()
            {
                ID= "bottom2",
                Label = "Net bottom 2",
                Expression = bottom2
            });
            Elements.AddBaseElement(Base);
            string sideAxis = "axis({"+ Elements.ToScript() + "})";
            return sideAxis;
        }
    }
}
