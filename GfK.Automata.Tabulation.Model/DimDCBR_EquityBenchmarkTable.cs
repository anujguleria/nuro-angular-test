﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimDCBR_EquityBenchmarkTable: DimBenchmarkTable
    {

        private List<Category> _Brands { get; set; }
        private string[] _parts { get; set; }

        public DimDCBR_EquityBenchmarkTable(string description, BoundField field, Container container, DimFilter filter) : base(description, field, container, filter)
        {
            Base = "Respondents who were asked the brand";
            _Brands = Container.CategoryMap;
            _parts = Field.Name.Split('.');
            CreateElements();
            Level = "HDATA";
            Side = SideAxis();
        }

        public string SideAxis()
        {
            return Container.Name +"." + _parts.Last() +  "{" + Elements.ToScript() + "}";
        }
        public void CreateElements()
        {
            /* each brand has a base count, CountsOnly=True
            DCBR_RC_GRID.RelationshipClass{
                base_OldSpice'Base: Old Spice' expression('LevelID={OldSpice}') [IsHidden=True,CountsOnly=True], 
	            active_OldSpice'active: Old Spice' expression('LevelID={OldSpice} AND RelationshipClass.ContainsAny({_1})') [IsHidden=True,CountsOnly=True]
	            OldSpice 'Old Spice' derived('(active_OldSpice / base_OldSpice) * 100')[CountsOnly = True]}
                ...
                benchmark 'Benchmark' derived('(OldSpice+Degree+...) / {brands.Count}') [CountsOnly=true]
                )
            */
            Elements = new DimAxisElements();
            foreach(Category brand in _Brands)
            {
                _createBrandElements(brand);
            }
            string benchExp = "({0}) / {1}";
            benchExp = String.Format(benchExp, String.Join("+", (from b in _Brands select b.Name)), _Brands.Count());
            Elements.Add(new DimAxisElement(countsOnly: true)
            {
                ID="benchmark",
                Label= "Benchmark Active Brand Equity",
                Type=ExpressionType.etDerived,
                Expression = benchExp
            });
            Elements.AddBaseElement(Base);
    
        }
        private void _createBrandElements(Category brand)
        {
            IEnumerable<Category> cats = ((BoundField)Container.Items.First()).Categories();
            string var = "[" + _parts.Last() + "]";
            string brandExp = DimAxisHelper.ContainsAnyExpression("LevelID", brand);
            BoundField field = (BoundField)Container.Items[Container.CategoryMap.IndexOf(brand)];
            DimAxisElement e = null;
            // Base element for this brand
            string brandLabel = _brandLabel(brand);
            e = new DimAxisElement(countsOnly: true, hide: true)
            {
                ID = "base_" + brand.Name,
                Label = "Base: " + brandLabel,
                Expression = brandExp
            };
            Elements.Add(e);

            e = new DimAxisElement(countsOnly: true, hide: true)
            {
                ID = "active_" + brand.Name,
                Label = "active: " + brandLabel,
                Expression = brandExp + " AND " + DimAxisHelper.ContainsAnyExpression(var, field.Category("_1",true))
            };
            Elements.Add(e);

            e = new DimAxisElement(countsOnly: true)
            {
                ID = brand.Name,
                Label = brandLabel,
                Type = ExpressionType.etDerived,
                Expression = String.Format("(active_{0} / base_{0}) * 100", brand.Name)
            };
            Elements.Add(e);
        }
        private string _brandLabel(Category brand)
        {
            return DimAxisHelper.CleanText(brand.Label(Context, Language, Container.Document, true));
        }
    }
}
