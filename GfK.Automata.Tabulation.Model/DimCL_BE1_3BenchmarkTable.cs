﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimCL_BE1_3BenchmarkTable: DimBenchmarkTable
    {

        private Int32 _topCount = 2;
        //private bool _hideCats = true;
        private bool _sortDesc = true;
        //private string _benchmarkLabel = "";
        private string _topParent = "CL_BE1_Loop";
        private Container _topContainer = null;

        public DimCL_BE1_3BenchmarkTable(string language, string context, string desc, Container c, DimFilter filter) : base(language,context,desc,c,filter)
        {
            AddQTypeMod("_3");
            Field = (BoundField)Container.Items.First();
            _topContainer = (Container)Container.Document.Find(_topParent);
            SetElements();
            Side = SideAxis(AxisType.atAxis);
            Side += "\"\"" + Description + "\"\"";
        }
        
        public void SetElements()
        {
            /*
             axis({
                b 'base' base(), 
                e1 'Attribute1 Brand1' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e2 'Attribute1 Brand2' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand2}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e3 'Attribute1 Brand3' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand3}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e4 'Attribute1 Brand4' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand4}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e5 'Attribute1 Brand5' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand5}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e6 'Attribute1 Brand6' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand6}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e7 'Attribute1 Brand7' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand7}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e8 'Attribute1 Brand8' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand8}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e9 'Attribute1 Brand9' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand9}].CL_BE1.ContainsAny({_4,_5}) ') [IsHidden=True], 
                e10 'Attribute1 Brand10' expression(' CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand10}].CL_BE1.ContainsAny({_4,_5}) '), 
                d1 'Benchmark ' derived('(e1+e2+e3+e4+e5+e6+e7+e8+e9+e10) / 10 '),
                ..
                ..
                ..

                }) "Description of table" *banMonthly
             */
            List<string> expressions = new List<string>();
            
            string[] cParts = DimAxisHelper.GetVarParts(Container);
            string[] varParts = DimAxisHelper.GetVarParts(Field);
            
            
            List<string> catLst = (from c in DimAxisHelper.GetCategories(Field.Items, _sortDesc).Take(_topCount).ToList() select c.Name).ToList();
            foreach (Category cat in _topContainer.CategoryMap)
            {
                string attribute = cat.Label(Context, Language, Field.Document);
                List<string> derived = new List<string>();
                foreach (Category brand in Container.CategoryMap)
                {
                    if (InvalidBrandNames.Contains(brand)) { continue; }
                    string brandLabel = DimAxisHelper.GetLabel(brand);
                    string exp = _topContainer.Name + "[{" + cat.Name + "}].";
                    exp += cParts[1] + "[{" + brand.Name + "}]." + varParts[2] + ".ContainsAny({" + String.Join(",", catLst) + "})";
                    DimAxisElement e = new DimAxisElement(hide: true)
                    {
                        ID = "e" + Elements.Items.Count.ToString(),
                        Label = "",
                        Expression = exp
                    };
                    Elements.Add(e);
                    derived.Add(e.ID);
                }
                DimAxisElement benchmark = new DimAxisElement()
                {
                    Type = ExpressionType.etDerived,
                    ID = "bm" + cat.Name,
                    Label = "Benchmark: " + attribute,
                };
                string dExp = "(" + String.Join("+", derived) + ")";
                dExp += " / " + derived.Count.ToString();
                
                benchmark.Expression = dExp;
                Elements.Add(benchmark);
            }
            Elements.AddBaseElement(Base);

        }
    }
}
