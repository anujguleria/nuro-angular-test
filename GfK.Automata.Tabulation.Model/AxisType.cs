﻿namespace GfK.Automata.Tabulation.Model
{
    public enum AxisType
    {
        atAxis,
        atField,
        atContainer
    }

}
