﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimBRIA_MDetailTable: DimDetailTable
    {
        private Int32 _topCount = 0;
        private bool _sortDesc = false;
        private bool _hideCats = false;
        public DimBRIA_MDetailTable(string language, string context, string desc, BoundField f, Container c = null) : base (language, context, desc, f, c, null)
        {
            Side = CategorySideAxis(this, _hideCats, _sortDesc);
            FormatDescription("Attribute", c);
        }
    }
}
