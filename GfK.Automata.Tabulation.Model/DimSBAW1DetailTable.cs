﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSBAW1DetailTable: DimDetailTable
    {
        
        public DimSBAW1DetailTable(string language, string context, string desc, List<string> orderedBrands, BoundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            Base = "All respondents";
            Side = SBAW1SideAxis();

        }
        public DimSBAW1DetailTable(string language, string context, string desc, List<string> orderedBrands, BoundField f, Container c, DimFilter filter, string overrideName) : base(language, context, desc, f, c, filter)
        {
            // This override was used for testing with a different variable "overrideName"
            Base = "All respondents";
            Side = SBAW1SideAxis();
            // need this to avoid error in DimTabPlan as the PRIORITY_BRAND_LIST 
            // doesn't have a QType or a Container parent
            Field = (Field)f.Document.Find(overrideName);
            
        }
        public string SBAW1SideAxis()
        {
            string side = "axis(";
            return side += _SideAxis() + ")";
        }
        public string SBAW1SideAxis(List<string> orderedBrands, string overrideName)
        {
            // This overload was used during testing and probably not needed for production
            string side = overrideName;
            return side += _SideAxis();
        }
        private string _SideAxis()
        {
            string side = "{";
            
            Elements = new DimAxisElements();
            // Assumes the _orderedDict method contains all brands needed and that they exist in the Field variable.
            
            foreach (Category cat in ((BoundField)Field).Categories())
            {
                //Category cat = ((BoundField)Field).Category(itm, true);
                string catLabel = cat.Label(Context, Language, Field.Document, true);
                DimAxisElement e = new DimAxisElement()
                {
                    ID = cat.Name,
                    Label = catLabel,
                    Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, cat)
                };
                Elements.Add(e);
            }

            Elements.AddBaseElement(Base);
            side += Elements.ToScript();
            side += "}";
            return side;
        }
    }
}
