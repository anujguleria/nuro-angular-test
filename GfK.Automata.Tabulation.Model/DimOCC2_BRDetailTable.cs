﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimOCC2_BRDetailTable : DimDetailTable
    {
        public DimOCC2_BRDetailTable(string language, string context, string desc, BoundField f, Category cat, BoundField iterator, DimFilter filter) : base(language, context, desc, f, null, filter)
        {
            Side = CategorySideAxis(0, false, false);
            FormatDescription("[INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC2]", cat);
            //    FormatDescription("[INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC2]", itm, c);
            // AddFilter("f2","f2", DimFilterExpressionBuilder...)
            AddFilter("", "f", DimFilterExpressionBuilder.VariableContains(iterator, cat));
            //FilterDescription += " Aware of brand";

        }

    }
}
