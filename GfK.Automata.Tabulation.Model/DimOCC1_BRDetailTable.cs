﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimOCC1_BRDetailTable: DimDetailTable
    {
        public DimOCC1_BRDetailTable(string language, string context, string desc, BoundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            Side = CategorySideAxis(this, false, false);
            FormatDescription("INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC1", c);

        }

    }
}
