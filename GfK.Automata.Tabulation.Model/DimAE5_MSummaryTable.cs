﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAE5_MSummaryTable : DimSummaryTable
    {
        private Container _parent = null;

        public DimAE5_MSummaryTable(string language, string context, string desc, Field f, Container c, Container parent) : base(language, context, desc, f, c, null)
        {
            _parent = parent;
            Base = "Exposed to ad and recall ad";
            Side = SummaryAttributeSideAxis((BoundField)f, c, "Net Top 2 (Somewhat/Strongly Agree)");
            FormatDescription("AD", c);
        }

        public string SummaryAttributeSideAxis(BoundField f, Container c, string label)
        {
            // c.Items iteration of all
            // f.Items iteration of rating scale  
            // c.CategoryMap = attributes

            // string[] varParts = f.Name.Split(_delimiter);

            string exp = "axis({ b '" + Base + "' base() ";
            string condExp = ".ContainsAny({" + f.ItemAt(f.Items.Count - 2).Name + "," + f.Items.Last().Name + "})'";
            Int32 attributeCount = c.Items.Count / c.CategoryMap.Count;
            //for (int = 0; )
            for (Int32 n = 0; n <= c.CategoryMap.Count-1; n++)
            {
                exp += ", " + "" + " ex" + n.ToString() + String.Format(" '{0}' ", c.CategoryMap[n].Label(Context, Language, c.Document)) + " expression('";
                exp += c.Items[n].Name.ToString() + condExp + ") [IsHidden = False]";
            }
            
            exp += "})" + String.Format(" '{0}'", label);
            return exp;
        }

    }

}
