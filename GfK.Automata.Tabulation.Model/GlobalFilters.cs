﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public class GlobalFilters : IScriptableItem
    {
        public Dictionary<string, GlobalFilter> Items { get; set; } = new Dictionary<string, GlobalFilter>();
        

        public void Add(string name, GlobalFilter f)
        {
            if (Items.ContainsKey(name))
            {
                Items[name] = f;
                return;
            }
            Items.Add(name, f);
            
        }
        public void Add(GlobalFilter f)
        {
            Add(f.Name, f);
        }
        public string GetScript(ScriptType scriptType)
        {
            StringBuilder sb = new StringBuilder();
            foreach (GlobalFilter f in Items.Values)
            {
                sb.AppendLine();
                sb.Append(f.GetScript(ScriptType.stDimensions));
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
    
}
