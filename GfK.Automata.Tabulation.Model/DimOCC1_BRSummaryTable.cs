﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimOCC1_BRSummaryTable: DimSummaryTable
    {
        private char _delimiter = '.';
        

        public DimOCC1_BRSummaryTable(string language, string context, string desc, BoundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            AddQTypeMod("AVG");  // Core: 68, 69-70, 89
            SetElements();
            Side = SideAxis(AxisType.atAxis);

        }
        public void SetElements()
        {
            Elements = new DimAxisElements();
            Dictionary<string, DimAxisElements> brandDict = new Dictionary<string, DimAxisElements>();
            Dictionary<string, string> brandLabels = new Dictionary<string, string>();
            string brandLabel = null;
            foreach (BoundField itm in Container.Items) // Occasions
            {
                string occName = Container.CategoryMap[Container.IndexOf(itm)].Name;
                string occLabel = Container.CategoryMap[Container.IndexOf(itm)].Label(Context, Language, Container.Document);
                brandLabel = null;
                foreach (Category cat in itm.Categories()) // Brands
                {
                    if (InvalidBrandNames.Contains(cat.Name)) { continue; }
                    if (cat.IsOtherCategory()) { continue; }
                    if ((brandDict.ContainsKey(cat.Name) == false))
                    {
                        brandLabel = cat.Label(Context, Language, Container.Document, true);
                        brandDict.Add(cat.Name, new DimAxisElements());
                        brandLabels.Add(cat.Name, brandLabel);
                    }
                    string catLabel = cat.Label(Context, Language, Container.Document, true);
                    string id = cat.Name + occName;
                    DimAxisElement e = new DimAxisElement(hide: true)
                    {
                        ID = "e" + cat.Name + occName,
                        Label = catLabel + "-" + occLabel,
                        Expression = itm.Name + " = {" + cat.Name + "}"
                    };
                    Elements.Add(e);
                    brandDict[cat.Name].Add(e);
                }
            }
            // Brand Benchmark
            foreach (string brand in brandDict.Keys)
            {
                brandLabel = brandLabels[brand];
                Elements.Add(new DimAxisElement(countsOnly: true, decimals: 2)
                {
                    ID = "bm" + brand,
                    Label = brandLabel,
                    Type = ExpressionType.etDerived,
                    Expression = "(" + String.Join("+", (from ele in brandDict[brand] select ele.ID)) + ") / b"
                });
            }

            Elements.AddBaseElement(Base);
        }
        

    }
}
