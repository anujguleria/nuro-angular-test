﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimCL_BE1_4SummaryTable: DimSummaryTable
    {
        private Int32 _topCount = 2;
        private bool _sortDesc = true;
        private Char _delimiter = '.';
        private string _topParent = "CL_BE1_Loop";
        private Container _top = null;
        public DimCL_BE1_4SummaryTable(string language, string context, string desc, Container c, DimFilter filter) : base(language, context, desc, null, c, filter)
        {
            Base = "Base";
            AddQTypeMod("_4");
            Field = (BoundField)Container.Items.First();
            _top = (Container)Field.Document.Find(_topParent);
            Side = CL_BE1_4SummaryAttributeSideAxis();

        }

        public string CL_BE1_4SummaryAttributeSideAxis()
        {
            //  axis({
            //    b base(), 
            //    ex1 'Attribute1 * Brand1' expression('CL_BE1_Loop[{_1}].CL_BE1_GRID[{_1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex2 'Attribute2 * Brand1' expression('CL_BE1_Loop[{_2}].CL_BE1_GRID[{_1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex3 'Attribute3 * Brand1' expression('CL_BE1_Loop[{_3}].CL_BE1_GRID[{_1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex4 'Attribute4 * Brand1' expression('CL_BE1_Loop[{_4}].CL_BE1_GRID[{_1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    d1 derived('(@1+@2+@3+@4) / 4')
            //    ex5 'Attribute1 * Brand1' expression('CL_BE1_Loop[{_1}].CL_BE1_GRID[{_2}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex6 'Attribute2 * Brand1' expression('CL_BE1_Loop[{_2}].CL_BE1_GRID[{_2}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex7 'Attribute3 * Brand1' expression('CL_BE1_Loop[{_3}].CL_BE1_GRID[{_2}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex8 'Attribute4 * Brand1' expression('CL_BE1_Loop[{_4}].CL_BE1_GRID[{_2}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    d2 derived('(@1+@2+@3+@4) / 4')
            //    ...  
            //    ...
            //    })

            string label = "Overall Brand Equity";
            

            string exp = "axis({ b '" + Base + "' base() [IsHidden=False] ";
            List<string> catLst = (from c in DimAxisHelper.GetCategories(Field.Items, _sortDesc).Take(_topCount).ToList() select c.Name).ToList();
            string condExp = ".ContainsAny({" + String.Join(",", catLst) + "})'";
            Int32 attributeCount = Container.Items.Count / Container.CategoryMap.Count;
            Int32 eCount = 0;
            attributeCount = _top.CategoryMap.Count;
            //for (int = 0; )
            for(Int32 i=0; i < Container.CategoryMap.Count; i++)
            {
                if (InvalidBrandNames.Contains(Container.CategoryMap[i])) { continue; }
                string[] varParts = Container.Items[i].Name.Split(_delimiter);
                List<string> derived = new List<string>();
                string brandLabel = Container.CategoryMap[i].Label(Context, Language, Container.Document);
                for (Int32 n = 0; n < attributeCount; n++)
                {
                    eCount += 1;
                    exp += ", " + "" + " ex" + eCount + " expression('";
                    exp += _topParent + "[{" + _top.CategoryMap[n].Name + "}]." + varParts[1] + "." + varParts[2] + condExp + ") [IsHidden = True] ";
                    //exp += varParts[0].Replace("_1", "_" + n.ToString()) + "." + varParts[1] + "." + varParts[2] + condExp + ") [IsHidden = true]";
                    derived.Add("@" + (n+1).ToString());

                }
                exp += ", " + "d" + i.ToString() + " '" + brandLabel + " " + label + "' derived('(" + String.Join("+", derived) + ") / " + attributeCount.ToString() + "')";
                
            }
            exp += "}) " + "\"\"" + Description + "\"\"";
            return exp;
        }
    }
}
