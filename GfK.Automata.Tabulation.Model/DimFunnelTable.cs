﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimFunnelTable: DimDetailTable
    {
        private Dictionary<Int32, DimAxisElement> _Rows { get; set; } = new Dictionary<Int32,DimAxisElement>();
        private DimAxisElement _Current { get; set; }  // represents the "current level" 
        private Category _Category { get; set; }
        private Int32 _Level { get; set; } = 0;
        private string _prev { get { return "f" + (_Level - 1).ToString(); } }
        public DimFunnelTable(string lang, string context, Category cat): base(lang, context)
        {
            Elements = new DimAxisElements();
            _Category = cat;
            Description = "FUNNEL: " + DimAxisHelper.GetLabel(cat);
        }
        public void BuildAxis()
        {
            List < Int32 > keys = _Rows.Keys.ToList();
            keys.Sort();
            // without Level 1, we can't have a funnel
            if (!(_Rows.ContainsKey(1)))
            { return; }
            // if the funnel is discontiguous, we can't have a funnel
            if (keys.Max() > keys.Count())
            { return; }
            // if there aren't at least 3 levels
            if (_Level < 3) { return; }
            _Level = 0;
            for (Int32 i=1; i <= keys.Max(); i++)
            {
                _Level++;
                DimAxisElement e = _Rows[i];
                Elements.Add(e);
                _Current = e;
                if (i > 1)
                {
                    _addConversionRow(i);
                    _addConversionLossRow(i);
                }
            }

            Side = "axis({ " + Elements.ToScript() + " })";

        }
        public void AddRow(DimAxisElement e, Int32 level)
        {
            _Level = level;
            e.ID = "f" + _Level.ToString();
            _Rows.Add(level, e);

        }
        private void _addConversionRow(Int32 level)
        {
            // Stub label: Conversion
            // Formula: Numerator = % Current level / Denominator = % level above it
            string lev = (level).ToString();
            DimAxisElement e = new DimAxisElement(countsOnly:true)
            {
                ID = "conv" + lev,
                Label = "Conversion" + lev,
                Type = ExpressionType.etDerived,
                // Numerator = % Current level / Denominator = % level above it
                Expression = "100 * (" + _Current.ID + "/" + _prev + ")"
            };
            Elements.Add(e);
        }

        private void _addConversionLossRow(Int32 level)
        {
            // Stub label: Convestion(Loss model)
            // Formula: Numberator = (% level above current level - % current level) /
            //     Denominator = % level above current level
            string lev = (level).ToString();
            DimAxisElement e = new DimAxisElement(countsOnly:true)
            {
                ID = "conloss" + lev,
                Label = "Conversion (Loss model)" + lev,
                Type = ExpressionType.etDerived,
                Expression = "100 * (" + _prev + " - " + _Current.ID + ")/" + _prev
            };
            Elements.Add(e);
        }

        public static void GetFunnelRows(DimDetailTable tbl, BoundField f)
        {
            
            foreach (Category c in f.Categories())
            {
                DimAxisElement e = new DimAxisElement()
                {
                    ID = c.Name,
                    Label = c.Label(tbl.Context, tbl.Language, f.Document, true),
                    Expression = DimAxisHelper.ContainsAnyExpression(f.Name, c)
                };
                tbl.FunnelRows.Add(c, e);
            }
        }

    }
}
