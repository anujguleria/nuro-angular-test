﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimOutflowDetailTable: DimDefaultDetailTable
    {
        BoundField outflow { get; set; }
        BoundField DurSeg { get; set; }
        Category brand { get; set; }
        BoundField SW4 { get; set; }
        public DimOutflowDetailTable(string language, string context, string description, Category brand, BoundField outflow, BoundField DurSeg, DimFilter filter): base(language,context, description,outflow, filter)
        {
            Base = "Total Respondents";
            this.outflow = outflow;
            this.DurSeg = DurSeg;
            this.brand = brand;
            this.SW4 = DimCustomerHarmonicsController.SW4;
            Side = SideAxis();
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, DurSeg.Document, true));
        }
        public string SideAxis()
        {
            Elements = CreateElements(outflow); // overwrites anything produced by base class
            return "axis({" + Elements.ToScript() + "})";

        }
        public string SideAxis(Category brand)
        {
            // this was used to test only, before we had the variables from DMS.
            //
            //IF (DURSEG=1 AND (SW4=1 OR 2)) OR (DURSEG=2 AND (SW4=1 OR 2)) OR (DURSEG=3 AND SW4=1) OR (SW4 = 9), THEN OUTFLOW=2.
            //IF(DURSEG = 3 AND SW4 = 2) OR SW4 = 3, THE OUTFLOW = 1.


            throw (new NotImplementedException());

            //OutflowElements.Init(SW4, DurSeg);
            //Elements = OutflowElements.GetElements();

            //Elements.AddBaseElement(Base);

            //return "axis({" + Elements.ToScript() + "})";
        }


    }
}
