﻿using System;
using System.Collections.Generic;
using GfK.Automata.Model;
using System.Linq;
//using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimTOT_CONBenchmarkTable : DimBenchmarkTable, IScriptableItem
    {
        public DimTOT_CONBenchmarkTable(DimAxisElements summaryElements, string language, string context, string desc, Field f, Container c) : base(summaryElements, language, context, desc, f, c, false, null)
        {
            AdjustElements();
            Side = SideAxis(AxisType.atAxis);
           
        }
        public void AdjustElements()
        {
            // base class creates a "base adjustment" item that needs to be excluded from the benchmark calc.
            IEnumerable<DimAxisElement> elements = Elements.Items.Where(o => o.ID != "badj" && o.ID != "benchmark");
            DimAxisElement bmElement = Elements.Items.Where(o => o.ID == "benchmark").First();
            bmElement.Label = "1st or 2nd Choice Benchmark";
            bmElement.Expression = "(" + String.Join("+", (from e in elements select e.ID)) + ") / " + elements.Count().ToString();
        }

    }
}