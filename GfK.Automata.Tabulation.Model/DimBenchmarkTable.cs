﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;
using static GfK.Automata.Tabulation.Model.DimAxisElement;


namespace GfK.Automata.Tabulation.Model
{
    public class DimBenchmarkTable : DimAggregatedTable, IDetailTable
    {

        private Int32 _topCount = 2;
        private bool _hideCats = true;
        private bool _sortDesc = true;
        private string _benchmarkLabel = "";
        private Container _topContainer = null;
        public DimFilter Filter {get; set;}
        public DimAxisElements SummaryElements = null;
        public DimBenchmarkTable(string language, string context, string desc, Field f, Container c, bool brandsInColumns, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            
            this.Base = "Base: All respondents";
            this.Description = " Benchmark: " + Description;

            // Some tables have specific subclass 
            // TO DO: All Benchmark Tables should be classed.
            if (this is DimTOT_CONBenchmarkTable)
            {
                return;
            }
            if (this is DimOCC1_BRBenchmarkTable)
            {
                //Core: 71-72
                
                return;
            }


            if (c == null)
            {
                Side = CategorySideAxis( _topCount, _hideCats, _sortDesc);
            }
            else if (brandsInColumns == true)
            {   // OCC1_BR, BRIA_M
                Side = CategorySideAxis((BoundField)f, c, 0);
            }
            else
            {
                Side = CategorySideAxis(c, _topCount, _hideCats, _sortDesc);
            }
            
        }
        public DimBenchmarkTable(DimAxisElements summaryElements, string language, string context, string desc, Field f, Container c, bool brandsInColumns, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            SummaryElements = summaryElements;
            GetElementsFromSummary();
            Side = SideAxis(AxisType.atAxis);
        }
        public DimBenchmarkTable(string description, Container c, DimFilter filter): base(null, null, description, null, c, filter)
        {

        }
        public DimBenchmarkTable(string language, string context, string description, BoundField f, Container c, DimFilter filter=null): base(language, context, description, f, c, filter)
        {
            // AE1Benchmark Table
            Base = "Exposed to Ad";
            Description = " Benchmark: " + Description;
        }
        public DimBenchmarkTable(string language, string context, string desc, BoundField f, Container c, string parentName, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            // for CL_BE1_5 Summary Benchmark of all attributes.
            Description = " Benchmark: " + Description;

        }
        public DimBenchmarkTable(string language, string context, string desc, Container c, DimFilter filter) : base(language, context, desc, null, c, filter)
        {

            // for CL_BE1_3
            Description = "Benchmark: " + Description;

        }

        public DimBenchmarkTable(string desc, Category brand, BoundField f, Container c, DimFilter filter): base(desc,brand,f,c,filter)
        {
            Description = "Benchmark: " + Description;
        }
        public DimBenchmarkTable(string desc, BoundField f, Container c, DimFilter filter) : base(desc, null, f, c, filter)
        {
            Description = "Benchmark: " + Description;
        }
        public DimBenchmarkTable(string language, string context, string desc, BoundField f, Container c, Int32 topCountOverride, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            // ABAW
            _benchmarkLabel = "Benchmark: Yes, aware of brand";
            Description = "Benchmark: " + Description;
            Side = CategorySideAxis(c, topCountOverride, _hideCats, false);

        }

        public DimBenchmarkTable(string language, string context, string desc, Container c, Container parent, DimFilter filter) : base(language, context, desc, null, c, filter)
        {
            // AE6_S_M
            Base = "Exposed to ad and recall ad";
            Description = "Benchmark: " + Description;
            Field = (BoundField)c.Items[0]; 
            Side = AE6_S_M_BenchmarkSideAxis(c, parent);
        
        }
        public DimBenchmarkTable(string language, string context, string desc, BoundField f, Container c, Container parent, string label, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            // AE5,  AE7
            Base = "Exposed to ad and recall ad";
            Description = "Benchmark: " + Description;
            Side = AEBenchmarkAttributeSideAxis(f, c, parent, label);
        }

        public string NumericSideAxis(UnboundField f)
        {
            
            Console.WriteLine("Numeric " + f.Name);
            throw new NotImplementedException();
            //return "";
        }

        

        public string CategorySideAxis(Int32 topCount = 0, bool hideCats = false, bool sortDescending = false)
        {
            string sideAxis = "";
            
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(Field.Items, sortDescending);
            List<string> catNames = (from Category c in Field.Items select c.Name).ToList();
            if (topCount > 0)
            {
                // displays descending categories UNDER the NET category
                //if(sortDescending==true)
                //{
                //    allCats = allCats.OrderByDescending(s => s.Name.Replace("_", ""));
                //}
                string netExp = DimAxisHelper.TopBoxNetExp(this, allCats, topCount, hideCats);
                sideAxis = "{0}{{'" + this.Base + "' base(), ";
                sideAxis = String.Format(sideAxis, Field.Name);
                sideAxis = sideAxis + netExp;
                //
                foreach (Category cat in allCats.Skip(allCats.Count() - topCount))
                {
                    sideAxis += ", " + cat.Name + " [IsHidden=" + hideCats.ToString() + "]";
                }
                sideAxis += "}";
            }
            else
            {
                // Displays categories in the programmed/survey order 
                sideAxis = "{0}{{'" + this.Base + "' base(), {1}}}";
                string sideCats = String.Join(", ", catNames);
                sideAxis = String.Format(sideAxis, Field.Name.ToUpper(), sideCats);
            }
            return sideAxis;
        }

        public string CategorySideAxis(BoundField field, Container container, Int32 topCount = 0)
        {
            // implementing this overload for the BRIA_M grid which has brands by columns
            // Also for OCC1_BR_GRID which has brands by columns
            // other Benchmark category axes are created from grids where brands are rows.
            // topCount -- probably want to exclude the "None" option from this calculation
            IEnumerable<Category> gridCats = DimAxisHelper.GetCategories(container.Items[0].Items, sortDesc: false);
            gridCats = gridCats.Where(o => ( ! ((InvalidBrandNames.Contains(o)) || (o.IsOtherCategory()))));
            //string leftExp = container.Name.ToString();
            string rightExp = field.Name.ToString(); // container.Items[0].Name.ToString().Replace("{" + container.CategoryMap[0].Name.ToString() + "}", ".."); 
            rightExp += "{ base() " + DimAxisHelper.ScriptCategories(gridCats, 0, true);
            rightExp += ", " + DimAxisHelper.BenchmarkAvgExpression(gridCats.ToList(), "Benchmark");
            rightExp += "}";

            return rightExp;
        }

        public string CategorySideAxis(Container container, Int32 topCount = 0, bool hideCats = false, bool sortDescending = false)
        {
            // For Grid Summary Tables...
            string[] varParts = Field.Name.Split('.');
            IEnumerable<Category> gridCats = DimAxisHelper.GetCategories(container.CategoryMap, sortDescending);
            string leftExp = container.Name + "{"; // base() " + DimAxisHelper.ScriptCategories(container.CategoryMap, 0, true);
            DimAxisElements gridItems = new DimAxisElements();
            foreach(Category cat in Container.CategoryMap)
            {
                string catLab = cat.Label(Context, Language, Container.Document);
                gridItems.Add(new DimAxisElement(cat.Name, DimAxisHelper.CleanText(catLab),"",true));
            }
            leftExp += gridItems.ToScript();
            leftExp += ", " + DimAxisHelper.BenchmarkAvgExpression(container.CategoryMap, "Net Benchmark");
            leftExp += "}";

            string rightExp = Container.Name + "." + varParts.Last();
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(Field.Items, sortDescending); 

            if (topCount != 0)
            {
                if (sortDescending == true)
                {
                    allCats = allCats.OrderByDescending(s => s.Name.Replace("_", ""));
                }
                DimAxisElement netElement = new DimAxisElement();
                
                string netExp = DimAxisHelper.ContainsAnyExpression("[" + varParts.Last() + "]", allCats.Take(topCount));
                netElement.Type = ExpressionType.etExpression;
                netElement.Expression = netExp; //"expression('" + netExp + "')";
                netElement.ID = "bm";
                netElement.Label = "Benchmark";
                rightExp += "{'" + this.Base + "' base()," + netElement.ToScript();
                IEnumerable<Category> otherCats = allCats.Skip(allCats.Count() - topCount);

            }
            else
            {
                rightExp += "{'" + this.Base + "' base()," + DimAxisHelper.ScriptCategories(allCats, topCount, hideCats);
                foreach (Category cat in allCats)
                {
                    rightExp += ", " + cat.Name + " [IsHidden=" + hideCats.ToString() + "]";
                }
            }
            rightExp += "}";
            string sideAxis = leftExp + " > " + rightExp;
            return sideAxis;
        }



        public string AEBenchmarkAttributeSideAxis(BoundField f, Container c, Container parent, string label)
        {
            // c.Items iteration of all
            // f.Items iteration of rating scale  
            // c.CategoryMap = attributes

            string exp = "axis({ b base(), ";
            Dictionary<Int32,string> derived = new Dictionary<Int32,string>();
            Dictionary<Int32,string> expressions = new Dictionary<Int32,string>();
            string condExp = ".ContainsAny({" + f.ItemAt(f.Items.Count - 2).Name + "," + f.Items.Last().Name + "})'";
            Int32 attCount = c.CategoryMap.Count;
            Int32 adCount = parent.CategoryMap.Count;

            // Just brute force the loop slicing
            for (Int32 adIndex = 1; adIndex <= adCount; adIndex++) 
            {
                for (Int32 attIndex = 1; attIndex <= attCount; attIndex++)
                {
                    Int32 catIndex = (attIndex - 1);
                    Int32 itmIndex = attIndex + ((adIndex - 1) * attCount) - 1;
                    string id = " ex" + expressions.Count.ToString();
                    //Console.WriteLine(itmIndex.ToString() + "    " + c.Items[itmIndex].Name + "  " + c.CategoryMap[catIndex].Label(Context, Language, c.Document));
                    string attExp = id;
                    string lab = String.Format(" '{0}' ", c.CategoryMap[catIndex].Label(Context, Language, c.Document));
                    attExp += lab + " expression('";
                    attExp += c.Items[itmIndex].Name.ToString() + condExp + ") [IsHidden = True]";
                    expressions.Add(itmIndex, attExp);
                    if(derived.ContainsKey(attIndex))
                    {
                        derived[attIndex] += "+" + id;
                    }
                    else
                    {
                        derived.Add(attIndex, id);
                    }
                }
            }

            // Adds the axis expressions to syntax expression
            exp += String.Join(", ", expressions.Values);

            // Adds the derived elements
            for (Int32 attIndex = 1; attIndex <= attCount; attIndex++)
            {
                string dExp = "d" + attIndex.ToString();
                dExp += String.Format(" '{0}' ", c.CategoryMap[attIndex-1].Label(Context, Language, c.Document)) + " derived('(";
                dExp += derived[attIndex] + ")/" + adCount.ToString() + "')";
                exp += ", " + dExp;
            }
            
            exp += "})" + String.Format(" '{0}'", label);
            return exp;
        }

        public string AE6_S_M_BenchmarkSideAxis(Container c, Container parent)
        {
            
            string[] varParts = Field.Name.Split('.');
            List<string> elements = new List<string>();
            string baseExp = "b '" + Base + "' base()";
            string axExp = "axis({";
            string exp = "";
            List<string> topCats = DimAxisHelper.GetCategoryNames((BoundField)Field, true).Take(2).ToList();
            string cats = String.Join(", ", topCats);
            string itm = varParts[varParts.Length - 1];
            elements.Add(baseExp);
            string key = null;
            Dictionary<string,List<string>> derived = new Dictionary<string, List<string>>();
            derived.Add("Learn", new List<string>());
            derived.Add("Talk", new List<string>());
            derived.Add("Do", new List<string>());
            foreach (Category _ad in parent.CategoryMap)
            {
                string adContainer = Container.Name.Replace("..", _ad.Name);

                key = "learn_" + _ad.Name;
                derived["Learn"].Add(key);
                exp = key +" 'Learn' expression('";
                exp += adContainer + "[" + Container.CategoryMap[0].Name + "]." + itm + ".ContainsAny({" + cats + "})";
                exp += " OR ";
                exp += adContainer + "[" + Container.CategoryMap[1].Name + "]." + itm + ".ContainsAny({" + cats + "})";
                exp += "') [IsHidden = True]"; // expression
                elements.Add(exp);

                key = "talk_" + _ad.Name;
                derived["Talk"].Add(key);
                exp = key + "'Talk' expression('";
                exp += adContainer + "[" + Container.CategoryMap[2].Name + "]." + itm + ".ContainsAny({" + cats + "})";
                exp += " OR ";
                exp += adContainer + "[" + Container.CategoryMap[3].Name + "]." + itm + ".ContainsAny({" + cats + "})";
                exp += " OR ";
                exp += adContainer + "[" + Container.CategoryMap[4].Name + "]." + itm + ".ContainsAny({" + cats + "})";
                exp += "') [IsHidden = True]"; // expression
                elements.Add(exp);

                key = "do_" + _ad.Name;
                derived["Do"].Add(key);
                exp = key + "'Do' expression('";
                exp += adContainer + "[" + Container.CategoryMap[5].Name + "]." + itm + ".ContainsAny({" + cats + "})";
                exp += " OR ";
                exp += adContainer + "[" + Container.CategoryMap[6].Name + "]." + itm + ".ContainsAny({" + cats + "})";
                exp += " OR ";
                exp += adContainer + "[" + Container.CategoryMap[7].Name + "]." + itm + ".ContainsAny({" + cats + "})";
                exp += "') [IsHidden = True]"; // expression
                elements.Add(exp);
            }

            foreach(string k in derived.Keys)
            {
                string bExp = String.Format("'Benchmark: {0} ' derived('({1})/{2}')", k, String.Join(" + ", derived[k]), derived[k].Count());
                elements.Add("d_" + k + bExp);
            }
            axExp += String.Join(", ", elements) + "})";
            return axExp;

        }

        public void GetElementsFromSummary()
        {
            DimAxisElements elements = SummaryElements;
            elements.RemoveBaseElements();
            foreach(DimAxisElement e in elements)
            {
                e.Properties[pIsHidden] = true;
                Elements.Add(e);
                
            }
            string bmExp = null;
            bmExp = "(" + String.Join("+", (from e in elements select e.ID)) + ") / " + elements.Count().ToString();

            Elements.Add(new DimAxisElement("benchmark", "Benchmark")
            {
                Type = ExpressionType.etDerived,
                Expression = bmExp
            });

        }

    }
}
