﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimNetflowDetailTable: DimDetailTable
    {
        public UnboundField Net { get; set; }
        public BoundField Inflow { get; set; }
        public BoundField Outflow { get; set; }
        public DimNetflowDetailTable(string description, Category brand, UnboundField net, BoundField inflow, BoundField outflow, DimFilter filter): base(description, brand, net, filter)
        {
            Net = net;
            Inflow = inflow;
            Outflow = outflow;
            Base = "Total Respondents";
            CreateElements();
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, brand.Document, true));
            Side = SideAxis();
        }
        public string SideAxis()
        {

            string sideAxis = "axis({" + Elements.ToScript() + "})";
            return sideAxis;
        }
        public void CreateElements()
        {
            Elements = new DimAxisElements();
            Elements.Add( CreateMeanElement(Net,1) );
            foreach(Category c in Inflow.Categories())
            {
                Elements.Add(new DimAxisElement()
                {
                    ID = c.Name + Inflow.Name,
                    Label = c.Label(Context, Language, c.Document, true),
                    Expression = DimAxisHelper.ContainsAnyExpression(Inflow, c)
                });

            }
            foreach (Category c in Outflow.Categories())
            {
                Elements.Add(new DimAxisElement()
                {
                    ID = c.Name + Outflow.Name,
                    Label = c.Label(Context, Language, c.Document, true),
                    Expression = DimAxisHelper.ContainsAnyExpression(Inflow, c)
                });

            }
            Elements.AddBaseElement(Base);

        }

    }
}
