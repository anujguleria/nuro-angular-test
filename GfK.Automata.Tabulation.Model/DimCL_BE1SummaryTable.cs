﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimCL_BE1SummaryTable : DimSummaryTable
    {
        private Char _delimiter = '.';
        private Int32 _topCount = 2;
        private bool _sortDesc = true;
        private bool _hideNetCats = true;
        private string _topParent = "CL_BE1_Loop";
        private Container _top = null;
        public DimCL_BE1SummaryTable(string language, string context, string desc, Field f, Container c, Int32 i = -1) : base(language, context, desc, f, c, null)
        {

            _top = (Container)Field.Document.Find(_topParent);
            Side = SummaryAttributeSideAxis(i  );

        }
 

        public string SummaryAttributeSideAxis(Int32 i)
        {
            // c.Items iteration of all
            // f.Items iteration of rating scale  
            // c.CategoryMap = brands

            //  axis({
            //    b base(), 
            //    ex1 'Attribute1 * Brand1' expression('CL_BE1_Loop[{_1}].CL_BE1_GRID[{_1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex2 'Attribute2 * Brand1' expression('CL_BE1_Loop[{_2}].CL_BE1_GRID[{_1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex3 'Attribute3 * Brand1' expression('CL_BE1_Loop[{_3}].CL_BE1_GRID[{_1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex4 'Attribute4 * Brand1' expression('CL_BE1_Loop[{_4}].CL_BE1_GRID[{_1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    d derived('(@1+@2+@3+@4) / 4')
            //    })

            //DimAxisHelper.GetCategories(f.Items, _sortDesc).ToList<Category>().Take(_topCount).ToList()[1].Name

            string label = "Overall Brand Equity";
            string[] varParts = Field.Name.Split(_delimiter);
            List<string> catLst = (from c in DimAxisHelper.GetCategories(Field.Items, _sortDesc).Take(_topCount).ToList() select c.Name).ToList();
            string exp = "axis({ b '" + Base + "' base() ";
            List<string> derived = new List<string>();

            string condExp = ".ContainsAny({" + String.Join(",", catLst) + "})'";
            Int32 attributeCount = _top.CategoryMap.Count;
            //for (int = 0; )
            for (Int32 n = 0; n < attributeCount; n ++)
            {
                exp += ", " + "" + " ex" + n.ToString() + " expression('";
                exp += _topParent + "[{" + _top.CategoryMap[n].Name + "}]." + varParts[1] + "." + varParts[2] + condExp + ") [IsHidden = True] ";
                //exp += varParts[0].Replace("_1", "_" + n.ToString()) + "." + varParts[1] + "." + varParts[2] + condExp + ") [IsHidden = true]";

                derived.Add("@" + (n+1).ToString());
            }
            exp += ", " + "d '"+ Container.CategoryMap[i].Label(Context,Language,Container.Document) + " " + label + "' derived('(" + String.Join("+", derived) + ") / " + attributeCount.ToString() + "')";
            exp += "}) " + "\"\"" + Description + "\"\"";
            return exp;
        }

    }

    
}
