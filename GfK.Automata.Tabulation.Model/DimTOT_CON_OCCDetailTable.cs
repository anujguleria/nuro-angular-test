﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimTOT_CON_OCCDetailTable  
    {
        // This class is different so far, in that it is not directly inheriting from base class DimDetailTable/DimAggregateTable
        // reason for this is to control the logic/iteration WITHIN this class and not in the DimTabulationProcessor
        //      this is the implementation that should've been done for other table classes.

        private static bool _hideCats = false;
        static DimTOT_CON_OCCDetailTable() //
        {

        }

        public static List<IScriptableItem> CreateTOT_CON_OCCDetailTables(string lang, string context, string desc, BoundField f, Container c, DimFilter filter, string endDate)
        {
            /* 
             * Creates a list of detail tables per spec 
             *   Detail table for all occasions we're reporting (Top 5 occasions + Net: Other) 
             *   by each brand showing full response scale.  5 occasions X up to 10 brands = 50 tables. 
             * 
             */
            string iterName = "OCC2";
            BoundField iterator = (BoundField)f.Document.Find(iterName);
            List<IScriptableItem> tables = new List<IScriptableItem>();
            IScriptableItem table = null;
            //string where = DimQueryData.MostRecentMonth(endDate);
            
            //List<KeyValuePair<Category, Int32>> sortCats = DimQueryData.SortedCategories(iterator, where);
            //List<Int32> topCounts = (from i in sortCats select i.Value).Take(5).ToList();
            //List<Category> topCats = (from i in sortCats.Where(i => topCounts.Min() <=i.Value) select i.Key).ToList();
            foreach (Category itm in iterator.Categories())
            {
                
                table = new DimDetailTable(lang, context, desc, f, c, filter);
                ((DimAggregatedTable)table).AddQTypeMod(itm.Name);
                ((DimDetailTable)table).Side = TOT_CON_SideAxis(((DimDetailTable)table), _hideCats);
                ((DimAggregatedTable)table).FormatDescription("[INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC2]", itm);
                ((DimDetailTable)table).AddFilter("","f", DimFilterExpressionBuilder.VariableContains(iterator, itm));
                ((DimAggregatedTable)table).FormatDescription("Brand", c);
                tables.Add(table);
            }

            return tables;

        }

        private static string TOT_CON_SideAxis(DimDetailTable t, bool hideCats)

        {
            string sideAxis = "";
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(t.Field.Items, false);
            
            t.Elements.AddBaseElement(t.Base);
            // Add each category:
            foreach(Category c in allCats)
            {
                t.Elements.Add(new DimAxisElement()
                {
                    ID = c.Name,
                    Label = c.Label(t.Context, t.Language, t.Field.Document, true),
                    Expression = t.Field.Name + "={" + c.Name + "}"
                });
            }
            // Add an element where Not Aware
            Category brand = t.Container.CategoryMap[t.Container.IndexOf(t.Field)];
            t.Elements.Add(new DimAxisElement()
            {
                ID = "eNotAware",
                Label = "Not aware",
                Expression = t.Field.Name + " Is Null"
            });
            t.Elements.Add( new DimAxisElement()
            {
                ID="C1",
                Label= "Consider (1,2,3)",
                Expression = DimAxisHelper.ContainsAnyExpression(t.Field.Name, allCats.Take(3))
            });
            t.Elements.Add(new DimAxisElement()
            {
                ID = "C2",
                Label = "1st/2nd Choice",
                Expression = DimAxisHelper.ContainsAnyExpression(t.Field.Name, allCats.Take(2))
            });
            
            sideAxis = "axis({";
            sideAxis += t.Elements.ToScript();
            sideAxis +=  "})"; 
            return sideAxis;
        }

    }
}
