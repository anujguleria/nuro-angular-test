﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimInflowTable: DimDefaultDetailTable
    {

        public DimInflowTable(string language, string context, string desc, Category brand, BoundField f, DimFilter filter) : base(language, context, desc, f, filter)
        {
            // this constructor receives a dummy BoundField object, which only exists at runtime
            // so we can define a correct GfKQType for this variable which doesn't exist.
            Base = "Total Respondents";
            Side = SideAxis();
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, brand.Document, true));
        }
        public string SideAxis()
        {
            CreateElements((BoundField)Field);
            string sideAxis = ((BoundField)Field).Name;
            sideAxis +=  "{" + Elements.ToScript() + "}";
            return sideAxis;
        }

        public DimInflowTable(string language, string context, string desc, Int32 level, Category brand, BoundField f, DimFilter filter) : base(language, context, desc, f, filter)
        {
            throw (new NotImplementedException());
            // this constructor receives a dummy BoundField object, which only exists at runtime
            // so we can define a correct GfKQType for this variable which doesn't exist.
            //Base = "Total Respondents";
            //Side = SideAxis(level);
            //Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, brand.Document, true));
        }
        public string SideAxis(Int32 level)
        {
            foreach(DimAxisElement e in InflowElements.GetElements(level))
            {
                Elements.Add(e);
            }
            Elements.AddBaseElement(Base);
            return "axis({" + Elements.ToScript() + "})";
        }
    }
}
