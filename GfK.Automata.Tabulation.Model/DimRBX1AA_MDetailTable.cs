﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimRBX1AA_MDetailTable: DimDefaultDetailTable
    {
        public DimRBX1AA_MDetailTable(string desc, BoundField f, Container c, DimFilter filter):base(null,null,desc,f,c,filter)
        {
            Base = "Total Respondents";
            CreateElements((BoundField)Field);
            Side = SideAxis();
        }
        public string SideAxis()
        {
            return Field.Name + "{" + Elements.ToScript() + "}";
        }
    }
}
