﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSTPULL_CELLDetailTable:DimDetailTable

    {

        public DimSTPULL_CELLDetailTable(string language, string context, string desc, Category brand, BoundField field, DimFilter filter): base(language,context,desc,field,filter)
        {
            AddQTypeMod("CELL");
            Side = SideAxis();
            string brandLabel = DimAxisHelper.CleanText(brand.Label(Context, Language, Field.Document));
            Description += " - " + brandLabel;
            
        }
        public string SideAxis()
        {
            // establishes the table's Elements collection
            CategorySideAxis(0, false, false);

            Elements.Add(new DimAxisElement()
            {
                ID = "secure",
                Label = "Secure Segment",
                Expression = Field.Name + ".ContainsAny({_2,_3,_6})"
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "neutral",
                Label = "Neutral Segment",
                Expression = Field.Name + ".ContainsAny({_1,_5,_9})"
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "atrisk",
                Label = "At Risk Segment",
                Expression = Field.Name + ".ContainsAny({_4,_7,_8})"
            });
            string sideAxis = Field.Name + "{" + Elements.ToScript() + "}";
            return sideAxis;
        }
    }
}
