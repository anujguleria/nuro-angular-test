﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimINFLOW_GRIDCrosstabTable: DimCrosstabTable
    {
        public DimINFLOW_GRIDCrosstabTable(string description, Category brand, BoundField field, BoundField sideField, BoundField topField, DimFilter filter): base(description,brand, field, sideField,topField,filter)
        {
            Base = "Total respondents";
            Side = SideAxis();
            Top = TopAxis();
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, brand.Document, true));

        }

    }
    
}
