﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimQuadrantTable: DimAggregatedTable
    {
        public Int32 X { get; set; }
        public Int32 Y { get; set; }
        public DimQuadrantTable(string desc, Category brand, Int32 x, Int32 y, Container c, DimFilter filter): base(desc, brand, null, c, filter)
        {
            X = x;
            Y = y;
            Description = String.Format(Description, X, Y);
        }
        public string SideAxis()
        {
            return "axis({" + Elements.ToScript() + "})";
        }
    }
}
