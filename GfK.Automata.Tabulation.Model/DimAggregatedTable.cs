﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pattern.Model;
using GfK.Automata.Model;


namespace GfK.Automata.Tabulation.Model
{
    public abstract class DimAggregatedTable : IScriptableItem
    {
        public string Description { get; set; }
        public string FilterDescription { get; set; }
        public string FilterExpression { get; set; }
        public string FilterName { get; set; }
        public string FilterLevel { get; set; }
        public string Name { get; set; }
        public string Side { get; set; }
        public string TableDoc { get; set; }
        public string Top { get; set; }
        public string Base { get; set; }
        public string Language { get; protected set; }
        public string Context { get; protected set; }
        public string Level { get; protected set; }
        public TableProperties Properties { get; set; } = new TableProperties();
        public DimTableAnnotations Annotations { get; set; } = new DimTableAnnotations();
        public Field Field { get; set; }
        public Dictionary<Category, DimAxisElement> FunnelRows { get; set; } = new Dictionary<Category, DimAxisElement>();
        public DimAxisElements Elements { get; set; } = new DimAxisElements();
        public Category Brand { get; set; }
        private string _qTypeModifier = "";
        public string QTypeModifier
        {
            get
            {
                return _qTypeModifier;
            }
            set
            {
                // appends another modifier
                if (QTypeModifier != "")
                {
                    _qTypeModifier += "." + value;
                }
                else
                {
                    _qTypeModifier = value;
                }
            }
        }
        public void AdjustRounding()
        {
            if(this is DimSummaryTable || this is DimBenchmarkTable)
            {
                Properties.SetProperty(TableProperties.AdjustRounding, "False");
            }
        }
        public string SideAxis(AxisType axisType)
        {
            switch (axisType)
            {
                case AxisType.atAxis:
                    AdjustRounding();
                    return "axis({" + Elements.ToScript() + "})";
                case AxisType.atField:
                    return Field.Name + "{" + Elements.ToScript() + "}";
                case AxisType.atContainer:
                    return Container.Name + "{" + Elements.ToScript() + "}";
            }
            return null;
        }
        public Container Container { get; protected set;  }
        public DimAggregatedTable(string language, string context)
        {
            
            Context = context;
            Language = language;
        }
        public DimAggregatedTable(string desc, Category brand, BoundField f, Container c, DimFilter filter)
        {
            Language = ProcessorConfiguration.Language;
            Context = ProcessorConfiguration.Context;
            Brand = brand;
            Description = desc;
            Brand = brand;
            Field = f;
            Container = c;
            AddFilter(filter);
        }
        public DimAggregatedTable(string desc, Container c, Container top, DimFilter filter)
        {
            Language = ProcessorConfiguration.Language;
            Context = ProcessorConfiguration.Context;
            Description = desc;
            Container = c;
            AddFilter(filter);
        }
        public DimAggregatedTable(string language, string context, string desc, Field f, Container c, DimFilter filter)
        {
            if (context is null) { Context = context; }
            else { Context = ProcessorConfiguration.Context; }
            if (language is null) { Language = language; }
            else { Language = ProcessorConfiguration.Language; }
            Description = desc;
            FilterDescription = null;
            FilterExpression = null;
            FilterName = null;
            Name = ""; 
            Side = null;
            TableDoc = null;
            Top = null;
            Base = null;
            Field = f;
            if(Field is null && c.Items.First() is Field)
            { Field = (Field)c.Items.First(); }
            Container = c;
            _SetDescription(desc);
            AddFilter(filter);
            if (this is DimDetailTable)
            { _qTypeModifier = ""; }
            else if (this is DimBenchmarkTable)
            { _qTypeModifier = ".BEN"; }
            else if (this is DimSummaryTable)
            {
                if (this is DimOCC1_BRSummaryTable)
                {
                    // even though this is a Summary table, we just want it named like OCC1_BR.AVG.1, etc. not OCC1_BR.SUM.AVG.1
                    QTypeModifier = "";
                    return;
                }
                _qTypeModifier = ".SUM";
            }

        }
        public DimAggregatedTable(string description, Category brand, BoundField field, DimFilter filter)
        {
            // crosstab. field = dummy object that contains the GfKQType property for this crosstab name.
            // used also with Netflwo_IN tables
            Language = ProcessorConfiguration.Language;
            Context = ProcessorConfiguration.Context;
            Description = description;
            Field = field;
            AddFilter(filter);
            
        }
        public DimAggregatedTable(string description, Category brand, UnboundField netField, DimFilter filter)
        {
            // using for Netflow tables
            Language = ProcessorConfiguration.Language;
            Context = ProcessorConfiguration.Context;
            Description = Description;
            Field = netField;
            AddFilter(filter);
        }
        public DimAxisElement CreateMeanElement(UnboundField f, Int32 decimals = 0)
        {
            DimAxisElement e = new DimAxisElement(decimals: decimals)
            {
                ID = "_" + f.Name,
                Label = f.Label(Context, Language, f.Document, true),
                Type = ExpressionType.etMean,
                Expression = f.Name
            };
            return e;
        }
        public DimAxisElements CreateElements(BoundField field)
        {
            DimAxisElements elements = new DimAxisElements();
            string con = ProcessorConfiguration.Context;
            string lan = ProcessorConfiguration.Language;
            foreach (Category cat in field.Categories())
            {
                {
                    string catLab = DimAxisHelper.CleanText(cat.Label(con, lan, cat.Document, true));
                    elements.Add(new DimAxisElement(cat.Name, catLab, DimAxisHelper.ContainsAnyExpression(field,cat)));
                }
            }
            elements.AddBaseElement(Base);
            return elements;
        }
        private void _SetDescription(string desc)
        {
            if (desc.Trim() == "" )
            {
                Description = Field.Label(Context, Language, Field.Document).Replace("\"", "'");
            }
            else
            {
                Description = desc;
            }

        }
        public void AddFilter(string desc, string name, string expression)
        {
            AddFilter(desc, name, expression, null);
        }
        public void AddFilter(string desc, string name, string expression, string level)
        {

            if (String.IsNullOrEmpty(FilterExpression))
            {
                FilterDescription += desc;
                FilterName += name;
                FilterExpression = expression;
                FilterLevel = level;
            }
            else
            {
                // Appends filter condition with an And operator if a FilterExpression already exists.
                // FilterName, FilterDescription can be re-assigned or overwritten in caller, but this method does not alter them.
                FilterExpression += " AND " + expression;
            }

        }
        public void AppendToFunnel(Dictionary<Category, DimAxisElement> dict, string label, string brandName)
        {
            AppendToFunnel(dict, label);
        }
        public void AppendToFunnel(Dictionary<Category, DimAxisElement> dict, string label)
        {
            foreach (KeyValuePair<Category, DimAxisElement> itm in dict)
            {
                Category c = itm.Key;
                DimAxisElement e = new DimAxisElement();
                e.Label = itm.Value.Label;
                e.ID = itm.Value.ID;
                e.Type = itm.Value.Type;
                e.Expression = itm.Value.GetExpression();
                
                try
                {
                    string funnelLevel = _getFunnelLevel(this);
                    DimFunnelTables.AddRow(c, e, label, funnelLevel);
                }
                catch (KeyNotFoundException)
                {
                    Console.WriteLine("Field does not contain gfkFunnelLevel:\t" + Field.Name);
                    // get out of here, none of the itms in this dict will work.
                    return;
                }
                
            }
        }
        private string _getFunnelLevel(IScriptableItem table)
        {
            if (table is DimSBAWTOTTable)
            { return DimSBAWController.FunnelLevel();  }
            return _getFunnelLevel();
        }
        private string _getFunnelLevel()
        {
            const string funnelProp = GfKProperties.GfKFunnelLevel;
            if (_getFunnelLevel(Container) != "")
            { return Container.Properties[funnelProp];  }
            if (_getFunnelLevel(Field) != "")
            { return Field.Properties[funnelProp]; }
            throw new KeyNotFoundException();
        }
        private string _getFunnelLevel(Container c)
        {
            if (c!= null && c.Properties.ContainsKey(GfKProperties.GfKFunnelLevel))
            { return c.Properties[GfKProperties.GfKFunnelLevel]; }
            return "";
        }
        private string _getFunnelLevel(Field f)
        {
            if (f != null && f.Properties.ContainsKey(GfKProperties.GfKFunnelLevel))
            { return f.Properties[GfKProperties.GfKFunnelLevel]; }
            return "";
        }
        private string _SBAWFunnelLevelOverride()
        {
            const string SBAW1 = "SBAW1";
            const string SBAW2_GRID = "SBAW2_GRID";
            // if container is null, we need to see if the field is SBAW1
            if (Container is null && (!(Field is null)) && (!(Field.Name == SBAW1)))
            { return ""; }
            
            // Container is null; Field == SBAW1 || Container is not null && Container == SBAW2_GRID

            if ((Container is null && (!(Field is null) && Field.Name == SBAW1)) || (!(Container is null) && Container.Name == SBAW2_GRID))
            {
                Field s1 = (Field)Field.Document.Find(SBAW1);
                Container s2 = (Container)Field.Document.Find(SBAW2_GRID);
                if (!(s1 is null) && !(s2 is null))
                {
                    List<Int32> levels = new List<Int32>();
                    string lv = _getFunnelLevel(s1);
                    if (lv != String.Empty) { levels.Add(Convert.ToInt32(lv)); }
                    lv = _getFunnelLevel(s2);
                    if (lv != String.Empty) { levels.Add(Convert.ToInt32(lv)); }
                    return levels.Min().ToString();
                }
                if(s2 is null) { return _getFunnelLevel(s1); }
                if(s1 is null) { return _getFunnelLevel(s2); }
            }
            return "";
        }
         public void AddQTypeMod(string qTypeMod)
        {
            QTypeModifier = qTypeMod;
        }
        public void AddFilter(DimFilter filter)
        {
            // parse a filter object and create the expression 
            if (!(filter is null))
            { AddFilter(filter.Description, filter.Name, filter.Expression, filter.Level); };

        }
        #region FORMATDESCRIPTION
        public void FormatDescription(string key, Category cat)
        {
            string desc = Description;

            desc = Description.Replace(key, cat.Label(Context, Language, cat.Document, true));
            desc = DimAxisHelper.CleanText(desc);
            Description = desc;

        }
        public void FormatDescription(string key, BoundField itm, Container c)
        {
            string desc = Description;

            desc = Description.Replace(key, c.CategoryMap[c.IndexOf(itm)].Label(Context, Language, c.Document, true));

            Description = desc;

        }

        public void FormatDescription(string key, Container c)
        {
            string desc = Description;

            for (Int32 i = 0; i < c.CategoryMap.Count; i++)
            {
                string cat = "{" + c.CategoryMap[i].Name + "}";
                if (Field.Name.Contains(cat) == true)
                {
                    string rep = c.CategoryMap[i].Label(Context, Language, c.Document, true);
                    if (rep == "") { rep = key + " " + i.ToString(); }
                    rep = rep.Replace("\"", "'");
                    desc = Description.Replace("[" + key.ToUpper() + "]", rep);
                    break;
                }
            }
            Description = DimAxisHelper.CleanText(desc);

        }
        public void FormatDescription(string key, string replacement)
        {
            Description = Description.Replace(key, replacement);
        }

        public void FormatDescription(string key, List<Container> items)
        {
            string desc = Description;
            foreach (Container itm in items)
            {
                foreach (Category _i in itm.CategoryMap)
                {
                    if (Field.Name.Contains(_i.Name))
                    {
                        desc = Description.Replace("[" + key.ToUpper() + "]", _i.Label(Context, Language, Field.Document, true));
                    }
                }
            }
            Description = desc;
        }

        public void FormatDescription(string key, BoundField var, Category itm, BoundField mappedVar)
        {
            // retreives a category label by INDEX from the mapped var
            // used for "past tensing", etc.
            Int32 catIndex = -1;
            Category mappedCat = null;
            try
            {
                catIndex = var.Categories().IndexOf(itm);
                mappedCat = mappedVar.Categories()[catIndex];
            }
            catch (IndexOutOfRangeException e)
            {
                // if index mapping will not suffice, do not attempt anything fancy, just exit gracefully
                return;
            }
            Description = Description.Replace(key, mappedCat.Label(Context, Language, var.Document, true));
            
        }
        #endregion FORMATDESCRIPTION

        protected void SetTabulationLevel()
        {
            string lvl = null;
            if (!(Container is null))
            {
                lvl = Container.Name.Split('.').First();
            };
            Level = lvl;
        }
        public void Apply( Field field, GfK.Automata.Pattern.Model.Pattern p, string language, string context )
        {
            // To do:  apply patterns to set DimAggregatedTable properties
        }

        public string GetScript(ScriptType type)
        {
            switch(type)
            {
                case ScriptType.stDimensions:
                    return _toDimensionsScript();
                default:
                    throw new NotImplementedException();
            }
        }

        private string _formatForScripter( string axisExpression )
        {
            if (axisExpression is null) return null;
            char[] characters = axisExpression.ToCharArray();
            int charCount = characters.Count();
            bool isInQuotedString = false;
            int indentLevel = 1;
            StringBuilder sb = new StringBuilder();
            char character;
            for( int i = 0; i < charCount; i++ )
            {
                character = characters[i];
                bool addNewline = false;
                switch( character )
                {
                    case '(':
                        if (!isInQuotedString) indentLevel++;
                        break;
                    case ')':
                        if (!isInQuotedString) indentLevel--;
                        break;
                    case ',':
                        if (!isInQuotedString)
                        {
                            addNewline = true; 
                            if ( i < charCount - 1 )
                            {
                                if ( characters[ i + 1 ] == ' ' )
                                {
                                    i++;
                                }
                            }
                        }
                        break;
                    case '\'':
                        if ( i < charCount - 1 )
                        {
                            if (i == 0)
                            {
                                isInQuotedString = true; 
                            }
                            else
                            {
                                if (characters[i + 1] != '\'' && characters[i - 1] != '\'') 
                                {
                                    isInQuotedString = !isInQuotedString;
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
                sb.Append(character);
                if (addNewline)
                {
                    sb.Append("\" _");
                    sb.AppendLine();
                    sb.Append("".PadLeft(indentLevel * 4));
                    sb.Append("+ \"");
                    addNewline = false;
                }
            }
            return sb.ToString();
        }

        private string _toDimensionsScript()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("' ");
            sb.Append(Name);
            if ( Description != null )
            {
                sb.Append(":  ");
                sb.Append(Description);
            }
            if ( FilterDescription != null )
            {
                sb.Append(" (");
                sb.Append(FilterDescription);
                sb.Append(")");
            }
            sb.AppendLine();
            sb.Append(TableDoc);
            sb.Append("Set t = .Tables.AddNew(\"");
            sb.Append(Name);
            sb.Append("\", \"");
            sb.Append(_formatForScripter(Side));
            if (Top != null)
            {
                sb.Append(" * ");
                sb.Append(_formatForScripter(Top));
            }
            sb.Append("\"");
            if (Description != null)
            {
                sb.Append(", \"");
                sb.Append(Description);
                sb.Append("\"");
            }
            sb.Append(")");
            if ( FilterName != null )
            {
                sb.AppendLine();
                sb.Append("t.Filters.AddNew(\"");
                sb.Append(FilterName);
                sb.Append("\", \"");
                sb.Append(FilterExpression);
                sb.Append("\"");
                if ( FilterDescription != null )
                {
                    sb.Append(", \"");
                    sb.Append(FilterDescription);
                    sb.Append("\"");
                }
                if (FilterLevel != null)
                {
                    sb.Append(", \"");
                    sb.Append(FilterLevel);
                    sb.Append("\"");
                }
                sb.Append(")");
            }
            if (Level != null )
            {
                sb.AppendLine();
                sb.Append("t.Level=\"" + Level + "\"");
                sb.AppendLine();
            }
            if (Annotations.Count() > 0)
            {
                
                foreach(DimAnnotation a in Annotations)
                {
                    string i = Convert.ToInt32(a.Index).ToString();   
                    sb.AppendLine();
                    sb.AppendLine("Set a = TableDoc.Default.Annotations[" + i + "]");
                    sb.AppendLine("t.Annotations[" + i + "].Specification = a.Specification + \"\\n" + a.Specification + "\"");
                    sb.AppendLine();
                }
            }
            if (Properties.Count() > 0)
            {
                foreach(string k in Properties.Keys)
                {
                    sb.AppendLine();
                    sb.AppendLine("SetProperty( t.Properties, " + k + ", " + Properties[k] + " )");
                }
                
            }
            sb.AppendLine();
            sb.AppendLine();
            return sb.ToString();
        }
    }
}
