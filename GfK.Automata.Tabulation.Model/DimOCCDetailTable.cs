﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimOCCDetailTable: DimDetailTable 
    {
        // Top 5 occasions (highest %) from response list and Neta ll other occasions together as "Other"
        // recalibrate Top 5 after each year
        private Int32 _topCount = 5;
        private string _where { get; set; }
        private Category _otherCat { get; set; }
        private IEnumerable<Category> _allCats { get; set; }
        private IEnumerable<Category> _topCats { get; set; }
        private IEnumerable<Category> _otherCats { get; set; }
        public DimOCCDetailTable(string language, string context, string desc, BoundField f, Container c, DimFilter filter, string maxDate) : base(language, context, desc, f, c, filter)
        {
            _where = DimQueryData.MostRecentMonth(maxDate);
            _allCats = ((BoundField)Field).Categories();
            _otherCat = _allCats.Where(o => o.IsOtherCategory()).First();
            
            SetElements();
            Side = SideAxis(AxisType.atField);

        }

        public void SetElements()
        {
            Elements = new DimAxisElements();
            SortCategories();
            foreach (Category itm in _topCats)
            {
                DimAxisElement e = new DimAxisElement()
                {
                    ID = "e" + itm.Name,
                    Label = itm.Label(Context, Language, Field.Document),
                    Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, itm)
                };
                Elements.Add(e);
            }
            // append the remaining sorted categories
            foreach (Category itm in _otherCats)
            {
                DimAxisElement e = new DimAxisElement()
                {
                    ID = itm.Name,
                    Label = itm.Label(Context, Language, Field.Document),
                    Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, itm),
                };
                Elements.Add(e);
            }
            
            Elements.AddBaseElement(Base);
        }
        public string SideAxis(BoundField f, int topCount, bool hideCats, bool sortDescending, string where)
        {

            _allCats = DimAxisHelper.GetCategories(f.Items, sortDescending);
            List<Category> otherCats = _allCats.ToList();
            List<KeyValuePair<Category, Int32>> sortCats = DimQueryData.SortedCategories(f, where);
            // top 5 counts
            List<Int32> topCounts = (from i in sortCats select i.Value).Take(topCount).ToList();
            List<Category> topCats = (from i in sortCats.Where(i => topCounts.Min() <= i.Value) select i.Key).ToList();
            
            DimAxisElements expressions = new DimAxisElements();
            DimAxisElements netExp = new DimAxisElements();
            expressions.AddBaseElement(Base);
            foreach(Category itm in topCats)
            {
                DimAxisElement e = new DimAxisElement()
                {
                    ID = "e" + expressions.Items.Count.ToString(),
                    Label = itm.Label(Context, Language, f.Document),
                    Expression = DimAxisHelper.ContainsAnyExpression(f.Name, itm)
                };
                otherCats.Remove(itm);
                expressions.Add(e);
            }
            
            foreach (Category cat in otherCats)
            {
                DimAxisElement e = new DimAxisElement()
                {
                    ID = cat.Name,
                    Label = cat.Label(Context, Language, f.Document),
                    Expression = DimAxisHelper.ContainsAnyExpression(f.Name, cat),
                };
                netExp.Add(e);
            }
            DimAxisElement net = new DimAxisElement() { ID = "NET", Label = "Other (Net)" };
            net.Type = ExpressionType.etNet;
            net.Expression = netExp.ToScript();
            expressions.Add(net);

            string sideAxis = f.Name + "{";
            sideAxis += expressions.ToScript() + "}";
            return sideAxis;
        }

        public void SortCategories()
        {
            // We need to sort the categories by frequency, but fix the "Other" category at bottom of list
            
            List<KeyValuePair<Category, Int32>> sortCats = DimQueryData.SortedCategories((BoundField)Field, _where);
            KeyValuePair<Category,Int32> other = sortCats.Where(o => o.Key.Equals(_otherCat)).First();
            sortCats.Remove(other);
            IEnumerable<Int32> topCounts = (from i in sortCats select i.Value).Take(_topCount);
            _topCats = (from i in sortCats.Where(i => topCounts.Min() <= i.Value) select i.Key);

            _otherCats = (from i in sortCats.Where(o => o.Value < topCounts.Min()) select i.Key);

            List<Category> finalOtherCats = _otherCats.ToList();
            finalOtherCats.Add(_otherCat);
            _otherCats = finalOtherCats;
        }
    }
}
