﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAE1DetailTable: DimDetailTable
    {
        public DimAE1DetailTable(string description, Int32 catIndex, BoundField field, Container container, DimFilter filter): base(description,field,container,filter)
        {
            Base = "Exposed to Ad";
            //this.QTypeModifier = "AE1";
            SetElements();
            Category Ad = container.CategoryMap[catIndex];
            Side = SideAxis(AxisType.atField);
            FormatDescription("[AD]", DimAxisHelper.CleanText(Ad.Label(Context,Language,Field.Document,true)));
        }
        public void SetElements()
        {
            Elements.AddBaseElement(Base);
            foreach(Category cat in ((BoundField)Field).Categories())
            {
                Elements.Add(cat);
            }
            Elements.Add(new DimAxisElement()
            {
                ID = "tot",
                Label= "Total",
                Type = ExpressionType.etTotal,
                Expression = "total()",
                
            });
        }
    }
}
