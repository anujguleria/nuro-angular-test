﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;


namespace GfK.Automata.Tabulation.Model
{
    public class DimPI_OCCDetailTable : DimDetailTable
    {
        private Int32 _topCount = 2;
        private bool _hideCats = false;
        private bool _sortDesc = true;
        private List<string> _CatList { get; set; }
        public DimPI_OCCDetailTable(string language, string context, string desc, BoundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            _BuildTable(null);
        }
        public DimPI_OCCDetailTable(string language, string context, string desc, BoundField f, Container c, BoundField filterVar, Category occ, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            _BuildTable(filterVar);

            if (filterVar != null)
            {
                FormatDescription("[INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC2]", occ );
                AddFilter("OCC1 & OCC2 asked (Aware of brand)",
                        "OccAware", DimFilterExpressionBuilder.VariableContains(filterVar, occ) );
                
            }
        }
        
        private void _BuildTable(BoundField filterVar)
        {
            _getModifier();
            _setCatLst();
            Base = "OCC1 & OCC2 asked (Aware of brand)";
            CreateElementsTop2SideAxis(_CatList, _topCount, "PRIORITY_BRAND_AWARE");
            Side = SideAxis(AxisType.atAxis);
            string s = Container.CategoryMap[Container.IndexOf(Field)].Label(Context, Language, Field.Document);
            s = DimAxisHelper.CleanText(s);
            Side += " '" + s + "'";
            Description += " " + s;
        }

        private void _setCatLst()
        {
            _CatList = new List<string>();
            _CatList.Add("Very likely to purchase");        //_4
            _CatList.Add("Somewhat likely to purchase");
            _CatList.Add("Somewhat unlikely to purchase");
            _CatList.Add("Very unlikely to purchase");          //_1
        }

        private void _getModifier()
        {
            // append brandname to the qtype
            
            _getModifier(Container.CategoryMap[Container.Items.IndexOf(Field)]);
        }
        private void _getModifier(Category c)
        {
            AddQTypeMod(c.Name);
        }
        private void _getModifier(string s)
        {
            AddQTypeMod(s);
        }
    }
}
