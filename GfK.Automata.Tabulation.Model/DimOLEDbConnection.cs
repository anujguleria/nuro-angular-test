﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimOLEDbConnection
    {
        public const string
            OrderNone = "",
            OrderAsc = "ASC",
            OrderDesc = "DESC";
        public enum dbTableName
        {
            dbVDATA = 0,
            dbHDATA
        }
        private const string
            _VDATA = "VDATA",
            _HDATA = "HDATA";
        private static string _TableName { get; set; }
        public enum dbCategoryReturnType
        {
            dbValues = 0,
            dbNames = 1
        }
        public static dbCategoryReturnType ReturnType { get; set; }
        private static string _connString { get; set; }
        private static string _query { get; set; }
        private static List<dynamic> _allValues { get; set; }
        public static OleDbConnection Conn { get; set;}
        private static OleDbCommand _command { get; set; }
        private static OleDbDataReader _rs  { get; set;}
        public static string ConnString { get{ return _connString; } }
        static DimOLEDbConnection()
        {
        }

        public static void SetConnectionString(string language, string context, string path, string ddfPath, dbCategoryReturnType returnType= dbCategoryReturnType.dbNames, dbTableName tableName=dbTableName.dbVDATA)
        {
            if (ddfPath == null || ddfPath.Trim().Length == 0)
            {
                ddfPath = path.Replace(".mdd", ".ddf");
            }
            OleDbConnectionStringBuilder CSB = new OleDbConnectionStringBuilder()
            {
                Provider = "mrOleDB.Provider.2"
            };
            CSB.Add("MR Init Category Names", (Int32)returnType);  // 1 = use category NAMES instead of mapped values for categorical data
            CSB.Add("MR Init Category Context", context);
            CSB.Add("MR Init Category Language", language);
            CSB.Add("Initial Catalog", path);
            CSB.Add("Location", ddfPath);
            CSB.Add("Data Source", "mrDataFileDsc");
            string cs = CSB.ToString();

            if (cs != _connString)
            {
                // if we're changing the return type, or any other connection string prop, we need to re-instantiate the Conn object
                Conn = null;
                Conn = new OleDbConnection(cs);
                ReturnType = returnType;
            }
           
            _command = new OleDbCommand();
            // set the table level
            if (tableName == dbTableName.dbVDATA)
            {
                _TableName = _VDATA;
            }
            else
            { _TableName = _HDATA; }

        }

        private static void _openConn()
        {
            try
            {
                if (Conn is null  )
                {
                    Conn = new OleDbConnection(_connString);
                    Conn.Open();
                }
                else if (Conn.State == System.Data.ConnectionState.Closed)
                {
                    Conn.Open();
                }
            }
            catch (Exception ex)
            {
                
                Console.WriteLine("Error opening connection string \n\n" + _connString);
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }

        }
        private static void _closeConn()
        {
            Conn.Close();
            if (!(_rs is null))
            {
                _rs.Close();
            }
            
            //if (_persist == false)
            //{
            //    _conn.Close();
            //}
        }
        public static bool ColumnIsEmpty(Field f)
        {
            string col = _formatColumnName(f.Name);
            _openConn();
            string q = "SELECT COUNT(" + col + ") FROM [" + _TableName + "] WHERE ";
            q += col + " Is Not Null";
            _query = q;
            _command.CommandText = _query;
            _command.Connection = Conn;
            _rs = _command.ExecuteReader();
            bool isEmpty = true;
 
            while (_rs.Read() && isEmpty)
            {
                isEmpty = (Convert.ToInt32(_rs.GetValue(0).ToString()) == 0 );
            }
            _rs.Close();
            _closeConn();
            return isEmpty;
        }
        public static Dictionary<Category,string> CategoryFrequencies(BoundField f, string where, string order = OrderDesc)
        {
            Dictionary<Category, string> d = new Dictionary<Category, string>();

            string col = _formatColumnName(f.Name);
            _openConn();
            foreach (Category cat in f.Categories())
            {
                
                string q = "SELECT COUNT(" + col + ".ContainsAny({" + cat.Name + "})) FROM [" + _TableName + "] WHERE ";
                q += "(" + col + ".ContainsAny({ " + cat.Name + "}) = True)";
                q += " AND (" + where + ")";
                q += " GROUP BY " + col;
                _query = q;
                //_query = "SELECT COUNT(" + col + ".ContainsAny({" + cat.Name + "})) FROM VDATA WHERE (" + col + ".ContainsAny({" + cat.Name + "}) = True)";
                _command.CommandText = _query;
                _command.Connection = Conn;
                _rs = _command.ExecuteReader();
                Int32 thisCount = 0;
                while (_rs.Read())
                {
                    thisCount += (Int32)_rs.GetValue(0);
                    if (d.ContainsKey(cat))
                    {
                        d[cat] = thisCount.ToString();
                    }
                    else
                    {
                        d.Add(cat, thisCount.ToString());
                    }
                }
                _rs.Close();
            }
            _closeConn();
            return d;
        }

        public static List<String> GetFreqSortedValues(string columnName, string where = "", string order = OrderDesc)
        {
            List<String> vals = new List<String>();

            //columnName = _formatColumnName(columnName);
            _openConn();
            _query = "SELECT " + _formatColumnName(columnName) + ", ";
            _query += "COUNT(*) FROM [" + _TableName + "] ";
            _query += " WHERE " + _formatColumnName(columnName) + " IS NOT NULL ";
            if(where!="")
            {
                _query += "AND " + where;
            }
            _query += " GROUP BY " + _formatColumnName(columnName) + " ";
            _query += " ORDER BY COUNT(*) " + order;
                
            _command.CommandText = _query;
            _command.Connection = Conn;
            _rs = _command.ExecuteReader();

            while (_rs.Read())
            {
                vals.Add(_rs.GetValue(0).ToString());
            }
            _closeConn();
            return vals;
        }

        public static List<dynamic> GetDistinctValues(Container c, string where = "", string order = OrderNone)
        {
            List<dynamic> vals = new List<dynamic>();
            _openConn();
            Int32 i = 0;
            foreach (Category cat in c.CategoryMap)
            {

                string columnName = c.Items[i].Name;  //c.Name + "[{" + cat.Name + "}]" ;
                List<dynamic> getVals = GetDistinctValues(columnName, where, order);
                vals.AddRange(getVals);
                i++;
            }
            _closeConn();
            return vals.Distinct().ToList();
        }
        
        public static List<dynamic> GetDistinctValues(string columnName, string where = "", string order = OrderNone )
        {
            // returns distinct values of a column or columns
            //columnName = _formatColumnName(columnName);
            List<dynamic> vals = new List<dynamic>();
            _openConn();
            _query = "SELECT DISTINCT " + _formatColumnName(columnName) + " ";
            _buildQueryString(where, order, _formatColumnName(columnName));
            _command.CommandText = _query;
            _command.Connection = Conn;
            _rs = _command.ExecuteReader();
            
            while (_rs.Read())
            {
                vals.Add(_rs.GetValue(0));
            }
            _closeConn();
            return vals;
            
        }
        public static List<dynamic> GetValues(string columnName, string where = "")
        {
            switch (_TableName)
            {
                case _HDATA:
                    return new List<dynamic>();
                case _VDATA:
                default:
                    return _getValuesFromVData(columnName, @where);
            }
        }
        private static List<dynamic> _getValuesFromHData(string columnName, string where = "")
        {
            throw (new NotImplementedException());
            //return null;
        }
        private static List<dynamic> _getValuesFromVData(string columnName, string where = "")
        {

            // wrap the column name in double-quotes in case it contains square brackets,etc.
            //columnName = _formatColumnName(columnName);

            // returns all of the values of a column or columns
            _openConn();
            List<dynamic> vals = new List<dynamic>();
            _query = "SELECT " + _formatColumnName(columnName) + " ";
            _buildQueryString(where, OrderNone, _formatColumnName(columnName));
            _command.CommandText = _query;
            _command.Connection = Conn;
            _rs = _command.ExecuteReader();

            while (_rs.Read())
            {
                vals.Add( _rs.GetValue(0) );
            }
            _closeConn();
            _allValues = vals.Select(v => (dynamic)v).ToList();
            return vals;
        }
        public static DateTime GetMaxDate(string columnName, string where = "")
        {
            _openConn();
            _query = "SELECT MAX( LocalToUTCTime( " + _formatColumnName(columnName) + ", GetTimeZone() ) )";
            _buildQueryString(where, OrderNone, _formatColumnName(columnName));
            _command.CommandText = _query;
            _command.Connection = Conn;
            dynamic d = _command.ExecuteScalar();
            return Convert.ToDateTime(d);
        }
        public static dynamic GetMaxValue(string columnName, string where = "")
        {
            // returns the max value of a column or columns
            _openConn();
            //columnName = _formatColumnName(columnName);
            _query = "SELECT MAX(" + _formatColumnName(columnName) + ") ";
            _buildQueryString(where, OrderNone, _formatColumnName(columnName));
            _command.CommandText = _query;
            _command.Connection = Conn;
            dynamic max = _command.ExecuteScalar();
            _closeConn();
            return _validNumericReturnType(max);
            
        }
        public static dynamic GetMinValue(string columnName, string where = "")
        {
            // returns the min value of a column or columns
            _openConn();
            _query = "SELECT MIN(" + _formatColumnName(columnName) + ") ";
            _buildQueryString(where, OrderNone, _formatColumnName(columnName));
            _command.CommandText = _query;
            _command.Connection = Conn;
            dynamic min = _command.ExecuteScalar();
            _closeConn();

            return _validNumericReturnType(min);

        }

        
        public static List<double> GetPercentileRange(string columnName, double min, double max , string where = "" )
        {
            // returns the min and the max data values based upon two percentile parameters defining the min and max constraints, inclusive
            List<double> vals = new List<double>();
            _openConn();
            //columnName = _formatColumnName(columnName);
            _query = String.Format("SELECT {0}", _formatColumnName(columnName));
            _buildQueryString(where, OrderAsc, _formatColumnName(columnName));
            _command.CommandText = _query;
            _command.Connection = Conn;
            _rs = _command.ExecuteReader();
            while (_rs.Read())
            {
                vals.Add(double.Parse(_rs.GetValue(0).ToString()));
            }
            _allValues = vals.Select(v => (dynamic)v).ToList();
            
            double[] ret = new double[2];
            double minVal = _getPercentileValue(vals.ToArray(), min);
            double maxVal = _getPercentileValue(vals.ToArray(), max);
            List<dynamic> range = _allValues.Where(v => (v >= minVal && v <= maxVal)).Distinct().ToList();
            ret[0] = minVal;
            ret[1] = maxVal;

            _closeConn();
            return ret.ToList();
        }

        public static List<dynamic> GetPercentileValues(string columnName, double min, double max, string where = "")
        {
            // return a list of all distinct values in the range inclusive of endpoints.
            // this method does not directly invoke the _openConn or _closeConn, those are handled by the GetPercentileRange
            
            List<double> range = GetPercentileRange(columnName, min, max, where);
            return _allValues.Where(v => (v >= range[0] && v <= range[1])).Distinct().ToList();
        }

        public static List<dynamic> GetPercentileValues(Container c, string[] labels, string where = "")
        {
            //List<dynamic> vals = new List<dynamic>();
            //_openConn();
            //_persist = true;
            //Int32 i = 0;
            //double min = 0;
            //double max = 0;

            //foreach (Category cat in c.CategoryMap)
            //{
               
            //    string columnName = c.Items[i].Name;
            //    List<dynamic> getVals = GetPercentileValues(columnName, min, max, where);
            //    vals.AddRange(getVals);
            //    i++;
            //}
            //_persist = false;
            //_closeConn();
            return null;
        }
        private static double _getPercentileValue(double[] sequence, double pct)
        {

            int N = sequence.Length;
            double n = (N - 1) * pct + 1;
            if (n == 1d) return sequence[0];
            else if (n == N) return sequence[N - 1];
            else
            {
                int k = (int)n;
                double d = n - k;
                return sequence[k - 1] + d * (sequence[k] - sequence[k - 1]);
            }
        }

        private static void _buildQueryString(string where, string order, string columnName="")
        {
            // where clause is optionally provided as SQL syntax
            // order defines the ORDER BY clause

            //_query += " ORDER BY [" + columnName + "] ASC";

            // 
            //columnName = _formatColumnName(columnName);

            //
            if (where != "")
            {
                where = " WHERE " + where + " AND " + columnName + " Is Not Null";
            }
            else
            {
                where = " WHERE " + columnName + " Is Not Null";
            }

            // sort the result set if requested to do so
            string orderBy = "";
            if (order!=OrderNone && columnName != "")
            {
                orderBy = String.Format(" ORDER BY {0} {1} ", columnName, order);
            }
            else
            {
                orderBy = "";
            }
            _query += String.Format(" FROM {0} {1} {2}", _TableName, where, orderBy);
        }
        private static string _formatColumnName(string columnName)
        {
            List<Char> chars = new List<Char>(new Char[] { '[', '{', ']', '}', '.' });
            foreach(Char c in chars)
            {
                if (columnName.Contains(c.ToString()))
                {
                    columnName = "\"" + columnName + "\"";
                    return columnName;
                }
            }

            return "[" + columnName + "]";
        }
        private static dynamic _validNumericReturnType(dynamic val, bool allowString=false, bool allowBool=false)
        {
            // The return type may be dynamic (likely int, float, or datetime).  
            // Throw a NotImplementedException for any other data type
            if (val == null)
            {
                return null;
            }
            switch (val.GetType().Name)
            {
                case "DBNull":
                    return null;
                case "Int32":
                    return (Int32)val;
                case "Int64":
                    return (Int64)val;
                case "DateTime":
                    return (DateTime)val;
                case "String":
                    if(allowString)
                    {
                        return (string)val;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                case "Bool":
                case "Boolean":
                    if(allowBool)
                    {
                        return (bool)val;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                   
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
