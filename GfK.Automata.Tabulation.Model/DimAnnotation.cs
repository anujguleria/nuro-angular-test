﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAnnotation
    {
        public AnnotationType Index { get; set; }
        public string Specification { get; set; }
        public DimAnnotation(AnnotationType i, string specification)
        {
            Index = i;
            Specification = "Note: " + specification;
        }

    }
}
