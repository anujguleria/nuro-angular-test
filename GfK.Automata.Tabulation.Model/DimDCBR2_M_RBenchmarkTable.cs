﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimDCBR2_M_RBenchmarkTable: DimBenchmarkTable
    {
        private const string
            _base = "base",
            _atRisk = "atrisk",
            _strong = "strong",
            _weak = "weak";
        private List<Category> _Brands { get; set; }
        private string[] _parts { get; set; }
        private Dictionary<string, DimAxisElements> _elements { get; set; } = new Dictionary<string, DimAxisElements> {
            { _base, new DimAxisElements() },
            { _weak, new DimAxisElements() },
            { _strong, new DimAxisElements() },
            { _atRisk, new DimAxisElements() }
        };
        public DimDCBR2_M_RBenchmarkTable(string description, BoundField field, Container container, DimFilter filter): base(description, field, container, filter)
        {
            Base = "Base"; //: Respondents who were asked the brand";
            _Brands = ((BoundField)Field).Categories().Where(b => !(InvalidBrandNames.Contains(b))).ToList();
            _parts = Field.Name.Split('.');
            CreateElements();
            Level = "HDATA";
            Side = SideAxis();
            

        }
        public string SideAxis()
        {
            return Container.Name + "{" + Elements.ToScript() + "}";
        }
        public void CreateElements()
        {
            /*
           DCBR_M.DCBR2_M{
               bOldSpice 'Base OldSpice'   expression('[CBR].ContainsAny({OldSpice})')[IsHidden = True],
               wwOldSpice ''               expression('LevelID.ContainsAny({Statement1,Statement2,Statement3}) AND [CBR].ContainsAny({OldSpice})')[IsHidden = True],
               wOldSpice 'Weak Old Spice'  derived('(wwOldSpice / bOldSpice) * 100')[CountsOnly = True],
               ssOldSpice ''               expression('LevelID.ContainsAny({Statement4,Statement5,Statement6}) AND [CBR].ContainsAny({OldSpice})')[IsHidden = True],
               sOldSpice 'Strong Old Spice' derived('(ssOldSpice / bOldSpice) * 100')[CountsOnly = True],
               aaOldSpice ''               expression('LevelID.ContainsAny({Statement7,Statement8,Statement9}) AND [CBR].ContainsAny({OldSpice})')[IsHidden = True],
               arOldSpice 'At Risk Old Spice' derived('(aaOldSpice / bOldSpice) * 100')[CountsOnly = True]
               }
          */

            foreach (Category brand in _Brands)
            {
                _createBrandElements(brand);
            };
            Elements.Add(_elements[_base]);
            Elements.Add(_elements[_weak]);
            Elements.Add(_elements[_strong]);
            Elements.Add(_elements[_atRisk]);

            DimAxisElement e = null;
            Int32 denominator = _Brands.Count();
            string exp = null;
            IEnumerable<string> itms = null;

            // Weak
            itms = (from itm in _elements[_weak].Where(o => o.Type == ExpressionType.etDerived) select itm.ID);
            exp = "(" + String.Join("+", itms) + ") / " + denominator.ToString();
            e = new DimAxisElement(countsOnly:true)
            {
                ID = "bmWeak",
                Label = "Weak Benchmark",
                Type = ExpressionType.etDerived,
                Expression = exp
            };
            Elements.Add(e);
            // Strong
            itms = (from itm in _elements[_strong].Where(o => o.Type == ExpressionType.etDerived) select itm.ID);
            exp = "(" + String.Join("+", itms) + ") / " + denominator.ToString();
            e = new DimAxisElement(countsOnly: true)
            {
                ID = "bmStrong",
                Label = "Strong Benchmark",
                Type = ExpressionType.etDerived,
                Expression = exp
            };
            Elements.Add(e);
            // At Risk
            itms = (from itm in _elements[_atRisk].Where(o => o.Type == ExpressionType.etDerived) select itm.ID);
            exp = "(" + String.Join("+", itms) + ") / " + denominator.ToString();
            e = new DimAxisElement(countsOnly: true)
            {
                ID = "bmAtRisk",
                Label = "At Risk Benchmark",
                Type = ExpressionType.etDerived,
                Expression = exp
            };
            Elements.Add(e);
            Elements.AddBaseElement(Base);
        }
        private void _createBrandElements(Category brand)
        {
            IEnumerable<Category> cats = Container.CategoryMap;
            string var = "[" + _parts.Last() + "]";
            // each brand needs a base expression, and weak/strong/at-risk expressions and corresponding derived exp for each

            string brandExp = DimAxisHelper.ContainsAnyExpression(var, brand);
            string catsExp = null;
            DimAxisElement e = null;
            // Base element for this brand
            string brandLabel = DimAxisHelper.GetLabel(brand);
            e = new DimAxisElement(countsOnly: true, hide: true)
            {
                ID = "base_" + brand.Name,
                Label = "Base: " + brandLabel,
                Expression = brandExp
            };
            _elements["base"].Add(e);
            
            // WEAK
            catsExp = DimAxisHelper.ContainsAnyExpression("LevelID", cats.Take(3));
            e = new DimAxisElement(countsOnly:true, hide: true)
            {
                ID = "w_" + brand.Name,
                Label = "exp: " + brandLabel,
                Expression = catsExp + " AND " + brandExp
            };
            _elements[_weak].Add(e);

            e = new DimAxisElement(countsOnly: true, hide: true)
            {
                ID = "weak_" + brand.Name,
                Label = "wexp: " + brandLabel,
                Type = ExpressionType.etDerived,
                Expression = String.Format("(w_{0} / base_{0}) * 100 ",brand.Name)
            };
            _elements[_weak].Add(e);

            // STRONG
            catsExp = DimAxisHelper.ContainsAnyExpression("LevelID", cats.Skip(3).Take(3));
            e = new DimAxisElement(countsOnly: true, hide: true)
            {
                ID = "s_" + brand.Name,
                Label = "sexp: " + brandLabel,
                Expression = catsExp + " AND " + brandExp
            };
            _elements[_strong].Add(e);
            e = new DimAxisElement(countsOnly: true, hide: true)
            {
                ID = "strong_" + brand.Name,
                Label = "Strong: " + brandLabel,
                Type = ExpressionType.etDerived,
                Expression = String.Format("(s_{0} / base_{0}) * 100 ", brand.Name)
            };
            _elements[_strong].Add(e);
            // At Risk
            catsExp = DimAxisHelper.ContainsAnyExpression("LevelID", cats.Skip(6).Take(3));
            e = new DimAxisElement(countsOnly: true, hide: true)
            {
                ID = "ar_" + brand.Name,
                Label = "sexp: " + brandLabel,
                Expression = catsExp + " AND " + brandExp
            };
            _elements[_atRisk].Add(e);
            e = new DimAxisElement(countsOnly: true, hide: true)
            {
                ID = "atrisk_" + brand.Name,
                Label = "At Risk: " + brandLabel,
                Type = ExpressionType.etDerived,
                Expression = String.Format("(ar_{0} / base_{0}) * 100 ", brand.Name)
            };
            _elements[_atRisk].Add(e);
        }

    }
}
