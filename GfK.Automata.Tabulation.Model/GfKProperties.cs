﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public class GfKProperties
    {
        public const string
            GfKFunnelLevel = "gfkFunnelLevel",
            GfKQType = "GfKQType",
            GfKCatOther = "GfKCatOther";

        public GfKProperties()
        {

        }

    }

}
