﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSBAWBenchmarkTable: DimBenchmarkTable
    {

        public DimSBAWBenchmarkTable(string language,string context, string desc, Container container, DimFilter filter): base(language,context,desc, container, filter)
        {
            Field.Properties[GfKProperties.GfKQType] = "SBAW";
            Base = "All respondents";
            CreateElements();
            Side = SideAxis(AxisType.atAxis);
            
        }
        public void CreateElements()
        {
            DimSBAWSummaryTable throwaway = new DimSBAWSummaryTable(Language, Context, Description, Container, Filter);
            throwaway.Elements.RemoveBaseElements(); // get rid of the base element for now
            foreach (DimAxisElement e in throwaway.Elements.Items)
            {
                string exp = e.GetExpression();
                Elements.Add(new DimAxisElement(e.ID, e.Label,hide:true));
                Elements.Items.Last().Expression = e.GetExpression() + " is not null ";
                
            }
            throwaway = null;
            DimAxisElement ben = new DimAxisElement(decimals: 1)
            {
                ID = "benchmark",
                Label = "Unaided Awareness Benchmark",
                Type = ExpressionType.etDerived,
                Expression = "(" + Elements.Join("+") + ") / " + Elements.Items.Count.ToString()
            };
            Elements.AddBaseElement(Base);
            Elements.Add(ben);
        }
    }
}
