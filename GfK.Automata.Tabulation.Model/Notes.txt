﻿* SBAW		-- can we mock up using ABAW??
* BRIA_M	-- recode scripting not yet implemented, placeholder tables only.
* Funnel
* AE1:AE2, AE1, AE2 (Ad Evaluation:2) not implemented
* HTML from AE5_M details
* HTML from AE7_M details


* SOP, SPEND currently not banded "from data", but hard-coded bands.
  Absent any specific algorithm request (e.g., quartiles, etc), this is all I can do for now.

*** To do later, not required
PI_OCC axis expression can be given like:
PI_OCC_GRID[{Brand2}].PI_OCC{'OCC1 & OCC2 asked (Aware of brand)' base(), t1 'Top 2' expression('[PI_OCC].ContainsAny({_4,_3})'), e2 '2' expression('    [PI_OCC] = {_2}'), e1 '1' expression('[PI_OCC]= {_1}')} * banMonthly

Postponed per Amy/Erica

* BRI_M - not implemented