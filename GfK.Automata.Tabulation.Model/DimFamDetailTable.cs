﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;


namespace GfK.Automata.Tabulation.Model
{
    public class DimFamDetailTable : DimDetailTable
    {
        private Int32 _topCount = 2;
        private bool _sortDesc = true;
        private bool _hideCats = false;
        private List<string>_CatList { get; set; }
        public DimFamDetailTable(string language, string context, string desc, BoundField f, Container c = null) : base(language, context, desc, f, c, null)
        {
            _setCatLst();
            CreateElementsTop2SideAxis(_CatList, _topCount, "PRIORITY_BRAND_AWARE");
            Side = SideAxis(AxisType.atAxis);
            FormatDescription("Brand", c);
            
        }
        private void _setCatLst()
        {
            _CatList = new List<string>();
            _CatList.Add("Very familiar");        //_4
            _CatList.Add("Somewhat familiar");
            _CatList.Add("Just a little familiar");
            _CatList.Add("Not aware");          //_1
        }
    }
}
