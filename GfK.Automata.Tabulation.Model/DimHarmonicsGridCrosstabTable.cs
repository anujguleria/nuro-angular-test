﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimHarmonicsGridCrosstabTable: DimCrosstabTable
    {
        public DimHarmonicsGridCrosstabTable(string description, Category brand, BoundField Field, BoundField sideField, BoundField topField, DimFilter filter): base(description, brand, Field, sideField, topField, filter)
        {
            Base = "Total Respondents";
            Side = SideAxis();
            Top = TopAxis();
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, brand.Document, true));
        }
        public new string SideAxis()
        {
            string[] varParts = SideField.Name.Split('.');
            string varName = varParts.Last();
            // Stub of bottom 2, middle 1, top 2
            Elements.Add(new DimAxisElement()
            {
                ID = "lowPull",
                Label = "Low Pull (1,2)",
                Expression = DimAxisHelper.ContainsAnyExpression(varName, SideField.Categories().Take(2))
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "medPull",
                Label = "Medium Pull (3)",
                Expression = DimAxisHelper.ContainsAnyExpression(varName, SideField.Categories().Skip(2).Take(1))
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "hiPull",
                Label = "High Pull (4,5)",
                Expression = DimAxisHelper.ContainsAnyExpression(varName, SideField.Categories().Skip(3))
            });
            string sideAxis = SideField.Name + "{" + Elements.ToScript() + "}";
            return sideAxis;
        }
        public new string TopAxis()
        {
            // Banner of bottom 2, middle 1, top 2
            string[] varParts = TopField.Name.Split('.');
            string varName = varParts.Last();
            TopElements.Add(new DimAxisElement()
            {
                ID = "lowStick",
                Label = "Low Stickiness (1,2)",
                Expression = DimAxisHelper.ContainsAnyExpression(varName, TopField.Categories().Take(2))
            });
            TopElements.Add(new DimAxisElement()
            {
                ID = "medStick",
                Label = "Medium Stickiness (3)",
                Expression = DimAxisHelper.ContainsAnyExpression(varName, TopField.Categories().Skip(2).Take(1))
            });
            TopElements.Add(new DimAxisElement()
            {
                ID = "hiStick",
                Label = "High Stickiness (4,5)",
                Expression = DimAxisHelper.ContainsAnyExpression(varName, TopField.Categories().Skip(3))
            });
            TopElements.AddBaseElement(Base);
            string topAxis = TopField.Name + "{" + TopElements.ToScript() + "}";
            return topAxis;

        }

    }
}
