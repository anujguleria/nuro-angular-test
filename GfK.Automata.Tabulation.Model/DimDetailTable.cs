﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GfK.Automata.Model;


namespace GfK.Automata.Tabulation.Model
{
    public partial class DimDetailTable : DimAggregatedTable, IDetailTable
    {
        private Int32 _topCount = 0;
        private bool _hideCats = false;
        private bool _sortDesc = false;
        public DimDetailTable(string language, string context, string desc, BoundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            
            this.Base = "Base: All respondents";
            if (this is DimAE1AE2DetailTable) { return; }
            if (this is DimDefaultDetailTable) { return; }
            if (this is DimSBAWMeanDetailTable) { return; }
            
            if (c is null)
            {
                if (f.DataType == DataType.dtBoolean)
                {
                    Side = BooleanSideAxis();
                }
                else
                {
                    Side = CategorySideAxis(_topCount, _hideCats, _sortDesc);
                }
            }
            else
            {
                Side = CategorySideAxis(c, _topCount, _hideCats, _sortDesc);
                //.Side = NumericSideAxis(c);
            }

        }
        public DimDetailTable(string desc, Category brand, BoundField f, Container c, DimFilter filter): base(desc,brand,f,c,filter)
        {
            
        }
        public DimDetailTable(string desc, BoundField f, Container c, DimFilter filter) : base(desc, null, f, c, filter)
        {
            
        }
        public DimDetailTable(string language, string context, string desc, BoundField f, DimFilter filter): base(language, context, desc, f, null, filter)
        {
            
            if (this is DimSW2_TDetailTable) { return; }
            if (this is DimSW4_PDetailTable) { return; }
            if (this is DimSTPUL_T_MDetailTable) { return; }
            if (this is DimSTPULL_CELLDetailTable) { return; }
            if (this is DimOUTFLOW_GRIDDetailTable) { return; }
            if (this is DimInflowTable) { return; }
            if (this is DimOutflowDetailTable) { return; }
            throw new NotImplementedException();
        }
        public DimDetailTable(string language, string context, string desc, UnboundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            
            this.Base = "Base: All respondents";
            
            if (this is DimNumOwn_TDetailTable) { return; }
            if (this is DimSpendDetailTable) { return; }
            if (this is DimDefaultDetailTable) { return; }
            if (this is DimSBAWOrderDetailTable) { return; }
            if (this is DimSBAWMeanDetailTable) { return; }
            if (c is null)
            {
                Side = NumericSideAxis(f);
            }
            else
            {
                Side = NumericSideAxis(c);
            }
        }
        public DimDetailTable(string desc, Category brand, UnboundField f, DimFilter filter): base(desc, brand, f, filter)
        {
            // for use with Netflow tables.
        }
        public DimDetailTable(string desc, Category brand, BoundField f, DimFilter filter) : base(desc, brand, f, filter)
        {
            // for use with Netflow tables.
        }
        public DimDetailTable(string language, string context):base(language, context)
        {
            // for use with DimFunnelTable

        }
        public string BooleanSideAxis()
        {
            // {base(), eExpression 'True' expression('Fulcrum.RoutedSample = true'), eExpression1 'False' expression('Fulcrum.RoutedSample = false')}
            //sideAxis += "eExpression 'True' expression('{0} = true'), eExpression1 'False' expression('{0} = false')}}";
            Elements.AddBaseElement(Base);
            Elements.Add(new DimAxisElement("t")
            {
                Label = "True",
                Type = ExpressionType.etBoolean,
                Expression = Field.Name + " = true"
            });
            Elements.Add(new DimAxisElement("f")
            {
                Label = "False",
                Type = ExpressionType.etBoolean,
                Expression = Field.Name + " = false"
            });
            string sideAxis = Field.Name + "{" + Elements.ToScript() + "}";
            return sideAxis;
        }
        public string NumericSideAxis(UnboundField field)
        {
            // placeholder -- creates numeric summary for a field/grid slice
            Elements.Add(new DimAxisElement(decimals:2) {
                ID = "avg",
                Label = "Mean",
                Type = ExpressionType.etMean,
                Expression = Field.Name,
                
                });
            Elements.Add(new DimAxisElement(decimals: 2)
            {
                ID = "se",
                Label = "Standard Error",
                Type = ExpressionType.etStdError,
                Expression = Field.Name
                
            });
            Elements.Add(new DimAxisElement(decimals: 2)
            {
                ID = "sd",
                Label = "Standard Deviation",
                Type = ExpressionType.etStdDev,
                Expression = Field.Name
                
            });
            
            Elements.AddBaseElement(Base);
            string sideAxis = Field.Name + "{" + Elements.ToScript() + "}";
            return sideAxis;
        }
        public string NumericSideAxis(Container container)
        {
            // Creates MEAN score from Grid field.
            // NUMOWN_T_GRID > NUMOWN_T_GRID[{..}].NUMOWN_T{'Base: All respondents' base(), 'Mean' mean(NUMOWN_T_GRID[{..}].NUMOWN_T)}  

            string sideAxis = "";
            string gridExp = container.Items[0].Name.ToString().Replace(container.CategoryMap[0].Name.ToString(), "..");
            sideAxis = container.Name + " > " + gridExp + "{'" + this.Base + "' base(), 'Mean' mean(" + gridExp + ") [Decimals=2]}  ";
            return sideAxis;
        }
        public void CreateElementsTop2SideAxis(List<string>catLabels,Int32 topCount)
        {
            CreateElementsTop2SideAxis(catLabels, topCount, string.Empty);
        }
        public void CreateElementsTop2SideAxis(List<string> catLabels, Int32 topCount, string includeNotAwareVarName)
        {
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(Field.Items, true);
            List<string> catNames = DimAxisHelper.GetCategoryNames(allCats.ToList());

            Elements = new DimAxisElements();
            Elements.AddBaseElement(Base);

            DimAxisElement e = new DimAxisElement("N1", "Net: " + String.Join("/", catLabels.Take(topCount)));
            e.Expression = DimAxisHelper.ContainsAnyExpression((BoundField)Field, allCats.Take(topCount));
            Elements.Add(e);

            foreach (Category c in allCats)
            {
                string lab = catLabels[allCats.ToList().IndexOf(c)]; // c.Label(Context, Language, Container.Document)
                e = new DimAxisElement(c.Name, lab);
                string exp = Field.Name + " = { " + c.Name + "}";
                if (allCats.Last().Equals(c) && (includeNotAwareVarName != String.Empty) )
                {
                    exp += " OR " + DimAxisHelper.NotAwareExpression(Field, Container, includeNotAwareVarName);
                }
                e.Expression = exp;
                Elements.Add(e);
            }
        }

        public void AddNotAwareElement(string varName)
        {
            Field helperField = (Field)Field.Document.Find(varName);
            Category brand = Container.CategoryMap[Container.IndexOf(Field)];
            Elements.Add(new DimAxisElement()
            {
                ID = "notAware",
                Label = "Not aware",
                Expression = "NOT " + DimAxisHelper.ContainsAnyExpression(helperField.Name, brand)
            });
        }
        //public string SideAxis(AxisType axisType)
        //{
        //    switch (axisType)
        //    {
        //        case AxisType.atAxis:
        //            return "axis({" + Elements.ToScript() + "})";
        //        case AxisType.atField:
        //            return Field.Name + "{" + Elements.ToScript() + "}";
        //    }
        //    return null;
        //}

        public string CategorySideAxis(IScriptableItem tbl, bool hideCats, bool sortDescending)
        {
            Elements = new DimAxisElements();
            // for BRIA_M, OCC1_BR detail
            if (Container == null)
            {   // instead of throwing NotImplementedException, we can overload:
                return CategorySideAxis(_topCount, _hideCats, _sortDesc);
            }
            string[] varParts = DimAxisHelper.GetVarParts(Field);
            string sideAxis = Field.Name + "{";
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(Field.Items, sortDescending);
            List<string> catNames = DimAxisHelper.GetCategoryNames(allCats);
            foreach(Category c in allCats)
            {
                if (InvalidBrandNames.Contains(c)) { continue; }
                if (c.IsOtherCategory()) { continue; }
                Elements.Add(new DimAxisElement()
                {
                    ID = "e" + Elements.Items.Count.ToString(),
                    Label = DimAxisHelper.CleanText(c.Label(Context, Language, Field.Document)),
                    Expression = DimAxisHelper.ContainsAnyExpression(varParts.Last(), c)
                });
            }
            Elements.AddBaseElement(Base);
            sideAxis += Elements.ToScript() + "}";
            return sideAxis;
        }

        public string CategorySideAxis(Int32 topCount = 0, bool hideCats = false, bool sortDescending=false)
        {
            string sideAxis = "";
            string[] varParts = DimAxisHelper.GetVarParts(Field);
            string var = "";
            if(Container == null)
            { var = base.Field.Name; }
            else
            { var = Container.Name; }
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(Field.Items, sortDescending);
            List<string> catNames = (from Category c in Field.Items select c.Name).ToList();
            if (topCount > 0)
            {
                // displays descending categories UNDER the NET category
                allCats = allCats.OrderByDescending(s => s.Name.Replace("_", ""));
                string netExp = DimAxisHelper.TopBoxNetExp(this, allCats, topCount, hideCats);
                sideAxis = var + "{";
                sideAxis = sideAxis + netExp;
                //
                foreach (Category cat in allCats.Skip(topCount))
                {
                    _createElement(cat);
                }
                Elements.AddBaseElement(Base);
                sideAxis += Elements.ToScript() + "," + DimAxisHelper.AddTotal() + "}";
            }
            else
            {
                // Displays categories in the programmed/survey order 
                //sideAxis = "{0}{{'" + this.Base + "' base(), {1}, {2}}}";
                Elements = new DimAxisElements();
                foreach(Category cat in ((BoundField)base.Field).Categories())
                {
                    _createElement(cat);
                }
                
                sideAxis = var + "{";
                Elements.AddBaseElement(Base);
                sideAxis += Elements.ToScript();
                sideAxis += "}";

            }
            return sideAxis;
        }
        private void _createElement(Category cat)
        {
            DimAxisElement e = null;
            string[] varParts = DimAxisHelper.GetVarParts(Field);
            if (Container == null)
            {
                e = new DimAxisElement(hide:_hideCats)
                {
                    ID = cat.Name,
                    Label = DimAxisHelper.CleanText(cat.Label(Context, Language, base.Field.Document)),
                    Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, cat),
                };
                Elements.Add(e);
            }
        }
        
        public string CategorySideAxis(Container container, Int32 topCount = 0, bool hideCats = false, bool sortDescending = false)
        {
            // For Grid Summary Tables...
            // this seems like a hack but without a good way to identify 
            //      the "FAM_GRID[{..}].FAM" (specifically the ".FAM")
            string sideAxis = container.Name + " > " + container.Items[0].Name.ToString().Replace(container.Items[0].Items[0].Name.ToString(), "..");
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(container.Items[0].Items, sortDescending); //(from GfK.Automata.Model.Category c in f.Items select c).ToList();


            if (topCount != 0)
            {
                //allCats = allCats.OrderByDescending(s => s.Name.Replace("_", ""));
                string netExp = DimAxisHelper.TopBoxNetExp(this, allCats, topCount, hideCats);
                sideAxis += "{'" + this.Base + "' base()," + netExp;
                IEnumerable<Category> otherCats = allCats.Skip(allCats.Count() - topCount);

                sideAxis += DimAxisHelper.ScriptCategories(otherCats, topCount, hideCats);


            }
            else
            {
                sideAxis += "{'" + this.Base + "' base()";
                sideAxis += DimAxisHelper.ScriptCategories(allCats, topCount, hideCats);
                foreach (Category cat in allCats)
                {
                    sideAxis += ", " + cat.Name + " [IsHidden=" + hideCats.ToString() + "]";
                }
            }
            sideAxis += "," + DimAxisHelper.AddTotal() + "}";
            return sideAxis;
        }

    }
}
