﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimRBX2_M_FT_NDTQuadrantTable: DimQuadrantTable
    {

        public DimRBX2_M_FT_NDTQuadrantTable(string desc, Category brand, Int32 x, Int32 y, Container c, DimFilter filter): base(desc, brand, x,y, c, filter)
        {
            Base = "Base"; // "Respondents who rated brand";
            CreateElements(brand);
            Side = SideAxis();
            FormatDescription("[BRAND]", DimAxisHelper.CleanText(brand.Label(ProcessorConfiguration.Context, ProcessorConfiguration.Language, Container.Document)));
            Annotations.Add(new DimAnnotation(AnnotationType.atFooter, "RBX Score = [% Quadrant 2 (Memorable & Positive) - % Quadrant 1 (Memorable & Negative)]"));
        }

        public void CreateElements(Category brand)
        {

            Elements = new DimAxisElements();
            string xName = DimAxisHelper.GetQuadrantField(Container, brand, "X").Name;
            string yName = DimAxisHelper.GetQuadrantField(Container, brand, "Y").Name;

            Elements.Add(new DimAxisElement(hide:false)
            {
                ID = "b_" + brand.Name,
                Label = "Base: " + DimAxisHelper.GetLabel(Container,brand),
                Type = ExpressionType.etBase,
                Expression = DimAxisHelper.ContainsAnyExpression(DimRBXController.Iterator, brand)
            });

            Elements.Add(new DimAxisElement()
            {
                ID = "q1"+brand.Name,
                Label = "Quadrant 1 (Memorable & Negative)",
                Expression = String.Format("{0} <= {1} And {2} > {3}", xName, X, yName, Y)
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "q2" + brand.Name,
                Label = "Quadrant 2 (Memorable & Positive)",
                Expression = String.Format("{0} > {1} And {2} > {3}", xName, X, yName, Y)
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "q3" + brand.Name,
                Label = "Quadrant 3 (Not Memorable & Positive)",
                Expression = String.Format("{0} > {1} And {2} <= {2}", xName, X, yName, Y)
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "q4" + brand.Name,
                Label = "Quadrant 4 (Not Memorable & Negative)",
                Expression = String.Format("{0} <= {1} And {2} <= {3}", xName, X, yName, Y)
            });
            Elements.Add(new DimAxisElement(decimals:1)
            {
                ID = "rbx" + brand.Name,
                Label = "RBX Score - " + DimAxisHelper.GetLabel(Container,brand),
                Type =ExpressionType.etDerived,
                Expression = String.Format(" q2{0} - q1{0} ", brand.Name)
            });
            
        }
    }
}
