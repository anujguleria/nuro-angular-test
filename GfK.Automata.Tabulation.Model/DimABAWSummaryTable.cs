﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimABAWSummaryTable : DimSummaryTable
    {
        public DimABAWSummaryTable(string language, string context, string desc, Field f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            // summary table for "Yes, aware of"
            Side = SummarySideAxis(c, 1, true, false, "Yes, aware of brand");
            AppendToFunnel(FunnelRows, "Awareness");
            
        }
    }
}
