﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimFunnelTables
    {
        // DimFunnelTables presents a Dictionary keyed by category Name 
        //
        public static Dictionary<string, DimFunnelTable> Items { get; set; }
        public static Int32 Count { get { return Items.Count; } }
        public static string Language { get; set; } 
        public static string Context { get; set; } 
        static DimFunnelTables()
        {
            Language = ProcessorConfiguration.Language;
            Context = ProcessorConfiguration.Context;
            Items = new Dictionary<string, DimFunnelTable>();
        }
        public static void AddRow(Category cat, DimAxisElement e, string label, string funnelLevel)
        {
            // this method adds the DimAxisElement e, to the Items[cat.Name]
            // if Items[cat.Name] doesn't exist, we need to create it
            // To Do: we can possibly initialize empty DimFunnelTable earlier in process 
            // based on Priority_Brand_List DefinedList (or otherwise)
            e.Label = label;
            Int32 level = Convert.ToInt32(funnelLevel);
            if (Items.ContainsKey(cat.Name))
            {
                Items[cat.Name].AddRow(e, level);
                return;
            }
            else
            {
                Items.Add(cat.Name, new DimFunnelTable(Language, Context, cat));
                Items[cat.Name].AddRow(e, level);

            }

        }
        public static void CreateFunnelRows(DimDetailTable tbl, BoundField f)
        {

            foreach (Category c in f.Categories())
            {
                DimAxisElement e = new DimAxisElement()                
                {
                    ID = c.Name,
                    Label = c.Label(tbl.Context, tbl.Language, f.Document, true),
                    Expression = DimAxisHelper.ContainsAnyExpression(f.Name, c)
                };

                tbl.FunnelRows.Add(c, e);
            }
        }
    }
}
