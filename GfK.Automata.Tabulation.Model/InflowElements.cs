﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class InflowElements
    {
        public static BoundField SW1 { get; set; }
        public static BoundField SW2 { get; set; }
        
        static InflowElements()
        {



        }
        private static Dictionary<Int32, DimAxisElements> _elements = new Dictionary<int, DimAxisElements>();

        public static DimAxisElements GetElements(Int32 level)
        {
            
            return _elements[level];

        }

        public static void Init(BoundField sw1, BoundField sw2)
        {
            SW1 = sw1;
            SW2 = sw2;
            _elements.Add(1, new DimAxisElements());
            _elements.Add(2, new DimAxisElements());
            _elements.Add(3, new DimAxisElements());

            _elements[1].Add(new DimAxisElement()
            {
                ID = "_in1",
                Label = "Recently switched to client",
                Expression = SW1.Name + "={_1} AND NOT " + SW2.Name + "={ DidNot }",
            });
            _elements[1].Add(new DimAxisElement()
            {
                ID = "_out1",
                Label = "Existing customer that did not switch",
                Expression = "(" + SW1.Name + "={ _1 } AND " + SW2.Name + "={ DidNot }) OR " + SW1.Name + ".ContainsAny({ _2,_3,_4})",
            });
            _elements[2].Add(new DimAxisElement()
            {
                ID = "_in2",
                Label = "Recently switched to client",
                Expression = SW1.Name + ".ContainsAny({_1,_2}) AND NOT " + SW2.Name + "={ DidNot }",
            });
            _elements[2].Add(new DimAxisElement()
            {
                ID = "_out2",
                Label = "Existing customer that did not switch",
                Expression = "(" + SW1.Name + ".ContainsAny({_1,_2}) AND " + SW2.Name + "={ DidNot }) OR " + SW1.Name + ".ContainsAny({_3,_4})",
            });
            _elements[3].Add(new DimAxisElement()
            {
                ID = "_in3",
                Label = "Recently switched to client",
                Expression = SW1.Name + ".ContainsAny({_1,_2,_3}) AND NOT " + SW2.Name + "={ DidNot }",
            });
            _elements[3].Add(new DimAxisElement()
            {
                ID = "_out3",
                Label = "Existing customer that did not switch",
                Expression = "(" + SW1.Name + ".ContainsAny({_1,_2,_3}) AND " + SW2.Name + "={ DidNot }) OR " + SW1.Name + ".ContainsAny({ _4})",
            });
        }


    }
}
