﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimRBXController
    {
        private static List<IScriptableItem> _Tables { get; set; } = null;
        private static string _Lang { get; set; } = ProcessorConfiguration.Language;
        private static string _Context { get; set; } = ProcessorConfiguration.Context;
        private static MetadataDocument _Doc { get; set; } = null;
        public static bool CreatedRBXTables { get; set; } = false;
        public static Container Container { get; set; }
        public static BoundField Iterator { get; set; }
        public static Container TopContainer { get; set; }
        
        static DimRBXController() { }

        public static void Process(MetadataObject obj)
        {

        }
        public static List<IScriptableItem> CreateRBX_Tables(Container container)
        {
            _Tables = new List<IScriptableItem>();
            Container = container;
            Iterator = (BoundField)Container.Document.Find("RBX_BRANDS");
            TopContainer = (Container)Container.Document.Find("RBX2_M_FT");

            SummaryBrandPerformanceTables();

            BrandPerformanceTables(50, 50);

            BrandPerformanceTables(60, 60);

            BrandPerformanceTables(70, 70);
            
            CreatedRBXTables = true;
            return _Tables;
        }
        
        public static void BrandPerformanceTables(Int32 x, Int32 y)
        {
            // creates individual brand detail tables and the summary table
            DimAxisElements summaryElements = new DimAxisElements();
            List<IScriptableItem> _tables = new List<IScriptableItem>();
            string desc = "Brand Performance - Brand Quadrant Performance (x={0}/y={1}) - [BRAND]";
            IScriptableItem t = null;
            foreach (Category brand in Container.CategoryMap) 
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                t = new DimRBX2_M_FT_NDTQuadrantTable(desc, brand, x, y, Container, null);
                summaryElements.Add(((DimQuadrantTable)t).Elements);
                _tables.Add(t);
            }
            t = new DimRBX2_M_FT_NDTSummaryTable(desc, summaryElements, x, y, Container, null);
            _tables.Add(t);

            t = new DimRBX2_M_FT_NDTBenchmarkTable("Brand Performance - RBX Score (x={0}/y={1})", summaryElements, x, y, Container, null);
            _tables.Add(t);
            _Tables.AddRange(_tables);
        }
        public static void SummaryBrandPerformanceTables()
        {

            IScriptableItem table = new DimRBX2_M_FT_NDTTable("Brand Performance - Average Impression (x) and Memorability (y)", Container, null);
            _Tables.Add(table);
            table = null;
        }


        public static DimAxisElement BrandBaseElement(Category brand)
        {
            DimAxisElements elements = new DimAxisElements();
            elements.AddBaseElement("");
            DimAxisElement b = elements.Items[0];
            b.ID = "b_" + brand.Name;
            b.Label = "Base: " + DimAxisHelper.GetLabel(Container, brand);
            b.Type = ExpressionType.etBase;
            b.Expression = "^.^." + DimAxisHelper.ContainsAnyExpression(DimRBXController.Iterator, brand);
            return b;
        }
        public static DimAxisElement XMeanElement(Category brand, UnboundField f)
        {
            DimAxisElement e = new DimAxisElement()
            {
                ID = "_mx" + brand.Name,
                Label = DimAxisHelper.GetLabel(Container, brand) + " Mean Impression (x)",
                Type = ExpressionType.etMean,
                Expression = f.Name
            };
            return e;
        }
        public static DimAxisElement YMeanElement(Category brand, UnboundField f)
        {
            DimAxisElement e = new DimAxisElement()
            {
                ID = "_my" + brand.Name,
                Label = DimAxisHelper.GetLabel(Container, brand) + " Mean Memorability (y)",
                Type = ExpressionType.etMean,
                Expression = f.Name
            };
            return e;
        }

    }
}
