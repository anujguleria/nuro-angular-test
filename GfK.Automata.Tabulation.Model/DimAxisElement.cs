﻿using System.Collections.Generic;
using GfK.Automata.Model;
using System.Linq;
using System;

namespace GfK.Automata.Tabulation.Model
{

    public enum ExpressionType
    {
        etCategory,
        etNone,
        etBase,
        etExpression,
        etNet,
        etCombine,
        etMean,
        etDerived,
        etStdError,
        etStdDev,
        etBoolean,
        etTotal

    }
    public class DimAxisElement
    {
        public const string
            pIsHidden = "IsHidden",
            pCountsOnly = "CountsOnly",
            pDecimals = "Decimals",
            pMultiplier = "Multiplier";
        public string ID { get; set; }
        public string Label { get; set; }
        private string _expression { get; set; }
        public string Expression
        {
            get { return _expression; }
            set { _expression = _getExpression(value); }
        }
        public ExpressionType Type { get; set; }
        public Dictionary<string, dynamic> Properties = new Dictionary<string, dynamic>();

        public DimAxisElement(string id = "", string lab = "", string exp = "", bool hide = false, bool countsOnly = false, Int32 decimals = -1)
        {
            // axis element can be instantiated with any (or none) of the properties in constructor
            Type = ExpressionType.etExpression;
            ID = id;
            Label = lab;
            Expression = exp;
            Properties.Add(pIsHidden, hide);
            Properties.Add(pCountsOnly, countsOnly);
            if (decimals >=0 )
            {
                Properties.Add(pDecimals, decimals);
            }
        }

        public string ToScript()
        {
            string exp = ID + "'" + Label + "' " + Expression + " " + _propertyScript();
            return exp;
        }
        public string GetExpression()
        {
            string e = Expression;
            Int32 start = 0;
            Int32 end = 0;
            if (e=="") { return e; }
            switch (Type)
            {
                case ExpressionType.etCategory:
                    return null;
                case ExpressionType.etTotal:
                    return "total()";
                case ExpressionType.etBase:
                case ExpressionType.etBoolean:
                case ExpressionType.etNone:
                case ExpressionType.etExpression:
                case ExpressionType.etDerived:
                    // "derived('" + e + "')";
                    // if no type is assigned, but an expression is passed, then assume it's an etExpression
                    start = e.IndexOf("'");
                    end = e.LastIndexOf("'");
                    break;
                case ExpressionType.etNet:
                case ExpressionType.etCombine:
                    // "net({" + e + "})";
                    //"combine({" + e + "})";
                    start = e.IndexOf("{");
                    end = e.LastIndexOf("}");
                    break;
                case ExpressionType.etMean:
                case ExpressionType.etStdDev:
                case ExpressionType.etStdError:
                    start = e.IndexOf("(");
                    end = e.LastIndexOf(")");
                    break;

            }
            start += 1;
            end = (end - start);
            return e.Substring(start,end);
        }
        private string _getExpression(string e)
        {
            string exp = "";

            switch (Type)
            {
                case ExpressionType.etCategory:
                    return null;
                case ExpressionType.etBase:
                    if(e == "") return "base()";
                    return "base('" + e + "') ";
                case ExpressionType.etTotal:
                    return "total()";
                case ExpressionType.etBoolean:
                case ExpressionType.etNone:
                case ExpressionType.etExpression:
                    // if no type is assigned, but an expression is passed, then assume it's an etExpression
                    if (e == "") { return exp; }
                    return "expression('" + e + "')";
                case ExpressionType.etNet:
                    return "net({" + e + "})";
                case ExpressionType.etCombine:
                    return "combine({" + e + "})";
                case ExpressionType.etMean:
                    return "mean(" + e + ")";
                case ExpressionType.etDerived:
                    return "derived('" + e + "')";
                case ExpressionType.etStdDev:
                    return "stdDev(" + e + ")";
                case ExpressionType.etStdError:
                    return "stdErr(" + e + ")";
                
                    
            }


            return exp;
        }
        private string _propertyScript()
        {
            string propscript = "";
            if (Properties.Count != 0)
            {
                List<string> prop = new List<string>();
                foreach (KeyValuePair<string, dynamic> itm in Properties)
                {
                    if (itm.Value is bool )
                    {
                        switch (itm.Key)
                        {
                            case pIsHidden:
                                if (Type == ExpressionType.etBase && itm.Value == false)
                                { prop.Add(itm.Key + "=" + itm.Value.ToString()); }
                                else if (itm.Value == false)
                                {
                                    // ignore
                                }
                                else
                                {   // True, not a Base element, needs explicitly hidden
                                    prop.Add(itm.Key + "=" + itm.Value.ToString()); } 
                                break;
                            case pCountsOnly:
                                if (itm.Value == true)
                                { prop.Add(itm.Key + "=" + itm.Value.ToString()); } 
                                break;
                            default:
                                if (itm.Value == true)
                                prop.Add(itm.Key + "=" + itm.Value.ToString());
                                // ignore False booleans
                                break;
                        }

                    }
                    if (itm.Value is Int32)
                    {
                        prop.Add(itm.Key + "=" + itm.Value.ToString());
                    }
                    if (itm.Value is string)
                    {
                        prop.Add(itm.Key + "=" + itm.Value);
                    }

                }

                if (prop.Count > 0)
                {
                    propscript += "[" + String.Join(",", prop) + "]";
                }
            }

            return propscript;
        }
        public void AddProperty(string propName, string propVal)
        {
            Properties.Add(propName, propVal);
        }
    }
}