﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;


namespace GfK.Automata.Tabulation.Model
{
    public class DimFavDetailTable : DimDetailTable
    {
        private Int32 _topCount = 2;
        private bool _sortDesc = true;
        private bool _hideCats = false;
        private List<string> _CatList { get; set;}
        public DimFavDetailTable(string language, string context, string desc, BoundField f, Container c = null) : base(language,context, desc, f, c, null)
        {
            _setCatLst();
            Base = "Aware of brand";
            CreateElementsTop2SideAxis(_CatList, _topCount);
            AddNotAwareElement("PRIORITY_BRAND_AWARE");
            Side = SideAxis(AxisType.atAxis);
            FormatDescription("Brand", c);
            //Side += " '" + DimAxisHelper.CleanText(Description, "") + "'";
        }
        private void _setCatLst()
        {
            _CatList = new List<string>();
            _CatList.Add("Very favorable");         //_4
            _CatList.Add("Somewhat favorable");
            _CatList.Add("Somewhat unfavorable");
            _CatList.Add("Very unfavorable");       //_1
        }

    }
}
