﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public enum ScriptType
    {
        stDimensions
    }

    public interface IScriptableItem
    {
        string GetScript( ScriptType type );
    }
}
