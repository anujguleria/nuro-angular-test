﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSpendDetailTable: DimDetailTable
    {
        private List<string> _CatList { get; set; }
        private DimAxisElements _elements = new DimAxisElements();
        private Dictionary<string, List<double>> _bands { get; set; }
        public DimSpendDetailTable(string language, string context, string description, UnboundField Field, Container Container, DimFilter filter, Dictionary<string, List<double>> bands) : base(language, context, description, Field, Container, filter)
        {
            _bands = bands;
            CreateElements();
            Side = SideAxis(AxisType.atField);
        }
        public DimSpendDetailTable(string language, string context, string description, UnboundField Field, Container Container, DimFilter filter, Dictionary<string, List<double>> bands, bool includeDist) : base(language, context, description, Field, Container, filter)
        {
            _bands = bands;
            CreateElements();
            CreateDistElements();
            Side = SideAxis(AxisType.atField);
        }
        public void CreateDistElements()
        {
            // adds an element for every distinct value from Data
            //List<Int32> values = DimQueryData.GetDistinctValues((UnboundField)Field, "");
            string[] varParts = DimAxisHelper.GetVarParts(Field);
            for (Int32 i = Field.MinValue; i <= Field.MaxValue; i++)
            {
                string exp = String.Format("[{0}] = {1}", varParts[1], i.ToString());
                Elements.Add(new DimAxisElement()
                {
                    ID = "_" + i.ToString(),
                    Label = i.ToString(),
                    Expression = exp
                });
            }

        }
        public void CreateElements()
        {
            Elements = new DimAxisElements();
            _elements.AddBaseElement(Base);
            Int32 i = 0;
            string[] varParts = DimAxisHelper.GetVarParts(Field);
            foreach(KeyValuePair<string,List<double>> pair in _bands)
            {
                
                DimAxisElement e = new DimAxisElement("e" + (i++).ToString(), pair.Key);
                string exp = "";
                string filterExp = "";
                string var = varParts[1];
                 
                Int32 min = (Int32)_bands[pair.Key][0];
                Int32 max = (Int32)_bands[pair.Key][1];
                if (pair.Key != _bands.Keys.ToList().Last())
                {
                    exp = "[{0}] >= {1} AND [{0}] <= {2}";
                    exp = String.Format(exp, varParts[1], min, max);
                    e.Label = String.Format("${0} - ${1}", min.ToString(), max.ToString());
                    filterExp = "SUM(" + Container.Name + ".(" + var + " > " + min.ToString() + "))";
                    filterExp += " AND ";
                    filterExp += "SUM(" + Container.Name + ".(" + var + " <=" + max.ToString() + "))";
                }
                else
                {   
                    exp = "[{0}] >= {1}";
                    exp = String.Format(exp, varParts[1], min);
                    e.Label = " > " + min.ToString();
                    filterExp = "SUM(" + Container.Name + ".(" + var + " > " + min.ToString() + "))";
                }
                GlobalFilter fltr = (new GlobalFilter("PriceSegment_" + (i).ToString(), ProjectType.TECH)
                {
                    Description = e.Label,
                    Expression = filterExp
                });
                ProcessorConfiguration.GlobalFilters.Add(fltr);
                e.Expression = exp;
                _elements.Add(e);

            }
            Elements = _elements;
        }



    }
}
