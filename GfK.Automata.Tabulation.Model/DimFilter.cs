﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public class DimFilter
    {   
        // class represents a filter that can be applied to individual table
        // this is to be differentiated from GlobalFilter class which is an object appending to MRS output metadata
        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public string Expression { get; protected set; }
        public string Level { get; protected set; }
        public DimFilter(string name, string desc, string expression)
        {
            Name = name;
            Description = desc;
            Expression = expression;
        }
        public DimFilter(string name, string desc, string expression, string level)
        {
            Name = name;
            Description = desc;
            Expression = expression;
            Level = level;
        }
    }
}
