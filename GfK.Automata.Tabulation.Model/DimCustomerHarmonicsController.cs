﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimCustomerHarmonicsController
    {
        static List<IScriptableItem> _Tables { get; set; } = new List<IScriptableItem>();
        
        static string _Lang { get; set; }
        static string _Context { get; set; }
        static MetadataDocument _Doc { get; set; }
        public static BoundField Iterator { get; set; }
        public static List<Category> Brands { get; set; }
        public static BoundField SW4 { get; set; }
        public static BoundField SW1_T { get; set; }
        public static BoundField SW2_T { get; set; }
        public static BoundField Outflow { get; set; }
        public static BoundField Inflow1 { get; set; }
        public static BoundField Inflow2 { get; set; }
        public static BoundField Inflow3 { get; set; }
        public static UnboundField Netflow1 { get; set; }
        public static UnboundField Netflow2 { get; set; }
        public static UnboundField Netflow3 { get; set; }
        public static BoundField Netflow1_in { get; set; }
        public static BoundField Netflow2_in { get; set; }
        public static BoundField Netflow3_in { get; set; }
        private static bool _harmonicsDMSScriptVariablesExist { get
            {
                if (Outflow is null || Inflow1 is null ||
                    Inflow2 is null || Inflow3 is null ||
                    Netflow1 is null || Netflow1_in is null ||
                    Netflow2 is null || Netflow2_in is null ||
                    Netflow3 is null || Netflow3_in is null)
                {
                    return false;
                }
                return true;
            }
        }
        private static bool _iteratorExists { get
            {
                return (!(Iterator is null));
            }
        }
        private static bool _processInflow { get; set; }

        static DimCustomerHarmonicsController()
        {
            _Lang = ProcessorConfiguration.Language;
            _Context = ProcessorConfiguration.Context;
        }
        public static void Init(MetadataDocument D)
        {
            if (D is null)
            {
                _processInflow = false;
                return;
            }
            _Doc = D;
            Iterator = (BoundField)_Doc.Find("PURLAST_T");
            if(!(Iterator is null)) 
            { Brands = Iterator.Categories(); }
            SW4 = (BoundField)_Doc.Find("SW4");
            SW1_T = (BoundField)_Doc.Find("SW1_T");
            SW2_T = (BoundField)_Doc.Find("SW2_T");
            if( !(SW1_T is null) && !(SW2_T is null))
            {
                _processInflow = true;
                InflowElements.Init(SW1_T, SW2_T);
            }
            // these fields created by Harmonics DMS script
            Outflow = (BoundField)_Doc.Document.Find("outflow");
            Inflow1 = (BoundField)_Doc.Document.Find("inflow1");
            Inflow2 = (BoundField)_Doc.Document.Find("inflow2");
            Inflow3 = (BoundField)_Doc.Document.Find("inflow3");
            Netflow1 = (UnboundField)_Doc.Document.Find("netflow1");
            Netflow1_in = (BoundField)_Doc.Document.Find("netflow1_in");
            Netflow2 = (UnboundField)_Doc.Document.Find("netflow2");
            Netflow2_in = (BoundField)_Doc.Document.Find("netflow2_in");
            Netflow3 = (UnboundField)_Doc.Document.Find("netflow3");
            Netflow3_in = (BoundField)_Doc.Document.Find("netflow3_in");
            // if the script hasn't been run, then we can't produce these tables
            if (!_harmonicsDMSScriptVariablesExist) { return; }
            
            Outflow.Properties.Add(GfKProperties.GfKQType, "OUTFLOW");
            Inflow1.Properties.Add(GfKProperties.GfKQType, "INFLOW1");
            Inflow2.Properties.Add(GfKProperties.GfKQType, "INFLOW2");
            Inflow3.Properties.Add(GfKProperties.GfKQType, "INFLOW3");
            Netflow1.Properties.Add(GfKProperties.GfKQType, "NETFLOW1");
            Netflow2.Properties.Add(GfKProperties.GfKQType, "NETFLOW2");
            Netflow3.Properties.Add(GfKProperties.GfKQType, "NETFLOW3");
            
        }
        private static bool _initTables()
        {
            if (_iteratorExists)
            {
                _Tables = new List<IScriptableItem>();
                return true;
            }
            return false;
            
        }
        public static List<IScriptableItem> SW1_TTables(BoundField f)
        {
            DimDetailTable _table = null;
            // standard tables
            if (!(_initTables())) { return null; };
            foreach(Category brand in Brands)
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                _table = new DimDetailTable(_Lang, _Context, "Tenure with [BRAND FROM PURLAST_T]", f, null, null);
                _table.FormatDescription("[BRAND FROM PURLAST_T]", brand);
                _Tables.Add(_table);
                _table = null;
            }

                
            return _Tables;
        }
        public static List<IScriptableItem> SW2_TTables(BoundField f)
        {
            /*
             * "Standard table. 
                Net: ""Switched from another [CATEGORY]"" [All numbered brands in list. Include ""other"" (97) but exclude ""I did not switch"" (99)]"
                
            */
            IScriptableItem _table = null;
            if (!(_initTables())) { return null; };
            foreach (Category brand in Brands)
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                _table = new DimSW2_TDetailTable(_Lang, _Context, "[CATEGORY] Switched from Before a Customer of [BRAND FROM PURLAST_T]", brand, f, null);
                _Tables.Add(_table);
                _table = null;
            }

            if (!(SW1_T is null))
            {
                BoundField obj = DummyField("INFLOW_GRID", "INFLOW_GRID");
                foreach (Category brand in Brands)
                {
                    if (InvalidBrandNames.Contains(brand)) { continue; }
                    _table = new DimINFLOW_GRIDCrosstabTable("Crosstab of Tenure and Brand Switched From", brand, obj, SW1_T, f, null);
                    _Tables.Add(_table);
                    _table = null;
                }
            }

            return _Tables;
        }
        public static List<IScriptableItem> SW3_MTables(Container c)
        {
            //1. Summary table of "First choice" (1).
            //2. Summary table of "First or second choice" (1,2).
            IScriptableItem _table = null;
            if (!(_initTables())) { return null; };
            foreach (Category brand in Brands)
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                DimFilter filter = _getBrandFilter(brand);

                _table = new DimSW3_MSummaryTable(_Lang, _Context, "Switching Consideration/Preference – First Choice", 1, brand, c, filter);
                _Tables.Add(_table);
                _table = null;
                _table = new DimSW3_MSummaryTable(_Lang, _Context, "Switching Consideration/Preference – First or Second Choice", 2, brand, c, filter);
                _Tables.Add(_table);
                _table = null;
            }
            return _Tables;
        }
        public static List<IScriptableItem> SW4_PTables(BoundField f)
        {
            //Standard table. Include "Not asked SW4_P" in base.
            IScriptableItem _table = null;
            if (!(_initTables())) { return null; };
            foreach(Category brand in Brands)
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                _table = new DimSW4_PDetailTable(_Lang, _Context, "Likelihood of Switching", brand, f, null);
                _Tables.Add(_table);
                _table = null;
            }
            return _Tables;
        }
        public static List<IScriptableItem> STPUL_T_MTables(Container c)
        {

            IScriptableItem _table = null;
            if (!(_initTables())) { return null; };
            if (!_harmonicsDMSScriptVariablesExist) { return null; }
            foreach (Category brand in Brands)
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                DimFilter filter = _getBrandFilter(brand);

                /*
                    * STPUL_T_M:   "Standard table for both items
                                Nets: Top 2 box (4,5) Bottom 2 box (1,2)"
                */
                BoundField f = (BoundField)c.Items[0];
                _table = new DimSTPUL_T_MDetailTable(_Lang, _Context, "Stickiness", brand, f, c, filter);
                _Tables.Add(_table);
                _table = null;

                /*   STIC_PULL:   Crosstab of STIC1 and PULL1_T including nets.
                    Banner: Low Stickiness (1,2); Medium Stickiness (3); High Stickiness (4,5)
                    Stubs: Low Pull (1,2); Medium Pull (3); High Pull (4,5)
                */
                BoundField obj = DummyField("HARMONICSGRID", "HARMONICSGRID");
                BoundField sideField = (BoundField)c.Items[0];
                BoundField topField = (BoundField)c.Items[1];
                _table = new DimHarmonicsGridCrosstabTable("Crosstab of Stickiness and Pull Questions", brand, obj, sideField, topField, filter);
                _Tables.Add(_table);
                _table = null;
                
            }
            return _Tables;
        }

        public static List<IScriptableItem> STPULL_CELLTables(BoundField field)
        {
            IScriptableItem _table = null;
            if(!(_initTables())) { return null; }
            if (!_harmonicsDMSScriptVariablesExist) { return null; }
            foreach (Category brand in Brands)
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                DimFilter filter = _getBrandFilter(brand);
                _table = new DimSTPULL_CELLDetailTable(_Lang, _Context, "Relationship Durability", brand, field, filter);
                
                _Tables.Add(_table);
                _table = null;
            }

            return _Tables;
        }
        public static List<IScriptableItem> DURSEG_Tables(BoundField field)
        {
            IScriptableItem _table = null;
            if (!(_initTables())) { return null; }
            if (!_harmonicsDMSScriptVariablesExist) { return null; }

            foreach (Category brand in Brands)
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                DimFilter filter = _getBrandFilter(brand);
                
                // Table # OUTFLOW_GRID

                if (!(SW4 is null))
                {
                    
                    _table = new DimOUTFLOW_GRIDDetailTable(_Lang, _Context, "Crosstab of Durability Segments and Likelihood to Switch", brand, field, filter);
                    _Tables.Add(_table);
                    _table = null;

                    //BoundField obj = DummyField("OUTFLOW", "OUTFLOW");
                    _table = new DimOutflowDetailTable(_Lang, _Context, "OUTFLOW", brand, Outflow, field, filter);
                }
                if( !(SW4 is null) && (_processInflow == true))
                {

                    //BoundField obj = DummyField("INFLOW", "INFLOW");
                    // we can create inflow/outflow tables at this point
                    _table = new DimInflowTable(_Lang, _Context, "INFLOW1", brand, Inflow1, filter);
                    _Tables.Add(_table);
                    _table = null;

                    _table = new DimInflowTable(_Lang, _Context, "INFLOW2", brand, Inflow2, filter);
                    _Tables.Add(_table);
                    _table = null;

                    _table = new DimInflowTable(_Lang, _Context, "INFLOW3", brand, Inflow3, filter);
                    _Tables.Add(_table);
                    _table = null;
                    
                    // Tabl;e of copmputed variables: Stubs: Netflow, Inflow, Outflow (1,2,3 of each).
                    _table = new DimNetflowDetailTable("NETFLOW1 (INFLOW1 - OUTFLOW)", brand, Netflow1, Inflow1, Outflow, filter);
                    _Tables.Add(_table);
                    _table = null;

                    _table = new DimNetflowDetailTable("NETFLOW2 (INFLOW2 - OUTFLOW)", brand, Netflow2, Inflow2, Outflow, filter);
                    _Tables.Add(_table);
                    _table = null;

                    _table = new DimNetflowDetailTable("NETFLOW3 (INFLOW3 - OUTFLOW)", brand, Netflow3, Inflow3, Outflow, filter);
                    _Tables.Add(_table);
                    _table = null;
                    /* 
                     * "Crosstab of OUTFLOW and INFLOW1.
                        Banner: ""Outflow - Likely to leave (OUTFLOW=1)""; ""Likely to stay (OUTFLOW=2)"".
                        Stubs: ""Inflow - Recently switched to client (INFLOW1=1)""; ""Existing customer that did not switch (INFLOW1=2)""."
                    */
                    BoundField obj = DummyField("OUTFLOW_INFLOW1", "OUTFLOW_INFLOW1");
                    _table = new DimOUTFLOW_INFLOWCrosstabTable("Crosstab of Outflow and Inflow1", brand, obj, Inflow1, Outflow, filter);
                    _Tables.Add(_table);
                    _table = null;

                    obj = DummyField("OUTFLOW_INFLOW2", "OUTFLOW_INFLOW2");
                    _table = new DimOUTFLOW_INFLOWCrosstabTable("Crosstab of Outflow and Inflow2", brand, obj, Inflow2, Outflow, filter);
                    _Tables.Add(_table);
                    _table = null;

                    obj = DummyField("OUTFLOW_INFLOW3", "OUTFLOW_INFLOW3");
                    _table = new DimOUTFLOW_INFLOWCrosstabTable("Crosstab of Outflow and Inflow3", brand, obj, Inflow3, Outflow, filter);
                    _Tables.Add(_table);
                    _table = null;

                    //Insights for NETFLOW - Trend

                    _table = new DimNetflowTrendDetailTable("Insights for NETFLOW - TREND", brand, Netflow1_in, filter);
                    _Tables.Add(_table);
                    _table = null;

                    _table = new DimNetflowTrendDetailTable("Insights for NETFLOW - TREND", brand, Netflow2_in, filter);
                    _Tables.Add(_table);
                    _table = null;

                    _table = new DimNetflowTrendDetailTable("Insights for NETFLOW - TREND", brand, Netflow3_in, filter);
                    _Tables.Add(_table);
                    _table = null;

                }

            }
            
            return _Tables;
        }

        private static DimFilter _getBrandFilter(Category brand)
        {
            return new DimFilter(
                    name: brand.Name,
                    desc: brand.Name,
                    expression: DimAxisHelper.ContainsAnyExpression(Iterator.Name, brand)
                );
        }

        private static BoundField DummyField(string objName, string qType)
        {
            BoundField obj = new BoundField();
            obj.Name = objName;
            obj.Properties = new Dictionary<string, dynamic>();
            obj.Properties.Add(GfKProperties.GfKQType, qType);
            return obj;
        }
    }
}
