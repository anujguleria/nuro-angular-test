﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSBAWSummaryTable : DimSummaryTable
    {
        private DimFilter _Filter { get; set; }

        public DimSBAWSummaryTable(string language, string context, string description, Container container, DimFilter filter) : base(language, context, description, (BoundField)null, container, filter)
        {
            _Filter = filter;
            // overload which is borrowed by SBAWBenchmarkTable
            Base = "All respondents";
            QTypeModifier = "MEAN";
            CreateElements();
            Side = SideAxis(AxisType.atAxis);
        }

        public void CreateElements()
        {
            DimSBAWMeanDetailTable throwaway = new DimSBAWMeanDetailTable(Language, Context, Description, Container, _Filter);
            Elements = throwaway.Elements;
            Elements.RemoveBaseElements(); // get rid of the base element for now
            Elements.AddBaseElement(Base);
            throwaway = null;
        }

    }
}
