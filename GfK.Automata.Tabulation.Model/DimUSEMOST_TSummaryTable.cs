﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimUSEMOST_TSummaryTable: DimSummaryTable
    {
        public DimUSEMOST_TSummaryTable(string language, string context, string desc, Field f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            Side = SummarySideAxis(Container, 0, false, false);
            AppendToFunnel(FunnelRows, "Use most");
            // 
            //foreach(Category cat in ((BoundField)Field).Categories())
            //{
            //    DimAxisElement e = new DimAxisElement();

            //    // for funnel rows:
            //    e.ID = cat.Name;
            //    e.Label = "Use most";
            //    e.Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, cat);
            //    FunnelRows.Add(cat, e);
            //}
        }
    }
}
