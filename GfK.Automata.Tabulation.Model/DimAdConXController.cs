﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimAdConXController
    {
        private static List<IScriptableItem> _Tables { get; set; } = null;
        private static string _Lang { get; set; } = ProcessorConfiguration.Language;
        private static string _Context { get; set; } = ProcessorConfiguration.Context;
        private static MetadataDocument _Doc { get; set; } = null;
        public static bool CreatedRBXTables { get; set; } = false;
        public static Container Container { get; set; }
        public static BoundField Iterator { get; set; }
        public static Container TopContainer { get; set; }
        static DimAdConXController()
        {

        }
        public static void Init()
        {
            
            
        }
        public static List<IScriptableItem> CreateAdConXTables()
        {
            _Tables = new List<IScriptableItem>();
            TopContainer = (Container)Container.Document.Find("AE_Loop");
            Iterator = (BoundField)TopContainer.Items.Find(o => o.Name.Contains("AdRecognitionADS"));
            SummaryAdEvaluationTable();
            AdPerformanceTables(50, 50);
            AdPerformanceTables(60, 60);
            AdPerformanceTables(70, 70);
            return _Tables;
        }

        public static void AdPerformanceTables(Int32 x, Int32 y)
        {
            // creates individual brand detail tables and the summary table
            DimAxisElements summaryElements = new DimAxisElements();
            List<IScriptableItem> _tables = new List<IScriptableItem>();
            string desc = "Ad Evaluation Score (x={0}/y={1}) - [AD]";
            IScriptableItem t = null;
            foreach (Category ad in Container.CategoryMap)
            {
                t = new DimAdConX3_M_NDTQuadrantTable(desc, ad, x, y, Container, null);
                summaryElements.Add(((DimQuadrantTable)t).Elements);
                _tables.Add(t);
            }
            t = new DimAdConX3_M_NDTSummaryTable(desc, summaryElements, x, y, Container, null);
            _tables.Add(t);

            _Tables.AddRange(_tables);
        }
        public static void SummaryAdEvaluationTable()
        {
            foreach(Category ad in TopContainer.CategoryMap)
            {
                IScriptableItem table = new DimAdConX3_M_NDTTable("Summary Table: Ad Evaluation Score - Average Impression (x) and Vividness (y)", ad, Iterator, TopContainer, Container, null);
                _Tables.Add(table);
                table = null;
            }
            
        }
    }
}
