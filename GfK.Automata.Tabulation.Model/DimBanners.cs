﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public class DimBanners
    {
        public Dictionary<string,string> Banners = new Dictionary<string,string>();
        public DimBanners()
        {
            // placeholder class for now, we will use this later with the DimTabPlan method and iteration of banner creation, etc
        }
  
        public void Add(string mrsName, string varName)
        {
            Banners.Add(mrsName, varName);
        }

    }
}
