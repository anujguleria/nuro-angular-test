﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAxisElements : IEnumerable<DimAxisElement>
    {

        public List<DimAxisElement> Items { get; set; }
        public DimAxisElements()
        {
            Items = new List<DimAxisElement>();
        }
        public void AddBaseElement(string label, string exp, bool hidden=false)
        {
            if (label is null) { label = String.Empty; }
            if( !(label.StartsWith("Base"))) { label = "Base " + label; }
            DimAxisElement b = new DimAxisElement(hide:hidden) {
                ID = "b",
                Label = label,
                Type = ExpressionType.etBase,
                Expression = exp
            };
            Items.Insert(0, b);
        }
        public void AddBaseElement(string label)
        {
            AddBaseElement(label, "", false);
        }
        
        public void RemoveBaseElements()
        {
            RemoveByType(ExpressionType.etBase);
        }
        public void RemoveByType(ExpressionType eType)
        {
            Items.RemoveAll((e) => { return (e.Type == eType); });
        }
        public void Add(IEnumerable<DimAxisElement>elements)
        {
            foreach(DimAxisElement e in elements)
            {
                Add(e);
            }
        }

        public void Add(IEnumerable<Category> cats)
        {
            Add(cats, false);
        }
        public void Add(IEnumerable<Category> cats, bool hideElements)
        {
            foreach (Category c in cats)
            {
                Add(c, hideElements);
            }
        }
        public void Add(Category c)
        {
            Add(c, false);
        }
        public void Add(Category c, bool hideElement)
        {
            string lab = c.Label(ProcessorConfiguration.Context, ProcessorConfiguration.Language, ProcessorConfiguration.Document, true);
            lab = DimAxisHelper.CleanText(lab);
            if (lab.Trim() == String.Empty) { lab = c.Name; }
            Items.Add(new DimAxisElement(hide:hideElement)
            {
                ID = c.Name,
                Label = lab,
                Type = ExpressionType.etCategory,
                Expression = null
            });
        }
        public void Add(DimAxisElement itm)
        {
            Items.Add(itm);
        }
        public void Add(DimAxisElements elements)
        {
            Items.AddRange(elements.Items);
        }
        public string Join(string delimiter)
        {
            return String.Join(delimiter, (from itm in Items select itm.ID) );
        }
        public string ToScript()
        {
            return String.Join(", ", (from e in Items select e.ToScript()));
        }

        public IEnumerator<DimAxisElement> GetEnumerator()
        {
            return ((IEnumerable<DimAxisElement>)Items).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<DimAxisElement>)Items).GetEnumerator();
        }
    }
}
