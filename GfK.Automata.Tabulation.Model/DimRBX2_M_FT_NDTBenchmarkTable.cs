﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimRBX2_M_FT_NDTBenchmarkTable: DimBenchmarkTable
    {
        public DimRBX2_M_FT_NDTBenchmarkTable(string description, DimAxisElements elements, Int32 x, Int32 y, Container container, DimFilter filter): base(description, container, filter)
        {
            // similar to the summary table, this receives elements that will be modified.
            // Elements are created during the preceding brand detail tables, and passed in the constructor
            Elements = elements;
            Base = "Base"; // "Respondents who rated the brand";
            UpdateElements();
            Side = SideAxis(AxisType.atAxis);
            Description = String.Format(Description, x, y);
            Annotations.Add(new DimAnnotation(AnnotationType.atFooter, "RBX Score = [% Quadrant 2 (Memorable & Positive) - % Quadrant 1 (Memorable & Negative)]"));
        }

        public void UpdateElements()
        {
            // modifies the elements received from the detail/summary table, hiding rows that need hidden, etc
            // Hide all elements
            foreach (DimAxisElement e in Elements)
            {
                if (e.Type != ExpressionType.etDerived)
                {
                    e.Properties[DimAxisElement.pIsHidden] = true;
                    //e.Properties[DimAxisElement.pCountsOnly] = true;
                }
                if (e.ID == "b") { e.Properties[DimAxisElement.pIsHidden] = false; }
            }
            IEnumerable<DimAxisElement> rbxElements = Elements.Where(e => e.Type == ExpressionType.etDerived);
            string bmExp = "(" + String.Join("+", (from e in rbxElements select e.ID)) + ")";
            bmExp += " / " + rbxElements.Count().ToString();
            DimAxisElement bm = new DimAxisElement()
            {
                ID = "benchmarkRBX",
                Type= ExpressionType.etDerived,
                Label = "Benchmark Average RBX",
                Expression = bmExp
            };
            Elements.Add(bm);

        }
    }
}
