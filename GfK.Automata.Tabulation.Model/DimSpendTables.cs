﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimSpendTables
    {
        private static List<IScriptableItem> _Tables { get; set; }
        private static string _lang { get; set; }
        private static string _context { get; set; }
        private static UnboundField _Field { get; set; }
        private static List<string> _bandLabels { get; set; }
        private static List<double> _bands { get; set; }
        private static Container _Container { get; set; }
        private static Int32 _min { get; set; }
        private static Int32 _max { get; set; }
        static DimSpendTables()
        {

            _Tables = new List<IScriptableItem>();

        }

        public static List<IScriptableItem> CreateSpendTables(Container c, string language, string context, Dictionary<string, DimFilter> filters)
        {
           
            _Container = c;
            _lang = language;
            _context = context;
            
            if (DimOLEDbConnection.ColumnIsEmpty((Field)_Container.Items.First())) { return null; }
            Dictionary<string, List<double>> percentiles = _getPercentiles();
            foreach (UnboundField f in _Container.Items)
            {
                _Field = f;
                IScriptableItem table = null;
                table = new DimSpendDetailTable(_lang, _context, "Estimated Spend for Next Purchase", _Field, _Container, null, percentiles, true);
                _Tables.Add(table);
                table = null;

            }
            Dictionary<string, List<double>> segments = _getPercentiles();
            foreach (UnboundField f in _Container.Items)
            {
                _Field = f;
                IScriptableItem table = null;
                table = new DimSpendDetailTable(_lang, _context, "Price Segments", _Field, _Container, null, segments);
                ((DimDetailTable)table).AddQTypeMod("SEG");
                _Tables.Add(table);
                table = null;

            }

            return _Tables;
        }
        private static Dictionary<string, List<double>> _getPercentiles()
        {
            if (DimOLEDbConnection.ColumnIsEmpty((Field)_Container.Items.First())) { return null; }
            List<double> vals = new List<double>();
            // quintiles
            Dictionary<string, List<double>> d = new Dictionary<string, List<double>>();
            vals = DimOLEDbConnection.GetPercentileRange(_Container.Items.First().Name, 0, .2, "");
            d.Add("First band", vals);
            vals = DimOLEDbConnection.GetPercentileRange(_Container.Items.First().Name, .2, .4, "");
            d.Add("Second band", vals);
            vals = DimOLEDbConnection.GetPercentileRange(_Container.Items.First().Name, .4, .6, "");
            d.Add("Third band", vals);
            vals = DimOLEDbConnection.GetPercentileRange(_Container.Items.First().Name, .6, .8, "");
            d.Add("Fourth band", vals);
            vals = DimOLEDbConnection.GetPercentileRange(_Container.Items.First().Name, .8, 1, "");
            d.Add("Fifth band", vals);
            return d;

        }
        private static Dictionary<string, List<double>> _getPriceSegments()
        {
            Dictionary<string, List<double>> d = new Dictionary<string, List<double>>();
            
            // quintiles
            d.Add("< $150", new List<double>() { 0, 150 });
            d.Add("$150 to < $300", new List<double>() { 150, 300 });
            d.Add("$300 to < $500", new List<double>() { 300, 500 });
            d.Add("$500 to $1000", new List<double>() { 500, 1000 });
            return d;

        }


    }
}
