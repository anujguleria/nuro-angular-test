﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimTabPlan : IScriptableItem
    {

        private static List<IScriptableItem> _items;
        public ProcessorConfiguration Configuration;
        public static Int64 ItemCount;
        private static Dictionary<string, Int32> _qTypeCounter = new Dictionary<string, Int32>();
        //public Dictionary<string, DimFilter> Filters { get; protected set; }

        public DimTabPlan(ProcessorConfiguration configuration)
        {
            _items = new List<IScriptableItem>();
            Configuration = configuration;
        }

        public void Add(IScriptableItem item)
        {

            if (item != null)
            {
                string qtype = GetQType(item).Replace(" ", "");
                //string tableName = _getRosettaName(qtype, true);
                ((DimAggregatedTable)item).Name = "T" + (ItemCount += 1).ToString();
                // table description BEGINS with qtype for Rosetta tagging
                ((DimAggregatedTable)item).Description = _getRosettaName(item, qtype, true) + " " + ((DimAggregatedTable)item).Description;

                /**  TO FIX LATER **/
                // hack for POC to include banner in spec
                string topAxis = ((DimAggregatedTable)item).Top;
                if (topAxis is null)
                {
                    topAxis = (string)Configuration["BANNERNAMES"]["MONTHLY_BANNER"];
                }
                else
                {
                    topAxis = (string)Configuration["BANNERNAMES"]["MONTHLY_BANNER"] + " > " + topAxis;
                }
                ((DimAggregatedTable)item).Top = topAxis;
                _items.Add(item);
            }
        }

        public void Add(List<IScriptableItem> items)
        {
            if (items is null) { return; }
            foreach (IScriptableItem i in items)
            {
                Add(i);
            }
        }

        public void Clear()
        {
            _items.Clear();
        }

        public void Remove(IScriptableItem item)
        {
            _items.Remove(item);
        }

        public string GetScript(ScriptType type)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_getScriptHeader());
            foreach (IScriptableItem item in _items)
            {
                sb.Append(item.GetScript(type));
                sb.AppendLine();
                sb.AppendLine();
            }

            sb.Append(_getPopulate());
            sb.Append(_getFilterTables());
            sb.Append(_getHelperFunctions());
            sb.Append(_getBannerFunctions());
            
            return sb.ToString();
        }

        public string _getPopulate()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("End With");
            sb.AppendLine(Configuration["TableDoc"] + ".Populate()");
            sb.AppendLine("Dim saveAsFileName");
            sb.AppendLine("saveAsFileName = FMTD");
            sb.AppendLine(_getSaveAs());
            sb.AppendLine();
            return sb.ToString();
        }
        public string _getSaveAs()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Configuration["TableDoc"] + ".Save(saveAsFileName)");
            return sb.ToString();
        }

        public string _getFilterTables()
        {
            StringBuilder sb = new StringBuilder();
            GlobalFilter lastFilter = null;

            
            IEnumerable<GlobalFilter> projectFilters = ProcessorConfiguration.GlobalFilters.Items.Values.Where(i => i.ProjectType == Configuration["ProjectType"]);
            if (projectFilters.Count() == 0) { return String.Empty; }

            sb.AppendLine("With " + Configuration["TableDoc"]);
            foreach (IScriptableItem filter in projectFilters)
            {
                if (!(lastFilter is null))
                {
                    sb.AppendLine("    .Global.Filters.Remove(\"" + lastFilter.Name + "\")");
                }
                sb.AppendLine("    " + filter.GetScript(ScriptType.stDimensions));
                sb.AppendLine("    " + Configuration["TableDoc"] + ".Populate()");
                sb.AppendLine("    saveAsFileName = Replace(FMTD, \".mtd\", \"." + ((GlobalFilter)filter).Name + ".mtd\")");
                sb.AppendLine("    " + _getSaveAs());
                sb.AppendLine();
                lastFilter = (GlobalFilter)filter;
            }
            sb.AppendLine("End With");
            return sb.ToString();
        }
        public string _getBannerFunctions()
        {
            StringBuilder sb = new StringBuilder();
            string path = AppDomain.CurrentDomain.BaseDirectory + @"Resources\CreateBannerVariables.mrs";

            using (StreamReader r = new StreamReader(path))
            {
                while (true)
                {
                    string line = r.ReadLine();
                    if (line == null)
                    {
                        break;
                    }
                    sb.AppendLine(line);
                }

            }

            return sb.ToString();
        }

        public string _getHelperFunctions()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("'-----------Helper Functions ------------");
            sb.AppendLine();
            sb.AppendLine("Sub SetProperty(Properties, Name, Value)");
            sb.AppendLine("    Dim Property");
            sb.AppendLine("    Set Property = Properties.FindItem(Name)");
            sb.AppendLine("    If IsNullObject(Property) Then");
            sb.AppendLine("        Set Property = Properties.CreateProperty()");
            sb.AppendLine("        Property.Name = Name");
            sb.AppendLine("        Property.Value = Value");
            sb.AppendLine("        Properties.Add(Property)");
            sb.AppendLine("    Else");
            sb.AppendLine("        Property.Value = Value");
            sb.AppendLine("    End If");
            sb.AppendLine("End Sub");
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("Function FormatFileNameDate()");
            sb.AppendLine("    Dim ret");
            sb.AppendLine("    ret = \"_\"");
            sb.AppendLine("    ret = ret + CText( DatePart(Now(), \"yyyy\") )");
            sb.AppendLine("    ret = ret + CText( DatePart(Now(), \"m\") )");
            sb.AppendLine("    ret = ret + CText( DatePart(Now(), \"d\") )");
            sb.AppendLine("    FormatFileNameDate = ret + \".mtd\"");
            sb.AppendLine("End Function");
            return sb.ToString();
        }

        public string _getScriptHeader()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("#define MDAT \"" + Configuration["MDMPath"] + "\"");
            sb.AppendLine("#define MDSC \"" + Configuration["MDSC"] + "\"");
            sb.AppendLine("#define VERS \"" + Configuration["MetadataVersion"] + "\"");
            sb.AppendLine("#define CDAT \"" + Configuration["CaseDataPath"] + "\"");
            sb.AppendLine("#define CDSC \"" + Configuration["CDSC"] + "\"");
            sb.AppendLine("#define PROJ \"" + Configuration["Project"] + "\"");
            sb.AppendLine("#define FMTD \"" + Configuration["MTDPath"] + "\"");
            sb.AppendLine("#define MAXDATE \"" + Configuration["MAXDATE"] + "\"");
            sb.AppendLine("#define DATEVAR \"" + Configuration["DATEVAR"] + "\"");
            //sb.AppendLine("#define MONTHLY_BANNER \"" + Configuration["MONTHLY_BANNER"] + "\"");
            //sb.AppendLine("#define QUARTER_BANNER \"" + Configuration["QUARTER_BANNER"] + "\"");
            foreach (string _k in Configuration["BANNERNAMES"].Keys)
            {
                sb.AppendLine("#define " + _k + " \"" + Configuration["BANNERNAMES"][_k] + "\"");
            }
            sb.AppendLine("#define MDAT_BANNERS \"" + Configuration["MDAT_BANNERS"] + "\"");
            sb.AppendLine();
            sb.AppendLine("Dim mddPath");
            sb.AppendLine("If CreateMDDWithBanners() Then");
            sb.AppendLine("    mddPath = MDAT_BANNERS");
            sb.AppendLine("Else");
            sb.AppendLine("    mddPath = MDAT");
            sb.AppendLine("End If");
            sb.AppendLine();
            sb.AppendLine("Dim " + Configuration["TableDoc"]);
            sb.AppendLine("Set " + Configuration["TableDoc"] + " = CreateObject(\"TOM.Document\")");
            sb.AppendLine(Configuration["TableDoc"] + ".DataSet.Load(mddPath, , CDAT, CDSC, PROJ, VERS)");
            sb.AppendLine(Configuration["TableDoc"] + ".DataSet.MDMDocument.Contexts.Item[\"" + Configuration["Context"] + "\"].Alternatives.Add(\"Question\")");
            sb.AppendLine(Configuration["TableDoc"] + ".Context = \"" + Configuration["Context"] + "\"");
            sb.AppendLine(Configuration["TableDoc"] + ".Language = \"" + Configuration["Language"] + "\"");
            sb.AppendLine();
            sb.AppendLine("With " + Configuration["TableDoc"] + ".Default");
            sb.AppendLine("    .Clear()");
            sb.AppendLine("    .CellItems.AddNew(1 '! ColPercent !')");
            sb.AppendLine("    SetProperty(.Properties, \"AutoBaseSpecification\", \"Base base()\")");
            sb.AppendLine("    SetProperty(.Properties, \"AutoUnweightedBaseSpecification\", \"UnweightedBase unweightedbase()\")");
            sb.AppendLine("    SetProperty(.Properties, \"ShowPercentSigns\", False)");
            sb.AppendLine("    SetProperty(.Properties, \"AdjustRounding\", True)");
            sb.AppendLine("    Dim stat");
            sb.AppendLine("    Set stat = .Statistics.Add(\"ColumnProportions\")");
            sb.AppendLine("    SetProperty(stat.Properties, \"SigLevel\", 5)");
            sb.AppendLine("    SetProperty(.Properties, \"TestAtLowerLevels\", False)");
            sb.AppendLine("    Set stat = .Statistics.Add(\"ColumnMeans\")");
            sb.AppendLine("    SetProperty(stat.Properties, \"SigLevel\", 5)");
            sb.AppendLine("    SetProperty(.Properties, \"TestAtLowerLevels\", False)");
            sb.AppendLine("    .Annotations[5 '! LeftFooter !'].Specification =\"{CellContents \\p \\n}{Statistics \\p \\n}{Rules \\p}{CellItemSymbols \\p}{PopulateWarnings \\p}\"");
            sb.AppendLine("End With");
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("Dim t  'Table object");
            sb.AppendLine("Dim a  ' Annotation object");
            sb.AppendLine("Dim globalFilter ' Global Filter");
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("With " + Configuration["TableDoc"]);
            sb.AppendLine();
            return sb.ToString();
        }

        #region GetGfKQType
        private static string GetQType(IScriptableItem item)
        {
            if (item is DimFunnelTable)
            {
                return "FUNNEL";
            }
            return GetQType(((DimAggregatedTable)item).Field, ((DimAggregatedTable)item).Container);
        }
        
        public static string GetQType(Container container)
        {
            if (container != null && container.Properties.ContainsKey(GfKProperties.GfKQType))
            {
                return container.Properties[GfKProperties.GfKQType];
            }
            return "";
        }
        public static string GetQType(Field field)
        {

            if (field != null && field.Properties.ContainsKey(GfKProperties.GfKQType))
            {
                return field.Properties[GfKProperties.GfKQType];
            }
            return "";
        }
        public static string GetQType(Field field, Container container)
        {

            string propVal = "";
            if (GetQType(field) == "")
            {
                propVal = GetQType(container);
            }
            else
            {
                propVal = GetQType(field);
            }
            if (propVal.Trim() == String.Empty)
            { propVal = field.Name; }

            return propVal.ToUpper();
        }

        #endregion GetGfKQType

        private string _getRosettaName(IScriptableItem itm, string qtype, bool increment)
        {
            // returns Rosetta-specified "Name" prefix for tables, per format:
            // GfKQtype.Index
            // each qtype will be index from 1 to n

            string tableName = "{0}.{1}";
            string mod = ((DimAggregatedTable)itm).QTypeModifier;
            if (mod != String.Empty)
            {
                // QTypeModifier goes after the base Qtype to identify tables, 
                // i.e., CL_BE1_3.BEN vs CL_BE1_54.BEN where _3 and _5 are subtypes of Benchmark
                if (mod.First() != '.')
                {
                    mod = "." + mod;
                }

                qtype += mod.Trim();
            }

            if (_qTypeCounter.ContainsKey(qtype))
            {
                if (increment)
                {
                    _qTypeCounter[qtype] += 1;
                }
                else
                {
                    // possibly we need to query this later without incrementing it
                    throw (new NotImplementedException());
                }
            }
            else
            {
                // key doesn't exist, so we begin with index 0
                _qTypeCounter.Add(qtype, 1);
            }
            Int32 i = _qTypeCounter[qtype];
            return String.Format(tableName, qtype, i);

        }

    }
}
