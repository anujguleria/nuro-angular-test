﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimCL_BE1_5BenchmarkTable : DimBenchmarkTable
    {
        private Char _delimiter = '.';
        private static string _topParent = "CL_BE1_Loop";
        private Container _top = null;
        private Int32 _topCount = 2;
        //private bool _hideCats = true;
        private bool _sortDesc = true;
        public DimCL_BE1_5BenchmarkTable(string language, string context, string desc, Container c, DimFilter filter) : base(language, context, desc, null, c, _topParent, filter)
        {
            AddQTypeMod("_5");
            Field = (BoundField)Container.Items.First();
            _top = (Container)Field.Document.Find(_topParent);
            // mostly same code as CL_BE1_4SummaryAttributeSideAxis, but exclude the derived elements
            CL_BE1_5Elements();
            Side = SideAxis(AxisType.atAxis);
            Side += "\"\"" + Description + "\"\"";

        }

        public void CL_BE1_5Elements()
        {

            //  axis({
            //    b base(), 
            //    ex1 'Attribute1 * Brand1' expression('CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex2 'Attribute2 * Brand1' expression('CL_BE1_Loop[{_2}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex3 'Attribute3 * Brand1' expression('CL_BE1_Loop[{_3}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex4 'Attribute4 * Brand1' expression('CL_BE1_Loop[{_4}].CL_BE1_GRID[{Brand1}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex5 'Attribute1 * Brand1' expression('CL_BE1_Loop[{_1}].CL_BE1_GRID[{Brand2}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex6 'Attribute2 * Brand1' expression('CL_BE1_Loop[{_2}].CL_BE1_GRID[{Brand2}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex7 'Attribute3 * Brand1' expression('CL_BE1_Loop[{_3}].CL_BE1_GRID[{Brand2}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ex8 'Attribute4 * Brand1' expression('CL_BE1_Loop[{_4}].CL_BE1_GRID[{Brand2}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    ...  
            //    ...
            //    ex40 'Attribute4 * Brand1' expression('CL_BE1_Loop[{_4}].CL_BE1_GRID[{Brand10}].CL_BE1.ContainsAny({_4,_5})')[IsHidden = true],
            //    

            //e.g.:

            //     'Benchmark' derived('(   
 
            //     ex1 + ex2 + ex3 + ex4 + ex5 + ex6 + ex7 + ex8 + ex9 + ex10 +
            //     ex11 + ex12 + ex13 + ex14 + ex15 + ex16 + ex17 + ex18 + ex19 + ex20 +
            //     ex21 + ex22 + ex23 + ex24 + ex25 + ex26 + ex27 + ex28 + ex29 + ex30 +
            //     ex31 + ex32 + ex33 + ex34 + ex35 + ex36 + ex37 + ex38 + ex39 + ex40) / 40')}) ' Summary Table: Client''s Brand Equity Measure -Overall Brand Equity(Aware of brand)'


            //    })

            string label = "Overall Brand Equity";
            List<string> catLst = (from c in DimAxisHelper.GetCategories(Field.Items, _sortDesc).Take(_topCount).ToList() select c.Name).ToList();
            List<string> derived = new List<string>();
            Int32 attributeCount = Container.Items.Count / Container.CategoryMap.Count;
            Int32 eCount = 0;
            attributeCount = _top.CategoryMap.Count;
            //for (int = 0; )
            for (Int32 i = 0; i < Container.CategoryMap.Count; i++)
            {
                if (InvalidBrandNames.Contains(Container.CategoryMap[i])) { continue; }
                string[] varParts = Container.Items[i].Name.Split(_delimiter);
                
                string brandLabel = Container.CategoryMap[i].Label(Context, Language, Container.Document);
                for (Int32 n = 0; n < attributeCount; n++)
                {
                    string var = _topParent + "[{" + _top.CategoryMap[n].Name + "}]." + varParts[1] + "." + varParts[2];
                    string condExp = DimAxisHelper.ContainsAnyExpression(var, catLst);
                    derived.Add("@" + eCount.ToString());
                    Elements.Add(new DimAxisElement(hide: true)
                    {
                        ID = "e" + Elements.Count().ToString(),
                        Label = String.Format("Attribute {0} - {1}", n, Container.CategoryMap[i].Name),
                        Expression = condExp
                    });
                }
            }
            Elements.Add(new DimAxisElement()
            {
                ID = "bm",
                Label = label,
                Type = ExpressionType.etDerived,
                Expression = "(" + String.Join("+", (from e in Elements select e.ID)) + ") / " + Elements.Count().ToString()
            });
            Elements.AddBaseElement(Base);
            
        }
        
    }
}
