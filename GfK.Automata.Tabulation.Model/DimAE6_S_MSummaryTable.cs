﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAE6_S_MSummaryTable: DimSummaryTable
    {
        private Container _parent { get; set; }
        private Category _ad { get; set; }
        public DimAE6_S_MSummaryTable(string language, string context, string description, BoundField f, Container c, Container parent, Category ad) : base(language, context, description, f, c, null)
        {
            Base = "Exposed to ad and recall ad";
            _parent = parent;
            _ad = ad;
            Side = AE6_S_MSummarySideAxis();
            SetElements();
            Side = SideAxis(AxisType.atAxis);

            FormatDescription("[AD]", _ad.Label(Context, Language, parent.Document));
        }
        public void SetElements()
        {

            string[] varParts = DimAxisHelper.GetVarParts(Field);
            DimAxisElements learnElements = new DimAxisElements();
            DimAxisElements talkElements = new DimAxisElements();
            DimAxisElements doElements = new DimAxisElements();
                        
            List<string> topCats = DimAxisHelper.GetCategoryNames((BoundField)Field, true).Take(2).ToList();
            string catNames = String.Join(", ", topCats);
            string adContainer = Container.Name.Replace("..", _ad.Name);
            string itm = varParts[varParts.Length - 1];

                
            for (Int32 catIndex = 0; catIndex <= 1; catIndex++)
            {
                learnElements.Add(new DimAxisElement()
                {
                    ID = "_l" + Container.CategoryMap[catIndex].Name,
                    Label = DimAxisHelper.GetLabel(Container.CategoryMap[catIndex]),
                    Expression = adContainer + "[" + Container.CategoryMap[catIndex].Name + "]." + itm + ".ContainsAny({" + catNames + "})"
                });
            }

            for(Int32 catIndex=2;catIndex<=4;catIndex++)
            {
                talkElements.Add(new DimAxisElement()
                {
                    ID = "_t" + Container.CategoryMap[catIndex].Name,
                    Label = DimAxisHelper.GetLabel(Container.CategoryMap[catIndex]),
                    Expression = adContainer + "[" + Container.CategoryMap[catIndex].Name + "]." + itm + ".ContainsAny({" + catNames + "})"
                });
            }

            for (Int32 catIndex = 5; catIndex <= 7; catIndex++)
            {
                doElements.Add(new DimAxisElement()
                {
                    ID = "_d" + Container.CategoryMap[catIndex].Name,
                    Label = DimAxisHelper.GetLabel(Container.CategoryMap[catIndex]),
                    Expression = adContainer + "[" + Container.CategoryMap[catIndex].Name + "]." + itm + ".ContainsAny({" + catNames + "})"
                });
                    
            }

            Elements.Add(new DimAxisElement()
            {
                ID = "_learn",
                Label = "Net: Learn",
                Type = ExpressionType.etNet,
                Expression = learnElements.ToScript()
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "_talk",
                Label = "Net: Talk",
                Type = ExpressionType.etNet,
                Expression = talkElements.ToScript()
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "_do",
                Label = "Net: Do",
                Type = ExpressionType.etNet,
                Expression = doElements.ToScript()
            });

            Elements.AddBaseElement(Base);

        }
        public string AE6_S_MSummarySideAxis()
        {
            /* NOTE: There is no AE6_S_M_Grid[_3] element though spec indicates there should be
             * 
             * 
             * 
               axis({
                    b 'Base' base(),
                    e1 'Learn: Ad1, AE6 attribute 1,2' 
                        expression('
                            AE_Loop[Ad1].AE6_S_M_GRID[_1].AE6_S_M.ContainsAny({_4,_5}) OR
                            AE_Loop[Ad1].AE6_S_M_GRID[_2].AE6_S_M.ContainsAny({_4,_5})
                                    '),
                    e2 'Talk: Ad1, AE6 attribute 3,4,5' 
                        expression('
                            AE_Loop[Ad1].AE6_S_M_GRID[_4].AE6_S_M.ContainsAny({_4,_5}) OR
                            AE_Loop[Ad1].AE6_S_M_GRID[_5].AE6_S_M.ContainsAny({_4,_5}) OR
                            AE_Loop[Ad1].AE6_S_M_GRID[_6].AE6_S_M.ContainsAny({_4,_5})
                                    '),
                    e3 'Do: Ad1, AE6 attribute 3,4,5' 
                        expression('
                            AE_Loop[Ad1].AE6_S_M_GRID[_7].AE6_S_M.ContainsAny({_4,_5}) OR
                            AE_Loop[Ad1].AE6_S_M_GRID[_8].AE6_S_M.ContainsAny({_4,_5}) OR
                            AE_Loop[Ad1].AE6_S_M_GRID[_9].AE6_S_M.ContainsAny({_4,_5})
                                    ')
                    })
            */
            string[] varParts = Field.Name.Split('.');
            List<string> elements = new List<string>();
            string baseExp = "b '" + Base + "' base()";
            string axExp = "axis({";
            string exp = "";
            List<string> topCats = DimAxisHelper.GetCategoryNames((BoundField)Field, true).Take(2).ToList();
            string cats = String.Join(", ", topCats);
            string adContainer = Container.Name.Replace("..", _ad.Name);
            string itm = varParts[varParts.Length - 1];
            elements.Add(baseExp);
            exp = "e1 'Learn' expression('";
            exp += adContainer + "["+ Container.CategoryMap[0].Name + "]." + itm + ".ContainsAny({" + cats + "})";
            exp += " OR ";
            exp += adContainer + "[" + Container.CategoryMap[1].Name + "]." + itm + ".ContainsAny({" + cats + "})";
            exp += "')"; // expression
            elements.Add(exp);

            exp = "e2 'Talk' expression('";
            exp += adContainer + "[" + Container.CategoryMap[2].Name + "]." + itm + ".ContainsAny({" + cats + "})";
            exp += " OR ";
            exp += adContainer + "[" + Container.CategoryMap[3].Name + "]." + itm + ".ContainsAny({" + cats + "})";
            exp += " OR ";
            exp += adContainer + "[" + Container.CategoryMap[4].Name + "]." + itm + ".ContainsAny({" + cats + "})";
            exp += "')"; // expression
            elements.Add(exp);

            exp = "e3 'Do' expression('";
            exp += adContainer + "[" + Container.CategoryMap[5].Name + "]." + itm + ".ContainsAny({" + cats + "})";
            exp += " OR ";
            exp += adContainer + "[" + Container.CategoryMap[6].Name + "]." + itm + ".ContainsAny({" + cats + "})";
            exp += " OR ";
            exp += adContainer + "[" + Container.CategoryMap[7].Name + "]." + itm + ".ContainsAny({" + cats + "})";
            exp += "')"; // expression
            elements.Add(exp);

            axExp += String.Join(", ", elements) + "})";
            return axExp;
        }
    }
}
