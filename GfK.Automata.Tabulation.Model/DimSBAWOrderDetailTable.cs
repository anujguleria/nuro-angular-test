﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSBAWOrderDetailTable: DimDetailTable
    {
        
        public DimSBAWOrderDetailTable(string language, string context, string description, UnboundField brandField, Container container, DimFilter filter): base(language,context, description, brandField, container, filter)
        {
            Base = "Mentioned Brand";
            QTypeModifier = "ORD";
            CreateElements();
            Side = SideAxis();
            string brandLabel = Container.CategoryMap[Container.IndexOf(brandField)].Label(Context,Language,Container.Document,true);
            Field = (Field)Container.Document.Find("SBAW1"); // hacks the field used in tab plan for assigning the name in Reporter output.
            FormatDescription("[BRAND]", brandLabel);
            
        }
        public string SideAxis()
        {
            return Field.Name + "{" + Elements.ToScript() + "}";
        }
        public void CreateElements()
        {
            string[] varParts = Container.Name.Split('.');
            string varName = "[" + varParts.Last() + "]";
            Elements = new DimAxisElements();
            Elements.Add(new DimAxisElement() 
            {
                ID = "_1",
                Label = "First",
                Expression = varName + " = 1"
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "_2",
                Label = "Second",
                Expression = varName + " = 2"
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "_3",
                Label = "Third",
                Expression = varName + " = 3"
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "_4_5",
                Label = "Net: 4 or 5 (4th or 5th)",
                Expression = String.Format("({0} = 4) OR ({0} = 5)", varName)
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "_6",
                Label = "Net: 6th or higher",
                Expression = varName + " >= 6"
            });

            Elements.AddBaseElement(Base);
        }
    }
}
