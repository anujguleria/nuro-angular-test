﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{

    public static class InvalidBrandNames
    {
        // Hack for bad metadata
        public static List<string> _items { get; set; } = new List<string>(){
            "Brand1",
            "Brand2",
            "Brand3",
            "Brand4",
            "Brand5",
            "Brand6",
            "Brand8",
            "Brand7",
            "Brand9",
            "Brand10",
            "Brand 1",
            "Brand 2",
            "Brand 3",
            "Brand 4",
            "Brand 5",
            "Brand 6",
            "Brand 8",
            "Brand 7",
            "Brand 9",
            "Brand 10",
            "Brand1.png",
            "Brand2.png",
            "Brand3.png",
            "Brand4.png",
            "Brand5.png",
            "Brand6.png",
            "Brand8.png",
            "Brand7.png",
            "Brand9.png",
            "Brand10.png",
            "None"
            };
        public static bool Contains(Category cat)
        {
            string label = cat.Label(ProcessorConfiguration.Context, ProcessorConfiguration.Language, ProcessorConfiguration.Document, true);
            label = DimAxisHelper.CleanText(label);
            return (Contains(cat.Name) || Contains(label));
            
        }
        public static bool Contains(string name)
        {
            return _items.Contains(name);
        }
        public static List<string> InvalidBrands
        {
            get {return _items; }
        }

    };
}
