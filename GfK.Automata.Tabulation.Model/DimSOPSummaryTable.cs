﻿using System;
using System.Collections.Generic;
using System.Linq;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimSOPSummaryTable: DimSummaryTable
    {
        public DimSOPSummaryTable(string language, string context, string desc, Field f, Container c, DimFilter filter) : base(language, context, desc, f, c, null)
        {
            Base = "Total purchases";
            CreateElements();
            Side = SideAxis();
            

        }

        public void CreateElements()
        {
            
            string[] varParts = Field.Name.Split('.');
            string baseMult = Container.Name + "." + varParts.Last();
            foreach (UnboundField f in Container.Items)
            {
                string brandName = Container.CategoryMap[Container.Items.IndexOf(f)].Name;
                string brandLabel = Container.CategoryMap[Container.Items.IndexOf(f)].Label(Context, Language, Container.Document);
                brandLabel = DimAxisHelper.CleanText(brandLabel);
                string brandExp = String.Format("^.{0} > 0", f.Name);
                DimAxisElement e = new DimAxisElement();
                e.ID = brandName;
                e.Label = brandLabel;
                e.Expression = brandExp;
                e.AddProperty(DimAxisElement.pMultiplier, f.Name);
                Elements.Add(e);
            }
            Elements.AddBaseElement(Base);
            Elements.Items[0].AddProperty(DimAxisElement.pMultiplier, baseMult);

        }
        public string SideAxis()
        {
            string[] varParts = Field.Name.Split('.');
            string axis = Container.Name + "[..]." + varParts.Last();

            return axis + "{" + Elements.ToScript() + "}";
        }
    }
}
