﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimABAWDetailTable: DimDetailTable
    {
        public DimABAWDetailTable(string desc, Category brand, BoundField field, Container container, DimFilter filter): base(desc, brand, field,container,filter)
        {
            Brand = brand;
            CreateElements();
            Side = SideAxis(AxisType.atAxis);
            Description += String.Format(" {0}", DimAxisHelper.CleanText( Brand.Label(Context, Language, Field.Document) ));

        }
        public void CreateElements()
        {
            Category aware = ((BoundField)Field).Categories()[0];       // {_1}
            Category notAware = ((BoundField)Field).Categories()[1];    // {_2}

            string naExpression = DimAxisHelper.NotAwareExpression(Field, Container, "PRIORITY_BRAND_AWARE");
            Elements = new DimAxisElements();
            Elements.Add(new DimAxisElement("_aware", "Aware")
            {
                Expression = Field.Name + "={" + aware.Name + "}"
            });
            Elements.Add(new DimAxisElement("_notAware", "Not aware")
            {
                Expression = Field.Name + "={" + notAware.Name +"} OR " + naExpression
            });
            Elements.AddBaseElement(Base);
            
        }
    }
}
