﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimBRIE_MSummaryTable : DimSummaryTable
    {
        private Container _Parent { get; set; } = null;
        private Char _delimiter = '.';
        public DimBRIE_MSummaryTable(string language, string context, string desc, Field f, Container c ) : base(language, context, desc, f, c, null)
        {

            Side = SummaryBrandSideAxis((BoundField)f);
            FormatDescription("Brand", c);
            
        }
        public DimBRIE_MSummaryTable(string language, string context, string desc, Field f, Container c, Container parent) : base(language, context, desc, f, c, null)
        {
            _Parent = parent;
            SetElementsAttribute();
            Side = SummaryAttributeSideAxis();

            // Description = "Summary Table: Brand Imagery - [ATTRIBUTE] - Top 2 Box";
            // parent.CategoryMap contains the labels we need here.
            FormatDescription("ATTRIBUTE", parent);
        }
        public string SummaryBrandSideAxis(BoundField f)
        {
            // BRAND Summary for each Attribute
            // BRIE_M_Loop[{..}] > BRIE_M_Loop[..].BRIE_M_GRID[{Brand1}].BRIE_M{'Base: All respondents' base(), N1 'Top 2 Box' net({_3 [IsHidden=True], _4 [IsHidden=True]})}

            string[] varParts = f.Name.Split(_delimiter);
            string netExp = "N1 " + DimAxisHelper.NetExpression(this, "Top 2 Box", f.Categories().Skip(2), true);
            netExp += "})";
            string sideAxis = varParts[0].Replace("{_1}", "{..}") + " > ";
            varParts[0] = varParts[0].Replace("[{_1}]", "");
            sideAxis += String.Join(".", varParts);
            sideAxis += "{ '" + this.Base + "' base()," + netExp + "}";
            return sideAxis;
            
        }
        public string SummaryAttributeSideAxis()
        {

            // ATTRIBUTE Summary for each BRAND
            // BRIE_M_Loop[..].BRIE_M_GRID > BRIE_M_Loop[{_2}].BRIE_M_GRID[{..}].BRIE_M{ 'Base: All respondents' base(),N1 'Top 2 Box' net({_3 [IsHidden=True], _4 [IsHidden=True]})}

            string[] varParts = DimAxisHelper.GetVarParts(Field);
            // exclude invalid brands:
            DimAxisElements brandElements = new DimAxisElements();
            IEnumerable<Category> validBrands = DimAxisHelper.GetCategories(Container.CategoryMap,false).Where(o => !(InvalidBrandNames.Contains(o)));
            brandElements.Add(validBrands);
            brandElements.AddBaseElement(Base);
            string sideAxis = _Parent.Items.First().Name + "{" + brandElements.ToScript() + "} > ";
            // we're always working with the first instance of this ATTRIBUTE, so the category map refers to brand
            varParts[1] = varParts[1].Replace("{" + Container.CategoryMap.First().Name + "}", "{..}");
            sideAxis += String.Join(".", varParts);
            sideAxis += "{" + Elements.ToScript() + "}";
            return sideAxis;
        }
        public void SetElementsAttribute()
        {
            DimAxisElements top2 = new DimAxisElements();
            top2.Add(((BoundField)Field).Categories().Skip(2), true);
            DimAxisElement net1 = new DimAxisElement()
            {
                ID = "N1",
                Label = "Net top 2",
                Type = ExpressionType.etNet,
                Expression = top2.ToScript()
            };
            Elements.Add(net1);
            Elements.AddBaseElement(Base);
        }
    }
}
