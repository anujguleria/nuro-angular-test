﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimRBX2_M_FT_NDTTable: DimSummaryTable
    {
        public DimRBX2_M_FT_NDTTable(string description, Container c, DimFilter filter): base(null,null,description,null,c,filter)
        {
            Base = "Respondents who rated brand";
            CreateElements();
            Side = SideAxis();
            
            
        }
        public string SideAxis()
        {
            return "axis({" + Elements.ToScript() + "})";
        }
        public void CreateElements()
        {
            Elements = new DimAxisElements();
            //IEnumerable < Category > = Container.CategoryMap.Where(o => DimRBXController.Iterator.Categories().Contains(o));
            List<Category> cats = DimRBXController.Iterator.Categories();
            
            foreach (Category brand in DimRBXController.Iterator.Categories())
            {

                UnboundField xField = DimAxisHelper.GetQuadrantField(Container, brand, "X"); // xFields.ToList().First();
                UnboundField yField = DimAxisHelper.GetQuadrantField(Container, brand, "Y");
                Elements.Add( DimRBXController.BrandBaseElement(brand) );
                Elements.Add( DimRBXController.XMeanElement(brand, xField) );
                Elements.Add( DimRBXController.YMeanElement(brand, yField) );
            }
        }
        


    }
}
