﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimOCC1_BRBenchmarkTable : DimBenchmarkTable
    {

        public DimOCC1_BRBenchmarkTable(string language, string context, string description, BoundField f, Container c, bool brandsInColumns, DimFilter filter) : base(language, context, description, f, c, brandsInColumns, filter)
        {

            AddQTypeMod("AVGBEN");
            Side = OCC1_BRAVGBENSideAxis();
            

        }

        public DimOCC1_BRBenchmarkTable(DimAxisElements summaryElements, string language, string context, string description, BoundField f, Container c, bool brandsInColumns, DimFilter filter) : base(language, context, description, f, c, brandsInColumns, filter)
        {
            AddQTypeMod("AVGBEN");
            SummaryElements = summaryElements;
            AdjustElements();

            Side = SideAxis(AxisType.atAxis);
            
        }
        public void AdjustElements()
        {
            // take the summary elements, ensure all elements are hidden
            // add a derived element summing the valid brands, divided by number of valid brands
            //bmAvg 'Average' derived('(bmOldSpice+bmSecret+bmSpeedStick+bmDegree+bmGillette+bmMitchum+bmAxe+bmBan+bmDove+bmSure+bmOtherS+bmNone)/12') [Decimals=2,CountsOnly=True]
            Elements = new DimAxisElements();
            SummaryElements.RemoveBaseElements();
            foreach(DimAxisElement e in SummaryElements)
            {
                e.Properties[DimAxisElement.pIsHidden] = true;
                Elements.Add(e);
            }
            IEnumerable<DimAxisElement> brandBenchmarks = Elements.Where(o => o.Type == ExpressionType.etDerived);
            Int32 brandCount = brandBenchmarks.Count();
            string avgExpression = "(" + String.Join("+", (from e in brandBenchmarks select e.ID)) + ") / " + brandCount.ToString();
            DimAxisElement benchmark = new DimAxisElement(countsOnly:true,decimals:1)
            {
                ID = "benchmark",
                Label = "Average",
                Type = ExpressionType.etDerived,
                Expression = avgExpression
            };
            Elements.Add(benchmark);
            Elements.AddBaseElement(Base);

        }
        public string OCC1_BRAVGBENSideAxis()
        {
            string sideAxis = "axis({ b '" + Base + "' base(), ";
            Dictionary<string, string> expressions = new Dictionary<string, string>();
            foreach(BoundField f in Container.Items)
            {
                Category cat = Container.CategoryMap[Container.Items.IndexOf(f)];
                string occasion = cat.Label(Context, Language, Container.Document);
                string exp = cat.Name + "'" + occasion +"' expression('";
                exp += f.Name + ".AnswerCount() > 0";
                exp += "') [IsHidden = True] ";
                expressions.Add(cat.Name, exp);
            }
            string benchmark = "avg 'Average number of occasions' derived( '(" + String.Join("+", expressions.Keys.ToList()) + ")";
            benchmark += " / b ";
            benchmark += "' ) [CountsOnly=True,Decimals=1]";
            expressions.Add("avg",benchmark);
            sideAxis += String.Join(", ", expressions.Values) + "})";
            return sideAxis;
        }
        
    }
}
