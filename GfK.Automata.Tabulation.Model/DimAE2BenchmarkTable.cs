﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAE2BenchmarkTable: DimBenchmarkTable
    {
        public DimAxisElements Elements = new DimAxisElements();
        public DimAE2BenchmarkTable(string language, string context, string description, BoundField f, Container c, DimFilter filter) : base(language, context, description, f, c, filter)
        {
            Side = AE2BenchmarkSideAxis();
        }

        public string AE2BenchmarkSideAxis()
        {
            // AE2


            foreach (Category ad in Container.CategoryMap)
            {
                DimAxisElement e = new DimAxisElement();
                e.ID = "e" + Elements.Items.Count.ToString();
                e.Label = ad.Label(Context, Language, Container.Document);
                e.Expression = "LevelID={" + ad.Name + "} AND ([AE1] = {Yes}) AND ([AE2] = [AE2B_Brandname])";
                e.Properties[DimAxisElement.pIsHidden] = true;
                Elements.Add(e);

            }
            DimAxisElement net = new DimAxisElement("benchmark", "Benchmark");
            net.Type = ExpressionType.etDerived;
            net.Expression = "(" + Elements.Join("+") + ") / " + Elements.Items.Count.ToString();
            Elements.Add(net);

            Elements.AddBaseElement(Base);


            string side = Container.Name + "{" + Elements.ToScript() + "}";
            return side;

        }
    }
}
