﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class DimFilterExpressionBuilder
    {
        static DimFilterExpressionBuilder()
        {

        }

        static public string VariableContains(string fieldName, string categoryName, MetadataDocument doc)
        {
            string exp = "";
            try
            {
                BoundField filterVar = (BoundField)doc.Find(fieldName);
                Category cat = (Category)filterVar.Find(categoryName);
                exp = VariableContains(filterVar, cat);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to build filter expression as: {0}.ContainsAny({1})", fieldName, categoryName);
                Console.WriteLine(e.Message);
            }
            return exp;
        }
        static public string VariableContains(string fieldName, string categoryName, IScriptableItem table)
        {

            return VariableContains(fieldName, categoryName, ((DimAggregatedTable)table).Field.Document);

        }
        static public string VariableContains(BoundField f, Category c)
        {

            // returns a simple filter expression where f.ContainsAny({c.Name})

            string exp = "" + f.Name + ".ContainsAny({";
            exp += c.Name + "})";
            return exp;
        }
        static public string VariableContains(BoundField f, List<Category> cats)
        {
            string exp = "";
            exp += "" + f.Name + ".ContainsAny({";
            exp += String.Join(",", (from GfK.Automata.Model.Category c in cats select c.Name)) + "})";

            return exp;
        }

    }

}
