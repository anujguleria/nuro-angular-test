﻿using System;
using System.Linq;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;
using System.Collections.Generic;


namespace GfK.Automata.Tabulation.Model
{
    public class DimTOT_CONDetailTable : DimDetailTable
    {
        public DimTOT_CONDetailTable(string language, string context, string desc, BoundField f, Container c = null) : base(language, context, desc, f, c, null)
        {
            CreateElements(f);
            // Add an element where ABAW_GRID[Brand].ABAW = "Not aware"
            
            Side = SideAxis(AxisType.atAxis);
            FormatDescription("Brand", c);
        }

        public void CreateElements(Field f)
        {
            
            IEnumerable<Category> allCats = DimAxisHelper.GetCategories(f.Items, false);

            List<string> elements = new List<string>();
            Elements.AddBaseElement(Base);
            Elements.Add(new DimAxisElement()
            {
                ID = "n1",
                Label = "Net: Consider (1,2,3)",
                Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, allCats.Take(3))
            });
            Elements.Add(new DimAxisElement()
            {
                ID = "n2",
                Label = "Net: First / Second Choice)",
                Expression = DimAxisHelper.ContainsAnyExpression(Field.Name, allCats.Take(2))
            });
            foreach(Category c in ((BoundField)Field).Categories())
            {
                Elements.Add(new DimAxisElement()
                {
                    ID = "e" + c.Name,
                    Label = c.Label(Context, Language, c.Document, true),
                    Expression = Field.Name + "={" + c.Name + "}"
                });
            }

            Field helperField = (Field)Field.Document.Find("PRIORITY_BRAND_AWARE");
            Category brand = Container.CategoryMap[Container.IndexOf(Field)];
            DimAxisElement notAware = new DimAxisElement()
            {
                ID = "notAware",
                Label = "Not Aware",
                Expression = "NOT " + DimAxisHelper.ContainsAnyExpression(helperField.Name, brand)
            };
            Elements.Add( notAware );
            

        }


    }
}