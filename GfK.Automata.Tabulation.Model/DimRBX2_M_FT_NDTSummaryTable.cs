﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimRBX2_M_FT_NDTSummaryTable: DimSummaryTable, IQuadrantSummaryTable
    {

        public DimRBX2_M_FT_NDTSummaryTable(string description, DimAxisElements elements, Int32 x, Int32 y, Container container, DimFilter filter): base(null,null,description,null, container,filter)
        {
            // Elements are created during the preceding brand detail tables, and passed in the constructor
            Elements = elements;
            Base = "Respondents who rated the brand";
            UpdateElements();
            Side = SideAxis();
            Description = String.Format(Description, x, y);
            Annotations.Add(new DimAnnotation(AnnotationType.atFooter, "RBX Score = [% Quadrant 2 (Memorable & Positive) - % Quadrant 1 (Memorable & Negative)]"));
        }
        public string SideAxis()
        {
            return SideAxis(AxisType.atAxis);
        }
        public void UpdateElements()
        {
            // modifies the elements received from the detail table, hiding rows that need hidden, etc
            IEnumerable<DimAxisElement> hidden = Elements.Where(e => e.Type != ExpressionType.etDerived);
            foreach(DimAxisElement e in Elements)
            {
                if (e.Type != ExpressionType.etDerived)
                { e.Properties[DimAxisElement.pIsHidden] = true; }
                
            }
            Elements.AddBaseElement(Base);
        }
    }
}
