﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimBRIA_MBenchmarkTable: DimBenchmarkTable
    {
        public DimBRIA_MBenchmarkTable(string language, string context, string desc, Field f, Container c, bool brandsInColumns, DimFilter filter): base(language,context,desc,f,c,brandsInColumns,filter)
        {
            // handled by the base class except for explicilty giving the table title from attribute
            string attributeLabel = DimAxisHelper.GetLabel(Container.CategoryMap[Container.IndexOf(Field)]);
            FormatDescription("Benchmarks", attributeLabel);
            Side += " '" + attributeLabel + "'";
            
        }
    }
}
