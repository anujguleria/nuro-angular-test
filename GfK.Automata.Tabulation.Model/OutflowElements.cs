﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public static class OutflowElements
    {
        public static BoundField SW4 { get; set; }
        public static BoundField DurSeg { get; set; }
        static OutflowElements()
        {

        }
        private static DimAxisElements _elements = new DimAxisElements();
        public static DimAxisElements GetElements()
        { return _elements; }
        public static void Init(BoundField sw, BoundField durseg)
        {
            SW4 = sw;
            DurSeg = durseg;
            if ((SW4 is null) || (DurSeg is null)) { return; };
            _elements = new DimAxisElements();
            _elements.Add(new DimAxisElement()
            {
                ID = "_out",
                Label = "Outflow - Likely to leave",
                Expression = "(" + DurSeg.Name + "={_3} AND " + SW4.Name + "={_2}) OR " + SW4.Name + "={_3}"
            });
            string inExp = _inflowExpression();
            _elements.Add(new DimAxisElement()
            {
                ID = "_in",
                Label = "Likely to stay",
                Expression = inExp
            });
            
        }
        private static string _inflowExpression()
        {
            string inExp = "";
            inExp += "(" + DurSeg.Name + "={_1} AND " + SW4.Name + ".ContainsAny({_1,_2}))";
            inExp += " OR ";
            inExp += "(" + DurSeg.Name + "={_2} AND " + SW4.Name + ".ContainsAny({_1,_2}))";
            inExp += " OR ";
            inExp += "(" + DurSeg.Name + "={_3} AND " + SW4.Name + ".ContainsAny({_1}))";
            inExp += " OR ";
            inExp += SW4.Name + "={_9}";
            return inExp;
        }
    }
   
    
}
