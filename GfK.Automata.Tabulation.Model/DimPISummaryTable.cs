﻿using System;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Service
{
    public class DimPISummaryTable: DimSummaryTable
    {
        private Int32 _topCount = 2;
        private bool _sortDesc = true;
        private bool _hideCats = true;

        public DimPISummaryTable(string language, string context, string desc, Field f, Container c = null) : base(language, context, desc, f, c, null)
        {
            Base = "Aware of brand";
            Side = SummarySideAxis(c, _topCount, _hideCats, _sortDesc);
            //SetTabulationLevel();
        }

    }
}