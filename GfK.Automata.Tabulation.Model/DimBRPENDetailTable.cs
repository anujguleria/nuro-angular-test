﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    //language, context, "", field, null, Filters["Version2"])
    public class DimBRPENDetailTable : DimDetailTable
    {
        public DimBRPENDetailTable(string language, string context, string description, BoundField field, Container container, DimFilter filter) : base(language, context, description, field, container, filter)
        {
            // SideAxis is generated from the base class
            DimFunnelTable.GetFunnelRows(this, field);
        }

    }


}
