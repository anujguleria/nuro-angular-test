﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimNumOwn_TDetailTable: DimDetailTable
    {
        public DimNumOwn_TDetailTable(string language, string context, string desc, UnboundField f, Container c, DimFilter filter) : base(language, context, desc, f, c, filter)
        {
            AddQTypeMod("NUM");
            Side = NumOwn_TNumSideAxis();
            GetFunnelRows();
        }
        public string NumOwn_TNumSideAxis()
        {
            // NUMOWN_T_GRID > NUMOWN_T_GRID[].NUMOWN_T{ b 'Base' base(), e0 'Not owned' expression('NUMOWN_T=0'), 'Owned' expression('NUMOWN_T>0')}
            // recodes in to 0 = not owned, 1 = owned
            string[] varParts = Field.Name.Split('.');
            List<string> expressions = new List<string>();
            // get rid of the Level/index from varParts[0]
            string contName = varParts[0].Substring(0, varParts[0].IndexOf("["));
            string sideAxis = contName + " > " + contName + "." + varParts[1] + "{";
            expressions.Add("b '" + Base + "' base()");
            expressions.Add("owned 'Owned brand' expression('" + varParts[1] + ">0 Or " + varParts[1] + " Is Null')");
            expressions.Add("notowned 'Not owned' expression('" + varParts[1] + "=0')");
            sideAxis += String.Join(",", expressions) + "}";
            return sideAxis;
        }
        public void GetFunnelRows()
        {
            string[] varParts = Field.Name.Split('.');
            foreach(Category cat in Container.CategoryMap)
            {
                string sliceName = Container.Name + "[" + cat.Name + "]." + varParts.Last();
                
                DimAxisElement e = new DimAxisElement();
                e.ID = cat.Name;
                e.Label = "Own Brand";
                string exp = sliceName + " > 0 Or " + sliceName + " Is Null";
                e.Expression = exp;
                FunnelRows.Add(cat, e);
            }
        }
    }
}
