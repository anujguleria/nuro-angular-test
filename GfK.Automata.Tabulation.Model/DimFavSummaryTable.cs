﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimFavSummaryTable : DimSummaryTable
    {
        private Int32 _topCount = 2;
        private bool _hideCats = true;
        private bool _sortDesc = true;
        public DimFavSummaryTable(string language, string context, string desc, Field f, Container c = null) : base(language, context, desc, f, c, null)
        {
            Base = "Aware of brand";
            Side = SummarySideAxis(c, _topCount, _hideCats, _sortDesc);
            AppendToFunnel(FunnelRows, "Favorability");
            //SetTabulationLevel();
        }

    }
}
