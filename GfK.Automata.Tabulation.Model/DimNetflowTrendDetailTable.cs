﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimNetflowTrendDetailTable: DimDetailTable
    {
        public DimNetflowTrendDetailTable(string desc, Category brand, BoundField f, DimFilter filter):base(desc, brand, f, filter)
        {
            Base = "Total respondents";
            Elements = CreateElements((BoundField)Field);
            Side = SideAxis();
            Description += " - " + DimAxisHelper.CleanText(brand.Label(Context, Language, brand.Document, true));
        }

        public string SideAxis()
        {
            return Field.Name + "{" + Elements.ToScript() + "}";
        }
    }
}
