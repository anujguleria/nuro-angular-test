﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Model
{
    public class DimTableAnnotations: List<DimAnnotation>, IEnumerable<DimAnnotation>
    {
        public DimTableAnnotations(): base()
        {

        }
        public new void Add(DimAnnotation itm)
        {   
            if ((from o in this select o.Index).Contains(itm.Index))
            {
                string msg = "Annotations[" + itm.Index.ToString() + "] already exists!";
                throw new ArgumentException(msg);
            }
            base.Add(itm);
        }
    }
}
