﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Model
{
    public class DimAE1AE2DetailTable: DimDetailTable
    {

        // IF AE_Loop[AEcat].AE2B_BrandName <> AE_Loop[AEcat].AE2 THEN AE_Loop[AEcat].AE2B.ask() 
        // That AE2B question is only shown if they get the brand wrong.
        // So I think you can write expressions for each thing in AELoop

        // helper variables needed in this table:
        public BoundField AE2 { get; set; }
        public BoundField AE2B_Brandname { get; set; }

        public DimAE1AE2DetailTable(string language, string context, string desc, BoundField f, BoundField ae2, BoundField ae2BrandName, Container c = null, DimFilter filter = null) : base(language, context, desc, f, c, filter)
        {
            AE2 = ae2;
            AE2B_Brandname = ae2BrandName;
            Side = AE1AE2SideAxis();
            FormatDescription("Ad", c);
        }
        public string AE1AE2SideAxis()
        {
            /*
            AE_Loop[{ Ad1}]{
                b 'Base' base(),
                _recall 'Recall Ad' expression('[AE1]={_1}'),
                _cbr 'Correct Brand Recall' expression('[AE2] = [AE2B_Brandname]'),
                _ibr 'Incorrect Brand Recall' expression('[AE2] <> [AE2B_Brandname]'),
                ... 
                    list of incorrectly chosen brands 
                ...
                _dnr 'Do not recall ad' expression('[AE1]={_2}')
                }
            */
            DimAxisElements elements = new DimAxisElements();
            DimAxisElement e = null;
            string[] varParts = Field.Name.Split('.');
            string var = "[" + varParts.Last() + "]";
            elements.AddBaseElement(Base);
            // Recall ad
            e = new DimAxisElement("_recall", "Recall Ad");
            e.Expression = "[AE1] = {Yes}";
            elements.Add(e);
            // Correct Brand Recall
            e = new DimAxisElement("_cbr", "Correct Brand Recall");
            e.Expression = "([AE1] = {Yes}) AND ([AE2] = [AE2B_Brandname])";
            elements.Add(e);

            // Incorrect Brand Recall
            DimAxisElements incorrectItems = _getIncorrectBrands();
            DimAxisElement incorrectNet = new DimAxisElement();
            incorrectNet.ID = "_ibr";
            incorrectNet.Label = "Incorrect Brand Recall";
            incorrectNet.Type = ExpressionType.etNet;
            incorrectNet.Expression = incorrectItems.ToScript();
            elements.Add(incorrectNet);

            // Do not recall ad
            e = new DimAxisElement("_dnr", "Do not recall ad");
            e.Expression = "[AE1]={No}";
            elements.Add(e);

            String side = varParts[0] + "{" + elements.ToScript() + "}";

            return side;
        }
        private DimAxisElements _getIncorrectBrands()
        {
            DimAxisElements netElements = new DimAxisElements();
            List<Category> brandList = AE2.Categories().ToList();
            foreach (Category brand in AE2.Categories())
            {
                if (InvalidBrandNames.Contains(brand)) { continue; }
                Int32 labIndex = AE2.Categories().IndexOf(brand);
                DimAxisElement b = new DimAxisElement("i" + labIndex.ToString());
                b.Label = brand.Label(Context, Language, Container.Document, true);
                b.Expression = "([AE2] = {" + brand.Name + "} And [AE2B_BrandName] <> {" + brand.Name + "})";
                netElements.Add(b);
            }
            return netElements;
        }
        public static Dictionary<string, BoundField> HelperVariables(Container Container, Field Field)
        {
            string ad = "";
            BoundField AE2 = null;
            BoundField AE2B_Brandname = null;
            try
            {
                Int32 level = -1 + ((Container.IndexOf(Field) + 1) % (Container.Items.Count / Container.CategoryMap.Count));
                ad = "{" + Container.CategoryMap[level].Name + "}";
            }
            catch (ArgumentOutOfRangeException e)
            {
                // if this level doesn't exist, we 
                Console.WriteLine(String.Format("Container:\t{0}\nField:\t{1}", Container.Name, Field.Name));
            }
            
            AE2 = ((BoundField)Container.Find(Container.Name + "[" + ad + "].AE2"));
            AE2B_Brandname = ((BoundField)Container.Find(Container.Name + "[" + ad + "].AE2B_BrandName"));
            return new Dictionary<string, BoundField>()
            {
                {"AE2",AE2 },
                {"AE2B_BrandName", AE2B_Brandname }
            };
        }
    }
}
