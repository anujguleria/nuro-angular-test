﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfK.Automata.Pipeline.API.ViewModels
{
    public class ProjectViewModel
    {
        public string Name { get; set; }
        public int ProjectID { get; set; }
        public ICollection<ProjectUserViewModel> projectUsers { get; set; }
        public ICollection<PipelineViewModel> pipeLines { get; set; }
    }
}