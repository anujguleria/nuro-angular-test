﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfK.Automata.Pipeline.API.ViewModels
{
    public class ProjectUserViewModel
    {
        public int ProjectID { get; set; }
        public int UserID { get; set; }
        public DateTime LastAccess { get; set; }
        public virtual ProjectViewModel Project { get; set; }
        public virtual UserViewModel User { get; set; }
    }
}