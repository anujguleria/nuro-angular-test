﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfK.Automata.Pipeline.API.ViewModels
{
    public class PipelineInstanceViewModel
    {
        public int PipelineInstanceId { get; set; }
        public DateTime Started { get; set; }
        public DateTime? Ended { get; set; }
        public int PipelineId { get; set; }
        public PipelineViewModel Pipeline { get; set; }
    }
}