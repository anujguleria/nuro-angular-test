﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfK.Automata.Pipeline.API.ViewModels
{
    public class PipelineViewModel
    {
        public int PipeLineID { get; set; }
        public string Name { get; set; }
        public int ProjectID { get; set; }
        public virtual ProjectViewModel project { get; set; }
        public ICollection<PipelineInstanceViewModel> PipelineInstances { get; set; }
    }
}