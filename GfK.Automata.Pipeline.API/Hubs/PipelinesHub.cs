﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Hubs;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service;

namespace GfK.Automata.Pipeline.API.Hubs
{
    public class PipelinesHub : AutoHub
    {
        private string prefix = "pipelineID_";
        public PipelinesHub()
        {
            base.idPrefix = prefix;
        }
      
        public void PushNotification(int who, Object message)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<PipelinesHub>();
            base.PushNotification(context,who, message);
            
        }
    }
    
}

