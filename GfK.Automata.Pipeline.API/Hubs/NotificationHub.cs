﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.API.Identity;

namespace GfK.Automata.Pipeline.API.Hubs
{
    public class NotificationHub : AutoHub
    {
        private string prefix = "NotificationId_";
        public NotificationHub()
        {
            base.idPrefix = prefix;
        }
      
        public async Task PushNotification(string email, Object notification)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            base.PushNotification(context, email, notification);
        }
    }
}