﻿using GfK.Automata.Pipeline.API.Identity;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.Hubs
{
    public class AutoHub : Hub
    {
        protected readonly static ConnectionMapping<string> _contexts = new ConnectionMapping<string>();
        protected string idPrefix;
        public string Id
        {
            get
            {
                return idPrefix + this.Context.QueryString["id"];
            }
        }

        public override Task OnConnected()
        {
            _contexts.Add(Id, Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _contexts.Remove(Id, Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            if (!_contexts.GetConnections(Id).Contains(Context.ConnectionId))
            {
                _contexts.Add(Id, Context.ConnectionId);
            }
            return base.OnReconnected();
        }

        public virtual void PushNotification(string email, object nofiifcation)
        {

        }

        public virtual void PushNotification(int id, object notification)
        {
        }

        public virtual void PushNotification(IHubContext context, int id, object message)
        {
            foreach (var connectionID in _contexts.GetConnections(idPrefix + id))
            {
                context.Clients.Client(connectionID).newNotification(message);
            }
        }

        public virtual void PushNotification(IHubContext context, string email, object notification)
        {
            context.Clients.User(email).notifyUser(notification);
        }

    }

    public class GenericNotification : Hub
    {
        protected readonly static ConnectionMapping1<string> _contexts = new ConnectionMapping1<string>();
        private string username;
        private readonly IProjectUserService _projectUserService;
        private readonly IUserService _userService;

        public GenericNotification() { }
        public GenericNotification(IProjectUserService projectUserService, IUserService userService)
        {
            _projectUserService = projectUserService;
            _userService = userService;
        }
        public override Task OnConnected()
        {
            username = Context.User.Identity.Name.ToString();
            _contexts.Add(Context.ConnectionId, username);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _contexts.Remove(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            //  username = Context.User.Identity.Name.ToString();
            if (!_contexts.ContainsConnection(Context.ConnectionId))
            {
                _contexts.Add(Context.ConnectionId, username);
            }
            return base.OnReconnected();
        }
        public async Task PushNotification(int? projectId, object notification)
        {
            int projectid = projectId.GetValueOrDefault(-1);
            if (projectid != -1)
            {
                IHubContext context = GlobalHost.ConnectionManager.GetHubContext<GenericNotification>();
                Dictionary<string, string> connectionsUserMap = _contexts.GetConnections();
                foreach (var connectionID in connectionsUserMap.Keys)
                {
                    User user = await _userService.GetUser(connectionsUserMap[connectionID]);
                    List<int> projectIds = _projectUserService.GetProjectsIdsByUserId(user.UserID).Result.ToList();
                    if (projectIds.Contains(projectid))
                        context.Clients.Client(connectionID).newNotification(notification);
                }
            }
        }
    }
}