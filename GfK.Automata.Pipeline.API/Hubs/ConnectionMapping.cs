﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfK.Automata.Pipeline.API.Hubs
{
    public class ConnectionMapping<T>
    {
        private  readonly Dictionary<T, HashSet<string>> _connections =
            new Dictionary<T, HashSet<string>>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }
    }

    public class ConnectionMapping1<T>
    {
        private readonly Dictionary<T, string> _connections =
            new Dictionary<T, string>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string username)
        {
            lock (_connections)
            {
                if(!_connections.ContainsKey(key))
                _connections.Add(key, username);
            }
        }

        public Dictionary<T,string> GetConnections()
        {
            return _connections;
        }

        public bool ContainsConnection(T connectionId)
        {
            return _connections.ContainsKey(connectionId);
        }

        public void Remove(T key)
        {
            lock (_connections)
            {
                _connections.Remove(key);
            }
        }
    }
}