﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.Hubs
{
    public class PipelineInstanceHub : AutoHub
    {

        private string prefix = "pipelineInstanceTaskID_";

        public PipelineInstanceHub()
        {
            base.idPrefix = prefix;
        }
        public void PushNotification(int who, Object message)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<PipelineInstanceHub>();
            base.PushNotification(context, who, message);
        }
    }
}