﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.Hubs
{
    public class PipelineTaskHub:AutoHub
    {
        private string prefix = "pipelineTaskID_";
        public PipelineTaskHub()
        {
            base.idPrefix = prefix;
        }
        public void PushNotification(int who, object message)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<PipelineTaskHub>();
            base.PushNotification(context, who, message);
        }
    }
}