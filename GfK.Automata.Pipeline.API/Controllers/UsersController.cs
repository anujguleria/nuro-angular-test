﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin;
using System.Threading;

using Newtonsoft.Json;

using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.API.Views;
using GfK.Automata.Pipeline.API.Identity;
using Newtonsoft.Json.Linq;
using GfK.Automata.Pipeline.API.HTTPStatusExtensions;
using System.Configuration;
using Microsoft.AspNet.SignalR;
using GfK.Automata.Pipeline.API.Hubs;
using System.Collections.Specialized;
using System.Net.Http.Formatting;

namespace GfK.Automata.Pipeline.API.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IProjectService _projectService;
        private readonly IProjectUserService _projectUserService;
        private readonly IAuthenticationService _authenticationService;
        private readonly INotificationService _notificationService;
        private readonly User _currentlyLoggedInUser;
        private readonly IGapRoleUserService _gapRoleUserService;
        private readonly IGapGroupUserService _gapGroupUserService;

        public UsersController(IUserService userService, IProjectService projectService
            , IAuthenticationService authenticationService, IProjectUserService projectUserService
            , IGapRoleUserService gapRoleUserService, IGapGroupUserService gapGroupUserService, INotificationService notificationService)
        {
            _projectService = projectService;
            _userService = userService;
            _authenticationService = authenticationService;
            _projectUserService = projectUserService;
            _gapRoleUserService = gapRoleUserService;
            _gapGroupUserService = gapGroupUserService;
            _notificationService = notificationService;

            Task<User> currentUserName = _userService.GetUser(_authenticationService.GetCurrentlyLoggedInUserName(User));
            currentUserName.Wait();
            _currentlyLoggedInUser = currentUserName.Result;

            _projectService.CurrentlyLoggedInUser = _currentlyLoggedInUser;
        }

        /// <summary>
        /// Retrieves list of Users with assigned Roles
        /// </summary>
        // GET api/Users
        [Route("api/Users")]
        public async Task<IHttpActionResult> Get()
        {
            IEnumerable<User> Users = await _userService.GetUsersWithRoles();

            return Ok(Users);
        }

        // GET api/Users/5
        public async Task<User> Get(int id)
        {
            User user = new User();

            return user;
        }

        [Route("api/Users/GetCurrentLoggedInUser")]
        public async Task<User> GetCurrentLoggedInUser()
        {
            string username = await AuthenticationUser.GetCurrentlyLoggedInUserNameAsync(User);

            User currentlyLoggedInUser = await _userService.GetUser(username);
            if (currentlyLoggedInUser == null)
            {
                if (username.Contains("@"))
                    currentlyLoggedInUser = await _userService.GetUser(username.Split('@')[0]);
                else
                    currentlyLoggedInUser = await _userService.GetUser(username + "@" + ConfigurationManager.AppSettings["AuthenticationDomain"]);
            }

            if (currentlyLoggedInUser != null)
            {
                currentlyLoggedInUser.GapRoleUsers = (await _gapRoleUserService.GetUserRoles(currentlyLoggedInUser.UserID)).ToList();
                currentlyLoggedInUser.GapGroupUsers = (await _gapGroupUserService.GetUserGroupsBrief(currentlyLoggedInUser.UserID)).ToList();
            }
            return currentlyLoggedInUser;
        }

        /// <summary>
        /// Retrieves user and groups to which assigned
        /// </summary>
        /// <param name="id">User ID</param>
        /// <response code="404">User does not exist.</response>
        // GET api/Users/5/Groups
        [Route("api/Users/{id}/Groups")]
        public async Task<IHttpActionResult> GetWithGroups(int id)
        {
            User user = await _userService.GetUserWithGroups(id);

            if (user == null)
            {
                return this.NotFound("User " + id.ToString() + " does not exist.");
            }
            else
            {
                return Ok(user);
            }
        }

        /// <summary>
        /// Retrieves the list of all Projects assigned the currently logged in User
        /// </summary>
        /// <returns></returns>

        // api/Users/Projects
        public async Task<IHttpActionResult> GetProjects()
        {
            User currentlyLoggedInUser = await _userService.GetUser(await AuthenticationUser.GetCurrentlyLoggedInUserNameAsync(User));
            IEnumerable<Project> projects = await _projectService.GetUserProjects();

            return Ok(projects);
        }

        /// <summary>
        /// Creates a new user and assigns roles
        /// </summary>
        /// <remarks>
        /// A user and their assigned roles are created. The users email address and role ids are all that is required.
        /// 
        ///     {
        ///         "Email": "newcreate&gfk.com",
        ///         "GapRoleUsers": [
        ///                             {
        ///                                 "GapRoleID": 1,
        ///                             },
        ///                             {
        ///                                 "GapRoleID": 3,
        ///                             }
        ///                         ]
        ///     }
        /// </remarks>
        /// <param name="user">New User</param>
        /// <returns>Returns the ID of newly created user</returns>
        /// <response code="409">User with email already exists</response>
        [HttpPost]
        [Route("api/Users")]
        public async Task<IHttpActionResult> Post([FromBody]User user)
        {
            int newUserId = await _userService.CreateUser(user);

            if (newUserId == -2)
            {
                return this.Conflict("User " + user.Email + " already exists.");
            }

            return Created("", newUserId);
        }

        /// <summary>
        /// Validates a users credentials against AD, returning bearer token if valid.
        /// </summary>
        /// <remarks>
        /// The API validates a username/password against Active Directory.  Usernames may optionally include the 
        /// domain (e.g. @gfk.com).  If the credentials validate, a bearer token is returned.  If a user is
        /// logging in for the first time a User is created in the database.
        /// <code>
        ///     {
        ///         "Username": "michael.oconnell",
        ///         "Password": "MyPwd"
        ///     } 
        /// </code>
        /// 
        /// Response to a successful validation
        ///  <code>
        ///     {
        ///         "access_token": "MOttWY84gKJVaKlVflEK905YyolpXP1 ... DTAKPllcwtRM6F7", 
        ///         "UserId": "6"   
        ///     } 
        ///  </code>
        /// 
        ///  Response to an unsuccessful validation
        /// 
        ///     {
        ///         "Message": "Error - invalid_grant - Incorrect user or password" 
        ///     } 
        /// </remarks> 
        /// <response code="400">Authentication failed</response>

        // POST api/Users/Login
        [Route("api/Users/Login")]
        public async Task<IHttpActionResult> Post([FromBody]LoginViewModel value)
        {
            AuthenticationTokenRequestResponse response = await AuthenticationToken.GetToken(value);

            // AuthenticationTicket needed?
            if (!(await AuthenticationToken.RequestResponseIsOK(response)))
            {
                return BadRequest(await AuthenticationToken.BadRequestResponse(response));
            }
            else
            {
                Dictionary<string, string> currentUserId = new Dictionary<string, string>();
                User currentLoggedInUser = await _userService.GetUser(await AuthenticationToken.GetADUserName(value));

                if (currentLoggedInUser != null)
                {
                    currentUserId.Add("UserId", currentLoggedInUser.UserID.ToString());
                }
                else
                {
                    int newUserID = await _userService.CreateUser(await AuthenticationToken.GetADUserName(value));
                    currentUserId.Add("UserId", newUserID.ToString());
                }

                return Ok(await AuthenticationToken.OKResponse(response, currentUserId));
            }
        }
        [Route("api/Users/LastAccess/{projectID}")]
        public async Task<IHttpActionResult> Post(int projectID)
        {
            DateTime updatedTime = await _projectUserService.UpdateProjectUser(_currentlyLoggedInUser.Email, projectID);
            return Ok(updatedTime);
        }

        /// <summary>
        /// Update a user including groups/roles
        /// </summary>
        /// <param name="id">User ID</param>
        /// <remarks>
        /// Minimally required are the UserID, Email, for each GapGroupUsers the GapGroupID and UserID, and for GapRoleUsers
        /// GapRoleID and UserID.  Full GapGroups (as below) or GapRoles can be included but are not necessary.
        /// 
        /// {
        /// "UserID": 10,
        /// "Email": "michael.oconnell@gfk.com",
        /// "projectUsers": [],
        /// "GapGroupUsers": [
        /// {
        /// "GapGroup": {
        /// "Name": "DefaultGroupMike",
        /// "GapGroupID": 1,
        /// "Projects": null,
        /// "GapGroupUsers": []
        /// },
        /// "GapGroupID": 1,
        /// "UserID": 10
        /// }
        /// ],
        /// "GapRoleUsers": null
        /// }
        /// </remarks>
        /// <returns></returns>
        /// <response code="404">User ID is invalid</response>
        // PUT api/Users/5
        public async Task<IHttpActionResult> Put(int id, [FromBody]User value)
        {
            try
            {
                if (value.Email == _currentlyLoggedInUser.Email)
                {
                    await _userService.UpdateCurrentUser(_currentlyLoggedInUser, value);
                }
                else
                {
                    await _userService.UpdateUser(value);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Store update, insert, or delete statement affected an unexpected number of rows (0)"))
                {
                    return this.NotFound("No user with Id " + id.ToString() + " appears to exist.");
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Delete user {id}
        /// </summary>
        /// <param name="id"> Id of user to delete</param>
        /// <returns> OK (200) upon successful delete</returns>
        /// <response code="401">Current user does not have privilege</response>
        /// <response code="404">User ID is invalid</response>
        // DELETE api/Users/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (!(await _userService.IsUserAdmin(_currentlyLoggedInUser.UserID)))
                {
                    return Unauthorized();
                }
                else
                {
                    User user = await _userService.GetUser(id);
                    if (user == null)
                    {
                        return Content(HttpStatusCode.NotFound, "User with id " + id.ToString() + " not found.");
                    }
                    else
                    {
                        await _userService.DeleteUser(user);
                        return Ok();
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [Route("api/Users/LoggedInUser/Notifications")]
        public async Task<IHttpActionResult> GetNotifications()
        {
            return Ok(await _notificationService.GetNotifications(_currentlyLoggedInUser.UserID));
        }

        [Route("api/Users/LoggedInUser/Notifications")]
        public async Task<IHttpActionResult> DeleteAllNotification()
        {
            await _notificationService.DeleteNotificationOfUser(_currentlyLoggedInUser.UserID);
            return Ok();
        }

        [Route("api/Users/LoggedInUser/Notifications/{notificationID}")]
        public async Task<IHttpActionResult> DeleteNotification(int notificationID)
        {
            await _notificationService.DeleteNotification(notificationID);
            return Ok();
        }

        [Route("api/Users/{email}/Notifications")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> PostNotification(string email, [FromBody] Notification notification)
        {
            NotificationHub hub = new NotificationHub();
            User user = await _userService.GetUser(email);
            notification.user = user;
            await hub.PushNotification(email, notification);
            Notification created = await _notificationService.CreateNotification(notification);
            return Created("", created);
        }
        /// <summary>
        /// This api takes a mapping of notificationd IDs and user IDs then it fetch the notifications and send out to the specified users
        /// </summary>
        /// <remarks>
        /// 
        /// <code>
        ///     {
        ///         "22": "56"
        ///     } 
        /// </code>
        /// 
        /// <response code="200">Notifications have been successfully sent to logged in users usign signalR connection</response>
        /// </remarks> 
        /// <response code="400">Authentication failed</response>

        [HttpPost]
        [Route("api/Users/PipelineNotifications")]
        [AllowAnonymous]
        public async Task PostPipelineNotification(FormDataCollection UserIDNotificationIDMapping)
        {
            NameValueCollection MyNameValueCollection = UserIDNotificationIDMapping.ReadAsNameValueCollection();
            foreach (string  notificationID in MyNameValueCollection.Keys)
            {
                NotificationHub hub = new NotificationHub();
                int userID = Convert.ToInt32(UserIDNotificationIDMapping.Get(notificationID));
                User user = await _userService.GetUser(userID);
                int notificationIDInt = Convert.ToInt32(notificationID);
                Notification notification = await _notificationService.GetNotificationByID(notificationIDInt);
                notification.user = null;
                await hub.PushNotification(user.Email, notification);
            }
        }
    }
}