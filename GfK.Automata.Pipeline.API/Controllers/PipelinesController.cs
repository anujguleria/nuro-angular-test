﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.API.Identity;
using GfK.Automata.Pipeline.API.HTTPStatusExtensions;
using GfK.Automata.Pipeline.API.HTTPRequestExtensions;
using GfK.Automata.Pipeline.API.HTTPRequestHeaderValidation;
using System.Collections.ObjectModel;
using GfK.Automata.Pipeline.Data.Service.Dto;
using GfK.Automata.Pipeline.API.Hubs;
using System.Web.Http.OData;


namespace GfK.Automata.Pipeline.API.Controllers
{
    public class PipelinesController : ApiController
    {
        private readonly IPipelineService _pipelineService;
        private readonly IPipelineInstanceService _pipelineInstanceService;
        private readonly IUserService _userService;
        private readonly IPipelineTaskService _pipelineTaskService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IScheduleService _scheduleService;
        private readonly IGapGroupUserPipelineService _gapGroupUserPipelineService;
        private readonly IPipelineRunRequestService _pipelineRunRequestService;
        private readonly User _currentlyLoggedInUser;
        private readonly IProjectUserService _projectUserService;
        private readonly IPipelineNotificationSettingsService _pipelineNotificationSettingsService;

        public PipelinesController(IPipelineService pipelineService, IPipelineInstanceService pipelineInstanceService, IUserService userService
            , IPipelineTaskService pipelineTaskService, IAuthenticationService authenticationService, IScheduleService scheduleService
            , IGapGroupUserPipelineService gapGroupUserPipelineService, IPipelineRunRequestService pipelineRunRequestService, IProjectUserService projectUserService,
            IPipelineNotificationSettingsService pipelineNotificationSettingsService)
        {
            _pipelineInstanceService = pipelineInstanceService;
            _pipelineService = pipelineService;
            _userService = userService;
            _pipelineTaskService = pipelineTaskService;
            _authenticationService = authenticationService;
            _scheduleService = scheduleService;
            _gapGroupUserPipelineService = gapGroupUserPipelineService;
            _pipelineRunRequestService = pipelineRunRequestService;
            _projectUserService = projectUserService;
            _pipelineNotificationSettingsService = pipelineNotificationSettingsService;

            Task<User> currentUserName = _userService.GetUser(_authenticationService.GetCurrentlyLoggedInUserName(User));
            currentUserName.Wait();
            _currentlyLoggedInUser = currentUserName.Result;

            _pipelineService.CurrentlyLoggedInUser = _currentlyLoggedInUser;
        }

        private async Task<IHttpActionResult> PipelineAccessibility(int pipelineID)
        {
            IHttpActionResult result = Ok();
            Model.Pipeline pipeline = await _pipelineService.GetPipeline(pipelineID);

            if (pipeline == null)
            {
                result = this.NotFound("Pipeline " + pipelineID.ToString() + " does not exist.");
            }
            else if (!await _pipelineService.UserCanAccessPipeline(pipeline))
            {
                result = Unauthorized();
            }

            return result;
        }

        /// <summary>
        /// Retrieves the list of all Pipelines
        /// </summary>
        /// <returns></returns>

        // GET api/Pipelines
        public async Task<IHttpActionResult> Get()
        {
            IEnumerable<Model.Pipeline> pipelines = await _pipelineService.GetPipelines();

            return Ok(pipelines);
        }

        /// <summary>
        /// Returns pipeline {id}
        /// </summary>
        /// <param name="id">Id of Pipeline to retrieve</param>
        /// <response code="200">Pipeline information</response>
        /// <response code="401">Pipeline is not accessible to user.</response>
        /// <response code="404">Pipeline ID was not found</response>

        // GET api/Pipelines/5
        [Route("api/Pipelines/{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            Model.Pipeline pipeline = await _pipelineService.GetPipeline(id);
            return Ok(pipeline);
        }
        [Route("api/Pipelines/PipelineInstances/{id}")]
        public async Task<IHttpActionResult> GetPipelineInstance(int id)
        {
            PipelineInstance instance = await _pipelineInstanceService.GetPipelineInstance(id);
            return Ok(instance);
        }
        [Route("api/Pipelines/PipelineInstances/{id}/Logs")]
        public async Task<IHttpActionResult> GetPipelineInstanceLogs(int id)
        {
            PipelineInstance instance = await _pipelineInstanceService.GetPipelineInstanceWithLogs(id);
            return Ok(instance);
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetPipeline(int id)
        {
            Model.Pipeline pipeline = await _pipelineService.GetPipeline(id);

            if (pipeline == null)
            {
                return this.NotFound("Pipeline " + id.ToString() + " does not exist.");
            }
            else if (!await _pipelineService.UserCanAccessPipeline(pipeline))
            {
                return Unauthorized();
            }

            return Ok(pipeline);
        }

        [Route("api/Pipelines/{id}/Schedule")]
        public async Task<IHttpActionResult> GetSchedule(int id)
        {
            // TODO - figure out a better way to do this for now we're just mapping to / from local time
            Schedule schedule = (await _pipelineService.GetPipeline(id)).Schedule;
            if (schedule != null)
            {
                TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
                DateTime utc = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime, tz);
                //schedule.StartTime = new DateTime(utc.Year, utc.Month, utc.Day, utc.Hour, utc.Minute, utc.Second);
            }
            return Ok(schedule);
        }
        [Route("api/Pipelines/{id}/Schedule")]
        public async Task<IHttpActionResult> PostSchedule(int id, [FromBody]Schedule schedule)
        {
            // TODO - figure out a better way to do this for now we're just mapping to / from local time
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            //schedule.StartTime = TimeZoneInfo.ConvertTimeFromUtc(schedule.StartTime, tz);
            Model.Pipeline pipeline = await _pipelineService.GetPipelineNoTracking(id);

            if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(pipeline, _currentlyLoggedInUser))
            {
                return Unauthorized();
            }
            else
            {
                if (pipeline.Schedule == null)
                {
                    schedule.ScheduleID = await _scheduleService.CreateSchedule(schedule);
                    pipeline.Schedule = schedule;
                    await _pipelineService.UpdatePipeline(pipeline);
                }
                else
                {
                    schedule.ScheduleID = pipeline.Schedule.ScheduleID;
                    await _scheduleService.UpdateSchedule(schedule);
                }

                return Ok(schedule);
            }
        }
        [Route("api/Pipelines/Schedules/GetTimezones")]
        public async Task<IHttpActionResult> GetTimeZones()
        {
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();

            return Ok(timeZones);
        }

        /// <summary>
        /// Runs pipeline {id}
        /// </summary>
        /// <remarks>
        /// Submits a request to execute a pipeline.  The Automata service responsible for running pipelines monitors for
        /// these run requests. Requestors can specify the context in which pipeline is to be run. Options are : "None" - run only
        /// {id}, "Parent" (default) - run {id} and all its parents, "Children" - run {id} and all its children.  
        /// 
        ///     {
        ///         RunPipelineContext : "Parent"
        ///     }
        ///     
        /// </remarks>
        /// <param name="context" required="false"></param>
        /// <response code="201">Pipeline run request successfully added</response>
        /// <response code="400">Typically indicates request body is not correctly formatted.</response>
        /// <response code="401">Pipeline is not accessible to user.</response>

        [Route("api/Pipelines/{id}/Run")]
        [HttpPost]
        public async Task<IHttpActionResult> PostPipelineRunRequest(int id, [FromBody]PipelineRunRequestContext context)
        {
            if (!await _pipelineService.UserCanAccessPipeline(id))
            {
                return Unauthorized();
            }
            else
            {
                await _pipelineService.RunPipeline(id, context);
                return Created("Submitted", id);
            }
        }

        /// <summary>
        /// Retrieves a pipeline by name.
        /// </summary>
        /// <remarks>
        ///  
        /// A pipeline name is resolved within the context of a project and, optionally, a parent pipeline.  Consequently requests to identify
        /// a pipeline by {name} minimally require a project id be provided.  The body provides JSON indicating
        /// the 'context' within which the pipeline name is to be resolved.
        /// 
        /// NOTE - the ProjectID must reference a project to which the currently logged in user has access.
        /// 
        /// Given the URL ... api/Pipelines/ByName/P1 and the body as below, the api will see if there is a pipeline named P1 directly
        /// associated with project 22
        /// 
        ///     {
        ///         "ProjectID": 22,
        ///         "ParentID": null
        ///     }
        /// 
        /// or its equivalent
        /// 
        ///     {
        ///         "ProjectID": 22
        ///     }
        ///     
        /// Given the URL ... api/Pipelines/ByName/P2 and the body as below, the api will see if there is a pipeline named P2 dependent
        /// on pipelineId 6.  The parent pipeline does not itself need to be directly associated with the project.
        /// 
        ///     {
        ///         "ProjectID": 22,
        ///         "ParentID": 6
        ///     }
        /// 
        /// </remarks> 
        /// <response code="200">A pipeline by {name} exists.</response>
        /// <response code="400">Typically indicates request body is not correctly formatted.</response>
        /// <response code="404">Pipeline not found, name may not be in use or Project is not accessible to user.</response>
        /// 
        // POST api/Pipelines/ByName/{name}
        [Route("api/Pipelines/ByName/{name}")]
        [HttpPost]
        public async Task<IHttpActionResult> PostByName(string name, [FromBody]object context)
        {
            dynamic requestValues = await PipelineHttpRequestValidator.GetRequestByName(context);

            if (await HttpRequestValidator.IsError(requestValues))
            {
                return BadRequest(await HttpRequestValidator.GetError(requestValues));
            }
            else
            {
                Model.Pipeline pipeline = await _pipelineService.GetPipeline(name, requestValues);

                if (pipeline == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(pipeline);
                }
            }
        }

        /// <summary>
        /// Retrieves the list of Pipeline Instances for a Pipeline
        /// </summary>
        /// <remarks>
        /// 
        /// Paging can be achieved with the $skip and $top parameters. For example to
        /// get the first ten rows append ?$skip=0&amp;$top=10 to the api request. To
        /// get the next ten rows append ?$skip=10&amp;$top=10
        /// 
        ///     api/Pipelines/102/PipelineInstances?$skip=0&amp;$top=10
        ///     api/Pipelines/102/PipelineInstances?$skip=10&amp;$top=10
        ///     
        /// 
        /// </remarks>
        /// <returns></returns>
        /// <param name="id">Pipeline ID</param>
        /// <response code="401">Pipeline is not accessible to user.</response>

        // GET api/Pipelines/{id}/PipelineInstances
        [EnableQuery]
        [Route("api/Pipelines/{id}/PipelineInstances")]
        public async Task<IHttpActionResult> GetPipelineInstances(int id)
        {
            Model.Pipeline pipeline = await _pipelineService.GetPipeline(id);

            if (!await _pipelineService.UserCanAccessPipeline(id))
            {
                return Unauthorized();
            }
            else
            {
                IEnumerable<PipelineInstance> pipelineInstances = (await _pipelineInstanceService.GetInstancesForPipeline(id));

                return Ok(pipelineInstances.AsQueryable());
            }
        }

        /// <summary>
        /// Retrieves the list of Tasks for a Pipeline
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Pipeline ID</param>
        /// <response code="401">Pipeline is not accessible to user.</response>
        /// <response code="404">Pipeline ID was not found</response>

        // GET api/Pipelines/{id}/PipelineTasks
        [Route("api/pipelines/{id}/PipelineTasks")]
        public async Task<IHttpActionResult> GetPipelineTasks(int id)
        {
            Model.Pipeline pipeline = await _pipelineService.GetPipeline(id);

            if (pipeline == null)
            {
                return NotFound();
            }
            else if (!await _pipelineService.UserCanAccessPipeline(pipeline))
            {
                return Unauthorized();
            }
            else
            {
                IEnumerable<PipelineTask> pipelineTasks = await _pipelineTaskService.GetPipelineTasks(id);

                return Ok(pipelineTasks);
            }
        }

        /// <summary>
        /// Retrieves a Pipeline Task with {name} in Pipeline {id}
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Pipeline ID</param>
        /// <param name="name">PipelineTask Name</param>
        /// <response code="401">Pipeline is not accessible to user.</response>
        /// <response code="404">Pipeline does not exist.</response>
        /// <response code="404">Pipeline Task with {name} was not found</response>

        // GET api/Pipelines/{id}/PipelineTasks/ByName/{name}
        [Route("api/Pipelines/{id}/PipelineTasks/ByName/{name}")]
        public async Task<IHttpActionResult> GetPipelineTaskByName(int id, string name)
        {
            Model.Pipeline pipeline = await _pipelineService.GetPipeline(id);

            if (pipeline == null)
            {
                return this.NotFound("Pipeline " + id.ToString() + " does not exist.");
            }
            else if (!await _pipelineService.UserCanAccessPipeline(pipeline))
            {
                return Unauthorized();
            }
            else
            {
                PipelineTask pipelineTask = await _pipelineTaskService.GetPipelineTaskByName(id, name);
                if (pipelineTask == null)
                {
                    return this.NotFound("Pipeline Task '" + name + "' does not exist in the pipeline.");
                }
                else
                {
                    return Ok(pipelineTask);
                }
            }
        }

        /// <summary>
        /// Retrieves the first Pipeline Task following {afterPosition} in Pipeline {id}
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Pipeline ID</param>
        /// <param name="name">After Sequence Position</param>
        /// <response code="401">Pipeline is not accessible to user.</response>
        /// <response code="404">Pipeline does not exist.</response>        
        /// <response code="404">There are no pipeline tasks following {afterPosition}</response>

        // GET api/Pipelines/{id}/PipelineTasks/Next/{afterPosition}
        [Route("api/Pipelines/{id}/PipelineTasks/Next/{afterPosition}")]
        public async Task<IHttpActionResult> GetPipelineTaskNext(int id, int afterPosition)
        {
            Model.Pipeline pipeline = await _pipelineService.GetPipeline(id);

            if (pipeline == null)
            {
                return this.NotFound("Pipeline " + id.ToString() + " does not exist.");
            }
            else if (!await _pipelineService.UserCanAccessPipeline(pipeline))
            {
                return Unauthorized();
            }
            else
            {
                PipelineTask pipelineTask = await _pipelineTaskService.GetPipelineTaskByPosition(id, afterPosition);
                if (pipelineTask == null)
                {
                    return this.NotFound("There are no additional Tasks after position " + afterPosition.ToString() + " .");
                }
                else
                {
                    return Ok(pipelineTask);
                }
            }
        }

        /// <summary>
        /// Retrieves pipeline parameter tasks available to pipeline {id}'s parameters for mapping
        /// </summary>
        /// <remarks>
        /// Returns a list of system paramter tasks belonging to ancestral pipelines.  Currently 'INPUT' is supported.
        /// This will return a list of the System End tasks from ancestral pipelines.  The parameters associated with
        /// these tasks are eligible for mapping to {id}'s Input parameters.
        /// </remarks>
        /// <param name="id">Current pipeline</param>
        /// <param name="direction">'INPUT' or 'OUTPUT'</param>
        /// <returns></returns>

        // GET api/Pipelines/{id}/Parameters/{direction}
        [Route("api/Pipelines/{id}/Parameters/{direction}")]
        public async Task<IHttpActionResult> GetInputParameters(int id, string direction)
        {
            IEnumerable<PipelineTask> availableInputParameters = await _pipelineService.GetPipelineAvailableParameters(id, direction);

            return Ok(availableInputParameters);
        }
        /// <summary>
        /// Adds a new pipeline to either a project or another pipeline
        /// </summary>
        /// <remarks>
        /// Minimally a request to add a new pipeline must provide values for the ProjectID and the pipeline
        /// Name.  The ParentID (i.e. parent pipeline) is optional.  
        /// 
        /// The project Id is required
        /// in part to confirm the currently logged in user can modify the project.   If the ParentID entry is null
        /// the new pipeline is directly assigned to the project.  If the ParentID is not null, the new pipleline
        /// becomes a dependent pipeline of ParentID.  
        /// 
        /// The below requests 'project pipeline' be created and directly associated with projectID 22.
        /// 
        ///     {
        ///         "Name": "project pipeline",
        ///         "ProjectID": 22,
        ///         "ParentID": null
        ///     }
        /// 
        /// The below requests the pipeline 'Dependent pipeline' be assigned to pipeline 6.  The ProjectID, 22, indicates
        /// the project owning all pipelines and does not necessarily directly own pipeline 6 (i.e. pipeline 6 might itself
        /// be a dependent pipeline).
        /// 
        ///     {
        ///         "Name": "Dependent pipeline",
        ///         "ProjectID": 22,
        ///         "ParentID": 6
        ///     }
        /// 
        /// Successfully created pipelines return the pipeline id assigned the new pipe.
        /// 
        /// </remarks> 
        /// <response code="201">Pipeline successfully created</response>
        /// <response code="409">Pipeline was not created, reason specified in the response</response>

        // POST api/Pipelines
        [Route("api/Pipelines")]
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]Model.Pipeline value)
        {
            int newPipelineId = await _pipelineService.AddPipeline(value);

            if (newPipelineId == -2)
            {
                return this.Conflict("Pipeline name already in use.");
            }

            return Created("", newPipelineId);
        }

        /// <summary>
        /// Update a Pipeline Name
        /// </summary>
        /// <remarks>
        /// This API currently only supports updating a Pipeline's name.  Minimally only the pipeline id
        /// and name are required.
        /// 
        ///     {
        ///         "PipeLineID": 102,
        ///         "Name": "New Name"
        ///     }
        /// 
        /// In this example all fields are provided but only the Name will be updated.
        /// 
        ///     {
        ///         "Schedule": null,
        ///         "PipeLineID": 102,
        ///         "Name": "New Name",
        ///         "ProjectID": 100,
        ///         "ParentID": null,
        ///         "ModuleID": null,
        ///         "ParentToPipelineID": 100,
        ///         "LastRun": null,
        ///         "LastStatus": null,
        ///         "project": null,
        ///         "Module": null,
        ///         "PipelineInstances": null,
        ///         "notifications": null,
        ///         "Parameters": null
        ///     }
        /// 
        /// </remarks>
        /// <param name="id">ID of Pipeline to Update</param>
        /// <param name="value">Update information</param>
        /// <returns>OK</returns>
        /// <response code="200">Pipeline successfully updated</response>
        /// <response code="401">Pipeline is not accessible to user.</response>
        /// <response code="404">Pipeline ID was not found</response>

        // PUT api/Pipelines/5
        [HttpPut]
        [Route("api/Pipelines/{id}")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]Model.Pipeline value)
        {
            IHttpActionResult piplineAccessibility = await PipelineAccessibility(id);
            if (piplineAccessibility.ToString() != "System.Web.Http.Results.OkResult")
            {
                return piplineAccessibility;
            }
            else
            {
                await _pipelineService.UpdatePipeline(value);
                return Ok();
            }
        }

        /// <summary>
        /// Delete a pipeline
        /// </summary>
        /// <remarks>
        /// Deletes pipeline {id}
        /// </remarks>
        /// <param name="id">ID of Pipeline to Delete</param>
        /// <response code="200">Pipeline successfully deleted</response>
        /// <response code="401">Pipeline is not accessible to user.</response>
        /// <response code="404">Pipeline ID was not found</response>

        // DELETE api/Pipelines/5
        [Route("api/Pipelines/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            IHttpActionResult piplineAccessibility = await PipelineAccessibility(id);
            if (piplineAccessibility.ToString() != "System.Web.Http.Results.OkResult")
            {
                return piplineAccessibility;
            }
            else
            {
                try
                {
                    await _pipelineService.DeletePipline(id);
                    return Ok();
                }
                catch (InvalidOperationException invalidDelete)
                {
                    return Content(HttpStatusCode.Forbidden, invalidDelete.Message.ToString());
                }
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/Pipelines/{pipelineId}/events")]
        public async Task RaiseEventToClients(int pipelineId)
        {
            int? projectID = await _pipelineService.GetPipelineProjectID(pipelineId);
            GenericNotification pipeHub = new GenericNotification(_projectUserService, _userService);
            dynamic notificationObj = new JObject();
            notificationObj.pipelineId = pipelineId;
            notificationObj.type = "Pipeline";
            pipeHub.PushNotification(projectID, notificationObj);

        }
        [HttpGet]
        [AllowAnonymous]
        [Route("api/Pipelines/Instances/{instanceId}/events")]
        public async Task RaiseEventToClientsForPipeInstance(int instanceId)
        {
            PipelineInstance pipelineInstance = await _pipelineInstanceService.GetPipelineInstance(instanceId);
            int? projectID = await _pipelineService.GetPipelineProjectID(pipelineInstance.PipelineId);
            GenericNotification pipeHub = new GenericNotification(_projectUserService, _userService);
            dynamic notificationObj = new JObject();
            notificationObj.instanceId = instanceId;
            notificationObj.type = "PipelineInstance";
            pipeHub.PushNotification(projectID, notificationObj);

        }

        /// <summary>
        /// Retrieves notification setting of the given pipeline id
        /// </summary>
        /// <remarks>
        /// Get the notification settings of pipeline
        /// </remarks>
        /// <param name="pipelineId">ID of the pipeline to get notification settings of that pipeline</param>
        /// <response code="200">Notification setting information</response>
        [HttpGet]
        [Route("api/Pipelines/{pipelineId}/PipelineNotificationSettings")]
        public async Task<IHttpActionResult> GetNotificationSettings(int pipelineId)
        {
            IEnumerable<PipelineNotificationSetting> pipelineNotificationSettings = await _pipelineNotificationSettingsService.GetNotificationSettings(pipelineId);
            return Ok(pipelineNotificationSettings);
        }

        /// <summary>
        /// Update notification settings {pipelineNotificationSettings} of the given pipelineid
        /// </summary>
        /// <param name="pipelineID">Id of the pipeline for which notification settings should add/update.</param>
        /// <param name="pipelineNotificationSettings"></param>
        /// <response code="200">Notification settings successfully add/update.</response>
        [HttpPut]
        [Route("api/Pipelines/{pipelineID}/PipelineNotificationSettings")]
        public async Task<IHttpActionResult> PutNotificationSettings(int pipelineID,[FromBody] List<PipelineNotificationSetting> pipelineNotificationSettings)
        {

            await _pipelineNotificationSettingsService.UpdateNotificationSettings(pipelineID,pipelineNotificationSettings);
            return Ok();
        }

        /// <summary>
        /// Delete notification setting from the given pipelineid.
        /// </summary>
        /// <param name="pipelineId">ID of Pipeline for which user should be removed from notifiation settings.</param>
        /// <param name="userID">ID of the user to remove from the notification settings of the given pipelineid.</param>
        /// <response code="200">User successfully removed from the notification setting of the pipeline.</response>
        [HttpDelete]
        [Route("api/Pipelines/{pipelineId}/PipelineNotificationSettings/{userID}")]
        public async Task<IHttpActionResult> DeleteNotificationSettings(int pipelineId, int userID)
        {
            await _pipelineNotificationSettingsService.DeleteNotificationSetting(pipelineId, userID);
            return Ok();
        }
    }
}