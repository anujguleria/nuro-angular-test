﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.API.HTTPStatusExtensions;
using GfK.Automata.Pipeline.API.Identity;
using System.Configuration;
using TimeZoneConverter;

namespace GfK.Automata.Pipeline.API.Controllers
{
    public class SchedulesController : ApiController
    {
        private readonly IScheduleService _scheduleService;

        public SchedulesController(IScheduleService scheduleService)
        {
            _scheduleService = scheduleService;
        }

        // GET api/<controller>/5
        [Route("api/Schedules/{id}"), HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            // TODO - figure out a better way to do this for now we're just mapping to / from local time
            Schedule schedule = await _scheduleService.GetSchedule(id);
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            schedule.StartTime = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime, tz);
            return Ok(schedule);
        }

        [Route("api/Schedules/Timezone"), HttpPost]
        public async Task<IHttpActionResult> PostTimezone([FromBody]dynamic iana)
        {
            string tz = TZConvert.IanaToWindows(iana);
            return Ok(tz);
        }

        // PUT api/<controller>/5
        public async Task<IHttpActionResult> Put(int id, [FromBody]Schedule value)
        {
            // TODO - figure out a better way to do this for now we're just mapping to / from local time
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(value.TimeZoneID);
            value.StartTime = TimeZoneInfo.ConvertTimeFromUtc(value.StartTime, tz);
            value.ScheduleID = id;
            await _scheduleService.UpdateSchedule(value);
            return Ok();
        }

        // POST api/<controller>/5
        public async Task<IHttpActionResult> Post([FromBody]Schedule value)
        {
            // TODO - figure out a better way to do this for now we're just mapping to / from local time
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(value.TimeZoneID);
            value.StartTime = TimeZoneInfo.ConvertTimeFromUtc(value.StartTime, tz);
            return Ok(await _scheduleService.CreateSchedule(value));
        }
    }
}