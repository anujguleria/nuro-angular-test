﻿using GfK.Automata.Pipeline.API.HTTPStatusExtensions;
using GfK.Automata.Pipeline.API.Identity;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace GfK.Automata.Pipeline.API.Controllers
{
    public class GroupsController : ApiController
    {
        private readonly IGapGroupService _gapGroupService;
        private readonly IUserService _userService;
        private readonly IAuthenticationService _authenticationService;
        private readonly User _currentlyLoggedInUser;

        public GroupsController(IGapGroupService gapGroupService, IAuthenticationService authenticationService, IUserService userService)
        {
            _gapGroupService = gapGroupService;
            _userService = userService;
            _authenticationService = authenticationService;

            Task<User> currentUserName = _userService.GetUser(_authenticationService.GetCurrentlyLoggedInUserName(User));
            currentUserName.Wait();
            _currentlyLoggedInUser = currentUserName.Result;
        }

        /// <summary>
        /// Retrieves the list of all GAP Groups.
        /// </summary>
        /// <remarks>
        /// The results do not include projects belonging to the group or users in the group. These
        /// can be retrieved on separate API calls.
        /// 
        /// [
        ///     {
        ///         "Name": "DefaultGroup",
        ///         "GapGroupID": 1,
        ///         "Projects": null,
        ///         "GapGroupUsers": null
        ///     }
        /// ]
        /// 
        /// </remarks>
        /// <returns></returns>

        // GET api/Groups
        public async Task<IHttpActionResult> Get()
        {
            IEnumerable<GapGroup> gapGroups = await _gapGroupService.GetGapGroups();

            return Ok(gapGroups);
        }

        /// <summary>
        /// Retrieves an individual GAP group.
        /// </summary>
        /// <param name="id">Group ID</param>
        /// <remarks>
        /// The results do not include projects belonging to the group or users in the group. These
        /// can be retrieved on separate API calls.
        /// </remarks>
        /// <returns>
        ///     {
        ///         "Name": "DefaultGroup",
        ///         "GapGroupID": 1,
        ///         "Projects": null,
        ///         "GapGroupUsers": null
        ///     }
        /// </returns>
        /// <response code="404">Group with {id} does not exist.</response>
        // GET api/Groups/{id}
        public async Task<IHttpActionResult> Get(int id)
        {
            GapGroup gapGroup = await _gapGroupService.GetGapGroup(id);

            if (gapGroup == null)
            {
                return this.NotFound("Group " + id.ToString() + " does not exist.");
            }
            else
            {
                return Ok(gapGroup);
            }
        }

        /// <summary>
        /// Retrieves an individual GAP group with assigned users.
        /// </summary>
        /// <param name="id">Group ID</param>
        /// <remarks>
        /// The results do not include projects belonging to the group.. These
        /// can be retrieved on separate API call.
        /// </remarks>
        /// <response code="404">Group with {id} does not exist.</response>
        // GET api/Groups/{id}/Users
        [Route("api/Groups/{id}/Users")]
        public async Task<IHttpActionResult> GetWithUsers(int id)
        {
            GapGroup gapGroup = await _gapGroupService.GetGapGroupWithUsers(id);

            if (gapGroup == null)
            {
                return this.NotFound("Group " + id.ToString() + " does not exist.");
            }
            else
            {
                return Ok(gapGroup);
            }
        }

        /// <summary>
        /// Adds a new Group 
        /// </summary>
        /// <remarks>
        /// Only the new Group's name is provided.   Names must be unique and a status code
        /// of 409 is returned if a Group name is already in use. 
        /// 
        /// {"Name" : "NewGroupName" }
        /// 
        /// Successfully created Groups return the Group Id assigned the new group.
        /// </remarks>
        /// <response code="201">Group created, returns new id</response>
        /// <response code="409">Group name already in use.</response>
        public async Task<IHttpActionResult> Post([FromBody]GapGroup value)
        {
            int newGapGroupID = await _gapGroupService.AddGapGroup(value);

            if (newGapGroupID == -2)
            {
                return this.Conflict("Group name already in use.");
            }

            return Created("", newGapGroupID);
        }

        /// <summary>
        /// Updates a group / assigned users
        /// </summary>
        /// <param name="id">Group ID</param>
        /// <remarks>
        /// To updated Gap Group Users only the Gap Group ID and User ID need be provided per the below. 
        /// 
        /// <code>
        /// {
        ///     "Name": "MikeGroup26",
        ///     "GapGroupID": 6,
        ///     "Projects": null,
        ///     "GapGroupUsers": [
        ///         {
        ///             "GapGroupID": 6,
        ///             "UserID": 10
        ///         },
        ///         {
        ///             "GapGroupID": 6,
        ///             "UserID": 4
        ///         }
        ///     ]
        /// }
        /// </code>
        /// 
        /// NOTE - the user list must be complete, that is it must include every user expected to be on the group.
        /// </remarks>
        /// <response code="404">Group ID is invalid</response>
        public async Task<IHttpActionResult> Put(int id, [FromBody] GapGroup value)
        {
            try
            {
                value.GapGroupID = id;
                await _gapGroupService.UpdateGapGroup(value);

                return Ok();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Store update, insert, or delete statement affected an unexpected number of rows (0)"))
                {
                    return this.NotFound("No Group with Id " + id.ToString() + " appears to exist.");
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Deletes Group {id}
        /// </summary>
        /// <param name="id">ID of Group to delete</param>
        /// <returns></returns>
        /// <response code="401">Current user does not have privilege</response>
        /// <response code="403">Group owns Projects (list of names returned) </response>
        /// <response code="404">Group ID is invalid</response>
        // DELETE api/Groups/5
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (!(await _userService.IsUserAdmin(_currentlyLoggedInUser.UserID)))
                {
                    return Unauthorized();
                }
                else
                {
                    GapGroup group = await _gapGroupService.GetGapGroup(id);
                    if (group == null)
                    {
                        return Content(HttpStatusCode.NotFound, "Group with id " + id.ToString() + " not found.");
                    }
                    else
                    {
                        if (await _gapGroupService.GapGroupHasProjects(group))
                        {
                            return Content(HttpStatusCode.Forbidden, await _gapGroupService.GetGapGroupProjectNames(group));
                        }
                        else
                        {
                            await _gapGroupService.DeleteGroup(group);
                            return Ok();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
