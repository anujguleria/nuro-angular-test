﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.API.HTTPStatusExtensions;
using GfK.Automata.Pipeline.API.Identity;
using System.Configuration;
using GfK.Automata.Pipeline.Data.Service.Clone;

namespace GfK.Automata.Pipeline.API.Controllers
{
    public class ProjectsController : ApiController
    {
        private readonly IProjectService _projectService;
        private readonly IPipelineService _pipelineService;
        private readonly IProjectUserService _projectUserService;
        private readonly IUserService _userService;
        private readonly IProjectCloneService _projectCloneService;
        private readonly IAuthenticationService _authenticationService;
        private readonly User _currentlyLoggedInUser;

        public ProjectsController(IProjectService projectService, IPipelineService pipelineService, IProjectUserService projectUserService, IUserService userService
            ,IAuthenticationService authenticationService, IProjectCloneService projectCloneService)
        {
            _projectService = projectService;
            _pipelineService = pipelineService;
            _projectUserService = projectUserService;
            _userService = userService;
            _projectCloneService = projectCloneService;
            _authenticationService = authenticationService;
            
            Task<User> currentUserName = _userService.GetUser(_authenticationService.GetCurrentlyLoggedInUserName(User));
            currentUserName.Wait();
            _currentlyLoggedInUser = currentUserName.Result;

            _projectService.CurrentlyLoggedInUser = _currentlyLoggedInUser;
            _pipelineService.CurrentlyLoggedInUser = _currentlyLoggedInUser;
            _projectCloneService.CurrentlyLoggedInUser = _currentlyLoggedInUser;
        } 

        /// <summary>
        /// Retrieves the list of all Projects
        /// </summary>
        /// <returns></returns>

        // GET api/Projects
        public async Task<IHttpActionResult> Get()
        {
            IEnumerable<Project> projects = await _projectService.GetUserProjects();

            return Ok(projects);
        }

        /// <summary>
        /// Retrieves the list of all Projects including their pipelines.  
        /// </summary>
        /// <remarks>
        /// Pipelines include their last run date / status as applicable
        /// </remarks>/// 
        /// <returns></returns>
        /// 
        // GET api/Projects/Pipelines
        [Route("api/Projects/Pipelines")] // direct route so no confusion with api/Projects/{id}
        public async Task<IHttpActionResult> GetWithPipelines()
        {
            IEnumerable<Project> projects = await _projectService.GetProjectsWithPipelines();

            return Ok(projects);
        }
        /// <summary>
        /// Retrieves the list of Pipelines for a Project
        /// </summary>
        /// <returns></returns>
        /// /// <response code="404">Project does not exist</response>
        /// 
        /// <param name="id">Project ID</param>

        // GET api/Projects/{id}/Pipelines
        public async Task<IHttpActionResult> GetPipelines(int id)
        {
            IEnumerable<Model.Pipeline> pipelines = null;

            pipelines = await _pipelineService.GetPipelinesByProject(id);    
            
            if (pipelines != null)
            {
                return Ok(pipelines);
            }
            else
            {
                return NotFound();
            }            
        }

        /// <summary>
        /// Clones a project including all its Pipelines
        /// </summary>
        /// <remarks>
        /// To clone a project the name of the cloned project and the Group to which it is assigned is provided. The
        /// below clones project {id} into a new project named ClonedProject assigned to group TargetGroup.  The cloning
        /// creates the new project and clones all pipelines/dependent pipelines/pipeline tasks from {id}.
        /// 
        /// NOTE - user input parameter values are NOT copied over to the cloned parameter task.  Parameter inputs that
        /// are outputs of prior pipeline tasks ARE re-mapped and reflected in the cloned copy.
        /// 
        ///     {
        ///         "Name": "ClonedProject",
        ///         "Group": "TargetGroup"
        ///     }
        /// 
        /// </remarks>
        /// <returns></returns>
        /// <response code="201">Project cloned</response>
        /// <response code="401">User does not have right to clone project {id} / Group {Group} does not exist or user is not assigned.</response>
        /// <param name="id">Project ID to clone</param>

        // POST api/Projects/{id}/Clone
        [HttpPost]
        [Route("api/Projects/{id}/Clone")]
        public async Task<IHttpActionResult> PostProjectClone(int id, [FromBody]object context)
        {
            try
            {
                int clonedProjectID = await _projectCloneService.CloneProject(id, context);

                return Created("Project successfully cloned.", clonedProjectID);
            }
            catch (Exception ex)
            {
                if (ex.GetType().ToString() == "System.InvalidOperationException")
                {
                    return this.UnAuthorizedAccess(ex.Message);
                }
                else
                {
                    throw;
                }
            }
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// Retrieve information for a project by its name.  
        /// </summary>
        /// <param name="name">Name of Project being requested</param>
        /// <remarks>
        /// Can be used to confirm the existence of a project.  For example can check if a user entered Project name is already in use.
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Project exists by requested name</response>
        /// <response code="404">Project does not exist</response>

        // GET api/Projects/ByName/{name}
        [Route("api/Projects/ByName/{name}")]
        public async Task<IHttpActionResult> GetByName(string name)
        {
            Project project = await _projectService.GetProject(name);

            if (project == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(project);
            }
        }

        /// <summary>
        /// Creates a new Project
        /// </summary>
        /// <remarks>
        /// When creating a new project generally only the name is available.   Once the project is
        /// created and assigned an Id additional attributes (e.g. pipelines) can be assigned.
        /// 
        ///     {
        ///        "name": "My New Project",
        ///     }
        /// </remarks>
        /// <returns>Project Id assigned the new project</returns>
        /// <param name="value">Project as Json</param>
        /// <response code="201">Project created</response>
        /// <response code="409">Project name in use</response>

        // POST api/<controller>
        //[Authorize]
        public async Task<IHttpActionResult> Post([FromBody]Project value)
        {
            if ((await _projectService.ProjectExists(value.Name)))
            {
                return this.Conflict("Project with name '" + value.Name + "' already exists.");                
            }

            int newProjectId = await _projectService.CreateProject(value);
            await _projectUserService.AddProjectToUser(_currentlyLoggedInUser.Email, newProjectId, DateTime.UtcNow);
     
            return Created("api/Projects/" + newProjectId.ToString(), newProjectId);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}