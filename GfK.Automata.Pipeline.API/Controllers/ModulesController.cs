﻿using GfK.Automata.Pipeline.API.HTTPRequestHeaderValidation;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace GfK.Automata.Pipeline.API.Controllers
{
    public class ModulesController : ApiController
    {
        private readonly IModuleService _moduleService;

        public ModulesController(IModuleService moduleService)
        {
            _moduleService = moduleService;
        }

        /// <summary>
        /// Retrieves the list of all Modules
        /// </summary>
        /// <returns></returns>
        /// <response code = "200">Modules retrieved.</response>

        // GET api/Modules
        public async Task<IHttpActionResult> Get()
        {
            IEnumerable<Module> modules = await _moduleService.GetModules(m => m.ModuleType != "PIPELINE" && m.ModuleType != "SYSTEM");
            
            // - need to figure out a better way to not get the recursive blow out of modules and groups modules.OrderBy(p => p.ModuleGroup.Name).OrderBy(p => p.Name);
            modules.OrderBy(p => p.Name);
            return Ok(modules);
        }

        /// <summary>
        /// Retrieve Module {id}'s parameters with mappings
        /// </summary>
        /// <remarks>
        /// Retrieves Module {id}'s parameters.  Each parameter includes a list
        /// of parameters to which it is mapped (PipelineTaskInputValues).  If this
        /// list is empty ([]) or missing (null) the parameter is not in use.
        /// 
        /// 
        /// {
        ///     "Parameters": [
        ///         {
        ///             "ParameterType": {
        ///                 "ParameterTypeID": 4,
        ///                 "Name": "String",
        ///                 "Description": null
        ///             },
        ///             "ParameterID": 30,
        ///             "Name": "MikePost_2_Start_P1",
        ///             "Description": "MikePost_2_Start_P1",
        ///             "Direction": "Inout",
        ///             "SequencePosition": 1,
        ///             "ParameterTypeID": 4,
        ///             "UserInputValues": null,
        ///             "PipelineTaskInputValues": [
        ///                 {
        ///                     "PipelineTaskInputValueID": 14,
        ///                     "PriorPipelineTaskID": 14,
        ///                     "PriorPipeLineTaskParameterID": 30,
        ///                     "ParameterID": 3,
        ///                     "PipelineTaskID": 4,
        ///                     "PipelineID": 103,
        ///                     "PriorPipelineID": 103
        ///                 }
        ///             ]
        ///         }
        ///     ]
        /// }
        /// 
        /// 
        /// </remarks>
        /// <returns></returns>
        /// <response code = "200">Parameters retrieved.</response>

        // GET api/Modules/{id}/Parameters
        [HttpGet]
        [Route("api/Modules/{id}/Parameters")]
        public async Task<IHttpActionResult> GetParameters(int id)
        {
            IEnumerable<Parameter> parametersWithMappings = await _moduleService.GetModulesParametersWithMappings(id);

            return Ok(parametersWithMappings);
        }

        /// <summary>
        /// Retrieves Module {id}
        /// </summary>
        /// <returns></returns>
        /// <response code = "200">Module retrieved.</response>

        // GET api/Modules/{id}
        [HttpGet]
        [Route("api/Modules/{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            Module modules = await _moduleService.GetModule(id);

            return Ok(modules);
        }

        /// <summary>
        /// Adds a Module and its Parameters
        /// </summary>
        /// <remarks>
        /// Module with no parameters
        /// 
        ///     {
        ///         "Name": "ModuleName",
        ///         "Description": "New Module",
        ///         "ModuleType": "Builtin",
        ///         "AssemblyName": "assembly.name",
        ///         "TypeName": "assembly.name.type",
        ///         "ModuleGroupID": 1
        ///     }
        ///  
        /// Module with Parameters
        /// 
        ///     {
        ///         "Name": "ModuleName",
        ///         "Description": "New Module",
        ///         "ModuleType": "Builtin",
        ///         "AssemblyName": "assembly.name",
        ///         "TypeName": "assembly.name.type",
        ///         "ModuleGroupID": 1,
        ///         "Parameters": [
        ///             {
        ///                 "Name": "P1",
        ///                 "Description": "P1",
        ///                 "Direction": "IN",
        ///                 "SequencePosition": 1,
        ///                 "ParameterTypeID": 4,
        ///             }     
        ///         ]
        ///     }
        ///     
        /// </remarks>
        /// <param name="id">Module ID</param>
        /// 
        /// <response code = "201">Module created.</response>

        // POST api/Modules
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody] Module value)
        {
            int newModuleID = await _moduleService.CreateModule(value);

            return Created("Module created.", newModuleID);
        }

        /// <summary>
        /// Adds a custom Module, its Parameters, implementing Pipeline
        /// </summary>
        /// <remarks>
        /// Creates a user-defined custom module.  A custom module's implementation
        /// is an underlying Pipeline.  Users build the pipeline to define the modules
        /// behavior.  This API call builds the basic framework including the starting
        /// pipeline.
        /// 
        /// Module with no parameters
        /// 
        ///     {
        ///         "Name": "ModuleName",
        ///         "Description": "New Module",
        ///         "ModuleType": "Custom",
        ///         "AssemblyName": "assembly.name",
        ///         "TypeName": "assembly.name.type",
        ///         "ModuleGroupID": 1
        ///     }
        ///  
        /// Module with Parameters
        /// 
        ///     {
        ///         "Name": "ModuleName",
        ///         "Description": "New Module",
        ///         "ModuleType": "Custom",
        ///         "AssemblyName": "assembly.name",
        ///         "TypeName": "assembly.name.type",
        ///         "ModuleGroupID": 1,
        ///         "Parameters": [
        ///             {
        ///                 "Name": "P1",
        ///                 "Description": "P1",
        ///                 "Direction": "IN",
        ///                 "SequencePosition": 1,
        ///                 "ParameterTypeID": 4,
        ///             }     
        ///         ]
        ///     }
        ///     
        /// </remarks>
        /// <param name="id">Module ID</param>
        /// 
        /// <response code = "201">Module created.</response>

        // POST api/Modules/Custom
        [HttpPost]
        [Route("api/Modules/Custom")]
        public async Task<IHttpActionResult> PostCustom([FromBody] Module value)
        {
            int newModuleID = await _moduleService.CreateCustomModule(value);

            return Created("Module created.", newModuleID);
        }

        /// <summary>
        /// Update an existing module's parameters
        /// </summary>
        /// <remarks>
        /// Parameters can be updated, deleted or added.   The modification types (e.g. Add) are specified along with the
        /// target parameters. 
        /// 
        /// For Updates the following fields MUST be included: ParameterID, Name, Description, Direction, SequencePosition, and
        /// ParameterTypeID.
        /// 
        /// For Deletes only the ParameterID need be provided.
        /// 
        /// For Adds the following fields MUST be included: Name, Direction, SequencePosition, and ParameterTypeID.
        /// 
        /// NOTE: In all cases parameters are to be INOUT.  ParameterTypeID should correspond to the id given the type 'String'
        /// 
        /// Sample Request:
        /// 
        ///     {
        ///         "modifications" : [
        ///             {
        ///                 "Action": "Update",
        ///                 "Parameters": [
        ///                     {
        ///                         "ParameterID" : 1,
        ///                         "Name": "P1",
        ///                         "Description": "P1",
        ///                         "Direction": "IN",
        ///                         "SequencePosition": 1,
        ///                         "ParameterTypeID": 4
        ///                     }
        ///                 ]
        ///             },
        ///             {
        ///                 "Action": "Delete",
        ///                 "Parameters": [
        ///                     {
        ///                         "ParameterID" : 2
        ///                     }
        ///                 ]
        ///             },
        ///             {
        ///                 "Action": "Add",
        ///                 "Parameters": [
        ///                     {
        ///                         "Name": "P2",
        ///                         "Description": "P2",
        ///                         "Direction": "IN",
        ///                         "SequencePosition": 2,
        ///                         "ParameterTypeID": 4
        ///                     }
        ///                 ]
        ///             }        
        ///         ]
        ///     }
        /// 
        /// </remarks>
        /// <response code="200">Module parameters successfully updated</response>

        // PUT api/Modules/{id}/Parameters
        [HttpPut]
        [Route("api/Modules/{id}/Parameters")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]object value)
        {
            await _moduleService.UpdateModuleParameters(id, await HttpRequestValidator.GetRequestContentAsJson(value));

            return Ok();
        }
    }
}