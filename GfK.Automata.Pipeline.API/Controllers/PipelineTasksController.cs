﻿using GfK.Automata.Pipeline.API.HTTPRequestHeaderValidation;
using GfK.Automata.Pipeline.API.Identity;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;

namespace GfK.Automata.Pipeline.API.Controllers
{
    public class PipelineTasksController : ApiController
    {
        private readonly IPipelineTaskService _pipelineTaskService;
        private readonly IPipelineTaskInstanceService _pipelineTaskInstanceService;
        private readonly IUserService _userService;
        private readonly IGapGroupUserPipelineService _gapGroupUserPipelineService;
        private readonly IAuthenticationService _authenticationService;
        private readonly User _currentlyLoggedInUser;


        public PipelineTasksController(IPipelineTaskService pipelineTaskService, IUserService userService, IAuthenticationService authenticationService
            ,IGapGroupUserPipelineService gapGroupUserPipelineService, IPipelineTaskInstanceService pipelineTaskInstanceService)
        {
            _pipelineTaskService = pipelineTaskService;
            _userService = userService;
            _authenticationService = authenticationService;
            _gapGroupUserPipelineService = gapGroupUserPipelineService;
            _pipelineTaskInstanceService = pipelineTaskInstanceService;

            Task<User> currentUserName = _userService.GetUser(_authenticationService.GetCurrentlyLoggedInUserName(User));
            currentUserName.Wait();
            _currentlyLoggedInUser = currentUserName.Result;
        }

        /// <summary>
        /// Adds a new pipeline task to a pipeline
        /// </summary>
        /// <remarks>
        /// The below requests a Pipeline Task based on ModuleID 1 be added to pipeline 6.  Its sequence position indicates
        /// it is the first pipeline task in the pipeline.  A pipeline task does not require Values but all the other fields
        /// included below must be provided.
        /// 
        /// This example is adding the first pipeline task to a pipeline.  It has a single input which is directly provided
        /// rather than coming from a prior pipeline task
        /// 
        ///     {  
        ///         "Name":"Task 1",
        ///         "SequencePosition":1,
        ///         "PipeLineID":5,
        ///         "ModuleID":1,
        ///         "UserInputValues":[
        ///             {  
        ///                 "InputValue":"P1 value",
        ///                 "ParameterID":3,
        ///             }
        ///         ]
        ///     }
        /// 
        /// This example inserts a new pipeline task in the second position.   It maps the outputs of the prior pipeline task
        /// (1005) to its inputs.  In this example all inputs to the new pipeline task are mapped to outputs from the prior task.
        /// 
        ///     {  
        ///         "Name":"Task 2",
        ///         "SequencePosition":2,
        ///         "PipeLineID":5,
        ///         "ModuleID":3,
        ///         "PipelineTaskInputValues":[
        ///             {  
        ///                 "ParameterID":4,
        ///                 "PipelineID":5,
        ///                 "PriorPipelineTaskID":1005,
        ///                 "PriorPipeLineTaskParameterID":1
        ///             },
        ///             {  
        ///                 ParameterID":5,
        ///                 "PipelineID":5,
        ///                 "PriorPipelineTaskID":1005,
        ///                 "PriorPipeLineTaskParameterID":2
        ///                 },
        /// 	        {  
        ///                 "ParameterID":6,
        ///                 "PipelineID":5,
        ///                 "PriorPipelineTaskID":1005,
        ///                 "PriorPipeLineTaskParameterID":3
        ///             }
        ///         ]
        ///     }
        /// 
        /// Successfully created pipelines return the pipeline task id assigned the new pipeline task.
        /// 
        /// </remarks> 
        /// <response code="201">Pipeline Task successfully created</response>

        // POST api/PipelineTasks
        public async Task<IHttpActionResult> Post([FromBody]PipelineTask value)
        {
            if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(value.PipeLineID, _currentlyLoggedInUser))
            {
                return Unauthorized();
            }
            else
            {
                int newPipelineTaskId = await _pipelineTaskService.AddPipelineTask(value);

                return Created("", newPipelineTaskId);
            }

        }
        /// <summary>
        /// Retrieves a pipelineTask by name.
        /// </summary>
        /// <remarks>
        ///  
        /// A pipelinetask name is resolved within the context of a project and, optionally, a parent pipeline.  Consequently requests to identify
        /// a pipelinetask by {name} minimally require a project id be provided.  The body provides JSON indicating
        /// the 'context' within which the pipeline name is to be resolved.
        /// 
        /// NOTE - the ProjectID must reference a project to which the currently logged in user has access.
        /// 
        /// Given the URL ... api/PipelineTasks/ByName/PT1 and the body as below, the api will see if there is a pipelinetask named PT1 directly
        /// associated with project 22
        /// 
        ///     {
        ///         "ProjectID": 22,
        ///         "PipelineID": null
        ///     }
        /// 
        /// or its equivalent
        /// 
        ///     {
        ///         "ProjectID": 22
        ///     }
        ///     
        /// Given the URL ... api/Pipelines/ByName/PT2 and the body as below, the api will see if there is a pipelinetask named PT2 dependent
        /// on pipelineId 6.  The parent pipeline does not itself need to be directly associated with the project.
        /// 
        ///     {
        ///         "ProjectID": 22,
        ///         "PipelineID": 6
        ///     }
        /// 
        /// </remarks> 
        /// <response code="200">A pipelinetask by {name} exists.</response>
        /// <response code="400">Typically indicates request header is not correctly formatted.</response>
        /// <response code="404">PipelineTask not found, name may not be in use or Project is not accessible to user.</response>
        /// 
        // POST api/Pipelines/ByName/{name}
        [Route("api/PipelineTasks/ByName/{name}")]
        [HttpPost]
        public async Task<IHttpActionResult> PostByName(string name, [FromBody]object context)
        {
            dynamic requestValues = await PipelineTaskHttpRequestValidator.GetRequestByName(context);

            if (await HttpRequestValidator.IsError(requestValues))
            {
                return BadRequest(await HttpRequestValidator.GetError(requestValues));
            }
            else
            {
                Model.PipelineTask pipelineTask = await _pipelineTaskService.GetPipelineTask(name, _currentlyLoggedInUser, requestValues);

                if (pipelineTask == null)
                {
                    return NotFound();
                }
                else if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(pipelineTask.PipeLineID, _currentlyLoggedInUser))
                {
                    return Unauthorized();
                }
                else
                {
                    return Ok(pipelineTask);
                }
            }
        }
        /// <summary>
        /// Update an existing pipeline task
        /// </summary>
        /// <remarks>
        /// Inputs for a Pipeline Task are entirely replaced by the Put content.  For example if a pipeline task has two user inputs and a third is added the Put 
        /// must provide not only the new input but the two current inputs.  This is because the Put will delete the two existing inputs and add those inputs
        /// specified by the Put.
        /// 
        ///     {
        ///         "PipelineTaskInputValues": [
        ///             {
        ///                 "ParameterID":29,
        ///                 "PipelineID":107,
        ///                 "PriorPipelineTaskID":1,
        ///                 "PriorPipeLineTaskParameterID":1,
        ///                 "PriorPipelineID": 101
        ///             }        
        ///         ],
        ///         "UserInputValues": [],
        ///         "PipelineTaskID": 12,
        ///         "Name": "System End",
        ///         "SequencePosition": 0,
        ///         "PipeLineID": 107,
        ///         "ModuleID": 7
        ///     }
        /// 
        /// </remarks>
        /// <response code="200">Pipeline Task successfully updated</response>

        // PUT api/PipelineTasks/5
        [HttpPut]
        [Route("api/PipelineTasks/{id}")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]PipelineTask value)
        {
            if (! await _gapGroupUserPipelineService.UserCanAccessPipeline(value.PipeLineID, _currentlyLoggedInUser))
            {
                return Unauthorized();
            }
            else
            {
                value.PipelineTaskID = id;
                await _pipelineTaskService.UpdatePipelineTask(value);

                return Ok();
            }
        }

        [HttpGet]
        [Route("api/PipelineTasks/{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            PipelineTask pipelineTask = await _pipelineTaskService.GetPipelineTask(id);

            if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(pipelineTask.PipeLineID, _currentlyLoggedInUser))
            {
                return Unauthorized();
            }
            else
            {
                return Ok(await _pipelineTaskService.GetPipelineTask(id));
            }            
        }


        /// <summary>
        /// Retrieves list of Module log messages for a Pipeline Task instance
        /// </summary>
        /// <remarks>
        /// 
        /// Retrieves the module log messages for a specific pipeline task instance. Instances
        /// are retrieved with api/Pipelines/{id}/PipelineInstances.  The Pipeline Task ID and
        /// Pipeline Task Instance ID can be taken from this return set.
        /// 
        /// The default order is to sort ascending by the log date (i.e. messages first logged
        /// by the module appear first).  This order can be overriden by appending ?$orderby=LogDate desc 
        /// to the api call to retrieve the oldest log messages first.
        /// 
        ///     api/PipelineTasks/1/PipelineTaskInstances/12/Logs?$orderby=LogDate desc
        /// 
        /// Paging can be achieved with the $skip and $top parameters. For example to
        /// get the first ten rows append ?$skip=0&amp;$top=10 to the api request. To
        /// get the next ten rows append ?$skip=10&amp;$top=10
        /// 
        ///     api/PipelineTasks/1/PipelineTaskInstances/12/Logs?$skip=0&amp;$top=10
        ///     api/PipelineTasks/1/PipelineTaskInstances/12/Logs?$skip=10&amp;$top=10
        /// 
        /// Control options can be combined.  The below sorts the result by LogDate ascending while skipping 
        /// the first 9 rows before returning 5 rows (i.e. returns rows 10 - 14).
        /// 
        ///     api/PipelineTasks/1/PipelineTaskInstances/12/Logs?$orderby=LogDate asc&amp;$skip=9&amp;$top=5
        /// 
        /// </remarks>
        /// <returns></returns>
        /// <param name="id">Pipeline Task ID</param>
        /// <param name="tid">Pipeline Task Instance ID</param>
        /// <response code="401">Pipeline is not accessible to user.</response>

        [HttpGet]
        [Route("api/PipelineTasks/{id}/PipelineTaskInstances/{tid}/Logs")]
        [EnableQuery]
        public async Task<IHttpActionResult> Get(int id, int tid)
        {
            PipelineTask pipelineTask = await _pipelineTaskService.GetPipelineTask(id);

            if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(pipelineTask.PipeLineID, _currentlyLoggedInUser))
            {
                return Unauthorized();
            }
            else
            {
                return Ok((await _pipelineTaskInstanceService.GetPipelineTaskInstanceWithModuleLog(tid)).PipelineTaskInstanceModuleLogs);
            }
        }

        [HttpPost]
        [Route("api/PipelineTasks/Move/{id}")]
        public async Task<IHttpActionResult> Move(int id, [FromBody]int count)
        {
            PipelineTask pipelineTask = await _pipelineTaskService.GetPipelineTask(id);

            if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(pipelineTask.PipeLineID, _currentlyLoggedInUser))
            {
                return Unauthorized();
            }
            else
            {
                await _pipelineTaskService.MovePipelineTask(id, count);
                return Ok();
            }
        }

        [HttpDelete]
        [Route("api/PipelineTasks/{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                PipelineTask pipelineTask = await _pipelineTaskService.GetPipelineTask(id);

                if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(pipelineTask.PipeLineID, _currentlyLoggedInUser))
                {
                    return Unauthorized();
                }
                else
                {
                    await _pipelineTaskService.DeletePipelineTask(id);
                    return Ok();
                }
            }
            catch(VoilationOfRefrentialIntegrityException ex)
            {
                return Conflict();
            }
        }
    }
}