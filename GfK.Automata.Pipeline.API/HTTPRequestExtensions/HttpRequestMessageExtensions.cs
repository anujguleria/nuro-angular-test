﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.HTTPRequestExtensions
{
    public static class HttpRequestMessageExtensions
    {
        public static async Task<string> GetHeaderValue(this HttpRequestMessage request, string name)
        {
            IEnumerable<string> values;
            bool found = request.Headers.TryGetValues(name, out values);

            if (found)
            {
                return values.FirstOrDefault();
            }

            return null;
        }
    }
}