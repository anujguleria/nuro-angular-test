﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace GfK.Automata.Pipeline.API.Handlers
{
    public class APIErrorHandler : IExceptionHandler
    {
        public Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            var customObject = new CustomObject
            {
                Message = new { Message = context.Exception.ToString() },
                Status = 500,
                Data = "" // whatever
            };
            //Necessary to return Json
            var jsonType = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            jsonType.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;

            var response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, customObject, jsonType);

            context.Result = new ResponseMessageResult(response);

            return Task.FromResult(0);
        }
    }

    internal class CustomObject
    {
        public object Message { get; set; }
        public int Status { get; set; }
        public string Data { get; set; }
    }
}