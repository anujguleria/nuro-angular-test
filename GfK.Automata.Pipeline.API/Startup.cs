﻿using GfK.Automata.Pipeline.API.Identity;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Service;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

[assembly: OwinStartup(typeof(GfK.Automata.Pipeline.API.Startup))]

namespace GfK.Automata.Pipeline.API
{
    public partial class Startup
    {
        static Startup()
        {
            OAuthOptions = new OAuthAuthorizationServerOptions();
        }
        public static OAuthAuthorizationServerOptions OAuthOptions
        {
            get;
            set;
        }

        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            Task<int> expiryInDaysRequest = AuthenticationSettings.GetExpiryDays();
            int expiryInDays = expiryInDaysRequest.Result;

            Task<string> tokenURLRequest = AuthenticationSettings.GetTokenRequestURLSuffix();
            string tokenURL = tokenURLRequest.Result;

            // TODO - should handle this better via container / DI
            IDbFactory dbFactory = new DBFactory();
            IUnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            IUserRepository userRepository = new UserRepository(dbFactory);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IGapRoleUserRepository gapRoleUserRepository = new GapRoleUserRepository(dbFactory);
            IGapRoleUserService gapRoleUserService = new GapRoleUserService(gapRoleUserRepository, unitOfWork);

            dbFactory = new DBFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            IGapGroupUserRepository gapGroupUserRepository = new GapGroupUserRepository(dbFactory);
            IGapGroupUserService gapGroupUserService = new GapGroupUserService(gapGroupUserRepository, unitOfWork);

            IUserService userService = new UserService(userRepository, gapRoleUserService, gapGroupUserService, unitOfWork);

            OAuthOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString(tokenURL),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(expiryInDays),
                Provider = new DomainAuthorizationProvider(userService)
            };
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            app.MapSignalR();
            GlobalHost.HubPipeline.RequireAuthentication();
        }
    }
}