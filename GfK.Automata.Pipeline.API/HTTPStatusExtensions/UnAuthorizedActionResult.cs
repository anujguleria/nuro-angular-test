﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace GfK.Automata.Pipeline.API.HTTPStatusExtensions
{
    public class UnAuthorizedActionResult : BaseCustomActionResult
    {
        public UnAuthorizedActionResult(string message, HttpRequestMessage request) : base(message, request)
        {

        }

        public override HttpResponseMessage Execute()
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.Content = new StringContent(Message); // Put the message in the response body (text/plain content).
            response.RequestMessage = Request;
            return response;
        }
    }
}