﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GfK.Automata.Pipeline.API.HTTPStatusExtensions
{
    public static class ApiControllerExtensions
    {
        public static ConflictActionResult Conflict(this ApiController controller, string message)
        {
            return new ConflictActionResult(message, controller.Request);
        }
        public static NotFoundActionResult NotFound(this ApiController controller, string message)
        {
            return new NotFoundActionResult(message, controller.Request);
        }
        public static UnAuthorizedActionResult UnAuthorizedAccess(this ApiController controller, string message)
        {
            return new UnAuthorizedActionResult(message, controller.Request);
        }
    }
}