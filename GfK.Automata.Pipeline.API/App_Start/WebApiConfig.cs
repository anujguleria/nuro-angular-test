﻿using GfK.Automata.Pipeline.API.Handlers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace GfK.Automata.Pipeline.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Services.Replace(typeof(IExceptionHandler), new APIErrorHandler());
            // Web API configuration and services

            config.Filters.Add(new AuthorizeAttribute());
            // Web API routes
            config.MapHttpAttributeRoutes();

          
            config.Routes.MapHttpRoute(
                name: "ProjectPipelinesApi",
                routeTemplate: "api/Projects/{id}/Pipelines",
                defaults: new { controller = "Projects", action = "GetPipelines" }
            );

            config.Routes.MapHttpRoute(
                name: "UserProjectsApi",
                routeTemplate: "api/Users/Projects",
                defaults: new { controller = "Users", action = "GetProjects" }
            );

            config.Routes.MapHttpRoute(
                name: "PipelineInstancesApi",
                routeTemplate: "api/Pipelines/{id}/PipelineInstances",
                defaults: new { controller = "Pipelines", action = "GetPipelineInstances" }
            );

            config.Routes.MapHttpRoute(
                name: "PipelineTaskApi",
                routeTemplate: "api/Pipelines/{id}/PipelineTasks",
                defaults: new { controller = "Pipelines", action = "GetPipelineTasks" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
