﻿using System.Web;
using System.Web.Mvc;

namespace GfK.Automata.Pipeline.API
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
