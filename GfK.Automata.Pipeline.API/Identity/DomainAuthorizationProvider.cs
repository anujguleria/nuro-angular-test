﻿using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.Identity
{
    internal class DomainAuthorizationProvider : OAuthAuthorizationServerProvider
    {
        IUserService _userService;
        public DomainAuthorizationProvider(IUserService userService)
        {
            _userService = userService;
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            ClaimsIdentity userIdentity = new ClaimsIdentity();

            string username = context.UserName
                + (context.UserName.Contains("@") ? "" : "@" + ConfigurationManager.AppSettings["AuthenticationDomain"]);

            DomanUserLoginProvider domainLoginProvider = new DomanUserLoginProvider(await AuthenticationSettings.GetName());
            domainLoginProvider.ValidateCredentials(username, context.Password, out userIdentity);

            if (userIdentity == null)
            {
                context.Rejected();
            }
            else
            {
                User currentLoggedInUser = await _userService.GetUser(username);

                if (currentLoggedInUser == null)
                { 
                    int newUserID = await _userService.CreateUser(username);
                }
                context.Validated(userIdentity);
            }
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
    }
}