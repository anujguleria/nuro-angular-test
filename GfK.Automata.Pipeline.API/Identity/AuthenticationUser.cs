﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.Identity
{
    public static class AuthenticationUser
    {
        public static async Task<string> GetCurrentlyLoggedInUserNameAsync(IPrincipal user)
        {
            if (user != null && user.Identity != null && user.Identity.IsAuthenticated)
            {
                return user.Identity.Name;
            }
            else
            {
                return ConfigurationManager.AppSettings.Get("ProxyAuthenticationUser");
            }
        }

        public static string GetCurrentlyLoggedInUserName(IPrincipal user)
        {
            Task<string> userRequest = GetCurrentlyLoggedInUserNameAsync(user);
            userRequest.Wait();

            return userRequest.Result;
        }
    }
}