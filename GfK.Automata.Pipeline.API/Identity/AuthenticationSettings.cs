﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.Identity
{
    public static class AuthenticationSettings
    {
        public static async Task<string> GetName()
        {
            return ConfigurationManager.AppSettings.Get("AuthenticationDomain");
        }
        public static async Task<int> GetExpiryDays()
        {
            return int.Parse(ConfigurationManager.AppSettings.Get("AuthenticationExpiryDays"));
        }
        public static async Task<string> GetTokenRequestURLSuffix()
        {
            return "/token";
        }
    }
}