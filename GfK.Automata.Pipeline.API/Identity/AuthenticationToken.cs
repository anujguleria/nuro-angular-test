﻿using GfK.Automata.Pipeline.API.Views;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.Identity
{
    public static class AuthenticationToken
    {
        public static async Task<AuthenticationTokenRequestResponse> GetTokenRequestResponse(HttpResponseMessage response)
        {
            string rawResponse = await response.Content.ReadAsStringAsync();

            return new AuthenticationTokenRequestResponse(rawResponse);
        }

        public static async Task<bool> RequestResponseIsOK(AuthenticationTokenRequestResponse requestResponse)
        {
            return requestResponse.IsOK;
        }

        public static async Task<dynamic> OKResponse(AuthenticationTokenRequestResponse requestResponse, Dictionary<string, string> append = null)
        {
            dynamic response = requestResponse.Token;

            if (append != null)
            {
                foreach (string appendItem in append.Keys)
                {
                    response[appendItem] = append[appendItem];
                }
            }

            return response;
        }

        public static async Task<string> BadRequestResponse(AuthenticationTokenRequestResponse requestResponse)
        {
            string badResponse = "Error - " + requestResponse.RequestResponse.Property("error").Value.ToString()  + " - Incorrect user or password";

            return badResponse.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty);
        }

        public static async Task<string> GetTokenRequestURL()
        {
            return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority
                + await AuthenticationSettings.GetTokenRequestURLSuffix(); ;
        }

        public static async Task<string> GetADUserName (LoginViewModel loginCredentials)
        {
            string fullUserName = loginCredentials.Username.Trim();

            char[] ADDomain = (await AuthenticationSettings.GetName()).ToCharArray();
            char[] userName = loginCredentials.Username.ToCharArray();

            Array.Reverse(userName);
            Array.Reverse(ADDomain);

            if (!new string(userName).StartsWith(new string(ADDomain)))
            {
                fullUserName = loginCredentials.Username + "@" + await AuthenticationSettings.GetName();
            }

            return fullUserName;
        }
        public static async Task<AuthenticationTokenRequestResponse> GetToken(LoginViewModel loginCredentials)
        {
            AuthenticationTokenRequestResponse tokenRequestResponse;

            using (HttpClient client = new HttpClient())
            {
                var credentials = new Dictionary<string, string>
                {
                    {"grant_type", "password"},
                    {"username", await GetADUserName(loginCredentials)},
                    {"password", loginCredentials.Password},
                };

                HttpResponseMessage response = await client.PostAsync(await GetTokenRequestURL(), new FormUrlEncodedContent(credentials));
                tokenRequestResponse = await GetTokenRequestResponse(response);
            }

            return tokenRequestResponse;
        }
    }
}