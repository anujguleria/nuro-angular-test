﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;

namespace GfK.Automata.Pipeline.API.Identity
{
    public class AuthenticationService : IAuthenticationService
    {
        public async Task<string> GetCurrentlyLoggedInUserNameAsync(IPrincipal user)
        {
            if (user != null && user.Identity != null && user.Identity.IsAuthenticated)
            {
                return user.Identity.Name;
            }
            else
            {
                return ConfigurationManager.AppSettings.Get("ProxyAuthenticationUser");
            }
        }

        public string GetCurrentlyLoggedInUserName(IPrincipal user)
        {
            Task<string> getUserNameTask = GetCurrentlyLoggedInUserNameAsync(user);
            getUserNameTask.Wait();

            return getUserNameTask.Result;
        }
    }
}