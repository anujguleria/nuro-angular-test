﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.DirectoryServices.AccountManagement;
using System.Configuration;

namespace GfK.Automata.Pipeline.API.Identity
{
    public class DomanUserLoginProvider : ILoginProvider
    {
        public bool ValidateCredentials(string userName, string password, out ClaimsIdentity identity)
        {
            if (ConfigurationManager.AppSettings["BypassAuth"] != null && bool.Parse(ConfigurationManager.AppSettings["BypassAuth"]))
            {
                identity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Name, userName));
                return true;
            }
            else
            {
                using (var pc = new PrincipalContext(ContextType.Domain, _domain))
                {
                    bool isValid = pc.ValidateCredentials(userName, password);
                    if (isValid)
                    {
                        identity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);
                        identity.AddClaim(new Claim(ClaimTypes.Name, userName));
                    }
                    else
                    {
                        identity = null;
                    }

                    return isValid;
                }
            }
        }

        public DomanUserLoginProvider(string domain)
        {
            _domain = domain;
        }

        private readonly string _domain;
    }
}