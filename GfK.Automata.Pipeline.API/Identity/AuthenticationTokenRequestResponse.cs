﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfK.Automata.Pipeline.API.Identity
{
    public class AuthenticationTokenRequestResponse
    {
        private readonly dynamic _requestResponse;

        public AuthenticationTokenRequestResponse(string requestResponse)
        {
            _requestResponse = JObject.Parse(requestResponse);
        }
        public bool IsOK
        {
            get
            {
                return RequestResponse.Property("error") == null;
            }                
        }
        public dynamic RequestResponse
        {
            get
            {
                return _requestResponse;
            }
        }
        public dynamic Token
        {
            get
            {
                dynamic token = new JObject();
                token.access_token = (IsOK ? RequestResponse.access_token.ToString() : "");

                return token;
            }
        }
        public dynamic Error
        {
            get
            {
                dynamic error = new JObject();
                error.error = (IsOK ? "" : RequestResponse.error.ToString());

                return error;
            }
        }
    }
}