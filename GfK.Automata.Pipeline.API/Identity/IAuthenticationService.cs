﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.API.Identity
{
    public interface IAuthenticationService
    {
        Task<string> GetCurrentlyLoggedInUserNameAsync(IPrincipal user);
        string GetCurrentlyLoggedInUserName(IPrincipal user);
    }
}
