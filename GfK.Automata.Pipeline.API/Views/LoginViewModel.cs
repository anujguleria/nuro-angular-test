﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfK.Automata.Pipeline.API.Views
{
  public class LoginViewModel
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}