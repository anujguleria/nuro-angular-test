﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

using GfK.Automata.Pipeline.API.HTTPRequestExtensions;

namespace GfK.Automata.Pipeline.API.HTTPRequestHeaderValidation
{
    public static class HttpRequestValidator
    {
        public async static Task<dynamic> GetRequestContentAsJson(object content)
        {
            JObject request;

            try
            {
                request = (content == null ? new JObject() : JObject.Parse(content.ToString()));
            }
            catch (Exception ex)
            {
                request = await SetErrorMessage("Request incorrectly formatted - " + ex.Message);
            }

            return request;
        }

        public async static Task<bool> IsError(dynamic request)
        {
            return request != null && request.Property("error") != null;
        }

        public async static Task<dynamic> SetErrorMessage(string errorMessage)
        {
            dynamic content = new JObject();
            content.error = errorMessage;

            return content;
        }

        public async static Task<string> GetError(dynamic request)
        {
            return (await IsError(request) ? request.error.ToString() : "");
        }
    }
}