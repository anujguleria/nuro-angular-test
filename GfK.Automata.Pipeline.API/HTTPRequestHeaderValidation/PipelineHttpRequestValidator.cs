﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

using GfK.Automata.Pipeline.API.HTTPRequestExtensions;

namespace GfK.Automata.Pipeline.API.HTTPRequestHeaderValidation
{
    public static class PipelineHttpRequestValidator
    {
        public async static Task<dynamic> GetRequestByName(object requestBody)
        {
            dynamic content = await HttpRequestValidator.GetRequestContentAsJson(requestBody);

            if (!(await HttpRequestValidator.IsError(content)))
            {
                int validationID;
                if (content.Property("ProjectID") == null || !(int.TryParse(content.ProjectID.ToString().Trim(), out validationID)))
                {
                    content = await HttpRequestValidator.SetErrorMessage("Required ProjectId value is null, missing or non-numeric.");
                }
                else
                {
                    if (content.Property("ParentID") == null)
                    {
                        content.ParentID = null;
                    }
                    else if (content.ParentID != null && !(int.TryParse(content.ParentID.ToString().Trim(), out validationID)))
                    {
                        content = await HttpRequestValidator.SetErrorMessage("ParentId appears to be non-numeric.");
                    }
                }
            }

            return content;
        }
    }
}