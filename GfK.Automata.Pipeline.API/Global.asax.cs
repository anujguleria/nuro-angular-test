﻿using GfK.Automata.Pipeline.API.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GfK.Automata.Pipeline.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Dependency Injection for API Controllers
            Bootstrapper.Run();

            // Avoid issues when an object refers to object(s) that themselves refer to the original object.  For
            // example a Pipeline has a Project property, Projects in turn have a collection of Pipelines.  Each
            // of these has a Project which has a collection of Pipelines ... etc. The below effectively tells 
            // the api to not spin into a self-referencing loop.
            HttpConfiguration configure = GlobalConfiguration.Configuration;
            configure.Formatters.JsonFormatter
                                .SerializerSettings
                                .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }
    }
}
