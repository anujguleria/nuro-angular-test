﻿using AutoMapper;
using GfK.Automata.Pipeline.API.ViewModels;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfK.Automata.Pipeline.API.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }
        //protected override void Configure()
        //{
        //    MapperConfiguration config = new MapperConfiguration(cfg => 
        //    {
        //        cfg.CreateMap<Project, ProjectViewModel>();
        //        cfg.CreateMap<User, UserViewModel>();
        //        cfg.CreateMap<ProjectUser, ProjectUserViewModel>();
        //        cfg.CreateMap<Model.Pipeline, PipelineViewModel>();
        //        cfg.CreateMap<PipelineInstance, PipelineInstanceViewModel>();
        //    });
        //}
    }
}