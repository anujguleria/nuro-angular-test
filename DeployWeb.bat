

REM - create temp folders if it doesn't exist
IF EXIST %1\APIDeploy GOTO APIDIRCREATED
mkdir %1\APIDeploy
:APIDIRCREATED

IF EXIST %1\WebDeploy GOTO WEBDIRCREATED
mkdir %1\WebDeploy
:WEBDIRCREATED
REM - run publish to temp folder
"E:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe" %1\GfK.Automata.Pipeline.API\GfK.Automata.Pipeline.API.csproj /p:DeployOnBuild=true /p:PublishProfile=DeployPublishProfile /p:VisualStudioVersion=15.0 /p:publishUrl=%1\APIDeploy /p:Configuration=%2

cd %1\GfK.Automata.Pipeline.Web\automata-web
call npm install
if %errorlevel% neq 0 exit /b %errorlevel%
REM - have to revert this package for some reason for now
call npm i enhanced-resolve@3.3.0
if %errorlevel% neq 0 exit /b %errorlevel%
call ng build --prod --env=%2 --base-href %4 --output-path %1\WebDeploy
if %errorlevel% neq 0 exit /b %errorlevel%



REM - not sure if we need this but just in case
if %errorlevel% neq 0 exit /b %errorlevel%

REM - stop service
%systemroot%\System32\sc \\nuew-aptango01.ext.gfk stop "GAP_%2"
REM - copy from temp folder
%systemroot%\System32\xcopy %1\APIDeploy %3\API /s /d /y
if %errorlevel% neq 0 (
	echo "error while copying api"
	exit /b %errorlevel%)
%systemroot%\System32\xcopy %1\WebDeploy %3 /s /d /y
if %errorlevel% neq 0 (
	echo "error while copying web"
	exit /b %errorlevel%)
REM - sleep for a minute to give win service time to shut down
%systemroot%\System32\ping 127.0.0.1 -n 60 > NUL
if %errorlevel% neq 0 (
	echo "error sleeping"
	exit /b %errorlevel%)
%systemroot%\System32\xcopy %1\GfK.Automata.Pipeline.Windows.Service\bin\%2 \\nuew-aptango01.ext.gfk\c$\Services\GAP\%2 /s /d /y
if %errorlevel% neq 0 (
	echo "error while copying win service"
	exit /b %errorlevel%)

REM - start service
%systemroot%\System32\sc \\nuew-aptango01.ext.gfk start "GAP_%2"
if %errorlevel% neq 0 (
	echo "error starting service"
	exit /b %errorlevel%)