﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    public interface IModule
    {
        ILogger<ModuleLogMessageType> Logger { get; set; }
        Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters);
    }
}
