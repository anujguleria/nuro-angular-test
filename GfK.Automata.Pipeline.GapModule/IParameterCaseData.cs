﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    public interface IParameterCaseData : IParameter, IParameterSerialize
    {
        string FilePath { get; set; }
    }
}
