﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public abstract class ParameterMetadataBase : ParameterBase, IParameterMetadata
    {
        public ParameterMetadataBase(string moduleName, string parameterName) :base(moduleName, parameterName)
        {

        }

        public dynamic Metadata { get; set; }

        public override async Task Serialize(string path)
        {
            await base.Serialize(path, this);
        }
    }
}
