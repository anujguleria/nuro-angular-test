﻿using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GfK.Automata.Pipeline.GapModule
{
    public interface IParameterValue<T> where T : class
    {
        Task<T> GetValue();
        Task SetValue(T value);

        Task SerializeYourself(T value);
    }
}
