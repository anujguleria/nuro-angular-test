﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    public interface IParameterDimensionsCapable : IParameter, IParameterSerialize
    {
        string DataFilePath { get; set; }
        DimensionDataFileType DimensionDataFileType { get; set; }
        string GetConnectionString();
        string MDDPath { get; set; }
    }
    public enum DimensionDataFileType
    {
        DDF,
        SPSS,
        Dimensions
    }
}
