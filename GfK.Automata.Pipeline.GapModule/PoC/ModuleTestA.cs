﻿using GfK.Automata.Pipeline.GapModule;
using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule.PoC
{
    public class ModuleTestA : IModule
    {
        public ILogger<ModuleLogMessageType> Logger { get; set; }

        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {            
            IParameterString param1 = (IParameterString)(await parameters.GetParameter("P3"));
            int p1Length = await param1.GetParameterValueLength();
            string p1Value = param1.Value;
            string param1Name = param1.Definition.Name;

            IParameter param1raw = await parameters.GetParameter("P3");
            string param1RawValue = ((IParameterString)param1raw).Value;

            Dictionary<string, string> values = new Dictionary<string, string>();
            foreach(KeyValuePair<string, IParameter> inputParameter in parameters.Parameters)
            {
                IParameter parameter = inputParameter.Value;

                if (parameter.GetType().Name == "ParameterString")
                {
                    values.Add(parameter.Definition.Name, ((IParameterString)parameter).Value);
                }

                if (parameter.GetType().Name == "ParameterScript")
                {
                    values.Add(parameter.Definition.Name, ((IParameterScript)parameter).Value);
                }
            }

            string fileName = @"C:\Code\taskinput_ModuleA_" + Guid.NewGuid().ToString() + ".txt";
            using (StreamWriter logWriter = new StreamWriter(fileName, append: false))
            {
                foreach(KeyValuePair<string, string> value in values)
                {
                    await logWriter.WriteLineAsync("Parameter " + value.Key + " has value '" + value.Value + "'");
                }

                SystemParameters systemParameters = parameters.SystemParameters;
                await logWriter.WriteLineAsync("Project ID: " + systemParameters.ProjectID.ToString());
            }

            IParameterValueCollection returnParameters = new ParameterValueCollection();

            IParameterString script = new ParameterString("ToolsTask", "P1");
            script.Value = param1RawValue + " - " + "String P1";
            await returnParameters.AddParameter(script);

            script = new ParameterString("ToolsTask", "P2");
            script.Value = param1RawValue + " - " + "String P2";
            await returnParameters.AddParameter(script);

            IParameterString stringValue = new ParameterString("ToolsTask", "P3");
            stringValue.Value = param1RawValue + " - " + "String P3";
            await returnParameters.AddParameter(stringValue);

            IParameterMetadata metadata = new ParameterMetadata("ToolsTask", "P4");
            metadata.Metadata = "{\"GroupName\" : \"Admin\"}";
            await returnParameters.AddParameter(metadata);

            await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "Completed run of ModuleTestA - " + returnParameters.Parameters.Count.ToString());

            await Task.Delay(20000);

            return returnParameters;
        }
    }
}
