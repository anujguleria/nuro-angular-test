﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule.PoC
{
    public class ModuleTestB : IModule
    {
        public ILogger<ModuleLogMessageType> Logger { get; set; }

        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            foreach (KeyValuePair<string, IParameter> inputParameter in parameters.Parameters)
            {
                IParameter parameter = inputParameter.Value;

                if (parameter.GetType().Name == "ParameterString")
                {
                    values.Add(parameter.Definition.Name, ((IParameterString)parameter).Value);
                }

                if (parameter.GetType().Name == "ParameterScript")
                {
                    values.Add(parameter.Definition.Name, ((IParameterScript)parameter).Value);
                }

                if (parameter.GetType().Name == "ParameterMetadata")
                {
                    values.Add(parameter.Definition.Name, (((IParameterMetadata)parameter).Metadata).ToString());
                }
            }

            string fileName = @"C:\Code\taskinput_ModuleB_" + Guid.NewGuid().ToString() + ".txt";
            using (StreamWriter logWriter = new StreamWriter(fileName, append: false))
            {
                foreach (KeyValuePair<string, string> value in values)
                {
                    await logWriter.WriteLineAsync("Parameter " + value.Key + " has value '" + value.Value + "'");
                }

                SystemParameters systemParameters = parameters.SystemParameters;
                await logWriter.WriteLineAsync("Project ID: " + systemParameters.ProjectID.ToString());
            }

            await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "Completed run of ModuleTestB - " + parameters.Parameters.Count.ToString());

            return new ParameterValueCollection();
        }
    }
}
