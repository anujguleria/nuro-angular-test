﻿using GfK.Automata.Pipeline.GapModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule.PoC
{
    public class ManageModule
    {

        public async Task RunModule()
        {
            IModule moduleTest = (IModule)Activator.CreateInstance("GfK.Automata.Pipeline.GapModule", "GfK.Automata.Pipeline.GapModule.PoC.ModuleTest").Unwrap();

            IParameterValueCollection parameters = new ParameterValueCollection();
            string paramString = "Param1String";
            string paramScript = "Param1Script";

            await parameters.AddParameterValue("ModuleTest", "Param1", "string", paramString);
            await parameters.AddParameterValue("ModuleTest", "Param2", "script", paramScript);

            IParameterValueCollection outputParameters = await moduleTest.RunTask(parameters);
        }
    }
}
