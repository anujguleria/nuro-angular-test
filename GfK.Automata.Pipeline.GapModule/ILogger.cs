﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    public interface ILogger<TEnum> where TEnum : struct
    {
        int PipelineTaskInstanceID { get; set; }

        void Log(TEnum level, string message);
        Task LogAsync(TEnum level, string message);
    }
}