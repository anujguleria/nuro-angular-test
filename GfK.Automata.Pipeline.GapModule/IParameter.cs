﻿using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    public interface IParameter
    {
        Parameter Definition { get; set; }
 
        Task<ParameterDirection> GetDirection();
        string Value { get; set; }
    }
}
