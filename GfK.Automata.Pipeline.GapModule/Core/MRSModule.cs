﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule.Core
{
    public class MRSModule : IModule
    {
        private string latestError = null;
        public ILogger<ModuleLogMessageType> Logger { get; set; }
        IParameterValueCollection returnParameters;
        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            string script = (await parameters.GetParameter("Script") == null ? null :
               ((IParameter)(await parameters.GetParameter("Script"))).Value);
            string scriptPath = (await parameters.GetParameter("ScriptPath") == null ? null :
               ((IParameter)(await parameters.GetParameter("ScriptPath"))).Value);
            // log parameters
            for (int i = 0; i < 5; i++)
            {
                IParameter parameter = await parameters.GetParameter("Argument" + i);
                if (parameter != null && parameter.Value != null && parameter.Value.Trim().Length > 0)
                {
                    await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "Argument" + i + " = " + parameter.Value);
                }
            }
            Process process = new Process();
            returnParameters = new ParameterValueCollection();
            process.StartInfo.WorkingDirectory = parameters.SystemParameters.TempFolder;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;

            string mrsFileName = Guid.NewGuid().ToString() + ".mrs";
            if (scriptPath != null && scriptPath.Trim().Length > 0)
            {
                FileInfo scriptFileInfo = new FileInfo(scriptPath);
                script = File.ReadAllText(scriptPath);
                mrsFileName = scriptFileInfo.Name;
            }
            script = parameters.Liquify(script);
            await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "script: " + script);
            string newPath = parameters.SystemParameters.TempFolder + Guid.NewGuid().ToString() + "\\";
            Directory.CreateDirectory(newPath);
            string newScriptFile = newPath + mrsFileName;
            File.WriteAllText(newScriptFile, script);
            process.StartInfo.FileName = "mrScriptCL.exe";
            process.StartInfo.Arguments = "\"" + newScriptFile + "\"";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.ErrorDataReceived += Process_ErrorDataReceived;
            process.OutputDataReceived += Process_OutputDataReceived;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            System.Threading.Thread.Sleep(60000);
            if (process.ExitCode != 0)
            {
                throw new Exception("exe exited with error code - " + process.ExitCode);
            }
            if (latestError != null)
            {
                throw new Exception(latestError);
            }
            process.Dispose();
            return returnParameters;
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                ModuleLogMessageType logType = ModuleLogMessageType.INFORMATIONAL;
                if (e.Data.ToUpper().Contains("ERROR"))
                {
                    latestError = e.Data;
                    logType = ModuleLogMessageType.ERROR;
                }
                else if (e.Data.ToUpper().Contains("WARNING"))
                {
                    logType = ModuleLogMessageType.WARNING;
                }
                ProcessLog(e.Data, logType).Wait();
            }
        }

        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                latestError = e.Data;
                ProcessLog(e.Data, ModuleLogMessageType.ERROR).Wait();
            }
        }
        private async Task ProcessLog(string log, ModuleLogMessageType logType)
        {
            if (log != null && log.Trim() != "")
            {
                if (log.StartsWith("PARAMETER~") && log.Split('~').Length == 3)
                {
                    if (Regex.IsMatch(log.Split('~')[1], "^return[1-5]$", RegexOptions.IgnoreCase))
                    {
                        string index = Regex.Match(log.Split('~')[1], "^return([1-5])$", RegexOptions.IgnoreCase).Groups[1].Value;
                        await returnParameters.AddParameter(new ParameterString("DimensionsModule", "Return" + index)
                        {
                            Value = log.Split('~')[2]
                        });
                    }
                    else
                    {
                        returnParameters.DynamicParameters[log.Split('~')[1]] = log.Split('~')[2];
                    }
                }
                else
                {
                    await Logger.LogAsync(logType, log);
                }
            }
        }
    }
}
