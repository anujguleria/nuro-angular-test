﻿using DotLiquid;
using DotLiquid.NamingConventions;
using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule.Core
{
    public class DMSModule : IModule
    {
        public ILogger<ModuleLogMessageType> Logger { get; set; }
        IParameterValueCollection returnParameters;
        private string latestError = null;
        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            string dmsScript = (await parameters.GetParameter("DMSScript") == null ? null :
               ((IParameter)(await parameters.GetParameter("DMSScript"))).Value);
            string dmsScriptFile = (await parameters.GetParameter("DMSScriptFile") == null ? null :
                ((IParameter)(await parameters.GetParameter("DMSScriptFile"))).Value);
            // log parameters
            for (int i = 0; i < 5; i++)
            {
                IParameter parameter = await parameters.GetParameter("Argument" + i);
                if (parameter != null && parameter.Value != null && parameter.Value.Trim().Length > 0)
                {
                    await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "Argument" + i + " = " + parameter.Value);
                }
            }
            string guid = Guid.NewGuid().ToString();
            string tempPath = parameters.SystemParameters.TempFolder + "\\" + guid;
            Directory.CreateDirectory(tempPath);
            if (dmsScriptFile != null && dmsScriptFile.Trim().Length != 0)
            {
                dmsScript = File.ReadAllText(dmsScriptFile);
            }
            string finalDMSScriptFile = tempPath + "\\script.dms";
            // dotliquid replace
            dmsScript = parameters.Liquify(dmsScript);
            await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "script: " + dmsScript);
            File.WriteAllText(finalDMSScriptFile, dmsScript);
            IParameterValueCollection returnParameters = new ParameterValueCollection();

            Process process = new Process();
            returnParameters = new ParameterValueCollection();
            process.StartInfo.WorkingDirectory = parameters.SystemParameters.TempFolder;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;

            process.StartInfo.FileName = "dmsrun.exe";
            process.StartInfo.Arguments = "\"" + finalDMSScriptFile + "\"";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.ErrorDataReceived += Process_ErrorDataReceived;
            process.OutputDataReceived += Process_OutputDataReceived;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            System.Threading.Thread.Sleep(60000);
            if (process.ExitCode != 0)
            {
                throw new Exception("exe exited with error code - " + process.ExitCode);
            }
            if (latestError != null)
            {
                throw new Exception(latestError);
            }
            process.Dispose();
            return returnParameters;




            /*
            DMSRunner runner = new DMSRunner(dmsScript, tempPath);
            runner.Save(finalDMSScriptFile);
            string validationString = runner.Validate("<BR/>");
            if (runner.GetErrors() > 0)
            {
                await ProcessLog(returnParameters, runner.GetLog());
                await Logger.LogAsync(ModuleLogMessageType.ERROR, validationString);
                throw new Exception("Dimensions Module Failed Validation - " + runner.GetSingleLogString("<BR/>"));
            }
            runner.Run();
            if (runner.GetErrors() > 0)
            {
                await ProcessLog(returnParameters, runner.GetLog());
                throw new Exception("Dimensions Module Failed Execution - " + runner.GetSingleLogString("<BR/>"));
            }
            await ProcessLog(returnParameters, runner.GetLog());*/
            if (latestError != null)
            {
                throw new Exception(latestError);
            }
            return returnParameters;
        }


        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                ModuleLogMessageType logType = ModuleLogMessageType.INFORMATIONAL;
                if (e.Data.ToUpper().Contains("ERROR") && !e.Data.Contains("0 Error(s)"))
                {
                    latestError = e.Data;
                    logType = ModuleLogMessageType.ERROR;
                }
                else if (e.Data.ToUpper().Contains("WARNING") && !e.Data.Contains("0 Warning(s)"))
                {
                    logType = ModuleLogMessageType.WARNING;
                }
                ProcessLog(e.Data, logType).Wait();
            }
        }

        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                latestError = e.Data;
                ProcessLog(e.Data, ModuleLogMessageType.ERROR).Wait();
            }
        }
        private async Task ProcessLog(string log, ModuleLogMessageType logType)
        {
            if (log != null && log.Trim() != "")
            {
                if (log.StartsWith("PARAMETER~") && log.Split('~').Length == 3)
                {
                    if (Regex.IsMatch(log.Split('~')[1], "^return[1-5]$", RegexOptions.IgnoreCase))
                    {
                        string index = Regex.Match(log.Split('~')[1], "^return([1-5])$", RegexOptions.IgnoreCase).Groups[1].Value;
                        await returnParameters.AddParameter(new ParameterString("DimensionsModule", "Return" + index)
                        {
                            Value = log.Split('~')[2]
                        });
                    }
                    else
                    {
                        returnParameters.DynamicParameters[log.Split('~')[1]] = log.Split('~')[2];
                    }
                }
                else
                {
                    await Logger.LogAsync(logType, log);
                }
            }
        }
        //private async Task ProcessLog(IParameterValueCollection returnParameters, string[] logLines)
        //{
        //    foreach (string log in logLines)
        //    {
        //        if (log.StartsWith("PARAMETER~") && log.Split('~').Length == 3)
        //        {
        //            if (Regex.IsMatch(log.Split('~')[1], "^return[1-5]$", RegexOptions.IgnoreCase))
        //            {
        //                string index = Regex.Match(log.Split('~')[1], "^return([1-5])$", RegexOptions.IgnoreCase).Groups[1].Value;
        //                await returnParameters.AddParameter(new ParameterString("DimensionsModule", "Return" + index)
        //                {
        //                    Value = log.Split('~')[2]
        //                });
        //            }
        //            else
        //            {
        //                returnParameters.DynamicParameters[log.Split('~')[1]] = log.Split('~')[2];
        //            }
        //        }
        //        else
        //        {
        //            ModuleLogMessageType logType = ModuleLogMessageType.INFORMATIONAL;
        //            if (log.ToUpper().Contains("ERROR"))
        //            {
        //                logType = ModuleLogMessageType.ERROR;
        //                latestError = log;
        //            }
        //            else if (log.ToUpper().Contains("WARNING"))
        //            {
        //                logType = ModuleLogMessageType.WARNING;
        //            }
        //            await Logger.LogAsync(logType, log);
        //        }
        //    }
        //}
    }
}
