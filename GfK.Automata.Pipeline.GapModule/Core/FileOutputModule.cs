﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule.Core
{
    public class FileOutputModule : IModule
    {
        public ILogger<ModuleLogMessageType> Logger { get; set; }
        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            try
            {
                string filePath = ((IParameter)(await parameters.GetParameter("InputFilePath"))).Value;
                string outputFilePath = ((IParameter)(await parameters.GetParameter("OutputPath"))).Value;
                bool overwrite = (await parameters.GetParameter("Overwrite")) == null || ((IParameter)(await parameters.GetParameter("Overwrite"))).Value.Trim() == "" ? false :
                    bool.Parse(((IParameter)(await parameters.GetParameter("Overwrite"))).Value);
                FileInfo file = new FileInfo(filePath);
                File.Copy(filePath, outputFilePath + "\\" + file.Name, overwrite);
                IParameterString pathParameter = new ParameterString("FileOutputModule", "OutputFilePath")
                {
                    Value = outputFilePath + "\\" + file.Name
                };
                IParameterValueCollection returnParameters = new ParameterValueCollection();
                await returnParameters.AddParameter(pathParameter);
                return returnParameters;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
