﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model.Domains;

namespace GfK.Automata.Pipeline.GapModule.Core
{
    public class EXEModule : IModule
    {
        private string latestError = null;
        public ILogger<ModuleLogMessageType> Logger { get; set; }
        IParameterValueCollection returnParameters;

        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            string command = (await parameters.GetParameter("Command") == null ? null :
               ((IParameter)(await parameters.GetParameter("Command"))).Value);
            string scriptPath = (await parameters.GetParameter("ScriptPath") == null ? null :
               ((IParameter)(await parameters.GetParameter("ScriptPath"))).Value);
            // log parameters
            for (int i = 0; i < 5; i++)
            {
                IParameter parameter = await parameters.GetParameter("Argument" + i);
                if (parameter != null && parameter.Value != null && parameter.Value.Trim().Length > 0)
                {
                    await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "Argument" + i + " = " + parameter.Value);
                }
            }
            returnParameters = new ParameterValueCollection();
            List<Tuple<string, string>> commands = new List<Tuple<string, string>>();
            if (scriptPath != null && scriptPath.Trim().Length > 0)
            {
                FileInfo scriptFileInfo = new FileInfo(scriptPath);
                string script = File.ReadAllText(scriptPath);
                script = parameters.Liquify(script);
                string newPath = parameters.SystemParameters.TempFolder + Guid.NewGuid().ToString() + "\\";
                Directory.CreateDirectory(newPath);
                string newScriptFile = newPath + scriptFileInfo.Name;
                File.WriteAllText(newScriptFile, script);
                commands.Add(new Tuple<string, string>(newScriptFile, ""));
            }
            else
            {
                command = parameters.Liquify(command);
                string newPath = parameters.SystemParameters.TempFolder + Guid.NewGuid().ToString() + "\\";
                Directory.CreateDirectory(newPath);
                string newScriptFile = newPath + "\\script.bat";
                File.WriteAllText(newScriptFile, command);
                commands.Add(new Tuple<string, string>(newScriptFile, ""));
                /*foreach (string line in Regex.Split(command, "\r\n|\r|\n"))
                {
                    commands.Add(new Tuple<string, string>("cmd.exe", "/c " + line));
                }*/
            }
            foreach (Tuple<string, string> tuple in commands)
            {
                Process process = new Process();
                process.StartInfo.WorkingDirectory = parameters.SystemParameters.TempFolder;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.FileName = tuple.Item1;
                process.StartInfo.Arguments = tuple.Item2;
                process.ErrorDataReceived += Process_ErrorDataReceived;
                process.OutputDataReceived += Process_OutputDataReceived;
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();
                System.Threading.Thread.Sleep(60000);
                if (process.ExitCode != 0)
                {
                    throw new Exception("exe exited with error code - " + process.ExitCode);
                }
                process.Dispose();
            }
            if (latestError != null)
            {
                throw new Exception(latestError);
            }
            return returnParameters;
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                ModuleLogMessageType logType = ModuleLogMessageType.INFORMATIONAL;
                if (e.Data.ToUpper().Contains("ERROR"))
                {
                    logType = ModuleLogMessageType.ERROR;
                    latestError = e.Data;
                }
                else if (e.Data.ToUpper().Contains("WARNING"))
                {
                    logType = ModuleLogMessageType.WARNING;
                }
                ProcessLog(e.Data, logType).Wait();
            }
        }

        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                latestError = e.Data;
                ProcessLog(e.Data, ModuleLogMessageType.ERROR).Wait();
            }
        }
        private async Task ProcessLog(string log, ModuleLogMessageType logType)
        {
            if (log != null && log.Trim() != "")
            {
                if (log.StartsWith("PARAMETER~") && log.Split('~').Length == 3)
                {
                    if (Regex.IsMatch(log.Split('~')[1], "^return[1-5]$", RegexOptions.IgnoreCase))
                    {
                        string index = Regex.Match(log.Split('~')[1], "^return([1-5])$", RegexOptions.IgnoreCase).Groups[1].Value;
                        await returnParameters.AddParameter(new ParameterString("DimensionsModule", "Return" + index)
                        {
                            Value = log.Split('~')[2]
                        });
                    }
                    else
                    {
                        returnParameters.DynamicParameters[log.Split('~')[1]] = log.Split('~')[2];
                    }
                }
                else
                {
                    await Logger.LogAsync(logType, log);
                }
            }
        }
    }
}
