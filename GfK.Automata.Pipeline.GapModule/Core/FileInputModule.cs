﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule.Core
{
    public class FileInputModule : IModule
    {
        public ILogger<ModuleLogMessageType> Logger { get; set; }
        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            string filePath = ((IParameter)(await parameters.GetParameter("InputFilePath"))).Value;
            FileInfo file = new FileInfo(filePath);
            string guid = Guid.NewGuid().ToString();
            string tempPath = parameters.SystemParameters.TempFolder + "\\" + guid;
            Directory.CreateDirectory(tempPath);
            File.Copy(filePath, tempPath + "\\" + file.Name);
            IParameterString pathParameter = new ParameterString("FileInputModule", "OutputFilePath")
            {
                Value = tempPath + "\\" + file.Name
            };
            IParameterValueCollection returnParameters = new ParameterValueCollection();
            await returnParameters.AddParameter(pathParameter);
            return returnParameters;
        }
    }
}
