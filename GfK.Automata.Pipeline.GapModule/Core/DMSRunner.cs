﻿using System;
using System.Collections.Specialized;
using System.IO;
using SPSSMR.Data.Transformations;

namespace GfK.Automata.Pipeline.GapModule.Core
{
    /// <summary>
    /// Summary description for DMSRunner.
    /// </summary>
    public class DMSRunner
    {
        private Job _Job;
        private string _Script;
        private string _TempDirectory;
        private StringCollection Log = new StringCollection();
        private int _RecordCount = 0;
        private DateTime _Start, _End;

        public DMSRunner(string Script, string TempDirectory)
        {
            _Script = Script;
            _TempDirectory = TempDirectory;
            _Job = new Job();

            _Job.AfterJobEnd += new System.EventHandler(_Job_AfterJobEnd);
            _Job.BadCase += new System.EventHandler(_Job_BadCase);
            _Job.BeforeJobStart += new System.EventHandler(_Job_BeforeJobStart);
            _Job.JobEnd += new System.EventHandler(_Job_JobEnd);
            _Job.JobStart += new System.EventHandler(_Job_JobStart);
            _Job.JobStatus += new System.EventHandler(_Job_JobStatus);
            _Job.NextCase += new System.EventHandler(_Job_NextCase);

            _Job.LoadString(_Script, null);
            _Job.TempDirectory = _TempDirectory;
        }

        public void Save(string Path)
        {
            StreamWriter Writer = null;

            try
            {
                Writer = new StreamWriter(Path, false);
                Writer.WriteLine(_Script);
                Writer.Flush();
            }
            finally
            {
                if (Writer != null)
                {
                    Writer.Close();
                    Writer = null;
                }
            }
        }

        public string Validate(string LineFeed)
        {
            try
            {
                System.Text.StringBuilder Out = new System.Text.StringBuilder(100);
                StringCollection Output = _Job.Validate();
                for (int i = 0; i < Output.Count; i++)
                {
                    Out.Append(DateTime.Now.ToShortTimeString() + " - " + Output[i] + LineFeed);
                }
                return Out.ToString();
            }
            catch (Exception Ex)
            {
                Log.Add(" !! ERROR VALIDATING !! - " + Ex.Message);
                throw new Exception("VALIDATION ERROR", Ex);
            }
        }
        public bool Run()
        {
            if (_Job != null && _Job.IsParsed)
            {
                try
                {
                    _Job.Run();
                    return true;
                }
                catch (Exception Ex)
                {
                    Log.Add(" !! ERROR !! - " + Ex.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public long GetWarnings()
        {
            if (_Job != null)
            {
                return _Job.WarningCount;
            }
            else
            {
                return -1;
            }
        }

        public long GetErrors()
        {
            if (_Job != null)
            {
                return _Job.ErrorCount;
            }
            else
            {
                return -1;
            }
        }

        public string[] GetLog()
        {
            string[] aOutput = new string[Log.Count];
            Log.CopyTo(aOutput, 0);
            return aOutput;
        }

        public string GetSingleLogString(string LineBreak)
        {
            string[] aOutput = new string[Log.Count];
            Log.CopyTo(aOutput, 0);
            System.Text.StringBuilder Builder = new System.Text.StringBuilder(aOutput.Length * 20);
            for (int i = 0; i < aOutput.Length; i++)
            {
                Builder.Append(aOutput[i]);
                Builder.Append(LineBreak);
            }
            return Builder.ToString();
        }

        private void _Job_AfterJobEnd(object sender, EventArgs e)
        {
            Log.Add(DateTime.Now.ToShortTimeString() + " - Job Ended");
            Log.Add(DateTime.Now.ToShortTimeString() + " Elasped execution time - " + (_End - _Start));
            Log.Add(DateTime.Now.ToShortTimeString() + " Total records = " + _RecordCount);
        }

        private void _Job_BadCase(object sender, EventArgs e)
        {
            Log.Add(DateTime.Now.ToShortTimeString() + " - Error Writing Record (" + _RecordCount + ")");
            if (e is SPSSMR.Data.Transformations.JobEventArgs)
            {
                Log.Add(((SPSSMR.Data.Transformations.JobEventArgs)e).ActivityDescription);
            }
        }

        private void _Job_BeforeJobStart(object sender, EventArgs e)
        {
            Log.Add(DateTime.Now.ToShortTimeString() + 
                (e is SPSSMR.Data.Transformations.JobEventArgs ? " - " + ((SPSSMR.Data.Transformations.JobEventArgs)e).ActivityDescription : ""));
        }

        private void _Job_JobEnd(object sender, EventArgs e)
        {
            _End = DateTime.Now;
            Log.Add(DateTime.Now.ToShortTimeString() + " - Job Ending");
        }

        private void _Job_JobStart(object sender, EventArgs e)
        {
            _RecordCount = 0;
            _Start = DateTime.Now;
            Log.Add(DateTime.Now.ToShortTimeString() + " - Job Starting");
        }

        private void _Job_JobStatus(object sender, EventArgs e)
        {
            Log.Add(DateTime.Now.ToShortTimeString() +
                (e is SPSSMR.Data.Transformations.JobEventArgs ? " - " + ((SPSSMR.Data.Transformations.JobEventArgs)e).ActivityDescription : ""));
        }

        private void _Job_NextCase(object sender, EventArgs e)
        {
            _RecordCount++;
            if (_RecordCount <= 100 && ((_RecordCount % 10) == 0))
            {
                Log.Add(DateTime.Now.ToShortTimeString() + " - Writing Record " + _RecordCount);
            }
            else if (_RecordCount > 100 && ((_RecordCount % 100) == 0))
            {
                Log.Add(DateTime.Now.ToShortTimeString() + " - Writing Record " + _RecordCount);
            }
        }
    }
}
