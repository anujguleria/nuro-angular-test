﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model.Domains;

namespace GfK.Automata.Pipeline.GapModule.Core
{
    public class DimensionsModule : IModule
    {
        public ILogger<ModuleLogMessageType> Logger { get; set; }

        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            string survey = ((IParameter)(await parameters.GetParameter("SurveyName"))).Value;
            string version = 
                ((await parameters.GetParameter("Version"))) == null ? "{..}" : 
                ((IParameter)(await parameters.GetParameter("Version"))).Value;
            string cluster = ((IParameter)(await parameters.GetParameter("Cluster"))).Value;
            string guid = Guid.NewGuid().ToString();
            string tempPath = parameters.SystemParameters.TempFolder + "\\" + guid;
            Directory.CreateDirectory(tempPath);
            // get mdd file
            File.Copy(ConfigurationManager.AppSettings["Cluster_" + cluster + "_FMROOT"] + "\\" + survey + "\\" + survey + ".mdd", tempPath + "\\" + survey + "_in.mdd");
            string dms = GetDMSScript(survey, version, cluster, tempPath);
            string dmsFile = tempPath + "\\" + survey + ".dms";
            File.WriteAllText(dmsFile, dms);
            IParameterValueCollection returnParameters = new ParameterValueCollection();
            DMSRunner runner = new DMSRunner(dms, tempPath);
            runner.Save(dmsFile);
            runner.Validate("<BR/>");
            if (runner.GetErrors() > 0)
            {
                await ProcessLog(returnParameters, runner.GetLog());
                throw new Exception("Dimensions Module Failed Validation - " + runner.GetSingleLogString("<BR/>"));
            }
            runner.Run();
            if (runner.GetErrors() > 0)
            {
                await ProcessLog(returnParameters, runner.GetLog());
                throw new Exception("Dimensions Module Failed Execution - " + runner.GetSingleLogString("<BR/>"));
            }
            await ProcessLog(returnParameters, runner.GetLog());
            await returnParameters.AddParameter(new ParameterString("DimensionsModule", "DimensionsDataFile")
            {
                Value = tempPath + "\\" + survey + ".ddf"
            });
            await returnParameters.AddParameter(new ParameterString("DimensionsModule", "MDDFile")
            {
                Value = tempPath + "\\" + survey + "_out.mdd"
            });
            return returnParameters;
        }
        private async Task ProcessLog(IParameterValueCollection returnParameters, string[] logLines)
        {
            foreach (string log in logLines)
            {
                if (log.StartsWith("PARAMETER~") && log.Split('~').Length == 3)
                {
                    returnParameters.DynamicParameters[log.Split('~')[1]] = log.Split('~')[2];
                }
                else
                {
                    ModuleLogMessageType logType = ModuleLogMessageType.INFORMATIONAL;
                    if (log.StartsWith("ERROR"))
                    {
                        logType = ModuleLogMessageType.ERROR;
                    }
                    else if (log.StartsWith("WARNING"))
                    {
                        logType = ModuleLogMessageType.WARNING;
                    }
                    await Logger.LogAsync(logType, log);
                }
            }
        }
        private string GetDMSScript(string survey, string version, string cluster, string tempFileLocation)
        {
            StringBuilder dms = new StringBuilder();
            if (version == null || version.Trim() == "")
            {
                version = "{..}";
            }
            string crlf = "\r\n";
            string select = "Select * from vdata";


            dms.Append("#define MySelectQuery \"" + select + "\"" + crlf);

            dms.Append(crlf + crlf);

            // TODO 
            // add proper support for clusters via the cluster key and proper config settings
            dms.Append("InputDatasource(myInputDataSource, \"RDBInput.dms\")" + crlf);
            dms.Append("    ConnectionString = \"Provider=mrOleDB.Provider.2;Data Source=" + "mrRdbDsc2" + ";Location=\"Provider=SQLOLEDB.1;Data Source="
                + ConfigurationManager.AppSettings["Cluster_" + cluster + "_SQL"] + ";Initial Catalog=" + survey + ";UID=pmswebuser;PWD=Webuserpm5;\";Initial Catalog=" + tempFileLocation + @"\" + survey + "_in.mdd;MR Init MDM Version=" + version + ";MR Init Project=" + survey + "\"" + crlf);
            //"Provider=mrOleDB.Provider.2;Data Source=mrRdbDsc2;Location="Provider=SQLOLEDB.1;Password=il2wfps!;Persist Security Info=True;User ID=nop_dba;Initial Catalog=JPAUTO2;Data Source=MRSQLVR01";Initial Catalog=\\Nas01\wrdataservices\Work\ExportQuantum\1f128f36-5756-4ef0-ac3e-40270f154ecd-44833\JPAUTO2\JPAUTO2.mdd;MR Init MDM Version={..};MR Init Project=JPAUTO2"
            dms.Append("    SelectQuery = MySelectQuery" + crlf);
            dms.Append("End InputDatasource" + crlf);

            dms.Append(crlf + crlf);

            dms.Append("OutputDataSource(Output, \"DDFOutput\")" + crlf);
            dms.Append("   ConnectionString = \"Provider=mrOleDB.Provider.2;Data Source=mrDataFileDsc;Location=" + tempFileLocation + @"\" + survey + ".ddf\"" + crlf);
            dms.Append("   MetaDataOutputName = " + tempFileLocation + @"\" + survey + "_out.mdd " + crlf);
            dms.Append("End OutputDataSource" + crlf);
            return dms.ToString();
        }
    }
}
