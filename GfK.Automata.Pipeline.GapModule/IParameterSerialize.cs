﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    public interface IParameterSerialize
    {
        Task Serialize(string path, object value);
        Task Serialize(string path);
        Task<object> Deserialize(string path);
    }
}
