﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public abstract class ParameterDimensionsCapableBase : ParameterBase, IParameterDimensionsCapable
    {
        public ParameterDimensionsCapableBase(string moduleName, string parameterName) : base(moduleName, parameterName)
        {

        }

        public string DataFilePath { get; set; }
        public DimensionDataFileType DimensionDataFileType { get; set; }
        public string MDDPath { get; set; }

        public string GetConnectionString()
        {
            throw new NotImplementedException();
        }

        public override async Task Serialize(string path)
        {
            await base.Serialize(path, this);
        }
    }
}
