﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public class ParameterScriptBase : ParameterBase, IParameterScript
    {
        public ParameterScriptBase(string moduleName, string parameterName) :base(moduleName, parameterName)
        {

        }

        public override async Task Serialize(string path)
        {
            await base.Serialize(path, this);
        }
    }
}
