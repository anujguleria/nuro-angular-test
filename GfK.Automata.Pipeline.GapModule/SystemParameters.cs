﻿using System;
using System.IO;
using System.Configuration;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public class SystemParameters
    {
        public SystemParameters()
        {
            do
            {
                TempFolder = (ConfigurationManager.AppSettings["TempFolderPath"] == null ? Path.GetTempPath() : ConfigurationManager.AppSettings["TempFolderPath"])
                    + Guid.NewGuid().ToString();
            }
            while (File.Exists(TempFolder));
            Directory.CreateDirectory(TempFolder);
        }
        public string TempFolder { get; set; }

        public int ProjectID { get; set; }
        public string GetNewTempFolder()
        {
            string guid;
            string tempPath;
            do
            {
                guid = Guid.NewGuid().ToString();
                tempPath = TempFolder + "\\" + guid;
            } while (Directory.Exists(tempPath));
            Directory.CreateDirectory(tempPath);
            return tempPath;
        }
    }
}