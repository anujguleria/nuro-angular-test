﻿using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.GapModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public class ParameterString : ParameterStringBase
    {
        public ParameterString(string moduleName, string parameterName) : base(moduleName, parameterName)
        {

        }
    }
}
