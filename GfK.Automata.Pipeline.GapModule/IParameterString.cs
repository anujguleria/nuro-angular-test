﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    public interface IParameterString : IParameter, IParameterSerialize
    {
        string Value { get; set; }

        Task<int> GetParameterValueLength();
    }
}
