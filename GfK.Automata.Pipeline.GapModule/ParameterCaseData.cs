﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public class ParameterCaseData : ParameterCaseDataBase
    {
        public ParameterCaseData(string moduleName, string parameterName) : base(moduleName, parameterName)
        {
        }
    }
}
