﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using Newtonsoft.Json.Linq;
using DotLiquid;
using DotLiquid.NamingConventions;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public abstract class ParameterValueCollectionBase : IParameterValueCollection
    {
        public ParameterValueCollectionBase()
        {
            Parameters = new Dictionary<string, IParameter>();
            SystemParameters = new SystemParameters();
            DynamicParameters = new Dictionary<string, string>();
        }
        public string Liquify(string templateCode)
        {
            Template template = Template.Parse(templateCode);  // Parses and compiles the template
            Template.NamingConvention = new CSharpNamingConvention();
            var drop = new 
            {
                DynamicParameters = new Dictionary<string, object>(),
                StaticParameters = new Dictionary<string, object>(),
                SystemParameters = new Dictionary<string, object>(),
                DynamicParameterList = new List<object>(),
                StaticParameterList = new List<object>(),
                SystemParameterList = new List<object>()
            };
            drop.SystemParameters["TempFolder"] = SystemParameters.TempFolder;
            drop.SystemParameterList.Add(new { Name = "TempFolder", Value = SystemParameters.TempFolder });
            drop.SystemParameters["ProjectID"] = SystemParameters.ProjectID.ToString();
            drop.SystemParameterList.Add(new { Name = "ProjectID", Value = SystemParameters.ProjectID.ToString() });
            foreach (string key in Parameters.Keys)
            {
                drop.StaticParameters[key] = Parameters[key].Value;
                drop.StaticParameterList.Add(new { name = key, value = Parameters[key].Value });
            }
            foreach (string key in DynamicParameters.Keys)
            {
                drop.DynamicParameters[key] = DynamicParameters[key];
                drop.DynamicParameterList.Add(new { name = key, value = DynamicParameters[key] });
            }
            return template.Render(Hash.FromAnonymousObject(drop));
        }

        //Dictionary<string, IParameter> Parameters { get; set; }
        public Dictionary<string, IParameter> Parameters { get; set; }

        public Dictionary<string, string> DynamicParameters { get; set; }
        public SystemParameters SystemParameters { get; set; }

        public async Task AddParameter(IParameter parameter)
        {
            Parameters[parameter.Definition.Name] = parameter;
        }
        public async Task AddParameterValue(string moduleName, string parameterName, string parameterType, object value)
        {
            if (String.Equals(parameterType, "String", StringComparison.OrdinalIgnoreCase))
            {
                IParameterString parameterString = new ParameterString(moduleName, parameterName);
                parameterString.Value = (string)value;
                Parameters[parameterName] = parameterString;
            }

            if (String.Equals(parameterType, "Script", StringComparison.OrdinalIgnoreCase))
            {
                IParameterScript parameterScript = new ParameterScript(moduleName, parameterName);
                parameterScript.Value = (string)value;
                Parameters[parameterName] = parameterScript;
            }
        }

        public async virtual Task<Dictionary<string, Parameter>> GetParameterDefinitions()
        {
            Dictionary<string, Parameter> parameters = new Dictionary<string, Parameter>();

            foreach(string parameterName in Parameters.Keys)
            {
                parameters.Add(parameterName, Parameters[parameterName].Definition);
            }

            return parameters;
        }

        public async virtual Task<Dictionary<string, string>> GetParameterTypes()
        {
            throw new NotImplementedException();
        }

        public async Task<Dictionary<string, IParameter>> GetParameters()
        {
            return Parameters;
        }

        public async Task<IParameter> GetParameter(string name)
        {
            if (Parameters.ContainsKey(name))
            {
                return Parameters[name];
            }

            return null;
        }
    }
}
