﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public abstract class ParameterStringBase : ParameterBase, IParameterString
    {
        public ParameterStringBase(string moduleName, string parameterName) : base(moduleName, parameterName)
        {

        }

        public async Task<int> GetParameterValueLength()
        {
            return Value.Length;
        }

        public override async Task Serialize(string path)
        {
            await base.Serialize(path, this);
        }
    }
}
