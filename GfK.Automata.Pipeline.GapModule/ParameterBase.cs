﻿using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public abstract class ParameterBase : IParameter, IParameterSerialize
    {
        public ParameterBase(string moduleName, string parameterName)
        {
            Definition = new Parameter() { Name = parameterName };
        }

        public string Value { get; set; }


        public Parameter Definition { get; set; }

        public virtual async Task<ParameterDirection> GetDirection()
        {
            if (Definition == null)
            {
                throw new NullReferenceException("No underlying Parameter has been defined.");
            }
            else
            {
                return (ParameterDirection)Enum.Parse(typeof(ParameterDirection), Definition.Direction);
            }                     
        }

        public async Task Serialize(string path, object value)
        {
            try
            {
                using (Stream serializerStream = File.Open(path, FileMode.Create))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(serializerStream, value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task<object> Deserialize(string path)
        {
            try
            {
                using (Stream deserializerStream = File.Open(path, FileMode.Open))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    object deserialized = formatter.Deserialize(deserializerStream);

                    return deserialized;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public abstract Task Serialize(string path);
    }
}
