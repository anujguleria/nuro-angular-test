﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model.Domains;

namespace GfK.Automata.Pipeline.GapModule.InternalSystem
{
    public class Stub : IModule
    {
        public ILogger<ModuleLogMessageType> Logger { get; set; }

        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            return parameters;
        }
    }
}
