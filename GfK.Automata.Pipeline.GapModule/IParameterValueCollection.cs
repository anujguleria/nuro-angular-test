﻿using DotLiquid;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    public interface IParameterValueCollection
    {
        Dictionary<string, IParameter> Parameters { get; set; }

        Task<Dictionary<string, Parameter>> GetParameterDefinitions();
        Task<Dictionary<string, IParameter>> GetParameters();
        Task<IParameter> GetParameter(string name);
        Task<Dictionary<string, string>> GetParameterTypes();
        Task AddParameterValue(string moduleName, string parameterName, string parameterType, object value);
        Task AddParameter(IParameter parameter);
        Dictionary<string, string> DynamicParameters { get; set; }
        SystemParameters SystemParameters { get; set; }
        string Liquify(string templateCode);
    }
}
