﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.GapModule
{
    [Serializable()]
    public class ParameterMetadata : ParameterMetadataBase
    {
        public ParameterMetadata(string moduleName, string parameterName) : base(moduleName, parameterName)
        {

        }
    }
}
