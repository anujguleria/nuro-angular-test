﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Service;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.CLTools.EnhancedSPSSCreator
{
    class Options
    {
        [Option('c', "context", Required = false, HelpText = "Context to use for labels when creating a new layout file.")]
        public string Context { get; set; }

        [Option('l', "language", Required = false, HelpText = "Language to use for labels when creating a new layout file.")]
        public string Language { get; set; }

        [Option('m', "mdd", Required = true, HelpText = "Input MDD file.")]
        public string MDD { get; set; }

        [Option('d', "ddf", Required = true, HelpText = "Input DDF file.")]
        public string DDF { get; set; }

        [Option('s', "sav", Required = true, HelpText = "Output SAV file.")]
        public string SAV { get; set; }

        [Option('v', "version", DefaultValue = "{..}", Required = false, HelpText = "MDD file version." )]
        public string Version { get; set; }

        [Option('x', "xlsx", Required = true, HelpText = "XLSX layout file to create/read.")]
        public string XLSX { get; set; }

        [Option("create", HelpText = "Create new XLSX layout file.", MutuallyExclusiveSet = "create")]
        public bool Create { get; set; }

        [Option("read", HelpText = "Read XLSX layout file.", MutuallyExclusiveSet = "read")]
        public bool Read { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args,options))
            {
                ProcessorConfiguration conf = new ProcessorConfiguration();
                conf.Add("LayoutFilePath", options.XLSX);
                conf.Add("SAVOutputPath", options.SAV);
                conf.Add("DDFFilePath", options.DDF);
                conf.Add("MDDFilePath", options.MDD);
                if (options.Create)
                {
                    conf.Add("Context", options.Context);
                    conf.Add("Language", options.Language);
                    conf.Add("GenerateLayout", true);
                }
                else
                {
                    conf.Add("GenerateLayout", false);
                }
                Console.WriteLine("Reading metadata.");
                MetadataDocument document = MDMReader.Read(options.MDD, options.Version, MetadataSourceType.stDimensionsMDD);
                conf.Add("Document", document);
                Console.WriteLine("Processing data.");
                DimEnhancedSPSSProcessor.Process(conf);
                Console.WriteLine("Finished.");
            }
        }
    }
}
