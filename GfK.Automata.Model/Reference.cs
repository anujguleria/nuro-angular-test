﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Model
{
    public class Reference : MetadataObject
    {
        public Reference() { }

        public Reference(string name, MetadataDocument document)
        {
            Name = name;
            Document = document;
            IsReference = true;
            ObjectType = Model.ObjectType.otReference;
        }
    }
}
