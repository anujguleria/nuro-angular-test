﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace GfK.Automata.Model
{
    public class DefinedList : Container
    {
        public DefinedList() { }

        public DefinedList( string name ) : base( name )
        {
            // inheritance was here
            ObjectType = ObjectType.otDefinedList;
        }
    }
}
