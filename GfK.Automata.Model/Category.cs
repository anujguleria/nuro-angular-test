﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Model
{
    public class Category : MetadataObject, ILabeledMetadataObject
    {
        public Labels Labels { get; private set; }

        public Category() { }

        public Category( string name )
        {
            Name = name;
            _items = new List<MetadataObject>();
            Labels = new Labels();
            ObjectType = Model.ObjectType.otCategory;
            Properties = new Dictionary<string, dynamic>();
        }

        public void AddLabel( string context, string language, string text, MetadataDocument d )
        {
            if ( !d.ContainsText( text ) )
            {
                d.AddText(text);
            }
            int index = d.IndexOfText(text);
            Labels.Add(new Label { Context = context, Language = language, Text = index });
        }

        public new void Add(MetadataObject item )
        {
            throw new NotImplementedException();
        }

        public HashSet<string> Keywords( MetadataDocument document )
        {
            string s = Normalize( document );
            string[] a = s.Split();
            HashSet<string> l = new HashSet<string>(a);
            return l;
        }

        public string Label(string context, string language, MetadataDocument document)
        {
            return Labels.Get(context, language, document, true);
        }
        public string Label(string context, string language, MetadataDocument document, bool escape)
        {
            return Labels.Get(context, language, document, escape);
        }
        public string Normalize( MetadataDocument document )
        {
            return Normalize(document.CurrentContext, document.CurrentLanguage, document);
        }

        public string Normalize( string context, string language, MetadataDocument document )
        {
            return Labels.Normalize(context, language, document);
        }

        public bool IsOtherCategory()
        {

            // should use a more robust method of determining this from Metadata if possible
            // for now, using category names and/or labels
            switch (Name.ToUpper())
            {
                case "OTHER":
                case "OTHERS":
                    return true;
            }
            
            return false;
        }
        public static bool IsOtherCategory(string label)
        {
            switch (label.ToUpper())
            {
                case "OTHER":
                case "OTHERS":
                    return true;
            }

            return false;
        }
    }
}
