﻿using System.IO;
using Newtonsoft.Json;

namespace GfK.Automata.Model
{
    public class MetadataDocumentReader
    {
        static MetadataDocumentReader() { }

        static public MetadataDocument Read( string path )
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new MetadataDocumentContractResolver(),
                Formatting = Formatting.Indented,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                TypeNameHandling = TypeNameHandling.Auto
            };
            return (MetadataDocument)JsonConvert.DeserializeObject<MetadataDocument>(File.ReadAllText(path), settings);
        }
    }
}
