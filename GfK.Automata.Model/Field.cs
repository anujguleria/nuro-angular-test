﻿using System.Collections.Generic;
using MDMLib;

namespace GfK.Automata.Model
{
    public abstract class Field : MetadataObject, ILabeledMetadataObject
    {
        protected DataType _datatype;

        public DataType DataType { get { return _datatype; } set {} }
        public dynamic EffectiveMaxValue { get; protected set; }
        public dynamic EffectiveMinValue { get; protected set; }
        public string Expression { get; protected set; }
        public bool HasCaseData { get; protected set; }
        public Labels Labels { get; protected set; }
        public dynamic MaxValue { get; protected set; }
        public dynamic MinValue { get; protected set; }

        public Field() { }

        public HashSet<string> Keywords(MetadataDocument document)
        {
            string s = Normalize(document);
            string[] a = s.Split();
            HashSet<string> l = new HashSet<string>(a);
            return l;
        }

        public void AddLabel( string context, string language, string text, MetadataDocument document)
        {
            Labels.Add(context, language, text, document);
        }

        public string Label(string context, string language, MetadataDocument document)
        {
            return Labels.Get(context, language, document, true);
        }
        public string Label(string context, string language, MetadataDocument document, bool escape)
        {
            return Labels.Get(context, language, document, escape);
        }

        public string Normalize(MetadataDocument document)
        {
            return Normalize(document.CurrentContext, document.CurrentLanguage, document);
        }

        public string Normalize(string context, string language, MetadataDocument document)
        {
            return Labels.Normalize(context, language, document);
        }

        public void SetExpression(string expression)
        {
            Expression = expression;
        }

    }
}
