﻿using System.Collections.Generic;
using MDMLib;

namespace GfK.Automata.Model
{
    public class UnboundField : Field
    {
        public UnboundField() { }

        public UnboundField(string name, DataType type, dynamic effectiveminvalue, dynamic effectivemaxvalue, string expression, bool hascasedata, bool isderived, bool isreference, bool issystem, dynamic minvalue, dynamic maxvalue)
        {
            Name = name;
            _datatype = type;
            EffectiveMaxValue = effectivemaxvalue;
            EffectiveMinValue = effectiveminvalue;
            Expression = expression;
            HasCaseData = hascasedata;
            IsDerived = isderived;
            IsReference = isreference;
            IsSystem = issystem;
            Labels = new Model.Labels();
            MaxValue = maxvalue;
            MinValue = minvalue;
            ObjectType = Model.ObjectType.otField;
            Properties = new Dictionary<string, dynamic>();
        }
    }
}
