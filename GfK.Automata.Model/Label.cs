﻿using System.Collections.Generic;

namespace GfK.Automata.Model
{
    public class Label
    {
        public string Context { get; set; }
        public string Language { get; set; }
        public int Text { get; set; }

        public Label() { }
    }
}
