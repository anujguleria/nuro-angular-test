﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace GfK.Automata.Model
{
    public enum ObjectType
    {
        otCategory,
        otCollection,
        otDefinedList,
        otDocument,
        otField,
        otReference
    }

    public interface IMetadataObject
    {
    }
}
