﻿using System.Collections.Generic;

namespace GfK.Automata.Model
{
    public interface ILabeledMetadataObject
    {
        void AddLabel(string context, string language, string text, MetadataDocument document);
        HashSet<string> Keywords(MetadataDocument document);
        string Label(string context, string language, MetadataDocument document);
        string Normalize(MetadataDocument document);
        string Normalize(string context, string language, MetadataDocument document);
    }
}
