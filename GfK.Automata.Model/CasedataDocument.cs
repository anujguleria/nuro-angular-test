﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Model
{
    public enum CasedataSourceType
    {
        stCSV,
        stDimensionsADO,
        stDimensionsCSV,
        stDimensionsLog,
        stDimensionsQV,
        stDimensionsSample,
        stDimensionsSAV,
        stDimensionsSC,
        stDimensionsSSS,
        stExcel,
        stGAMApplication,
        stSAV
    }

}
