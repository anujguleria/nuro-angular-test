﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace GfK.Automata.Model
{
    public class Container : MetadataObject
    {
        public List<Category> CategoryMap { get; protected set; }

        public Container() { }
 
        public Container( string name ) 
        {
            Name = name;
            _items = new List<MetadataObject>();
            ObjectType = Model.ObjectType.otCollection;
            Properties = new Dictionary<string, dynamic>();
            CategoryMap = new List<Category>();
        }

        public void SetCategoryMap( List<Category> cm )
        {
            CategoryMap = cm;
        }
    }
}
