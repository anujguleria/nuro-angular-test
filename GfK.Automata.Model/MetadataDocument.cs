﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;

namespace GfK.Automata.Model
{
    public enum MetadataSourceType
    {
        stCSV,
        stDimensionsADO,
        stDimensionsCSV,
        stDimensionsLog,
        stDimensionsMDD,
        stDimensionsQC,
        stDimensionsQV,
        stDimensionsSample,
        stDimensionsSAV,
        stDimensionsSC,
        stDimensionsSSS,
        stExcel,
        stGAMApplication,
        stSAV
    }

    public class MetadataDocument : MetadataObject
    {
        private string _asmversion;
        private Dictionary<string, int> _texts;
        private MetadataSourceType _type;

        public string Path { get; set; }
        public HashSet<string> Contexts { get; private set; }
        public string CurrentContext { get; private set; }
        public string CurrentLanguage { get; private set; }
        public HashSet<string> Languages { get; private set; }

        public MetadataDocument() { }

        public MetadataDocument( MetadataSourceType type )
        {
            _asmversion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            _texts = new Dictionary<string, int>();
            _type = type;
            _items = new List<MetadataObject>();
            Document = this;
            Properties = new Dictionary<string, dynamic>();
            Contexts = new HashSet<string>();
            Languages = new HashSet<string>();
            ObjectType = Model.ObjectType.otDocument;
        }

        public void AddContext( string context )
        {
            Contexts.Add(context);
        }

        public void AddLanguage( string language )
        {
            Languages.Add(language);
        }

        public void AddText( string text )
        {
            if ( !ContainsText( text ) )
            {
                int nextIndex = _texts.Count;
                _texts.Add(text, nextIndex);
            }
        }

        public bool ContainsText( string text )
        {
            return _texts.ContainsKey(text);
        }

        public int IndexOfText( string text )
        {
            if ( ContainsText( text ) )
            {
                return _texts[text];
            }
            else
            {
                return -1;
            }
        }

        public void Log(string path)
        {
            using (StreamWriter writer = new StreamWriter(path))
            {
                writer.WriteLine(ToJSON());
            }
        }

        public void Serialize(string path)
        {
            string json = ToJSON();
            using (FileStream zipToWrite = new FileStream(path, FileMode.Create))
            {
                using (GZipStream compressionStream = new GZipStream(zipToWrite, CompressionMode.Compress))
                {
                    byte[] b = Encoding.UTF8.GetBytes(json);
                    compressionStream.Write(b, 0, b.Length);
                }
            }
        }

        public void SetContext(string context)
        {
            if (Contexts.Contains(context))
            {
                CurrentContext = context;
            }
            else
            {
                throw new InvalidContextException();
            }
        }

        public void SetLanguage(string language)
        {
            if (Languages.Contains(language))
            {
                CurrentLanguage = language;
            }
            else
            {
                throw new InvalidLanguageException();
            }
        }

        public string TextAt(int index)
        {
            return _texts.FirstOrDefault(o => o.Value == index).Key;
        }

        public string ToJSON()
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new MetadataDocumentContractResolver(), Formatting = Formatting.Indented,
                PreserveReferencesHandling = PreserveReferencesHandling.All, TypeNameHandling = TypeNameHandling.Auto,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            };
            return (JsonConvert.SerializeObject(this, settings));
        }

        public MetadataSourceType Type()
        {
            return _type;
        }
    }
}
