﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace GfK.Automata.Model
{
    public class Labels
    {
        public List<Label> Items;

        public Labels()
        {
            Items = new List<Label>();
        }

        public void Add(Label label)
        {
            int count = Items.Count(o => (o.Context == label.Context && o.Language == label.Language));
            if (count == 0)
            {
                Items.Add(label);
            }
            else
            {
                throw new DuplicateLabelException();
            }
        }

        public void Add(string context, string language, string text, MetadataDocument d)
        {
            if (!d.ContainsText(text))
            {
                d.AddText(text);
            }
            int index = d.IndexOfText(text);
            Label l = new Label { Context = context, Language = language, Text = index };
            Add(l);
        }

        public string Get(string context, string language, MetadataDocument d)
        {
            Label l = Items.Find(o => o.Context == context && o.Language == language);
            if (l is null)
            {
                return string.Empty;
            }
            else
            {
                return d.TextAt(l.Text);
            }
        }

        public string Get(string context, string language, MetadataDocument d, bool escape)
        {
            Label l = Items.Find(o => o.Context == context && o.Language == language);
            if (l is null)
            {
                return string.Empty;
            }
            else
            {
                if (escape)
                {
                    return _escape(d.TextAt(l.Text));
                }
                else
                {
                    return d.TextAt(l.Text);
                }
            }
        }

        public string Normalize(string context, string language, MetadataDocument document)
        {
            string s = Get(context, language, document);
            s = WebUtility.HtmlDecode(s);
            s = Regex.Replace(s, "<[^>*(>|$)", " ");
            s = Regex.Replace(s, " {2,}", " ");
            s = s.ToLower();
            return s;
        }

        public void Remove(Label label)
        {
            Items.Remove(label);
        }

        public void Remove(string context, string language)
        {
            Items.RemoveAll(o => (o.Context == context && o.Language == language));
        }

        private string _escape(string label)
        {
            label = label.Replace("\"", "'");
            label = label.Replace("'", "''");
            label = label.Replace("&", "\\&");
            return label;
        }
    }
}
