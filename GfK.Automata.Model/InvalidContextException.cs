﻿using System;

namespace GfK.Automata.Model
{
    public class InvalidContextException : Exception
    {
        public InvalidContextException()
        {
        }

        public InvalidContextException(string message): base(message)
        {
        }
    }
}
