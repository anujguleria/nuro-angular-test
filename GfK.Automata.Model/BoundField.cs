﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDMLib;

namespace GfK.Automata.Model
{
    public class BoundField : Field
    {
        public BoundField() { }

        public BoundField( string name, DataType type, dynamic effectiveminvalue, dynamic effectivemaxvalue, string expression, bool hascasedata, bool isderived, bool isreference, bool issystem, dynamic minvalue, dynamic maxvalue )
        {
            Name = name;
            _datatype = type;
            EffectiveMaxValue = effectivemaxvalue;
            EffectiveMinValue = effectiveminvalue;
            Expression = expression;
            HasCaseData = hascasedata;
            IsDerived = isderived;
            IsReference = isreference;
            IsSystem = issystem;
            base._items = new List<MetadataObject>();
            Labels = new Model.Labels();
            MaxValue = maxvalue;
            MinValue = minvalue;
            ObjectType = Model.ObjectType.otField;
            Properties = new Dictionary<string, dynamic>();
        }

        private void _appendCategoriesToList(List<MetadataObject> items, List<Category> list)
        { 
            int count = items.Count;
            MetadataObject item;
            for( int i = 0; i < count; i++ )
            {
                item = _items[i];
                switch( item.ObjectType )
                {
                    case ObjectType.otCategory:
                        list.Add((Category)item);
                        break;
                    case ObjectType.otReference:
                        Reference r = (Reference)item;
                        DefinedList l = (DefinedList)r.Document.Find(r.Name);
                        _appendCategoriesToList(l.Items, list);
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public List<Category> Categories()
        {
            List<Category> list = new List<Category>();
            _appendCategoriesToList(_items, list);
            return list;
        }
        public Int32 CategoryIndex(string catName, bool ignoreCase)
        {
            List<Category> cats = Categories();
            List<string> catNames = null;
            // returns first index matching catName 
            if (ignoreCase)
            {
                catName = catName.ToUpper();
                catNames = (from c in this.Categories() select c.Name.ToUpper()).ToList();
            }
            else
            {
                catNames = (from c in this.Categories() select c.Name).ToList();
            }
            Int32 i = catNames.IndexOf(catName);

            return i;
        }
        public Category Category(string catName, bool ignoreCase)
        {
            // returns category instance (first match) of catName
            Int32 cIndex = CategoryIndex(catName, ignoreCase);
            if (cIndex >= 0)
            { return Categories()[CategoryIndex(catName, ignoreCase)]; }
            else
            { return null; }
        }

    }
}
