﻿using System;

namespace GfK.Automata.Model
{
    public class DuplicateItemNameException : Exception
    {
        public DuplicateItemNameException()
        {
        }

        public DuplicateItemNameException(string message): base(message)
        {
        }
    }
}
