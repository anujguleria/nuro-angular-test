﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace GfK.Automata.Model
{
    public enum DataType
    {
        dtNone,
        dtLong,
        dtText,
        dtCategorical,
        dtObject,
        dtDate,
        dtDouble,
        dtBoolean
    }

    public abstract class MetadataObject : IMetadataObject
    {
        protected List<MetadataObject> _items;

        public string Name { get; set; }
        public string Alias { get; set; }
        public MetadataDocument Document { get; protected set; }
        public bool IsDerived { get; set; }
        public bool IsReference { get; set; }
        public bool IsSystem { get; set; }
        public List<MetadataObject> Items { get { return _items; } protected set { } }
        public ObjectType ObjectType { get; set; }
        public Dictionary<string, dynamic> Properties;

        public MetadataObject() { }

        public void Add( MetadataObject item )
        {
            item.Document = Document;
            if (_items.Any(o => o.Name == item.Name))
            {
                throw new DuplicateItemNameException();
            }
            else
            {
                _items.Add(item);
            }
        }

        public void Add( IEnumerable<MetadataObject> items )
        {
            foreach( MetadataObject item in items )
            {
                _items.Add(item);
            }
        }

        public void Clear()
        {
            _items.Clear();
        }

        public bool Contains( MetadataObject item )
        {
            return _items.Contains(item);
        }

        public MetadataObject Find( string name )
        {
            return _items.Find(o => o.Name == name);
        }

        public int IndexOf(MetadataObject item)
        {
            return _items.IndexOf(item);
        }

        public MetadataObject ItemAt( int index )
        {
            return _items[index];
        }

        public void Remove( MetadataObject item )
        {
            _items.Remove(item);
        }

        public void Remove( string name )
        {
            Remove(Find(name));
        }

    }
}
