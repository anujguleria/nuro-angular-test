﻿using System;

namespace GfK.Automata.Model
{
    public class InvalidLanguageException : Exception
    {
        public InvalidLanguageException()
        {
        }

        public InvalidLanguageException(string message): base(message)
        {
        }
    }
}
