﻿using System;

namespace GfK.Automata.Model
{
    public class DuplicateLabelException : Exception
    {
        public DuplicateLabelException()
        {
        }

        public DuplicateLabelException(string message): base(message)
        {
        }
    }
}
