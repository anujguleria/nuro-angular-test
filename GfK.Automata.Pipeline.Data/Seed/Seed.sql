﻿USE [Automata_Pipeline];
GO



IF EXISTS (SELECT * FROM sys.objects WHERE name = 'pipeline_seed' AND TYPE = 'P')
BEGIN
	DROP PROCEDURE [dbo].[Pipeline_Seed];
END;
go

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE PROCEDURE [dbo].[Pipeline_Seed]
AS
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;	

	BEGIN TRY

		DECLARE @projectID AS INT;
		DECLARE @pipelineID AS INT;
		DECLARE @userID AS INT;

		BEGIN TRANSACTION;

		DELETE FROM dbo.[User]
		 WHERE email = 'seed.user@gfk.com';

		INSERT INTO dbo.[user] (Email) VALUES ('seed.user@gfk.com');
		SET @userID = SCOPE_IDENTITY();

		DELETE FROM dbo.Project
		 WHERE name = 'Seed Project 1';

		INSERT INTO dbo.Project(Name) VALUES ('Seed Project 1');
		SET @projectID = SCOPE_IDENTITY();

		INSERT INTO dbo.Pipeline(Name, ProjectID) VALUES ('Pipeline SP1_P1', @projectID);
		SET @pipelineID = SCOPE_IDENTITY();

		INSERT INTO dbo.PipelineInstance([Started], Ended, PipelineId) 
			VALUES ('2017-06-18 15:07:43.197', '2017-06-18 15:15:43.197', @pipelineID)
				    ,('2017-06-20 15:07:43.197', NULL, @pipelineID);

		INSERT INTO dbo.Pipeline(Name, ProjectID) VALUES ('Pipeline SP1_P2', @projectID);

		INSERT INTO dbo.ProjectUser(ProjectID, UserID, LastAccess) VALUES (@projectID, @userID, '2017-06-25 15:07:43.197');

		DELETE FROM dbo.Project
		 WHERE name = 'Seed Project 2';

		INSERT INTO dbo.Project(Name) VALUES ('Seed Project 2');
		SET @projectID = SCOPE_IDENTITY();

		INSERT INTO dbo.ProjectUser(ProjectID, UserID, LastAccess) VALUES (@projectID, @userID, '2017-06-23 15:07:43.197');

		COMMIT;

	END TRY
	BEGIN CATCH

		DECLARE @errorMessage	NVARCHAR(4000);		
		DECLARE @errorSeverity	TINYINT;
		DECLARE @errorState		TINYINT;

		SET @errorMessage = OBJECT_NAME(@@PROCID) + N': ' + ERROR_MESSAGE();
		SET @errorSeverity = ERROR_SEVERITY();
		SET @errorState = ERROR_STATE();

		IF @@TRANCOUNT > 0
			ROLLBACK;

		RAISERROR (@errorMessage, @errorSeverity, @errorState);
		
	END CATCH;
END;
GO

EXECUTE Pipeline_Seed;
GO

-- modules

DECLARE @ModuleID INT

IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'DimensionsModule')
BEGIN

INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
SELECT 'DimensionsModule', 'pull data from Dimensions cluster', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.DimensionsModule'
FROM ModuleGroup
WHERE ModuleGroup.Name = 'Data Sources'

SET @ModuleID = @@IDENTITY

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'SurveyName', 'Survey Name', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'Version', 'Survey Version', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'Cluster', 'Dimensions Cluster Key', 'IN', 3, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'DimensionsDataFile', 'Resulting Data File', 'OUT', 4, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'Dimensions Capable'

END

IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'FileInputModule')
BEGIN

INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
SELECT 'FileInputModule', 'brings a file into pipeline env', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.FileInputModule'
FROM ModuleGroup
WHERE ModuleGroup.Name = 'Data Sources'

SET @ModuleID = @@IDENTITY

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'InputFilePath', 'full path of file to pull in', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'OutputFilePath', 'path as it existing within gap for use in other modules', 'OUT', 2, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'


END

IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'FileOutputModule')
BEGIN

INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
SELECT 'FileOutputModule', 'brings a file into pipeline env', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.FileOutputModule'
FROM ModuleGroup
WHERE ModuleGroup.Name = 'Transformation'

SET @ModuleID = @@IDENTITY

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'InputFilePath', 'path of file within gap', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'OutputPath', 'path to write the file out to', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'OutputFilePath', 'full path of the final file written out', 'OUT', 3, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'


END



IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'DMSModule')
BEGIN

INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
SELECT 'DMSModule', 'runs a dms script', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.DMSModule'
FROM ModuleGroup
WHERE ModuleGroup.Name = 'Transformation'

SET @ModuleID = @@IDENTITY

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'DMSScript', 'script to be run (is only used if no file specified)', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'DMSScriptFile', 'file that contains script to be run (is used if specified)', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

END



IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'EXEModule')
BEGIN

INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
SELECT 'EXEModule', 'runs a command line executable', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.EXEModule'
FROM ModuleGroup
WHERE ModuleGroup.Name = 'Tools'

SET @ModuleID = @@IDENTITY

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'Command', 'script to be run (is only used if no file specified)', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'ScriptPath', 'file that contains script to be run (is used if specified)', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

END



IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'MRSModule')
BEGIN

INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
SELECT 'MRSModule', 'runs an MRS script', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.MRSModule'
FROM ModuleGroup
WHERE ModuleGroup.Name = 'Tools'

SET @ModuleID = @@IDENTITY

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'Script', 'script to be run (is only used if no file specified)', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
SELECT 'ScriptPath', 'file that contains script to be run (is used if specified)', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
FROM ParameterType
WHERE Name = 'String'

END
