﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using System.Linq.Expressions;
using GfK.Automata.Pipeline.Model.Views;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class ProjectPipelineHierarchyRepository : RepositoryBaseReadOnly<ProjectPipelineHierarchy>, IProjectPipelineHierarchyRepository
    {
        public ProjectPipelineHierarchyRepository(Infrastructure.IDbFactory dbfactory)
            : base(dbfactory) { }

        public async Task<ProjectPipelineHierarchy> GetPipelineHierarchy(int projectId, int pipelineID)
        {
            ProjectPipelineHierarchy hierarchy = await this.Get((p => p.ProjectID == projectId && p.PipeLineID == pipelineID));

            return hierarchy;
        }
    }
}
