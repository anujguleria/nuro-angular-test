﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IPipelineTaskInputValueRepository : Infrastructure.IRepository<PipelineTaskInputValue>
    {
        Task AddPipelineTaskInputValue(PipelineTaskInputValue pipelineTaskInputValue);
        Task DeletePipelineTaskInputs(int pipelineTaskId);
        Task<ICollection<PipelineTaskInputValue>> GetDependentPipelineTaskInputs(int pipelineTaskId);
    }
}
