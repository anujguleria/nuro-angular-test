﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class GapGroupUserPipelineRepository : RepositoryBaseReadOnly<GapGroupUserPipeline>, IGapGroupUserPipelineRepository
    {
        public GapGroupUserPipelineRepository(IDbFactory dbFactory) : base(dbFactory) { }
    }
}

