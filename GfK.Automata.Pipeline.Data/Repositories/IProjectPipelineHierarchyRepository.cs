﻿using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IProjectPipelineHierarchyRepository : Infrastructure.IRepositoryReadOnly<ProjectPipelineHierarchy>
    {
        Task<ProjectPipelineHierarchy> GetPipelineHierarchy(int projectId, int pipelineID);
    }
}
