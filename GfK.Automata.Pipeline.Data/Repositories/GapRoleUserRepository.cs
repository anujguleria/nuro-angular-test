﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class GapRoleUserRepository : Infrastructure.RepositoryBase<GapRoleUser>, IGapRoleUserRepository
    {
        public GapRoleUserRepository(Infrastructure.IDbFactory dbfactory)
            : base(dbfactory) { }

        public async Task<bool> IsUserRole(int UserID, string role)
        {
            var isRole = this.DbContext.GapRoleUsers.Where(u => u.UserID == UserID && u.GapRole.Name.ToUpper() == role.ToUpper())
                                                    .Select(ru => new { Name = ru.GapRole.Name });

            return isRole.Count() > 0;
        }
    }
}
