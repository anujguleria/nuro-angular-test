﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class PipelineRunRequestRepository : Infrastructure.RepositoryBase<PipelineRunRequest>, IPipelineRunRequestRepository
    {
        public PipelineRunRequestRepository(Infrastructure.IDbFactory dbfactory)
            : base(dbfactory) { }
    }
}
