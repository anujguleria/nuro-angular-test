﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class ModuleGroupRepository : Infrastructure.RepositoryBase<ModuleGroup>, IModuleGroupRepository
    {
        public ModuleGroupRepository(Infrastructure.IDbFactory dbfactory)
            : base(dbfactory) { }
    }
}
