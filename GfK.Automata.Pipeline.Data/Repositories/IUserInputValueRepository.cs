﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IUserInputValueRepository
    {
        Task AddPipelineTaskInputValue(UserInputValue pipelineTaskInputValue);
        Task DeletePipelineTaskInputs(int pipelineTaskId);
        Task<ICollection<UserInputValue>> GetPipelineTaskUserInputValues(int pipeLineTaskId);
    }
}
