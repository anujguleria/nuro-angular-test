﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class PipelineInstanceRepository : Infrastructure.RepositoryBase<PipelineInstance>, IPipelineInstanceRepository
    {
        public PipelineInstanceRepository(Infrastructure.IDbFactory dbFactory)
            : base(dbFactory) { }

        public async Task<PipelineInstance> GetPipelineInstanceLastRun(int pipelineId)
        {
            PipelineInstance lastPipelineInstance = this.DbContext.PipelineInstances.AsNoTracking().Where(p => p.PipelineId == pipelineId).OrderByDescending(p => p.Started).FirstOrDefault();
            return lastPipelineInstance;
        }

        public async Task<IEnumerable<PipelineInstance>> GetPipelineInstances(int pipelineId)
        {
            IEnumerable<PipelineInstance> pipelineInstances = await this.GetMany((p => p.PipelineId == pipelineId));

            return pipelineInstances;
        }
        public async Task<PipelineInstance> GetWithLogs(int pipelineInstanceId)
        {
            PipelineInstance pipelineInstance = this.DbContext.PipelineInstances.Include("PipelineTaskInstances.PipelineTaskInstanceModuleLogs").Where(p => p.PipelineInstanceId == pipelineInstanceId).FirstOrDefault();
            return pipelineInstance;
        }
    }
}
