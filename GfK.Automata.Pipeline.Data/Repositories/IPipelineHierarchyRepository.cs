﻿using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IPipelineHierarchyRepository : Infrastructure.IRepositoryReadOnly<PipelineHierarchy>
    {
        Task<IEnumerable<PipelineHierarchy>> GetPipelineParentHierarchy(int pipelineID);
        Task<IEnumerable<PipelineHierarchy>> GetPipelineChildrenHierarchy(int pipelineID);
    }
}
