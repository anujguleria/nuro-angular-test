﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IPipelineRepository : Infrastructure.IRepository<Model.Pipeline>
    {
        Task<IEnumerable<Model.Pipeline>> GetProjectPipelines(int projectId);
        Task<Model.Pipeline> GetPipelineByName(string pipelineName);
        Task<Model.Pipeline> GetNoTracking(int id);
    }
}
