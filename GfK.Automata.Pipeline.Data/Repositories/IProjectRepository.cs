﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IProjectRepository : Infrastructure.IRepository<Project>
    {
        Task<Project> GetProjectByName(string ProjectName);
        Task<IEnumerable<string>> GetGroupProjectNames(int groupID);
    }
}
