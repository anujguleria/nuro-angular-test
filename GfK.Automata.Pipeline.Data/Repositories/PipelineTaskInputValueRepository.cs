﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class PipelineTaskInputValueRepository : Infrastructure.RepositoryBase<PipelineTaskInputValue>, IPipelineTaskInputValueRepository
    {
        public PipelineTaskInputValueRepository(IDbFactory dbfactory)
            :base(dbfactory) {  }

        public async Task AddPipelineTaskInputValue(PipelineTaskInputValue pipelineTaskInputValue)
        {
            await this.Add(pipelineTaskInputValue);
        }

        public async Task DeletePipelineTaskInputs(int pipelineTaskId)
        {
            await this.Delete((p => p.PipelineTaskID == pipelineTaskId));
        }
        public async Task<ICollection<PipelineTaskInputValue>> GetDependentPipelineTaskInputs(int pipelineTaskId)
        {
            IEnumerable<PipelineTaskInputValue> inputs=  await this.GetMany(P => P.PriorPipelineTaskID == pipelineTaskId);
            return inputs.ToList<PipelineTaskInputValue>();
        }
    }
}
