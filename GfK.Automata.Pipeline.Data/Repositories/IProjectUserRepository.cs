﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IProjectUserRepository : Infrastructure.IRepository<ProjectUser>
    {
        Task<IEnumerable<ProjectUser>> GetUsersByProjectId(int projectID);
        Task<IEnumerable<ProjectUser>> GetProjectsByUserId(int userID);
        Task<ProjectUser> GetProject(int projectID, User user);
    }
}
