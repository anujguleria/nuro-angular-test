﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class ProjectRepository : Infrastructure.RepositoryBase<Project>, IProjectRepository
    {
        public ProjectRepository(Infrastructure.IDbFactory dbFactory)
            : base(dbFactory) { }

        public async Task<Project> GetProjectByName(string ProjectName)
        {
            IEnumerable<Project> projects = await this.GetMany((c => c.Name == ProjectName));

            if (Enumerable.Count(projects) == 0)
            {
                return null;
            }
            else
            {
                return projects.First();
            }        
        }

        public async Task<IEnumerable<string>> GetGroupProjectNames(int groupID)
        {
            List<string> nameList = new List<string>();
            var projectNames = this.DbContext.Projects.Where(p => p.GapGroupID == groupID)
                                                      .Select(p => new { Name = p.Name });

            foreach (var projectName in projectNames)
            {
                nameList.Add(projectName.ToString());
            }

            return nameList;
        }

        public override async Task<Project> Add(Project entity)
        {
            entity.ProjectID = await SQLSequence.GetNextValue(DbContext.Database, await SQLSequence.ProjectPipelineIDSequence());

            return await base.Add(entity);
        }
        public override async Task Update(Project entity)
        {
            await base.Update(entity);
        }
    }
}
