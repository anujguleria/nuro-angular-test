﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class PipelineTaskRepository : Infrastructure.RepositoryBase<PipelineTask>, IPipelineTaskRepository
    {
        public PipelineTaskRepository(IDbFactory dbfactory)
            : base(dbfactory) { }
    }
}
