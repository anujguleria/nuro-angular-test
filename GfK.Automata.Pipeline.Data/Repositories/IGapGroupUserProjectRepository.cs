﻿using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IGapGroupUserProjectRepository : Infrastructure.IRepositoryReadOnly<GapGroupUserProject>
    {
    }
}
