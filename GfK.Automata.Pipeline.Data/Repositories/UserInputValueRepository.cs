﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class UserInputValueRepository : Infrastructure.RepositoryBase<UserInputValue>, IUserInputValueRepository
    {
        public UserInputValueRepository(IDbFactory dbfactory)
            :base(dbfactory)  {  }

        public async Task AddPipelineTaskInputValue(UserInputValue userInputValue)
        {
            await this.Add(userInputValue);
        }

        public async Task DeletePipelineTaskInputs(int pipelineTaskId)
        {
            await this.Delete(p => p.PipelineTaskID == pipelineTaskId);
        }
        public async Task<ICollection<UserInputValue>> GetPipelineTaskUserInputValues(int pipeLineTaskId)
        {
            IEnumerable<UserInputValue> userInputs= await this.GetMany(p => p.PipelineTaskID == pipeLineTaskId);
            return userInputs.ToList<UserInputValue>();
        }

    }
}
