﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class ScheduleRepository : Infrastructure.RepositoryBase<GfK.Automata.Pipeline.Model.Schedule>, IScheduleRepository
    {
        public ScheduleRepository(Infrastructure.IDbFactory dbfactory)
            : base(dbfactory) { }
    }
}
