﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class UserRepository : Infrastructure.RepositoryBase<User>, IUserRepository
    {
        public UserRepository(Infrastructure.IDbFactory dbFactory)
            : base(dbFactory) { }

        public async Task<User> GetUserByEmail(string email)
        {
            var user = this.DbContext.Users.Where(c => c.Email == email).FirstOrDefault();
            return user;
        }
    }
}
