﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class TaskGroupRepository : Infrastructure.RepositoryBase<ModuleGroup>, ITaskGroupRepository
    {
        public TaskGroupRepository(Infrastructure.IDbFactory dbfactory)
            : base(dbfactory) { }
    }
}
