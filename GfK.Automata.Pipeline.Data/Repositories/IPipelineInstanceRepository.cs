﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IPipelineInstanceRepository : Infrastructure.IRepository<PipelineInstance>
    {
        Task<IEnumerable<PipelineInstance>> GetPipelineInstances(int pipelineId);
        Task<PipelineInstance> GetPipelineInstanceLastRun(int pipelineId);
        Task<PipelineInstance> GetWithLogs(int pipelineInstanceId);
    }
}
