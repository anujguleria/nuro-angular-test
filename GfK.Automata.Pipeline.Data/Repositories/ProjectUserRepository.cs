﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class ProjectUserRepository : Infrastructure.RepositoryBase<ProjectUser>, IProjectUserRepository
    {
        public ProjectUserRepository(Infrastructure.IDbFactory dbFactory)
            : base(dbFactory) { }

        public async Task<ProjectUser> GetProject(int projectID, User user)
        {
            ProjectUser projectUser = this.DbContext.ProjectUsers.Where(pu => pu.UserID == user.UserID).Where(pu => pu.ProjectID == projectID).FirstOrDefault();

            return projectUser;
        }

        public async Task<IEnumerable<ProjectUser>> GetProjectsByUserId(int userID)
        {
            List<ProjectUser> userProjects = this.DbContext.ProjectUsers.Where((pu => pu.UserID == userID)).OrderByDescending(pu => pu.LastAccess).ToList();

            return userProjects;
        }

        public async Task<IEnumerable<ProjectUser>> GetUsersByProjectId(int projectID)
        {
            IEnumerable<ProjectUser> userProjects = await this.GetMany((pu => pu.ProjectID == projectID));

            return userProjects;
        }
    }
}
