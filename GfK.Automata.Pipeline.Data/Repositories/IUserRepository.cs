﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public interface IUserRepository : Infrastructure.IRepository<User>
    {
        Task<User> GetUserByEmail(string email);
    }
}
