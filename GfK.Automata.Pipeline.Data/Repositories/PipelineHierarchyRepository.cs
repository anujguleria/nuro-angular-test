﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class PipelineHierarchyRepository : RepositoryBaseReadOnly<PipelineHierarchy>, IPipelineHierarchyRepository
    {
        public PipelineHierarchyRepository(Infrastructure.IDbFactory dbfactory)
            : base(dbfactory) { }

        public async Task<IEnumerable<PipelineHierarchy>> GetPipelineParentHierarchy(int pipelineID)
        {
            IEnumerable<PipelineHierarchy> pipelineHierarchy = await this.GetMany(p => p.PipelineID == pipelineID);

            return pipelineHierarchy;
        }

        public async Task<IEnumerable<PipelineHierarchy>> GetPipelineChildrenHierarchy(int pipelineID)
        {
            IEnumerable<PipelineHierarchy> pipelineHierarchy = await this.GetMany(p => p.HierarchyPipelineID == pipelineID);

            return pipelineHierarchy;
        }
    }
}
