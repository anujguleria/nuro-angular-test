﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Repositories
{
    public class PipelineRepository : Infrastructure.RepositoryBase<Model.Pipeline>, IPipelineRepository
    {
        public PipelineRepository(Infrastructure.IDbFactory dbfactory)
            : base(dbfactory) { }

        public async Task<Model.Pipeline> GetPipelineByName(string pipelineName)
        {
            IEnumerable<Model.Pipeline> pipelines = await this.GetMany((c => c.Name == pipelineName));

            return pipelines.First();
        }

        public async Task<Model.Pipeline> GetNoTracking(int id)
        {
            return this.DbContext.PipeLines.AsNoTracking().Single(p => p.PipeLineID == id);
        }

        public async Task<IEnumerable<Model.Pipeline>> GetProjectPipelines(int projectId)
        {
            List<Model.Pipeline> pipeLines = (await this.GetMany(p => p.ProjectID == projectId)).ToList();

            return pipeLines.AsEnumerable<Model.Pipeline>();
        }
        
        public override async Task<Model.Pipeline> Add(Model.Pipeline entity)
        {
            entity.PipeLineID = await SQLSequence.GetNextValue(DbContext.Database, await SQLSequence.ProjectPipelineIDSequence());

            return await base.Add(entity);
        }
    }
}
