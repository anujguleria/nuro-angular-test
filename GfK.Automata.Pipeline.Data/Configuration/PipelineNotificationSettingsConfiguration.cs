﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class PipelineNotificationSettingsConfiguration : EntityTypeConfiguration<PipelineNotificationSetting>
    {
        public PipelineNotificationSettingsConfiguration()
        {
            ToTable("PipelineNotificationSettings").HasKey(key => new { key.PipelineNotificationSettingID})
                .HasRequired<Model.Pipeline>(p => p.Pipeline)
                .WithMany(n => n.PipelineNotificationSettings);

            ToTable("PipelineNotificationSettings").HasRequired<User>(u => u.User);

        }

    }
}
