﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

using GfK.Automata.Pipeline.Model;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class GapRolePrivilegeConfiguration : EntityTypeConfiguration<GapRolePrivilege>
    {
        public GapRolePrivilegeConfiguration()
        {
            ToTable("GapRolePrivilege").HasKey(p => p.GapRolePrivilegeID);

            Property(p => p.Name).IsRequired()
                                 .HasMaxLength(100)
                                 .IsUnicode(false)
                                 .HasColumnAnnotation("Index",
                                                      new IndexAnnotation(new[]
                                                          {
                                                            new IndexAttribute("Unq_GapRolePrivilege_Name") {IsUnique = true}
                                                          }
                                                      ));


        }
    }
}
