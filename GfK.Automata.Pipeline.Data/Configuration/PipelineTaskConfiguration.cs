﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class PipelineTaskConfiguration : EntityTypeConfiguration<PipelineTask>
    {
        public PipelineTaskConfiguration()
        {
            ToTable("PipelineTask").HasKey(p => p.PipelineTaskID)
                                   .HasMany<UserInputValue>(p => p.UserInputValues);

            ToTable("PipelineTask").HasMany<PipelineTaskInputValue>(p => p.PipelineTaskInputValues);

            Property(p => p.PipeLineID).IsRequired()
                                       .HasColumnAnnotation("Index",
                                                            new IndexAnnotation(new[]
                                                                {
                                                                    new IndexAttribute ("Unq_PipelineTask_PipeLineID_Name", 1) {IsUnique = true}
                                                                }
                                                           ));

            Property(p => p.Name).IsRequired()
                                 .HasMaxLength(100)
                                 .HasColumnAnnotation("Index",
                                                        new IndexAnnotation(new[]
                                                            {
                                                                new IndexAttribute ("Unq_PipelineTask_PipeLineID_Name", 2) {IsUnique = true}
                                                            }
                                                      ));

            Property(p => p.PipelineTaskID).HasColumnAnnotation("Index",
                                                                    new IndexAnnotation(new[]
                                                                    {
                                                                        new IndexAttribute("Unq_PipelineTask_PipelineTaskID_PipelineID", 1)
                                                                            {IsUnique = true }
                                                                    }
                                                               ));

            Property(p => p.PipeLineID).HasColumnAnnotation("Index",
                                                                    new IndexAnnotation(new[]
                                                                    {
                                                                        new IndexAttribute("Unq_PipelineTask_PipelineTaskID_PipelineID", 2)
                                                                            {IsUnique = true }
                                                                    }
                                                               ));

            Property(p => p.SequencePosition).IsRequired();
            Property(p => p.ModuleID).IsRequired();

        }
    }
}
