﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class ModuleGroupConfiguration : EntityTypeConfiguration<ModuleGroup>
    {
        public ModuleGroupConfiguration()
        {
            ToTable("ModuleGroup").HasKey(p => p.ModuleGroupID);                                  ;

            Property(p => p.Name).IsRequired()
                                 .HasMaxLength(200)
                                 .IsUnicode()
                                 .HasColumnAnnotation("Index",
                                                       new IndexAnnotation(new[]
                                                           {
                                                                new IndexAttribute("Unq_ModuleGroup_Name", 1) {IsUnique = true}
                                                           }
                                                      ));

            Property(p => p.Description).IsOptional().HasMaxLength(1000).IsUnicode();
        }
    }
}
