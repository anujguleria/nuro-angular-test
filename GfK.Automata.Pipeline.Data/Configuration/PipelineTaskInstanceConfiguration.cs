﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class PipelineTaskInstanceConfiguration : EntityTypeConfiguration<PipelineTaskInstance>
    {
        public PipelineTaskInstanceConfiguration()
        {
            string toTable = "PipelineTaskInstance";

            ToTable(toTable).HasRequired<PipelineInstance>(p => p.PipelineInstance)
                            .WithMany(p => p.PipelineTaskInstances)
                            .WillCascadeOnDelete(false);

            ToTable(toTable).HasRequired<PipelineTask>(p => p.PipelineTask)
                            .WithMany(p => p.PipelineTaskInstances);

            Property(p => p.Started).IsRequired();
            Property(p => p.Ended).IsOptional();
            Property(p => p.Status).IsRequired().HasMaxLength(9);
            Property(p => p.Message).IsOptional();
        }
    }
}
