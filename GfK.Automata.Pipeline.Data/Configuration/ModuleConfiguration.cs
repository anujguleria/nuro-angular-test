﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class ModuleConfiguration : EntityTypeConfiguration<Module>
    {
        public ModuleConfiguration()
        {
            ToTable("Module").HasKey(p => p.ModuleID)
                             .HasMany<Parameter>(p => p.Parameters);

            ToTable("Module").HasMany<PipelineTask>(p => p.PipelineTasks)
                             .WithRequired().WillCascadeOnDelete(false);

            Property(p => p.Name).IsRequired()                                 
                                 .HasMaxLength(100)
                                 .HasColumnAnnotation("Index",
                                                      new IndexAnnotation(new[]
                                                          {
                                                            new IndexAttribute("Unq_Module_Name") {IsUnique = true}
                                                          }
                                                      ));

            Property(p => p.ModuleType).IsRequired().IsUnicode(false).HasMaxLength(10);
            Property(p => p.Description).IsOptional().HasMaxLength(1000);
            Property(p => p.ModuleGroupID).IsRequired();
            Property(p => p.AssemblyName).HasMaxLength(150).IsUnicode(false);
            Property(p => p.TypeName).HasMaxLength(150).IsUnicode(false);
        }
    }
}
