﻿using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class GapGroupUserPipelineConfiguration : EntityTypeConfiguration<GapGroupUserPipeline>
    {
        public GapGroupUserPipelineConfiguration()
        {
            ToTable("dbo.GapGroupUserPipeline").HasKey(k => new { k.UserID, k.PipeLineID, k.GapGroupID });
        }
    }
}
