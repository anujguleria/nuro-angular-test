﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class ParameterConfiguration : EntityTypeConfiguration<Parameter>
    {
        public ParameterConfiguration()
        {
            ToTable("Parameter").HasKey(p => p.ParameterID)
                                .HasMany<UserInputValue>(p => p.UserInputValues)
                                .WithRequired().WillCascadeOnDelete(true);

            ToTable("Parameter").HasMany<PipelineTaskInputValue>(p => p.PipelineTaskInputValues)
                                .WithRequired().WillCascadeOnDelete(true);

            Property(p => p.ModuleID).IsRequired()
                                     .HasColumnAnnotation("Index",
                                                            new IndexAnnotation(new[]
                                                                {
                                                                    new IndexAttribute("Unq_Parameter_ModuleID_Name", 1) {IsUnique = true}
                                                                }
                                                            ));

            Property(p => p.Name).HasMaxLength(100).IsRequired()
                                 .HasColumnAnnotation("Index",
                                                       new IndexAnnotation(new[]
                                                           {
                                                                new IndexAttribute("Unq_Parameter_ModuleID_Name", 2) {IsUnique = true}
                                                           }
                                                      ));

            Property(p => p.Description).IsOptional().HasMaxLength(1000).IsUnicode();
        }
    }
}
