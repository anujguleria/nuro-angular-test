﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class UserInputValueConfiguration : EntityTypeConfiguration<UserInputValue>
    {
        public UserInputValueConfiguration()
        {
            ToTable("UserInputValue").HasKey(p => p.UserInputValueID);

            Property(p => p.InputValue).IsUnicode(true).IsRequired();

            Property(p => p.PipelineTaskID).IsRequired(); 
            Property(p => p.ParameterID).IsRequired();
        }
    }
}
