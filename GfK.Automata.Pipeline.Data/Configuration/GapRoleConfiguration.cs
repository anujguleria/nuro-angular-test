﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class GapRoleConfiguration : EntityTypeConfiguration<GapRole>
    {
        public GapRoleConfiguration()
        {
            ToTable("GapRole").HasKey(p => p.GapRoleID);

            Property(p => p.Name).IsRequired()
                                 .HasMaxLength(100)
                                 .IsUnicode(false);

            Property(p => p.Name).HasColumnAnnotation("Index",
                                                      new IndexAnnotation(new[]
                                                         {
                                                            new IndexAttribute("Unq_GapRole_Name") {IsUnique = true}
                                                          }
                                                      ));
        }
    }
}
