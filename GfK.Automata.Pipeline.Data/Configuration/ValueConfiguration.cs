﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class ValueConfiguration : EntityTypeConfiguration<Value>
    {
        public ValueConfiguration()
        {
            ToTable("Value").HasKey(p => p.ValueID);

            Property(p => p.InputValue).IsOptional()
                                       .IsUnicode(true)
                                       .HasMaxLength(1000);

            Property(p => p.InputPriorTaskValueID).IsOptional();
            Property(p => p.ParameterID).IsRequired();
            Property(p => p.PipelineTaskID).IsRequired();

        }
    }
}
