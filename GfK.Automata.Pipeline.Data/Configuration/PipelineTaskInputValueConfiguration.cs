﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class PipelineTaskInputValueConfiguration : EntityTypeConfiguration<PipelineTaskInputValue>
    {
        public PipelineTaskInputValueConfiguration()
        {
            ToTable("PipelineTaskInputValue").HasKey(p => p.PipelineTaskInputValueID);

            Property(p => p.PriorPipelineTaskID).IsRequired();
            Property(p => p.PriorPipeLineTaskParameterID).IsRequired();
            Property(p => p.ParameterID).IsRequired();
            Property(p => p.PipelineTaskID).IsRequired();
            Property(p => p.PipelineID).IsRequired();
        }
    }
}
