﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class GapGroupConfiguration : EntityTypeConfiguration<GapGroup>
    {
        public GapGroupConfiguration()
        {
            ToTable("GapGroup").HasKey(p => p.GapGroupID)
                .HasMany<GapGroupUser>(p => p.GapGroupUsers);

            Property(p => p.Name).IsRequired()
                                 .HasMaxLength(100)
                                 .HasColumnAnnotation("Index",
                                                      new IndexAnnotation(new[]
                                                          {
                                                            new IndexAttribute("Unq_GapGroup_Name") {IsUnique = true}
                                                          }
                                                      ));
        }
    }
}