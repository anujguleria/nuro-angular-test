﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class PipelineTaskInstanceModuleLogConfiguration : EntityTypeConfiguration<PipelineTaskInstanceModuleLog>
    {
        public PipelineTaskInstanceModuleLogConfiguration()
        {
            ToTable("PipelineTaskInstanceModuleLog").HasKey(p => p.PipelineTaskInstanceModuleLogID);

            Property(p => p.LogMessageType).IsRequired().HasMaxLength(13).IsUnicode(false);

            Property(p => p.LogDate).IsRequired();
            
            Property(p => p.LogMessage).IsRequired();

            Property(p => p.PipelineTaskInstanceId).IsRequired();
        }
    }
}
