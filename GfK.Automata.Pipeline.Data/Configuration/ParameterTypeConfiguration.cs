﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class ParameterTypeConfiguration : EntityTypeConfiguration<ParameterType>
    {
        public ParameterTypeConfiguration()
        {
            ToTable("ParameterType").HasKey(p => p.ParameterTypeID)
                                    //.HasMany<Parameter>(p => p.Parameters)
                                    //.WithRequired(p => p.ParameterType).WillCascadeOnDelete(false)
                                    ;

            Property(p => p.Name).IsRequired()
                                 .HasMaxLength(100)
                                 .HasColumnAnnotation("Index",
                                                       new IndexAnnotation(new []
                                                           {
                                                                new IndexAttribute("Unq_ParameterType_Name", 1) { IsUnique = true}
                                                           }
                                                     ));

            Property(p => p.Description).IsOptional().HasMaxLength(1000).IsUnicode();
        }
    }
}
