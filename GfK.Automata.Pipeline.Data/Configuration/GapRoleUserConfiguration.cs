﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class GapRoleUserConfiguration : EntityTypeConfiguration<GapRoleUser>
    {
        public GapRoleUserConfiguration()
        {
            ToTable("GapRoleUser").HasKey(key => new { key.GapRoleID, key.UserID })
                                  .HasRequired <GapRole>(p => p.GapRole)
                                  .WithMany(p => p.GapRoleUsers);

            ToTable("GapRoleUser").HasRequired<User>(u => u.User)
                                  .WithMany(u => u.GapRoleUsers);
        }
    }
}
