﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class PipelineInstanceConfiguration : EntityTypeConfiguration<PipelineInstance>
    {
        public PipelineInstanceConfiguration()
        {
            ToTable("PipelineInstance").HasRequired<Model.Pipeline>(p => p.Pipeline)
                                       .WithMany(p => p.PipelineInstances);

            Property(p => p.Started).IsRequired();
            Property(p => p.Ended).IsOptional();
            Property(p => p.Status).IsRequired().HasMaxLength(9);
            Property(p => p.Message).IsOptional();
        }
    }
}
