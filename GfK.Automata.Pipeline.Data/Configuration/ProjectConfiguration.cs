﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class ProjectConfiguration: EntityTypeConfiguration<Project>
    {
        public ProjectConfiguration()
        {
            ToTable("Project").HasRequired<GapGroup>(p => p.GapGroup)
                              .WithMany(p => p.Projects)
                              .HasForeignKey(p => p.GapGroupID)
                              .WillCascadeOnDelete(false);

            Property(p => p.Name).IsRequired()
                                 .HasMaxLength(100)
                                 .HasColumnAnnotation("Index",
                                                      new IndexAnnotation(new[]
                                                          {
                                                            new IndexAttribute("Unq_Project_Name") {IsUnique = true}
                                                          }
                                                      ));

            Property(p => p.ProjectID).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
