﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

using GfK.Automata.Pipeline.Model;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GfK.Automata.Pipeline.Data.Configuration 
{
    public class PipelineConfiguration : EntityTypeConfiguration<Model.Pipeline>
    {
        public PipelineConfiguration()
        {
            ToTable("Pipeline").HasOptional<Project>(p => p.project)
                               .WithMany(p => p.pipeLines)
                               .HasForeignKey(p => p.ProjectID);
            
            ToTable("Pipeline").HasOptional<Schedule>(p => p.Schedule)
                               .WithOptionalDependent(p => p.Pipeline);

            ToTable("Pipeline").HasMany<Model.Pipeline>(p => p.Pipelines);

            ToTable("Pipeline").HasOptional<Module>(p => p.Module);

            ToTable("Pipeline").HasMany<PipelineNotificationSetting>(p => p.PipelineNotificationSettings);

            ToTable("Pipeline").Ignore(p => p.LastRun);
            ToTable("Pipeline").Ignore(p => p.LastStatus);
            //ToTable("Pipeline").Ignore(p => p.ParentToPipelineID); // computed column in table

            ToTable("Pipeline").Ignore(p => p.Parameters);

            Property(p => p.ProjectID).IsOptional()
                                      .HasColumnAnnotation("Index",
                                                           new IndexAnnotation(new[]
                                                               {
                                                                    new IndexAttribute("Unq_Pipeline_ProjectId_Name", 1) {IsUnique = true}
                                                               }
                                                           ));
            Property(p => p.Name).IsRequired()
                                 .HasMaxLength(200)
                                 .HasColumnAnnotation("Index",
                                                      new IndexAnnotation(new[]
                                                          {
                                                                new IndexAttribute("Unq_Pipeline_ProjectId_Name", 2) {IsUnique = true}
                                                          }
                                                      ));
            Property(p => p.PipeLineID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(p => p.ParentToPipelineID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
        }
    }
}
