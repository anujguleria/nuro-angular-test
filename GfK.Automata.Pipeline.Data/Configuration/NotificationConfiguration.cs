﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

using GfK.Automata.Pipeline.Model;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    class NotificationConfiguration : EntityTypeConfiguration<Model.Notification>
    {
        public NotificationConfiguration()
        {
            ToTable("Notification").HasRequired<Model.Pipeline>(n => n.Pipeline)
                .WithMany(p => p.notifications)
                .HasForeignKey(n => n.PipelineID);
            ToTable("Notification").HasRequired<User>(n => n.user)
                .WithMany(u => u.notifications)
                .HasForeignKey(n => n.UserID);

            ToTable("Notification").Ignore(n => n.ProjectName);
            ToTable("Notification").Ignore(n => n.PipelineName);

            Property(n => n.NotificationID).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
