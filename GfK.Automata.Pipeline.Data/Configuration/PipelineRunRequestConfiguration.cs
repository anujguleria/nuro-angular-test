﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class PipelineRunRequestConfiguration : EntityTypeConfiguration<PipelineRunRequest>
    {
        public PipelineRunRequestConfiguration()
        {
            ToTable("PipelineRunRequest").HasKey(k => k.PipelineRunRequestID);

            Property(p => p.PipelineID).IsRequired()
                                        .HasColumnAnnotation("Index",
                                                                new IndexAnnotation(new[]
                                                                    {
                                                                        new IndexAttribute("Unq_PipelineRunRequest_PipelineID_RunRequestDate", 1) {IsUnique = true}
                                                                    }
                                                            ));

            Property(p => p.RunRequestDate).IsRequired()
                                           .HasColumnAnnotation("Index",
                                                                   new IndexAnnotation(new[]
                                                                       {
                                                                            new IndexAttribute("Unq_PipelineRunRequest_PipelineID_RunRequestDate", 2) {IsUnique = true}
                                                                       }
                                                               ));

            Property(p => p.RunPipelineContext).IsRequired().HasMaxLength(8).IsUnicode(false);
        }
    }
}
