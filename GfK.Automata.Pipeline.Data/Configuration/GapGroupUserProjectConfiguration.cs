﻿using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class GapGroupUserProjectConfiguration : EntityTypeConfiguration<GapGroupUserProject>
    {
        public GapGroupUserProjectConfiguration()
        {
            ToTable("dbo.GapGroupUserProject").HasKey(k => new { k.UserID, k.ProjectID, k.GapGroupID });
        }
    }
}
