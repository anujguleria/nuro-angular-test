﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("User");
            Property(p => p.Email).IsRequired()
                                  .HasMaxLength(500)
                                  .HasColumnAnnotation("Index",
                                                      new IndexAnnotation(new[]
                                                          {
                                                            new IndexAttribute("Unq_Name") {IsUnique = true}
                                                          }
                                                      ));
            Property(p => p.UserID).IsRequired();
        }
    }
}
