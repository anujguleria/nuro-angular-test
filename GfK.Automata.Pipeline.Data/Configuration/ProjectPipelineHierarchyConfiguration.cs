﻿using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class ProjectPipelineHierarchyConfiguration : EntityTypeConfiguration<ProjectPipelineHierarchy>
    {
        public ProjectPipelineHierarchyConfiguration()
        {
            ToTable("ProjectPipelineHierarchy").HasKey(k => new {k.ProjectID, k.PipeLineID });
        }

    }
}
