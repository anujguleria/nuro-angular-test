﻿using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class PipelineHierarchyConfiguration : EntityTypeConfiguration<PipelineHierarchy>
    {
        public PipelineHierarchyConfiguration()
        {
            ToTable("dbo.PipelineHierarchy").HasKey(k => new { k.PipelineID, k.HierarchyPipelineID });
        }
    }
}
