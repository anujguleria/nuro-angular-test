﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class ProjectUserConfiguration : EntityTypeConfiguration<ProjectUser>
    {
        public ProjectUserConfiguration()
        {
            ToTable("ProjectUser").HasKey(key => new { key.ProjectID, key.UserID })
                                  .HasRequired<User>(u => u.User)
                                  .WithMany(u => u.projectUsers);
            Property(p => p.ProjectID).IsRequired();
            Property(p => p.UserID).IsRequired();
            Property(p => p.LastAccess).IsRequired();
        }
    }
}
