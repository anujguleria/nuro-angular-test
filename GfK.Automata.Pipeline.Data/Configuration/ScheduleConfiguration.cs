﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

using GfK.Automata.Pipeline.Model;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GfK.Automata.Pipeline.Data.Configuration 
{
    public class ScheduleConfiguration : EntityTypeConfiguration<GfK.Automata.Pipeline.Model.Schedule>
    {
        public ScheduleConfiguration()
        {
            ToTable("Schedule").HasKey(s => s.ScheduleID);
            Property(s => s.TimeZoneID).HasMaxLength(100);
        }
    }
}
