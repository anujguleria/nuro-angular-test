﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Configuration
{
    public class GapGroupUserConfiguration : EntityTypeConfiguration<GapGroupUser>
    {
        public GapGroupUserConfiguration()
        {
            ToTable("GapGroupUser").HasKey(key => new { key.GapGroupID, key.UserID })
                                  .HasRequired<GapGroup>(p => p.GapGroup)
                                  .WithMany(p => p.GapGroupUsers);

            ToTable("GapGroupUser").HasRequired<User>(u => u.User)
                                  .WithMany(u => u.GapGroupUsers);
        }
    }
}
