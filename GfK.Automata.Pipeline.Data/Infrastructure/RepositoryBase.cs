﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;

namespace GfK.Automata.Pipeline.Data.Infrastructure
{
    public abstract class RepositoryBase<T> where T : class
    {
        private GAPEntities _dataContext;
        private readonly IDbSet<T> _dbSet;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected GAPEntities DbContext
        {
            get { return _dataContext ?? (_dataContext = DbFactory.Init()); }
        }
        protected RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            _dbSet = DbContext.Set<T>();
        }
        public virtual async Task<T> Add(T entity)
        {
            return _dbSet.Add(entity);
        }
        public virtual async Task Update(T entity)
        {
            _dbSet.Attach(entity);
            _dataContext.Entry(entity).State = EntityState.Modified;
        }
        public virtual async Task Delete(T entity)
        {
            _dbSet.Attach(entity);
            _dbSet.Remove(entity);
        }
        public virtual async Task Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = _dbSet.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                _dbSet.Remove(obj);
        }
        public virtual async Task<T> GetById(int id)
        {
            return _dbSet.Find(id);
        }
        public virtual async Task<IEnumerable<T>> GetAll()
        {
            return _dbSet.ToList();
        }
        public virtual async Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> where)
        {
            return _dbSet.Where(where).ToList();
        }
        public async Task<T> Get(Expression<Func<T, bool>> where)
        {
            return _dbSet.Where(where).FirstOrDefault<T>();
        }
    }
}
