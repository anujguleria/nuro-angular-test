﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace GfK.Automata.Pipeline.Data.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        // Marks an entity as new
        Task<T> Add(T entity);
        // Marks an entity as modified
        Task Update(T entity);
        // Marks an entity to be removed
        Task Delete(T entity);
        Task Delete(Expression<Func<T, bool>> where);
        // Get an entity by int id
        Task<T> GetById(int id);
        // Get an entity using delegate
        Task<T> Get(Expression<Func<T, bool>> where);
        // Gets all entities of type T
        Task<IEnumerable<T>> GetAll();
        // Gets entities using delegate
        Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> where);
    }
}
