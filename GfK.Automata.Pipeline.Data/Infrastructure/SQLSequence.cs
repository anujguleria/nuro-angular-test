﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Infrastructure
{
    public static class SQLSequence
    {
        public async static Task<string> ProjectPipelineIDSequence()
        {
            return ConfigurationManager.AppSettings["ProjectPipelineSequence"].ToString();
        }
        public async static Task<int> GetNextValue(Database database, string sequence)
        {
            string sequenceQuery = "SELECT NEXT VALUE FOR " + sequence;
            DbRawSqlQuery<int> nextSequenceValue = database.SqlQuery<int>(sequenceQuery);

            return await nextSequenceValue.FirstAsync();
        }
        public async static Task<int> GetCurrentValue(Database database, string sequence)
        {
            string sequenceQuery = "SELECT CURRENT VALUE FOR " + sequence;
            DbRawSqlQuery<int> nextSequenceValue = database.SqlQuery<int>(sequenceQuery);

            return await nextSequenceValue.FirstAsync();
        }
    }
}
