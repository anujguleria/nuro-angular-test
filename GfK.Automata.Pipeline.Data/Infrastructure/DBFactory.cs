﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Infrastructure
{
    public class DBFactory : Disposable, IDbFactory
    {
        GAPEntities dbContext;

        public GAPEntities Init()
        {
            return dbContext ?? (dbContext = new GAPEntities());
        }
        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
