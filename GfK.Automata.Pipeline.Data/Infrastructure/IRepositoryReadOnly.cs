﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Infrastructure
{
    public interface IRepositoryReadOnly<T> where T : class
    {
        // Get an entity using delegate
        Task<T> Get(Expression<Func<T, bool>> where);
        Task<IEnumerable<T>> GetMany(Func<T, bool> where);
    }
}
