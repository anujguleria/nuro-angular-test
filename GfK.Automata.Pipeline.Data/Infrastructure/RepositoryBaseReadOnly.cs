﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Infrastructure
{
    public abstract class RepositoryBaseReadOnly<T> where T : class
    {
        private GAPEntities _dataContext;
        private readonly IDbSet<T> _dbSet;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected GAPEntities DbContext
        {
            get { return _dataContext ?? (_dataContext = DbFactory.Init()); }
        }
        protected RepositoryBaseReadOnly(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            _dbSet = DbContext.Set<T>();
        }
        public async Task<T> Get(Expression<Func<T, bool>> where)
        {
            return _dbSet.Where(where).FirstOrDefault<T>();
        }
        public async Task<IEnumerable<T>> GetMany(Func<T, bool> where)
        {
            return _dbSet.Where(where);
        }
    }
}
