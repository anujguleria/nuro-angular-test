﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Model.Views;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace GfK.Automata.Pipeline.Data
{
    public class GAPEntities : DbContext
    {
        public GAPEntities() : base("name=Pipeline") { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ProjectUser> ProjectUsers { get; set; }
        public DbSet<Model.Pipeline> PipeLines { get; set; }
        public DbSet<ProjectPipelineHierarchy> ProjectPipelineHierarchy {get; set;}
        public DbSet<ModuleGroup> ModuleGroups { get; set; }
        public DbSet<ParameterType> ParameterTypes { get; set; }
        public DbSet<PipelineInstance> PipelineInstances { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<GfK.Automata.Pipeline.Model.Schedule> Schedules { get; set; }
        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<PipelineTask> PipelineTasks { get; set; }
        public DbSet<PipelineTaskInstance> PipelineTaskInstances { get; set; }
        public DbSet<GapGroup> GapGroups { get; set; }
        public DbSet<GapRole> GapRoles { get; set; }
        public DbSet<GapRolePrivilege> GapRolePrivileges { get; set; }
        public DbSet<GapGroupUser> GapGroupUsers { get; set; }
        public DbSet<GapRoleUser> GapRoleUsers { get; set; }
        public DbSet<GapGroupUserProject> GapGroupUserProjects { get; set; }
        public DbSet<GapGroupUserPipeline> GapGroupUserPipelines { get; set; }
        public DbSet<PipelineRunRequest> PipelineRunRequests { get; set; }

        public DbSet<PipelineHierarchy> PipelineHierarchies { get; set; }

        public virtual async Task Commit()
        {
            await base.SaveChangesAsync();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new Configuration.ProjectConfiguration());
            modelBuilder.Configurations.Add(new Configuration.UserConfiguration());
            modelBuilder.Configurations.Add(new Configuration.ProjectUserConfiguration());
            modelBuilder.Configurations.Add(new Configuration.PipelineConfiguration());
            modelBuilder.Configurations.Add(new Configuration.PipelineInstanceConfiguration());
            modelBuilder.Configurations.Add(new Configuration.ProjectPipelineHierarchyConfiguration());
            modelBuilder.Configurations.Add(new Configuration.ModuleGroupConfiguration());
            modelBuilder.Configurations.Add(new Configuration.ParameterTypeConfiguration());
            modelBuilder.Configurations.Add(new Configuration.ModuleConfiguration());
            modelBuilder.Configurations.Add(new Configuration.ParameterConfiguration());
            modelBuilder.Configurations.Add(new Configuration.PipelineTaskConfiguration());
            modelBuilder.Configurations.Add(new Configuration.ScheduleConfiguration());
            modelBuilder.Configurations.Add(new Configuration.PipelineTaskInstanceConfiguration());
            modelBuilder.Configurations.Add(new Configuration.GapGroupConfiguration());
            modelBuilder.Configurations.Add(new Configuration.GapRoleConfiguration());
            modelBuilder.Configurations.Add(new Configuration.GapRolePrivilegeConfiguration());
            modelBuilder.Configurations.Add(new Configuration.GapGroupUserConfiguration());
            modelBuilder.Configurations.Add(new Configuration.GapRoleUserConfiguration());
            modelBuilder.Configurations.Add(new Configuration.GapGroupUserProjectConfiguration());
            modelBuilder.Configurations.Add(new Configuration.GapGroupUserPipelineConfiguration());
            modelBuilder.Configurations.Add(new Configuration.PipelineHierarchyConfiguration());

            modelBuilder.Configurations.Add(new Configuration.PipelineRunRequestConfiguration());
            modelBuilder.Configurations.Add(new Configuration.NotificationConfiguration());

            //modelBuilder.Entity<Module>().HasRequired<ModuleGroup>(p => p.ModuleGroup).WithMany().WillCascadeOnDelete(false);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Types().Configure(t => t.MapToStoredProcedures());
            modelBuilder.HasDefaultSchema("dbo");
        }
    }
}
