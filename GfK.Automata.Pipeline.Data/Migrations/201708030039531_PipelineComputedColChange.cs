namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineComputedColChange : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.Pipeline", "ParentToPipelineID", c => c.Int(nullable: false));
            AlterStoredProcedure(
                "dbo.Pipeline_Insert",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Pipeline]([PipeLineID], [Name], [ProjectID], [ParentID])
                      VALUES (@PipeLineID, @Name, @ProjectID, @ParentID)
                      
                      SELECT t0.[ParentToPipelineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
            
            AlterStoredProcedure(
                "dbo.Pipeline_Update",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Pipeline]
                      SET [Name] = @Name, [ProjectID] = @ProjectID, [ParentID] = @ParentID
                      WHERE ([PipeLineID] = @PipeLineID)
                      
                      SELECT t0.[ParentToPipelineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
            
        }
        
        public override void Down()
        {
            //DropColumn("dbo.Pipeline", "ParentToPipelineID");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
