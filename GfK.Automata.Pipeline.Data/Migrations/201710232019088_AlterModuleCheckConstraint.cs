namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;
    
    public partial class AlterModuleCheckConstraint : DbMigration
    {
        public override void Up()
        {
            this.DropConstraint("dbo.Module", "CK_Module_ModuleType");
            this.DropConstraint("dbo.Module", "DF_Module_ModuleType");

            AlterColumn("dbo.Module", "ModuleType", c => c.String(nullable: false, maxLength: 10, unicode: false));
            AlterStoredProcedure(
                "dbo.Module_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                        AssemblyName = p.String(maxLength: 150, unicode: false),
                        TypeName = p.String(maxLength: 150, unicode: false),
                        ModuleType = p.String(maxLength: 10, unicode: false),
                        ModuleGroupID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Module]([Name], [Description], [AssemblyName], [TypeName], [ModuleType], [ModuleGroupID])
                      VALUES (@Name, @Description, @AssemblyName, @TypeName, @ModuleType, @ModuleGroupID)
                      
                      DECLARE @ModuleID int
                      SELECT @ModuleID = [ModuleID]
                      FROM [dbo].[Module]
                      WHERE @@ROWCOUNT > 0 AND [ModuleID] = scope_identity()
                      
                      SELECT t0.[ModuleID]
                      FROM [dbo].[Module] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ModuleID] = @ModuleID"
            );
            
            AlterStoredProcedure(
                "dbo.Module_Update",
                p => new
                    {
                        ModuleID = p.Int(),
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                        AssemblyName = p.String(maxLength: 150, unicode: false),
                        TypeName = p.String(maxLength: 150, unicode: false),
                        ModuleType = p.String(maxLength: 10, unicode: false),
                        ModuleGroupID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Module]
                      SET [Name] = @Name, [Description] = @Description, [AssemblyName] = @AssemblyName, [TypeName] = @TypeName, [ModuleType] = @ModuleType, [ModuleGroupID] = @ModuleGroupID
                      WHERE ([ModuleID] = @ModuleID)"
            );

            this.CreateColumnDefault("dbo.Module", "ModuleType", "'BUILTIN'", "DF_Module_ModuleType");
            this.CreateCheckConstraint("dbo.Module", "([ModuleType]='BUILTIN' OR [ModuleType]='CUSTOM' OR [ModuleType]='SYSTEM' OR [ModuleType]='PIPELINE')", "CK_Module_ModuleType");
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Module", "ModuleType", c => c.String(nullable: false, maxLength: 10, unicode: false));
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
