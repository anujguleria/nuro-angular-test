namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnableCRUDStoredProcedures : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
                "dbo.Project_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100),
                    },
                body:
                    @"INSERT [dbo].[Project]([Name])
                      VALUES (@Name)
                      
                      DECLARE @ProjectID int
                      SELECT @ProjectID = [ProjectID]
                      FROM [dbo].[Project]
                      WHERE @@ROWCOUNT > 0 AND [ProjectID] = scope_identity()
                      
                      SELECT t0.[ProjectID]
                      FROM [dbo].[Project] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ProjectID] = @ProjectID"
            );
            
            CreateStoredProcedure(
                "dbo.Project_Update",
                p => new
                    {
                        ProjectID = p.Int(),
                        Name = p.String(maxLength: 100),
                    },
                body:
                    @"UPDATE [dbo].[Project]
                      SET [Name] = @Name
                      WHERE ([ProjectID] = @ProjectID)"
            );
            
            CreateStoredProcedure(
                "dbo.Project_Delete",
                p => new
                    {
                        ProjectID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Project]
                      WHERE ([ProjectID] = @ProjectID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Project_Delete");
            DropStoredProcedure("dbo.Project_Update");
            DropStoredProcedure("dbo.Project_Insert");
        }
    }
}
