namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateProjectPipelineID_Sequence : DbMigration
    {
        public override void Up()
        {
            Sql("CREATE SEQUENCE Project_Pipeline_ID_Seq " +
                "  AS INTEGER " +
                "  START WITH 100 " + 
                "  INCREMENT BY 1 " +
                "  MINVALUE 1 " +
                "  MAXVALUE 2147483647 " +
                "  NO CYCLE");
        }
        
        public override void Down()
        {
            Sql("DROP SEQUENCE Project_Pipeline_ID_Seq");
        }
    }
}
