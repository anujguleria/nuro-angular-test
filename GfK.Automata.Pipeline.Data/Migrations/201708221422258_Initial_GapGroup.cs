namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_GapGroup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GapGroup",
                c => new
                    {
                        GapGroupID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.GapGroupID)
                .Index(t => t.Name, unique: true, name: "Unq_GapProject_Name");
            
            CreateStoredProcedure(
                "dbo.GapGroup_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100),
                    },
                body:
                    @"INSERT [dbo].[GapGroup]([Name])
                      VALUES (@Name)
                      
                      DECLARE @GapGroupID int
                      SELECT @GapGroupID = [GapGroupID]
                      FROM [dbo].[GapGroup]
                      WHERE @@ROWCOUNT > 0 AND [GapGroupID] = scope_identity()
                      
                      SELECT t0.[GapGroupID]
                      FROM [dbo].[GapGroup] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[GapGroupID] = @GapGroupID"
            );
            
            CreateStoredProcedure(
                "dbo.GapGroup_Update",
                p => new
                    {
                        GapGroupID = p.Int(),
                        Name = p.String(maxLength: 100),
                    },
                body:
                    @"UPDATE [dbo].[GapGroup]
                      SET [Name] = @Name
                      WHERE ([GapGroupID] = @GapGroupID)"
            );
            
            CreateStoredProcedure(
                "dbo.GapGroup_Delete",
                p => new
                    {
                        GapGroupID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[GapGroup]
                      WHERE ([GapGroupID] = @GapGroupID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.GapGroup_Delete");
            DropStoredProcedure("dbo.GapGroup_Update");
            DropStoredProcedure("dbo.GapGroup_Insert");
            DropIndex("dbo.GapGroup", "Unq_GapProject_Name");
            DropTable("dbo.GapGroup");
        }
    }
}
