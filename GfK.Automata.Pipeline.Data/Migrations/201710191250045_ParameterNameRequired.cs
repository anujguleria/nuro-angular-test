namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParameterNameRequired : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Parameter", "Unq_Parameter_ModuleID_Name");
            AlterColumn("dbo.Parameter", "Name", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.Parameter", new[] { "ModuleID", "Name" }, unique: true, name: "Unq_Parameter_ModuleID_Name");
        }
        
        public override void Down()
        {

        }
    }
}
