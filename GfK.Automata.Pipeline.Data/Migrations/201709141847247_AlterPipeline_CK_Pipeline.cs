namespace GfK.Automata.Pipeline.Data.Migrations
{
    using GfK.Automata.Pipeline.Data.Migrations.Operations;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterPipeline_CK_Pipeline : DbMigration
    {
        public override void Up()
        {
            this.DropConstraint("Pipeline", "CK_Pipeline");

            this.CreateCheckConstraint("Pipeline"
                , @"((CASE WHEN ([ParentID] IS NOT NULL AND [ProjectID] IS NULL AND [ModuleID] IS NULL)
                             OR ([ParentID] IS NULL AND [ProjectID] IS NOT NULL AND [ModuleID] IS NULL) 
                             OR ([ParentID] IS NULL AND [ProjectID] IS NULL AND [ModuleID] IS NOT NULL) THEN (1) ELSE (0) END=(1)))"
                , "CK_Pipeline");
        }
        
        public override void Down()
        {
        }
    }
}
