namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    using GfK.Automata.Pipeline.Data.Migrations.Operations;
    
    public partial class SetProject_GapGroupID_Default : DbMigration
    {
        public override void Up()
        {
            this.CreateColumnDefault("Project", "GapGroupID", "1", "DF_Project_GroupID");
        }
        
        public override void Down()
        {
        }
    }
}
