namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPipelineRunRequestFK : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.PipelineRunRequest", new string[] { "PipelineID" }
                , "dbo.Pipeline", new string[] { "PipelineID" }
                , false
                , "FK_PipelineRunRequest_Pipeline_PipelineID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PipelineRunRequest", "FK_PipelineRunRequest_Pipeline_PipelineID");
        }
    }
}
