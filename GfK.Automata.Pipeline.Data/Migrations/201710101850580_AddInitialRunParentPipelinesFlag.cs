namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class AddInitialRunParentPipelinesFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PipelineRunRequest", "RunParentPipeline", c => c.Boolean());
            AlterStoredProcedure(
                "dbo.PipelineRunRequest_Insert",
                p => new
                    {
                        PipelineID = p.Int(),
                        RunParentPipeline = p.Boolean(),
                        RunRequestDate = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[PipelineRunRequest]([PipelineID], [RunParentPipeline], [RunRequestDate])
                      VALUES (@PipelineID, @RunParentPipeline, @RunRequestDate)
                      
                      DECLARE @PipelineRunRequestID int
                      SELECT @PipelineRunRequestID = [PipelineRunRequestID]
                      FROM [dbo].[PipelineRunRequest]
                      WHERE @@ROWCOUNT > 0 AND [PipelineRunRequestID] = scope_identity()
                      
                      SELECT t0.[PipelineRunRequestID]
                      FROM [dbo].[PipelineRunRequest] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineRunRequestID] = @PipelineRunRequestID"
            );
            
            AlterStoredProcedure(
                "dbo.PipelineRunRequest_Update",
                p => new
                    {
                        PipelineRunRequestID = p.Int(),
                        PipelineID = p.Int(),
                        RunParentPipeline = p.Boolean(),
                        RunRequestDate = p.DateTime(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineRunRequest]
                      SET [PipelineID] = @PipelineID, [RunParentPipeline] = @RunParentPipeline, [RunRequestDate] = @RunRequestDate
                      WHERE ([PipelineRunRequestID] = @PipelineRunRequestID)"
            );

            this.CreateColumnDefault("dbo.PipelineRunRequest", "RunParentPipeline", "1", "DF_PipelineRunRequest_RunParentPipeline");

            Sql("UPDATE dbo.PipelineRunRequest SET RunParentPipeline = 1");
        }
        
        public override void Down()
        {
            
            DropColumn("dbo.PipelineRunRequest", "RunParentPipeline");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
