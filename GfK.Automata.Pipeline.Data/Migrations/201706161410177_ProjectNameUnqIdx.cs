namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectNameUnqIdx : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Project", "Name", unique: true, name: "Unq_Name");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Project", "Unq_Name");
        }
    }
}
