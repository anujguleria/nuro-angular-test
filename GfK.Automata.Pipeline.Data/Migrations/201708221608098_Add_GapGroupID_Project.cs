namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_GapGroupID_Project : DbMigration
    {
        public override void Up()
        {
            RenameIndex(table: "dbo.Project", name: "Unq_Name", newName: "Unq_Project_Name");
            AddColumn("dbo.Project", "GapGroupID", c => c.Int());
            AlterStoredProcedure(
                "dbo.Project_Insert",
                p => new
                    {
                        ProjectID = p.Int(),
                        Name = p.String(maxLength: 100),
                        GapGroupID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Project]([ProjectID], [Name], [GapGroupID])
                      VALUES (@ProjectID, @Name, @GapGroupID)"
            );
            
            AlterStoredProcedure(
                "dbo.Project_Update",
                p => new
                    {
                        ProjectID = p.Int(),
                        Name = p.String(maxLength: 100),
                        GapGroupID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Project]
                      SET [Name] = @Name, [GapGroupID] = @GapGroupID
                      WHERE ([ProjectID] = @ProjectID)"
            );
            
        }
        
        public override void Down()
        {
            DropColumn("dbo.Project", "GapGroupID");
            RenameIndex(table: "dbo.Project", name: "Unq_Project_Name", newName: "Unq_Name");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
