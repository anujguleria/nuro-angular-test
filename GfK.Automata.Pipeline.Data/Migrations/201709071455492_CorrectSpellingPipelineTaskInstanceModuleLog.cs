namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrectSpellingPipelineTaskInstanceModuleLog : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PipeineTaskInstanceModuleLog", newName: "PipelineTaskInstanceModuleLog");
            DropPrimaryKey("dbo.PipelineTaskInstanceModuleLog");
            DropColumn("dbo.PipelineTaskInstanceModuleLog", "PipeineTaskInstanceModuleLogID");
            AddColumn("dbo.PipelineTaskInstanceModuleLog", "PipelineTaskInstanceModuleLogID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.PipelineTaskInstanceModuleLog", "PipelineTaskInstanceModuleLogID");

            CreateStoredProcedure(
                "dbo.PipelineTaskInstanceModuleLog_Insert",
                p => new
                    {
                        LogDate = p.DateTime(),
                        LogMessageType = p.String(),
                        LogMessage = p.String(),
                        PipelineTaskInstanceId = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PipelineTaskInstanceModuleLog]([LogDate], [LogMessageType], [LogMessage], [PipelineTaskInstanceId])
                      VALUES (@LogDate, @LogMessageType, @LogMessage, @PipelineTaskInstanceId)
                      
                      DECLARE @PipelineTaskInstanceModuleLogID int
                      SELECT @PipelineTaskInstanceModuleLogID = [PipelineTaskInstanceModuleLogID]
                      FROM [dbo].[PipelineTaskInstanceModuleLog]
                      WHERE @@ROWCOUNT > 0 AND [PipelineTaskInstanceModuleLogID] = scope_identity()
                      
                      SELECT t0.[PipelineTaskInstanceModuleLogID]
                      FROM [dbo].[PipelineTaskInstanceModuleLog] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineTaskInstanceModuleLogID] = @PipelineTaskInstanceModuleLogID"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTaskInstanceModuleLog_Update",
                p => new
                    {
                        PipelineTaskInstanceModuleLogID = p.Int(),
                        LogDate = p.DateTime(),
                        LogMessageType = p.String(),
                        LogMessage = p.String(),
                        PipelineTaskInstanceId = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineTaskInstanceModuleLog]
                      SET [LogDate] = @LogDate, [LogMessageType] = @LogMessageType, [LogMessage] = @LogMessage, [PipelineTaskInstanceId] = @PipelineTaskInstanceId
                      WHERE ([PipelineTaskInstanceModuleLogID] = @PipelineTaskInstanceModuleLogID)"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTaskInstanceModuleLog_Delete",
                p => new
                    {
                        PipelineTaskInstanceModuleLogID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PipelineTaskInstanceModuleLog]
                      WHERE ([PipelineTaskInstanceModuleLogID] = @PipelineTaskInstanceModuleLogID)"
            );
            
            DropStoredProcedure("dbo.PipeineTaskInstanceModuleLog_Insert");
            DropStoredProcedure("dbo.PipeineTaskInstanceModuleLog_Update");
            DropStoredProcedure("dbo.PipeineTaskInstanceModuleLog_Delete");
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.PipelineTaskInstanceModuleLog_Delete");
            DropStoredProcedure("dbo.PipelineTaskInstanceModuleLog_Update");
            DropStoredProcedure("dbo.PipelineTaskInstanceModuleLog_Insert");
            AddColumn("dbo.PipelineTaskInstanceModuleLog", "PipeineTaskInstanceModuleLogID", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.PipelineTaskInstanceModuleLog");
            DropColumn("dbo.PipelineTaskInstanceModuleLog", "PipelineTaskInstanceModuleLogID");
            AddPrimaryKey("dbo.PipelineTaskInstanceModuleLog", "PipeineTaskInstanceModuleLogID");
            RenameTable(name: "dbo.PipelineTaskInstanceModuleLog", newName: "PipeineTaskInstanceModuleLog");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
