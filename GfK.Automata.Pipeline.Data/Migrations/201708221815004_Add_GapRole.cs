namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_GapRole : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GapRole",
                c => new
                    {
                        GapRoleID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.GapRoleID)
                .Index(t => t.Name, unique: true, name: "Unq_GapRole_Name");
            
            CreateStoredProcedure(
                "dbo.GapRole_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100, unicode: false),
                    },
                body:
                    @"INSERT [dbo].[GapRole]([Name])
                      VALUES (@Name)
                      
                      DECLARE @GapRoleID int
                      SELECT @GapRoleID = [GapRoleID]
                      FROM [dbo].[GapRole]
                      WHERE @@ROWCOUNT > 0 AND [GapRoleID] = scope_identity()
                      
                      SELECT t0.[GapRoleID]
                      FROM [dbo].[GapRole] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[GapRoleID] = @GapRoleID"
            );
            
            CreateStoredProcedure(
                "dbo.GapRole_Update",
                p => new
                    {
                        GapRoleID = p.Int(),
                        Name = p.String(maxLength: 100, unicode: false),
                    },
                body:
                    @"UPDATE [dbo].[GapRole]
                      SET [Name] = @Name
                      WHERE ([GapRoleID] = @GapRoleID)"
            );
            
            CreateStoredProcedure(
                "dbo.GapRole_Delete",
                p => new
                    {
                        GapRoleID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[GapRole]
                      WHERE ([GapRoleID] = @GapRoleID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.GapRole_Delete");
            DropStoredProcedure("dbo.GapRole_Update");
            DropStoredProcedure("dbo.GapRole_Insert");
            DropIndex("dbo.GapRole", "Unq_GapRole_Name");
            DropTable("dbo.GapRole");
        }
    }
}
