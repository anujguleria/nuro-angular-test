namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_GapRole_User : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GapRoleUser",
                c => new
                    {
                        GapRole_GapRoleID = c.Int(nullable: false),
                        User_UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GapRole_GapRoleID, t.User_UserID })
                .ForeignKey("dbo.GapRole", t => t.GapRole_GapRoleID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.User_UserID, cascadeDelete: true)
                .Index(t => t.GapRole_GapRoleID)
                .Index(t => t.User_UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GapRoleUser", "User_UserID", "dbo.User");
            DropForeignKey("dbo.GapRoleUser", "GapRole_GapRoleID", "dbo.GapRole");
            DropIndex("dbo.GapRoleUser", new[] { "User_UserID" });
            DropIndex("dbo.GapRoleUser", new[] { "GapRole_GapRoleID" });
            DropTable("dbo.GapRoleUser");
        }
    }
}
