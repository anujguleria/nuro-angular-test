namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPipelineHierarchyView : DbMigration
    {
        public override void Up()
        {
            Sql(@"CREATE  VIEW [dbo].[PipelineHierarchy] AS
                WITH pipes AS
                ( SELECT Name, ParentID, PipeLineID, Schedule_ScheduleID, PipeLineID AS leafPipelineID, 1 AS treelevel
                    FROM [dbo].[Pipeline] AS leaf
                  UNION ALL
                SELECT pip.Name, pip.ParentID, pip.PipeLineID, pip.Schedule_ScheduleID, pipes.leafPipeLineID, treeLevel + 1
                    FROM [dbo].[Pipeline] AS pip
	                JOIN pipes
	                  ON pipes.ParentID = pip.PipeLineID
                )
                SELECT [Name], leafPipelineID as PipelineID, PipelineID as HierarchyPipelineID, Schedule_ScheduleID, treelevel
                  FROM pipes"
                );            
        }
        
        public override void Down()
        {
            Sql("DROP VIEW [dbo].[PipelineHierarchy] ");
        }
    }
}
