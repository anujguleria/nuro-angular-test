namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModuleAssemblyTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Module", "AssemblyName", c => c.String(maxLength: 150, unicode: false));
            AddColumn("dbo.Module", "TypeName", c => c.String(maxLength: 150, unicode: false));
            AlterStoredProcedure(
                "dbo.Module_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                        AssemblyName = p.String(maxLength: 150, unicode: false),
                        TypeName = p.String(maxLength: 150, unicode: false),
                        ModuleGroupID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Module]([Name], [Description], [AssemblyName], [TypeName], [ModuleGroupID])
                      VALUES (@Name, @Description, @AssemblyName, @TypeName, @ModuleGroupID)
                      
                      DECLARE @ModuleID int
                      SELECT @ModuleID = [ModuleID]
                      FROM [dbo].[Module]
                      WHERE @@ROWCOUNT > 0 AND [ModuleID] = scope_identity()
                      
                      SELECT t0.[ModuleID]
                      FROM [dbo].[Module] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ModuleID] = @ModuleID"
            );
            
            AlterStoredProcedure(
                "dbo.Module_Update",
                p => new
                    {
                        ModuleID = p.Int(),
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                        AssemblyName = p.String(maxLength: 150, unicode: false),
                        TypeName = p.String(maxLength: 150, unicode: false),
                        ModuleGroupID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Module]
                      SET [Name] = @Name, [Description] = @Description, [AssemblyName] = @AssemblyName, [TypeName] = @TypeName, [ModuleGroupID] = @ModuleGroupID
                      WHERE ([ModuleID] = @ModuleID)"
            );
            
        }
        
        public override void Down()
        {
            DropColumn("dbo.Module", "TypeName");
            DropColumn("dbo.Module", "AssemblyName");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
