namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateGapRoleUserAsEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GapRoleUser", "GapRole_GapRoleID", "dbo.GapRole");
            DropForeignKey("dbo.GapRoleUser", "User_UserID", "dbo.User");
            DropIndex("dbo.GapRoleUser", new[] { "GapRole_GapRoleID" });
            DropIndex("dbo.GapRoleUser", new[] { "User_UserID" });
            DropTable("dbo.GapRoleUser");

            CreateTable(
                "dbo.GapRoleUser",
                c => new
                    {
                        GapRoleID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GapRoleID, t.UserID })
                .ForeignKey("dbo.GapRole", t => t.GapRoleID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.GapRoleID)
                .Index(t => t.UserID);
            

            CreateStoredProcedure(
                "dbo.GapRoleUser_Insert",
                p => new
                    {
                        GapRoleID = p.Int(),
                        UserID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[GapRoleUser]([GapRoleID], [UserID])
                      VALUES (@GapRoleID, @UserID)"
            );
            
            CreateStoredProcedure(
                "dbo.GapRoleUser_Update",
                p => new
                    {
                        GapRoleID = p.Int(),
                        UserID = p.Int(),
                    },
                body:
                    @"RETURN"
            );
            
            CreateStoredProcedure(
                "dbo.GapRoleUser_Delete",
                p => new
                    {
                        GapRoleID = p.Int(),
                        UserID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[GapRoleUser]
                      WHERE (([GapRoleID] = @GapRoleID) AND ([UserID] = @UserID))"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.GapRoleUser_Delete");
            DropStoredProcedure("dbo.GapRoleUser_Update");
            DropStoredProcedure("dbo.GapRoleUser_Insert");
            CreateTable(
                "dbo.GapRoleUser",
                c => new
                    {
                        GapRole_GapRoleID = c.Int(nullable: false),
                        User_UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GapRole_GapRoleID, t.User_UserID });
            
            DropForeignKey("dbo.GapRoleUser", "UserID", "dbo.User");
            DropForeignKey("dbo.GapRoleUser", "GapRoleID", "dbo.GapRole");
            DropIndex("dbo.GapRoleUser", new[] { "UserID" });
            DropIndex("dbo.GapRoleUser", new[] { "GapRoleID" });
            DropTable("dbo.GapRoleUser");
            CreateIndex("dbo.GapRoleUser", "User_UserID");
            CreateIndex("dbo.GapRoleUser", "GapRole_GapRoleID");
            AddForeignKey("dbo.GapRoleUser", "User_UserID", "dbo.User", "UserID", cascadeDelete: true);
            AddForeignKey("dbo.GapRoleUser", "GapRole_GapRoleID", "dbo.GapRole", "GapRoleID", cascadeDelete: true);
        }
    }
}
