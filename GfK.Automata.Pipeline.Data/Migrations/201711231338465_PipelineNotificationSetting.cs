namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineNotificationSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PipelineNotificationSetting",
                c => new
                    {
                        PipelineNotificationSettingID = c.Int(nullable: false, identity: true),
                        PipelineID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        PipelineNotificationSettingType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PipelineNotificationSettingID)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Pipeline", t => t.PipelineID, cascadeDelete: true)
                .Index(t => t.PipelineID)
                .Index(t => t.UserID);
            
            CreateStoredProcedure(
                "dbo.PipelineNotificationSetting_Insert",
                p => new
                    {
                        PipelineID = p.Int(),
                        UserID = p.Int(),
                        PipelineNotificationSettingType = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PipelineNotificationSetting]([PipelineID], [UserID], [PipelineNotificationSettingType])
                      VALUES (@PipelineID, @UserID, @PipelineNotificationSettingType)
                      
                      DECLARE @PipelineNotificationSettingID int
                      SELECT @PipelineNotificationSettingID = [PipelineNotificationSettingID]
                      FROM [dbo].[PipelineNotificationSetting]
                      WHERE @@ROWCOUNT > 0 AND [PipelineNotificationSettingID] = scope_identity()
                      
                      SELECT t0.[PipelineNotificationSettingID]
                      FROM [dbo].[PipelineNotificationSetting] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineNotificationSettingID] = @PipelineNotificationSettingID"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineNotificationSetting_Update",
                p => new
                    {
                        PipelineNotificationSettingID = p.Int(),
                        PipelineID = p.Int(),
                        UserID = p.Int(),
                        PipelineNotificationSettingType = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineNotificationSetting]
                      SET [PipelineID] = @PipelineID, [UserID] = @UserID, [PipelineNotificationSettingType] = @PipelineNotificationSettingType
                      WHERE ([PipelineNotificationSettingID] = @PipelineNotificationSettingID)"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineNotificationSetting_Delete",
                p => new
                    {
                        PipelineNotificationSettingID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PipelineNotificationSetting]
                      WHERE ([PipelineNotificationSettingID] = @PipelineNotificationSettingID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.PipelineNotificationSetting_Delete");
            DropStoredProcedure("dbo.PipelineNotificationSetting_Update");
            DropStoredProcedure("dbo.PipelineNotificationSetting_Insert");
            DropForeignKey("dbo.PipelineNotificationSetting", "PipelineID", "dbo.Pipeline");
            DropForeignKey("dbo.PipelineNotificationSetting", "UserID", "dbo.User");
            DropIndex("dbo.PipelineNotificationSetting", new[] { "UserID" });
            DropIndex("dbo.PipelineNotificationSetting", new[] { "PipelineID" });
            DropTable("dbo.PipelineNotificationSetting");
        }
    }
}
