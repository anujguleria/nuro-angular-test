namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Index_FK_Cleanup : DbMigration
    {
        public override void Up()
        {
            this.DropIndex("dbo.User", "Unq_Name");
            this.CreateIndex("dbo.User", "Email", true, "Unq_User_Email");

            this.AddForeignKey("dbo.PipelineTaskInputValue", "PriorPipeLineTaskParameterID", "dbo.Parameter", "ParameterID", false);
        }
        
        public override void Down()
        {
        }
    }
}
