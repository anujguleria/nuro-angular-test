namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seed_RolePrivilegeMapping : DbMigration
    {
        public override void Up()
        {
            // Retaining but commenting because these rely on the Seeded data.   Seeding runs after all migrations meaning that 
            // these may run before necessary data is seeded.  This will cause a sql error.

            //Sql("INSERT INTO dbo.GapRolePrivilegeGapRole(GapRole_GapRoleID, GapRolePrivilege_GapRolePrivilegeID) "
            //    + "SELECT GapRoleID, GapRolePrivilegeID FROM dbo.GapRole CROSS JOIN dbo.GapRolePrivilege WHERE GapRole.Name = 'Admin'");

            //Sql("INSERT INTO dbo.GapRolePrivilegeGapRole(GapRole_GapRoleID, GapRolePrivilege_GapRolePrivilegeID) "
            //    + "VALUES((SELECT GapRoleID FROM dbo.GapRole WHERE Name = 'Execute')," 
            //    + "       (SELECT GapRolePrivilegeID FROM dbo.GapRolePrivilege WHERE Name = 'Run Pipeline'))");

            //Sql("INSERT INTO dbo.GapRolePrivilegeGapRole(GapRole_GapRoleID, GapRolePrivilege_GapRolePrivilegeID) "
            //    + "VALUES((SELECT GapRoleID FROM dbo.GapRole WHERE Name = 'Execute'),"
            //    + "       (SELECT GapRolePrivilegeID FROM dbo.GapRolePrivilege WHERE Name = 'View Results'))");

            //Sql("INSERT INTO dbo.GapRolePrivilegeGapRole(GapRole_GapRoleID, GapRolePrivilege_GapRolePrivilegeID) "
            //    + "VALUES((SELECT GapRoleID FROM dbo.GapRole WHERE Name = 'Configure'),"
            //    + "       (SELECT GapRolePrivilegeID FROM dbo.GapRolePrivilege WHERE Name = 'Run Pipeline'))");

            //Sql("INSERT INTO dbo.GapRolePrivilegeGapRole(GapRole_GapRoleID, GapRolePrivilege_GapRolePrivilegeID) "
            //    + "VALUES((SELECT GapRoleID FROM dbo.GapRole WHERE Name = 'Configure'),"
            //    + "       (SELECT GapRolePrivilegeID FROM dbo.GapRolePrivilege WHERE Name = 'View Results'))");

            //Sql("INSERT INTO dbo.GapRolePrivilegeGapRole(GapRole_GapRoleID, GapRolePrivilege_GapRolePrivilegeID) "
            //    + "VALUES((SELECT GapRoleID FROM dbo.GapRole WHERE Name = 'Configure'),"
            //    + "       (SELECT GapRolePrivilegeID FROM dbo.GapRolePrivilege WHERE Name = 'Edit Pipeline'))");

            //Sql("INSERT INTO dbo.GapRolePrivilegeGapRole(GapRole_GapRoleID, GapRolePrivilege_GapRolePrivilegeID) "
            //    + "VALUES((SELECT GapRoleID FROM dbo.GapRole WHERE Name = 'Configure'),"
            //    + "       (SELECT GapRolePrivilegeID FROM dbo.GapRolePrivilege WHERE Name = 'Configure Pipeline'))");
        }
        
        public override void Down()
        {
            Sql("TRUNCATE TABLE dbo.GapRolePrivilegeGapRole");
        }
    }
}
