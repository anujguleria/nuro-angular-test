namespace GfK.Automata.Pipeline.Data.Migrations
{
    using GfK.Automata.Pipeline.Data.Migrations.Operations;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterPipeline_ParentComputedColumn : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Pipeline", "Unq_Pipeline_ParentToPipelineID_Name");

            Sql("ALTER TABLE dbo.Pipeline DROP COLUMN ParentToPipelineID");

            this.CreatePersistedComputedColumn("dbo.Pipeline", "ParentToPipelineID"
                , "CASE WHEN [ProjectID] IS NOT NULL THEN [ProjectID] WHEN [ParentID] IS NOT NULL THEN [ParentID] ELSE [ModuleID] END");

            string[] columnList = new string[] { "ParentToPipelineID", "Name" };
            CreateIndex("dbo.Pipeline", columnList, true, "Unq_Pipeline_ParentToPipelineID_Name");
        }
        
        public override void Down()
        {

        }
    }
}
