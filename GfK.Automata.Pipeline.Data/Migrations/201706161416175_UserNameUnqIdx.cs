namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserNameUnqIdx : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.User", "Email", unique: true, name: "Unq_Name");
        }
        
        public override void Down()
        {
            DropIndex("dbo.User", "Unq_Name");
        }
    }
}
