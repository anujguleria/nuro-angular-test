namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class SetRunParentPipelineDefaultTrue : DbMigration
    {
        public override void Up()
        {
            this.DropConstraint("dbo.PipelineRunRequest", "DF_PipelineRunRequest_RunParentPipeline");
            this.CreateColumnDefault("dbo.PipelineRunRequest", "RunParentPipeline", "1", "DF_PipelineRunRequest_RunParentPipeline");
        }
        
        public override void Down()
        {
        }
    }
}
