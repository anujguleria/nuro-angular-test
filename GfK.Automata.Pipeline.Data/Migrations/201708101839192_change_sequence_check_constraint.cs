namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class change_sequence_check_constraint : DbMigration
    {
        public override void Up()
        {
            this.DropConstraint("PipelineTask", "CK_PipelineTask_SequencePosition");
            this.CreateCheckConstraint("PipelineTask"
                , "(SequencePosition >= 0)"
                , "CK_PipelineTask_SequencePosition");
        }
        
        public override void Down()
        {
            this.DropConstraint("PipelineTask", "CK_PipelineTask_SequencePosition");
            this.CreateCheckConstraint("PipelineTask"
                , "(SequencePosition > 0)"
                , "CK_PipelineTask_SequencePosition");
        }
    }
}
