namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class AddPipelineParentComputedColumn : DbMigration
    {
        public override void Up()
        {
            this.CreatePersistedComputedColumn("dbo.Pipeline", "ParentToPipelineID"
                , "CASE WHEN [ProjectID] IS NOT NULL THEN [ProjectID] ELSE [ParentID] END");
        }
        
        public override void Down()
        {
            Sql("ALTER TABLE dbo.Pipeline DROP COLUMN ParentToPipelineID");
        }
    }
}
