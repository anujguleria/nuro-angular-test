namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModule_ParameterTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModuleGroup",
                c => new
                    {
                        ModuleGroupID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Description = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.ModuleGroupID)
                .Index(t => t.Name, unique: true, name: "Unq_ModuleGroup_Name");
            
            CreateTable(
                "dbo.ParameterType",
                c => new
                    {
                        ParameterTypeID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.ParameterTypeID)
                .Index(t => t.Name, unique: true, name: "Unq_ParameterType_Name");
                       
            CreateStoredProcedure(
                "dbo.ModuleGroup_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 200),
                        Description = p.String(maxLength: 1000),
                    },
                body:
                    @"INSERT [dbo].[ModuleGroup]([Name], [Description])
                      VALUES (@Name, @Description)
                      
                      DECLARE @ModuleGroupID int
                      SELECT @ModuleGroupID = [ModuleGroupID]
                      FROM [dbo].[ModuleGroup]
                      WHERE @@ROWCOUNT > 0 AND [ModuleGroupID] = scope_identity()
                      
                      SELECT t0.[ModuleGroupID]
                      FROM [dbo].[ModuleGroup] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ModuleGroupID] = @ModuleGroupID"
            );
            
            CreateStoredProcedure(
                "dbo.ModuleGroup_Update",
                p => new
                    {
                        ModuleGroupID = p.Int(),
                        Name = p.String(maxLength: 200),
                        Description = p.String(maxLength: 1000),
                    },
                body:
                    @"UPDATE [dbo].[ModuleGroup]
                      SET [Name] = @Name, [Description] = @Description
                      WHERE ([ModuleGroupID] = @ModuleGroupID)"
            );
            
            CreateStoredProcedure(
                "dbo.ModuleGroup_Delete",
                p => new
                    {
                        ModuleGroupID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[ModuleGroup]
                      WHERE ([ModuleGroupID] = @ModuleGroupID)"
            );
            
            CreateStoredProcedure(
                "dbo.ParameterType_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                    },
                body:
                    @"INSERT [dbo].[ParameterType]([Name], [Description])
                      VALUES (@Name, @Description)
                      
                      DECLARE @ParameterTypeID int
                      SELECT @ParameterTypeID = [ParameterTypeID]
                      FROM [dbo].[ParameterType]
                      WHERE @@ROWCOUNT > 0 AND [ParameterTypeID] = scope_identity()
                      
                      SELECT t0.[ParameterTypeID]
                      FROM [dbo].[ParameterType] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ParameterTypeID] = @ParameterTypeID"
            );
            
            CreateStoredProcedure(
                "dbo.ParameterType_Update",
                p => new
                    {
                        ParameterTypeID = p.Int(),
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                    },
                body:
                    @"UPDATE [dbo].[ParameterType]
                      SET [Name] = @Name, [Description] = @Description
                      WHERE ([ParameterTypeID] = @ParameterTypeID)"
            );
            
            CreateStoredProcedure(
                "dbo.ParameterType_Delete",
                p => new
                    {
                        ParameterTypeID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[ParameterType]
                      WHERE ([ParameterTypeID] = @ParameterTypeID)"
            );
            
            AlterStoredProcedure(
                "dbo.Pipeline_Insert",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Pipeline]([PipeLineID], [Name], [ProjectID], [ParentID])
                      VALUES (@PipeLineID, @Name, @ProjectID, @ParentID)"
            );
            
            AlterStoredProcedure(
                "dbo.Pipeline_Update",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Pipeline]
                      SET [Name] = @Name, [ProjectID] = @ProjectID, [ParentID] = @ParentID
                      WHERE ([PipeLineID] = @PipeLineID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.ParameterType_Delete");
            DropStoredProcedure("dbo.ParameterType_Update");
            DropStoredProcedure("dbo.ParameterType_Insert");
            DropStoredProcedure("dbo.ModuleGroup_Delete");
            DropStoredProcedure("dbo.ModuleGroup_Update");
            DropStoredProcedure("dbo.ModuleGroup_Insert");
            AddColumn("dbo.Pipeline", "ParentToPipelineID", c => c.Int(nullable: false));
            DropIndex("dbo.ParameterType", "Unq_ParameterType_Name");
            DropIndex("dbo.ModuleGroup", "Unq_ModuleGroup_Name");
            DropTable("dbo.ParameterType");
            DropTable("dbo.ModuleGroup");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
