// <auto-generated />
namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AlterPipeline_CK_Pipeline : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AlterPipeline_CK_Pipeline));
        
        string IMigrationMetadata.Id
        {
            get { return "201709141847247_AlterPipeline_CK_Pipeline"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
