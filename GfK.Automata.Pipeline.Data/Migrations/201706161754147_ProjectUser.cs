namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectUser : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectUser",
                c => new
                    {
                        ProjectID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        LastAccess = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProjectID, t.UserID })
                .ForeignKey("dbo.Project", t => t.ProjectID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.ProjectID)
                .Index(t => t.UserID);
            
            CreateStoredProcedure(
                "dbo.ProjectUser_Insert",
                p => new
                    {
                        ProjectID = p.Int(),
                        UserID = p.Int(),
                        LastAccess = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[ProjectUser]([ProjectID], [UserID], [LastAccess])
                      VALUES (@ProjectID, @UserID, @LastAccess)"
            );
            
            CreateStoredProcedure(
                "dbo.ProjectUser_Update",
                p => new
                    {
                        ProjectID = p.Int(),
                        UserID = p.Int(),
                        LastAccess = p.DateTime(),
                    },
                body:
                    @"UPDATE [dbo].[ProjectUser]
                      SET [LastAccess] = @LastAccess
                      WHERE (([ProjectID] = @ProjectID) AND ([UserID] = @UserID))"
            );
            
            CreateStoredProcedure(
                "dbo.ProjectUser_Delete",
                p => new
                    {
                        ProjectID = p.Int(),
                        UserID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[ProjectUser]
                      WHERE (([ProjectID] = @ProjectID) AND ([UserID] = @UserID))"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.ProjectUser_Delete");
            DropStoredProcedure("dbo.ProjectUser_Update");
            DropStoredProcedure("dbo.ProjectUser_Insert");
            DropForeignKey("dbo.ProjectUser", "UserID", "dbo.User");
            DropForeignKey("dbo.ProjectUser", "ProjectID", "dbo.Project");
            DropIndex("dbo.ProjectUser", new[] { "UserID" });
            DropIndex("dbo.ProjectUser", new[] { "ProjectID" });
            DropTable("dbo.ProjectUser");
        }
    }
}
