namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineModule : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pipeline", "ModuleID", c => c.Int());
            CreateIndex("dbo.Pipeline", "ModuleID");
            AddForeignKey("dbo.Pipeline", "ModuleID", "dbo.Module", "ModuleID");
            AlterStoredProcedure(
                "dbo.Pipeline_Insert",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                        ModuleID = p.Int(),
                        Schedule_ScheduleID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Pipeline]([PipeLineID], [Name], [ProjectID], [ParentID], [ModuleID], [Schedule_ScheduleID])
                      VALUES (@PipeLineID, @Name, @ProjectID, @ParentID, @ModuleID, @Schedule_ScheduleID)
                      
                      SELECT t0.[ParentToPipelineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
            
            AlterStoredProcedure(
                "dbo.Pipeline_Update",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                        ModuleID = p.Int(),
                        Schedule_ScheduleID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Pipeline]
                      SET [Name] = @Name, [ProjectID] = @ProjectID, [ParentID] = @ParentID, [ModuleID] = @ModuleID, [Schedule_ScheduleID] = @Schedule_ScheduleID
                      WHERE ([PipeLineID] = @PipeLineID)
                      
                      SELECT t0.[ParentToPipelineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pipeline", "ModuleID", "dbo.Module");
            DropIndex("dbo.Pipeline", new[] { "ModuleID" });
            DropColumn("dbo.Pipeline", "ModuleID");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
