namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CascadeParameterDelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PipelineTaskInputValue", "ParameterID", "dbo.Parameter");
            DropForeignKey("dbo.UserInputValue", "ParameterID", "dbo.Parameter");
            AddForeignKey("dbo.PipelineTaskInputValue", "ParameterID", "dbo.Parameter", "ParameterID", cascadeDelete: true);
            AddForeignKey("dbo.UserInputValue", "ParameterID", "dbo.Parameter", "ParameterID", cascadeDelete: true);
        }
        
        public override void Down()
        {

        }
    }
}
