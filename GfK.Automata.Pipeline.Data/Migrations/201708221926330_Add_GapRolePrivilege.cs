namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_GapRolePrivilege : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GapRolePrivilege",
                c => new
                    {
                        GapRolePrivilegeID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.GapRolePrivilegeID)
                .Index(t => t.Name, unique: true, name: "Unq_GapRolePrivilege_Name");
            
            CreateTable(
                "dbo.GapRolePrivilegeGapRole",
                c => new
                    {
                        GapRolePrivilege_GapRolePrivilegeID = c.Int(nullable: false),
                        GapRole_GapRoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GapRolePrivilege_GapRolePrivilegeID, t.GapRole_GapRoleID })
                .ForeignKey("dbo.GapRolePrivilege", t => t.GapRolePrivilege_GapRolePrivilegeID, cascadeDelete: true)
                .ForeignKey("dbo.GapRole", t => t.GapRole_GapRoleID, cascadeDelete: true)
                .Index(t => t.GapRolePrivilege_GapRolePrivilegeID)
                .Index(t => t.GapRole_GapRoleID);
            
            CreateStoredProcedure(
                "dbo.GapRolePrivilege_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100, unicode: false),
                    },
                body:
                    @"INSERT [dbo].[GapRolePrivilege]([Name])
                      VALUES (@Name)
                      
                      DECLARE @GapRolePrivilegeID int
                      SELECT @GapRolePrivilegeID = [GapRolePrivilegeID]
                      FROM [dbo].[GapRolePrivilege]
                      WHERE @@ROWCOUNT > 0 AND [GapRolePrivilegeID] = scope_identity()
                      
                      SELECT t0.[GapRolePrivilegeID]
                      FROM [dbo].[GapRolePrivilege] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[GapRolePrivilegeID] = @GapRolePrivilegeID"
            );
            
            CreateStoredProcedure(
                "dbo.GapRolePrivilege_Update",
                p => new
                    {
                        GapRolePrivilegeID = p.Int(),
                        Name = p.String(maxLength: 100, unicode: false),
                    },
                body:
                    @"UPDATE [dbo].[GapRolePrivilege]
                      SET [Name] = @Name
                      WHERE ([GapRolePrivilegeID] = @GapRolePrivilegeID)"
            );
            
            CreateStoredProcedure(
                "dbo.GapRolePrivilege_Delete",
                p => new
                    {
                        GapRolePrivilegeID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[GapRolePrivilege]
                      WHERE ([GapRolePrivilegeID] = @GapRolePrivilegeID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.GapRolePrivilege_Delete");
            DropStoredProcedure("dbo.GapRolePrivilege_Update");
            DropStoredProcedure("dbo.GapRolePrivilege_Insert");
            DropForeignKey("dbo.GapRolePrivilegeGapRole", "GapRole_GapRoleID", "dbo.GapRole");
            DropForeignKey("dbo.GapRolePrivilegeGapRole", "GapRolePrivilege_GapRolePrivilegeID", "dbo.GapRolePrivilege");
            DropIndex("dbo.GapRolePrivilegeGapRole", new[] { "GapRole_GapRoleID" });
            DropIndex("dbo.GapRolePrivilegeGapRole", new[] { "GapRolePrivilege_GapRolePrivilegeID" });
            DropIndex("dbo.GapRolePrivilege", "Unq_GapRolePrivilege_Name");
            DropTable("dbo.GapRolePrivilegeGapRole");
            DropTable("dbo.GapRolePrivilege");
        }
    }
}
