namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class AddStatus_Direction_CheckConstraints : DbMigration
    {
        public override void Up()
        {
            this.CreateCheckConstraint("Parameter"
                , "(Direction ='In' OR Direction ='Out' OR Direction ='InOut')"
                , "CK_Parameter_Direction");

            this.CreateCheckConstraint("PipelineInstance"
                , "(Status ='Completed' OR Status ='Success' OR Status ='Failed' OR Status ='Running' OR Status ='Canceled')"
                , "CK_PipelineInstance_Status");

            this.CreateCheckConstraint("PipelineTaskInstance"
                , "(Status ='Completed' OR Status ='Success' OR Status ='Failed' OR Status ='Running' OR Status ='Canceled')"
                , "CK_PipelineTaskInstance_Status");

            this.CreateCheckConstraint("PipeineTaskInstanceModuleLog"
                , "(LogMessageType ='Informational' OR LogMessageType ='Warning' OR LogMessageType ='Error')"
                , "CK_PipeineTaskInstanceModuleLog_LogMessageType");

            this.CreateCheckConstraint("Parameter"
                , "(SequencePosition > 0)"
                , "CK_Parameter_SequencePosition");

            this.CreateCheckConstraint("PipelineTask"
                , "(SequencePosition > 0)"
                , "CK_PipelineTask_SequencePosition");
        }
        
        public override void Down()
        {
            this.DropConstraint("Parameter", "CK_Parameter_Direction");
            this.DropConstraint("PipelineInstance", "CK_PipelineInstance_Status");
            this.DropConstraint("PipelineTaskInstance", "CK_PipelineTaskInstance_Status");
            this.DropConstraint("PipeineTaskInstanceModuleLog", "CK_PipeineTaskInstanceModuleLog_LogMessageType");
            this.DropConstraint("Parameter", "CK_Parameter_SequencePosition");
            this.DropConstraint("PipelineTask", "CK_PipelineTask_SequencePosition");
        }
    }
}
