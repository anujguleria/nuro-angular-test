namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineTask_ModuleLogs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PipeineTaskInstanceModuleLog",
                c => new
                    {
                        PipeineTaskInstanceModuleLogID = c.Int(nullable: false, identity: true),
                        LogDate = c.DateTime(nullable: false),
                        LogMessageType = c.String(nullable: false, maxLength: 13, unicode: false),
                        LogMessage = c.String(nullable: false, maxLength: 4000),
                    PipelineTaskInstanceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PipeineTaskInstanceModuleLogID)
                .ForeignKey("dbo.PipelineTaskInstance", t => t.PipelineTaskInstanceId, cascadeDelete: true)
                .Index(t => t.PipelineTaskInstanceId);
            
            CreateStoredProcedure(
                "dbo.PipeineTaskInstanceModuleLog_Insert",
                p => new
                    {
                        LogDate = p.DateTime(),
                        LogMessageType = p.String(maxLength: 13, unicode: false),
                        LogMessage = p.String(maxLength: 4000),
                        PipelineTaskInstanceId = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PipeineTaskInstanceModuleLog]([LogDate], [LogMessageType], [LogMessage], [PipelineTaskInstanceId])
                      VALUES (@LogDate, @LogMessageType, @LogMessage, @PipelineTaskInstanceId)
                      
                      DECLARE @PipeineTaskInstanceModuleLogID int
                      SELECT @PipeineTaskInstanceModuleLogID = [PipeineTaskInstanceModuleLogID]
                      FROM [dbo].[PipeineTaskInstanceModuleLog]
                      WHERE @@ROWCOUNT > 0 AND [PipeineTaskInstanceModuleLogID] = scope_identity()
                      
                      SELECT t0.[PipeineTaskInstanceModuleLogID]
                      FROM [dbo].[PipeineTaskInstanceModuleLog] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeineTaskInstanceModuleLogID] = @PipeineTaskInstanceModuleLogID"
            );
            
            CreateStoredProcedure(
                "dbo.PipeineTaskInstanceModuleLog_Update",
                p => new
                    {
                        PipeineTaskInstanceModuleLogID = p.Int(),
                        LogDate = p.DateTime(),
                        LogMessageType = p.String(maxLength: 13, unicode: false),
                        LogMessage = p.String(maxLength: 4000),
                        PipelineTaskInstanceId = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PipeineTaskInstanceModuleLog]
                      SET [LogDate] = @LogDate, [LogMessageType] = @LogMessageType, [LogMessage] = @LogMessage, [PipelineTaskInstanceId] = @PipelineTaskInstanceId
                      WHERE ([PipeineTaskInstanceModuleLogID] = @PipeineTaskInstanceModuleLogID)"
            );
            
            CreateStoredProcedure(
                "dbo.PipeineTaskInstanceModuleLog_Delete",
                p => new
                    {
                        PipeineTaskInstanceModuleLogID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PipeineTaskInstanceModuleLog]
                      WHERE ([PipeineTaskInstanceModuleLogID] = @PipeineTaskInstanceModuleLogID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.PipeineTaskInstanceModuleLog_Delete");
            DropStoredProcedure("dbo.PipeineTaskInstanceModuleLog_Update");
            DropStoredProcedure("dbo.PipeineTaskInstanceModuleLog_Insert");
            DropForeignKey("dbo.PipeineTaskInstanceModuleLog", "PipelineTaskInstanceId", "dbo.PipelineTaskInstance");
            DropIndex("dbo.PipeineTaskInstanceModuleLog", new[] { "PipelineTaskInstanceId" });
            DropTable("dbo.PipeineTaskInstanceModuleLog");
        }
    }
}
