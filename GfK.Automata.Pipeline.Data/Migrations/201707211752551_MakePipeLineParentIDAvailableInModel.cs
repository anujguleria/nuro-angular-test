namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakePipeLineParentIDAvailableInModel : DbMigration
    {
        public override void Up()
        {
            // Added the computed column to the Model setting is database generated type to Computed.  The computed column was
            // already added via a prior Migration and consequently commented the AddColumn.

            //AddColumn("dbo.Pipeline", "ParentToPipelineID", c => c.Int(nullable: false));


            // The SPs were modified to return the ParentToPipelineID.  That is probably not harmful but misleading but because
            // (1) get runtime error if remove select statements, (2) don't want to modify the auto-generated SPs and (3) the
            // API does return the pipelineID leaving them in.

            AlterStoredProcedure(
                "dbo.Pipeline_Insert",
                p => new
                {
                    PipeLineID = p.Int(),
                    Name = p.String(maxLength: 200),
                    ProjectID = p.Int(),
                    ParentID = p.Int(),
                },
                body:
                    @"INSERT [dbo].[Pipeline]([PipeLineID], [Name], [ProjectID], [ParentID])
                      VALUES (@PipeLineID, @Name, @ProjectID, @ParentID)

                      SELECT t0.[ParentToPipelineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );

            AlterStoredProcedure(
                "dbo.Pipeline_Update",
                p => new
                {
                    PipeLineID = p.Int(),
                    Name = p.String(maxLength: 200),
                    ProjectID = p.Int(),
                    ParentID = p.Int(),
                },
                body:
                    @"UPDATE [dbo].[Pipeline]
                      SET [Name] = @Name, [ProjectID] = @ProjectID, [ParentID] = @ParentID
                      WHERE ([PipeLineID] = @PipeLineID)

                      SELECT t0.[ParentToPipelineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
        }

        public override void Down()
        {
            DropColumn("dbo.Pipeline", "ParentToPipelineID");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
