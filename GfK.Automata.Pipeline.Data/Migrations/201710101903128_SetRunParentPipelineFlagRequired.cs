namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetRunParentPipelineFlagRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PipelineRunRequest", "RunParentPipeline", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PipelineRunRequest", "RunParentPipeline", c => c.Boolean());
        }
    }
}
