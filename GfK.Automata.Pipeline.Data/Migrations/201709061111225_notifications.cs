namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class notifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notification",
                c => new
                    {
                        NotificationID = c.Int(nullable: false, identity: true),
                        PipelineID = c.Int(nullable: false),
                        Message = c.String(),
                        UserID = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.NotificationID)
                .ForeignKey("dbo.Pipeline", t => t.PipelineID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.PipelineID)
                .Index(t => t.UserID);
            
            CreateStoredProcedure(
                "dbo.Notification_Insert",
                p => new
                    {
                        PipelineID = p.Int(),
                        Message = p.String(),
                        UserID = p.Int(),
                        Timestamp = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[Notification]([PipelineID], [Message], [UserID], [Timestamp])
                      VALUES (@PipelineID, @Message, @UserID, @Timestamp)
                      
                      DECLARE @NotificationID int
                      SELECT @NotificationID = [NotificationID]
                      FROM [dbo].[Notification]
                      WHERE @@ROWCOUNT > 0 AND [NotificationID] = scope_identity()
                      
                      SELECT t0.[NotificationID]
                      FROM [dbo].[Notification] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[NotificationID] = @NotificationID"
            );
            
            CreateStoredProcedure(
                "dbo.Notification_Update",
                p => new
                    {
                        NotificationID = p.Int(),
                        PipelineID = p.Int(),
                        Message = p.String(),
                        UserID = p.Int(),
                        Timestamp = p.DateTime(),
                    },
                body:
                    @"UPDATE [dbo].[Notification]
                      SET [PipelineID] = @PipelineID, [Message] = @Message, [UserID] = @UserID, [Timestamp] = @Timestamp
                      WHERE ([NotificationID] = @NotificationID)"
            );
            
            CreateStoredProcedure(
                "dbo.Notification_Delete",
                p => new
                    {
                        NotificationID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Notification]
                      WHERE ([NotificationID] = @NotificationID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Notification_Delete");
            DropStoredProcedure("dbo.Notification_Update");
            DropStoredProcedure("dbo.Notification_Insert");
            DropForeignKey("dbo.Notification", "UserID", "dbo.User");
            DropForeignKey("dbo.Notification", "PipelineID", "dbo.Pipeline");
            DropIndex("dbo.Notification", new[] { "UserID" });
            DropIndex("dbo.Notification", new[] { "PipelineID" });
            DropTable("dbo.Notification");
        }
    }
}
