namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPipelineTask_Indexes : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.PipelineTaskInputValue", 
                new[] { "PipelineTaskID", "ParameterID" }, unique: true, name: "Unq_PipelineTaskInputValue_PipelineTaskID_ParameterID");

            CreateIndex("dbo.UserInputValue", 
                new[] { "PipelineTaskID", "ParameterID" }, unique: true, name: "Unq_UserInputValue_PipelineTaskID_ParameterID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PipelineTaskInputValue", "Unq_PipelineTaskInputValue_PipelineTaskID_ParameterID");
            DropIndex("dbo.UserInputValue", "Unq_UserInputValue_PipelineTaskID_ParameterID");
        }
    }
}
