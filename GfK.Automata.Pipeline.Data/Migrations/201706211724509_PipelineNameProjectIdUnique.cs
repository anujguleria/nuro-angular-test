namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineNameProjectIdUnique : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Pipeline", new[] { "ProjectID" });
            CreateIndex("dbo.Pipeline", new[] { "ProjectID", "Name" }, unique: true, name: "Unq_Pipeline_ProjectId_Name");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Pipeline", "Unq_Pipeline_ProjectId_Name");
            CreateIndex("dbo.Pipeline", "ProjectID");
        }
    }
}
