namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateUserAccessViews : DbMigration
    {
        public override void Up()
        {
            Sql("CREATE  VIEW [dbo].[GapGroupUserProject]  AS " +
                "  SELECT gu.UserID, pr.ProjectID, pr.GapGroupID " +
                "    FROM dbo.Project AS pr" +
                "    JOIN dbo.GapGroupUser AS gu " +
                "      ON gu.GapGroupID = pr.GapGroupID ");

            Sql("CREATE  VIEW [dbo].[GapGroupUserPipeline]  AS " +
                "  SELECT gu.UserID, pr.ProjectID, pip.PipeLineID " +
                "    FROM dbo.Pipeline AS pip" +
                "    JOIN dbo.Project AS pr " +
                "      ON pr.ProjectID = pip.ProjectID " +
                "    JOIN dbo.GapGroupUser AS gu" +
                "      ON gu.GapGroupID = pr.GapGroupID");

        }
        
        public override void Down()
        {
            Sql("DROP VIEW [dbo].[GapGroupUserProject] ");
            Sql("DROP VIEW [dbo].[GapGroupUserPipeline] ");
        }
    }
}
