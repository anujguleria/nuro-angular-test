namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixUnqPipelineTaskNameIdx : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PipelineTask", "Unq_PipelineTask_PipeLineID_Name");
            CreateIndex("dbo.PipelineTask", new string[] { "PipeLineID", "Name" }, true);
        }
        
        public override void Down()
        {
 
        }
    }
}
