namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatePipelineNameUniqueIndex : DbMigration
    {
        public override void Up()
        {
            string[] columnList = new string[] { "ParentToPipelineID", "Name" };
            CreateIndex("dbo.Pipeline", columnList, true, "Unq_Pipeline_ParentToPipelineID_Name");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Pipeline", "Unq_Pipeline_ParentToPipelineID_Name");
        }
    }
}
