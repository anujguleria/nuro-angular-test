namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetProjectsToDefaultGroup : DbMigration
    {
        public override void Up()
        {
            this.Sql("UPDATE dbo.Project SET GapGroupID = 1 WHERE GapGroupID IS NULL");

            AlterColumn("dbo.Project", "GapGroupID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Project", "GapGroupID", c => c.Int());
        }
    }
}
