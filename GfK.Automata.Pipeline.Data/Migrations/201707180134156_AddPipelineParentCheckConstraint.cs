namespace GfK.Automata.Pipeline.Data.Migrations
{
    using GfK.Automata.Pipeline.Data.Migrations.Operations;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPipelineParentCheckConstraint : DbMigration
    {
        public override void Up()
        {
            this.CreateCheckConstraint("Pipeline"
                ,"((CASE WHEN [ParentID] IS NOT NULL AND [ProjectID] IS NULL OR [ParentID] IS NULL AND [ProjectID] IS NOT NULL THEN (1) ELSE (0) END=(1)))"
                , "CK_Pipeline");
        }
        
        public override void Down()
        {
            this.DropConstraint("Pipeline", "CK_Pipeline");
        }
    }
}
