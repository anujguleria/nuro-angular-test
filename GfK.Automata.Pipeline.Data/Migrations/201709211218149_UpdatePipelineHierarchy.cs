namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePipelineHierarchy : DbMigration
    {
        public override void Up()
        {
            // Altered PipelineHierarchy to allow null in Schedule generating this migration.
        }
        
        public override void Down()
        {
            
        }
    }
}
