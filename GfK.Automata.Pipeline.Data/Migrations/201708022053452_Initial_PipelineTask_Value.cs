namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_PipelineTask_Value : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Value",
                c => new
                    {
                        ValueID = c.Int(nullable: false, identity: true),
                        InputValue = c.String(),
                        InputPriorTaskValueID = c.Int(),
                        ParameterID = c.Int(nullable: false),
                        PipelineTaskID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ValueID)
                .ForeignKey("dbo.Parameter", t => t.ParameterID)
                .ForeignKey("dbo.PipelineTask", t => t.PipelineTaskID, cascadeDelete: true)
                .Index(t => t.ParameterID)
                .Index(t => t.PipelineTaskID);
            
            CreateTable(
                "dbo.PipelineTask",
                c => new
                    {
                        PipelineTaskID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        SequencePosition = c.Int(nullable: false),
                        PipeLineID = c.Int(nullable: false),
                        ModuleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PipelineTaskID)
                .ForeignKey("dbo.Pipeline", t => t.PipeLineID, cascadeDelete: true)
                .ForeignKey("dbo.Module", t => t.ModuleID)
                .Index(t => new { t.PipeLineID, t.Name }, unique: true, name: "Unq_PipelineTask_PipeLineID_Name")
                .Index(t => t.ModuleID);
            
            CreateStoredProcedure(
                "dbo.Value_Insert",
                p => new
                    {
                        InputValue = p.String(),
                        InputPriorTaskValueID = p.Int(),
                        ParameterID = p.Int(),
                        PipelineTaskID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Value]([InputValue], [InputPriorTaskValueID], [ParameterID], [PipelineTaskID])
                      VALUES (@InputValue, @InputPriorTaskValueID, @ParameterID, @PipelineTaskID)
                      
                      DECLARE @ValueID int
                      SELECT @ValueID = [ValueID]
                      FROM [dbo].[Value]
                      WHERE @@ROWCOUNT > 0 AND [ValueID] = scope_identity()
                      
                      SELECT t0.[ValueID]
                      FROM [dbo].[Value] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ValueID] = @ValueID"
            );
            
            CreateStoredProcedure(
                "dbo.Value_Update",
                p => new
                    {
                        ValueID = p.Int(),
                        InputValue = p.String(),
                        InputPriorTaskValueID = p.Int(),
                        ParameterID = p.Int(),
                        PipelineTaskID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Value]
                      SET [InputValue] = @InputValue, [InputPriorTaskValueID] = @InputPriorTaskValueID, [ParameterID] = @ParameterID, [PipelineTaskID] = @PipelineTaskID
                      WHERE ([ValueID] = @ValueID)"
            );
            
            CreateStoredProcedure(
                "dbo.Value_Delete",
                p => new
                    {
                        ValueID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Value]
                      WHERE ([ValueID] = @ValueID)"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTask_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100),
                        SequencePosition = p.Int(),
                        PipeLineID = p.Int(),
                        ModuleID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PipelineTask]([Name], [SequencePosition], [PipeLineID], [ModuleID])
                      VALUES (@Name, @SequencePosition, @PipeLineID, @ModuleID)
                      
                      DECLARE @PipelineTaskID int
                      SELECT @PipelineTaskID = [PipelineTaskID]
                      FROM [dbo].[PipelineTask]
                      WHERE @@ROWCOUNT > 0 AND [PipelineTaskID] = scope_identity()
                      
                      SELECT t0.[PipelineTaskID]
                      FROM [dbo].[PipelineTask] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineTaskID] = @PipelineTaskID"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTask_Update",
                p => new
                    {
                        PipelineTaskID = p.Int(),
                        Name = p.String(maxLength: 100),
                        SequencePosition = p.Int(),
                        PipeLineID = p.Int(),
                        ModuleID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineTask]
                      SET [Name] = @Name, [SequencePosition] = @SequencePosition, [PipeLineID] = @PipeLineID, [ModuleID] = @ModuleID
                      WHERE ([PipelineTaskID] = @PipelineTaskID)"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTask_Delete",
                p => new
                    {
                        PipelineTaskID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PipelineTask]
                      WHERE ([PipelineTaskID] = @PipelineTaskID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.PipelineTask_Delete");
            DropStoredProcedure("dbo.PipelineTask_Update");
            DropStoredProcedure("dbo.PipelineTask_Insert");
            DropStoredProcedure("dbo.Value_Delete");
            DropStoredProcedure("dbo.Value_Update");
            DropStoredProcedure("dbo.Value_Insert");
            DropForeignKey("dbo.PipelineTask", "ModuleID", "dbo.Module");
            DropForeignKey("dbo.Value", "PipelineTaskID", "dbo.PipelineTask");
            DropForeignKey("dbo.PipelineTask", "PipeLineID", "dbo.Pipeline");
            DropForeignKey("dbo.Value", "ParameterID", "dbo.Parameter");
            DropIndex("dbo.PipelineTask", new[] { "ModuleID" });
            DropIndex("dbo.PipelineTask", "Unq_PipelineTask_PipeLineID_Name");
            DropIndex("dbo.Value", new[] { "PipelineTaskID" });
            DropIndex("dbo.Value", new[] { "ParameterID" });
            DropTable("dbo.PipelineTask");
            DropTable("dbo.Value");
        }
    }
}
