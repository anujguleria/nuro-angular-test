namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_User_GapGroup_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserGapGroup",
                c => new
                    {
                        User_UserID = c.Int(nullable: false),
                        GapGroup_GapGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserID, t.GapGroup_GapGroupID })
                .ForeignKey("dbo.User", t => t.User_UserID, cascadeDelete: true)
                .ForeignKey("dbo.GapGroup", t => t.GapGroup_GapGroupID, cascadeDelete: true)
                .Index(t => t.User_UserID)
                .Index(t => t.GapGroup_GapGroupID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserGapGroup", "GapGroup_GapGroupID", "dbo.GapGroup");
            DropForeignKey("dbo.UserGapGroup", "User_UserID", "dbo.User");
            DropIndex("dbo.UserGapGroup", new[] { "GapGroup_GapGroupID" });
            DropIndex("dbo.UserGapGroup", new[] { "User_UserID" });
            DropTable("dbo.UserGapGroup");
        }
    }
}
