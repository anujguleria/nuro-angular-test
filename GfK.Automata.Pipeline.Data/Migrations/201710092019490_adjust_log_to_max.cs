namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adjust_log_to_max : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE PipelineTaskInstanceModuleLog ALTER COLUMN LogMessage NVARCHAR(MAX) NOT NULL");
        }
        
        public override void Down()
        {
        }
    }
}
