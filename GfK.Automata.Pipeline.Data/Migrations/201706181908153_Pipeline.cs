namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Pipeline : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pipeline",
                c => new
                    {
                        PipeLineID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        ProjectID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PipeLineID)
                .ForeignKey("dbo.Project", t => t.ProjectID, cascadeDelete: true)
                .Index(t => t.ProjectID);
            
            CreateStoredProcedure(
                "dbo.Pipeline_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Pipeline]([Name], [ProjectID])
                      VALUES (@Name, @ProjectID)
                      
                      DECLARE @PipeLineID int
                      SELECT @PipeLineID = [PipeLineID]
                      FROM [dbo].[Pipeline]
                      WHERE @@ROWCOUNT > 0 AND [PipeLineID] = scope_identity()
                      
                      SELECT t0.[PipeLineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
            
            CreateStoredProcedure(
                "dbo.Pipeline_Update",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Pipeline]
                      SET [Name] = @Name, [ProjectID] = @ProjectID
                      WHERE ([PipeLineID] = @PipeLineID)"
            );
            
            CreateStoredProcedure(
                "dbo.Pipeline_Delete",
                p => new
                    {
                        PipeLineID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Pipeline]
                      WHERE ([PipeLineID] = @PipeLineID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Pipeline_Delete");
            DropStoredProcedure("dbo.Pipeline_Update");
            DropStoredProcedure("dbo.Pipeline_Insert");
            DropForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project");
            DropIndex("dbo.Pipeline", new[] { "ProjectID" });
            DropTable("dbo.Pipeline");
        }
    }
}
