namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_overwrite_field : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                                    IF NOT EXISTS (SELECT * FROM Parameter INNER JOIN Module ON Parameter.ModuleID = Module.ModuleID WHERE Module.Name = 'FileOutputModule'
                                        AND Parameter.Name = 'Overwrite')
                                    BEGIN
                                        INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                        SELECT 'Overwrite', 'if true, will overwrite the file at destination if it exists', 'IN', 4, ParameterType.ParameterTypeID, ModuleID
                                        FROM ParameterType, Module
                                        WHERE ParameterType.Name = 'String' AND Module.Name = 'FileOutputModule';
                                    END"
                );
        }
        
        public override void Down()
        {
        }
    }
}
