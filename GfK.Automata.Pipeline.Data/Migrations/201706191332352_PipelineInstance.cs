namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineInstance : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PipelineInstance",
                c => new
                    {
                        PipelineInstanceId = c.Int(nullable: false, identity: true),
                        Started = c.DateTime(nullable: false),
                        Ended = c.DateTime(),
                        PipelineId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PipelineInstanceId)
                .ForeignKey("dbo.Pipeline", t => t.PipelineId, cascadeDelete: true)
                .Index(t => t.PipelineId);
            
            CreateStoredProcedure(
                "dbo.PipelineInstance_Insert",
                p => new
                    {
                        Started = p.DateTime(),
                        Ended = p.DateTime(),
                        PipelineId = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PipelineInstance]([Started], [Ended], [PipelineId])
                      VALUES (@Started, @Ended, @PipelineId)
                      
                      DECLARE @PipelineInstanceId int
                      SELECT @PipelineInstanceId = [PipelineInstanceId]
                      FROM [dbo].[PipelineInstance]
                      WHERE @@ROWCOUNT > 0 AND [PipelineInstanceId] = scope_identity()
                      
                      SELECT t0.[PipelineInstanceId]
                      FROM [dbo].[PipelineInstance] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineInstanceId] = @PipelineInstanceId"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineInstance_Update",
                p => new
                    {
                        PipelineInstanceId = p.Int(),
                        Started = p.DateTime(),
                        Ended = p.DateTime(),
                        PipelineId = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineInstance]
                      SET [Started] = @Started, [Ended] = @Ended, [PipelineId] = @PipelineId
                      WHERE ([PipelineInstanceId] = @PipelineInstanceId)"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineInstance_Delete",
                p => new
                    {
                        PipelineInstanceId = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PipelineInstance]
                      WHERE ([PipelineInstanceId] = @PipelineInstanceId)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.PipelineInstance_Delete");
            DropStoredProcedure("dbo.PipelineInstance_Update");
            DropStoredProcedure("dbo.PipelineInstance_Insert");
            DropForeignKey("dbo.PipelineInstance", "PipelineId", "dbo.Pipeline");
            DropIndex("dbo.PipelineInstance", new[] { "PipelineId" });
            DropTable("dbo.PipelineInstance");
        }
    }
}
