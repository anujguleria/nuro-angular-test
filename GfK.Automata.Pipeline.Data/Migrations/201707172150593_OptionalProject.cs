namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OptionalProject : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project");
            DropIndex("dbo.Pipeline", "Unq_Pipeline_ProjectId_Name");
            AlterColumn("dbo.Pipeline", "ProjectID", c => c.Int());
            CreateIndex("dbo.Pipeline", new[] { "ProjectID", "Name" }, unique: true, name: "Unq_Pipeline_ProjectId_Name");
            AddForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project", "ProjectID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project");
            DropIndex("dbo.Pipeline", "Unq_Pipeline_ProjectId_Name");
            AlterColumn("dbo.Pipeline", "ProjectID", c => c.Int(nullable: false));
            CreateIndex("dbo.Pipeline", new[] { "ProjectID", "Name" }, unique: true, name: "Unq_Pipeline_ProjectId_Name");
            AddForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project", "ProjectID", cascadeDelete: true);
        }
    }
}
