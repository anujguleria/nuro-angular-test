namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineTaskInstances : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PipelineTaskInstance",
                c => new
                    {
                        PipelineTaskInstanceId = c.Int(nullable: false, identity: true),
                        PipelineTaskID = c.Int(nullable: false),
                        PipelineInstanceId = c.Int(nullable: false),
                        Started = c.DateTime(nullable: false),
                        Ended = c.DateTime(),
                        Status = c.String(nullable: false, maxLength: 9),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.PipelineTaskInstanceId)
                .ForeignKey("dbo.PipelineInstance", t => t.PipelineInstanceId)
                .ForeignKey("dbo.PipelineTask", t => t.PipelineTaskID, cascadeDelete: true)
                .Index(t => t.PipelineTaskID)
                .Index(t => t.PipelineInstanceId);
            
            CreateStoredProcedure(
                "dbo.PipelineTaskInstance_Insert",
                p => new
                    {
                        PipelineTaskID = p.Int(),
                        PipelineInstanceId = p.Int(),
                        Started = p.DateTime(),
                        Ended = p.DateTime(),
                        Status = p.String(maxLength: 9),
                        Message = p.String(),
                    },
                body:
                    @"INSERT [dbo].[PipelineTaskInstance]([PipelineTaskID], [PipelineInstanceId], [Started], [Ended], [Status], [Message])
                      VALUES (@PipelineTaskID, @PipelineInstanceId, @Started, @Ended, @Status, @Message)
                      
                      DECLARE @PipelineTaskInstanceId int
                      SELECT @PipelineTaskInstanceId = [PipelineTaskInstanceId]
                      FROM [dbo].[PipelineTaskInstance]
                      WHERE @@ROWCOUNT > 0 AND [PipelineTaskInstanceId] = scope_identity()
                      
                      SELECT t0.[PipelineTaskInstanceId]
                      FROM [dbo].[PipelineTaskInstance] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineTaskInstanceId] = @PipelineTaskInstanceId"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTaskInstance_Update",
                p => new
                    {
                        PipelineTaskInstanceId = p.Int(),
                        PipelineTaskID = p.Int(),
                        PipelineInstanceId = p.Int(),
                        Started = p.DateTime(),
                        Ended = p.DateTime(),
                        Status = p.String(maxLength: 9),
                        Message = p.String(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineTaskInstance]
                      SET [PipelineTaskID] = @PipelineTaskID, [PipelineInstanceId] = @PipelineInstanceId, [Started] = @Started, [Ended] = @Ended, [Status] = @Status, [Message] = @Message
                      WHERE ([PipelineTaskInstanceId] = @PipelineTaskInstanceId)"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTaskInstance_Delete",
                p => new
                    {
                        PipelineTaskInstanceId = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PipelineTaskInstance]
                      WHERE ([PipelineTaskInstanceId] = @PipelineTaskInstanceId)"
            );
            
            AlterStoredProcedure(
                "dbo.PipelineInstance_Insert",
                p => new
                    {
                        PipelineId = p.Int(),
                        Started = p.DateTime(),
                        Ended = p.DateTime(),
                        Status = p.String(maxLength: 9),
                        Message = p.String(),
                    },
                body:
                    @"INSERT [dbo].[PipelineInstance]([PipelineId], [Started], [Ended], [Status], [Message])
                      VALUES (@PipelineId, @Started, @Ended, @Status, @Message)
                      
                      DECLARE @PipelineInstanceId int
                      SELECT @PipelineInstanceId = [PipelineInstanceId]
                      FROM [dbo].[PipelineInstance]
                      WHERE @@ROWCOUNT > 0 AND [PipelineInstanceId] = scope_identity()
                      
                      SELECT t0.[PipelineInstanceId]
                      FROM [dbo].[PipelineInstance] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineInstanceId] = @PipelineInstanceId"
            );
            
            AlterStoredProcedure(
                "dbo.PipelineInstance_Update",
                p => new
                    {
                        PipelineInstanceId = p.Int(),
                        PipelineId = p.Int(),
                        Started = p.DateTime(),
                        Ended = p.DateTime(),
                        Status = p.String(maxLength: 9),
                        Message = p.String(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineInstance]
                      SET [PipelineId] = @PipelineId, [Started] = @Started, [Ended] = @Ended, [Status] = @Status, [Message] = @Message
                      WHERE ([PipelineInstanceId] = @PipelineInstanceId)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.PipelineTaskInstance_Delete");
            DropStoredProcedure("dbo.PipelineTaskInstance_Update");
            DropStoredProcedure("dbo.PipelineTaskInstance_Insert");
            DropForeignKey("dbo.PipelineTaskInstance", "PipelineTaskID", "dbo.PipelineTask");
            DropForeignKey("dbo.PipelineTaskInstance", "PipelineInstanceId", "dbo.PipelineInstance");
            DropIndex("dbo.PipelineTaskInstance", new[] { "PipelineInstanceId" });
            DropIndex("dbo.PipelineTaskInstance", new[] { "PipelineTaskID" });
            DropTable("dbo.PipelineTaskInstance");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
