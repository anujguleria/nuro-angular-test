namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrectParameterPriorPipeline : DbMigration
    {
        public override void Up()
        {
            // This is un-necessary.  Suspect that in migration where this handled had hand-code to handle adding the new column
            // initializing and then setting it to non-null

            //DropIndex("dbo.Parameter", "Unq_Parameter_ModuleID_Name");
            //AddColumn("dbo.PipelineTaskInputValue", "PriorPipelineID", c => c.Int(nullable: false));
            //AlterColumn("dbo.Parameter", "Name", c => c.String(nullable: false, maxLength: 100));
            //CreateIndex("dbo.Parameter", new[] { "ModuleID", "Name" }, unique: true, name: "Unq_Parameter_ModuleID_Name");
            //AlterStoredProcedure(
            //    "dbo.PipelineTaskInputValue_Insert",
            //    p => new
            //        {
            //            PriorPipelineTaskID = p.Int(),
            //            PriorPipeLineTaskParameterID = p.Int(),
            //            ParameterID = p.Int(),
            //            PipelineTaskID = p.Int(),
            //            PipelineID = p.Int(),
            //            PriorPipelineID = p.Int(),
            //        },
            //    body:
            //        @"INSERT [dbo].[PipelineTaskInputValue]([PriorPipelineTaskID], [PriorPipeLineTaskParameterID], [ParameterID], [PipelineTaskID], [PipelineID], [PriorPipelineID])
            //          VALUES (@PriorPipelineTaskID, @PriorPipeLineTaskParameterID, @ParameterID, @PipelineTaskID, @PipelineID, @PriorPipelineID)
                      
            //          DECLARE @PipelineTaskInputValueID int
            //          SELECT @PipelineTaskInputValueID = [PipelineTaskInputValueID]
            //          FROM [dbo].[PipelineTaskInputValue]
            //          WHERE @@ROWCOUNT > 0 AND [PipelineTaskInputValueID] = scope_identity()
                      
            //          SELECT t0.[PipelineTaskInputValueID]
            //          FROM [dbo].[PipelineTaskInputValue] AS t0
            //          WHERE @@ROWCOUNT > 0 AND t0.[PipelineTaskInputValueID] = @PipelineTaskInputValueID"
            //);
            
            //AlterStoredProcedure(
            //    "dbo.PipelineTaskInputValue_Update",
            //    p => new
            //        {
            //            PipelineTaskInputValueID = p.Int(),
            //            PriorPipelineTaskID = p.Int(),
            //            PriorPipeLineTaskParameterID = p.Int(),
            //            ParameterID = p.Int(),
            //            PipelineTaskID = p.Int(),
            //            PipelineID = p.Int(),
            //            PriorPipelineID = p.Int(),
            //        },
            //    body:
            //        @"UPDATE [dbo].[PipelineTaskInputValue]
            //          SET [PriorPipelineTaskID] = @PriorPipelineTaskID, [PriorPipeLineTaskParameterID] = @PriorPipeLineTaskParameterID, [ParameterID] = @ParameterID, [PipelineTaskID] = @PipelineTaskID, [PipelineID] = @PipelineID, [PriorPipelineID] = @PriorPipelineID
            //          WHERE ([PipelineTaskInputValueID] = @PipelineTaskInputValueID)"
            //);
            
        }
        
        public override void Down()
        {
            //DropIndex("dbo.Parameter", "Unq_Parameter_ModuleID_Name");
            //AlterColumn("dbo.Parameter", "Name", c => c.String(maxLength: 100));
            //DropColumn("dbo.PipelineTaskInputValue", "PriorPipelineID");
            //CreateIndex("dbo.Parameter", new[] { "ModuleID", "Name" }, unique: true, name: "Unq_Parameter_ModuleID_Name");
            //throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
