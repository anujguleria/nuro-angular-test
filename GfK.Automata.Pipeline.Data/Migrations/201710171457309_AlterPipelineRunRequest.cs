namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class AlterPipelineRunRequest : DbMigration
    {
        public override void Up()
        {
            Sql("TRUNCATE TABLE dbo.PipelineRunRequest");

            AddColumn("dbo.PipelineRunRequest", "RunPipelineContext", c => c.String(nullable: false, maxLength: 8, unicode: false, defaultValue: "Parent"));

            this.DropConstraint("dbo.PipelineRunRequest", "DF_PipelineRunRequest_RunParentPipeline");
            DropColumn("dbo.PipelineRunRequest", "RunParentPipeline");

            AlterStoredProcedure(
                "dbo.PipelineRunRequest_Insert",
                p => new
                    {
                        PipelineID = p.Int(),
                        RunPipelineContext = p.String(maxLength: 8, unicode: false),
                        RunRequestDate = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[PipelineRunRequest]([PipelineID], [RunPipelineContext], [RunRequestDate])
                      VALUES (@PipelineID, @RunPipelineContext, @RunRequestDate)
                      
                      DECLARE @PipelineRunRequestID int
                      SELECT @PipelineRunRequestID = [PipelineRunRequestID]
                      FROM [dbo].[PipelineRunRequest]
                      WHERE @@ROWCOUNT > 0 AND [PipelineRunRequestID] = scope_identity()
                      
                      SELECT t0.[PipelineRunRequestID]
                      FROM [dbo].[PipelineRunRequest] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineRunRequestID] = @PipelineRunRequestID"
            );
            
            AlterStoredProcedure(
                "dbo.PipelineRunRequest_Update",
                p => new
                    {
                        PipelineRunRequestID = p.Int(),
                        PipelineID = p.Int(),
                        RunPipelineContext = p.String(maxLength: 8, unicode: false),
                        RunRequestDate = p.DateTime(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineRunRequest]
                      SET [PipelineID] = @PipelineID, [RunPipelineContext] = @RunPipelineContext, [RunRequestDate] = @RunRequestDate
                      WHERE ([PipelineRunRequestID] = @PipelineRunRequestID)"
            );

            this.CreateCheckConstraint("dbo.PipelineRunRequest"
                , "(RunPipelineContext ='None' OR RunPipelineContext ='Parent' OR RunPipelineContext ='Children')"
                , "CK_PipelineRunRequest_RunPipelineContext");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PipelineRunRequest", "RunParentPipeline", c => c.Boolean(nullable: false));

            this.DropConstraint("dbo.PipelineRunRequest", "CK_PipelineRunRequest_RunPipelineContext");
            this.DropConstraint("dbo.PipelineRunRequest", "DF_PipelineRunRequest_RunContext");
            DropColumn("dbo.PipelineRunRequest", "RunPipelineContext");

            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
