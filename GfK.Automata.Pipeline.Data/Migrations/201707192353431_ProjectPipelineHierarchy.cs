namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectPipelineHierarchy : DbMigration
    {
        public override void Up()
        {
            Sql("CREATE  VIEW [dbo].[ProjectPipelineHierarchy]  AS " +
                "  WITH projectPipelines AS " +
                "(" +
                "   SELECT projRoot.PipeLineID, projRoot.Name, projRoot.ProjectID, projRoot.ParentID " +
                "     FROM dbo.Pipeline AS projRoot " +
                "    WHERE projRoot.ProjectID IS NOT NULL " +
                "   UNION ALL " +
                " SELECT pipelines.PipeLineID, pipelines.Name, projectPipelines.ProjectID, pipelines.ParentID " +
                "   FROM dbo.Pipeline AS pipelines " +
                "   JOIN projectPipelines " +
                "     ON projectPipelines.PipeLineID = pipelines.ParentID " +
                "  WHERE pipelines.ParentID IS NOT NULL " +
                ") " +
                "SELECT PipeLineID, Name, ProjectID, ParentID " +
                "  FROM projectPipelines ");                        
        }
        
        public override void Down()
        {
            Sql("DROP VIEW [dbo].[ProjectPipelineHierarchy] ");
        }
    }
}
