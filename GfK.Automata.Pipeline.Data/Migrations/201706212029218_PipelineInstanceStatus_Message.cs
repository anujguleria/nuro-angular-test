namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineInstanceStatus_Message : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PipelineInstance", "Status", c => c.String(nullable: false, maxLength: 9));
            AddColumn("dbo.PipelineInstance", "Message", c => c.String());
            AlterStoredProcedure(
                "dbo.PipelineInstance_Insert",
                p => new
                    {
                        Started = p.DateTime(),
                        Ended = p.DateTime(),
                        Status = p.String(maxLength: 9),
                        Message = p.String(),
                        PipelineId = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PipelineInstance]([Started], [Ended], [Status], [Message], [PipelineId])
                      VALUES (@Started, @Ended, @Status, @Message, @PipelineId)
                      
                      DECLARE @PipelineInstanceId int
                      SELECT @PipelineInstanceId = [PipelineInstanceId]
                      FROM [dbo].[PipelineInstance]
                      WHERE @@ROWCOUNT > 0 AND [PipelineInstanceId] = scope_identity()
                      
                      SELECT t0.[PipelineInstanceId]
                      FROM [dbo].[PipelineInstance] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineInstanceId] = @PipelineInstanceId"
            );
            
            AlterStoredProcedure(
                "dbo.PipelineInstance_Update",
                p => new
                    {
                        PipelineInstanceId = p.Int(),
                        Started = p.DateTime(),
                        Ended = p.DateTime(),
                        Status = p.String(maxLength: 9),
                        Message = p.String(),
                        PipelineId = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineInstance]
                      SET [Started] = @Started, [Ended] = @Ended, [Status] = @Status, [Message] = @Message, [PipelineId] = @PipelineId
                      WHERE ([PipelineInstanceId] = @PipelineInstanceId)"
            );
            
        }
        
        public override void Down()
        {
            DropColumn("dbo.PipelineInstance", "Message");
            DropColumn("dbo.PipelineInstance", "Status");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
