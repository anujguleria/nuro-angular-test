// <auto-generated />
namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_GapGroupID_Project : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_GapGroupID_Project));
        
        string IMigrationMetadata.Id
        {
            get { return "201708221608098_Add_GapGroupID_Project"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
