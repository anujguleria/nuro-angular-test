namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactorTaskInputFKs : DbMigration
    {
        public override void Up()
        {
            // These cause issues on delete of pipeline.  Cannot change to Cascade because that causes circular issues
            // with other FKs.  
            // TODO - alternative ref integrity to enforce.
            DropForeignKey("dbo.PipelineTaskInputValue", "FK_PipelineTaskInputValue_PipelineTask_PipelineTaskID");
            DropForeignKey("dbo.PipelineTaskInputValue", "FK_PipelineTaskInputValue_PipelineTask_PriorPipelineTaskID");
        }
        
        public override void Down()
        {
        }
    }
}
