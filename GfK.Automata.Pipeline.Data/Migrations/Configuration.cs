namespace GfK.Automata.Pipeline.Data.Migrations
{
    using GfK.Automata.Pipeline.Data.Migrations.Operations;
    using GfK.Automata.Pipeline.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Transactions;

    internal sealed class Configuration : DbMigrationsConfiguration<GfK.Automata.Pipeline.Data.GAPEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;

            SetSqlGenerator(
                        "System.Data.SqlClient",
                        new CustomSqlGenerator()
                    );
        }

        protected override void Seed(GfK.Automata.Pipeline.Data.GAPEntities context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.

            string seedSQL = @" 
                                DECLARE @ModuleID INT;

                                IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'DimensionsModule')
                                BEGIN
                                    INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                    SELECT 'DimensionsModule', 'pull data from Dimensions cluster', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.DimensionsModule'
                                    FROM ModuleGroup
                                    WHERE ModuleGroup.Name = 'Data Sources';

                                    SET @ModuleID = Scope_Identity();

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'SurveyName', 'Survey Name', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'Version', 'Survey Version', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'Cluster', 'Dimensions Cluster Key', 'IN', 3, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'DimensionsDataFile', 'Resulting Data File', 'OUT', 4, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'MDDFile', 'Resulting MDD File', 'OUT', 5, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';
                                END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @"
                            DECLARE @ModuleID INT;

                            IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'FileInputModule')
                            BEGIN
                                INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                SELECT 'FileInputModule', 'brings a file into pipeline env', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.FileInputModule'
                                FROM ModuleGroup
                                WHERE ModuleGroup.Name = 'Data Sources';

                                SET @ModuleID = Scope_Identity();

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'InputFilePath', 'full path of file to pull in', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'OutputFilePath', 'path as it existing within gap for use in other modules', 'OUT', 2, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';
                            END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @"
                            DECLARE @ModuleID INT;

                            IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'FileOutputModule')
                            BEGIN
                                INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                SELECT 'FileOutputModule', 'brings a file into pipeline env', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.FileOutputModule'
                                FROM ModuleGroup
                                WHERE ModuleGroup.Name = 'Transformation';

                                SET @ModuleID = Scope_Identity();

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'InputFilePath', 'path of file within gap', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'OutputPath', 'path to write the file out to', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'OutputFilePath', 'full path of the final file written out', 'OUT', 3, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';
                            END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @"
                            DECLARE @ModuleID INT;

                            IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'DMSModule')
                            BEGIN
                                INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                SELECT 'DMSModule', 'runs a dms script', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.DMSModule'
                                FROM ModuleGroup
                                WHERE ModuleGroup.Name = 'Transformation';

                                SET @ModuleID = Scope_Identity();

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'DMSScript', 'script to be run (is only used if no file specified)', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'Script';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'DMSScriptFile', 'file that contains script to be run (is used if specified)', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument1', 'generic argument that can be passed in', 'IN', 3, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument2', 'generic argument that can be passed in', 'IN', 4, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument3', 'generic argument that can be passed in', 'IN', 5, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument4', 'generic argument that can be passed in', 'IN', 6, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument5', 'generic argument that can be passed in', 'IN', 7, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return1', 'generic return variable that can be used', 'OUT', 8, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return2', 'generic return variable that can be used', 'OUT', 9, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return3', 'generic return variable that can be used', 'OUT', 10, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return4', 'generic return variable that can be used', 'OUT', 11, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return5', 'generic return variable that can be used', 'OUT', 12, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';
                           END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @"
                            DECLARE @ModuleID INT;

                            IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'EXEModule')
                            BEGIN
                                INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                SELECT 'EXEModule', 'runs a command line executable', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.EXEModule'
                                FROM ModuleGroup
                                WHERE ModuleGroup.Name = 'Tools';

                                SET @ModuleID = Scope_Identity();

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Command', 'script to be run (is only used if no file specified)', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'Script';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'ScriptPath', 'file that contains script to be run (is used if specified)', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument1', 'generic argument that can be passed in', 'IN', 3, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument2', 'generic argument that can be passed in', 'IN', 4, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument3', 'generic argument that can be passed in', 'IN', 5, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument4', 'generic argument that can be passed in', 'IN', 6, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument5', 'generic argument that can be passed in', 'IN', 7, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return1', 'generic return variable that can be used', 'OUT', 8, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return2', 'generic return variable that can be used', 'OUT', 9, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';


                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return3', 'generic return variable that can be used', 'OUT', 10, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';


                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return4', 'generic return variable that can be used', 'OUT', 11, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';


                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return5', 'generic return variable that can be used', 'OUT', 12, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';
                            END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @"
                            DECLARE @ModuleID INT;

                            IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'MRSModule')
                            BEGIN
                                INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                SELECT 'MRSModule', 'runs an MRS script', ModuleGroup.ModuleGroupID, 'GfK.Automata.Pipeline.GapModule', 'GfK.Automata.Pipeline.GapModule.Core.MRSModule'
                                FROM ModuleGroup
                                WHERE ModuleGroup.Name = 'Tools';

                                SET @ModuleID = Scope_Identity();

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Script', 'script to be run (is only used if no file specified)', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'Script';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'ScriptPath', 'file that contains script to be run (is used if specified)', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument1', 'generic argument that can be passed in', 'IN', 3, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument2', 'generic argument that can be passed in', 'IN', 4, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument3', 'generic argument that can be passed in', 'IN', 5, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument4', 'generic argument that can be passed in', 'IN', 6, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument5', 'generic argument that can be passed in', 'IN', 7, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return1', 'generic return variable that can be used', 'OUT', 8, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return2', 'generic return variable that can be used', 'OUT', 9, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';


                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return3', 'generic return variable that can be used', 'OUT', 10, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';


                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return4', 'generic return variable that can be used', 'OUT', 11, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';


                                INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Return5', 'generic return variable that can be used', 'OUT', 12, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';
                            END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @" 
                                DECLARE @ModuleID INT;
                                IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'DimTabulationGenerator')
                                BEGIN
                                    INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                    SELECT 'DimTabulationGenerator', 'pull data from Dimensions cluster', ModuleGroup.ModuleGroupID, 'GfK.Automata.Tabulation.Service', 'GfK.Automata.Tabulation.Service.DimTabulationModule'
                                    FROM ModuleGroup
                                    WHERE ModuleGroup.Name = 'Tabulation';

                                    SET @ModuleID = Scope_Identity();

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'MddPath', 'MDD Path', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'DdfPath', 'DDF Path', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'Language', 'Language', 'IN', 3, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'Context', 'Context', 'IN', 4, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'Version', 'Version', 'IN', 5, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'BannersMddPath', 'Banners MDD Path', 'INOUT', 6, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'MrsPath', 'Resulting Mrs File', 'INOUT', 7, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'MtdPath', 'MTD path that the MRS script will point to', 'OUT', 8, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';
                                END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @" 
                                DECLARE @ModuleID INT;

                                IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'DimValidationModule')
                                BEGIN
                                    INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                    SELECT 'DimValidationModule', 'generate validation script for dimensions project', ModuleGroup.ModuleGroupID, 'GfK.Automata.Tabulation.Service', 'GfK.Automata.Tabulation.Service.DimValidationModule'
                                    FROM ModuleGroup
                                    WHERE ModuleGroup.Name = 'Validation';

                                    SET @ModuleID = Scope_Identity();

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'OutputFolder', 'output folder containing all validation files', 'OUT', 1, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'DMSFile', 'DMS file to execute validation script', 'OUT', 2, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';
                                END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @" 
                            DECLARE @ModuleID INT;

							SELECT @ModuleID = ModuleID
							  FROM dbo.MODULE
							 WHERE  Name = 'EXEModule';

                            IF (@moduleID IS NOT NULL) AND NOT EXISTS (SELECT * FROM dbo.PARAMETER WHERE moduleID = @ModuleID AND name = 'Argument6')
                            BEGIN
                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument6', 'generic argument that can be passed in', 'IN', 8, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument7', 'generic argument that can be passed in', 'IN', 9, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument8', 'generic argument that can be passed in', 'IN', 10, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument9', 'generic argument that can be passed in', 'IN', 11, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument10', 'generic argument that can be passed in', 'IN', 12, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                UPDATE dbo.PARAMETER
                                   SET SequencePosition = SequencePosition + 5
                                 WHERE ModuleID = ModuleID
                                   AND name LIKE 'ret%';
                            END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @" 
                            DECLARE @ModuleID INT;

							SELECT @ModuleID = ModuleID
							  FROM dbo.MODULE
							 WHERE  Name = 'MRSModule';

                            IF (@moduleID IS NOT NULL) AND NOT EXISTS (SELECT * FROM dbo.PARAMETER WHERE moduleID = @ModuleID AND name = 'Argument6')
                            BEGIN
                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument6', 'generic argument that can be passed in', 'IN', 8, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument7', 'generic argument that can be passed in', 'IN', 9, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument8', 'generic argument that can be passed in', 'IN', 10, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument9', 'generic argument that can be passed in', 'IN', 11, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument10', 'generic argument that can be passed in', 'IN', 12, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                UPDATE dbo.PARAMETER
                                   SET SequencePosition = SequencePosition + 5
                                 WHERE ModuleID = ModuleID
                                   AND name LIKE 'ret%';
                            END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            
            seedSQL = @" 
                                DECLARE @ModuleID INT;
                                IF NOT EXISTS(SELECT * FROM Module WHERE Name = 'DimEnhancedSPSSModule')
                                BEGIN
                                    INSERT INTO Module (Name, Description, ModuleGroupID, AssemblyName, TypeName)
                                    SELECT 'DimEnhancedSPSSModule', 'makes an enhanced spss file', ModuleGroup.ModuleGroupID, 'GfK.Automata.Tabulation.Service', 'GfK.Automata.Tabulation.Service.DimEnhancedSPSSModule'
                                    FROM ModuleGroup
                                    WHERE ModuleGroup.Name = 'Transformation';

                                    SET @ModuleID = Scope_Identity();

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'context', 'context', 'IN', 1, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'language', 'language', 'IN', 2, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'mdd', 'mdd', 'IN', 3, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'ddf', 'ddf', 'IN', 4, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'sav', 'sav', 'INOUT', 5, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'version', 'version', 'IN', 6, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'xlsx', 'xlsx', 'INOUT', 7, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'create', 'create', 'IN', 8, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                    INSERT INTO Parameter (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                    SELECT 'read', 'read', 'IN', 9, ParameterType.ParameterTypeID, @ModuleID
                                    FROM ParameterType
                                    WHERE Name = 'String';

                                END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @" 
                            DECLARE @ModuleID INT;

							SELECT @ModuleID = ModuleID
							  FROM dbo.MODULE
							 WHERE  Name = 'DMSModule';

                            IF (@moduleID IS NOT NULL) AND NOT EXISTS (SELECT * FROM dbo.PARAMETER WHERE moduleID = @ModuleID AND name = 'Argument6')
                            BEGIN
                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument6', 'generic argument that can be passed in', 'IN', 8, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument7', 'generic argument that can be passed in', 'IN', 9, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument8', 'generic argument that can be passed in', 'IN', 10, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument9', 'generic argument that can be passed in', 'IN', 11, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument10', 'generic argument that can be passed in', 'IN', 12, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                UPDATE dbo.PARAMETER
                                   SET SequencePosition = SequencePosition + 5
                                 WHERE ModuleID = ModuleID
                                   AND name LIKE 'ret%';
                            END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            seedSQL = @" 
                            DECLARE @ModuleID INT;

							SELECT @ModuleID = ModuleID
							  FROM dbo.MODULE
							 WHERE  Name = 'DMSModule';

                            IF (@moduleID IS NOT NULL) AND NOT EXISTS (SELECT * FROM dbo.PARAMETER WHERE moduleID = @ModuleID AND name = 'Argument11')
                            BEGIN
                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument11', 'generic argument that can be passed in', 'IN', 13, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument12', 'generic argument that can be passed in', 'IN', 14, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument13', 'generic argument that can be passed in', 'IN', 15, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument14', 'generic argument that can be passed in', 'IN', 16, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                INSERT INTO PARAMETER (Name, Description, Direction, SequencePosition, ParameterTypeID, ModuleID)
                                SELECT 'Argument15', 'generic argument that can be passed in', 'IN', 17, ParameterType.ParameterTypeID, @ModuleID
                                FROM ParameterType
                                WHERE Name = 'String';

                                UPDATE dbo.PARAMETER
                                   SET SequencePosition = SequencePosition + 5
                                 WHERE ModuleID = ModuleID
                                   AND name LIKE 'ret%';
                            END;
                ";
            context.Database.ExecuteSqlCommand(seedSQL);

            context.ModuleGroups.AddOrUpdate(
                p => p.Name,
                    new ModuleGroup { Name = "Data Sources" },
                    new ModuleGroup { Name = "Tools" },
                    new ModuleGroup { Name = "Validation" },
                    new ModuleGroup { Name = "Transformation" },
                    new ModuleGroup { Name = "Tabulation" }
                );

            context.ParameterTypes.AddOrUpdate(
                p => p.Name,
                    new ParameterType { Name = "Dimensions Capable" },
                    new ParameterType { Name = "MetaData" },
                    new ParameterType { Name = "Data File (case data)" },
                    new ParameterType { Name = "String" },
                    new ParameterType { Name = "Table" },
                    new ParameterType { Name = "script" }
                );
        

            context.GapRoles.AddOrUpdate(
                p => p.Name,
                    new GapRole { Name = "Admin" },
                    new GapRole { Name = "Execute" },
                    new GapRole { Name = "Configure" }
                );

            context.GapRolePrivileges.AddOrUpdate(
                p => p.Name,
                    new GapRolePrivilege { Name = "Create Groups" },
                    new GapRolePrivilege { Name = "Create Users" },
                    new GapRolePrivilege { Name = "Manage Groups" },
                    new GapRolePrivilege { Name = "Manage Users" },
                    new GapRolePrivilege { Name = "Run Pipeline" },
                    new GapRolePrivilege { Name = "View Results" },
                    new GapRolePrivilege { Name = "Configure Pipeline" },
                    new GapRolePrivilege { Name = "Edit Pipeline" }
                );
        }
    }
}
