namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class PipelineRunRequest_DateDefault : DbMigration
    {
        public override void Up()
        {
            this.CreateColumnDefault("dbo.PipelineRunRequest", "RunRequestDate", " CURRENT_TIMESTAMP", "DF_PipelineRunRequestDate");
        }
        
        public override void Down()
        {
            this.DropConstraint("dbo.PipelineRunRequest", "DF_PipelineRunRequestDate");
        }
    }
}
