namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FK_Project_GapGroup : DbMigration
    {
        public override void Up()
        {
            this.Sql("IF NOT EXISTS(SELECT * FROM GapGroup WHERE Name = 'DefaultGroup') BEGIN INSERT INTO GapGroup (Name) VALUES ('DefaultGroup') UPDATE Project SET GapGroupID = (SELECT GapGroupID FROM GapGroup) END");
            CreateIndex("dbo.Project", "GapGroupID");
            AddForeignKey("dbo.Project", "GapGroupID", "dbo.GapGroup", "GapGroupID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Project", "GapGroupID", "dbo.GapGroup");
            DropIndex("dbo.Project", new[] { "GapGroupID" });
        }
    }
}
