namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_ProjectPipeline_Identity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pipeline", "ParentID", "dbo.Pipeline");
            DropForeignKey("dbo.PipelineInstance", "PipelineId", "dbo.Pipeline");
            DropForeignKey("dbo.ProjectUser", "ProjectID", "dbo.Project");
            DropForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project");
            DropPrimaryKey("dbo.Pipeline");
            DropPrimaryKey("dbo.Project");

            // Added = EF does not know how to drop Identify from a column
            Sql("ALTER TABLE dbo.project  ADD projectid_temp INT NULL");
            Sql("UPDATE dbo.Project    SET projectid_temp = projectid");
            Sql("ALTER TABLE dbo.Project  ALTER COLUMN projectid_temp INT NOT NULL");
            Sql("ALTER TABLE dbo.project DROP COLUMN projectid");
            Sql("EXECUTE sp_rename 'dbo.Project.projectid_temp', 'ProjectID', 'COLUMN'");

            Sql("ALTER TABLE dbo.pipeline  ADD pipelineid_temp INT NULL");
            Sql("UPDATE dbo.pipeline    SET pipelineid_temp = PipeLineID");
            Sql("ALTER TABLE dbo.pipeline  ALTER COLUMN pipelineid_temp INT NOT NULL");
            Sql("ALTER TABLE dbo.pipeline DROP COLUMN PipeLineID");
            Sql("EXECUTE sp_rename 'dbo.pipeline.pipelineid_temp', 'PipeLineID', 'COLUMN'");
            //

            AddPrimaryKey("dbo.Pipeline", "PipeLineID");
            AddPrimaryKey("dbo.Project", "ProjectID");
            AddForeignKey("dbo.Pipeline", "ParentID", "dbo.Pipeline", "PipeLineID");
            AddForeignKey("dbo.PipelineInstance", "PipelineId", "dbo.Pipeline", "PipeLineID", cascadeDelete: true);
            AddForeignKey("dbo.ProjectUser", "ProjectID", "dbo.Project", "ProjectID", cascadeDelete: true);
            AddForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project", "ProjectID");
            AlterStoredProcedure(
                "dbo.Pipeline_Insert",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Pipeline]([PipeLineID], [Name], [ProjectID], [ParentID])
                      VALUES (@PipeLineID, @Name, @ProjectID, @ParentID)"
            );
            
            AlterStoredProcedure(
                "dbo.Project_Insert",
                p => new
                    {
                        ProjectID = p.Int(),
                        Name = p.String(maxLength: 100),
                    },
                body:
                    @"INSERT [dbo].[Project]([ProjectID], [Name])
                      VALUES (@ProjectID, @Name)"
            );
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project");
            DropForeignKey("dbo.ProjectUser", "ProjectID", "dbo.Project");
            DropForeignKey("dbo.PipelineInstance", "PipelineId", "dbo.Pipeline");
            DropForeignKey("dbo.Pipeline", "ParentID", "dbo.Pipeline");
            DropPrimaryKey("dbo.Project");
            DropPrimaryKey("dbo.Pipeline");
            AlterColumn("dbo.Project", "ProjectID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Pipeline", "PipeLineID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Project", "ProjectID");
            AddPrimaryKey("dbo.Pipeline", "PipeLineID");
            AddForeignKey("dbo.Pipeline", "ProjectID", "dbo.Project", "ProjectID");
            AddForeignKey("dbo.ProjectUser", "ProjectID", "dbo.Project", "ProjectID", cascadeDelete: true);
            AddForeignKey("dbo.PipelineInstance", "PipelineId", "dbo.Pipeline", "PipeLineID", cascadeDelete: true);
            AddForeignKey("dbo.Pipeline", "ParentID", "dbo.Pipeline", "PipeLineID");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
