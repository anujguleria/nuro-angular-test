namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;
    
    public partial class ParameterMappingPriorPipeline : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PipelineTaskInputValue", "PriorPipelineID", c => c.Int(nullable: true));
            AlterStoredProcedure(
                "dbo.PipelineTaskInputValue_Insert",
                p => new
                    {
                        PriorPipelineTaskID = p.Int(),
                        PriorPipeLineTaskParameterID = p.Int(),
                        ParameterID = p.Int(),
                        PipelineTaskID = p.Int(),
                        PipelineID = p.Int(),
                        PriorPipelineID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PipelineTaskInputValue]([PriorPipelineTaskID], [PriorPipeLineTaskParameterID], [ParameterID], [PipelineTaskID], [PipelineID], [PriorPipelineID])
                      VALUES (@PriorPipelineTaskID, @PriorPipeLineTaskParameterID, @ParameterID, @PipelineTaskID, @PipelineID, @PriorPipelineID)
                      
                      DECLARE @PipelineTaskInputValueID int
                      SELECT @PipelineTaskInputValueID = [PipelineTaskInputValueID]
                      FROM [dbo].[PipelineTaskInputValue]
                      WHERE @@ROWCOUNT > 0 AND [PipelineTaskInputValueID] = scope_identity()
                      
                      SELECT t0.[PipelineTaskInputValueID]
                      FROM [dbo].[PipelineTaskInputValue] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineTaskInputValueID] = @PipelineTaskInputValueID"
            );
            
            AlterStoredProcedure(
                "dbo.PipelineTaskInputValue_Update",
                p => new
                    {
                        PipelineTaskInputValueID = p.Int(),
                        PriorPipelineTaskID = p.Int(),
                        PriorPipeLineTaskParameterID = p.Int(),
                        ParameterID = p.Int(),
                        PipelineTaskID = p.Int(),
                        PipelineID = p.Int(),
                        PriorPipelineID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineTaskInputValue]
                      SET [PriorPipelineTaskID] = @PriorPipelineTaskID, [PriorPipeLineTaskParameterID] = @PriorPipeLineTaskParameterID, [ParameterID] = @ParameterID, [PipelineTaskID] = @PipelineTaskID, [PipelineID] = @PipelineID, [PriorPipelineID] = @PriorPipelineID
                      WHERE ([PipelineTaskInputValueID] = @PipelineTaskInputValueID)"
            );

            Sql("UPDATE dbo.PipelineTaskInputValue SET PriorPipelineID = PipelineID");
            AlterColumn("dbo.PipelineTaskInputValue", "PriorPipelineID", c => c.Int(nullable: false));

            DropForeignKey("dbo.PipelineTaskInputValue", "FK_PipelineTaskInputValue_PipelineTask_PriorPipelineTaskID");
            AddForeignKey("dbo.PipelineTaskInputValue", new string[] { "PriorPipelineTaskID", "PriorPipelineID" }
                , "dbo.PipelineTask", new string[] { "PipelineTaskID", "PipelineID" }
                , false
                , "FK_PipelineTaskInputValue_PipelineTask_PriorPipelineTaskID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PipelineTaskInputValue", "FK_PipelineTaskInputValue_PipelineTask_PriorPipelineTaskID");
            DropColumn("dbo.PipelineTaskInputValue", "PriorPipelineID");

            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
