namespace GfK.Automata.Pipeline.Data.Migrations
{
    using GfK.Automata.Pipeline.Data.Migrations.Operations;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ProjectPipeline_IDDefaults : DbMigration
    {
        public override void Up()
        {
            this.CreateColumnDefault("Project", "ProjectId", " NEXT VALUE FOR Project_Pipeline_ID_Seq", "DF_GenerateProjectID");
            this.CreateColumnDefault("Pipeline", "PipelineId", " NEXT VALUE FOR Project_Pipeline_ID_Seq", "DF_GeneratePipelineID");
        }

        public override void Down()
        {
            this.DropConstraint("Project", "DF_GenerateProjectID");
            this.DropConstraint("Pipeline", "DF_GeneratePipelineID");
        }
    }
}
