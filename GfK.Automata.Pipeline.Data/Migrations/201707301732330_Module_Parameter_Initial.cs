namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Module_Parameter_Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Module",
                c => new
                    {
                        ModuleID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 1000),
                        ModuleGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ModuleID)
                .ForeignKey("dbo.ModuleGroup", t => t.ModuleGroupID)
                .Index(t => t.Name, unique: true, name: "Unq_Module_Name")
                .Index(t => t.ModuleGroupID);
            
            CreateTable(
                "dbo.Parameter",
                c => new
                    {
                        ParameterID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Description = c.String(maxLength: 1000),
                        Direction = c.String(),
                        SequencePosition = c.Int(nullable: false),
                        ParameterTypeID = c.Int(nullable: false),
                        ModuleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ParameterID)
                .ForeignKey("dbo.ParameterType", t => t.ParameterTypeID)
                .ForeignKey("dbo.Module", t => t.ModuleID, cascadeDelete: true)
                .Index(t => new { t.ModuleID, t.Name }, unique: true, name: "Unq_Parameter_ModuleID_Name")
                .Index(t => t.ParameterTypeID);
            
            CreateStoredProcedure(
                "dbo.Module_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                        ModuleGroupID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Module]([Name], [Description], [ModuleGroupID])
                      VALUES (@Name, @Description, @ModuleGroupID)
                      
                      DECLARE @ModuleID int
                      SELECT @ModuleID = [ModuleID]
                      FROM [dbo].[Module]
                      WHERE @@ROWCOUNT > 0 AND [ModuleID] = scope_identity()
                      
                      SELECT t0.[ModuleID]
                      FROM [dbo].[Module] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ModuleID] = @ModuleID"
            );
            
            CreateStoredProcedure(
                "dbo.Module_Update",
                p => new
                    {
                        ModuleID = p.Int(),
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                        ModuleGroupID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Module]
                      SET [Name] = @Name, [Description] = @Description, [ModuleGroupID] = @ModuleGroupID
                      WHERE ([ModuleID] = @ModuleID)"
            );
            
            CreateStoredProcedure(
                "dbo.Module_Delete",
                p => new
                    {
                        ModuleID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Module]
                      WHERE ([ModuleID] = @ModuleID)"
            );
            
            CreateStoredProcedure(
                "dbo.Parameter_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                        Direction = p.String(),
                        SequencePosition = p.Int(),
                        ParameterTypeID = p.Int(),
                        ModuleID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Parameter]([Name], [Description], [Direction], [SequencePosition], [ParameterTypeID], [ModuleID])
                      VALUES (@Name, @Description, @Direction, @SequencePosition, @ParameterTypeID, @ModuleID)
                      
                      DECLARE @ParameterID int
                      SELECT @ParameterID = [ParameterID]
                      FROM [dbo].[Parameter]
                      WHERE @@ROWCOUNT > 0 AND [ParameterID] = scope_identity()
                      
                      SELECT t0.[ParameterID]
                      FROM [dbo].[Parameter] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ParameterID] = @ParameterID"
            );
            
            CreateStoredProcedure(
                "dbo.Parameter_Update",
                p => new
                    {
                        ParameterID = p.Int(),
                        Name = p.String(maxLength: 100),
                        Description = p.String(maxLength: 1000),
                        Direction = p.String(),
                        SequencePosition = p.Int(),
                        ParameterTypeID = p.Int(),
                        ModuleID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Parameter]
                      SET [Name] = @Name, [Description] = @Description, [Direction] = @Direction, [SequencePosition] = @SequencePosition, [ParameterTypeID] = @ParameterTypeID, [ModuleID] = @ModuleID
                      WHERE ([ParameterID] = @ParameterID)"
            );
            
            CreateStoredProcedure(
                "dbo.Parameter_Delete",
                p => new
                    {
                        ParameterID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Parameter]
                      WHERE ([ParameterID] = @ParameterID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Parameter_Delete");
            DropStoredProcedure("dbo.Parameter_Update");
            DropStoredProcedure("dbo.Parameter_Insert");
            DropStoredProcedure("dbo.Module_Delete");
            DropStoredProcedure("dbo.Module_Update");
            DropStoredProcedure("dbo.Module_Insert");
            DropForeignKey("dbo.Module", "ModuleGroupID", "dbo.ModuleGroup");
            DropForeignKey("dbo.Parameter", "ModuleID", "dbo.Module");
            DropForeignKey("dbo.Parameter", "ParameterTypeID", "dbo.ParameterType");
            DropIndex("dbo.Parameter", new[] { "ParameterTypeID" });
            DropIndex("dbo.Parameter", "Unq_Parameter_ModuleID_Name");
            DropIndex("dbo.Module", new[] { "ModuleGroupID" });
            DropIndex("dbo.Module", "Unq_Module_Name");
            DropTable("dbo.Parameter");
            DropTable("dbo.Module");
        }
    }
}
