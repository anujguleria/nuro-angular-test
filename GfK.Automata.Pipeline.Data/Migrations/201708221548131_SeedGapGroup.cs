namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedGapGroup : DbMigration
    {
        public override void Up()
        {
            // This is an empty migration to trigger execution of seeding added for GapGroup
        }
        
        public override void Down()
        {
        }
    }
}
