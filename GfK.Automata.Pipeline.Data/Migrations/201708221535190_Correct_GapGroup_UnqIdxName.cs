namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Correct_GapGroup_UnqIdxName : DbMigration
    {
        public override void Up()
        {
            RenameIndex(table: "dbo.GapGroup", name: "Unq_GapProject_Name", newName: "Unq_GapGroup_Name");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.GapGroup", name: "Unq_GapGroup_Name", newName: "Unq_GapProject_Name");
        }
    }
}
