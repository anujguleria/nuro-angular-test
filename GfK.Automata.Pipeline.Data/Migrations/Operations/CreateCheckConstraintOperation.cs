﻿using System;
using System.Data.Entity.Migrations.Model;

namespace GfK.Automata.Pipeline.Data.Migrations.Operations
{
    public class CreateCheckConstraintOperation : ColumnConstraintOperation
    {
        public CreateCheckConstraintOperation()
            : base()
        {
        }

        public override string BuildDefaultName()
        {
            return string.Format("CK_{0}_{1}", Table, Column);
        }
    }
}