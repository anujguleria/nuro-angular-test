﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Migrations.Operations
{
    public class CreateColumnDefaultOperation : ColumnConstraintOperation
    {
        public CreateColumnDefaultOperation()
            :base()
        {
        }

        public override string BuildDefaultName()
        {
            return string.Format("DFLT_{0}_{1}", Table, Column);
        }
    }
}
