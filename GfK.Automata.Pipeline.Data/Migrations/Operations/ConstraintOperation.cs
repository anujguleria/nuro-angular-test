﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Migrations.Operations
{
    public abstract class ConstraintOperation : MigrationOperation
    {
        private string _table;
        private string _constraint;
        private string _constraintName;

        public ConstraintOperation()
            : base(null)
        {
        }

        public string Table
        {
            get { return _table; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(
                        "Argument is null or contains only white spaces.",
                        "value");
                }

                _table = value;
            }
        }

        public string Constraint
        {
            get { return _constraint; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(
                        "Argument is null or contains only white spaces.",
                        "value");
                }

                _constraint = value;
            }
        }

        public string ConstraintName
        {
            get { return _constraintName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(
                        "Argument is null or contains only white spaces.",
                        "value");
                }

                _constraintName = value;
            }
        }

        public override bool IsDestructiveChange
        {
            get { return false; }
        }

        public virtual string BuildDefaultName()
        {
            return string.Format("CONSTRAINT_{0}", Table);
        }
    }
}
