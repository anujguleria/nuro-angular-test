﻿using System.Data.Entity.Migrations.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Migrations.Operations
{
    public class CreateComputedColumnOperation : MigrationOperation
    {
        private string _table;
        private string _column;
        private string _expression;
        private bool _persisted;
        private bool _canBeNull;

        public CreateComputedColumnOperation()
            : base(null)
        {
        }

        public string Table
        {
            get { return _table; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(
                        "Argument is null or contains only white spaces.",
                        "value");
                }

                _table = value;
            }
        }

        public string Column
        {
            get { return _column; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(
                        "Argument is null or contains only white spaces.",
                        "value");
                }

                _column = value;
            }
        }

        public string Expression
        {
            get { return _expression; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(
                        "Argument is null or contains only white spaces.",
                        "value");
                }

                _expression = value;
            }
        }

        public bool Persisted
        {
            get { return _persisted; }
            set
            {
                _persisted = value;
            }
        }

        public bool CanBeNull
        {
            get { return _canBeNull; }
            set
            {
                 _canBeNull = value;
            }
        }
        public override bool IsDestructiveChange
        {
            get { return false; }
        }
    }
}
