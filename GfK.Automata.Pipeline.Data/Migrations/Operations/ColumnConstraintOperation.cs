﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Migrations.Operations
{
    public abstract class ColumnConstraintOperation : ConstraintOperation
    {
        private string _column;

        public ColumnConstraintOperation()
            : base()
        {
        }

        public string Column
        {
            get { return _column; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(
                        "Argument is null or contains only white spaces.",
                        "value");
                }

                _column = value;
            }
        }

        public override string BuildDefaultName()
        {
            return string.Format("CONSTRAINT_{0}_{1}", Table, Column);
        }
    }
}
