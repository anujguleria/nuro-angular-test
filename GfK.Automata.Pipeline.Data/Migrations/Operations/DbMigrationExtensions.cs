﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Migrations.Operations
{
    public static class DbMigrationExtensions
    {
        public static void CreateCheckConstraint(this DbMigration migration, string table, string checkConstraint, string constraintName = "")
        {
            var createCheckConstraint = new CreateCheckConstraintOperation
            {
                Table = table,
                Constraint = checkConstraint,
                ConstraintName = constraintName
            };

            ((IDbMigration)migration).AddOperation(createCheckConstraint);
        }
        public static void CreateColumnDefault(this DbMigration migration, string table, string column, string defaultValue, string name)
        {
            CreateColumnDefaultOperation createDefaultConstraint = new CreateColumnDefaultOperation
            {
                Table = table,
                Column = column,
                Constraint = defaultValue,
                ConstraintName = name
            };

            ((IDbMigration)migration).AddOperation(createDefaultConstraint);
        }
        public static void CreatePersistedComputedColumn(this DbMigration migration, string table, string column, string expression)
        {
            CreateComputedColumnOperation createPersistedComputedColumn = new CreateComputedColumnOperation
            {
                Table = table,
                Column = column,
                Expression = expression,
                CanBeNull = false,
                Persisted = true
            };

            ((IDbMigration)migration).AddOperation(createPersistedComputedColumn);
        }
        public static void CreateUnPersistedComputedColumn(this DbMigration migration, string table, string column, string expression, bool nullable)
        {
            CreateComputedColumnOperation createUnPersistedComputedColumn = new CreateComputedColumnOperation
            {
                Table = table,
                Column = column,
                Expression = expression,
                CanBeNull = nullable,
                Persisted = false
            };

            ((IDbMigration)migration).AddOperation(createUnPersistedComputedColumn);
        }

        public static void DropConstraint(this DbMigration migration, string table, string constraintName)
        {
            DropConstraintOperation dropConstraintOperation = new DropConstraintOperation
            {
                Table = table,
                ConstraintName = constraintName
            };

            ((IDbMigration)migration).AddOperation(dropConstraintOperation);
        }
    }
}
