﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Migrations.Operations
{
    class CustomSqlGenerator : SqlServerMigrationSqlGenerator
    {
        public void AddCreateCheckConstraintOperation(MigrationOperation migrationOperation)
        {
            CreateCheckConstraintOperation checkConstraintOperation = migrationOperation as CreateCheckConstraintOperation;

            if (checkConstraintOperation != null)
            {
                if (checkConstraintOperation.ConstraintName == null)
                {
                    checkConstraintOperation.ConstraintName = checkConstraintOperation.BuildDefaultName();
                }

                using (var writer = Writer())
                {
                    writer.WriteLine(
                        "ALTER TABLE {0} ADD CONSTRAINT {1} CHECK ({2})",
                        Name(checkConstraintOperation.Table),
                        Quote(checkConstraintOperation.ConstraintName),
                        checkConstraintOperation.Constraint
                    );
                    Statement(writer);
                }
            }
        }

        public void AddCreateColumnDefaultOperation(MigrationOperation migrationOperation)
        {
            CreateColumnDefaultOperation defaultOperation = migrationOperation as CreateColumnDefaultOperation;

            if (defaultOperation != null)
            {
                if (defaultOperation.ConstraintName == null)
                {
                    defaultOperation.ConstraintName = defaultOperation.BuildDefaultName();
                }

                using (var writer = Writer())
                {
                    writer.WriteLine(
                        "ALTER TABLE {0} ADD CONSTRAINT {1} DEFAULT ({2}) FOR {3}",
                        Name(defaultOperation.Table),
                        Quote(defaultOperation.ConstraintName),
                        defaultOperation.Constraint,
                        Quote(defaultOperation.Column)
                    );
                    Statement(writer);
                }
            }
        }

        public void AddCreateComputedColumnOperation(MigrationOperation migrationOperation)
        {
            CreateComputedColumnOperation createComputedColumnOperation = migrationOperation as CreateComputedColumnOperation;

            if (createComputedColumnOperation != null)
            {
                using (var writer = Writer())
                {
                    string persisted = (createComputedColumnOperation.Persisted ? " PERSISTED " : " NOT PERSISTED ");
                    string isNull = (createComputedColumnOperation.CanBeNull ? " NULL " : " NOT NULL ");

                    writer.WriteLine(
                        "ALTER TABLE {0} ADD {1} AS ({2}) {3} {4} ",
                        Name(createComputedColumnOperation.Table),
                        Quote(createComputedColumnOperation.Column),
                        createComputedColumnOperation.Expression,
                        persisted,
                        isNull
                    );
                    Statement(writer);
                }
            }
        }

        public void AddDropConstraintOperation(MigrationOperation migrationOperation)
        {
            DropConstraintOperation checkConstraintOperation = migrationOperation as DropConstraintOperation;

            if (checkConstraintOperation != null)
            {
                using (var writer = Writer())
                {
                    writer.WriteLine(
                        "ALTER TABLE {0} DROP CONSTRAINT {1}",
                        Name(checkConstraintOperation.Table),
                        Quote(checkConstraintOperation.ConstraintName)
                    );
                    Statement(writer);
                }
            }
        }

        protected override void Generate(MigrationOperation migrationOperation)
        {
            if (migrationOperation.GetType().ToString() == "GfK.Automata.Pipeline.Data.Migrations.Operations.CreateCheckConstraintOperation")
            {
                AddCreateCheckConstraintOperation(migrationOperation);
            }

            if (migrationOperation.GetType().ToString() == "GfK.Automata.Pipeline.Data.Migrations.Operations.CreateColumnDefaultOperation")
            {
                AddCreateColumnDefaultOperation(migrationOperation);
            }

            if (migrationOperation.GetType().ToString() == "GfK.Automata.Pipeline.Data.Migrations.Operations.CreateComputedColumnOperation")
            {
                AddCreateComputedColumnOperation(migrationOperation);
            }

            if (migrationOperation.GetType().ToString() == "GfK.Automata.Pipeline.Data.Migrations.Operations.DropConstraintOperation")
            {
                AddDropConstraintOperation(migrationOperation);
            }
        }
    }
}
