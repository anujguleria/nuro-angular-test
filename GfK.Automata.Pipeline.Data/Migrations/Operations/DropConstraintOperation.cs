﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Migrations.Operations
{
    public class DropConstraintOperation : ConstraintOperation
    {
        public DropConstraintOperation()
            :base() {  }
    }
}
