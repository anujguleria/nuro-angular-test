namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatingzoneasstring : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedule", "TimeZoneID", c => c.String());
            DropColumn("dbo.Schedule", "TimezoneAbbreviation");
            DropColumn("dbo.Schedule", "TimezoneOffset");
            AlterStoredProcedure(
                "dbo.Schedule_Insert",
                p => new
                    {
                        StartTime = p.DateTime(),
                        Enabled = p.Boolean(),
                        Recurrence = p.Byte(),
                        TimeZoneID = p.String(),
                        DailyRecurrenceType = p.Byte(),
                        RecurEvery = p.Int(),
                        OnMonday = p.Boolean(),
                        OnTuesday = p.Boolean(),
                        OnWednesday = p.Boolean(),
                        OnThursday = p.Boolean(),
                        OnFriday = p.Boolean(),
                        OnSaturday = p.Boolean(),
                        OnSunday = p.Boolean(),
                        MonthlyRecurrenceType = p.Byte(),
                        DayOfWeekOffset = p.Byte(),
                        DayOfMonth = p.Byte(),
                        YearlyRecurrenceType = p.Byte(),
                        MonthOfYear = p.Byte(),
                    },
                body:
                    @"INSERT [dbo].[Schedule]([StartTime], [Enabled], [Recurrence], [TimeZoneID], [DailyRecurrenceType], [RecurEvery], [OnMonday], [OnTuesday], [OnWednesday], [OnThursday], [OnFriday], [OnSaturday], [OnSunday], [MonthlyRecurrenceType], [DayOfWeekOffset], [DayOfMonth], [YearlyRecurrenceType], [MonthOfYear])
                      VALUES (@StartTime, @Enabled, @Recurrence, @TimeZoneID, @DailyRecurrenceType, @RecurEvery, @OnMonday, @OnTuesday, @OnWednesday, @OnThursday, @OnFriday, @OnSaturday, @OnSunday, @MonthlyRecurrenceType, @DayOfWeekOffset, @DayOfMonth, @YearlyRecurrenceType, @MonthOfYear)
                      
                      DECLARE @ScheduleID int
                      SELECT @ScheduleID = [ScheduleID]
                      FROM [dbo].[Schedule]
                      WHERE @@ROWCOUNT > 0 AND [ScheduleID] = scope_identity()
                      
                      SELECT t0.[ScheduleID]
                      FROM [dbo].[Schedule] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ScheduleID] = @ScheduleID"
            );
            
            AlterStoredProcedure(
                "dbo.Schedule_Update",
                p => new
                    {
                        ScheduleID = p.Int(),
                        StartTime = p.DateTime(),
                        Enabled = p.Boolean(),
                        Recurrence = p.Byte(),
                        TimeZoneID = p.String(),
                        DailyRecurrenceType = p.Byte(),
                        RecurEvery = p.Int(),
                        OnMonday = p.Boolean(),
                        OnTuesday = p.Boolean(),
                        OnWednesday = p.Boolean(),
                        OnThursday = p.Boolean(),
                        OnFriday = p.Boolean(),
                        OnSaturday = p.Boolean(),
                        OnSunday = p.Boolean(),
                        MonthlyRecurrenceType = p.Byte(),
                        DayOfWeekOffset = p.Byte(),
                        DayOfMonth = p.Byte(),
                        YearlyRecurrenceType = p.Byte(),
                        MonthOfYear = p.Byte(),
                    },
                body:
                    @"UPDATE [dbo].[Schedule]
                      SET [StartTime] = @StartTime, [Enabled] = @Enabled, [Recurrence] = @Recurrence, [TimeZoneID] = @TimeZoneID, [DailyRecurrenceType] = @DailyRecurrenceType, [RecurEvery] = @RecurEvery, [OnMonday] = @OnMonday, [OnTuesday] = @OnTuesday, [OnWednesday] = @OnWednesday, [OnThursday] = @OnThursday, [OnFriday] = @OnFriday, [OnSaturday] = @OnSaturday, [OnSunday] = @OnSunday, [MonthlyRecurrenceType] = @MonthlyRecurrenceType, [DayOfWeekOffset] = @DayOfWeekOffset, [DayOfMonth] = @DayOfMonth, [YearlyRecurrenceType] = @YearlyRecurrenceType, [MonthOfYear] = @MonthOfYear
                      WHERE ([ScheduleID] = @ScheduleID)"
            );
            
        }
        
        public override void Down()
        {
            AddColumn("dbo.Schedule", "TimezoneOffset", c => c.Int(nullable: false));
            AddColumn("dbo.Schedule", "TimezoneAbbreviation", c => c.String(maxLength: 10));
            DropColumn("dbo.Schedule", "TimeZoneID");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
