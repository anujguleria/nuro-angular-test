namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class AlignColumnTypeSize : DbMigration
    {
        public override void Up()
        {
            this.DropConstraint("Parameter", "CK_Parameter_Direction");
            this.AlterColumn("dbo.Parameter", "Direction", c => c.String(maxLength: 5, fixedLength: true, unicode: false, nullable: false));
            this.CreateCheckConstraint("Parameter"
                , "(Direction ='In' OR Direction ='Out' OR Direction ='InOut')"
                , "CK_Parameter_Direction");

            this.DropIndex("dbo.Pipeline", "Unq_Pipeline_ProjectId_Name");
            this.DropIndex("dbo.Pipeline", "Unq_Pipeline_ParentToPipelineID_Name");
            this.AlterColumn("dbo.Pipeline", "Name", c => c.String(maxLength: 100, nullable: false));
            this.CreateIndex("dbo.Pipeline", new string[] { "ParentToPipelineID", "Name" }, true, "Unq_Pipeline_ParentToPipelineID_Name");

            this.DropIndex("dbo.ModuleGroup", "Unq_ModuleGroup_Name");
            this.AlterColumn("dbo.ModuleGroup", "Name", c => c.String(maxLength: 100, nullable: false));
            this.CreateIndex("dbo.ModuleGroup", "Name", true, "Unq_ModuleGroup_Name");
        }
        
        public override void Down()
        {
        }
    }
}
