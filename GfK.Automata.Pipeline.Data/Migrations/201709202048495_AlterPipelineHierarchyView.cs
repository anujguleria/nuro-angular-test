namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class compare : DbMigration
    {
        public override void Up()
        {
            // This generated when switching the Schedule id to nullable in PipelineHierarchy.  The original
            // migration (AddPipelineHierarchyView) was generated when first defining the PipelineHiearchy class.
        }
        
        public override void Down()
        {

        }
    }
}
