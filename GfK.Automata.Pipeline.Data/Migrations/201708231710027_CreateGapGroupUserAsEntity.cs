namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateGapGroupUserAsEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserGapGroup", "User_UserID", "dbo.User");
            DropForeignKey("dbo.UserGapGroup", "GapGroup_GapGroupID", "dbo.GapGroup");
            DropIndex("dbo.UserGapGroup", new[] { "User_UserID" });
            DropIndex("dbo.UserGapGroup", new[] { "GapGroup_GapGroupID" });
            CreateTable(
                "dbo.GapGroupUser",
                c => new
                    {
                        GapGroupID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GapGroupID, t.UserID })
                .ForeignKey("dbo.GapGroup", t => t.GapGroupID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.GapGroupID)
                .Index(t => t.UserID);
            
            DropTable("dbo.UserGapGroup");
            CreateStoredProcedure(
                "dbo.GapGroupUser_Insert",
                p => new
                    {
                        GapGroupID = p.Int(),
                        UserID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[GapGroupUser]([GapGroupID], [UserID])
                      VALUES (@GapGroupID, @UserID)"
            );
            
            CreateStoredProcedure(
                "dbo.GapGroupUser_Update",
                p => new
                    {
                        GapGroupID = p.Int(),
                        UserID = p.Int(),
                    },
                body:
                    @"RETURN"
            );
            
            CreateStoredProcedure(
                "dbo.GapGroupUser_Delete",
                p => new
                    {
                        GapGroupID = p.Int(),
                        UserID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[GapGroupUser]
                      WHERE (([GapGroupID] = @GapGroupID) AND ([UserID] = @UserID))"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.GapGroupUser_Delete");
            DropStoredProcedure("dbo.GapGroupUser_Update");
            DropStoredProcedure("dbo.GapGroupUser_Insert");
            CreateTable(
                "dbo.UserGapGroup",
                c => new
                    {
                        User_UserID = c.Int(nullable: false),
                        GapGroup_GapGroupID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserID, t.GapGroup_GapGroupID });
            
            DropForeignKey("dbo.GapGroupUser", "UserID", "dbo.User");
            DropForeignKey("dbo.GapGroupUser", "GapGroupID", "dbo.GapGroup");
            DropIndex("dbo.GapGroupUser", new[] { "UserID" });
            DropIndex("dbo.GapGroupUser", new[] { "GapGroupID" });
            DropTable("dbo.GapGroupUser");
            CreateIndex("dbo.UserGapGroup", "GapGroup_GapGroupID");
            CreateIndex("dbo.UserGapGroup", "User_UserID");
            AddForeignKey("dbo.UserGapGroup", "GapGroup_GapGroupID", "dbo.GapGroup", "GapGroupID", cascadeDelete: true);
            AddForeignKey("dbo.UserGapGroup", "User_UserID", "dbo.User", "UserID", cascadeDelete: true);
        }
    }
}
