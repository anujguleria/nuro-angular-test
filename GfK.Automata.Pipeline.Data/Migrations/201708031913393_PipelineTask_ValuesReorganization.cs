namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineTask_ValuesReorganization : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Value", "ParameterID", "dbo.Parameter");
            DropForeignKey("dbo.Value", "PipelineTaskID", "dbo.PipelineTask");
            DropIndex("dbo.Value", new[] { "ParameterID" });
            DropIndex("dbo.Value", new[] { "PipelineTaskID" });
            DropIndex("dbo.PipelineTask", "Unq_PipelineTask_PipeLineID_Name");
            CreateTable(
                "dbo.PipelineTaskInputValue",
                c => new
                    {
                        PipelineTaskInputValueID = c.Int(nullable: false, identity: true),
                        PriorPipelineTaskID = c.Int(nullable: false),
                        PriorPipeLineTaskParameterID = c.Int(nullable: false),
                        ParameterID = c.Int(nullable: false),
                        PipelineTaskID = c.Int(nullable: false),
                        PipelineID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PipelineTaskInputValueID)
                .ForeignKey("dbo.PipelineTask", t => t.PipelineTaskID, cascadeDelete: true)
                .ForeignKey("dbo.Parameter", t => t.ParameterID)
                .Index(t => t.ParameterID)
                .Index(t => t.PipelineTaskID);
            
            CreateTable(
                "dbo.UserInputValue",
                c => new
                    {
                        UserInputValueID = c.Int(nullable: false, identity: true),
                        InputValue = c.String(),
                        ParameterID = c.Int(nullable: false),
                        PipelineTaskID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserInputValueID)
                .ForeignKey("dbo.PipelineTask", t => t.PipelineTaskID, cascadeDelete: true)
                .ForeignKey("dbo.Parameter", t => t.ParameterID)
                .Index(t => t.ParameterID)
                .Index(t => t.PipelineTaskID);
            
            CreateIndex("dbo.PipelineTask", new[] { "PipelineTaskID", "PipeLineID" }, unique: true, name: "Unq_PipelineTask_PipelineTaskID_PipelineID");
            CreateIndex("dbo.PipelineTask", "Name", unique: true, name: "Unq_PipelineTask_PipeLineID_Name");
            DropTable("dbo.Value");
            CreateStoredProcedure(
                "dbo.PipelineTaskInputValue_Insert",
                p => new
                    {
                        PriorPipelineTaskID = p.Int(),
                        PriorPipeLineTaskParameterID = p.Int(),
                        ParameterID = p.Int(),
                        PipelineTaskID = p.Int(),
                        PipelineID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[PipelineTaskInputValue]([PriorPipelineTaskID], [PriorPipeLineTaskParameterID], [ParameterID], [PipelineTaskID], [PipelineID])
                      VALUES (@PriorPipelineTaskID, @PriorPipeLineTaskParameterID, @ParameterID, @PipelineTaskID, @PipelineID)
                      
                      DECLARE @PipelineTaskInputValueID int
                      SELECT @PipelineTaskInputValueID = [PipelineTaskInputValueID]
                      FROM [dbo].[PipelineTaskInputValue]
                      WHERE @@ROWCOUNT > 0 AND [PipelineTaskInputValueID] = scope_identity()
                      
                      SELECT t0.[PipelineTaskInputValueID]
                      FROM [dbo].[PipelineTaskInputValue] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineTaskInputValueID] = @PipelineTaskInputValueID"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTaskInputValue_Update",
                p => new
                    {
                        PipelineTaskInputValueID = p.Int(),
                        PriorPipelineTaskID = p.Int(),
                        PriorPipeLineTaskParameterID = p.Int(),
                        ParameterID = p.Int(),
                        PipelineTaskID = p.Int(),
                        PipelineID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineTaskInputValue]
                      SET [PriorPipelineTaskID] = @PriorPipelineTaskID, [PriorPipeLineTaskParameterID] = @PriorPipeLineTaskParameterID, [ParameterID] = @ParameterID, [PipelineTaskID] = @PipelineTaskID, [PipelineID] = @PipelineID
                      WHERE ([PipelineTaskInputValueID] = @PipelineTaskInputValueID)"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineTaskInputValue_Delete",
                p => new
                    {
                        PipelineTaskInputValueID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PipelineTaskInputValue]
                      WHERE ([PipelineTaskInputValueID] = @PipelineTaskInputValueID)"
            );
            
            CreateStoredProcedure(
                "dbo.UserInputValue_Insert",
                p => new
                    {
                        InputValue = p.String(),
                        ParameterID = p.Int(),
                        PipelineTaskID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[UserInputValue]([InputValue], [ParameterID], [PipelineTaskID])
                      VALUES (@InputValue, @ParameterID, @PipelineTaskID)
                      
                      DECLARE @UserInputValueID int
                      SELECT @UserInputValueID = [UserInputValueID]
                      FROM [dbo].[UserInputValue]
                      WHERE @@ROWCOUNT > 0 AND [UserInputValueID] = scope_identity()
                      
                      SELECT t0.[UserInputValueID]
                      FROM [dbo].[UserInputValue] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[UserInputValueID] = @UserInputValueID"
            );
            
            CreateStoredProcedure(
                "dbo.UserInputValue_Update",
                p => new
                    {
                        UserInputValueID = p.Int(),
                        InputValue = p.String(),
                        ParameterID = p.Int(),
                        PipelineTaskID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[UserInputValue]
                      SET [InputValue] = @InputValue, [ParameterID] = @ParameterID, [PipelineTaskID] = @PipelineTaskID
                      WHERE ([UserInputValueID] = @UserInputValueID)"
            );
            
            CreateStoredProcedure(
                "dbo.UserInputValue_Delete",
                p => new
                    {
                        UserInputValueID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[UserInputValue]
                      WHERE ([UserInputValueID] = @UserInputValueID)"
            );
            
            DropStoredProcedure("dbo.Value_Insert");
            DropStoredProcedure("dbo.Value_Update");
            DropStoredProcedure("dbo.Value_Delete");
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.UserInputValue_Delete");
            DropStoredProcedure("dbo.UserInputValue_Update");
            DropStoredProcedure("dbo.UserInputValue_Insert");
            DropStoredProcedure("dbo.PipelineTaskInputValue_Delete");
            DropStoredProcedure("dbo.PipelineTaskInputValue_Update");
            DropStoredProcedure("dbo.PipelineTaskInputValue_Insert");
            CreateTable(
                "dbo.Value",
                c => new
                    {
                        ValueID = c.Int(nullable: false, identity: true),
                        InputValue = c.String(),
                        InputPriorTaskValueID = c.Int(),
                        ParameterID = c.Int(nullable: false),
                        PipelineTaskID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ValueID);
            
            DropForeignKey("dbo.UserInputValue", "ParameterID", "dbo.Parameter");
            DropForeignKey("dbo.PipelineTaskInputValue", "ParameterID", "dbo.Parameter");
            DropForeignKey("dbo.UserInputValue", "PipelineTaskID", "dbo.PipelineTask");
            DropForeignKey("dbo.PipelineTaskInputValue", "PipelineTaskID", "dbo.PipelineTask");
            DropIndex("dbo.UserInputValue", new[] { "PipelineTaskID" });
            DropIndex("dbo.UserInputValue", new[] { "ParameterID" });
            DropIndex("dbo.PipelineTask", "Unq_PipelineTask_PipeLineID_Name");
            DropIndex("dbo.PipelineTask", "Unq_PipelineTask_PipelineTaskID_PipelineID");
            DropIndex("dbo.PipelineTaskInputValue", new[] { "PipelineTaskID" });
            DropIndex("dbo.PipelineTaskInputValue", new[] { "ParameterID" });
            DropTable("dbo.UserInputValue");
            DropTable("dbo.PipelineTaskInputValue");
            CreateIndex("dbo.PipelineTask", new[] { "PipeLineID", "Name" }, unique: true, name: "Unq_PipelineTask_PipeLineID_Name");
            CreateIndex("dbo.Value", "PipelineTaskID");
            CreateIndex("dbo.Value", "ParameterID");
            AddForeignKey("dbo.Value", "PipelineTaskID", "dbo.PipelineTask", "PipelineTaskID", cascadeDelete: true);
            AddForeignKey("dbo.Value", "ParameterID", "dbo.Parameter", "ParameterID");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
