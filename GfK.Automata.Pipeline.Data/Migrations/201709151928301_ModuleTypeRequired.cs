namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using GfK.Automata.Pipeline.Data.Migrations.Operations;

    public partial class ModuleTypeRequired : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.Module SET ModuleType = 'BUILTIN'");
            AlterColumn("dbo.Module", "ModuleType", c => c.String(nullable: false, maxLength: 10));

            this.CreateColumnDefault("dbo.Module", "ModuleType", "'BUILTIN'", "DF_Module_ModuleType");
            this.CreateCheckConstraint("dbo.Module", "(ModuleType ='BUILTIN' OR ModuleType ='CUSTOM' OR ModuleType ='SYSTEM')", "CK_Module_ModuleType");
        }
        
        public override void Down()
        {
            this.DropConstraint("dbo.Module", "DF_Module_ModuleType");
            this.DropConstraint("dbo.Module", "CK_Module_ModuleType");
            AlterColumn("dbo.Module", "ModuleType", c => c.String(maxLength: 10));
        }
    }
}
