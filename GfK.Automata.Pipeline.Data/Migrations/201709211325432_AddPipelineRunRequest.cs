namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPipelineRunRequest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PipelineRunRequest",
                c => new
                    {
                        PipelineRunRequestID = c.Int(nullable: false, identity: true),
                        PipelineID = c.Int(nullable: false),
                        RunRequestDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PipelineRunRequestID)
                .Index(t => new { t.PipelineID, t.RunRequestDate }, unique: true, name: "Unq_PipelineRunRequest_PipelineID_RunRequestDate");
            
            CreateStoredProcedure(
                "dbo.PipelineRunRequest_Insert",
                p => new
                    {
                        PipelineID = p.Int(),
                        RunRequestDate = p.DateTime(),
                    },
                body:
                    @"INSERT [dbo].[PipelineRunRequest]([PipelineID], [RunRequestDate])
                      VALUES (@PipelineID, @RunRequestDate)
                      
                      DECLARE @PipelineRunRequestID int
                      SELECT @PipelineRunRequestID = [PipelineRunRequestID]
                      FROM [dbo].[PipelineRunRequest]
                      WHERE @@ROWCOUNT > 0 AND [PipelineRunRequestID] = scope_identity()
                      
                      SELECT t0.[PipelineRunRequestID]
                      FROM [dbo].[PipelineRunRequest] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipelineRunRequestID] = @PipelineRunRequestID"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineRunRequest_Update",
                p => new
                    {
                        PipelineRunRequestID = p.Int(),
                        PipelineID = p.Int(),
                        RunRequestDate = p.DateTime(),
                    },
                body:
                    @"UPDATE [dbo].[PipelineRunRequest]
                      SET [PipelineID] = @PipelineID, [RunRequestDate] = @RunRequestDate
                      WHERE ([PipelineRunRequestID] = @PipelineRunRequestID)"
            );
            
            CreateStoredProcedure(
                "dbo.PipelineRunRequest_Delete",
                p => new
                    {
                        PipelineRunRequestID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[PipelineRunRequest]
                      WHERE ([PipelineRunRequestID] = @PipelineRunRequestID)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.PipelineRunRequest_Delete");
            DropStoredProcedure("dbo.PipelineRunRequest_Update");
            DropStoredProcedure("dbo.PipelineRunRequest_Insert");
            DropIndex("dbo.PipelineRunRequest", "Unq_PipelineRunRequest_PipelineID_RunRequestDate");
            DropTable("dbo.PipelineRunRequest");
        }
    }
}
