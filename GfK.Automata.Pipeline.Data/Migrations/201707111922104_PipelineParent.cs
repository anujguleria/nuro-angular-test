namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineParent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pipeline", "ParentID", c => c.Int());
            CreateIndex("dbo.Pipeline", "ParentID");
            AddForeignKey("dbo.Pipeline", "ParentID", "dbo.Pipeline", "PipeLineID");
            AlterStoredProcedure(
                "dbo.Pipeline_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Pipeline]([Name], [ProjectID], [ParentID])
                      VALUES (@Name, @ProjectID, @ParentID)
                      
                      DECLARE @PipeLineID int
                      SELECT @PipeLineID = [PipeLineID]
                      FROM [dbo].[Pipeline]
                      WHERE @@ROWCOUNT > 0 AND [PipeLineID] = scope_identity()
                      
                      SELECT t0.[PipeLineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
            
            AlterStoredProcedure(
                "dbo.Pipeline_Update",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Pipeline]
                      SET [Name] = @Name, [ProjectID] = @ProjectID, [ParentID] = @ParentID
                      WHERE ([PipeLineID] = @PipeLineID)"
            );
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pipeline", "ParentID", "dbo.Pipeline");
            DropIndex("dbo.Pipeline", new[] { "ParentID" });
            DropColumn("dbo.Pipeline", "ParentID");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
