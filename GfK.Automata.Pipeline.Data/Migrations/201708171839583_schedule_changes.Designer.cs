// <auto-generated />
namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class schedule_changes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(schedule_changes));
        
        string IMigrationMetadata.Id
        {
            get { return "201708171839583_schedule_changes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
