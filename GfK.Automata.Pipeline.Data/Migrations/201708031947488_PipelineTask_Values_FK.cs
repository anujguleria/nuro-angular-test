namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PipelineTask_Values_FK : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.PipelineTaskInputValue", new string[] { "PipelineTaskID", "PipelineID" }
                , "dbo.PipelineTask", new string[] { "PipelineTaskID", "PipelineID" }
                , false
                , "FK_PipelineTaskInputValue_PipelineTask_PipelineTaskID");

            AddForeignKey("dbo.PipelineTaskInputValue", new string[] { "PriorPipelineTaskID", "PipelineID" }
                , "dbo.PipelineTask", new string[] { "PipelineTaskID", "PipelineID" }
                , false
                , "FK_PipelineTaskInputValue_PipelineTask_PriorPipelineTaskID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PipelineTaskInputValue", "FK_PipelineTaskInputValue_PipelineTask_PipelineTaskID");
            DropForeignKey("dbo.PipelineTaskInputValue", "FK_PipelineTaskInputValue_PipelineTask_PriorPipelineTaskID");
        }
    }
}
