namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Schedule : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Schedule",
                c => new
                    {
                        ScheduleID = c.Int(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        Recurrence = c.Byte(nullable: false),
                        TimezoneAbbreviation = c.String(maxLength: 10),
                        TimezoneOffset = c.Int(nullable: false),
                        DailyRecurrenceType = c.Byte(nullable: false),
                        RecurEvery = c.Int(nullable: false),
                        OnMonday = c.Boolean(nullable: false),
                        OnTuesday = c.Boolean(nullable: false),
                        OnWednesday = c.Boolean(nullable: false),
                        OnThursday = c.Boolean(nullable: false),
                        OnFriday = c.Boolean(nullable: false),
                        OnSaturday = c.Boolean(nullable: false),
                        OnSunday = c.Boolean(nullable: false),
                        MonthlyRecurrenceType = c.Byte(nullable: false),
                        DayOfWeekOffset = c.Byte(nullable: false),
                        DayOfMonth = c.Byte(nullable: false),
                        YearlyRecurrenceType = c.Byte(nullable: false),
                        MonthOfYear = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ScheduleID);
            
            AddColumn("dbo.Pipeline", "Schedule_ScheduleID", c => c.Int());
            CreateIndex("dbo.Pipeline", "Schedule_ScheduleID");
            AddForeignKey("dbo.Pipeline", "Schedule_ScheduleID", "dbo.Schedule", "ScheduleID");
            CreateStoredProcedure(
                "dbo.Schedule_Insert",
                p => new
                    {
                        StartTime = p.DateTime(),
                        Enabled = p.Boolean(),
                        Recurrence = p.Byte(),
                        TimezoneAbbreviation = p.String(maxLength: 10),
                        TimezoneOffset = p.Int(),
                        DailyRecurrenceType = p.Byte(),
                        RecurEvery = p.Int(),
                        OnMonday = p.Boolean(),
                        OnTuesday = p.Boolean(),
                        OnWednesday = p.Boolean(),
                        OnThursday = p.Boolean(),
                        OnFriday = p.Boolean(),
                        OnSaturday = p.Boolean(),
                        OnSunday = p.Boolean(),
                        MonthlyRecurrenceType = p.Byte(),
                        DayOfWeekOffset = p.Byte(),
                        DayOfMonth = p.Byte(),
                        YearlyRecurrenceType = p.Byte(),
                        MonthOfYear = p.Byte(),
                    },
                body:
                    @"INSERT [dbo].[Schedule]([StartTime], [Enabled], [Recurrence], [TimezoneAbbreviation], [TimezoneOffset], [DailyRecurrenceType], [RecurEvery], [OnMonday], [OnTuesday], [OnWednesday], [OnThursday], [OnFriday], [OnSaturday], [OnSunday], [MonthlyRecurrenceType], [DayOfWeekOffset], [DayOfMonth], [YearlyRecurrenceType], [MonthOfYear])
                      VALUES (@StartTime, @Enabled, @Recurrence, @TimezoneAbbreviation, @TimezoneOffset, @DailyRecurrenceType, @RecurEvery, @OnMonday, @OnTuesday, @OnWednesday, @OnThursday, @OnFriday, @OnSaturday, @OnSunday, @MonthlyRecurrenceType, @DayOfWeekOffset, @DayOfMonth, @YearlyRecurrenceType, @MonthOfYear)
                      
                      DECLARE @ScheduleID int
                      SELECT @ScheduleID = [ScheduleID]
                      FROM [dbo].[Schedule]
                      WHERE @@ROWCOUNT > 0 AND [ScheduleID] = scope_identity()
                      
                      SELECT t0.[ScheduleID]
                      FROM [dbo].[Schedule] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[ScheduleID] = @ScheduleID"
            );
            
            CreateStoredProcedure(
                "dbo.Schedule_Update",
                p => new
                    {
                        ScheduleID = p.Int(),
                        StartTime = p.DateTime(),
                        Enabled = p.Boolean(),
                        Recurrence = p.Byte(),
                        TimezoneAbbreviation = p.String(maxLength: 10),
                        TimezoneOffset = p.Int(),
                        DailyRecurrenceType = p.Byte(),
                        RecurEvery = p.Int(),
                        OnMonday = p.Boolean(),
                        OnTuesday = p.Boolean(),
                        OnWednesday = p.Boolean(),
                        OnThursday = p.Boolean(),
                        OnFriday = p.Boolean(),
                        OnSaturday = p.Boolean(),
                        OnSunday = p.Boolean(),
                        MonthlyRecurrenceType = p.Byte(),
                        DayOfWeekOffset = p.Byte(),
                        DayOfMonth = p.Byte(),
                        YearlyRecurrenceType = p.Byte(),
                        MonthOfYear = p.Byte(),
                    },
                body:
                    @"UPDATE [dbo].[Schedule]
                      SET [StartTime] = @StartTime, [Enabled] = @Enabled, [Recurrence] = @Recurrence, [TimezoneAbbreviation] = @TimezoneAbbreviation, [TimezoneOffset] = @TimezoneOffset, [DailyRecurrenceType] = @DailyRecurrenceType, [RecurEvery] = @RecurEvery, [OnMonday] = @OnMonday, [OnTuesday] = @OnTuesday, [OnWednesday] = @OnWednesday, [OnThursday] = @OnThursday, [OnFriday] = @OnFriday, [OnSaturday] = @OnSaturday, [OnSunday] = @OnSunday, [MonthlyRecurrenceType] = @MonthlyRecurrenceType, [DayOfWeekOffset] = @DayOfWeekOffset, [DayOfMonth] = @DayOfMonth, [YearlyRecurrenceType] = @YearlyRecurrenceType, [MonthOfYear] = @MonthOfYear
                      WHERE ([ScheduleID] = @ScheduleID)"
            );
            
            CreateStoredProcedure(
                "dbo.Schedule_Delete",
                p => new
                    {
                        ScheduleID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Schedule]
                      WHERE ([ScheduleID] = @ScheduleID)"
            );
            
            AlterStoredProcedure(
                "dbo.Pipeline_Insert",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                        Schedule_ScheduleID = p.Int(),
                    },
                body:
                    @"INSERT [dbo].[Pipeline]([PipeLineID], [Name], [ProjectID], [ParentID], [Schedule_ScheduleID])
                      VALUES (@PipeLineID, @Name, @ProjectID, @ParentID, @Schedule_ScheduleID)
                      
                      SELECT t0.[ParentToPipelineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
            
            AlterStoredProcedure(
                "dbo.Pipeline_Update",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Name = p.String(maxLength: 200),
                        ProjectID = p.Int(),
                        ParentID = p.Int(),
                        Schedule_ScheduleID = p.Int(),
                    },
                body:
                    @"UPDATE [dbo].[Pipeline]
                      SET [Name] = @Name, [ProjectID] = @ProjectID, [ParentID] = @ParentID, [Schedule_ScheduleID] = @Schedule_ScheduleID
                      WHERE ([PipeLineID] = @PipeLineID)
                      
                      SELECT t0.[ParentToPipelineID]
                      FROM [dbo].[Pipeline] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PipeLineID] = @PipeLineID"
            );
            
            AlterStoredProcedure(
                "dbo.Pipeline_Delete",
                p => new
                    {
                        PipeLineID = p.Int(),
                        Schedule_ScheduleID = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[Pipeline]
                      WHERE (([PipeLineID] = @PipeLineID) AND (([Schedule_ScheduleID] = @Schedule_ScheduleID) OR ([Schedule_ScheduleID] IS NULL AND @Schedule_ScheduleID IS NULL)))"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Schedule_Delete");
            DropStoredProcedure("dbo.Schedule_Update");
            DropStoredProcedure("dbo.Schedule_Insert");
            DropForeignKey("dbo.Pipeline", "Schedule_ScheduleID", "dbo.Schedule");
            DropIndex("dbo.Pipeline", new[] { "Schedule_ScheduleID" });
            DropColumn("dbo.Pipeline", "Schedule_ScheduleID");
            DropTable("dbo.Schedule");
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
