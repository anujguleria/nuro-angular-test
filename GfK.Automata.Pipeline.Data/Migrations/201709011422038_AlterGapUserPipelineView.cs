namespace GfK.Automata.Pipeline.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterGapUserPipelineView : DbMigration
    {
        public override void Up()
        {
            Sql("DROP VIEW [dbo].[GapGroupUserPipeline]");

            Sql("CREATE  VIEW [dbo].[GapGroupUserPipeline]  AS " +
                "  WITH projectPipelines AS " +
                "  ( " +
                   "   SELECT projRoot.PipeLineID, projRoot.Name, projRoot.ProjectID, projRoot.ParentID " +
                 "      FROM dbo.Pipeline AS projRoot " +
                "       WHERE projRoot.ProjectID IS NOT NULL " +
                 "      UNION ALL " +
                "      SELECT pipelines.PipeLineID, pipelines.Name, projectPipelines.ProjectID, pipelines.ParentID " +
               "         FROM dbo.Pipeline AS pipelines " +
                "        JOIN projectPipelines " +
                "          ON projectPipelines.PipeLineID = pipelines.ParentID " +
               "        WHERE pipelines.ParentID IS NOT NULL " +
                "   ) " +
               "     SELECT gu.UserID, pr.ProjectID, pip.PipeLineID, gu.GapGroupID " +
               "       FROM projectPipelines as pip " +
                "      JOIN dbo.Project AS pr " +
               "         ON pr.ProjectID = pip.ProjectID " +
               "       JOIN dbo.GapGroupUser AS gu " +
               "         ON gu.GapGroupID = pr.GapGroupID "
                );
        }
        
        public override void Down()
        {
            Sql("DROP VIEW [dbo].[GapGroupUserPipeline]");
        }
    }
}
