﻿using GfK.Automata.Model;

namespace GfK.Automata.Pattern.Service
{
    static public class PatternFactory
    {
        public static GfK.Automata.Pattern.Model.Pattern Find(BoundField field, string language, string context)
        {
            return null;
        }

        public static GfK.Automata.Pattern.Model.Pattern Find(UnboundField field, string language, string context)
        {
            return null;
        }

        public static Model.Pattern BoundFieldDetailTablePattern(BoundField field, string language, string context)
        {
            return null;
        }

        public static Model.Pattern UnboundFieldDetailTablePattern(UnboundField field, string language, string context)
        {
            return null;
        }

    }
}
