﻿using System;
using System.Collections.Generic;
using GfK.Automata.Tabulation.Service;

namespace GfK.Automata.Model.Debug
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Debug\Bagat-10\USC_BV_MOD.mdd";
            MetadataDocument d = MDMReader.Read(path, "{..}", MetadataSourceType.stDimensionsMDD);
            d.Log(@"C:\Debug\Bagat-10\dev.log");         
            d.Serialize(@"C:\Debug\Bagat-10\dev.gz");
            string path2 = @"C:\Debug\Bagat-10\dev.log";
            MetadataDocument doc2 = MetadataDocumentReader.Read(path2);

            string path3 = @"C:\Debug\Bagat-10\dev.txt";

            MetadataReporter.ToText(doc2, path3);            

            Console.Write("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
