REM - QAT call
REM - c:\users\brady.brau\code\gfk.automata\FullRun.bat c:\users\brady.brau\code\gfk.automata qat \\nuew-wwtango01.ext.gfk\d$\web\gap\ https://gapqat.gfk.com

C:\Users\Brady.Brau\bamboo-agent-home\xml-data\build-dir\GAP-GMBP-JOB1\DeployWeb.bat C:\Users\Brady.Brau\bamboo-agent-home\xml-data\build-dir\GAP-GMBP-JOB1 qat \\nuew-wwtango01.ext.gfk\d$\web\gap\qat https://gapqat.gfk.com

call c:\code\path\nuget.exe restore %1\GfK.Automata.sln
if %errorlevel% neq 0 exit /b %errorlevel%

call "E:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\MSBuild.exe" %1\GfK.Automata.sln /p:Configuration=%2
if %errorlevel% neq 0 exit /b %errorlevel%

call "E:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\MSTest.exe" /testcontainer:%1\GfK.Automata.Pipeline.Test\bin\%2\GfK.Automata.Pipeline.Test.dll /usestderr /nologo
if %errorlevel% neq 0 exit /b %errorlevel%

call %1\RunDBMigrations.bat %1 %2
if %errorlevel% neq 0 exit /b %errorlevel%

call %1\DeployWeb.bat %1 %2 %3%2 %4
if %errorlevel% neq 0 exit /b %errorlevel%

call "E:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\MSTest.exe" /testcontainer:%1\GfK.Automata.Pipeline.Test.Functional\bin\%2\GfK.Automata.Pipeline.Test.Functional.dll /usestderr /nologo
if %errorlevel% neq 0 exit /b %errorlevel%