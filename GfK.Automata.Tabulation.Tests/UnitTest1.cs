﻿using System;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;
using GfK.Automata.Tabulation.Service;
using GfK.Automata.Pattern.Model;

using Microsoft.VisualStudio.TestTools.UnitTesting;



namespace GfK.Automata.Tabulation.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {

            string path = @"C:\Debug\Bagat-10\";
            string file = "USC_BV_MOD_qtype";
            string mdd = path + file + ".mdd";
            string json = path + file + ".txt";
            string mrs = path + file + ".mrs";
            MetadataDocument d = MDMReader.Read(mdd, "{..}", MetadataSourceType.stDimensionsMDD);
            d.Log(json);
            ProcessorConfiguration c = new ProcessorConfiguration();
            c.Add("Document", d);
            c.Add("Language", "ENU");
            c.Add("Context", "Analysis");
            c.Add("MRSPath", mrs);
            c.Add("MDMPath", path);
            c.Add("MDSC", "mrScriptMDSC");
            c.Add("MetadataVersion", "{..}");
            c.Add("CaseDataPath", "not-a-real-path.ddf");
            c.Add("CDSC", "mrDataFileDSC");
            c.Add("Project", "");
            c.Add("MTDPath", "also-not-a-real-path.mtd");
            c.Add("TableDoc", "TableDoc");
            //DimTabulationProcessor.Process(c);

            DimTabPlan tp = new DimTabPlan(c);
            MetadataDocument document = (MetadataDocument)c["Document"];
            string language = (string)c["Language"];
            string context = (string)c["Context"];
            string mrsPath = (string)c["MRSPath"];
            DimTabulationProcessor.Process(tp, document, language, context, mrsPath);
            document.Find("Sex");

            Assert.IsTrue(true);
            Assert.AreEqual(3, 4);
            //Console.WriteLine("Press any key to exit...");
            //Console.ReadKey();
        }
    }
}
