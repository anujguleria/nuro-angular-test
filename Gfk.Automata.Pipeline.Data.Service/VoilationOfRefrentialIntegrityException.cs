﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class VoilationOfRefrentialIntegrityException : Exception
    {
        public VoilationOfRefrentialIntegrityException()
        : base() { }
        public VoilationOfRefrentialIntegrityException(string message)
       : base(message) { }

        public VoilationOfRefrentialIntegrityException(string message, Exception innerException)
        : base(message, innerException) { }


    }
}
