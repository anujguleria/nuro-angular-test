﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service.Messaging;
using System.Configuration;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class ScheduleService : IScheduleService
    {
        private readonly IScheduleRepository _scheduleRepository;
        public IUnitOfWork _unitOfWork;
        private readonly IQuartzService _quartzService;
        private readonly IGapMessageQueue _scheduleUpdateQueueManager;

        public event EventHandler<int> ScheduleFired;

        public ScheduleService(IScheduleRepository scheduleRepository, IUnitOfWork unitOfWork, IQuartzService quartzService)
        {
            this._scheduleRepository = scheduleRepository;
            this._unitOfWork = unitOfWork;
            _quartzService = quartzService;
            _quartzService.ScheduleFired += _quartzService_ScheduleFired;
            // TODO - should probably inject this
            QueueAttributes queueAttributes = new QueueAttributes { QueueName = ConfigurationManager.AppSettings.Get("PipelineQueueName") };
            IGapQueue pipelineQueue = new MsmqQueue(queueAttributes);
            _scheduleUpdateQueueManager = new ScheduleUpdateMessageQueue(pipelineQueue, queueAttributes);
        }

        private void _quartzService_ScheduleFired(object sender, int e)
        {
            ScheduleFired(this, e);
            Task<Schedule> scheduleTask = _scheduleRepository.Get(s => s.Pipeline.PipeLineID == e);
            scheduleTask.Wait();
            Schedule schedule = scheduleTask.Result;
            Task task = UpdateNextFireTime(schedule, false);
            task.Wait();
        }

        public async Task<bool> IsScheduledToRunAfter(Schedule schedule, DateTime afterDate)
        {
            bool isScheduled = false;

            if (schedule != null && schedule.Enabled && schedule.NextFireTime != null)
            {
                isScheduled = DateTime.Compare((DateTime)schedule.NextFireTime, afterDate) >= 0;
            }

            return isScheduled;
        }
        private async Task SendScheduleMessage(int id, string action)
        {

            GapQueueMessage gapQueueMessage = new GapQueueMessage();
            gapQueueMessage.ID = id;
            gapQueueMessage.RecipientType = "Schedule";
            gapQueueMessage.Action = action;

            await _scheduleUpdateQueueManager.SendMessageAsync(gapQueueMessage);
        }

        public async Task<IEnumerable<Schedule>> GetSchedules()
        {
            var schedules = await _scheduleRepository.GetAll();
            return schedules;
        }
        public async Task<int> CreateSchedule(Schedule schedule)
        {
            Schedule newSchedule = await _scheduleRepository.Add(schedule);
            await SaveSchedule();
            await SendScheduleMessage(schedule.ScheduleID, "UPDATE");
            return newSchedule.ScheduleID;
        }
        public async Task UpdateNextFireTime(Schedule schedule, bool isUtc)
        {
            schedule.NextFireTime = _quartzService.GetNextFireTime(schedule);
            
            await _scheduleRepository.Update(schedule);
            await SaveSchedule();
        }

        public async Task<Schedule> GetSchedule(int scheduleID)
        {
            return await _scheduleRepository.GetById(scheduleID);
        }

        public async Task<IEnumerable<Schedule>> GetSchedules(Expression<Func<Schedule, bool>> where)
        {
            return await _scheduleRepository.GetMany(where);
        }

        public async Task UpdateSchedule(Schedule schedule)
        {
            await _scheduleRepository.Update(schedule);
            await SaveSchedule();
            await SendScheduleMessage(schedule.ScheduleID, "UPDATE");
        }
        public async Task SaveSchedule()
        {
            await _unitOfWork.Commit();
        }

        public async Task LoadQuartzSchedule(int scheduleID)
        {
            Schedule schedule = await _scheduleRepository.GetById(scheduleID);
            await RemoveQuartzSchedule(schedule);
            await LoadQuartzSchedule(schedule);
        }

        public async Task RemoveQuartzSchedule(int scheduleID)
        {
            Schedule schedule = await _scheduleRepository.GetById(scheduleID);
            await RemoveQuartzSchedule(schedule);
        }

        public async Task LoadQuartzSchedules()
        {
            IEnumerable<Schedule> schedules = await GetSchedules();
            foreach (Schedule schedule in schedules)
            {
                try
                {
                    await LoadQuartzSchedule(schedule);
                }
                catch (Exception e)
                {
                    // ignore this for now until we have a way to log and not kill everything
                }
            }
        }
        public async Task StopQuartzSchedules()
        {
            _quartzService.StopAllTriggers();
        }

        public async Task LoadQuartzSchedule(Schedule schedule)
        {
            if (schedule.Enabled)
            {
                _quartzService.UpdateTrigger(schedule);
                await UpdateNextFireTime(schedule, true);
            }
        }
        
        public async Task RemoveQuartzSchedule(Schedule schedule)
        {
            _quartzService.RemoveTrigger(schedule);
        }
    }
}
