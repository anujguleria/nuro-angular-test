﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPipelineRepository _pipelineRepository;
        private readonly IProjectRepository _projectRepository;

        public NotificationService(INotificationRepository NotificationRepository, IUnitOfWork unitOfWork, IPipelineRepository pipelineRepository,
            IProjectRepository projectRepository)
        {
            _unitOfWork = unitOfWork;
            _notificationRepository = NotificationRepository;
            _pipelineRepository = pipelineRepository;
            _projectRepository = projectRepository;
        }

        public async Task<IEnumerable<Notification>> GetNotifications(int userID)
        {
            IEnumerable<Notification> Notifications = await _notificationRepository.GetMany(n => n.UserID == userID);
            // add pipeline name and project name
            foreach (Notification notification in Notifications)
            {
                await AddProperties(notification);
            }
            return Notifications;
        }


        public async Task<Notification> GetNotificationByID(int NotificationID)
        {
            Notification notification = await _notificationRepository.GetById(NotificationID);
            return notification;
        }

        public async Task AddProperties(Notification notification, string status = null)
        {
            Model.Pipeline pipeline = notification.Pipeline;
            if (pipeline == null)
            {
                pipeline = await _pipelineRepository.GetById(notification.PipelineID);
            }
            notification.ProjectName = await GetPipelineProjectName(pipeline);
            notification.PipelineName = pipeline.Name;
            if (!string.IsNullOrEmpty(status))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(DateTime.UtcNow.ToShortDateString());
                sb.Append(" ");
                sb.Append(notification.ProjectName);
                sb.Append(" ");
                sb.Append(notification.PipelineName);
                sb.Append(" ");
                sb.Append(status);
                sb.Append(".");
                notification.Message = sb.ToString();
            }
        }
        public async Task<string> GetPipelineProjectName(Model.Pipeline pipeline)
        {
            if (pipeline.ProjectID.HasValue && pipeline.project == null)
            {
                pipeline.project = await _projectRepository.GetById(pipeline.ProjectID.Value);
            }
            if (pipeline.project != null)
            {
                return pipeline.project.Name;
            }
            if (pipeline.ParentID.HasValue)
            {
                return await GetPipelineProjectName(await _pipelineRepository.GetById(pipeline.ParentID.Value));
            }
            return null;
        }
        public async Task DeleteNotification(int notificationID)
        {
            await _notificationRepository.Delete(n => n.NotificationID == notificationID);
            await _unitOfWork.Commit();
        }
        public async Task DeleteNotificationOfUser(int userID)
        {
            await _notificationRepository.Delete(n => n.UserID == userID);
        }
        public async Task<Notification> CreateNotification(Notification notification, string status = null)
        {

            Notification created = await _notificationRepository.Add(notification);
            await AddProperties(created, status);
            await _unitOfWork.Commit();
            // remove the user object for now
            created.user = null;
            return created;
        }
    }
}
