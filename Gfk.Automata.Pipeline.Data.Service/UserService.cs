﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IGapRoleUserService _gapRoleUserService;
        private readonly IGapGroupUserService _gapGroupUserService;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUserRepository userRepository, IGapRoleUserService gapRoleUserService, IGapGroupUserService gapGroupUserService, IUnitOfWork unitOfWork)
        {
            this._userRepository = userRepository;
            this._gapRoleUserService = gapRoleUserService;
            this._gapGroupUserService = gapGroupUserService;
            this._unitOfWork = unitOfWork;
        }

        public async Task<bool> IsUsernameInUse(User user)
        {
            User checkUser = await _userRepository.Get(p => p.Email.ToUpper() == user.Email.ToUpper());

            return checkUser != null;
        }

        public async Task<int> CreateUser(User user)
        {
            if ((await IsUsernameInUse(user)))
            {
                return -2;
            }
            else
            {
                User newUser = await _userRepository.Add(user);
                await SaveUser();

                return newUser.UserID;
            }
        }

        public async Task<int> CreateUser(string email)
        {
            User newUser = new User();
            newUser.Email = email;

            return await CreateUser(newUser);
        }

        public async Task<User> GetUser(int userID)
        {
            return await _userRepository.GetById(userID);
        }

        public async Task<User> GetUser(string email)
        {
            return await _userRepository.GetUserByEmail(email);
        }

        public async Task<User> GetUser(Expression<Func<User, bool>> where)
        {
            return await _userRepository.Get(where);
        }

        public async Task<User> GetUserWithGroups(int userID)
        {
            User user = await GetUser(userID);
            if (user != null)
            {
                user.GapGroupUsers = (await _gapGroupUserService.GetUserGroups(userID)).ToList();
            }

            return user;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            var users = await _userRepository.GetAll();
            return users;
        }

        public async Task<IEnumerable<User>> GetUsers(Expression<Func<User, bool>> where)
        {
            IEnumerable<User> users = (await _userRepository.GetMany(where)).OrderBy(p => p.Email);

            return users;
        }

        public async Task<IEnumerable<User>> GetUsersWithRoles()
        {
            IEnumerable<User> users = await GetUsers();
            foreach (User user in users)
            {
                user.GapRoleUsers = (await _gapRoleUserService.GetUserRoles(user.UserID)).ToList();
                user.GapRoleUsers.Where(p => p.GapRole.Name == "Admin").Select(r => new { Name = r.GapRole.Name });
                user.GapGroupUsers = (await _gapGroupUserService.GetUserGroupsBrief(user.UserID)).ToList();
            }

            return users;
        }

        public async Task<bool> IsUserAdmin(int UserID)
        {
            return await _gapRoleUserService.IsUserRole(UserID, "Admin");
        }

        public async Task UpdateUser(User user)
        {
            await _gapGroupUserService.UpdateUsersGroups(user.UserID, user.GapGroupUsers);
            await _gapRoleUserService.UpdateUsersRoles(user.UserID, user.GapRoleUsers);
            await _userRepository.Update(user);
            await SaveUser();
        }

        public async Task UpdateCurrentUser(User currentUser, User updatedUserValues)
        {
            await _gapGroupUserService.UpdateUsersGroups(currentUser.UserID, updatedUserValues.GapGroupUsers);
            await _gapRoleUserService.UpdateUsersRoles(currentUser.UserID, updatedUserValues.GapRoleUsers);
            await _userRepository.Update(currentUser);
            await SaveUser();
        }
        public async Task DeleteUser(User user)
        {
            await _userRepository.Delete(user);
            await SaveUser();
        }

        public async Task DeleteUser(Expression<Func<User, bool>> where)
        {
            await _userRepository.Delete(where);
            await SaveUser();
        }

        public async Task SaveUser()
        {
            await _unitOfWork.Commit();
        }
    }
}
