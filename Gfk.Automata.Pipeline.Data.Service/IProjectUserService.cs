﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IProjectUserService
    {
        Task AddUserToProject(ProjectUser projectUser);
        Task AddProjectToUser(ProjectUser projectUser);
        Task AddProjectToUser(string userEmail, int projectId, DateTime lastAccess);
        Task AddProjectUsers(IEnumerable<ProjectUser> projectUsers);
        Task RemoveUserFromProject(ProjectUser projectUser);
        Task<IEnumerable<int>> GetProjectsIdsByUserId(int userId);
        Task<bool> ProjectBelongsToUser(int projectId, User user);
        Task<IEnumerable<int>> GetProjectsIdsByUser(User user);
        Task<IEnumerable<int>> GetUserIdsByProjectId(int projectID);
        Task<ProjectUser> GetProjectUser(int projectId, User user);
        Task SaveProjectUser();
        Task<DateTime> UpdateProjectUser(string userEmail, int projectId);
    }
}
