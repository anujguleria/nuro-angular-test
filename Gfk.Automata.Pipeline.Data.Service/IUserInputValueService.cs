﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IUserInputValueService
    {
        Task ReplacePipelineTaskInputValues(int pipelineTaskId, ICollection<UserInputValue> replacementInputValues);
        Task DeletePipelineTaskInputValues(int pipelineTaskId);
        Task<ICollection<UserInputValue>> GetPipelineTaskUserInputValues(int pipelineTaskId);
    }
}
