﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IGapRoleUserService
    {
        Task AddUserRoles(int userID, IEnumerable<GapRoleUser> roles);
        Task<IEnumerable<GapRoleUser>> GetUserRoles(int userID);
        Task<bool> IsUserRole(int UserID, string role);
        Task RemoveRolesFromUser(int userID);
        Task UpdateUsersRoles(int userID, IEnumerable<GapRoleUser> rolesToAssign);
        Task SaveGapRoleUser();
    }
}
