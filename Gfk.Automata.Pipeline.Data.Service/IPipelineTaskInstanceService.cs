﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IPipelineTaskInstanceService
    {
        Task<int> AddPipelineTaskInstance(PipelineTaskInstance pipelineTaskInstance);
        Task<PipelineTaskInstance> GetPipelineTaskInstance(int pipelineTaskInstanceId);
        Task<PipelineTaskInstance> GetPipelineTaskInstanceWithModuleLog(int pipelineTaskInstanceId);
        Task UpdatePipelineTaskInstance(PipelineTaskInstance pipelineTaskInstance);
        Task SavePipelineTaskInstance();
    }
}
