﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using GfK.Automata.Pipeline;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Service.Dto;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IPipelineService
    {
        User CurrentlyLoggedInUser { get; set; }


        Task<bool> IsPipelineNameInUse(Model.Pipeline pipeline);
        Task<IEnumerable<Model.Pipeline>> FilterToUserAccessible(IEnumerable<Model.Pipeline> pipelinesToFilter);
        Task<int> AddPipeline(Model.Pipeline pipeline);
        Task<int> AddModulePipeline(string name, int moduleID);
        Task AddPipelineLastRun(Model.Pipeline pipeline);
        Task AddLastRun(IEnumerable<Model.Pipeline> pipelines);
        Task<GfK.Automata.Pipeline.Model.Pipeline> GetPipeline(int pipelineId);
        Task<GfK.Automata.Pipeline.Model.Pipeline> GetModulePipeline(int moduleID);
        Task<GfK.Automata.Pipeline.Model.Pipeline> GetPipeline(string pipelineName, int projectId, int? parentId);
        Task<Model.Pipeline> GetPipeline(string pipelineName, dynamic request);
        Task<IEnumerable<Model.Pipeline>> GetPipelines();
        Task<IEnumerable<Model.Pipeline>> GetPipelinesByProject(int projectId);
        Task<IEnumerable<Model.Pipeline>> GetPipelinesByProjectWithLastRun(int projectId);
        Task<int?> GetPipelineProjectID(int pipelineID);
        Task<IEnumerable<PipelineTask>> GetPipelineAvailableParameters(int pipelineID, string direction);
        Task RunPipeline(int pipelineID, PipelineRunRequestContext runContext);
        Task<bool> UserCanAccessPipeline(int pipelineID);
        Task<bool> UserCanAccessPipeline(Model.Pipeline pipeline);
        Task<bool> NewPipelineParentIsProject(dynamic newPipeline);
        Task<bool> NewPipelineParentIsPipeline(dynamic newPipeline);
        Task SavePipeline();
        Task UpdatePipeline(Model.Pipeline pipeline);
        Task<Model.Pipeline> GetPipelineNoTracking(int id);
        Task DeletePipline(int pipelineID);
    }
}
