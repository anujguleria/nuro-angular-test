﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Clone
{
    public interface IModuleCloneService
    {
        Task<ICollection<Parameter>> CloneModuleParameters(Module fromModule, Module toModule);
        Task<Module> CloneModule(int fromModuleID, string toModuleName);
    }
}
