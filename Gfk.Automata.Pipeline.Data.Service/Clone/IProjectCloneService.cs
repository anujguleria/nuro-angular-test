﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Clone
{
    public interface IProjectCloneService
    {
        User CurrentlyLoggedInUser { get; set; }

        Task<GapGroup> GetGapGroup(string groupName);
        Task<int> CloneProject(int fromProjectID, object context);
    }
}
