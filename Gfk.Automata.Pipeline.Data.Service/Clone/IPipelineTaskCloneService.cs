﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Clone
{
    public interface IPipelineTaskCloneService
    {
        User CurrentlyLoggedInUser { get; set; }

        Task ClonePipelineTasks(int fromPipelineID, int toPipelineID);
        Task ClonePipelineTask(int fromTaskID, int toPipelineID, IEnumerable<PipelineTask> toPipelineSystemTasks);
        Task ClonePipelineTaskInputParameters(IEnumerable<PipelineTaskInputValue> fromInputParameters, PipelineTask toPipelineTask, int toPipelineID);
    }
}
