﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Clone
{
    public interface IPipelineCloneService
    {
        User CurrentlyLoggedInUser { get; set; }

        Task ClonePipeline(Model.Pipeline fromPipeline, string toPipelineName, int? toProjectID, int? toPipelineID);
        Task ClonePipelines(IEnumerable<Model.Pipeline> fromPipelines, int? toProjectID, int? toPipelineID);
        Task CloneProjectPipelines(int fromProjectID, int toProjectID);
    }
}
