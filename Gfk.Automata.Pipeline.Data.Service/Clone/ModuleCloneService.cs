﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Service.Clone
{
    public class ModuleCloneService : IModuleCloneService
    {
        private readonly IModuleService _moduleService;
        private readonly IParameterService _parameterService;

        public ModuleCloneService(IModuleService moduleService, IParameterService parameterService)
        {
            _moduleService = moduleService;
            _parameterService = parameterService;
        }

        public async Task<ICollection<Parameter>> CloneModuleParameters(Module fromModule, Module toModule)
        {
            ICollection<Parameter> toParameters = new List<Parameter>();

            foreach(Parameter fromParameter in fromModule.Parameters)
            {
                Parameter toParameter = new Parameter()
                {
                    Name = fromParameter.Name,
                    Description = fromParameter.Description,
                    Direction = fromParameter.Direction,
                    SequencePosition = fromParameter.SequencePosition,
                    ParameterTypeID = fromParameter.ParameterTypeID,
                    ModuleID = toModule.ModuleID
                };

                toParameters.Add(toParameter);
            }

            await _parameterService.AddParameters(toParameters.AsEnumerable<Parameter>());
            return toParameters;
        }

        public async Task<Module> CloneModule(int fromModuleID, string toModuleName)
        {
            Module fromModule = await _moduleService.GetModule(fromModuleID);

            Module toModule = new Module()
            {
                Name = toModuleName,
                Description = fromModule.Description,
                AssemblyName = fromModule.AssemblyName,
                TypeName = fromModule.TypeName,
                ModuleType = fromModule.ModuleType,
                ModuleGroupID = fromModule.ModuleGroupID
            };

            toModule.Parameters = await CloneModuleParameters(fromModule, toModule);
            return toModule;
        }
    }
}
