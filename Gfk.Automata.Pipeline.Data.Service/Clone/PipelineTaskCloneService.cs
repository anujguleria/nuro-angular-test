﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Service.Clone
{
    public class PipelineTaskCloneService : IPipelineTaskCloneService
    {
        private readonly IGapGroupUserPipelineService _gapGroupUserPipelineService;
        private readonly IPipelineTaskService _pipelineTaskService;
        private readonly IPipelineTaskInputValueService _pipelineTaskInputValueService;
        private readonly IModuleService _moduleService;
        private readonly IModuleCloneService _moduleCloneService;

        private Dictionary<int, int> OldToNewPipelineTaskIDMapping = new Dictionary<int, int>();
        private Dictionary<int, int> OldParameterNewParameterIDMapping = new Dictionary<int, int>();
        private Dictionary<int, int> OlePipelineNewPipelineIDMapping = new Dictionary<int, int>();

        public User CurrentlyLoggedInUser { get; set; }        

        public PipelineTaskCloneService(IGapGroupUserPipelineService gapGroupUserPipelineService, IPipelineTaskService pipelineTaskService
            ,IPipelineTaskInputValueService pipelineTaskInputValueService, IModuleService moduleService, IModuleCloneService moduleCloneService)
        {
            _gapGroupUserPipelineService = gapGroupUserPipelineService;
            _pipelineTaskService = pipelineTaskService;
            _pipelineTaskInputValueService = pipelineTaskInputValueService;
            _moduleService = moduleService;
            _moduleCloneService = moduleCloneService;
        }

        private async Task<int> ResolveClonedID(Dictionary<int, int> mapping, int seekID)
        {
            return (mapping.Keys.Contains(seekID) ? mapping[seekID] : seekID);
        }

        public async Task ClonePipelineTaskInputParameters(IEnumerable<PipelineTaskInputValue> fromInputParameters, PipelineTask toPipelineTask, int toPipelineID)
        {
            ICollection<PipelineTaskInputValue> clonedInputValues = new List<PipelineTaskInputValue>();

            foreach (PipelineTaskInputValue pipelineTaskInputValue in fromInputParameters)
            {
                PipelineTaskInputValue newPipelineTaskInputValue = new PipelineTaskInputValue()
                {
                    PriorPipelineTaskID = OldToNewPipelineTaskIDMapping[pipelineTaskInputValue.PriorPipelineTaskID],
                    PriorPipeLineTaskParameterID = await ResolveClonedID(OldParameterNewParameterIDMapping, pipelineTaskInputValue.PriorPipeLineTaskParameterID),
                    ParameterID = await ResolveClonedID(OldParameterNewParameterIDMapping, pipelineTaskInputValue.ParameterID), 
                    PipelineTaskID = toPipelineTask.PipelineTaskID,
                    PipelineID = toPipelineID, 
                    PriorPipelineID = OlePipelineNewPipelineIDMapping[pipelineTaskInputValue.PriorPipelineID]
                };

                clonedInputValues.Add(newPipelineTaskInputValue);
            }

            toPipelineTask.PipelineTaskInputValues = clonedInputValues;
        }

        private async Task BuildParameterLookUp (Module fromPipelineTaskModule, Module clonedModule)
        {
            ICollection<Parameter> clonedParameters = await _moduleCloneService.CloneModuleParameters(fromPipelineTaskModule, clonedModule);

            foreach(Parameter fromParameter in fromPipelineTaskModule.Parameters)
            {
                int correspondingClonedParameterID = (clonedParameters.Single(p => p.Name == fromParameter.Name)).ParameterID;
                OldParameterNewParameterIDMapping.Add(fromParameter.ParameterID, correspondingClonedParameterID);
            }
        }

        private async Task<PipelineTask> GetSystemTask (int fromModuleID, int fromPipelineID, IEnumerable<PipelineTask> toPipelineSystemTasks)
        {
            Module fromPipelineTaskModule = await _moduleService.GetModule(fromModuleID);
            PipelineTask systemTask = null;

            if (fromPipelineTaskModule.ModuleType.ToUpper() == "PIPELINE") 
            {
                string fromTaskModuleNameSuffix = fromPipelineID.ToString() + "_START";
                systemTask = (fromPipelineTaskModule.Name.ToUpper().EndsWith(fromTaskModuleNameSuffix) ? toPipelineSystemTasks.FirstOrDefault() : toPipelineSystemTasks.LastOrDefault());
                await BuildParameterLookUp(fromPipelineTaskModule, await _moduleService.GetModule(systemTask.ModuleID));
            }

            return systemTask;
        }

        private async Task<PipelineTask> AddClonedPipelineTask (PipelineTask fromPipelineTask, int toPipelineID)
        {
            PipelineTask toPipelineTask = new PipelineTask()
            {
                Name = fromPipelineTask.Name,
                SequencePosition = fromPipelineTask.SequencePosition,
                ModuleID = fromPipelineTask.ModuleID,
                PipeLineID = toPipelineID
            };

            int newPipelineTaskID = await _pipelineTaskService.AddPipelineTask(toPipelineTask);
            toPipelineTask.PipelineTaskID = newPipelineTaskID;

            return toPipelineTask;
        }

        public async Task ClonePipelineTask(int fromTaskID, int toPipelineID, IEnumerable<PipelineTask> toPipelineSystemTasks)
        {
            PipelineTask fromPipelineTask = await _pipelineTaskService.GetPipelineTaskWithParameters(fromTaskID);            

            if (fromPipelineTask != null)
            {
                PipelineTask toPipelineTask = await GetSystemTask(fromPipelineTask.ModuleID, fromPipelineTask.PipeLineID, toPipelineSystemTasks);
                if (toPipelineTask == null)
                {
                    toPipelineTask = await AddClonedPipelineTask(fromPipelineTask, toPipelineID);
                }

                OldToNewPipelineTaskIDMapping.Add(fromPipelineTask.PipelineTaskID, toPipelineTask.PipelineTaskID);

                await ClonePipelineTaskInputParameters(fromPipelineTask.PipelineTaskInputValues, toPipelineTask, toPipelineID);
                await CloneUserInputParameters(fromPipelineTask.UserInputValues, toPipelineTask, toPipelineID);
                await _pipelineTaskService.UpdatePipelineTask(toPipelineTask);
            }
        }

        private async Task CloneUserInputParameters(ICollection<UserInputValue> userInputValues, PipelineTask toPipelineTask, int toPipelineID)
        {
            ICollection<UserInputValue> clonedInputValues = new List<UserInputValue>();

            foreach (UserInputValue userInputValue in userInputValues)
            {
                UserInputValue newUserInputValue = new UserInputValue()
                {
                    ParameterID = await ResolveClonedID(OldParameterNewParameterIDMapping, userInputValue.ParameterID),
                    PipelineTaskID = toPipelineTask.PipelineTaskID,
                    InputValue = userInputValue.InputValue
                };

                clonedInputValues.Add(newUserInputValue);
            }

            toPipelineTask.UserInputValues = clonedInputValues;
        }

        public async Task ClonePipelineTasks(int fromPipelineID, int toPipelineID)
        {
            if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(fromPipelineID, CurrentlyLoggedInUser))
            {
                throw new InvalidOperationException("User does not have access to clone-from pipeline " + fromPipelineID.ToString());
            }
            else if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(fromPipelineID, CurrentlyLoggedInUser))
            {
                throw new InvalidOperationException("User does not have access to clone-to pipeline " + toPipelineID.ToString());
            }
            else
            {
                OlePipelineNewPipelineIDMapping.Add(fromPipelineID, toPipelineID);
                IEnumerable<PipelineTask> toPipelineSystemTasks = (await _pipelineTaskService.GetPipelineTasks(toPipelineID)).OrderBy(p => p.SequencePosition);
                IEnumerable<PipelineTask> fromPipelineTasks = (await _pipelineTaskService.GetPipelineTasks(fromPipelineID)).OrderBy(p => p.SequencePosition);

                foreach (PipelineTask fromPipelineTask in fromPipelineTasks)
                {
                    await ClonePipelineTask(fromPipelineTask.PipelineTaskID, toPipelineID, toPipelineSystemTasks);
                }
            }
        }
    }
}
