﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using Newtonsoft.Json.Linq;

namespace GfK.Automata.Pipeline.Data.Service.Clone
{
    public class ProjectCloneService : IProjectCloneService
    {
        private readonly IGapGroupUserProjectService _gapGroupUserProjectService;
        private readonly IProjectService _projectService;
        private readonly IGapGroupService _gapGroupService;
        private readonly IPipelineCloneService _pipelineCloneService;
        private readonly IGapGroupUserService _gapGroupUserService;

        public User CurrentlyLoggedInUser { get; set; }

        public ProjectCloneService(IGapGroupUserProjectService gapGroupUserProjectService, IProjectService projectService, IGapGroupService gapGroupService
            ,IPipelineCloneService pipelineCloneService, IGapGroupUserService gapGroupUserService)
        {
            _gapGroupUserProjectService = gapGroupUserProjectService;
            _gapGroupService = gapGroupService;               
            _pipelineCloneService = pipelineCloneService;           
            _projectService = projectService;
            _gapGroupUserService = gapGroupUserService;
        }

        public async Task<GapGroup> GetGapGroup(string groupName)
        {
            IEnumerable<GapGroupUser> userGroups = await _gapGroupUserService.GetUserGroups(CurrentlyLoggedInUser.UserID);
            foreach (GapGroupUser gapGroupUser in userGroups)
            {
                if (gapGroupUser.GapGroup.Name == groupName)
                {
                    return gapGroupUser.GapGroup;
                }
            }

            throw new InvalidOperationException("Group " + groupName + " does not exist or user is not assigned.");
        }

        public async Task<int> CloneProject(int fromProjectID, object context)
        {
            _pipelineCloneService.CurrentlyLoggedInUser = CurrentlyLoggedInUser;

            if (! await _gapGroupUserProjectService.UserCanAccessProject(fromProjectID, CurrentlyLoggedInUser))
            {
                throw new InvalidOperationException("User does not have right to clone project " + fromProjectID.ToString());
            }
            else
            {
                JObject contextInformation = JObject.Parse(context.ToString());
                GapGroup gapGroup = await GetGapGroup(contextInformation.Property("Group").Value.ToString());

                Project newProject = new Project() { Name = contextInformation.Property("Name").Value.ToString(), GapGroupID = gapGroup.GapGroupID};
                int newProjectID = await _projectService.CreateProject(newProject);

                await _pipelineCloneService.CloneProjectPipelines(fromProjectID, newProjectID);

                return newProjectID;
            }
        }
    }
}
