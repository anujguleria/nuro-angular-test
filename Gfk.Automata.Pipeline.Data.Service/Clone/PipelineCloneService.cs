﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Service.Clone
{
    public class PipelineCloneService : IPipelineCloneService
    {
        private readonly IGapGroupUserPipelineService _gapGroupUserPipelineService;
        private readonly IGapGroupUserProjectService _gapGroupUserProjectService;
        private readonly IPipelineService _pipelineService;
        private readonly IPipelineTaskCloneService _pipelineTaskCloneService;

        public User CurrentlyLoggedInUser { get; set; }

        public PipelineCloneService(IGapGroupUserPipelineService gapGroupUserPipelineService, IPipelineService pipelineService, IPipelineTaskCloneService pipelineTaskCloneService
            , IGapGroupUserProjectService gapGroupUserProjectService)
        {
            _gapGroupUserPipelineService = gapGroupUserPipelineService;
            _gapGroupUserProjectService = gapGroupUserProjectService;
            _pipelineService = pipelineService;
            _pipelineTaskCloneService = pipelineTaskCloneService;            
        }

        public async Task ClonePipeline(Model.Pipeline fromPipeline, string toPipelineName, int? toProjectID, int? toPipelineID)
        {
            _pipelineTaskCloneService.CurrentlyLoggedInUser = CurrentlyLoggedInUser;

            if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(fromPipeline.PipeLineID, CurrentlyLoggedInUser))
            {
                throw new InvalidOperationException("User does not have right to clone pipeline " + fromPipeline.PipeLineID.ToString());
            }
            else
            {
                Model.Pipeline newPipeline = new Model.Pipeline() { Name = toPipelineName, ProjectID = toProjectID, ParentID = toPipelineID };
                int newPipelineID = await _pipelineService.AddPipeline(newPipeline);
                await _pipelineTaskCloneService.ClonePipelineTasks(fromPipeline.PipeLineID, newPipelineID);

                await ClonePipelines(fromPipeline.Pipelines, toProjectID, newPipelineID);
            }
        }

        public async Task ClonePipelines(IEnumerable<Model.Pipeline> fromPipelines, int? toProjectID, int? toPipelineID)
        {
            foreach (Model.Pipeline fromPipeline in fromPipelines)
            {
                await ClonePipeline(fromPipeline, fromPipeline.Name, toProjectID, toPipelineID);
            }
        }

        public async Task CloneProjectPipelines(int fromProjectID, int toProjectID)
        {
            if (!await _gapGroupUserProjectService.UserCanAccessProject(fromProjectID, CurrentlyLoggedInUser))
            {
                throw new InvalidOperationException("User does not have access to clone-from project " + fromProjectID.ToString());
            }
            else if (!await _gapGroupUserProjectService.UserCanAccessProject(toProjectID, CurrentlyLoggedInUser))
            {
                throw new InvalidOperationException("User does not have access to clone-to project " + toProjectID.ToString());
            }
            else
            {
                IEnumerable<Model.Pipeline> fromPipelines = (await _pipelineService.GetPipelinesByProject(fromProjectID));
                await ClonePipelines(fromPipelines, toProjectID, null);
            }
        }
    }
}
