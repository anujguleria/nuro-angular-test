﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public class MsmqQueue : IGapQueue
    {
        public QueueAttributes QueueAttributes { get; set; }
        private MessageQueue _messageQueue { get; set; }
        public ReceiveCompletedEventHandler MessageReceiveEvtHandler { get; set; } = null;

        public MsmqQueue(QueueAttributes queueAttributes)
        {
            QueueAttributes = queueAttributes;
        }

        public async Task<bool> IsEligibleAllMessages(JObject message)
        {
            return true;
        }

        public async Task<JObject> RetrieveMessageBodyAsJson(Message message)
        {
            return JObject.Parse(message.Body.ToString());
        }

        public async Task CreateQueue()
        {
            /*if (!(await QueueExists()))
            {
                MessageQueue.Create(QueueAttributes.QueueName);
            }*/
        }

        public async Task<bool> QueueExists()
        {
            return true;
            //return QueueAttributes != null && MessageQueue.Exists(QueueAttributes.QueueName);
        }

        public async Task<bool> QueueExists(string queueName)
        {
            return true;
            //return MessageQueue.Exists(queueName);
        }

        public async Task<JObject> GetFormattedMessage(Message message)
        {
            message.Formatter = new JsonMessageFormatter();
            JObject messageContent = await RetrieveMessageBodyAsJson(message);

            return messageContent;
        }

        public async Task<MessageQueue> OpenQueue(bool freshQueueInstance = false)
        {
            if (_messageQueue != null && !freshQueueInstance)
            {
                return _messageQueue;
            }
            else
            {
                _messageQueue = new MessageQueue(QueueAttributes.QueueName);
                _messageQueue.Formatter = new JsonMessageFormatter();
            }

            return _messageQueue;
        }

        public async Task InitializeListener(ReceiveCompletedEventHandler listenerDelegate)
        {
            if (_messageQueue == null)
            {
                await OpenQueue();
            }

            _messageQueue.ReceiveCompleted += listenerDelegate;
        }

        public async Task StartListening()
        {
            _messageQueue.BeginReceive();
        }
        public async Task<JObject> ReadNextMessage()
        {
            JObject formattedMessage = new JObject();

            MessageQueue messageQueue = await OpenQueue();
            if (messageQueue != null)
            {
                using (messageQueue)
                {
                    Message message = messageQueue.Receive(QueueAttributes.waitFor);
                    formattedMessage = await RetrieveMessageBodyAsJson(message);
                }
            }

            return formattedMessage;
        }

        public async Task<ICollection<JObject>> ReadFirstNMessages(Func<JObject, Task<bool>> isEligible, int numberMessagesToRead = -1)
        {
            ICollection<JObject> eligibleMessages = new List<JObject>();
            int numberMessagesRead = 0;

            MessageQueue messageQueue = await OpenQueue();
            if (messageQueue != null)
            {
                using (messageQueue)
                {
                    Message[] messages = messageQueue.GetAllMessages();

                    foreach (Message message in messages)
                    {
                        JObject messageContent = await GetFormattedMessage(message);
                        messageContent.Add("MessageID", message.Id);

                        if (await isEligible(messageContent))
                        {
                            eligibleMessages.Add(messageContent);

                            numberMessagesRead++;
                            if (numberMessagesToRead > 0 && numberMessagesRead == numberMessagesToRead)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            return eligibleMessages;
        }

        public async Task<JObject> ReadNextMessage(Func<JObject, Task<bool>> isEligible)
        {
            ICollection<JObject> eligibleMessages = await ReadFirstNMessages(isEligible, 1);

            return eligibleMessages.First();
        }

        public async Task<ICollection<JObject>> ReadAllMessages()
        {
            ICollection<JObject> eligibleMessages = await ReadAllMessages(IsEligibleAllMessages);

            return eligibleMessages;
        }

        public async Task<ICollection<JObject>> ReadAllMessages(Func<JObject, Task<bool>> isEligible)
        {
            ICollection<JObject> eligibleMessages = await ReadFirstNMessages(isEligible);

            return eligibleMessages;
        }

        public async Task SendMessage(JObject message)
        {
            try
            {
                // MessageQueue not thread safe, always request a new instance
                MessageQueue messageQueue = await OpenQueue(freshQueueInstance: true);

                if (messageQueue != null)
                {
                    using (messageQueue)
                    {
                        messageQueue.Send(message);
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task CloseQueue()
        {
            // Do not need to close MSMQ, references/handles to queue are managed by Using statements
        }

        public async Task<bool> RemoveMessageFromQueue(string messageID)
        {
            bool messageRemoved = false;

            MessageQueue messageQueue = await OpenQueue();
            if (messageQueue != null)
            {
                using (messageQueue)
                {
                    try
                    {
                        messageQueue.ReceiveById(messageID);
                        messageRemoved = true;
                    }
                    catch (Exception ex)
                    {
                        if (ex is InvalidOperationException && ex.Message == "Message requested was not found in the queue specified.")
                        {
                            // Another service pulled it
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
            }

            return messageRemoved;
        }
    }
}
