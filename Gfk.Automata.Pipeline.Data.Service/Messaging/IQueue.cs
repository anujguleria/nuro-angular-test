﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public interface IQueue<T, Q> where T : class
                                  where Q : class
    {
        Task CreateQueue();
        Task<bool> QueueExists(string queueName);
        Task<bool> QueueExists();
        Task<Q> OpenQueue(bool freshQueueInstance);
        Task SendMessage(T message);
        Task<ICollection<T>> ReadFirstNMessages(Func<T, Task<bool>> isEligible, int numberMessagesToRead);
        Task<T> ReadNextMessage();
        Task<T> ReadNextMessage(Func<T, Task<bool>> isEligible);
        Task<ICollection<T>> ReadAllMessages();
        Task<ICollection<T>> ReadAllMessages(Func<T, Task<bool>> isEligible);
        Task<bool> RemoveMessageFromQueue(string messageID);
        Task CloseQueue();
    }
}
