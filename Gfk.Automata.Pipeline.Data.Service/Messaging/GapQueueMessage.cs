﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public class GapQueueMessage
    {
        public GapQueueMessage()
        {

        }

        private GapMessageQueueRecipientType _gapMessageQueueRecipientType;
        private GapQueueMessageAction _gapQueueMessageAction;

        public bool IsFirst { get; set; } = false;
        public bool IsLast { get; set; } = false;
        public int ID { get; set; }
        public int? ParentID { get; set; }
        public int PipelineInstanceID { get; set; }
        public int PipelineTaskInstanceID { get; set; }
        public int? ResumePipelineID { get; set; }
        public int? ResumePipelineTaskID { get; set; }
        public string UniqueStamp { get; set; }
        public string Label { get; set; }
        public IPipelineRunStack PipelineRunStack { get; set; }
        public string RecipientType
        {
            get
            {
                return char.ToUpper(this._gapMessageQueueRecipientType.ToString()[0]) + this._gapMessageQueueRecipientType.ToString().Substring(1).ToLower();
            }
            set
            {
                if (!Enum.TryParse(value.ToUpper(), out _gapMessageQueueRecipientType))
                {
                    throw new InvalidCastException(value.ToString() + " is not a valid recipient type.");
                }
            }
        }
        public string Action
        {
            get
            {
                return char.ToUpper(this._gapQueueMessageAction.ToString()[0]) + this._gapQueueMessageAction.ToString().Substring(1).ToLower();
            }
            set
            {
                if (!Enum.TryParse(value.ToUpper(), out _gapQueueMessageAction))
                {
                    throw new InvalidCastException(value.ToString() + " is not a valid action type.");
                }
            }
        }

        public async static Task<GapMessageType> GetMessageType(dynamic message)
        {
            GapMessageType messageType = GapMessageType.Unknown;

            if (message != null && message.Property("RecipientType") != null && message.Property("Action") != null)
            {
                if (message.Property("RecipientType").Value.ToString() == "Pipeline" && message.Property("Action").Value.ToString() == "Run")
                {
                    messageType = GapMessageType.PipelineRun;
                }
                if (message.Property("RecipientType").Value.ToString() == "Pipelinetask" && message.Property("Action").Value.ToString() == "Run")
                {
                    messageType = GapMessageType.PipelineTaskRun;
                }
                if (message.Property("RecipientType").Value.ToString() == "Pipelinetask" && message.Property("Action").Value.ToString() == "Completed")
                {
                    messageType = GapMessageType.PipelineTaskComplete;
                }
                if (message.Property("RecipientType").Value.ToString().ToUpper() == "SCHEDULE" && message.Property("Action").Value.ToString().ToUpper() == "UPDATE")
                {
                    messageType = GapMessageType.ScheduleUpdate;
                }
            }

            return messageType;
        }
    }
}
