﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public class PipelineRunStack : IPipelineRunStack
    {
        public Stack<int> RunStack { get; set; }
    }
}
