﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public class QueueAttributes
    {
        public string QueueName { get; set; }
        public string Description { get; set; }
        public TimeSpan waitFor { get; set; } = new TimeSpan(0, 0, 20);
    }
}
