﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public interface IGapQueue : IQueue<JObject, MessageQueue>
    {
        Task<JObject> RetrieveMessageBodyAsJson(Message message);
        Task InitializeListener(ReceiveCompletedEventHandler listenerDelegate);
        Task StartListening();
    }
}
