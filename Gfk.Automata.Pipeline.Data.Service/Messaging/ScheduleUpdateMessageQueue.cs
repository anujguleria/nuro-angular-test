﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public class ScheduleUpdateMessageQueue : GapMessageQueueBase
    {
        public ScheduleUpdateMessageQueue(IGapQueue pipelineQueue, QueueAttributes queueAttributes)
            :base(pipelineQueue, queueAttributes)
        {
        }

        public override async Task<bool> CanProcessScheduleMessage(JObject message)
        {
            return message != null && message.Property("RecipientType") != null && message.Property("RecipientType").Value.ToString() == "Schedule";
        }
    }
}
