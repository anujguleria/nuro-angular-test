﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public interface IPipelineRunStack
    {
        Stack<int> RunStack { get; set; } 
    }
}
