﻿using GfK.Automata.Pipeline.Data.Service.Messaging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public class PipelineTaskMessageQueue : GapMessageQueueBase
    {
        public PipelineTaskMessageQueue(IGapQueue pipelineQueue, QueueAttributes queueAttributes)
            : base(pipelineQueue, queueAttributes)
        {
        }

        public override async Task<bool> CanProcessRunMessage(JObject message)
        {
            return message != null && message.Property("RecipientType") != null && message.Property("RecipientType").Value.ToString() == "Pipelinetask"
                && message.Property("Action") != null && message.Property("Action").Value.ToString() == "Run";
        }

        public override async Task<bool> CanProcessCompleteMessage(JObject message)
        {
            return message != null && message.Property("RecipientType") != null && message.Property("RecipientType").Value.ToString() == "Pipelinetask"
                && message.Property("Action") != null && message.Property("Action").Value.ToString() == "Completed";
        }
    }
}
