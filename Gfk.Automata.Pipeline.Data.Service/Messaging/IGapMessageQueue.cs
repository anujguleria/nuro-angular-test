﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public interface IGapMessageQueue
    {
        Task<string> GetQueueName();
        Task<JArray> ConvertPipelineRunStack(GapQueueMessage action);
        Task SendMessageAsync(GapQueueMessage action);
        void SendMessage(GapQueueMessage action);
        Task<ICollection<JObject>> ReadScheduleMessagesAsync();
        ICollection<JObject> ReadScheduleMessages();
        Task<ICollection<JObject>> ReadRunMessagesAsync();
        ICollection<JObject> ReadRunMessages();
        Task<ICollection<JObject>> ReadCompleteMessagesAsync();
        Task<bool> RemoveMessageFromQueue(JObject message);
        ICollection<JObject> ReadCompleteMessages();
        Task<bool> CanProcessRunMessage(JObject message);
        Task<bool> CanProcessCompleteMessage(JObject message);
        Task<bool> CanProcessScheduleMessage(JObject message);
    }
}
