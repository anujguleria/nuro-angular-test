﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public static class Messager
    {
        public static async Task SendMessage(GapQueueMessage gapQueueMessage)
        {
            QueueAttributes queueAttributes = new QueueAttributes { QueueName = ConfigurationManager.AppSettings.Get("PipelineQueueName") };
            IGapQueue pipelineQueue = new MsmqQueue(queueAttributes);

            IGapMessageQueue pipelineMessageQueue = new PipelineMessageQueue(pipelineQueue, queueAttributes);
            await pipelineMessageQueue.SendMessageAsync(gapQueueMessage);
        }

        public async static Task<string> GenerateUniqueStampAsync()
        {
            return DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
    }
}
