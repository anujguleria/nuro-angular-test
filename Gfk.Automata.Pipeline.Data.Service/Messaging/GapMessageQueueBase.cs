﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Messaging
{
    public abstract class GapMessageQueueBase : IGapMessageQueue
    {
        protected readonly IGapQueue _pipelineQueue;
        public QueueAttributes QueueAttributes { get; set; }

        public GapMessageQueueBase(IGapQueue pipelineQueue, QueueAttributes queueAttributes)
        {
            _pipelineQueue = pipelineQueue;
            QueueAttributes = queueAttributes;

            /*Task<bool> queueExists = _pipelineQueue.QueueExists();
            queueExists.Wait();

            if (!queueExists.Result)
            {
                Task createQueue = _pipelineQueue.CreateQueue();
                createQueue.Wait();
            }*/
        }

        public virtual async Task<string> GetQueueName()
        {
            return QueueAttributes.QueueName;
        }

        public virtual async Task<JArray> ConvertPipelineRunStack(GapQueueMessage action)
        {
            JArray runStackArray = new JArray();

            int runStackCount = (action.PipelineRunStack == null ? 0 : action.PipelineRunStack.RunStack.Count);
            for (int i = 0; i <= runStackCount - 1; i++)
            {
                runStackArray.Add((action.PipelineRunStack.RunStack.Pop()));
            }

            return runStackArray;
        }
        public virtual async Task SendMessageAsync(GapQueueMessage action)
        {
            JObject runPipelineMessage = new JObject();
            runPipelineMessage.Add("ID", action.ID);
            runPipelineMessage.Add("RecipientType", action.RecipientType);
            runPipelineMessage.Add("Action", action.Action);
            runPipelineMessage.Add("IsFirst", action.IsFirst.ToString());
            runPipelineMessage.Add("IsLast", action.IsLast.ToString());
            runPipelineMessage.Add("PipelineInstanceID", action.PipelineInstanceID);
            runPipelineMessage.Add("UniqueStamp", action.UniqueStamp);
            runPipelineMessage.Add("RunStack", await ConvertPipelineRunStack(action));

            if (action.ParentID != null)
            {
                runPipelineMessage.Add("ParentID", action.ParentID);
            }

            if (action.ResumePipelineID != null)
            {
                runPipelineMessage.Add("ResumePipelineID", action.ResumePipelineID);
                runPipelineMessage.Add("ResumePipelineTaskID", action.ResumePipelineTaskID);
            }

            await _pipelineQueue.SendMessage(runPipelineMessage);
        }

        public virtual void SendMessage(GapQueueMessage action)
        {
            Task sendRunMessageRequest = SendMessageAsync(action);
            sendRunMessageRequest.Wait();
        }

        public virtual async Task<bool> CanProcessRunMessage(JObject message)
        {
            return false;
        }

        public virtual async Task<bool> CanProcessCompleteMessage(JObject message)
        {
            return false;
        }

        public virtual async Task<bool> CanProcessScheduleMessage(JObject message)
        {
            return false;
        }

        public virtual async Task<ICollection<JObject>> ReadMessages(Func<JObject, Task<bool>> canProcessMessage)
        {
            ICollection<JObject> eligibleMessages = new List<JObject>();

            eligibleMessages = await _pipelineQueue.ReadAllMessages(canProcessMessage);

            return eligibleMessages;
        }

        public virtual async Task<ICollection<JObject>> ReadScheduleMessagesAsync()
        {
            ICollection<JObject> eligibleScheduleMessages = await ReadMessages(CanProcessScheduleMessage);

            return eligibleScheduleMessages;
        }

        public virtual ICollection<JObject> ReadScheduleMessages()
        {
            Task<ICollection<JObject>> eligibleScheduleMessagesRequest = ReadScheduleMessagesAsync();
            eligibleScheduleMessagesRequest.Wait();

            return eligibleScheduleMessagesRequest.Result;
        }


        public virtual async Task<ICollection<JObject>> ReadRunMessagesAsync()
        {
            ICollection<JObject> eligibleRunMessages = await ReadMessages(CanProcessRunMessage);

            return eligibleRunMessages;
        }

        public virtual ICollection<JObject> ReadRunMessages()
        {
            Task<ICollection<JObject>> eligibleRunMessagesRequest = ReadRunMessagesAsync();
            eligibleRunMessagesRequest.Wait();

            return eligibleRunMessagesRequest.Result;
        }

        public virtual async Task<ICollection<JObject>> ReadCompleteMessagesAsync()
        {
            ICollection<JObject> eligibleRunMessages = await ReadMessages(CanProcessCompleteMessage);

            return eligibleRunMessages;
        }

        public virtual ICollection<JObject> ReadCompleteMessages()
        {
            Task<ICollection<JObject>> eligibleRunMessagesRequest = ReadCompleteMessagesAsync();
            eligibleRunMessagesRequest.Wait();

            return eligibleRunMessagesRequest.Result;
        }

        public async Task<bool> RemoveMessageFromQueue(JObject message)
        {
            return await _pipelineQueue.RemoveMessageFromQueue(message.Property("MessageID").Value.ToString());
        }
    }
}
