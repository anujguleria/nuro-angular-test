﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineTaskInstanceModuleLogService : IPipelineTaskInstanceModuleLogService
    {
        private readonly IPipelineTaskInstanceModuleLogRespository _pipelineTaskInstanceModuleLogRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PipelineTaskInstanceModuleLogService(IPipelineTaskInstanceModuleLogRespository pipelineTaskInstanceModuleLogRepository, IUnitOfWork unitOfWork)
        {
            _pipelineTaskInstanceModuleLogRepository = pipelineTaskInstanceModuleLogRepository;
            _unitOfWork = unitOfWork;
        }
        public async Task AddPipelineTaskInstanceModuleLog(PipelineTaskInstanceModuleLog pipelineTaskInstanceModuleLog)
        {
            await _pipelineTaskInstanceModuleLogRepository.Add(pipelineTaskInstanceModuleLog);
            await SavePipelineTaskInstanceModuleLog();
        }

        public async Task<IEnumerable<PipelineTaskInstanceModuleLog>> GetPipelineTaskInstanceModuleLogs(int pipelineInstanceID)
        {
            IEnumerable<PipelineTaskInstanceModuleLog> pipelineTaskInstanceModuleLogs = 
                (await _pipelineTaskInstanceModuleLogRepository.GetMany(p => p.PipelineTaskInstanceId == pipelineInstanceID)).OrderBy(p => p.LogDate);

            return pipelineTaskInstanceModuleLogs;
        }

        public async Task SavePipelineTaskInstanceModuleLog()
        {
            await _unitOfWork.Commit();
        }
    }
}
