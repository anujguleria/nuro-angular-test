﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectUserService _projectUserService;
        private readonly IPipelineService _pipelineService;
        private readonly IGapGroupUserProjectService _gapGroupUserProjectService;
        private readonly IUnitOfWork _unitOfWork;

        public ProjectService(IProjectRepository projectRepository, IUnitOfWork unitOfWork, IProjectUserService projectUserService, IPipelineService pipelineService
            ,IGapGroupUserProjectService gapGroupUserProjectService)
        {
            this._projectRepository = projectRepository;
            this._projectUserService = projectUserService;
            this._pipelineService = pipelineService;
            this._gapGroupUserProjectService = gapGroupUserProjectService;
            this._unitOfWork = unitOfWork;
        }

        public User CurrentlyLoggedInUser { get; set; }

        public async Task<bool> ProjectExists(string projectName)
        {
            Project project = await _projectRepository.GetProjectByName(projectName);
            
            return project != null;
        }

        public async Task<bool> UserCanAccessProject(int projectID)
        {
            Project project = await _projectRepository.GetById(projectID);

            return  await UserCanAccessProject(project);
        }

        public async Task<bool> UserCanAccessProject(Project project)
        {
            bool canAccessProject = false;

            if (project != null && CurrentlyLoggedInUser != null)
            {
                canAccessProject = await _gapGroupUserProjectService.UserCanAccessProject(project, CurrentlyLoggedInUser);
            }

            return canAccessProject;
        }
        public async Task<IEnumerable<Project>> GetProjects(IEnumerable<int> projects)
        {
            List<Project> userProjects = new List<Project>();

            if (projects != null)
            {
                foreach (int projectid in projects)
                {
                    userProjects.Add(await _projectRepository.GetById(projectid));
                }
            }

            return userProjects.AsEnumerable<Project>();
        }

        public async Task<List<Model.Pipeline>> GetProjectPipelines(int projectID)
        {
            if (!await UserCanAccessProject(projectID))
            {
                return null;
            }
            else
            {
                return new List<Model.Pipeline>(await _pipelineService.GetPipelinesByProjectWithLastRun(projectID));
            }

        }
        public async Task<int> CreateProject(Project project)
        {
            Project newProject = await _projectRepository.Add(project);
            await SaveProject();

            return newProject.ProjectID;
        }

        public async Task<Project> GetProject(int projectID)
        {
            Project project = await _projectRepository.GetById(projectID);

            if (await UserCanAccessProject(project))
            {
                return project;
            }
            else
            {
                return null;
            }
        }

        public async Task<Project> GetProject(string projectName)
        {
            Project project = await _projectRepository.GetProjectByName(projectName);

            if (await UserCanAccessProject(project))
            {
                return project;
            }
            else
            {
                return null;
            }
        }

        public async Task<Project> GetProject(Expression<Func<Project, bool>> where)
        {
            Project project = await _projectRepository.Get(where);

            if (await UserCanAccessProject(project))
            {
                return project;
            }
            else
            {
                return null;
            }
        }

        public async Task<IEnumerable<Project>> GetProjectsWithPipelines()
        {
            IEnumerable<Project> projectsWithPipelines = await this.GetUserProjects();

            foreach (Project project in projectsWithPipelines)
            {
                project.pipeLines = await GetProjectPipelines(project.ProjectID);

                // Triggers pulling of project users
                ProjectUser pu = await _projectUserService.GetProjectUser(project.ProjectID, CurrentlyLoggedInUser);
            }
        
            return projectsWithPipelines;
        }
        public async Task<IEnumerable<Project>> GetProjects(Expression<Func<Project, bool>> where)
        {
            IEnumerable<Project> projects = await _projectRepository.GetMany(where);

            ICollection<Project> userProjects = new List<Project>();
            foreach (Project project in projects)
            {
                if (await UserCanAccessProject(project))
                {
                    userProjects.Add(project);
                }
            }

            return userProjects.AsEnumerable() ;
        }
        public async Task<IEnumerable<Project>> GetUserProjects()
        {
            IEnumerable<int> projects = await _gapGroupUserProjectService.GetUserProjectIds(CurrentlyLoggedInUser);
            IEnumerable<Project> userProjects = await GetProjects(projects);

            return userProjects.AsEnumerable<Project>();
        }

        public async Task<IEnumerable<string>> GetGroupProjectNames(int groupID)
        {
            return await _projectRepository.GetGroupProjectNames(groupID);
        }

        public async Task UpdateProject(Project project)
        {
            if (await UserCanAccessProject(project))
            {
                await _projectRepository.Update(project);
            }            
        }

        public async Task DeleteProject(Project project)
        {
            if (await UserCanAccessProject(project))
            {
                await _projectRepository.Delete(project);
            }
        }

        public async Task DeleteProject(Expression<Func<Project, bool>> where)
        {
            IEnumerable<Project> projects = await GetProjects(where);
            foreach (Project project in projects)
            {
                await _projectRepository.Delete(project);
            }
        }

        public async Task SaveProject()
        {
            await _unitOfWork.Commit();
        }
    }
}
