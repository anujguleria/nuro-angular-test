﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class GapRoleUserService : IGapRoleUserService
    {
        private readonly IGapRoleUserRepository _gapRoleUserRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GapRoleUserService(IGapRoleUserRepository gapRoleUserRepository, IUnitOfWork unitOfWork)
        {
            _gapRoleUserRepository = gapRoleUserRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task AddUserRoles(int userID, IEnumerable<GapRoleUser> roles)
        {
            if (roles != null)
            {
                foreach (GapRoleUser gapRoleUser in roles)
                {
                    gapRoleUser.UserID = userID;
                    gapRoleUser.GapRole = null;
                    gapRoleUser.User = null;

                    await _gapRoleUserRepository.Add(gapRoleUser);
                }
            }
        }

        public async Task<bool> IsUserRole(int UserID, string role)
        {
            return await _gapRoleUserRepository.IsUserRole(UserID, role);
        }

        public async Task RemoveRolesFromUser(int userID)
        {
            await _gapRoleUserRepository.Delete(p => p.UserID == userID);
        }

        public async Task UpdateUsersRoles(int userID, IEnumerable<GapRoleUser> rolesToAssign)
        {
            await RemoveRolesFromUser(userID);
            await AddUserRoles(userID, rolesToAssign);
        }

        public async Task<IEnumerable<GapRoleUser>> GetUserRoles(int userID)
        {
            IEnumerable<GapRoleUser> gapRoles = await _gapRoleUserRepository.GetMany(p => p.UserID == userID);

            // The above is pulling in large trees of data for GapRoleUsers, will only pull on demand
            foreach(GapRoleUser gapRoleUser in gapRoles)
            {
                gapRoleUser.GapRole.GapRoleUsers = null;
            }

            return gapRoles;
        }

        public async Task SaveGapRoleUser()
        {
            await _unitOfWork.Commit();
        }
    }
}
