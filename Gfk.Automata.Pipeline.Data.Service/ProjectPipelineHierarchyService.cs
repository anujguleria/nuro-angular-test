﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model.Views;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class ProjectPipelineHierarchyService : IProjectPipelineHierarchyService
    {
        private readonly IProjectPipelineHierarchyRepository _projectPipelineHierarchyRepository;

        public ProjectPipelineHierarchyService(IProjectPipelineHierarchyRepository projectPipelineHierarchyRepository)
        {
            _projectPipelineHierarchyRepository = projectPipelineHierarchyRepository;
        }
        public async Task<ProjectPipelineHierarchy> GetPipelineHierarchyCell(int projectId, int pipelineId)
        {
            ProjectPipelineHierarchy hierarchy = await _projectPipelineHierarchyRepository.GetPipelineHierarchy(projectId, pipelineId);

            return hierarchy;
        }

        public async Task<int?> GetPipelineProjectID(int pipelineID)
        {
            ProjectPipelineHierarchy hierarchyEntry = await _projectPipelineHierarchyRepository.Get(p => p.PipeLineID == pipelineID);

            return (hierarchyEntry == null ? null : hierarchyEntry.ProjectID);
        }
        public async Task<bool> PipelineInProjectPipelineHierarchy(int projectID, int? pipelineID)
        {
            if (pipelineID == null)
            {
                return false;
            }
            else
            {
                ProjectPipelineHierarchy hierarchyEntry = await GetPipelineHierarchyCell(projectID, (int)pipelineID);
                return hierarchyEntry != null;
            }
        }
    }
}
