﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IGapGroupUserService
    {
        Task AddGroupUsers(IEnumerable<GapGroupUser> users);
        Task<IEnumerable<GapGroupUser>> GetGroupUsers(int gapGroupID);
        Task<IEnumerable<GapGroupUser>> GetUserGroups(int userID);
        Task<IEnumerable<GapGroupUser>> GetUserGroupsBrief(int userID);
        Task RemoveUsersFromGroup(int gapGroupID);
        Task RemoveGroupsFromUser(int userID);
        Task UpdateGroupsUsers(int gapGroupID, IEnumerable<GapGroupUser> usersToAssign);
        Task UpdateUsersGroups(int userID, IEnumerable<GapGroupUser> groupsToAssign);
        Task SaveGapGroupUser();
    }
}
