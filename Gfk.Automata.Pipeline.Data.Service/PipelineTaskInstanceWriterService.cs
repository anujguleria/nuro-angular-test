﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using Newtonsoft.Json.Linq;
using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Data.Service.Messaging;
using System.Threading;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineTaskInstanceWriterService : IPipelineTaskInstanceWriterService
    {
        private readonly IPipelineTaskInstanceService _pipelineTaskInstanceService;
        public static SemaphoreSlim WriteLimit = new SemaphoreSlim(1, 1);

        public PipelineTaskInstanceWriterService(IPipelineTaskInstanceService pipelineTaskInstanceService)
        {
            _pipelineTaskInstanceService = pipelineTaskInstanceService;
        }

        public async Task<PipelineTaskInstance> InitializeInstance(string status, DateTime started, Dictionary<string, int> ids, DateTime? ended = default(DateTime?), string message = null)
        {
            PipelineTaskInstance pipelineTaskInstance = new PipelineTaskInstance
            {
                Status = status,
                Started = started,
                Ended = ended,
                Message = message
            };

            pipelineTaskInstance.PipelineTaskID = ids["PipelineTaskID"];
            pipelineTaskInstance.PipelineInstanceId = ids["PipelineInstanceID"];

            return pipelineTaskInstance;
        }

        public async Task<int> AddInstance(PipelineTaskInstance instance)
        {
            int newPipelineTaskInstance = await _pipelineTaskInstanceService.AddPipelineTaskInstance(instance);

            return newPipelineTaskInstance;
        }

        public async Task AddInstanceEndEntry(JObject message, string status, string addlMessage = null)
        {
            int pipelineTaskInstanceID = int.Parse(message.Property("PipelineTaskInstanceID").Value.ToString());

            PipelineTaskInstance pipelineTaskInstance = await _pipelineTaskInstanceService.GetPipelineTaskInstance(pipelineTaskInstanceID);

            pipelineTaskInstance.Status = status;
            pipelineTaskInstance.Ended = await MessageTime.UtcTime();
            pipelineTaskInstance.Message = addlMessage;

            await UpdateInstance(pipelineTaskInstance);
        }

        public async Task<int?> AddInstanceStartEntry(JObject message)
        {
            Dictionary<string, int> ids = new Dictionary<string, int>();

            ids.Add("PipelineTaskID", int.Parse(message.Property("ID").Value.ToString()));
            ids.Add("PipelineInstanceID", int.Parse(message.Property("PipelineInstanceID").Value.ToString()));
            PipelineTaskInstance pipelineTaskInstance = await InitializeInstance("Running", await MessageTime.UtcTime(), ids);

            int newPipelineTaskInstanceId = await _pipelineTaskInstanceService.AddPipelineTaskInstance(pipelineTaskInstance);
            return newPipelineTaskInstanceId;
        }

        public async Task UpdateInstance(PipelineTaskInstance instance)
        {
            //await WriteLimit.WaitAsync().ConfigureAwait(false);

            try
            {
                await _pipelineTaskInstanceService.UpdatePipelineTaskInstance(instance);
            }
            finally
            {
               // WriteLimit.Release();
            }

        }
    }
}
