﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Model.Views;
using GfK.Automata.Pipeline.Model.Domains;
using GfK.Automata.Pipeline.Data.Service.Messaging;
using GfK.Automata.Pipeline.Data.Service.Dto;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineService : IPipelineService
    {
        private readonly IPipelineRepository _pipelineRepository;
        private readonly IPipelineInstanceService _pipelineInstanceService;
        private readonly IProjectPipelineHierarchyService _projectPipelineHierarchyService;
        private readonly IPipelineHierarchyService _pipelineHierarchyService;
        private readonly IGapGroupUserProjectService _gapGroupUserProjectService;
        private readonly IProjectUserService _projectUserService;
        private readonly IGapGroupUserPipelineService _gapGroupUserPipelineService;
        private readonly IModuleService _moduleService;
        private readonly IScheduleService _scheduleService;
        private readonly IPipelineTaskService _pipelineTaskService;
        private readonly IUnitOfWork _unitOfWork;
   
        public PipelineService(IPipelineRepository pipelineRepository, IUnitOfWork unitOfWork, IPipelineInstanceService pipelineInstanceService
            ,IProjectPipelineHierarchyService projectPipelineHierarchyService, IProjectUserService projectUserService, IGapGroupUserPipelineService gapGroupUserPipelineService
            ,IGapGroupUserProjectService gapGroupUserProjectService, IModuleService moduleService, IPipelineTaskService pipelineTaskService
            ,IPipelineHierarchyService pipelineHierarchyService, IScheduleService scheduleService)
        {
            _pipelineRepository = pipelineRepository;
            _pipelineInstanceService = pipelineInstanceService;
            _projectPipelineHierarchyService = projectPipelineHierarchyService;
            _projectUserService = projectUserService;
            _gapGroupUserPipelineService = gapGroupUserPipelineService;
            _gapGroupUserProjectService = gapGroupUserProjectService;
            _moduleService = moduleService;
            _pipelineTaskService = pipelineTaskService;
            _pipelineHierarchyService = pipelineHierarchyService;
            _scheduleService = scheduleService;
            _unitOfWork = unitOfWork;
        }

        public User CurrentlyLoggedInUser { get; set; }


        public async Task<IEnumerable<Model.Pipeline>> FilterToUserAccessible(IEnumerable<Model.Pipeline> pipelinesToFilter)
        {
            ICollection<Model.Pipeline> userPipelines = new List<Model.Pipeline>();

            foreach (Model.Pipeline pipeline in pipelinesToFilter)
            {
                if (await _gapGroupUserPipelineService.UserCanAccessPipeline(pipeline, CurrentlyLoggedInUser))
                {
                    userPipelines.Add(pipeline);
                }
            }

            return userPipelines.AsEnumerable();
        }
        public async Task ClearProjectIDIfDependentPipeline(Model.Pipeline pipeline)
        {
            if (await _projectPipelineHierarchyService.PipelineInProjectPipelineHierarchy((int)pipeline.ProjectID, pipeline.ParentID))
            {
                pipeline.ProjectID = null;
            }
        }
        public async Task<bool> IsPipelineNameInUse(Model.Pipeline pipeline)
        {
            Model.Pipeline seekPipeline = null;

            if (pipeline.ParentID == null)
            {
                seekPipeline = await _pipelineRepository.Get((p => p.ParentToPipelineID == pipeline.ProjectID && p.Name == pipeline.Name));
            }
            else
            {
                seekPipeline = await _pipelineRepository.Get(((p => p.ParentToPipelineID == pipeline.ParentID && p.Name == pipeline.Name)));
            }

            return seekPipeline != null;
        }
        public async Task AddLastRun(IEnumerable<Model.Pipeline> pipelines)
        {
            foreach (Model.Pipeline pipeline in pipelines)
            {
                if (pipeline.Pipelines != null && pipeline.Pipelines.Count > 0)
                {
                    await AddLastRun(pipeline.Pipelines);
                }

                await AddPipelineLastRun(pipeline);
            }
        }
        public async Task AddPipelineLastRun(Model.Pipeline pipeline)
        {
            if (await _gapGroupUserPipelineService.UserCanAccessPipeline(pipeline, CurrentlyLoggedInUser))
            {
                PipelineInstance lastInstance = await _pipelineInstanceService.GetPipelineInstanceLastRun(pipeline.PipeLineID);

                if (lastInstance != null)
                {
                    pipeline.LastRun = lastInstance.Started;
                    pipeline.LastStatus = lastInstance.Status;
                }
            }
        }
        public async Task<int> AddPipeline(Model.Pipeline pipeline)
        {
            if (!await _gapGroupUserProjectService.UserCanAccessProject((int)pipeline.ProjectID, CurrentlyLoggedInUser))
            {
                return -2;
            }
            else
            {
                await ClearProjectIDIfDependentPipeline(pipeline);
            }

            if (await IsPipelineNameInUse(pipeline))
            {
                return -2;
            }

            Model.Pipeline newPipeline = await _pipelineRepository.Add(pipeline);
            await SavePipeline();

            Dictionary<string, int> systemModuleIds = await _moduleService.CreatePipelineSystemModules(newPipeline.Name + "_" + newPipeline.PipeLineID.ToString());
            await _pipelineTaskService.AddSystemPipelineTasks(newPipeline.PipeLineID, systemModuleIds["Start"], systemModuleIds["End"]);
            
            return newPipeline.PipeLineID;
        }

        public async Task<int> AddModulePipeline(string name, int moduleID)
        {
            Model.Pipeline pipeline = await _pipelineRepository.Add(new Model.Pipeline() { Name = name, ModuleID = moduleID });
            await SavePipeline();

            return pipeline.PipeLineID;
        }

        public async Task<GfK.Automata.Pipeline.Model.Pipeline> GetModulePipeline(int moduleID)
        {
            Model.Pipeline pipeline = await _pipelineRepository.Get(p => p.ModuleID == moduleID);
            return pipeline;

            // TODO - how do we determine the current user, do we need this if not exposed in API
            //if (!await _gapGroupUserPipelineService.UserCanAccessPipeline(pipeline, CurrentlyLoggedInUser))
            //{
            //    return null;
            //}
            //else
            //{
            //    return pipeline;
            //}
        }

        public async Task<Model.Pipeline> GetPipeline(int pipelineId)
        {   
            Model.Pipeline pipeline = await _pipelineRepository.GetById(pipelineId);

            if (! await _gapGroupUserPipelineService.UserCanAccessPipeline(pipeline, CurrentlyLoggedInUser))
            {
                return null;
            }
            else
            {
                await AddPipelineLastRun(pipeline);

                return pipeline;
            }
        }

        public async Task<Model.Pipeline> GetPipeline(string pipelineName, dynamic request)
        {
            Model.Pipeline seekPipeline = null;

            seekPipeline = await GetPipeline(pipelineName, int.Parse(request.ProjectID.ToString())
                , (request.ParentID == null ? null : int.Parse(request.ParentID.ToString())));

            return seekPipeline;
        }

        public async Task<int?> GetPipelineProjectID(int pipelineID)
        {
            return await _projectPipelineHierarchyService.GetPipelineProjectID(pipelineID);
        }

        public async Task<Model.Pipeline> GetPipeline(string pipelineName, int projectId, int? parentId)
        {
            Model.Pipeline seekPipeline = null;

            if (parentId == null)
            {
                seekPipeline = await _pipelineRepository.Get((p => p.ParentToPipelineID == projectId && p.Name == pipelineName));
            }
            else
            {
                seekPipeline = await _pipelineRepository.Get(((p => p.ParentToPipelineID == parentId && p.Name == pipelineName)));
            }

            if (await _gapGroupUserPipelineService.UserCanAccessPipeline(seekPipeline, CurrentlyLoggedInUser))
            {
                return seekPipeline;
            }
            {
                return null;
            }
        }

        public async Task<IEnumerable<Model.Pipeline>> GetPipelines()
        {
            IEnumerable<Model.Pipeline> pipelines = await _pipelineRepository.GetAll();

            return await FilterToUserAccessible(pipelines);
        }

        public async Task<Model.Pipeline> GetPipelineNoTracking(int id)
        {
            return await _pipelineRepository.GetNoTracking(id);
        }

        public async Task<IEnumerable<Model.Pipeline>> GetPipelinesByProject(int projectId)
        {
            IEnumerable<Model.Pipeline> pipelines = await _pipelineRepository.GetMany((p => p.ProjectID == projectId));

            IEnumerable<Model.Pipeline> userPipelines = await FilterToUserAccessible(pipelines);
            await AddLastRun(userPipelines);
            return userPipelines;
        }

        public async Task<IEnumerable<Model.Pipeline>> GetPipelinesByProjectWithLastRun(int projectId)
        {
            IEnumerable<Model.Pipeline> pipeLines = await this.GetPipelinesByProject(projectId);

            return pipeLines;
        }

        public async Task<IEnumerable<PipelineTask>> GetPipelineAvailableParameters(int pipelineID, string direction)
        {
            IEnumerable<PipelineTask> availableParameters = new List<PipelineTask>();

            List<PipelineHierarchy> pipelineHierarchy = (await _pipelineHierarchyService.GetPipelineHierarchy(pipelineID)).ToList();
            pipelineHierarchy.RemoveAt(0); // current pipeline first in hierarchy
            availableParameters = await _pipelineTaskService.GetAvailableParameters(pipelineHierarchy, direction);

            return availableParameters;
        }

        private async Task<IPipelineRunStack> GetPipelineHierarchy(int pipelineID, bool towardsParent = true)
        {
            IPipelineRunStack newPipelineRunStack = new PipelineRunStack() { RunStack = new Stack<int>() };

            IEnumerable<PipelineHierarchy> pipelineHierarchy = await _pipelineHierarchyService.GetPipelineHierarchy(pipelineID, towardsParent);
            foreach (PipelineHierarchy pipelineHiearchyLevel in pipelineHierarchy)
            {
                newPipelineRunStack.RunStack.Push((towardsParent ? pipelineHiearchyLevel.HierarchyPipelineID : pipelineHiearchyLevel.PipelineID));
            }
     
            return newPipelineRunStack;
        }

        private async Task<GapQueueMessage> GetRunRequestPipelineList(int pipelineID, PipelineRunRequestContext runContext)
        {
            GapQueueMessage gapQueueMessage = new GapQueueMessage();
            IPipelineRunStack newPipelineRunStack = new PipelineRunStack() { RunStack = new Stack<int>() };

            if (runContext != null && runContext.RunPipelineContext == RunPipelineContext.NONE)
            {
                gapQueueMessage.ID = pipelineID;
            }
            else
            {
                IPipelineRunStack updatedRunStack = await GetPipelineHierarchy(pipelineID
                    ,towardsParent: (runContext == null ? true : (runContext.RunPipelineContext == RunPipelineContext.PARENT)));
                gapQueueMessage.ID = updatedRunStack.RunStack.Pop();
                gapQueueMessage.PipelineRunStack = updatedRunStack;
            }

            return gapQueueMessage;
        }
        public async Task RunPipeline(int pipelineID, PipelineRunRequestContext runContext)
        {
            GapQueueMessage gapQueueMessage = await GetRunRequestPipelineList(pipelineID, runContext);

            gapQueueMessage.RecipientType = "Pipeline";
            gapQueueMessage.Action = "Run";
            gapQueueMessage.UniqueStamp = await Messager.GenerateUniqueStampAsync();
            gapQueueMessage.Label = "Run Pipeline Request";

            await Messager.SendMessage(gapQueueMessage);
        }

        public async Task<bool> NewPipelineParentIsProject(dynamic newPipeline)
        {
            return (newPipeline.Property("ProjectId") != null && newPipeline.Property("ParentPipelineId") == null);
        }

        public async Task<bool> NewPipelineParentIsPipeline(dynamic newPipeline)
        {
            return (newPipeline.Property("ProjectId") != null && newPipeline.Property("ParentPipelineId") != null);
        }

        public async Task SavePipeline()
        {
            await _unitOfWork.Commit();
        }

        public async Task UpdatePipeline(Model.Pipeline pipeline)
        {
            if (await _gapGroupUserPipelineService.UserCanAccessPipeline(pipeline, CurrentlyLoggedInUser))
            {
                Model.Pipeline targetPipeline = await _pipelineRepository.GetById(pipeline.PipeLineID);
                targetPipeline.Name = pipeline.Name;
                targetPipeline.Schedule = pipeline.Schedule;
                await _pipelineRepository.Update(targetPipeline);
                await SavePipeline();
            }
        }

        private async Task<bool> PipelineParametersReferenced (int pipelineID)
        {
            PipelineTask endTask = (await _pipelineTaskService.GetPipelineTasks(pipelineID)).LastOrDefault();
            IEnumerable<Parameter> endTaskParameters = await _moduleService.GetModulesParametersWithMappings(endTask.ModuleID);

            bool hasMappedParameters = false;
            foreach (Parameter parameter in endTaskParameters)
            {
                if (parameter.PipelineTaskInputValues != null && parameter.PipelineTaskInputValues.Count > 0)
                {
                    hasMappedParameters = true;
                    break;
                }
            }

            return hasMappedParameters;
        }

        public async Task DeletePipline(int pipelineID)
        {
            Model.Pipeline deletePipeline = await _pipelineRepository.GetById(pipelineID);

            if (deletePipeline.Pipelines != null && deletePipeline.Pipelines.Count > 0)
            {
                throw new InvalidOperationException("Error occurred while deleting the pipeline, please make sure this pipeline has no children.");
            }                       
            else if (deletePipeline.ModuleID != null)
            {
                throw new InvalidOperationException("Error occurred while deleting the pipeline, please make sure this pipeline is not set as Module.");
            }
            else if (await PipelineParametersReferenced(pipelineID))
            {
                throw new InvalidOperationException("Error occurred while deleting the pipeline, please make sure this pipeline is not referenced by another pipeline.");
            } 
            else if (await _scheduleService.IsScheduledToRunAfter(deletePipeline.Schedule, DateTime.UtcNow))
            {
                throw new InvalidOperationException("Error occurred while deleting the pipeline, please make sure this pipeline isn't scheduled to run.");
            }

            await _pipelineRepository.Delete(deletePipeline);
            await SavePipeline();
        }

        public async Task<bool> UserCanAccessPipeline(int pipelineID)
        {
            Model.Pipeline pipeline = await _pipelineRepository.GetById(pipelineID);
            return await UserCanAccessPipeline(pipeline);
        }

        public async Task<bool> UserCanAccessPipeline(Model.Pipeline pipeline)
        {
            return await _gapGroupUserPipelineService.UserCanAccessPipeline(pipeline, CurrentlyLoggedInUser);
        }
    }
}
