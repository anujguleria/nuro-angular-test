﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IPipelineRunRequestService
    {
        Task<string> GetRunContext(dynamic context);
        Task<int> AddPipelineRunRequest(int piplineID, dynamic context);
        Task<IEnumerable<PipelineRunRequest>> GetPipelineRunRequests();
        Task RemovePipelineRunRequest(PipelineRunRequest pipelineRunRequest);
        Task SavePipelineRunRequest();
    }
}
