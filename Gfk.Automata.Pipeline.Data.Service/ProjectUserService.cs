﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class ProjectUserService : IProjectUserService
    {
        private readonly IProjectUserRepository _projectUserRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;

        public ProjectUserService(IProjectUserRepository projectUserRepository, IUnitOfWork unitOfWork, IUserService userService)
        {
            _projectUserRepository = projectUserRepository;
            _unitOfWork = unitOfWork;
            _userService = userService;
        }
        public async Task AddProjectToUser(ProjectUser projectUser)
        {
            await _projectUserRepository.Add(projectUser);
            await SaveProjectUser();
        }

        public async Task<DateTime> UpdateProjectUser(string userEmail, int projectId)
        {
            DateTime lastAccess = DateTime.UtcNow;
            ProjectUser projectUser = await GetProjectUser(projectId, await _userService.GetUser(userEmail));
            if (projectUser != null)
            {
                projectUser.LastAccess = lastAccess;
                await _projectUserRepository.Update(projectUser);
                await SaveProjectUser();
            }
            else
            {
                await AddProjectToUser(userEmail, projectId, lastAccess);
            }
            return lastAccess;
        }

        public async Task AddProjectToUser(string userEmail, int projectId, DateTime lastAccess)
        {
            User user = await _userService.GetUser(userEmail);

            if (user != null)
            {
                ProjectUser projectUser = new ProjectUser()
                {
                    ProjectID = projectId,
                    UserID = user.UserID,
                    LastAccess = lastAccess
                };

                await AddProjectToUser(projectUser);
            }
        }

        public async Task AddUserToProject(ProjectUser projectUser)
        {
            await _projectUserRepository.Add(projectUser);
            await SaveProjectUser();
        }

        public async Task AddProjectUsers(IEnumerable<ProjectUser> projectUsers)
        {
            if (projectUsers != null)
            {
                foreach(ProjectUser projectUser in projectUsers)
                {
                    await AddProjectToUser(projectUser);
                }
            }
        }
        public async Task<IEnumerable<int>> GetProjectsIdsByUserId(int userId)
        {
            IEnumerable<ProjectUser> projectUsers = await _projectUserRepository.GetProjectsByUserId(userId);

            List<int> projectIds = new List<int>();
            foreach (ProjectUser project in projectUsers)
            {
                projectIds.Add(project.ProjectID);
            }

            return projectIds.AsEnumerable<int>();
        }

        public async Task<IEnumerable<int>> GetProjectsIdsByUser(User user)
        {
            IEnumerable<int> projectIds = null;

            if (user != null)
            {
                projectIds = await GetProjectsIdsByUserId(user.UserID);
            }
            
            return projectIds;
        }

        public async Task<IEnumerable<int>> GetUserIdsByProjectId(int projectID)
        {
            IEnumerable<ProjectUser> projectUsers = await _projectUserRepository.GetMany(p => p.ProjectID == projectID);

            List<int> userIds = new List<int>();
            foreach (ProjectUser user in projectUsers)
            {
                userIds.Add(user.UserID);
            }

            return userIds.AsEnumerable<int>();
        }

        public async Task RemoveUserFromProject(ProjectUser projectUser)
        {
            await _projectUserRepository.Delete(projectUser);
        }

        public async Task SaveProjectUser()
        {
            await _unitOfWork.Commit();
        }

        public async Task<bool> ProjectBelongsToUser(int projectId, User user)
        {
            ProjectUser projectUser = await GetProjectUser(projectId, user);

            return projectUser != null;
        }

        public async Task<ProjectUser> GetProjectUser(int projectId, User user)
        {
            if (user == null)
            {
                return null;
            }
            else
            {
                ProjectUser projectUser = await _projectUserRepository.GetProject(projectId, user);
                return projectUser;
            }
        }
    }
}
