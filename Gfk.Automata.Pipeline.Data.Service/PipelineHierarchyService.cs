﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model.Views;
using GfK.Automata.Pipeline.Data.Repositories;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineHierarchyService : IPipelineHierarchyService
    {
        IPipelineHierarchyRepository _pipelineHierarchyRepository;

        public PipelineHierarchyService(IPipelineHierarchyRepository pipelineHierarchyRepository)
        {
            _pipelineHierarchyRepository = pipelineHierarchyRepository;
        }

        public async Task<IEnumerable<PipelineHierarchy>> GetPipelineHierarchy(int piplinelineID, bool towardsParent = true)
        {
            IEnumerable<PipelineHierarchy> pipelineHierarchy = null;

            if (towardsParent)
            {
                pipelineHierarchy = (await _pipelineHierarchyRepository.GetPipelineParentHierarchy(piplinelineID)).OrderBy(p => p.TreeLevel);
            }
            else
            {
                pipelineHierarchy = (await _pipelineHierarchyRepository.GetPipelineChildrenHierarchy(piplinelineID)).OrderByDescending(p => p.TreeLevel);
            }

            return pipelineHierarchy;
        }
    }
}
