﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IInstanceWriterService<T, M> where T : class
                                           where M : class
    {
        Task<int> AddInstance(T instance);
        Task<T> InitializeInstance(string status, DateTime started, Dictionary<string, int> ids, DateTime? ended = null, string message = null);
        Task<int?> AddInstanceStartEntry(M message);
        Task AddInstanceEndEntry(M message, string status, string addlMessage = null);
        Task UpdateInstance(T instance);
    }
}
