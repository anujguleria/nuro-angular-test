﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model.Views;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class GapGroupUserProjectService : IGapGroupUserProjectService
    {
        private readonly IGapGroupUserProjectRepository _gapGroupUserProjectRepository;

        public GapGroupUserProjectService(IGapGroupUserProjectRepository gapGroupUserProjectRepository)
        {
            _gapGroupUserProjectRepository = gapGroupUserProjectRepository;
        }


        public async Task<IEnumerable<int>> GetUserProjectIds(User user)
        {
            ICollection<int> projectIds = new List<int>();

            if (user != null)
            {
                IEnumerable<GapGroupUserProject> userProjects = await _gapGroupUserProjectRepository.GetMany((p => p.UserID == user.UserID));


                foreach (GapGroupUserProject userProject in userProjects)
                {
                    projectIds.Add(userProject.ProjectID);
                }
            }

            return projectIds;
        }

        public async Task<bool> UserCanAccessProject(Project project, User user)
        {
            bool userCanAccessProject = false;

            if (project != null && user != null)
            {
                userCanAccessProject = await UserCanAccessProject(project.ProjectID, user);
            }

            return userCanAccessProject;
        }

        public async Task<bool> UserCanAccessProject(int projectID, User user)
        {
            bool userCanAccessProject = false;

            if (user != null)
            {
                GapGroupUserProject userProject = await _gapGroupUserProjectRepository.Get(p => p.ProjectID == projectID && p.UserID == user.UserID);
                userCanAccessProject = (userProject != null);
            }

            return userCanAccessProject;
        }
    }
}