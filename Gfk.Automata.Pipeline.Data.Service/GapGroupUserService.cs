﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class GapGroupUserService : IGapGroupUserService
    {
        private readonly IGapGroupUserRepository _gapGroupUserRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GapGroupUserService(IGapGroupUserRepository gapGroupUserRepository, IUnitOfWork unitOfWork)
        {
            _gapGroupUserRepository = gapGroupUserRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task AddGroupUsers(IEnumerable<GapGroupUser> users)
        {
            if (users != null)
            {
                foreach(GapGroupUser gapGroupUser in users)
                {
                    // Leaving these with values incorrectly generates SQL inserts to User/GapGroup. Presumption
                    // is both already exist when populating the intersection table.
                    gapGroupUser.User = null;
                    gapGroupUser.GapGroup = null;

                    await _gapGroupUserRepository.Add(gapGroupUser);
                }
            }
        }

        public async Task<IEnumerable<GapGroupUser>> GetGroupUsers(int gapGroupID)
        {
            IEnumerable<GapGroupUser> gapGroupUsers = (await _gapGroupUserRepository.GetMany(p => p.GapGroupID == gapGroupID)).OrderBy(p => p.User.Email);

            return gapGroupUsers;
        }

        public async Task<IEnumerable<GapGroupUser>> GetUserGroups(int userID)
        {
            IEnumerable<GapGroupUser> gapGroupUsers = (await _gapGroupUserRepository.GetMany(p => p.UserID == userID));//.OrderBy(p => p.GapGroup.Name);

            return gapGroupUsers;
        }

        public async Task<IEnumerable<GapGroupUser>> GetUserGroupsBrief(int userID)
        {
            IEnumerable<GapGroupUser> gapGroupUsers = await GetUserGroups(userID);
            
            if (gapGroupUsers != null)
            {
                foreach(GapGroupUser gapGroupUser in gapGroupUsers)
                {
                    gapGroupUser.GapGroup.GapGroupUsers = null;
                }
            }

            return gapGroupUsers;
        }
        public async Task UpdateUsersGroups(int userID, IEnumerable<GapGroupUser> groupsToAssign)
        {
            await RemoveGroupsFromUser(userID);
            await AddGroupUsers(groupsToAssign);
        }

        public async Task UpdateGroupsUsers(int gapGroupID, IEnumerable<GapGroupUser> usersToAssign)
        {
            await RemoveUsersFromGroup(gapGroupID);
            await AddGroupUsers(usersToAssign);
        }

        public async Task RemoveUsersFromGroup(int gapGroupID)
        {
            await _gapGroupUserRepository.Delete(p => p.GapGroupID == gapGroupID);
        }

        public async Task RemoveGroupsFromUser(int userID)
        {
            await _gapGroupUserRepository.Delete(p => p.UserID == userID);
        }

        public async Task SaveGapGroupUser()
        {
            await _unitOfWork.Commit();
        }
    }
}
