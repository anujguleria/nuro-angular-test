﻿using GfK.Automata.Pipeline.Data.Service;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public abstract class DatabaseLoggerBase
    {
        private readonly IPipelineTaskInstanceModuleLogService _pipelineTaskInstanceModuleLogService;
        public static SemaphoreSlim loggerGateway = new SemaphoreSlim(1, 1);

        public int PipelineTaskInstanceID { get; set; }

        public DatabaseLoggerBase(IPipelineTaskInstanceModuleLogService pipelineTaskInstanceModuleLogService)
        {
            _pipelineTaskInstanceModuleLogService = pipelineTaskInstanceModuleLogService;
        }

        public virtual void Log(ModuleLogMessageType level, string message)
        {
            Task addMessage = LogAsync(level, message);
            addMessage.Wait();
        }

        public virtual async Task LogAsync(ModuleLogMessageType level, string message)
        {
            await loggerGateway.WaitAsync().ConfigureAwait(false);

            try
            {
                PipelineTaskInstanceModuleLog pipelineTaskInstanceModuleLog = new PipelineTaskInstanceModuleLog()
                {
                    LogMessage = message,
                    LogMessageType = level.ToString(),
                    LogDate = DateTime.UtcNow,
                    PipelineTaskInstanceId = PipelineTaskInstanceID
                };

                await _pipelineTaskInstanceModuleLogService.AddPipelineTaskInstanceModuleLog(pipelineTaskInstanceModuleLog);
            }
            finally
            {
                loggerGateway.Release();
            }
        }
    }
}