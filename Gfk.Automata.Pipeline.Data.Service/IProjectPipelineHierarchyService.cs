﻿using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IProjectPipelineHierarchyService
    {
        Task<ProjectPipelineHierarchy> GetPipelineHierarchyCell(int projectId, int pipelineId);
        Task<bool> PipelineInProjectPipelineHierarchy(int projectID, int? pipelineID);
        Task<int?> GetPipelineProjectID(int pipelineID);
    }
}
