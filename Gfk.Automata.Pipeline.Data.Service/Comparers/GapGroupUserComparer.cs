﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Comparers
{
    public class GapGroupUserComparer : IEqualityComparer<GapGroupUser>
    {
        public bool Equals(GapGroupUser x, GapGroupUser y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if ((x == null && y != null) || (x != null && y == null))
            {
                return false;
            }
            else
            {
                return (x.GapGroupID == y.GapGroupID) && (x.UserID == y.UserID);
            }
        }

        public int GetHashCode(GapGroupUser obj)
        {
            throw new NotImplementedException();
        }
    }
}
