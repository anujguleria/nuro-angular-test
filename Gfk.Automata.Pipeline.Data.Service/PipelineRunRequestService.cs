﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineRunRequestService : IPipelineRunRequestService
    {
        private readonly IPipelineRunRequestRepository _pipelineRunRequestRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PipelineRunRequestService(IPipelineRunRequestRepository pipelineRunRequestRepository, IUnitOfWork unitOfWork)
        {
            _pipelineRunRequestRepository = pipelineRunRequestRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<string> GetRunContext(dynamic context)
        {
            string runParentPipelines = "PARENT";

            if (context.Property("RunPipelineContext") != null)
            {
                runParentPipelines = context.RunPipelineContext.ToString().Trim();
            }

            return runParentPipelines;
        }
        public async Task<int> AddPipelineRunRequest(int pipelineID, dynamic context)
        {
            PipelineRunRequest pipelineRunRequest = new PipelineRunRequest()
            {
                PipelineID = pipelineID,
                RunRequestDate = DateTime.UtcNow,
                RunPipelineContext = await GetRunContext(context)
            };

            PipelineRunRequest newPipelineRunRequest = await _pipelineRunRequestRepository.Add(pipelineRunRequest);
            await SavePipelineRunRequest();

            return newPipelineRunRequest.PipelineRunRequestID;
        }

        public async Task<IEnumerable<PipelineRunRequest>> GetPipelineRunRequests()
        {
            return await _pipelineRunRequestRepository.GetAll();
        }

        public async Task RemovePipelineRunRequest(PipelineRunRequest pipelineRunRequest)
        {
            if (pipelineRunRequest != null)
            {
                await _pipelineRunRequestRepository.Delete(pipelineRunRequest);
                await SavePipelineRunRequest();
            }            
        }

        public async Task SavePipelineRunRequest()
        {
            await _unitOfWork.Commit();
        }
    }
}
