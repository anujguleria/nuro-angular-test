﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IModuleService
    {
        Task<IEnumerable<Module>> GetModules();
        Task<IEnumerable<Module>> GetModules(Expression<Func<Module, bool>> where);
        Task<Module> GetModule(int moduleID);
        Task<IEnumerable<Parameter>> GetModulesParametersWithMappings(int moduleID);
        Task<int> CreateCustomModule(Module module);
        Task<Module> CreateCustomModuleStart(string rootName);
        Task<Module> CreateCustomModuleEnd(string rootName);
        Task<Dictionary<string, int>> CreatePipelineSystemModules(string rootName);
        Task<int> CreateModule(Module module);
        Task UpdateModuleParameters(int moduleID, dynamic updates);
        Task ProcessParameterAction(string action, Module targetModule, ICollection<Parameter> parameters);
        Task DeleteParameters(ICollection<Parameter> parameters);
        Task UpdateParameters(Module targetModule, ICollection<Parameter> parameters);
        Task AddParameters(Module targetModule, ICollection<Parameter> parameters);
        Task<ICollection<Parameter>> GetModificationParameters(dynamic modificationParameters);
        Task SaveModule();
    }
}
