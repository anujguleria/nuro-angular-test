﻿using GfK.Automata.Pipeline.Model;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IQuartzService
    {
        event EventHandler<int> ScheduleFired;
        ITrigger GetTrigger(Schedule schedule);

        ITrigger GetYearlyTrigger(Schedule schedule);

        ITrigger GetMonthlyTrigger(Schedule schedule);

        ITrigger GetWeeklyTrigger(Schedule schedule);

        ITrigger GetDailyTrigger(Schedule schedule);
        TriggerBuilder GetBaseTrigger(Schedule schedule);
        void RemoveTrigger(Schedule schedule);
        void UpdateTrigger(Schedule schedule);
        void StopAllTriggers();
        DateTime? GetNextFireTime(Schedule schedule);
        ITrigger GetNoneTrigger(Schedule schedule);
    }
}
