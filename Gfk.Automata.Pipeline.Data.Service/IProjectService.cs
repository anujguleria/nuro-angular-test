﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IProjectService
    {
        User CurrentlyLoggedInUser { get; set; }

        Task<int> CreateProject(Project project);
        Task UpdateProject(Project project);
        Task DeleteProject(Project project);
        Task DeleteProject(Expression<Func<Project, bool>> where);
        Task<Project> GetProject(int projectID);
        Task<bool> ProjectExists(string projectName);
        Task<Project> GetProject(string projectName);
        Task<Project> GetProject(Expression<Func<Project, bool>> where);
        Task<IEnumerable<Project>> GetProjectsWithPipelines();
        Task<List<Model.Pipeline>> GetProjectPipelines(int projectID);
        Task<IEnumerable<Project>> GetProjects(Expression<Func<Project, bool>> where);
        Task<IEnumerable<string>> GetGroupProjectNames(int groupID);
        Task<IEnumerable<Project>> GetUserProjects();
        Task<bool> UserCanAccessProject(int projectID);
        Task<bool> UserCanAccessProject(Project project);
        Task SaveProject();
    }
}
