﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class GapPipelineTaskService : IGapPipelineTaskService
    {
        private readonly IPipelineTaskRepository _pipelineTaskRepository;
        private readonly IModuleService _moduleService;

        public GapPipelineTaskService(IPipelineTaskRepository pipelineTaskRepository, IModuleService moduleService)
        {
            _pipelineTaskRepository = pipelineTaskRepository;
            _moduleService = moduleService;
        }

        public async Task<Module> GetPipelineTaskModule(int pipelineTaskID)
        {
            PipelineTask pipelineTask = await _pipelineTaskRepository.Get(p => p.PipelineTaskID == pipelineTaskID);
            Module module = await _moduleService.GetModule(pipelineTask.ModuleID);

            return module;
        }
    }
}
