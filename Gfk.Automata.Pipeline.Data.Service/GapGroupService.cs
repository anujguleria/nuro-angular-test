﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Service.Comparers;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class GapGroupService : IGapGroupService
    {
        private readonly IGapGroupRepository _gapGroupRepository;
        private readonly IGapGroupUserService _gapGroupUserService;
        private readonly IProjectService _projectService;
        private readonly IUnitOfWork _unitOfWork;

        public GapGroupService(IGapGroupRepository gapGroupRepository, IGapGroupUserService gapGroupUserService, IProjectService projectService, IUnitOfWork unitOfWork)
        {
            _gapGroupRepository = gapGroupRepository;
            _gapGroupUserService = gapGroupUserService;
            _projectService = projectService;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> IsGapGroupNameInUse(GapGroup gapGroup)
        {
            GapGroup checkGapGroup = await _gapGroupRepository.Get(p => p.Name.ToUpper() == gapGroup.Name.ToUpper());

            return checkGapGroup != null;
        }

        public async Task<int> AddGapGroup(GapGroup gapGroup)
        {
            if (await IsGapGroupNameInUse(gapGroup))
            {
                return -2;
            }
            else
            {
                GapGroup newGapGroup = await _gapGroupRepository.Add(gapGroup);
                await SaveGapGroup();

                return newGapGroup.GapGroupID;
            }
        }

        public async Task<GapGroup> GetGapGroup(int id)
        {
            GapGroup gapGroup = await _gapGroupRepository.GetById(id);

            return gapGroup;
        }

        public async Task<GapGroup> GetGapGroup(string gapName)
        {
            GapGroup gapGroup = await _gapGroupRepository.Get(p => p.Name == gapName);

            return gapGroup;
        }

        public async Task<GapGroup> GetGapGroupWithUsers(int id)
        {
            GapGroup gapGroup = await GetGapGroup(id);
            if (gapGroup != null)
            {
                gapGroup.GapGroupUsers = (await _gapGroupUserService.GetGroupUsers(gapGroup.GapGroupID)).ToList();
            }

            return gapGroup;
        }
        public async Task<IEnumerable<GapGroup>> GetGapGroups()
        {
            IEnumerable<GapGroup> gapGroups = (await _gapGroupRepository.GetAll()).OrderBy(p => p.Name);

            return gapGroups;
        }

        public async Task<IEnumerable<string>> GetGapGroupProjectNames(GapGroup gapGroup)
        {
            return await _projectService.GetGroupProjectNames(gapGroup.GapGroupID);
        }

        public async Task<bool> GapGroupHasProjects(GapGroup gapGroup)
        {
            return (await GetGapGroupProjectNames(gapGroup)).Count() > 0;
        }

        public async Task UpdateGapGroup(GapGroup gapGroup)
        {
            await _gapGroupUserService.UpdateGroupsUsers(gapGroup.GapGroupID, gapGroup.GapGroupUsers.AsEnumerable());
            await _gapGroupRepository.Update(gapGroup);
            await SaveGapGroup();
        }

        public async Task DeleteGroup(GapGroup gapGroup)
        {
            await _gapGroupRepository.Delete(gapGroup);
            await SaveGapGroup();
        }

        public async Task SaveGapGroup()
        {
            await _unitOfWork.Commit();
        }
    }
}
