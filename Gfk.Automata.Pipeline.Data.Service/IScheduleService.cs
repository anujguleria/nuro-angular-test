﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IScheduleService
    {
        Task<int> CreateSchedule(Schedule schedule);
        Task UpdateSchedule(Schedule schedule);
        Task<Schedule> GetSchedule(int scheduleID);
        Task LoadQuartzSchedule(int scheduleID);
        Task RemoveQuartzSchedule(int scheduleID);
        Task LoadQuartzSchedule(Model.Schedule schedule);
        Task RemoveQuartzSchedule(Model.Schedule schedule);
        Task LoadQuartzSchedules();
        Task StopQuartzSchedules();
        event EventHandler<int> ScheduleFired;
        Task<IEnumerable<Schedule>> GetSchedules(Expression<Func<Schedule, bool>> where);
        Task<bool> IsScheduledToRunAfter(Schedule schedule, DateTime afterDate);
    }
}
