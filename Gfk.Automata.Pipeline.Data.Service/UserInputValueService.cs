﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class UserInputValueService : IUserInputValueService
    {
        private readonly IUserInputValueRepository _userInputValueRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserInputValueService(IUserInputValueRepository userInputValueRepository, IUnitOfWork unitOfWork)
        {
            _userInputValueRepository = userInputValueRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task DeletePipelineTaskInputValues(int pipelineTaskId)
        {
            await _userInputValueRepository.DeletePipelineTaskInputs(pipelineTaskId);
        }

        public async Task ReplacePipelineTaskInputValues(int pipelineTaskId, ICollection<UserInputValue> replacementInputValues)
        {
            await DeletePipelineTaskInputValues(pipelineTaskId);

            if (replacementInputValues != null)
            {
                foreach (UserInputValue replacementInputValue in replacementInputValues)
                {
                    replacementInputValue.PipelineTaskID = pipelineTaskId;
                    await _userInputValueRepository.AddPipelineTaskInputValue(replacementInputValue);
                }
            }
        }
        public Task<ICollection<UserInputValue>> GetPipelineTaskUserInputValues(int pipelineTaskId)
        {
            return _userInputValueRepository.GetPipelineTaskUserInputValues(pipelineTaskId);
        }
    }
}
