﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineTaskInputValueService : IPipelineTaskInputValueService
    {
        private readonly IPipelineTaskInputValueRepository _pipelineTaskInputValueRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PipelineTaskInputValueService(IPipelineTaskInputValueRepository pipelineTaskInputValueRepository, IUnitOfWork unitOfWork)
        {
            _pipelineTaskInputValueRepository = pipelineTaskInputValueRepository;
            _unitOfWork = unitOfWork;
        }
        public async Task DeletePipelineTaskInputValues(int pipelineTaskId)
        {
            await _pipelineTaskInputValueRepository.DeletePipelineTaskInputs(pipelineTaskId);
        }
        public async Task<ICollection<PipelineTaskInputValue>> GetDependentPipelineTaskInputs(int pipelineTaskId)
        {
            return await _pipelineTaskInputValueRepository.GetDependentPipelineTaskInputs(pipelineTaskId);
        }
        public async Task ReplacePipelineTaskInputValues(int pipelineTaskId, ICollection<PipelineTaskInputValue> replacementInputValues,
            int pipelineId)
        {
            await DeletePipelineTaskInputValues(pipelineTaskId);

            if (replacementInputValues != null)
            {
                AddLookupsToInputValues(replacementInputValues, pipelineId);
                foreach (PipelineTaskInputValue replacementInputValue in replacementInputValues)
                {
                    replacementInputValue.PipelineTaskID = pipelineTaskId;
                    await _pipelineTaskInputValueRepository.AddPipelineTaskInputValue(replacementInputValue);
                }
            }
        }
        public void AddLookupsToInputValues(ICollection<PipelineTaskInputValue> replacementInputValues, int pipelineId)
        {
            if (replacementInputValues != null)
            {
                foreach (PipelineTaskInputValue replacementInputValue in replacementInputValues)
                {
                    replacementInputValue.PipelineID = pipelineId;
                }
            }
        }

        public async Task<ICollection<PipelineTaskInputValue>> GetDependentParameterMappings(int parameterID)
        {
            IEnumerable<PipelineTaskInputValue> dependentMappings = await _pipelineTaskInputValueRepository.GetMany(p => p.PriorPipeLineTaskParameterID == parameterID);

            return (ICollection<PipelineTaskInputValue>)dependentMappings;
        }
    }
}
