﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;
using System.Linq.Expressions;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class ModuleService : IModuleService
    {
        private readonly IModuleRepository _moduleRepository;
        //private readonly IPipelineService _pipelineService;
        private readonly IPipelineTaskService _pipelineTaskService;
        private readonly IParameterService _parameterService;
        private readonly IUnitOfWork _unitOfWork;

        public ModuleService(IModuleRepository moduleRepository, IPipelineTaskService pipelineTaskService, IUnitOfWork unitOfWork, IParameterService parameterService)
        {
            _unitOfWork = unitOfWork;
            _moduleRepository = moduleRepository;
            _parameterService = parameterService;
            //_pipelineService = pipelineService;
            _pipelineTaskService = pipelineTaskService;
        }

        public async Task<List<Parameter>> GetCustomModuleParameters(Module module, string seekDirection)
        {
            List<Parameter> parameters = new List<Parameter>();

            if (module.Parameters != null)
            {
                foreach (Parameter parameter in module.Parameters)
                {
                    if (String.Compare(parameter.Direction,seekDirection, true)  == 0 || String.Compare(parameter.Direction, "INOUT", true) == 0)
                    {
                        Parameter assignParameter = new Parameter()
                        {
                            Name = parameter.Name,
                            Description = parameter.Description,
                            Direction = "INOUT",
                            SequencePosition = parameter.SequencePosition,
                            ParameterTypeID = parameter.ParameterTypeID
                        };
                        parameters.Add(assignParameter);
                    }
                }
            }

            return parameters;
        }

        public async Task<int> CreateModule(Module module)
        {
            Module newModule = await _moduleRepository.Add(module);
            await SaveModule();

            return newModule.ModuleID;
        }

        public async Task<Module> CreateCustomModuleStart(string rootName)
        {
            Module startModule = new Module();
            startModule.Name = rootName + "_Start";
            startModule.Description = "Start module for " + rootName;
            startModule.ModuleGroupID = 1; // module.ModuleGroupID;
            startModule.ModuleType = "PIPELINE";
            startModule.AssemblyName = "GfK.Automata.Pipeline.GapModule.System";
            startModule.TypeName = "GfK.Automata.Pipeline.GapModule.System.Start";         

            return startModule;
        }

        public async Task<Module> CreateCustomModuleEnd(string rootName)
        {
            Module endModule = new Module();
            endModule.Name = rootName + "_End";
            endModule.Description = "End module for " + rootName;
            endModule.ModuleGroupID = 1; // module.ModuleGroupID;
            endModule.ModuleType = "PIPELINE";
            endModule.AssemblyName = "GfK.Automata.Pipeline.GapModule.System";
            endModule.TypeName = "GfK.Automata.Pipeline.GapModule.System.End";

            return endModule;
        }

        public async Task<Dictionary<string, int>> CreatePipelineSystemModules(string rootName)
        {
            Dictionary<string, int> systemModuleIds = new Dictionary<string, int>();

            Module startModule = await _moduleRepository.Add(await CreateCustomModuleStart(rootName));
            Module endModule = await _moduleRepository.Add(await CreateCustomModuleEnd(rootName));
            await SaveModule();

            systemModuleIds.Add("Start", startModule.ModuleID);
            systemModuleIds.Add("End", endModule.ModuleID);

            return systemModuleIds;
        }

        public async Task AddCustomModuleMasterAssemblyNames(Module module)
        {
            module.AssemblyName = module.Name + "_Assembly";
            module.TypeName = module.Name + "_Assembly.Master";
        }

        public async Task<int> CreateCustomModule(Module module)
        {
            Module startModule = await _moduleRepository.Add(await CreateCustomModuleStart(""));
            Module endModule = await _moduleRepository.Add(await CreateCustomModuleEnd(""));

            await AddCustomModuleMasterAssemblyNames(module);
            Module masterModule = await _moduleRepository.Add(module);
            await SaveModule();

            //int pipelineID = await _pipelineService.AddModulePipeline(module.Name + "_Pipeline", masterModule.ModuleID);
            //await _pipelineTaskService.AddSystemPipelineTasks(pipelineID, startModule.ModuleID, endModule.ModuleID);

            return masterModule.ModuleID;
        }

        public async Task<IEnumerable<Parameter>> GetModulesParametersWithMappings(int moduleID)
        {
            ICollection<Parameter> parameters = await _parameterService.GetModuleParmeters(moduleID);

            return (IEnumerable<Parameter>)parameters;
        }

        public async Task<Module> GetModule(int moduleID)
        {
            Module module = await _moduleRepository.Get((p => p.ModuleID == moduleID));

            return module;
        }

        public async Task<IEnumerable<Module>> GetModules()
        {
            IEnumerable<Module> modules = await _moduleRepository.GetAll();

            return modules;
        }
        public async Task<IEnumerable<Module>> GetModules(Expression<Func<Module, bool>> where)
        {
            IEnumerable<Module> modules = await _moduleRepository.GetMany(where);

            return modules;
        }

        public async Task<ICollection<Parameter>> GetModificationParameters(dynamic modificationParameters)
        {
            ICollection<Parameter> targetParameters = new List<Parameter>();

            foreach (dynamic parameter in modificationParameters)
            {
                Parameter target = Newtonsoft.Json.JsonConvert.DeserializeObject<Parameter>(parameter.ToString());
                targetParameters.Add(target);
            }

            return targetParameters;
        }

        public async Task AddParameters(Module targetModule, ICollection<Parameter> parameters)
        {
            if (targetModule.Parameters == null || targetModule.Parameters.Count == 0)
            {
                targetModule.Parameters = parameters;
            }
            else
            {
                foreach (Parameter parameter in parameters)
                {
                    if ((targetModule.Parameters.FirstOrDefault(p => p.Name.ToUpper() == parameter.Name.ToUpper())) == null)
                    {
                        targetModule.Parameters.Add(parameter);
                    }
                }
            }
        }

        public async Task UpdateParameters(Module targetModule, ICollection<Parameter> parameters)
        {
            foreach (Parameter parameter in parameters)
            {
                Parameter targetParmeter = targetModule.Parameters.FirstOrDefault(p => p.ParameterID == parameter.ParameterID);
                if (targetParmeter != null)
                {
                    targetParmeter.Name = parameter.Name;
                    targetParmeter.Description = parameter.Description;
                    targetParmeter.Direction = parameter.Direction;
                    targetParmeter.ParameterTypeID = parameter.ParameterTypeID;
                }
            }
        }

        public async Task DeleteParameters(ICollection<Parameter> parameters)
        {
            foreach (Parameter parameter in parameters)
            {
                await _parameterService.DeleteParameter(parameter.ParameterID);
            }
        }
        public async Task ProcessParameterAction(string action, Module targetModule, ICollection<Parameter> parameters)
        {
            if (action.ToUpper() == "ADD")
            {
                await AddParameters(targetModule, parameters);
            }
            if (action.ToUpper() == "UPDATE")
            {
                await UpdateParameters(targetModule, parameters);
            }
            if (action.ToUpper() == "DELETE")
            {
                await DeleteParameters(parameters);
            }
        }

        public async Task UpdateModuleParameters(int moduleID, dynamic updates)
        {
            Module targetModule = await _moduleRepository.GetById(moduleID);

            foreach (dynamic modification in updates.modifications)
            {
                List<Parameter> targetParameters = await GetModificationParameters(modification.Parameters);
                await ProcessParameterAction(modification.Action.ToString(), targetModule, targetParameters);
            }

            await SaveModule();
        }

        public async Task SaveModule()
        {
            await _unitOfWork.Commit();
        }
    }
}
