﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IPipelineTaskInstanceModuleLogService
    {
        Task AddPipelineTaskInstanceModuleLog(PipelineTaskInstanceModuleLog pipelineTaskInstanceModuleLog);
        Task<IEnumerable<PipelineTaskInstanceModuleLog>> GetPipelineTaskInstanceModuleLogs(int pipelineInstanceID);
        Task SavePipelineTaskInstanceModuleLog();
    }
}
