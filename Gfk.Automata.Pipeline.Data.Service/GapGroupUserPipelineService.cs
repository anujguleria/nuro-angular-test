﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model.Views;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class GapGroupUserPipelineService : IGapGroupUserPipelineService
    {
        private readonly IGapGroupUserPipelineRepository _gapGroupUserPipelineRepository;

        public GapGroupUserPipelineService(IGapGroupUserPipelineRepository gapGroupUserPipelineRepository)
        {
            _gapGroupUserPipelineRepository = gapGroupUserPipelineRepository;
        }
        public async Task<IEnumerable<int>> GetUserPipelineIds(User user)
        {
            if (user == null)
            {
                return null;
            }
            else
            {
                ICollection<int> pipelineIds = new List<int>();

                IEnumerable<GapGroupUserPipeline> userPipelines = await _gapGroupUserPipelineRepository.GetMany(p => p.UserID == user.UserID);
                foreach (GapGroupUserPipeline userPipeline in userPipelines)
                {
                    pipelineIds.Add(userPipeline.PipeLineID);
                }

                return pipelineIds.AsEnumerable();
            }
        }

        public async Task<bool> UserCanAccessPipeline(Model.Pipeline pipeline, User user)
        {
            bool canAccessPipeline = false;

            if (pipeline != null && user != null)
            {
                canAccessPipeline = await UserCanAccessPipeline(pipeline.PipeLineID, user);
            }

            return canAccessPipeline;
        }

        public async Task<bool> UserCanAccessPipeline(int pipelineID, User user)
        {
            bool canAccessPipeline = false;

            if (user != null)
            {
                GapGroupUserPipeline userPipeline = await _gapGroupUserPipelineRepository.Get(p => p.PipeLineID == pipelineID && p.UserID == user.UserID);
                canAccessPipeline = userPipeline != null;
            }

            return canAccessPipeline;
        }

        public async Task<bool> UserCanAccessPipelineProject(int projectID, User user)
        {
            bool canAccessPipelineProject = false;

            if (user != null)
            {
                GapGroupUserPipeline userPipeline = await _gapGroupUserPipelineRepository.Get(p => p.ProjectID == projectID && p.UserID == user.UserID);
                canAccessPipelineProject = userPipeline != null;
            }

            return canAccessPipelineProject;
        }
    }
}
