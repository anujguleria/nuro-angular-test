﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IUserService
    {
        Task<bool> IsUsernameInUse(User user);
        Task<int> CreateUser(User User);
        Task<int> CreateUser(string email);
        Task UpdateUser(User User);
        Task UpdateCurrentUser(User currentUser, User updatedUserValues);
        Task DeleteUser(User User);
        Task DeleteUser(Expression<Func<User, bool>> where);
        Task<User> GetUser(int UserID);
        Task<bool> IsUserAdmin(int UserID);
        Task<User> GetUser(string email);
        Task<User> GetUser(Expression<Func<User, bool>> where);
        Task<User> GetUserWithGroups(int UserID);
        Task<IEnumerable<User>> GetUsers();
        Task<IEnumerable<User>> GetUsersWithRoles();
        Task<IEnumerable<User>> GetUsers(Expression<Func<User, bool>> where);
        Task SaveUser();
    }
}
