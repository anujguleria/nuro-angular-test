﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class ParameterService : IParameterService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IParameterRepository _parameterRepository;
        private readonly IPipelineTaskInputValueService _pipelineTaskInputValueService;


        public ParameterService(IParameterRepository parameterRepository, IUnitOfWork unitOfWork, IPipelineTaskInputValueService pipelineTaskInputValueService)
        {
            _parameterRepository = parameterRepository;
            _pipelineTaskInputValueService = pipelineTaskInputValueService;
            _unitOfWork = unitOfWork;
        }

        public async Task AddParameters(IEnumerable<Parameter> parameters)
        {
            foreach (Parameter parameter in parameters)
            {
                await _parameterRepository.Add(parameter);
            }

            await SaveParameter();
        }

        public async Task DeleteParameter(int parameterID)
        {
            await _parameterRepository.Delete(p => p.ParameterID == parameterID);
        }

        public async Task<ICollection<Parameter>> GetModuleParmeters(int moduleID)
        {
            IEnumerable<Parameter> parameters = await _parameterRepository.GetMany(p => p.ModuleID == moduleID);

            foreach (Parameter parameter in parameters)
            {
                parameter.PipelineTaskInputValues = await _pipelineTaskInputValueService.GetDependentParameterMappings(parameter.ParameterID);
            }

            return (ICollection<Parameter>) parameters;
        }

        public async Task SaveParameter()
        {
            await _unitOfWork.Commit();
        }
    }
}
