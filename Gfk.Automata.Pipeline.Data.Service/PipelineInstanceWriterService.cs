﻿using GfK.Automata.Pipeline.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using Newtonsoft.Json.Linq;
using GfK.Automata.Pipeline.Data.Service.Messaging;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineInstanceWriterService : IPipelineInstanceWriterService
    {
        private readonly IPipelineInstanceService _pipelineInstanceService;

        public PipelineInstanceWriterService(IPipelineInstanceService pipelineInstanceService)
        {
            _pipelineInstanceService = pipelineInstanceService;
        }

        public async Task<int> AddInstance(PipelineInstance instance)
        {
            int newPipelineTaskInstance = await _pipelineInstanceService.AddPipelineInstance(instance);

            return newPipelineTaskInstance;
        }

        public async Task UpdateInstance(PipelineInstance instance)
        {
            await _pipelineInstanceService.UpdatePipelineInstance(instance);
        }

        public async Task<int?> AddInstanceStartEntry(JObject message)
        {
            Dictionary<string, int> ids = new Dictionary<string, int>();
            int? newInstanceID = null;

            if (bool.Parse(message.Property("IsFirst").Value.ToString()))
            {
                ids.Add("PipelineID", int.Parse(message.Property("ParentID").Value.ToString()));
                PipelineInstance pipelineInstance = await InitializeInstance("Running", await MessageTime.UtcTime(), ids);
                
                newInstanceID = await AddInstance(pipelineInstance);
            }

            return newInstanceID;
        }

        public async Task AddInstanceEndEntry(JObject message, string status, string addlMessage = null)
        {
            if (bool.Parse(message.Property("IsLast").Value.ToString()) || status.ToUpper() == "FAILED")
            {
                PipelineInstance pipelineInstance = await _pipelineInstanceService.GetPipelineInstance(int.Parse(message.Property("PipelineInstanceID").Value.ToString()));
                pipelineInstance.Status = status;
                pipelineInstance.Ended = await MessageTime.UtcTime();
                pipelineInstance.Message = addlMessage;

                await UpdateInstance(pipelineInstance);
            }
        }

        public async Task<PipelineInstance> InitializeInstance(string status, DateTime started, Dictionary<string, int> ids, DateTime? ended = null, string message = null)
        {
            PipelineInstance pipelineInstance = new PipelineInstance
            {
                Status = status,
                Started = started,
                Ended = ended,
                Message = message
            };

            pipelineInstance.PipelineId = ids["PipelineID"];

            return pipelineInstance;
        }
    }
}
