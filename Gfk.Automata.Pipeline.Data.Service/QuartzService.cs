﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

using Quartz;
using Quartz.Impl;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class QuartzService : IQuartzService
    {

        private static IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
        public event EventHandler<int> ScheduleFired;
        private Dictionary<int, ITrigger> triggers = new Dictionary<int, ITrigger>();

        public QuartzService()
        {
            // create quartz scheduler
            lock (scheduler)
            {
                if (!scheduler.IsStarted)
                {
                    scheduler.Start();
                }
            }
        }
        public void StopAllTriggers()
        {
            // TODO - do we need to shut down the scheduler?...if so, have to figure out who does this
            scheduler.Clear();
        }
        public void RemoveTrigger(Schedule schedule)
        {
            JobKey key = new JobKey(schedule.ScheduleID.ToString());
            if (scheduler.GetJobDetail(key) != null)
            {
                scheduler.DeleteJob(key);
            }
            if (triggers.ContainsKey(schedule.ScheduleID))
            {
                triggers.Remove(schedule.ScheduleID);
            }
        }
        public DateTime? GetNextFireTime(Schedule schedule)
        {
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            if (schedule.Recurrence == Model.ScheduleRecurrence.None)
            {
                ITrigger trigger = triggers[schedule.ScheduleID];
                if (!trigger.GetFireTimeAfter(DateTime.UtcNow.AddMinutes(1)).HasValue)
                {
                    return null;
                }
                DateTime utc = trigger.GetFireTimeAfter(DateTime.UtcNow.AddMinutes(1)).Value.DateTime;
                return TimeZoneInfo.ConvertTimeFromUtc(utc, timezone);
            }
            else
            { 
                JobKey key = new JobKey(schedule.ScheduleID.ToString());
                if (scheduler.GetJobDetail(key) != null && scheduler.GetTriggersOfJob(key)[0].GetFireTimeAfter(DateTime.UtcNow).HasValue)
                {
                    DateTime startTimeUTC = TimeZoneInfo.ConvertTimeToUtc(schedule.StartTime, timezone);
                    TriggerKey triggerKey = new TriggerKey(schedule.ScheduleID.ToString());
                    ITrigger scheduledTrigger = scheduler.GetTriggersOfJob(key)[0];
                    int justInCase = 0;
                    // get fire time after has some weird behavior that we normalize back out
                    DateTimeOffset? nextFire = scheduledTrigger.GetFireTimeAfter(DateTime.UtcNow.AddMinutes(1)).Value;
                    // sometimes it returns as the timezone time
                    if (nextFire.Value.Hour != schedule.StartTime.Hour)
                    {
                        if (nextFire.Value.Hour != startTimeUTC.Hour)
                        {
                            // if not, let's assume it's in local time with right time of timezone, so adjust
                            nextFire = nextFire.Value.LocalDateTime;
                            // for whatever reason it returns the next fire time as local time instead of the timezone it's in, we'l just adjust minutes based on offset
                            double offsetMinutes = TimeZoneInfo.Local.BaseUtcOffset.TotalMinutes - timezone.BaseUtcOffset.TotalMinutes;
                            nextFire = nextFire.Value.AddMinutes(offsetMinutes);
                        }
                        else
                        {
                            // sometimes it returns as UTC
                            nextFire = TimeZoneInfo.ConvertTimeFromUtc(nextFire.Value.DateTime, timezone);
                        }
                    }
                    return nextFire.Value.DateTime;
                }
            }
            return null;
        }
        public void UpdateTrigger(Schedule schedule)
        {
            RemoveTrigger(schedule);
            ITrigger trigger = GetTrigger(schedule);
            if (trigger != null)
            {
                IJobDetail job = JobBuilder.Create<QuartzJob>()
                    .WithIdentity(schedule.ScheduleID.ToString())
                    .Build();
                job.JobDataMap.Put("sender", this);
                job.JobDataMap.Put("schedule", schedule);
                scheduler.ScheduleJob(job, GetTrigger(schedule));
                triggers[schedule.ScheduleID] = trigger;
            }
        }
        // TODO - do we have to gracefully handle disposal with scheduler.Shutdown();

        public ITrigger GetTrigger(Schedule schedule)
        {
            switch (schedule.Recurrence)
            {
                case ScheduleRecurrence.Daily:
                    return GetDailyTrigger(schedule);
                case ScheduleRecurrence.Weekly:
                    return GetWeeklyTrigger(schedule);
                case ScheduleRecurrence.Monthly:
                    return GetMonthlyTrigger(schedule);
                case ScheduleRecurrence.Yearly:
                    return GetYearlyTrigger(schedule);
                case ScheduleRecurrence.None:
                    return GetNoneTrigger(schedule);
            }
            return null;
        }

        public ITrigger GetNoneTrigger(Schedule schedule)
        {
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            DateTime startDate = schedule.StartTime;
            return TriggerBuilder.Create()
                /*.WithCronSchedule(startDate.Second.ToString() // seconds
                        + " " + startDate.Minute.ToString() // minutes
                        + " " + startDate.Hour.ToString() // hour
                        + " " + startDate.Day// day of month
                        + " " + startDate.Month // month
                        + " ?" // day of the week
                        + " " + startDate.Year// year
                        , a => a.InTimeZone(timezone))
                .StartNow()*/
                .StartAt(TimeZoneInfo.ConvertTimeToUtc(startDate, timezone).ToUniversalTime())
                .Build();
        }

        public TriggerBuilder GetBaseTrigger(Schedule schedule)
        {
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            DateTime startDate = schedule.StartTime;
            return TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule(s =>
                {
                    s.InTimeZone(timezone)
                    .StartingDailyAt(new TimeOfDay(startDate.Hour, startDate.Minute, startDate.Second));
                })
                .WithSimpleSchedule(s =>
                {
                    s.RepeatForever();
                })
                .StartAt(TimeZoneInfo.ConvertTimeToUtc(startDate, timezone))
                .WithIdentity(schedule.ScheduleID.ToString());
        }

        public ITrigger GetYearlyTrigger(Schedule schedule)
        {
            TriggerBuilder triggerBuilder = GetBaseTrigger(schedule);
            DateTime startDate = schedule.StartTime;
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            switch (schedule.YearlyRecurrenceType)
            {
                case YearlyRecurrenceType.SpecificDayOfMonth:
                    return triggerBuilder.WithCronSchedule(startDate.Second.ToString() // seconds
                        + " " + startDate.Minute.ToString() // minutes
                        + " " + startDate.Hour.ToString() // hour
                        + " " + schedule.DayOfMonth // day of month
                        + " " + schedule.MonthOfYear // month
                        + " ?" // day of the week
                        + " *" // year
                        , a => a.InTimeZone(timezone))
                    .StartAt(TimeZoneInfo.ConvertTimeToUtc(startDate, timezone))
                    .Build();
                case YearlyRecurrenceType.XDayOfWeekOfMonth:

                    string day = "";
                    // TODO - right now this is saved as a 1 based Sunday to Saturday
                    if (schedule.DayOfMonth == 1) day = "SUN";
                    if (schedule.DayOfMonth == 2) day = "MON";
                    if (schedule.DayOfMonth == 3) day = "TUE";
                    if (schedule.DayOfMonth == 4) day = "WED";
                    if (schedule.DayOfMonth == 5) day = "THU";
                    if (schedule.DayOfMonth == 6) day = "FRI";
                    if (schedule.DayOfMonth == 7) day = "SAT";

                    return triggerBuilder
                    .WithCronSchedule(startDate.Second.ToString() // seconds
                        + " " + startDate.Minute.ToString() // minutes
                        + " " + startDate.Hour.ToString() // hour
                        + " ?" // day of month
                        + " " + schedule.MonthOfYear // month
                        + " " + day + "#" + schedule.DayOfWeekOffset // day of the week
                        + " *" // year
                        , a => a.InTimeZone(timezone))
                    .Build();
            }
            return null;
        }

        public ITrigger GetMonthlyTrigger(Schedule schedule)
        {
            TriggerBuilder triggerBuilder = GetBaseTrigger(schedule);
            DateTime startDateUtc = schedule.StartTime;
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            switch (schedule.MonthlyRecurrenceType)
            {
                case MonthlyRecurrenceType.SpecificDayOfMonth:
                    return triggerBuilder
                    .WithCronSchedule(startDateUtc.Second.ToString() // seconds
                        + " " + startDateUtc.Minute.ToString() // minutes
                        + " " + startDateUtc.Hour.ToString() // hour
                        + " " + schedule.DayOfMonth // day of month
                        + " */" + schedule.RecurEvery// month
                        + " ?" // day of the week
                        , a =>
                        {
                            a.InTimeZone(timezone);
                        }
                    )
                    .Build();
                case MonthlyRecurrenceType.XDayOfWeekOfMonth:
                    string day = "";
                    // TODO - right now this is saved as a 1 based Sunday to Saturday
                    if (schedule.DayOfMonth == 1) day = "SUN";
                    if (schedule.DayOfMonth == 2) day = "MON";
                    if (schedule.DayOfMonth == 3) day = "TUE";
                    if (schedule.DayOfMonth == 4) day = "WED";
                    if (schedule.DayOfMonth == 5) day = "THU";
                    if (schedule.DayOfMonth == 6) day = "FRI";
                    if (schedule.DayOfMonth == 7) day = "SAT";

                    return triggerBuilder
                    .WithCronSchedule(startDateUtc.Second.ToString() // seconds
                        + " " + startDateUtc.Minute.ToString() // minutes
                        + " " + startDateUtc.Hour.ToString() // hour
                        + " ?" // day of month
                        + " */" + schedule.RecurEvery// month
                        + " " + day + "#" + schedule.DayOfWeekOffset // day of the week
                        , a =>
                        {
                            a.InTimeZone(timezone);
                        }
                    )
                    .Build();
            }
            return null;
        }

        public ITrigger GetWeeklyTrigger(Schedule schedule)
        {
            DateTime startDate = schedule.StartTime;
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            TriggerBuilder triggerBuilder = GetBaseTrigger(schedule);
            List<System.DayOfWeek> days = new List<System.DayOfWeek>();
            if (schedule.OnMonday) days.Add(System.DayOfWeek.Monday);
            if (schedule.OnTuesday) days.Add(System.DayOfWeek.Tuesday);
            if (schedule.OnWednesday) days.Add(System.DayOfWeek.Wednesday);
            if (schedule.OnThursday) days.Add(System.DayOfWeek.Thursday);
            if (schedule.OnFriday) days.Add(System.DayOfWeek.Friday);
            if (schedule.OnSaturday) days.Add(System.DayOfWeek.Saturday);
            if (schedule.OnSunday) days.Add(System.DayOfWeek.Sunday);
            return triggerBuilder
                .WithCronSchedule(startDate.Second.ToString() // seconds
                    + " " + startDate.Minute.ToString() // minutes
                    + " " + startDate.Hour.ToString() // hour
                    + " ?" // day of month
                    + " *" // month
                    + " " + String.Join(",", days.ToArray()) + "/" + schedule.RecurEvery // day of the week
                    , a =>
                    {
                        a.InTimeZone(timezone);
                    }
                )
                .Build();
        }

        public ITrigger GetDailyTrigger(Schedule schedule)
        {
            TriggerBuilder triggerBuilder = GetBaseTrigger(schedule);
            DateTime startDate = schedule.StartTime;
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            switch (schedule.DailyRecurrenceType)
            {
                case DailyRecurrenceType.EveryXDays:
                    return triggerBuilder.WithCalendarIntervalSchedule(c =>
                    {
                        c.WithIntervalInDays(schedule.RecurEvery);
                        c.InTimeZone(timezone);
                        
                    })
                    .StartAt(TimeZoneInfo.ConvertTimeToUtc(startDate, timezone))
                    .Build();
                case DailyRecurrenceType.EveryWeekday:

                    return triggerBuilder
                    .WithCronSchedule(startDate.Second.ToString() // seconds
                        + " " + startDate.Minute.ToString() // minutes
                        + " " + startDate.Hour.ToString() // hour
                        + " ?" // day of month
                        + " *" // month
                        + " MON,TUE,WED,THU,FRI" // day of the week
                        , a => a.InTimeZone(timezone)
                    )
                    //.StartAt(TimeZoneInfo.ConvertTimeToUtc(startDate, timezone))
                    .Build();
            }
            return null;
        }
        public void FireSchedule(int scheduleID)
        {
            ScheduleFired(this, scheduleID);
        }
    }

    public class QuartzJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            // don't fire if first run is more than 3 minutes ago (quartz fires an initial job sometimes)
            Schedule schedule = (Schedule)context.MergedJobDataMap.Get("schedule");
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneID);
            QuartzService service = (QuartzService)context.MergedJobDataMap.Get("sender");
            DateTime nextFire = TimeZoneInfo.ConvertTimeToUtc(schedule.NextFireTime.Value);
            // for some reason it fires a job in the past even though next fire time is in the future (given 1 min of room)
            if (DateTime.UtcNow.Subtract(nextFire).TotalMinutes >= 0)
            {
                service.FireSchedule(schedule.Pipeline.PipeLineID);
            }
        }
    }
}
