﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineTaskInstanceService : IPipelineTaskInstanceService
    {
        private readonly IPipelineTaskInstanceRepository _pipelineTaskInstanceRepository;
        private readonly IPipelineTaskInstanceModuleLogService _pipelineTaskInstanceModuleLogService;
        private readonly IUnitOfWork _unitOfWork;

        public PipelineTaskInstanceService(IPipelineTaskInstanceRepository pipelineTaskInstanceRepository, IUnitOfWork unitOfWork
            ,IPipelineTaskInstanceModuleLogService pipelineTaskInstanceModuleLogService)
        {
            _pipelineTaskInstanceRepository = pipelineTaskInstanceRepository;
            _pipelineTaskInstanceModuleLogService = pipelineTaskInstanceModuleLogService;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> AddPipelineTaskInstance(PipelineTaskInstance pipelineTaskInstance)
        {
            PipelineTaskInstance newPipelineTaskInstance = await _pipelineTaskInstanceRepository.Add(pipelineTaskInstance);
            await SavePipelineTaskInstance();

            return newPipelineTaskInstance.PipelineTaskInstanceId;
        }
        public async Task<PipelineTaskInstance> GetPipelineTaskInstance(int pipelineTaskInstanceId)
        {
            PipelineTaskInstance pipelineTaskInstance = await _pipelineTaskInstanceRepository.GetById(pipelineTaskInstanceId);

            return pipelineTaskInstance;
        }

        public async Task<PipelineTaskInstance> GetPipelineTaskInstanceWithModuleLog(int pipelineTaskInstanceId)
        {
            PipelineTaskInstance pipelineTaskInstance = await GetPipelineTaskInstance(pipelineTaskInstanceId);

            if (pipelineTaskInstance != null)
            {
                pipelineTaskInstance.PipelineTaskInstanceModuleLogs =
                    (await _pipelineTaskInstanceModuleLogService.GetPipelineTaskInstanceModuleLogs(pipelineTaskInstance.PipelineTaskInstanceId)).ToList();
            }

            return pipelineTaskInstance;
        }

        public async Task UpdatePipelineTaskInstance(PipelineTaskInstance pipelineTaskInstance)
        {
            await _pipelineTaskInstanceRepository.Update(pipelineTaskInstance);
            await SavePipelineTaskInstance();
        }

        public async Task SavePipelineTaskInstance()
        {
            await _unitOfWork.Commit();
        }
    }
}
