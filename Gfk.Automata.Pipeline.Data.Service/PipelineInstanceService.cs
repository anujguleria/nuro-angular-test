﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineInstanceService : IPipelineInstanceService
    {
        private readonly IPipelineInstanceRepository _pipelineInstanceRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PipelineInstanceService(IPipelineInstanceRepository pipelineInstanceRepository, IUnitOfWork unitOfWork)
        {
            _pipelineInstanceRepository = pipelineInstanceRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> AddPipelineInstance(PipelineInstance pipelineInstance)
        {
            PipelineInstance newPipelineInstance = await _pipelineInstanceRepository.Add(pipelineInstance);
            await SavePipelineInstance();

            return newPipelineInstance.PipelineInstanceId;
        }

        public async Task<IEnumerable<PipelineInstance>> GetInstancesForPipeline(int pipelineId)
        {
            IEnumerable<PipelineInstance> pipelineInstances = await _pipelineInstanceRepository.GetPipelineInstances(pipelineId);

            // Retain pipeline in model for PipelineInstance but don't return in this API response to reduce content returned.
            foreach (PipelineInstance pipelineInstance in pipelineInstances)
            {
                pipelineInstance.Pipeline = null;
            }

            return pipelineInstances;
        }

        public async Task<PipelineInstance> GetPipelineInstance(int pipelineInstanceId)
        {
            PipelineInstance pipelineInstance = await _pipelineInstanceRepository.GetById(pipelineInstanceId);

            return pipelineInstance;
        }

        public async Task<PipelineInstance> GetPipelineInstanceWithLogs(int pipelineInstanceId)
        {
            PipelineInstance pipelineInstance = await _pipelineInstanceRepository.GetWithLogs(pipelineInstanceId);

            return pipelineInstance;
        }

        public async Task<PipelineInstance> GetPipelineInstanceLastRun(int pipelineId)
        {
            PipelineInstance lastRunInstance = await _pipelineInstanceRepository.GetPipelineInstanceLastRun(pipelineId);

            return lastRunInstance;
        }

        public async Task UpdatePipelineInstance(PipelineInstance pipelineInstance)
        {
            await _pipelineInstanceRepository.Update(pipelineInstance);
            await SavePipelineInstance();
        }
        public async Task SavePipelineInstance()
        {
            await _unitOfWork.Commit();
        }
    }
}
