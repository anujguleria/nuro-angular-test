﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IGapGroupUserPipelineService
    {
        Task<IEnumerable<int>> GetUserPipelineIds(User user);
        Task<bool> UserCanAccessPipeline(Model.Pipeline pipeline, User user);
        Task<bool> UserCanAccessPipeline(int pipelineID, User user);
        Task<bool> UserCanAccessPipelineProject(int projectID, User user);
    }
}
