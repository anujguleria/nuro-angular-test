﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IParameterService
    {
        Task AddParameters(IEnumerable<Parameter> parameters);
        Task DeleteParameter(int parameterID);
        Task SaveParameter();
        Task<ICollection<Parameter>> GetModuleParmeters(int moduleID);
    }
}
