﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface INotificationService
    {
        Task<IEnumerable<Notification>> GetNotifications(int userID);
        Task DeleteNotification(int notificationID);
        Task DeleteNotificationOfUser(int userID);
        Task<Notification> CreateNotification(Notification notification,string status = null);
        Task<Notification> GetNotificationByID(int NotificationID);
    }
}
