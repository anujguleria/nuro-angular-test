﻿using GfK.Automata.Pipeline.Data.Infrastructure;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineNotificationSettingsService : IPipelineNotificationSettingsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPipelineNotificationSettingsRepository _pipelineNotificationSettingRepository;

        public PipelineNotificationSettingsService(IUnitOfWork unitOfWork, IPipelineNotificationSettingsRepository pipelineNotificationSettingRepository)
        {
            _unitOfWork = unitOfWork;
            _pipelineNotificationSettingRepository = pipelineNotificationSettingRepository;
        }

        public async Task UpdateNotificationSettings(int pipelineID,IEnumerable<PipelineNotificationSetting> pipelineNotificationSettings)
        {
            if (pipelineNotificationSettings != null)
            {
                await  _pipelineNotificationSettingRepository.Delete(p=>p.PipelineID == pipelineID);
                foreach (PipelineNotificationSetting pipelineNotificationSetting in pipelineNotificationSettings)
                {
                    pipelineNotificationSetting.PipelineID = pipelineID;
                    pipelineNotificationSetting.User = null;
                    pipelineNotificationSetting.Pipeline = null; 
                    await AddPipelineNotificationSeting(pipelineNotificationSetting);
                }
                await SavePipelineNotificationSettings();
            }
        }
        public async Task DeleteNotificationSetting(int PipelineID, int UserID)
        {
            await _pipelineNotificationSettingRepository.Delete(p => p.UserID == UserID && p.PipelineID == PipelineID);
            await SavePipelineNotificationSettings();
        }
        public async Task<IEnumerable<PipelineNotificationSetting>> GetNotificationSettings(int PipelineID)
        {
            IEnumerable<PipelineNotificationSetting> pipelineNotificationSettings = await _pipelineNotificationSettingRepository.GetMany(p => p.PipelineID == PipelineID);
            // The above is pulling in large trees of data 
            foreach(PipelineNotificationSetting pipelineNotificationSetting in pipelineNotificationSettings)
            {
                pipelineNotificationSetting.Pipeline = null;
              //  pipelineNotificationSetting.User = null;
            }
            return pipelineNotificationSettings;
        }

      
        public async Task SavePipelineNotificationSettings()
        {
            await _unitOfWork.Commit();
        }

        public async Task AddPipelineNotificationSeting(PipelineNotificationSetting pipelineNotificationSetting)
        {
            await _pipelineNotificationSettingRepository.Add(pipelineNotificationSetting);
        }
    }
}
