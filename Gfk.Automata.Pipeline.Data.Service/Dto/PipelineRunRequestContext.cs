﻿using GfK.Automata.Pipeline.Model.Domains;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service.Dto
{
    public class PipelineRunRequestContext
    {
        public RunPipelineContext RunPipelineContext { get; set; }

    }
}
