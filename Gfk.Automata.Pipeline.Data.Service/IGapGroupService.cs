﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IGapGroupService
    {
        Task<IEnumerable<GapGroup>> GetGapGroups();
        Task<GapGroup> GetGapGroup(int id);
        Task<GapGroup> GetGapGroup(string gapName);
        Task<GapGroup> GetGapGroupWithUsers(int id);
        Task<IEnumerable<string>> GetGapGroupProjectNames(GapGroup gapGroup);
        Task<int> AddGapGroup(GapGroup gapGroup);
        Task<bool> IsGapGroupNameInUse(GapGroup gapGroup);
        Task<bool> GapGroupHasProjects(GapGroup gapGroup);
        Task DeleteGroup(GapGroup gapGroup);
        Task UpdateGapGroup(GapGroup gapGroup);
        Task SaveGapGroup();
    }
}
