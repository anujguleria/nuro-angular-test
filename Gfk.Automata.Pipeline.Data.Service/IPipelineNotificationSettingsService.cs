﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IPipelineNotificationSettingsService 
    {
        Task UpdateNotificationSettings(int pipelineID,IEnumerable<PipelineNotificationSetting> pipelineNotificationSettings);
        Task DeleteNotificationSetting(int pipelineID,int UserID);
        Task<IEnumerable<PipelineNotificationSetting>> GetNotificationSettings(int PipelineID);
        Task SavePipelineNotificationSettings();
    }
}
