﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IPipelineTaskInputValueService
    {
        Task ReplacePipelineTaskInputValues(int pipelineTaskId, ICollection<PipelineTaskInputValue> replacementInputValues,
            int pipelineId);
        Task DeletePipelineTaskInputValues(int pipelineTaskId);
        void AddLookupsToInputValues(ICollection<PipelineTaskInputValue> replacementInputValues, int pipelineId);
        Task<ICollection<PipelineTaskInputValue>> GetDependentPipelineTaskInputs(int pipelineTaskId);
        Task<ICollection<PipelineTaskInputValue>> GetDependentParameterMappings(int parameterID);
    }
}
