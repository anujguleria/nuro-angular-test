﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Data.Repositories;
using GfK.Automata.Pipeline.Data.Infrastructure;
using System.Data.SqlClient;
using GfK.Automata.Pipeline.Model.Views;

namespace GfK.Automata.Pipeline.Data.Service
{
    public class PipelineTaskService : IPipelineTaskService
    {
        private readonly IPipelineTaskRepository _pipelineTaskRepository;
        private readonly IPipelineTaskInputValueService _pipelineTaskInputValueService;
        private readonly IUserInputValueService _userInputValueService;
        private readonly IProjectUserService _projectUserService;
        private readonly IUnitOfWork _unitOfWork;

        public PipelineTaskService(IPipelineTaskRepository pipelineTaskRepository, IUnitOfWork unitOfWork, IPipelineTaskInputValueService pipelineTaskInputValueService
            ,IUserInputValueService userInputValueService, IProjectUserService projectUserService)
        {
            _pipelineTaskRepository = pipelineTaskRepository;
            _pipelineTaskInputValueService = pipelineTaskInputValueService;
            _userInputValueService = userInputValueService;
            _unitOfWork = unitOfWork;
            _projectUserService = projectUserService;
        }
        public async Task MovePipelineTask(int pipelineTaskID, int count)
        {
            PipelineTask pipelineTask = await _pipelineTaskRepository.Get(p => p.PipelineTaskID == pipelineTaskID);
            if (count < 0)
            {
                foreach (PipelineTask taskToMove in await _pipelineTaskRepository.GetMany(
                       t => t.PipeLineID == pipelineTask.PipeLineID && t.SequencePosition >= (pipelineTask.SequencePosition + count)
                       && t.SequencePosition < pipelineTask.SequencePosition))
                {
                    taskToMove.SequencePosition++;
                    await _pipelineTaskRepository.Update(taskToMove);
                }
            }
            else
            {
                foreach (PipelineTask taskToMove in await _pipelineTaskRepository.GetMany(
                       t => t.PipeLineID == pipelineTask.PipeLineID && t.SequencePosition <= (pipelineTask.SequencePosition + count)
                       && t.SequencePosition > pipelineTask.SequencePosition))
                {
                    taskToMove.SequencePosition--;
                    await _pipelineTaskRepository.Update(taskToMove);
                }
            }
            pipelineTask.SequencePosition = pipelineTask.SequencePosition + count;
            await _pipelineTaskRepository.Update(pipelineTask);
            await SavePipelineTask();
        }
        public async Task<int> AddPipelineTask(PipelineTask pipelineTask)
        {
            _pipelineTaskInputValueService.AddLookupsToInputValues(pipelineTask.PipelineTaskInputValues, pipelineTask.PipeLineID);
            PipelineTask newPipelineTask = await _pipelineTaskRepository.Add(pipelineTask);

            // bump sequence of all other tasks
            foreach (PipelineTask taskToMove in await _pipelineTaskRepository.GetMany(
                t => t.PipeLineID == pipelineTask.PipeLineID && t.SequencePosition >= pipelineTask.SequencePosition))
            {
                if (taskToMove.SequencePosition != 2147483647)
                {
                    taskToMove.SequencePosition++;
                    await _pipelineTaskRepository.Update(taskToMove);
                }
            }
            await SavePipelineTask();

            return newPipelineTask.PipelineTaskID;
        }

        public async Task<PipelineTask> CreatePipelineTaskSystem(string name, int pipelineID, int moduleID, int sequencePosition)
        {
            PipelineTask pipelineTask = new PipelineTask()
            {
                Name = name,
                PipeLineID = pipelineID,
                SequencePosition = sequencePosition,
                ModuleID = moduleID
            };

            return pipelineTask;
        }

        public async Task AddSystemPipelineTasks(int pipelineID, int startModuleID, int endModuleID)
        {
            PipelineTask startSystemTask = await _pipelineTaskRepository.Add(await CreatePipelineTaskSystem("System Start", pipelineID, startModuleID, 0));
            PipelineTask endSystemTask = await _pipelineTaskRepository.Add(await CreatePipelineTaskSystem("System End", pipelineID, endModuleID, 2147483647));

            await SavePipelineTask();
        }

        public async Task<PipelineTask> GetPipelineTask(int pipelineTaskID)
        {
            PipelineTask pipelineTask = await _pipelineTaskRepository.Get(p => p.PipelineTaskID == pipelineTaskID);
            pipelineTask.PipelineTaskInputValues = await _pipelineTaskInputValueService.GetDependentPipelineTaskInputs(pipelineTaskID);
            //pipelineTask.UserInputValues = await _userInputValueService.GetPipelineTaskUserInputValues(pipelineTaskID);

            return pipelineTask;
        }

        public async Task<PipelineTask> GetPipelineTaskWithParameters(int pipelineTaskID)
        {
            PipelineTask pipelineTask = await _pipelineTaskRepository.Get(p => p.PipelineTaskID == pipelineTaskID);

            return pipelineTask;
        }

        public async Task<PipelineTask> GetPipelineTaskByName(int pipeLineID, string pipelineTaskName)
        {
            PipelineTask pipelineTask = await _pipelineTaskRepository.Get(p => p.PipeLineID == pipeLineID && p.Name == pipelineTaskName);

            return pipelineTask;
        }
        public async Task<Model.PipelineTask> GetPipelineTask(string pipelineTaskName, User currentlyLoggedInUser, dynamic request)
        {
            Model.PipelineTask seekPipelineTask = null;

            seekPipelineTask = await GetPipelineTask(pipelineTaskName, currentlyLoggedInUser, int.Parse(request.ProjectID.ToString())
                , (request.PipelineID == null ? null : int.Parse(request.PipelineID.ToString())));

            return seekPipelineTask;
        }

        public async Task<Model.PipelineTask> GetPipelineTask(string pipelineTaskName, User currentlyLoggedInUser, int projectId, int? parentId)
        {
            Model.PipelineTask seekPipelineTask = null;

            if (parentId == null)
            {
                seekPipelineTask = await _pipelineTaskRepository.Get((p => p.Pipeline.project.ProjectID == projectId && p.Name == pipelineTaskName));
            }
            else
            {
                seekPipelineTask = await _pipelineTaskRepository.Get(((p => p.Pipeline.PipeLineID == parentId && p.Name == pipelineTaskName)));
            }

            return seekPipelineTask;
        }
        public async Task<int?> GetPipelineTaskSequenceNumber(int pipeineTaskID)
        {
            PipelineTask pipelineTask = await GetPipelineTask(pipeineTaskID);

            return (pipelineTask == null ? null : (int?)int.Parse(pipelineTask.SequencePosition.ToString()));
        }

        public async Task<int?> GetNextPipelineTaskID(int pipelineID, int afterSequencePosition)
        {
            PipelineTask pipelineTask = await GetPipelineTaskByPosition(pipelineID, afterSequencePosition);

            return (pipelineTask == null ? null : (int?)int.Parse(pipelineTask.PipelineTaskID.ToString()));
        }

        public async Task<int?> GetFirstPipelineTaskId(int pipelineID)
        {
            int? pipelineTaskId = await GetNextPipelineTaskID(pipelineID, -1);

            return pipelineTaskId;
        }

        public async Task<PipelineTask> GetPipelineTaskByPosition(int pipeLineID, int afterSequencePosition)
        {
            IEnumerable<PipelineTask> pipelineTasks = await _pipelineTaskRepository.GetMany(p => p.PipeLineID == pipeLineID && p.SequencePosition > afterSequencePosition);

            if (pipelineTasks != null  && pipelineTasks.Count() > 0)
            {
                return pipelineTasks.OrderBy(p => p.SequencePosition).First();
            }
            else
            {
                return null;
            }
        }

        public async Task<IEnumerable<PipelineTask>> GetPipelineTasks()
        {
            IEnumerable<PipelineTask> pipelineTasks = await _pipelineTaskRepository.GetAll();

            return pipelineTasks.OrderBy(p => p.SequencePosition);
        }

        public async Task<IEnumerable<PipelineTask>> GetPipelineTasks(int pipelineId)
        {
            IEnumerable<PipelineTask> pipelineTasks = await _pipelineTaskRepository.GetMany(p => p.PipeLineID == pipelineId);

            return pipelineTasks.OrderBy(p => p.SequencePosition);
        }

        public async Task<IEnumerable<PipelineTask>> GetAvailableParameters(IEnumerable<PipelineHierarchy> pipelineHierarchy, string direction)
        {
            List<PipelineTask> availableParameters = new List<PipelineTask>();

            foreach (PipelineHierarchy pipeline in pipelineHierarchy)
            {
                PipelineTask parameterProvider = await _pipelineTaskRepository.Get(p => p.Name == (direction.ToUpper() == "INPUT" ? "System End" : "System Start")
                && p.PipeLineID == pipeline.HierarchyPipelineID);

                if (parameterProvider != null)
                {
                    availableParameters.Add(parameterProvider);
                }
            }

            return availableParameters;
        }

        public async Task UpdatePipelineTask(PipelineTask pipelineTask)
        {
            await _pipelineTaskInputValueService.ReplacePipelineTaskInputValues(pipelineTask.PipelineTaskID, pipelineTask.PipelineTaskInputValues,
                pipelineTask.PipeLineID);
            await _userInputValueService.ReplacePipelineTaskInputValues(pipelineTask.PipelineTaskID, pipelineTask.UserInputValues);
            try
            {
                await _pipelineTaskRepository.Update(pipelineTask);
            }
            catch (Exception e)
            {
                throw;
            }

            await this.SavePipelineTask();
        }

        public async Task SavePipelineTask()
        {
            await _unitOfWork.Commit();
        }

        public async Task DeletePipelineTask(int pipelineTaskID)
        {
            ICollection<PipelineTaskInputValue> taskInputValues =await _pipelineTaskInputValueService.GetDependentPipelineTaskInputs(pipelineTaskID);
            PipelineTask task = await _pipelineTaskRepository.GetById(pipelineTaskID);
            if (taskInputValues.Count != 0)
            {
                throw new VoilationOfRefrentialIntegrityException(string.Format("Error occured while deleting the task \" {0} \", please make sure this task is not referenced by another task",task.Name));
            }
            // move all later tasks up
            // TODO - should do a better job than p.SequencePosition < 100000 at not moving the system end
            foreach (PipelineTask moveTask in (await GetPipelineTasks(task.PipeLineID)).Where(p => p.SequencePosition > task.SequencePosition && p.SequencePosition < 100000))
            {
                await MovePipelineTask(moveTask.PipelineTaskID, -1);
            }
            await _pipelineTaskRepository.Delete(task);
            await _unitOfWork.Commit();
        }
    }
}
