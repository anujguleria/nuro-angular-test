﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IPipelineInstanceService
    {
        Task<int> AddPipelineInstance(PipelineInstance pipelineInstance);
        Task UpdatePipelineInstance(PipelineInstance pipelineInstance);
        Task<IEnumerable<PipelineInstance>> GetInstancesForPipeline(int pipelineId);
        Task<PipelineInstance> GetPipelineInstance(int pipelineInstanceId);
        Task<PipelineInstance> GetPipelineInstanceLastRun(int pipelineId);
        Task SavePipelineInstance();
        Task<PipelineInstance> GetPipelineInstanceWithLogs(int pipelineInstanceId);
    }
}
