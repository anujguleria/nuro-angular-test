﻿using GfK.Automata.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IGapGroupUserProjectService
    {
        Task<IEnumerable<int>> GetUserProjectIds(User user);
        Task<bool> UserCanAccessProject(Project project, User user);
        Task<bool> UserCanAccessProject(int projectID, User user);
    }
}
