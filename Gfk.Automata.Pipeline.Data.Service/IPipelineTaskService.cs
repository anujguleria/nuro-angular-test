﻿using GfK.Automata.Pipeline.Model;
using GfK.Automata.Pipeline.Model.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Pipeline.Data.Service
{
    public interface IPipelineTaskService
    {
        Task<int> AddPipelineTask(PipelineTask pipelineTask);
        Task AddSystemPipelineTasks(int pipelineID, int startModuleID, int endModuleID);
        Task<PipelineTask> CreatePipelineTaskSystem(string name, int pipelineID, int moduleID, int sequencePosition);
        Task<IEnumerable<PipelineTask>> GetPipelineTasks(int pipelineId);
        Task<int?> GetPipelineTaskSequenceNumber(int pipelineTaskID);
        Task<PipelineTask> GetPipelineTaskByName(int pipelineID, string pipelineTaskName);
        Task<int?> GetNextPipelineTaskID(int pipelineID, int afterSequencePosition);
        Task<int?> GetFirstPipelineTaskId(int pipelineID);
        Task<PipelineTask> GetPipelineTaskByPosition(int pipelineID, int afterSequencePosition);
        Task<IEnumerable<PipelineTask>> GetAvailableParameters(IEnumerable<PipelineHierarchy> pipelineHierarchy, string direction);
        Task UpdatePipelineTask(PipelineTask pipelineTask);
        Task SavePipelineTask();
        Task<PipelineTask> GetPipelineTask(int pipelineTaskID);
        Task<PipelineTask> GetPipelineTaskWithParameters(int pipelineTaskID);
        Task MovePipelineTask(int pipelineTaskID, int count);
        Task DeletePipelineTask(int pipelineTaskID);
        Task<Model.PipelineTask> GetPipelineTask(string pipelineTaskName, User currentlyLoggedInUser, dynamic request);
    }
}
