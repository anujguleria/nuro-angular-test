﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Service
{
    public class MappedVariable
    {
        public string BooleanCategory { get; set; }
        public string SourceVariableName { get; set; }
        public string DestinationVariableName { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public string Max { get; set; }
        public List<MappedValue> Values { get; set; }

        public MappedVariable( string sourceVariableName, string destinationVariableName, string type, string label, string max )
        {
            BooleanCategory = null;
            SourceVariableName = sourceVariableName;
            DestinationVariableName = destinationVariableName;
            Type = type;
            Label = label;
            Max = max;
            Values = new List<MappedValue>();
        }

        public MappedVariable(string sourceVariableName, string destinationVariableName, string type, string label, string max, string booleanCategory)
        {
            BooleanCategory = booleanCategory;
            SourceVariableName = sourceVariableName;
            DestinationVariableName = destinationVariableName;
            Type = type;
            Label = label;
            Max = max;
            Values = new List<MappedValue>();
        }
    }
}
