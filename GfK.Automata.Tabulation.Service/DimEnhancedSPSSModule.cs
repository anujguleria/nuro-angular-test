﻿using GfK.Automata.Pipeline.GapModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;
using GfK.Automata.Pipeline.Model.Domains;

namespace GfK.Automata.Tabulation.Service
{
    public class DimEnhancedSPSSModule : IModule
    {
        public ILogger<ModuleLogMessageType> Logger { get; set; }

        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            string context = (await parameters.GetParameter("context")) == null ? null : (await parameters.GetParameter("context")).Value;
            string language = (await parameters.GetParameter("language")) == null ? null : (await parameters.GetParameter("language")).Value;
            string mdd = (await parameters.GetParameter("mdd")) == null ? null : (await parameters.GetParameter("mdd")).Value;
            string ddf = (await parameters.GetParameter("ddf")) == null ? null : (await parameters.GetParameter("ddf")).Value;
            string sav = (await parameters.GetParameter("sav")) == null ? null : (await parameters.GetParameter("sav")).Value;
            string version = (await parameters.GetParameter("version")) == null ? null : (await parameters.GetParameter("version")).Value;
            string xlsx = (await parameters.GetParameter("xlsx")) == null ? null : (await parameters.GetParameter("xlsx")).Value;
            string create = (await parameters.GetParameter("create")) == null ? null : (await parameters.GetParameter("create")).Value;
            string read = (await parameters.GetParameter("read")) == null ? null : (await parameters.GetParameter("read")).Value;
            ProcessorConfiguration conf = new ProcessorConfiguration();
            conf.Add("LayoutFilePath", xlsx);
            conf.Add("SAVOutputPath", sav);
            conf.Add("DDFFilePath", ddf);
            conf.Add("MDDFilePath", mdd);
            bool createTry;
            if (create != null && create.Trim().Length > 0 && bool.TryParse(create, out createTry) && createTry)
            {
                conf.Add("Context", context);
                conf.Add("Language", language);
                conf.Add("GenerateLayout", true);
            }
            else
            {
                conf.Add("GenerateLayout", false);
            }
            await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "Reading metadata.");
            MetadataDocument document = MDMReader.Read(mdd, version, MetadataSourceType.stDimensionsMDD);
            conf.Add("Document", document);
            await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "Processing data.");
            DimEnhancedSPSSProcessor.Process(conf);
            await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, "Finished.");
            IParameterValueCollection returnParameters = new ParameterValueCollection();
            await returnParameters.AddParameter(new ParameterString("DimEnhancedSPSSModule", "sav")
            {
                Value = sav
            });
            await returnParameters.AddParameter(new ParameterString("DimEnhancedSPSSModule", "xlsx")
            {
                Value = xlsx
            });
            return returnParameters;
        }
    }
}
