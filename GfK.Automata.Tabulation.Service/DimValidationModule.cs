﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;
using GfK.Automata.Pipeline.GapModule;
using GfK.Automata.Pipeline.Model;

namespace GfK.Automata.Tabulation.Service
{
    public class DimValidationModule : IModule
    {
        // Parameters
        //   0) PathToOutputFolder

        public ILogger<Pipeline.Model.Domains.ModuleLogMessageType> Logger { get; set; }

        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            IParameterValueCollection returnParameters = new ParameterValueCollection();
            await Generate(parameters, returnParameters);
            return returnParameters;
        }

        public async Task Generate(IParameterValueCollection parameters, IParameterValueCollection returnParameters)
        {
            string path = parameters.SystemParameters.GetNewTempFolder();
            GfK.Automata.Tabulation.Service.DimValidationGenerator.Generate(path);
            await returnParameters.AddParameter(new ParameterString("DimValidationModule", "OutputFolder")
            {
                Value = path
            });
            await returnParameters.AddParameter(new ParameterString("DimValidationModule", "DMSFile")
            {
                Value = path + "\\QAsyntax.dms"
            });
        }
    }
}
