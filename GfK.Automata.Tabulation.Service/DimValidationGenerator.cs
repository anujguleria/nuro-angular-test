﻿using System;
using System.IO;
using System.Reflection;

namespace GfK.Automata.Tabulation.Service
{
    public static class DimValidationGenerator
    {
        public static void Generate( string pathToOutputDirectory )
        {
            string filesDirectory = Path.Combine(pathToOutputDirectory, "Files");
            Directory.CreateDirectory(filesDirectory);
            var assembly = Assembly.GetExecutingAssembly();
            string[] filenames = assembly.GetManifestResourceNames();
            string baseFilename;
            string outputPath;
            foreach ( string filename in filenames )
            {
                if (filename.Contains("DimValidationGeneratorAssets"))
                {
                    string[] tmp = filename.Split('.');
                    baseFilename = tmp[tmp.Length - 2] + "." + tmp[tmp.Length - 1];
                    if (filename.Contains("Files"))
                    {
                        outputPath = Path.Combine(filesDirectory, baseFilename);
                    }
                    else
                    {
                        outputPath = Path.Combine(pathToOutputDirectory, baseFilename);
                    }
                    using (FileStream stream = File.Create(outputPath))
                    {
                        assembly.GetManifestResourceStream(filename).CopyTo(stream);
                    }
                }
            }
        }
    }
}
