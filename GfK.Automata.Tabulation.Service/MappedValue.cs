﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation.Service
{
    public class MappedValue
    {
        public dynamic SourceValue { get; set; }
        public dynamic DestinationValue { get; set; }
        public string Label { get; set; }
        public MappedValue( string sourceValue, string destinationValue, string label )
        {
            SourceValue = sourceValue;
            DestinationValue = destinationValue;
            Label = label;
        }

        public MappedValue( int sourceValue, int destinationValue, string label )
        {
            SourceValue = sourceValue;
            DestinationValue = DestinationValue;
            Label = label;
        }

        public MappedValue( string sourceValue, int destinationValue, string label )
        {
            SourceValue = sourceValue;
            DestinationValue = destinationValue;
            Label = label;
        }

        public MappedValue( int sourceValue, string destinationValue, string label )
        {
            SourceValue = sourceValue;
            DestinationValue = destinationValue;
            Label = label;
        }
    }
}
