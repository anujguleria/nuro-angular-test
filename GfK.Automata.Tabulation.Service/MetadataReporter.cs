﻿using System;
using System.Collections.Generic;
using System.IO;
using GfK.Automata.Model;

namespace GfK.Automata.Tabulation.Service
{
    public class MetadataReporter
    {
        static MetadataReporter()
        {

        }

        static void ItemsToText( List<MetadataObject> items, StreamWriter writer, string context, string language, int indentLevel = 0 )
        {
            int count = items.Count;
            int padding = indentLevel * 4;
            for( int i = 0; i < count; i++ )
            {
                //Console.WriteLine("Processing " + items[i].Name);
                switch( items[ i ].ObjectType )
                {
                    case ObjectType.otCategory:
                        throw new NotImplementedException(); // shouldn't have a category at this place in the document
                    case ObjectType.otCollection:
                        writer.WriteLine(items[i].Name + " [Container]");
                        ItemsToText(items[i].Items, writer, context, language, indentLevel + 1);
                        break;
                    case ObjectType.otDefinedList:
                        string s = ( items[i].Name + " [Defined List]" ).PadLeft( padding ) + "\n";
                        foreach( Category category in items[ i ].Items )
                        {
                            s += "".PadLeft(padding) + "  {" + category.Name + "}  " + category.Label(context, language, category.Document) + "\n";
                        }
                        s += "\n";
                        writer.WriteLine(s);
                        break;
                    case ObjectType.otDocument:
                        ItemsToText(items[i].Items, writer, context, language, indentLevel + 1);
                        break;
                    case ObjectType.otField:
                        Field f = (Field)items[i];
                        s = "".PadLeft(padding) + "  " + f.Name + " [" + f.DataType.ToString() + "] " + f.Label(context, language, f.Document) + "\n";
                        s += "".PadLeft(padding) + "  Alias: " + f.Alias + "\n";
                        s += "".PadLeft(padding) + "  HasCaseData: " + f.HasCaseData.ToString() + "\n";
                        s += "".PadLeft(padding) + "  IsDerived: " + f.IsDerived.ToString() + "\n";
                        s += "".PadLeft(padding) + "  IsSystem: " + f.IsSystem.ToString() + "\n";
                        s += "".PadLeft(padding) + "  IsReference: " + f.IsReference.ToString() + "\n";
                        if ( f.DataType == DataType.dtCategorical || f.DataType == DataType.dtBoolean )
                        {
                            BoundField bf = (BoundField)f;
                            foreach (Category category in bf.Categories())
                            {
                                s += "".PadLeft(padding) + "  {" + category.Name + "}  " + category.Label(context, language, category.Document) + "\n";
                            }
                        }
                        else
                        {
                            UnboundField uf = (UnboundField)f;
                            s += "".PadLeft(padding) + "  MinValue: " + uf.MinValue + "\n";
                            s += "".PadLeft(padding) + "  MaxValue: " + uf.MaxValue + "\n";
                            s += "".PadLeft(padding) + "  EffectiveMinValue: " + uf.EffectiveMinValue + "\n";
                            s += "".PadLeft(padding) + "  EffectiveMaxValue: " + uf.EffectiveMaxValue + "\n";
                        }
                        s += "\n";
                        writer.WriteLine(s);
                        break;
                    case ObjectType.otReference:
                        throw new NotImplementedException(); // Shouldn't have a reference at this level of the document
                    default:
                        throw new NotImplementedException();    
                }
            }
        }

        static public void ToText( MetadataDocument document, string path )
        {
            using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.UTF8))
            {
                ItemsToText(document.Items, sw, document.CurrentContext, document.CurrentLanguage);
            }
        }
    }
}
