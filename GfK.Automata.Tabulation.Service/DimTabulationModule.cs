﻿using GfK.Automata.Pipeline.GapModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;
using System.Threading;
using System.Globalization;
using System.Diagnostics;
using GfK.Automata.Pipeline.Model.Domains;
using System.Runtime.Serialization.Formatters.Binary;

namespace GfK.Automata.Tabulation.Service
{
    public class DimTabulationModule : IModule
    {
        public ILogger<Pipeline.Model.Domains.ModuleLogMessageType> Logger { get; set; }

        public async Task<IParameterValueCollection> RunTask(IParameterValueCollection parameters)
        {
            if (consoleRan)
            {
                IParameterValueCollection returnParameters = new ParameterValueCollection();
                // create new temp folder to use
                Path = parameters.SystemParameters.GetNewTempFolder();
                await Tester(parameters, returnParameters);
                return returnParameters;
            }
            else
            {
                return await RunExe(parameters);
            }
        }
        string Path;
        string File;
        string Mdd;
        string Json;
        string Mrs;
        string language;
        string context;
        string mrsPath;
        string returnMrs;
        string returnMtd;
        string latestError = null;
        public bool consoleRan = false;
        DimBanners B;
        MetadataDocument D;
        ProcessorConfiguration C;
        DimTabPlan TP;
        public async Task Tester(IParameterValueCollection parameters, IParameterValueCollection returnParameters)
        {
            language = (await parameters.GetParameter("Language")).Value;
            context = (await parameters.GetParameter("Context")).Value;
            mrsPath = (await parameters.GetParameter("MrsPath")).Value;
            Mdd = Path + "\\" + System.IO.Path.GetFileNameWithoutExtension((await parameters.GetParameter("MddPath")).Value) + ".mdd";
            System.IO.File.Copy((await parameters.GetParameter("MddPath")).Value, Mdd);
            File = System.IO.Path.GetFileNameWithoutExtension(Mdd);
            //Json = Path + File + ".txt";
            Mrs = System.IO.Path.Combine(Path, File + ".mrs");
            D = Service.MDMReader.Read(Mdd, (await parameters.GetParameter("Version")).Value, MetadataSourceType.stDimensionsMDD);
            //D.Log(Json);
            C = new Model.ProcessorConfiguration();
            B = new DimBanners();
            B.Add("MONTHLY_BANNER", "banMonthly");
            B.Add("QUARTER_BANNER", "banQuarterly");
            C.Add("BANNERNAMES", B.Banners);

            C.Add("Document", D);
            C.Add("Language", (await parameters.GetParameter("Language")).Value);
            C.Add("Context", (await parameters.GetParameter("Context")).Value);
            C.Add("MRSPath", Mrs);
            C.Add("MDMPath", Mdd);
            C.Add("MDSC", "mrScriptMDSC");
            C.Add("MetadataVersion", (await parameters.GetParameter("Version")).Value);
            C.Add("CaseDataPath", (await parameters.GetParameter("DdfPath")).Value);
            C.Add("CDSC", "mrDataFileDSC");
            C.Add("Project", "");
            C.Add("MTDPath", System.IO.Path.Combine(Path, File + ".mtd"));
            C.Add("TableDoc", "TableDoc");
            // query data for the banner variable creation
            DimOLEDbConnection.SetConnectionString(language, context, Mdd, (await parameters.GetParameter("DdfPath")).Value, DimOLEDbConnection.dbCategoryReturnType.dbValues, DimOLEDbConnection.dbTableName.dbVDATA);
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            await Logger.LogAsync(Pipeline.Model.Domains.ModuleLogMessageType.INFORMATIONAL, ((DateTime)DimOLEDbConnection.GetMaxValue("DataCollection.FinishTime")).ToString());
            C.Add("MAXDATE", ((DateTime)DimOLEDbConnection.GetMaxDate("DataCollection.FinishTime")).ToString());
            C.Add("DATEVAR", "DataCollection.FinishTime");
            //C.Add("QUARTER_BANNER", "banQuarter");
            //C.Add("MONTHLY_BANNER", "banMonthly");
            C.Add("MDAT_BANNERS", (await parameters.GetParameter("BannersMddPath")).Value);

            TP = new DimTabPlan(C);
            DimOLEDbConnection.SetConnectionString(language, context, Mdd, (await parameters.GetParameter("DdfPath")).Value,  DimOLEDbConnection.dbCategoryReturnType.dbNames);
            C.Add("ProjectType", DimQueryData.GetProjectType());

            //D = (MetadataDocument)C["Document"];
            TestTabPlan(TP, language, context, mrsPath, (await parameters.GetParameter("DdfPath")).Value);
            WriteOut(mrsPath);

            await returnParameters.AddParameter(new ParameterString("DimTabulationModule", "MrsPath")
            {
                Value = Mrs
            });
            await returnParameters.AddParameter(new ParameterString("DimTabulationModule", "MtdPath")
            {
                Value = System.IO.Path.Combine(Path, File + ".mtd")
            });
        }
        private async Task<string> GetParametersForArguments(IParameterValueCollection parameters)
        {
            return "\"" + (await parameters.GetParameter("Language")).Value + "\""
                + " \"" + (await parameters.GetParameter("Context")).Value + "\""
                + " \"" + (await parameters.GetParameter("MrsPath")).Value + "\""
                + " \"" + (await parameters.GetParameter("MddPath")).Value + "\""
                + " \"" + (await parameters.GetParameter("Version")).Value + "\""
                + " \"" + (await parameters.GetParameter("DdfPath")).Value + "\""
                + " \"" + (await parameters.GetParameter("BannersMddPath")).Value + "\"";
        }
        private async Task<IParameterValueCollection> GetReturnParameters()
        {
            IParameterValueCollection returnParameters = new ParameterValueCollection();
            await returnParameters.AddParameter(new ParameterString("DimTabulationModule", "MrsPath")
            {
                Value = returnMrs
            });
            await returnParameters.AddParameter(new ParameterString("DimTabulationModule", "MtdPath")
            {
                Value = returnMtd
            });
            return returnParameters;
        }
        private async Task<IParameterValueCollection> RunExe(IParameterValueCollection parameters)
        {
            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "\\GfK.Automata.Tabulation.Debug.exe";
            process.StartInfo.Arguments = "RUNMODULE " + await GetParametersForArguments(parameters);
            await Logger.LogAsync(ModuleLogMessageType.INFORMATIONAL, process.StartInfo.Arguments);
            process.ErrorDataReceived += Process_ErrorDataReceived;
            process.OutputDataReceived += Process_OutputDataReceived;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            System.Threading.Thread.Sleep(60000);
            if (process.ExitCode != 0)
            {
                throw new Exception("exe exited with error code - " + process.ExitCode);
            }
            process.Close();
            process.Dispose();
            IParameterValueCollection returnParameters = await GetReturnParameters();
            if (latestError != null)
            {
                throw new Exception(latestError);
            }
            return returnParameters;
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                ModuleLogMessageType logType = ModuleLogMessageType.INFORMATIONAL;
                if (e.Data.ToUpper().StartsWith("ERROR"))
                {
                    latestError = e.Data;
                    logType = ModuleLogMessageType.ERROR;
                }
                else if (e.Data.ToUpper().Contains("WARNING"))
                {
                    logType = ModuleLogMessageType.WARNING;
                }
                ProcessLog(e.Data, logType).Wait();
            }
        }

        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                latestError = e.Data;
                ProcessLog(e.Data, ModuleLogMessageType.ERROR).Wait();
            }
        }
        private async Task ProcessLog(string log, ModuleLogMessageType logType)
        {
            if (log != null && log.Trim() != "")
            {
                if (log.StartsWith("RETURNMRS~") && log.Split('~').Length == 2)
                {
                    returnMrs = log.Split('~')[1];
                }
                if (log.StartsWith("RETURNMTD~") && log.Split('~').Length == 2)
                {
                    returnMtd = log.Split('~')[1];
                }
                else
                {
                    await Logger.LogAsync(logType, log);
                }
            }
        }
        //private void _TestOleDB()
        //{
        //    string VarName = null;
        //    // https://www.ibm.com/support/knowledgecenter/en/SSLVQG_7.0.0/com.spss.ddl/mroledb_ref_conprops.htm
        //    DimOLEDbConnection.SetConnectionString(language, context, Mdd, DimOLEDbConnection.dbCategoryReturnType.dbNames);

        //    //// Max & Min
        //    VarName = "AGE";
        //    Console.WriteLine(String.Format("{0} Max : " + DimOLEDbConnection.GetMaxValue(VarName).ToString() + " Min : " + DimOLEDbConnection.GetMinValue(VarName).ToString(), VarName));
        //    // Distinct values
        //    VarName = "DataCollection.FinishTime";
        //    Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetDistinctValues(VarName)));
        //    // All values
        //    VarName = "OCC1";
        //    Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetValues(VarName)));
        //    Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetDistinctValues("SEX")));
        //    Console.WriteLine("Press any key to exit...");
        //    // GetPercentileRange
        //    VarName = "AGE";
        //    Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetPercentileRange(VarName, 0, 0.33)));
        //    Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetPercentileValues(VarName, .33, .50)));
        //    Console.WriteLine(String.Join("\n", DimOLEDbConnection.GetPercentileRange(VarName, .33, 0.50)));
        //    Console.ReadKey();

        //}


        public void WriteOut(string path)
        {
            using (System.IO.StreamWriter sw = new StreamWriter(path, false, Encoding.UTF8))
            {
                sw.WriteLine(TP.GetScript(ScriptType.stDimensions));
            }

            Console.WriteLine("Output MRS available at: " + path);

        }

        //public void TestField(string fieldName)
        //{

        //    // sets the initial connection string in case need to use it later
        //    DimOLEDbConnection.SetConnectionString(language, context, Mdd, DimOLEDbConnection.dbCategoryReturnType.dbNames);
        //    MetadataObject f = (MetadataObject)D.Find(fieldName);
        //    if (f != null)
        //    {

        //        Service.DimTabulationProcessor.Process(f, TP, language, context);
        //        string path = mrsPath.Replace(".mrs", "_" + fieldName + ".mrs");
        //        WriteOut(path);
        //    }
        //    else
        //    {
        //        Console.WriteLine(String.Format("{0} is not a valid field in the MDD", fieldName));
        //    }
        //}

        public void TestConfiguration()
        {

            Service.DimTabulationProcessor.Process(C);
        }
        public void TestTabPlan(DimTabPlan tp, string language, string context, string mrsPath, string ddfPath)
        {
            // processes everything.
            // sets the initial connection string in case need to use it later
            DimOLEDbConnection.SetConnectionString(language, context, Mdd, ddfPath, DimOLEDbConnection.dbCategoryReturnType.dbNames);
            Service.DimTabulationProcessor.Process(C);
            //Service.DimTabulationProcessor.Process(tp, D, language, context, mrsPath);
        }
    }
}
