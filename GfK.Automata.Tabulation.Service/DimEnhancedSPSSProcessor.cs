﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using GfK.Automata.Model;
using GfK.Automata.Tabulation.Model;
using ClosedXML.Excel;
using SpssLib.SpssDataset;

namespace GfK.Automata.Tabulation.Service
{
    public class DimEnhancedSPSSProcessor
    {
        private static HashSet<string> _deletions;
        private static HashSet<string> _fragments;
        private static string _lastBaseName;
        private static int _lastRow;
        private static HashSet<string> _names;
        private static int _order;
        private static HashSet<string> _right;
        private static IXLWorksheet _sheet;
        private static Dictionary<string, int> _columns;

        static DimEnhancedSPSSProcessor()
        {
            _deletions = new HashSet<string>();
            _deletions.Add("averagetime");
            _deletions.Add("boff");
            _deletions.Add("bon");
            _deletions.Add("comp");
            _deletions.Add("comp_redir");
            _deletions.Add("companyname");
            _deletions.Add("daynow");
            _deletions.Add("daynow2");
            _deletions.Add("dumtemptot");
            _deletions.Add("dumtimeoutcounter");
            _deletions.Add("end_of_survey");
            _deletions.Add("endins");
            _deletions.Add("fraudflag");
            _deletions.Add("gfkcc");
            _deletions.Add("gfkcountrycode");
            _deletions.Add("gfkdedupe");
            _deletions.Add("gfkdupeflag");
            _deletions.Add("gfkduplicaterespondent");
            _deletions.Add("gfkfiles");
            _deletions.Add("helpemail");
            _deletions.Add("id");
            _deletions.Add("interviewcount");
            _deletions.Add("interviewelapsedtime");
            _deletions.Add("intlen");
            _deletions.Add("log_error");
            _deletions.Add("monthnow");
            _deletions.Add("monthnow2");
            _deletions.Add("mp");
            _deletions.Add("myquot");
            _deletions.Add("postcard");
            _deletions.Add("proj");
            _deletions.Add("projectno");
            _deletions.Add("qfillflag");
            _deletions.Add("questionsanswered");
            _deletions.Add("quotapath");
            _deletions.Add("quotatotal_source");
            _deletions.Add("quval");
            _deletions.Add("respid");
            _deletions.Add("respondentid");
            _deletions.Add("samplesource");
            _deletions.Add("sessionstarttime");
            _deletions.Add("sp");
            _deletions.Add("subquotawebmd");
            _deletions.Add("stins");
            _deletions.Add("surveypath");
            _deletions.Add("surveytotaltiming");
            _deletions.Add("templatestyle");
            _deletions.Add("termflag");
            _deletions.Add("timetak");
            _deletions.Add("timingaudit");
            _deletions.Add("tmplt");
            _deletions.Add("txtinse");
            _deletions.Add("txtinss");
            _deletions.Add("uscfiles");
            _deletions.Add("weekdaynow");
            _deletions.Add("weekdaynow2");
            _deletions.Add("weeknow");
            _deletions.Add("weeknow2");
            _deletions.Add("yearnow");
            _deletions.Add("yearnow2");

            _fragments = new HashSet<string>(); // variable name left-side fragments that flag variables to be removed
            _fragments.Add("browser_");
            _fragments.Add("browser.");
            _fragments.Add("cln.");
            _fragments.Add("companyname");
            _fragments.Add("cmk.");
            _fragments.Add("dum_");
            _fragments.Add("erewards.");
            _fragments.Add("err_");
            _fragments.Add("eyed");
            _fragments.Add("flatlinedata_");
            _fragments.Add("gmi.");
            _fragments.Add("gol.");
            _fragments.Add("ins_");
            _fragments.Add("mrk_");
            _fragments.Add("myp.");
            _fragments.Add("nur_");
            _fragments.Add("oemrk_");
            _fragments.Add("otx.");
            _fragments.Add("prg_");
            _fragments.Add("progressbar_");
            _fragments.Add("progressbar.");
            _fragments.Add("rsn.");
            _fragments.Add("rxn.");
            _fragments.Add("scz.");
            _fragments.Add("quota_");
            _fragments.Add("qta_");
            _fragments.Add("qw_");
            _fragments.Add("redirect_");
            _fragments.Add("smt_");
            _fragments.Add("ssi.");
            _fragments.Add("surveytiming_");
            _fragments.Add("templatestyle");
            _fragments.Add("tpd_");
            _fragments.Add("txt_");
            _fragments.Add("uns.");
            _fragments.Add("url_");
            _fragments.Add("wws.");

            _names = new HashSet<string>();

            _order = 0;

            _right = new HashSet<string>();
            _right.Add("_ask");
            _right.Add("_asked");
            _right.Add("_cemrk");

            _columns = new Dictionary<string, int>();
            _columns.Add( "Dim Variable Name", 1 );
            _columns.Add( "Include In SAV", 2 );
            _columns.Add( "Order", 3 );
            _columns.Add( "Data Type", 4 );
            _columns.Add( "Min", 5 );
            _columns.Add( "Max", 6 );
            _columns.Add( "SAV Variable Name", 7 );
            _columns.Add( "Dim Category Name", 8 );
            _columns.Add( "SAV Category Value", 9 );
            _columns.Add( "SAV Label", 10 );
        }

        public static string CleanLabel( string label )
        {
            label = Regex.Replace(label, @"<[^>]+>|&nbsp;", " ");
            label = Regex.Replace(label, @"\s{2,}", " ");
            return label;
        }

        public static void CreateLayoutFile( MetadataDocument document, string pathToXLSXLayout, string language, string context )
        {
            Console.WriteLine("INFO: Creating Layout File");
            var workbook = new XLWorkbook();
            _sheet = workbook.Worksheets.Add("SAV Mapping Definitions");
            CreateLayoutFileHeader(language, context);
            CreateLayoutFileItems(language, context, document.Items);
            CreateLayoutFileFooter(language, context);
            FinalizeLayoutFile();
            try
            {
                workbook.SaveAs(pathToXLSXLayout);
            }
            catch ( System.IO.IOException )
            {
                Console.WriteLine("ERROR: Can not write to " + pathToXLSXLayout);
                Console.WriteLine("       If the file is currently open in Excel, please close the file and re-run this process.");
                Environment.Exit(1);
            }
        }

        public static void CreateLayoutFileFooter( string language, string context )
        {

        }

        public static void CreateLayoutFileItem( string language, string context, Field item, int index = 0 )
        {
            int newrow = _sheet.RowsUsed().Count() + 1;
            string basename = GetSAVVariableName(item.Name, index);
            _sheet.Cell(newrow, 1).Value = item.Name;
            _sheet.Cell(newrow, 2).Value = GetDefaultIncludeVariable(item);
            _sheet.Cell(newrow, 3).Value = ++_order;
            _sheet.Cell(newrow, 4).Value = item.DataType.ToString().Substring(2);
            _sheet.Cell(newrow, 5).SetDataType(XLCellValues.Number);
            try
            {
                _sheet.Cell(newrow, 5).Value = item.MinValue ?? item.EffectiveMinValue;
            }
            catch( System.OverflowException e )
            {
                _sheet.Cell(newrow, 5).Value = Decimal.MinValue;
            }
            _sheet.Cell(newrow, 6).SetDataType(XLCellValues.Number);
            try
            {
                _sheet.Cell(newrow, 6).Value = item.MaxValue ?? item.EffectiveMaxValue;
            }
            catch( System.OverflowException e )
            {
                _sheet.Cell(newrow, 6).Value = Decimal.MaxValue;
            }
            _sheet.Cell(newrow, 7).Value = basename;
            _sheet.Cell(newrow, 10).Value = CleanLabel(item.Label(context, language, item.Document));
            _lastRow = newrow;

            if (item.DataType == Automata.Model.DataType.dtCategorical)
            {
                bool isSinglePunch = true;
                if ((item.MaxValue ?? item.EffectiveMaxValue) > 1)
                {
                    isSinglePunch = false;
                }
                if (!isSinglePunch) _sheet.Cell(newrow, 7).Value = "";
                BoundField bound = (BoundField)item;
                Int64 value = 0;
                HashSet<Int64> values = new HashSet<Int64>();
                foreach (Category category in bound.Categories())
                {
                    newrow++;
                    if (!isSinglePunch)
                    {
                        value = 1;
                        string thisvarname = GetSAVVariableName(basename + "_" + category.Name);
                        _sheet.Cell(newrow, 7).Value = thisvarname;
                    }
                    else
                    {
                        if (category.Properties.ContainsKey("value"))
                        {
                            if (values.Contains((int)category.Properties["value"]))
                            {
                                Console.WriteLine("WARNING: Duplicate value property detected in " + item.Name + " - auto assigning value");
                                value = GetNextUnusedValue(values);
                            }
                            else
                            {
                                value = (Int64)category.Properties["value"];
                            }
                        }
                        else
                        {
                            value = GetNextUnusedValue(values);
                        }
                    }
                    values.Add(value);
                    _sheet.Cell(newrow, 8).Value = category.Name;
                    _sheet.Cell(newrow, 9).Value = value;
                    _sheet.Cell(newrow, 10).Value = CleanLabel(category.Label(context, language, bound.Document));
                }
            }
        }

        public static void CreateLayoutFileItems( string language, string context, List<MetadataObject> items, int index = 0 )
        {
            foreach( MetadataObject obj in items )
            {
                switch ( obj.ObjectType )
                {
                    case ObjectType.otCollection:
                        CreateLayoutFileItems(language, context, obj.Items);
                        break;
                    case ObjectType.otDefinedList:
                        // do nothing
                        break;
                    case ObjectType.otField:
                        Field field = (Field)obj;
                        //if (!field.HasCaseData) return;
                        if ( field.HasCaseData ) CreateLayoutFileItem(language, context, field, index);
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public static void CreateLayoutFileHeader( string language, string context )
        {
            _sheet.Cell("A1").Value = "Dim Variable Name";
            _sheet.Cell("B1").Value = "Include In SAV";
            _sheet.Cell("C1").Value = "Order";
            _sheet.Cell("D1").Value = "Data Type";
            _sheet.Cell("E1").Value = "Min";
            _sheet.Cell("F1").Value = "Max";
            _sheet.Cell("G1").Value = "SAV Variable Name";
            _sheet.Cell("H1").Value = "Dim Category Name";
            _sheet.Cell("I1").Value = "SAV Category Value";
            _sheet.Cell("J1").Value = "SAV Label (" + context + "-" + language + ")";
            _sheet.Row(1).Style.Font.Bold = true;
            _sheet.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        }

        public static void FinalizeLayoutFile()
        {
            Console.WriteLine("INFO: Finalizing layout file");
            _sheet.Style.Alignment.WrapText = true;
            _sheet.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
            Console.WriteLine("INFO: Setting column widths");
            _sheet.Column("A").Width = 45;
            _sheet.Column("B").Width = 15;
            _sheet.Column("C").Width = 10;
            _sheet.Column("D").Width = 15;
            _sheet.Column("E").Width = 15;
            _sheet.Column("F").Width = 15;
            _sheet.Column("G").Width = 30;
            _sheet.Column("H").Width = 30;
            _sheet.Column("I").Width = 20;
            _sheet.Column("J").Width = 40;
            Console.WriteLine("INFO: Setting alignment");
            _sheet.Range("B:F").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            _sheet.SheetView.FreezeRows(1);
            Console.WriteLine("INFO: Protecting cells");
            _sheet.Protect();
            int lastrow = _sheet.LastRowUsed().RowNumber();
            _sheet.Column("A").Style.Protection.SetLocked(true);
            _sheet.Range("A1:A" + lastrow.ToString()).Style.Fill.SetBackgroundColor(XLColor.FromHtml("#eee"));
            _sheet.Range("A1:A" + lastrow.ToString()).Style.Font.SetFontColor(XLColor.FromHtml("#333"));
            _sheet.Column("B").Style.Protection.SetLocked(false);
            _sheet.Column("C").Style.Protection.SetLocked(false);
            _sheet.Column("D").Style.Protection.SetLocked(true);
            _sheet.Range("D1:D" + lastrow.ToString()).Style.Fill.SetBackgroundColor(XLColor.FromHtml("#eee"));
            _sheet.Range("D1:D" + lastrow.ToString()).Style.Font.SetFontColor(XLColor.FromHtml("#333"));
            _sheet.Column("E").Style.Protection.SetLocked(true);
            _sheet.Range("E1:E" + lastrow.ToString()).Style.Fill.SetBackgroundColor(XLColor.FromHtml("#eee"));
            _sheet.Range("E1:E" + lastrow.ToString()).Style.Font.SetFontColor(XLColor.FromHtml("#333"));
            _sheet.Column("F").Style.Protection.SetLocked(true);
            _sheet.Range("F1:F" + lastrow.ToString()).Style.Fill.SetBackgroundColor(XLColor.FromHtml("#eee"));
            _sheet.Range("F1:F" + lastrow.ToString()).Style.Font.SetFontColor(XLColor.FromHtml("#333"));
            _sheet.Column("G").Style.Protection.SetLocked(false);
            _sheet.Column("H").Style.Protection.SetLocked(true);
            _sheet.Range("H1:H" + lastrow.ToString()).Style.Fill.SetBackgroundColor(XLColor.FromHtml("#eee"));
            _sheet.Range("H1:H" + lastrow.ToString()).Style.Font.SetFontColor(XLColor.FromHtml("#333"));
            _sheet.Column("I").Style.Protection.SetLocked(false);
            _sheet.Column("J").Style.Protection.SetLocked(false);
            Console.WriteLine("INFO: Done finalizing layout file");
        }

        public static string GetSAVVariableName( string name, int index = 0 )
        {
            //string prefix = string.Empty;
            //string tmp1 = string.Empty;
            //int periods = name.Count(o => o == '.');
            //int brackets = name.Count(o => o == '[');
            //if (periods > 0)
            //{
            //    prefix = name.Substring(0, name.IndexOf('.')).ToLower();
            //}
            //if ( brackets > 0 )
            //{
            //    tmp1 = name.Substring(0, name.IndexOf('[')).ToLower();
            ///}
            //if (tmp1.Length > 0 && tmp1.Length < prefix.Length) prefix = tmp1;
            //name = name.Substring(name.LastIndexOf('.') + 1); // remove everything before last period
            //if (!prefix.Equals(string.Empty)) name = prefix;
            Regex r1 = new Regex("[^a-zA-Z0-9]"); // remove non alphas
            name = r1.Replace(name, "").ToLower(); // convert to lower
            char[] chars = name.ToCharArray(); 
            if (!Char.IsLetter(chars[0])) name = "@" + name; // add @ if first character is not a letter
            if (name == "all") name = "@" + name; // reserved word
            if (name == "and") name = "@" + name; // reserved word
            if (name == "by") name = "@" + name; // reserved word
            if (name == "eq") name = "@" + name; // reserved word
            if (name == "ge") name = "@" + name; // reserved word
            if (name == "gt") name = "@" + name; // reserved word
            if (name == "le") name = "@" + name; // reserved word
            if (name == "lt") name = "@" + name; // reserved word
            if (name == "ne") name = "@" + name; // reserved word
            if (name == "not") name = "@" + name; // reserved word
            if (name == "or") name = "@" + name; // reserved word
            if (name == "to") name = "@" + name; // reserved word
            if (name == "with") name = "@" + name; // reserved word
            if (name.Length > 32) name = name.Substring(0, 32); // deal with long variable names
            string varname = name;
            // test index modification
            if ( index == 1 && _lastBaseName == name )
            {
                string test = varname + "_1";
                if (!_names.Contains(test))
                {
                    _names.Add(test);
                    _sheet.Cell(_lastRow, 7).Value = test;
                }
                index++;
            }
            // end index modification
            _lastBaseName = name;
            if (index > 0) varname += "_" + index.ToString();
            if (_names.Contains(varname))
            {
                return GetSAVVariableName(name, index + 1);
            }
            else
            {
                _names.Add(varname);
                return varname;
            }            
        }

        public static string GetDefaultIncludeVariable( Field f )
        {
            string include;
            if ( f.IsSystem == true )
            {
                switch( f.Name )
                {
                    case "Respondent.Serial":
                    case "DataCollection.FinishTime":
                    case "DataCollection.EndQuestion":
                        include = "Yes";
                        break;
                    default:
                        include = "No";
                        break;
                }
            }
            else
            {
                if (f.DataType == Automata.Model.DataType.dtText)
                {
                    include = "No";
                }
                else
                {
                    include = IncludeDataVariable(f.Name);
                }
            }
            return include;
        }

        public static Int64 GetNextUnusedValue(HashSet<Int64> values )
        {
            Int64 i = new Int64();
            for( i = 1; i <= 2147483648; i++ ) // max of numeric value in SPSS (overkill, I know, but loop should exit fast)
            {
                if (!values.Contains(i)) return i;
            }
            return 0;
        }

        public static string IncludeDataVariable( string name )
        {
            string lcname = name.ToLower();
            if (_deletions.Contains(lcname)) return "No";
            for( int i = 2; i <= 22; i++ )
            {
                if (IncludeDataVariableCheckLeft(i, lcname) == "No") return "No";
            }
            for( int i = 4; i <= 5; i++ )
            {
                if (IncludeDataVariableCheckRight(i, lcname) == "No") return "No";
            }
            return "Yes";
        }

        public static string IncludeDataVariableCheckLeft(int len, string fragment)
        {
            if (fragment.Length >= len)
            {
                fragment = fragment.Substring(0, len);
                if (_fragments.Contains(fragment)) return "No";
            }
            return "Yes";
        }

        public static string IncludeDataVariableCheckRight(int len, string fragment)
        {
            if (fragment.Length >= len)
            {
                int start = fragment.Length - len;
                fragment = fragment.Substring(start);
                if (_right.Contains(fragment)) return "No";
            }
            return "Yes";
        }

        public static void Process( ProcessorConfiguration c )
        {
            string context = (string)c["Context"];
            MetadataDocument document = (MetadataDocument)c[ "Document" ];
            string language = (string)c["Language"];
            string layoutXLSX = (string)c["LayoutFilePath"];
            string savPath = (string)c["SAVOutputPath"];
            if (((bool)c[ "GenerateLayout" ]) == true )
            {
                CreateLayoutFile(document, layoutXLSX, language, context);
            } 
            else
            {
                var workbook = new XLWorkbook(layoutXLSX);
                _sheet = workbook.Worksheet("SAV Mapping Definitions");
            }
            CreateSPSSFile(c[ "MDDFilePath" ], c[ "DDFFilePath" ], c[ "SAVOutputPath" ], c[ "Language" ] );
        }

        public static void CreateSPSSFile( string pathToMDDFile, string pathToDDFFile, string pathToSAVOutput, string language )
        {
            Console.WriteLine("INFO: Creating SPSS File");
            List<MappedVariable> MappedVariables = _getVariableMappings();
            var SPSSVariables = _getSPSSVariables(MappedVariables);
            var SPSSOptions = new SpssLib.DataReader.SpssOptions();
            OleDbConnection conn = _connectToDataSource(pathToMDDFile, pathToDDFFile, language);
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM VDATA", conn);
            OleDbDataReader reader = cmd.ExecuteReader();
            using (FileStream stream = new FileStream(pathToSAVOutput, FileMode.Create, FileAccess.Write))
            {
                using (var writer = new SpssLib.DataReader.SpssWriter(stream, SPSSVariables, SPSSOptions))
                {
                    int i = 0;
                    while( reader.Read())
                    {
                        i += 1;
                        _progress(i);
                        var newRecord = writer.CreateRecord();
                        for( int j = 0; j < SPSSVariables.Count(); j++ )
                        {
                            Variable thisVariable = SPSSVariables[j];
                            MappedVariable mv = MappedVariables.Find(o => o.DestinationVariableName == thisVariable.Name );
                            dynamic value = reader[mv.SourceVariableName];
                            if (!DBNull.Value.Equals(value))
                            {
                                switch( mv.Type.Trim().ToLower() )
                                {
                                    case "boolean":
                                        newRecord[j] = (double)Convert.ToInt16(value);
                                        break;
                                    case "categorical":
                                        value = ((string)value).Replace("{", "");
                                        value = ((string)value).Replace("}", "").Trim();
                                        if (((string)value).Length > 0)
                                        {
                                            if (mv.BooleanCategory is null)
                                            {
                                                newRecord[j] = (double)mv.Values.Find(o => ((string)o.SourceValue).ToLower() == ((string)value).ToLower()).DestinationValue;
                                            }
                                            else
                                            {
                                                string[] tmp = ((string)value).Split(',');
                                                bool isSet = false;
                                                foreach (string val in tmp)
                                                {
                                                    if (val == mv.BooleanCategory)
                                                    {
                                                        newRecord[j] = 1d;
                                                        isSet = true;
                                                    }
                                                }
                                                if (!isSet && tmp.Length > 0) newRecord[j] = 0d;
                                            }
                                        }
                                        else
                                        {
                                            newRecord[j] = null;
                                        }
                                        break;
                                    case "date":
                                        newRecord[j] = (Convert.ToDateTime(value) - new DateTime(1582, 10, 14)).TotalSeconds;
                                        break;
                                    case "double":
                                    case "long":
                                        newRecord[j] = (double)value;
                                        break;
                                    case "text":
                                        newRecord[j] = (string)value;
                                        break;
                                    default:
                                        throw new NotImplementedException("Data type not implemented: " + mv.Type);
                                }
                            }
                            else
                            {
                                newRecord[j] = null;
                            }
                        }
                        writer.WriteRecord(newRecord);
                    }
                    Console.WriteLine("INFO: " + i.ToString() + " total records processed");
                    reader.Close();
                    writer.EndFile();
                }
            }
            conn.Close();
        }

        private static void _progress( int i )
        {
            if (i % 1000 == 0) Console.WriteLine("INFO: " + i.ToString() + " records processed");
        }

        private static OleDbConnection _connectToDataSource( string pathToMDDFile, string pathToDDFFile, string language )
        {
            try
            {
                string connString = "Provider=mrOleDB.Provider.2;";
                connString += String.Format("Data Source=mrDataFileDSC;");
                connString += String.Format("MR Init Category Names=1;"); 
                connString += String.Format("MR Init MDM Language={0};", language);
                connString += String.Format("Initial Catalog={0};", pathToMDDFile);
                connString += String.Format("Location={0}", pathToDDFFile);
                OleDbConnection conn = new OleDbConnection(connString);
                conn.Open();
                return conn;
            }
            catch( Exception e )
            {
                Console.WriteLine("ERROR: " + e.Message);
                return null;
            }
        }

        private static List<MappedVariable> _getVariableMappings()
        {
            Console.WriteLine("INFO: Getting variable mappings from worksheet");
            List<MappedVariable> variables = new List<MappedVariable>();
            int lastusedrow = _sheet.LastRowUsed().RowNumber();
            IXLRow row;
            string lastDimVarName = string.Empty;
            string dataType = string.Empty;
            for (int i = 2; i <= lastusedrow; i++)
            {
                row = _sheet.Row(i);
                if (row.Cell(_columns["Include In SAV"]).GetValue<string>().Trim().ToLower() == "yes")
                {
                    dataType = row.Cell(_columns["Data Type"]).GetValue<string>().Trim().ToLower();
                    lastDimVarName = row.Cell(_columns["Dim Variable Name"]).GetValue<string>().Trim();
                    switch ( dataType )
                    {
                        case "categorical":
                            if (row.Cell(_columns["Max"]).GetValue<int>() == 1 )
                            {
                                i = _addCategoricalSingleVariableMapping(row, variables, dataType);
                            }
                            else
                            {
                                i = _addCategoricalMultiVariableMapping(row, variables, dataType, lastDimVarName, row.Cell(_columns["SAV Label"]).GetValue<string>().Trim());
                            }
                            break;
                        case "boolean":
                        case "date":
                        case "double":
                        case "long":
                        case "text":
                            _addSimpleVariableMapping(row, variables, dataType);
                            break;
                        default:
                            throw new NotImplementedException("Data type " + dataType + " not implemented");
                    }
                }
            }
            return variables;
        }

        private static int _addCategoricalSingleVariableMapping( IXLRow row, List<MappedVariable> variables, string dataType )
        {
            MappedVariable var = _addSimpleVariableMapping(row, variables, dataType);
            int nextRow = row.RowNumber() + 1;
            int lastusedrow = _sheet.LastRowUsed().RowNumber();
            int returnValue = lastusedrow;
            for (int i = nextRow; i >= nextRow && i <= lastusedrow; i++)
            {
                row = _sheet.Row(i);
                if (row.Cell(_columns["Include In SAV"]).GetValue<string>().Trim().Length > 0)
                {
                    returnValue = i - 1;
                    i = -1;
                }
                else
                {
                    var.Values.Add(new MappedValue(row.Cell(_columns["Dim Category Name"]).GetValue<string>().Trim(), row.Cell(_columns["SAV Category Value"]).GetValue<int>(), row.Cell(_columns[ "SAV Label" ]).GetValue<string>()));
                }
            }
            return returnValue;
        }

        private static int _addCategoricalMultiVariableMapping( IXLRow row, List<MappedVariable> variables, string dataType, string lastDimVarName, string variableLabel )
        {
            int nextRow = row.RowNumber() + 1;
            int lastusedrow = _sheet.LastRowUsed().RowNumber();
            int returnValue = lastusedrow;
            for (int i = nextRow; i >= nextRow && i <= lastusedrow; i++)
            {
                row = _sheet.Row(i);
                if (row.Cell(_columns["Include In SAV"]).GetValue<string>().Trim().Length > 0)
                {
                    returnValue = i - 1;
                    i = -1;
                }
                else
                {
                    if (variables.FindIndex(o => o.DestinationVariableName == row.Cell(_columns["SAV Variable Name"]).GetValue<string>().Trim()) >= 0)
                    {
                        Console.WriteLine("ERROR: Duplicate variable name found (" + row.Cell(_columns["SAV Variable Name"]).GetValue<string>().Trim() + ")");
                        Environment.Exit(1);
                    }
                    else
                    {
                        Console.WriteLine("INFO: Adding mapping for " + row.Cell(_columns["SAV Variable Name"]).GetValue<string>().Trim() );
                        string categoryLabel = row.Cell(_columns["SAV Label"]).GetValue<string>().Trim();
                        MappedVariable var = new MappedVariable(lastDimVarName, row.Cell(_columns["SAV Variable Name"]).GetValue<string>().Trim(), dataType, variableLabel + " - " + categoryLabel, "1", row.Cell(_columns["Dim Category Name"]).GetValue<string>().Trim());
                        var.Values.Add(new MappedValue(row.Cell(_columns["Dim Category Name"]).GetValue<string>().Trim(), 0, "Not selected"));
                        var.Values.Add(new MappedValue(row.Cell(_columns["Dim Category Name"]).GetValue<string>().Trim(), 1, categoryLabel));
                        variables.Add(var);
                    }
                }
            }
            return returnValue;
        }

        private static MappedVariable _addSimpleVariableMapping(IXLRow row, List<MappedVariable> variables, string dataType)
        {
            MappedVariable var;
            if (variables.FindIndex(o => o.DestinationVariableName == row.Cell(_columns["SAV Variable Name"]).GetValue<string>().Trim()) >= 0)
            {
                Console.WriteLine("ERROR: Duplicate variable name found (" + row.Cell(_columns["SAV Variable Name"]).GetValue<string>().Trim() + ")");
                var = null;
            }
            else
            {
                Console.WriteLine("INFO: Adding mapping for " + row.Cell(_columns["SAV Variable Name"]).GetValue<string>().Trim());
                string max = row.Cell(_columns["Max"]).GetValue<string>().Trim();
                var = new MappedVariable(row.Cell(_columns["Dim Variable Name"]).GetValue<string>().Trim(), row.Cell(_columns["SAV Variable Name"]).GetValue<string>().Trim(), dataType, row.Cell(_columns["SAV Label"]).GetValue<string>().Trim(), max);
                variables.Add(var);
            }
            return var;
        }

        private static List<Variable> _getSPSSVariables( List<MappedVariable> mappedVariables )
        {
            Console.WriteLine("INFO: Getting SPSS Variables from variable mappings");
            List<Variable> variables = new List<Variable>();
            foreach( MappedVariable var in mappedVariables )
            {
                Variable v = new Variable( var.DestinationVariableName );
                v.Label = var.Label;
                OutputFormat format;
                SpssLib.SpssDataset.DataType type;
                int width;
                int textwidth = 0;
                Alignment alignment = Alignment.Right;
                switch ( var.Type )
                {
                    case "boolean":
                        format = new OutputFormat(FormatType.F, 3, 1);
                        type = SpssLib.SpssDataset.DataType.Numeric;
                        width = 3;
                        break;
                    case "categorical":
                        type = SpssLib.SpssDataset.DataType.Numeric;
                        width = var.Max.Length + 2;
                        format = new OutputFormat(FormatType.F, width, 1);
                        if (var.Values.Count > 0)
                        {
                            Dictionary<double, string> labels = new Dictionary<double, string>();
                            foreach (MappedValue value in var.Values)
                            {
                                labels.Add(value.DestinationValue, value.Label);
                            }
                            v.ValueLabels = labels;
                        }
                        break;
                    case "date":
                        format = new OutputFormat(FormatType.DATETIME, 20);
                        type = SpssLib.SpssDataset.DataType.Numeric;
                        width = 20;
                        break;
                    case "double":
                        format = new OutputFormat(FormatType.F, 40, 16);
                        type = SpssLib.SpssDataset.DataType.Numeric;
                        width = 40;
                        break;
                    case "long":
                        format = new OutputFormat(FormatType.F, 12, 0);
                        type = SpssLib.SpssDataset.DataType.Numeric;
                        width = 12;
                        break;
                    case "text":
                        type = SpssLib.SpssDataset.DataType.Text;
                        width = Convert.ToInt32( var.Max );
                        format = new OutputFormat(FormatType.A, width);
                        textwidth = width;
                        alignment = Alignment.Left;
                        break;
                    default:
                        throw new NotImplementedException("Data type " + var.Type + " not implemented");
                }
                v.Alignment = alignment;
                v.PrintFormat = format;
                v.WriteFormat = format;
                v.Type = type;
                v.Width = width;
                if (textwidth > 0) v.TextWidth = width;
                //v.MissingValueType = MissingValueType.OneDiscreteMissingValue;
                variables.Add(v);
            }
            return variables;
        }
    }
}
