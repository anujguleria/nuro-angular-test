﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GfK.Automata.Model;
using GfK.Automata.Pattern.Model;
using GfK.Automata.Pattern.Service;
using GfK.Automata.Tabulation.Model;

namespace GfK.Automata.Tabulation.Service
{

    public static class DimTabulationProcessor
    {

        public static Dictionary<string, DimFilter> Filters { get; set; }
        static List<string> SpecialFields = new List<string>();
        public static ProcessorConfiguration Configuration { get; set; }
        
        static DimTabulationProcessor()
        {   
            //SpecialFields.Add("AE_Loop");
            SpecialFields.Add("SBAW1");
            
        }
        
        static public void Process( ProcessorConfiguration c )
        {
            DimTabPlan tp = new DimTabPlan(c);
            DimFunnelTables.Items.Clear(); 
            MetadataDocument document = (MetadataDocument)c["Document"];
            ProcessorConfiguration.Document = document;
            
            DimCustomerHarmonicsController.Init(document);
            string language = (string)c["Language"];
            string context = (string)c["Context"];
            string mrsPath = (string)c["MRSPath"];
            Filters = new DimFilters(document).Items;
            Process(c, tp, document, language, context, mrsPath);
            Process(c, tp, DimFunnelTables.Items);
        }

        private static void Process(ProcessorConfiguration c, DimTabPlan tp, Dictionary<string, DimFunnelTable> items)
        {
            foreach(DimFunnelTable tbl in items.Values)
            {
                tbl.BuildAxis();
                if (tbl.Side != null)
                { tp.Add(tbl); }
                
            }
        }

        static public void Process( ProcessorConfiguration c, MetadataObject obj)
        {
            // for testing
            DimTabPlan tp = new DimTabPlan(c);
            MetadataDocument document = (MetadataDocument)c["Document"];
            ProcessorConfiguration.Document = document;
            string language = (string)c["Language"];
            string context = (string)c["Context"];
            string mrsPath = (string)c["MRSPath"];
            Process(obj, tp, language, context);
        }
        static public void Process(ProcessorConfiguration c, DimTabPlan tabPlan, MetadataDocument document, string language, string context, string mrsPath )
        {
            Configuration = c;
            Process(document.Items, tabPlan, language, context);
            using (StreamWriter sw = new StreamWriter(mrsPath, false, Encoding.UTF8))
            {
                sw.WriteLine(tabPlan.GetScript(ScriptType.stDimensions));
            }
        }
        static public void Process(List<MetadataObject> items, DimTabPlan tp, string language, string context)
        {
            foreach (MetadataObject obj in items)
            {
                Process(obj, tp, language, context);
            }
        }

        static public void Process(Container container, DimTabPlan tp, string language, string context)
        {
            // handles grid/loop objects
            if (_processContainer(container) == false) { return;  }

            foreach (MetadataObject obj in container.Items)
            {
                if (obj.ObjectType == ObjectType.otCollection)
                {
                    Process(obj, tp, language, context, container);
                }
                else
                {
                    Process(obj, tp, language, context, container);
                }
            }
        }
        static public void Process(List<MetadataObject> obj, DimTabPlan tp, string language, string context, Container container = null, ProcessorConfiguration C = null)
        {
            MetadataDocument document = (MetadataDocument)C["Document"];
            
            ProcessorConfiguration.Document = document;
            foreach (MetadataObject o in obj)
            {
                Process(o, tp, language, context, container, C);
            }
            Process(C, tp, DimFunnelTables.Items);
        }
        static public void Process(MetadataObject obj, DimTabPlan tp, string language, string context, Container container = null, ProcessorConfiguration C = null)
        { 
            if (Configuration is null)
            {
                Configuration = C;
            }
            if (Filters is null)
            {
                // Debug option. Filters.Count >= 1 as long as ANY filters are defined in that class
                // exception only for debugging when debugging a single field, this Dict is not 
                // created yet, so we create it here .
                MetadataDocument doc = obj.Document;
                Filters = new DimFilters(obj.Document).Items;
            }

            switch ( obj.ObjectType )
            {
                case ObjectType.otCollection:
                    //Process(obj.Items, tp, language, context);
                    switch (DimTabPlan.GetQType((Container)obj))
                    {
                        case "ADCONX3_M":
                            List<IScriptableItem> tables = new List<IScriptableItem>();
                            DimAdConXController.Container = (Container)obj;
                            tables = DimAdConXController.CreateAdConXTables();
                            tp.Add(tables);
                            return;
                        default:
                            Process((Container)obj, tp, language, context);
                            break;
                    }
                    break;
                case ObjectType.otField:
                    if (((Field)obj).HasCaseData == false)
                    {
                        return;
                    }

                    Process((Field)obj, tp, language, context, container);
                    break;
                default:
                    // do nothing
                    break;
            }
        }

        static public void Process(Field field, DimTabPlan tp, string language, string context, Container container)
        {
            if (field.IsSystem == false)
            {
                switch (field.DataType)
                {

                    case DataType.dtBoolean:
                    case DataType.dtCategorical:
                        Process((BoundField)field, tp, language, context, container);
                        break;
                    case DataType.dtDate:
                    case DataType.dtDouble:
                    case DataType.dtLong:
                        Process((UnboundField)field, tp, language, context, container);
                        break;
                    case DataType.dtNone:
                    case DataType.dtObject:
                        Process((UnboundField)field, tp, language, context, container);
                        break;
                    case DataType.dtText:
                        if(SpecialFields.Contains(field.Name))
                        { Process((UnboundField)field, tp, language, context, container); }
                        break; // do nothing with Text fields until further notice
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        static public void Process(BoundField field, DimTabPlan tp, string language, string context, Container container)
        {
            IScriptableItem table = null;
            List<IScriptableItem> tables = new List<IScriptableItem>();
            Category brand = null;
            GfK.Automata.Pattern.Model.Pattern pattern = PatternFactory.Find(field, language, context);
            if (pattern is null)
            {
                if (_processField(field, container) == true)
                {
                    
                    string propVal = DimTabPlan.GetQType(field, container);
                    switch (propVal)
                    {
                        case "AE1":
                        case "AE2":
                        case "AE5_M":
                        case "AE6_S_M":
                        case "AE7_S_M":
                        //case "ADCONX3_M":
                            if (DimAE_LoopController.CreatedAELoopTables) { return; }
                            tables = DimAE_LoopController.CreateAE_LoopTables(container);
                            tp.Add(tables);
                            break;
                        case "RBX1AA_M":
                            //
                            table = new DimRBX1AA_MDetailTable("Incidence of Recent Experiences", field, container, null);
                            tp.Add(table);
                            table = null;
                            break;
                        case "RBX2":

                            break;
                        case "DCBR2_M":
                            /*One table per brand. 
                            Consumer Nets: Weak(1 - 3); At Risk(7 - 9); Strong(4 - 6).
                            B2B Nets: Weak(7); Transactional(8, 9); At Risk(1,2,4); Strong(3, 5, 6).
                            Healthcare Nets: Weak(7, 8); At Risk(1,2,4); Strong(3, 5, 6, 9)."
                            */
                            if (!container.Items.Last().Equals(field)) { break; }
                            
                            foreach (Category b in ((BoundField)field).Categories())
                            {
                                if (InvalidBrandNames.Contains(b)) { continue;  }
                                table = new DimDCBR2_M_RDetailTable("Consumer Brand Relationships - [BRAND]", b, field, container, null);
                                tp.Add(table);
                                table = null;
                            }

                            // benchmark table
                            table = new DimDCBR2_M_RBenchmarkTable("CBR - Relationship Dimensions", field, container, null);
                            tp.Add(table);
                            table = null;
                            break;
                        case "DCBRRELATIONSHIPCLASSGRID":
                            // one table per brand. brands in CategoryMap will be as fields in this grid.
                            brand = container.CategoryMap[container.IndexOf(field)];
                            if (InvalidBrandNames.Contains(brand)) { break; }
                            table = new DimDCBR_RC_GRIDDetailTable("Active and Latent Brand Equity (Based On Prefer #1 or #2) - [BRAND]", field, container, null);
                            tp.Add(table);
                            table = null;
                            break;
                        case "DCBREQUITYGRID":
                            // one table per brand. brands in CategoryMap will be as fields in this grid.
                            brand = container.CategoryMap[container.IndexOf(field)];
                            if (InvalidBrandNames.Contains(brand)) { break; }
                            table = new DimDCBR_EquityDetailTable("Prefer/Not prefer Based on Prefer #1 or #2 - [BRAND]", field, container, null);
                            tp.Add(table);
                            table = null;

                            if (container.Items.Last().Equals(field))
                            {
                                table = new DimDCBR_EquityBenchmarkTable("Active and Latent Brand Equity (Based on Prefer #1 or #2) - [BRAND]", field, container, null);
                                tp.Add(table);
                                table = null;
                            }

                            break;
                        case "SBAW1_FIRSTMENTION":
                        case "SBAW1_ALLOTHERMENTIONS":
                            DimSBAWController.CreateSBAW_Tables((Field)field, Filters);
                            tables = DimSBAWController.SBAWOrderTables();
                            tp.Add(tables);
                            tables = null;
                            break;
                        case "ABAW":
                            if (container.Items.Last().Equals(field))
                            {
                                foreach(Category c in container.CategoryMap)
                                {
                                    if (InvalidBrandNames.Contains(c)) { continue; }
                                    brand = c;
                                    BoundField brandField = ((BoundField)container.Items[container.CategoryMap.IndexOf(c)]);
                                    table = new DimABAWDetailTable("Aided Brand Awareness - ", brand, brandField, container, null);
                                    tp.Add(table);
                                    table = null;
                                }
                                table = new DimABAWSummaryTable(language, context, "Aided Brand Awareness", field, container, filter: null);
                                tp.Add(table);
                                table = null;

                                table = new DimBenchmarkTable(language, context, "Aided Brand Awareness", field, container, topCountOverride: 1, filter: null);
                                tp.Add(table);
                                table = null;
                            }

                            break;
                        case "CL_BE1":

                            if (container != null && (((BoundField)container.Items.Last()).Equals(field)))
                            {


                                // CategoryMap contains the BRANDS e.g., 10 brands x 4 attributes = 40 tables

                                // DETAIL client brand equity measure.
                                // requires a table with two nets

                                foreach (BoundField itm in container.Items)
                                {

                                    // we don't process some "dummy" brand names, e.g. Brand1, Brand2, Brand1.png, etc.
                                    string levelVar = DimAxisHelper.GetVarParts(itm)[1];
                                    Regex re = new Regex("{.*?}");
                                    string brandName = re.Matches(levelVar)[0].ToString().Replace("{","").Replace("}","");
                                    if (InvalidBrandNames.Contains(brandName)) { continue; };

                                    // Client's Brand Equity Measure - [BRAND] - [ITEM] (Aware of brand)
                                    // CL_BE1_1
                                    table = new DimCL_BE1DetailTable(language, context, "Client's Brand Equity Measure - [BRAND] - [ITEM] (Aware of brand)", itm, container);
                                    tp.Add(table);
                                    table = null;

                                }

                                for (Int32 i = 0; i < container.CategoryMap.Count; i++)
                                {
                                    brand = container.CategoryMap[i];
                                    if (InvalidBrandNames.Contains(brand)) { continue; }
                                    // Sumary table: Client's Brand Equity items -  [BRAND]  (Aware of brand)
                                    // CL_BE1_2
                                    table = new DimCL_BE1_2SummaryTable(language, context, "Client's Brand Equity items - [BRAND] (Aware of brand)", (BoundField)container.Items[i], container);
                                    tp.Add(table);
                                    table = null;
                                }


                                // Summary table: Client's Brand Equity items -  [BRAND]  (Aware of brand)
                                // Single table showing one Benchmark score for each of 4 attributes. Benchmark calc'd as average across all brands. 
                                // CL_BE1_3

                                table = new DimCL_BE1_3BenchmarkTable(language, context, "Summary table: Client's Brand Equity items -  [BRAND]  (Aware of brand)", container, null);
                                tp.Add(table);
                                table = null;

                                // Summary table: Client's Brand Equity Measure - Overall Brand Equity  (Aware of brand)
                                // CL_BE1_4
                                table = new DimCL_BE1_4SummaryTable(language, context, "Client's Brand Equity Measure - Overall Brand Equity  (Aware of brand)", container, filter: null);
                                tp.Add(table);
                                table = null;

                                // CL_BE_1_5
                                table = new DimCL_BE1_5BenchmarkTable(language, context, "Client's Brand Equity items - [BRAND] (Aware of brand)", container, filter: null);
                                tp.Add(table);
                                table = null;
                            }


                            break;

                        case "PI":

                            // similar to FAM, there will be detail, summary, benchmark.
                            brand = container.CategoryMap[container.Items.IndexOf(field)];
                            if (!(InvalidBrandNames.Contains(brand.Name)))
                            {
                                table = new DimPIDetailTable(language, context, "Purchase Intention - [BRAND]", field, container);
                                //((DimPIDetailTable)table).Side = ((DimDetailTable)table).CategorySideAxis(field, topCount: 2, sortDescending: true);
                                tp.Add(table);
                                table = null;
                            }
                            // Summary table of all brands in PI Grid
                            if (container != null && (((BoundField)container.Items.Last()).Equals(field)))
                            {
                                table = new DimPISummaryTable(language, context, "Purchase Intention", field, container);
                                tp.Add(table);

                                table = new DimBenchmarkTable(((DimAggregatedTable)table).Elements, language, context, "Purchase Intention", field, container, false, filter: null);
                                ((DimBenchmarkTable)table).Base = "Aware of brand";
                                tp.Add(table);
                                table = null;
                            }

                            break;
                        case "PURMOST":
                        case "PURMOST_T":
                            table = new DimPURMOSTDetailTable(language, context, "Brand purchased most", field, null, null);
                            ((DimDetailTable)table).AppendToFunnel(((DimDetailTable)table).FunnelRows, "FMCG: Purchased most often");
                            tp.Add(table);
                            table = null;
                            break;

                        case "FAM":
                            // Ultimately need to create TWO sets of tables, [ALL RESPONDENTS, AWARE OF BRAND]
                            // Detail Tables (FAM is grid/loop, so each Field is a slice e.g. FAM_GRID[{_1}]
                            brand = container.CategoryMap[container.Items.IndexOf(field)];
                            if (!(InvalidBrandNames.Contains(brand.Name)))
                            {
                                table = new DimFamDetailTable(language, context, "Familiarity - [BRAND] (All respondents)", field, container);
                                //((DimFamDetailTable)table).Apply(field, pattern, language, context);
                                tp.Add(table);
                                table = null;
                            }
                            
                            // Summary table of all brands in FAM Grid?
                            if (container != null && (((BoundField)container.Items.Last()).Equals(field)))
                            {
                                table = new DimFamSummaryTable(language, context, "Familiarity - Top 2 Box (All respondents)", field, container, filter: null);
                                //((DimFamSummaryTable)table).AppendToFunnel("Familiarity");
                                tp.Add(table);

                                table = new DimBenchmarkTable(((DimAggregatedTable)table).Elements, language, context, "Familiarity - Top 2 Box Benchmark (All respondents)", field, container, false, filter: null);
                                ((DimBenchmarkTable)table).Base = "Aware of brand";
                                //((DimBenchmarkTable)table).Apply(container,pattern,language,context);
                                tp.Add(table);
                                table = null;
                            }
                            break;
                        case "FAV":
                            // similar to FAM, there will be detail, summary, benchmark.
                            // FAV inherits from FAM
                            brand = container.CategoryMap[container.Items.IndexOf(field)];
                            if (!(InvalidBrandNames.Contains(brand.Name)))
                            {
                                table = new DimFavDetailTable(language, context, "Favorability - [BRAND]", field, container);
                                //((DimFavDetailTable)table).Side = ((DimDetailTable)table).CategorySideAxis(field, topCount: 2, sortDescending: true);
                                tp.Add(table);
                                table = null;
                            }
                            // Summary table of all brands in FAV Grid
                            if (container != null && (((BoundField)container.Items.Last()).Equals(field)))
                            {
                                table = new DimFavSummaryTable(language, context, "Favorability - Top 2 Box (All respondents)", field, container);
                                //((DimFamSummaryTable)table).Apply(container, pattern, language, context);
                                tp.Add(table);
                                //table = null;
                                
                                table = new DimBenchmarkTable(((DimAggregatedTable)table).Elements, language, context, "Favorability - Top 2 Box Benchmark (All resondents)", field, container, false, filter: null);
                                //((DimBenchmarkTable)table).Apply(container,pattern,language,context);
                                tp.Add(table);
                                table = null;
                            }
                            break;

                        case "PI_OCC":
                            // detail for each occasion per brand
                            // iterating over filter(s) based on OCC1 & OCC2???

                            if (container != null && (((BoundField)container.Items.Last()).Equals(field)))
                            {
                                tables = DimPI_OCCController.CreatePI_OCCTables(language, context, container, null, Configuration["MAXDATE"]);
                                tp.Add(tables);
                            };

                            break;

                        case "BRIA_M":
                            const Int32 BRIA_M_LIMIT = 20;
                            if (container != null && (((BoundField)container.Items.Last()).Equals(field)))
                            {
                                // BRIA_M_GRID[{_1}].BRIA_M
                                foreach (BoundField itm in container.Items)
                                {
                                    if (container.IndexOf(itm) >= BRIA_M_LIMIT) { break; }
                                    table = new DimBRIA_MDetailTable(language, context, "Brand Imagery - [ATTRIBUTE]", itm, container);
                                    tp.Add(table);
                                    table = null;
                                }
                                foreach (BoundField itm in container.Items)
                                {
                                    if (container.IndexOf(itm) >= BRIA_M_LIMIT) { break; }
                                    // BRIA_M_GRID > BRIA_M_GRID[..].BRIA_M -- needs the average of brand % score.
                                    // Brand Imagery - Benchmarks
                                    table = new DimBRIA_MBenchmarkTable(language, context, "Brand Imagery - Benchmarks", itm, container, brandsInColumns: true, filter: null);
                                    tp.Add(table);
                                    table = null;
                                }
                                
                            }
                            break;
                        case "BRIE_M":
                            // this is also a Loop/Grid which we need to handle the 3 levels properly. See CL_BE1 for example.
                            IEnumerable<MetadataObject> boundItems = container.Items.Where(o => (o is BoundField));
                            if (container != null && boundItems.Last().Equals(field))
                            {
                                
                                foreach (BoundField itm in boundItems)
                                {
                                    string levelVar = DimAxisHelper.GetVarParts(itm)[1];
                                    Regex re = new Regex("{.*?}");
                                    string brandName = re.Matches(levelVar)[0].ToString().Replace("{", "").Replace("}", "");
                                    string attributeName = re.Matches(DimAxisHelper.GetVarParts(itm)[0])[0].ToString().Replace("{", "").Replace("}", "");
                                    if (InvalidBrandNames.Contains(brandName)) { continue; };
                                    // list full scale, net Top 2, net bottom 2
                                    // each Attribute by each brand, 
                                    //  e.g., Att1 X Brand 1, Att1 x Brand 2, Att1 x Brand 3, ...,  
                                    //      Att2 x Brand 1, Att2 x Brand 2, ..., AttN x Brand 10 
                                    table = new DimBRIE_MDetailTable(language, context, "Brand Imagery - [BRAND] - [ATTRIBUTE]", attributeName, itm, container);
                                    tp.Add(table);
                                    table = null;
                                }

                                // Summary tables: All brands, per attribute
                                //field = (BoundField)container.Items.First();

                                for (Int32 i = 0; i < container.CategoryMap.Count; i++)
                                {
                                    if (InvalidBrandNames.Contains(container.CategoryMap[i])) { continue; }
                                    table = new DimBRIE_MSummaryTable(language, context, "Brand Imagery - [BRAND] - Top 2 Box", (BoundField)container.Items[i], container);
                                    //tp.Add(table);
                                    // NOT IMPLEMENTED FOR MVP
                                    table = null;
                                }


                                // Summary tables: All attributes, per brand, top 2 box
                                string parentName = DimAxisHelper.GetVarParts(container,false).First();
                                Container parent = (Container)container.Document.Find(parentName);
                                for (Int32 i = 0; i < container.Items.Count; i += container.CategoryMap.Count)
                                {
                                    if(!(container.Items[i] is BoundField)) { continue; }
                                    table = new DimBRIE_MSummaryTable(language, context, "Brand Imagery - [ATTRIBUTE] - Top 2 Box", (BoundField)container.Items[i], container, parent);
                                    tp.Add(table);
                                    table = null;
                                }

                                // BRIE_M.BEN 
                                foreach(Category cat in parent.CategoryMap)
                                {
                                    // create benchmark for this category
                                    table = new DimBRIE_MBenchmarkTable("Brand Imagery - Benchmarks", cat, parent, container, null);
                                    tp.Add(table);
                                    table = null;
                                }


                            }

                            break;
                        case "OCC1_BR":
                            // detail table
                            if (container != null && (((BoundField)container.Items.Last()).Equals(field)))
                            {
                                

                                foreach (BoundField itm in container.Items)
                                {
                                    table = new DimOCC1_BRDetailTable(language, context, "Brands purchased for [INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC1] in the past [TIME PERIOD]", (BoundField)itm, container, null);
                                    tp.Add(table);
                                    table = null;
                                }

                                foreach (BoundField itm in container.Items)
                                {
                                    //Core: 71-72 // OCC1_BR.BEN
                                    string occasion = container.CategoryMap[container.Items.IndexOf(itm)].Label(context, language, container.Document);
                                    table = new DimBenchmarkTable(language, context, "Occasion based brand usage - " + occasion, itm, container, brandsInColumns: true, filter: null);
                                    tp.Add(table);
                                    table = null;

                                }

                                // Core 68, 69-70, 89
                                table = new DimOCC1_BRSummaryTable(language, context, "Average number of occasions per brand", field, container, null);
                                tp.Add(table);
                                

                                // Core: 69-70
                                table = new DimOCC1_BRBenchmarkTable(((DimAggregatedTable)table).Elements, language, context, "Occasions - Average number of occasions by brand", field, container, true, null);
                                tp.Add(table);
                                table = null;

                            }
                            break;
                        case "OCC2_BR":

                            // requires iteration over OCC1/LST_OCC
                            BoundField iterator = (BoundField)field.Document.Find("OCC1");

                            if (iterator != null)
                            {
                                foreach (Category cat in iterator.Categories())
                                {

                                    table = new DimOCC2_BRDetailTable(language, context, "Brands purchased for last occasion of [INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC2] ", field, cat, iterator, null);
                                    tp.Add(table);
                                    table = null;
                                }
                            }
                            break;
                        case "OCC1":

                            table = new DimOCCDetailTable(language, context, "Occasions", field, container, null, Configuration["MAXDATE"]);
                            tp.Add(table);

                            table = null;
                            break;

                        case "OCC2":

                            table = new DimOCCDetailTable(language, context, "Last Occasion", field, container, null, Configuration["MAXDATE"]);
                            tp.Add(table);
                            table = null;
                            break;
                        //case "CON_1_OCC":
                        //case "CON_2_OCC":
                        case "TOT_CON_OCC":
                            // Creating one detail table for TOT_CON_OCC for each of Top 5 responses to OCC2.
                            tables = DimTOT_CON_OCCDetailTable.CreateTOT_CON_OCCDetailTables(language, context, "Consideration/Preference –  [INSERT \"\"ING\"\" VERSION OF SITUATION/OCCASION SELECTED IN OCC2] - [BRAND] (All respondents)", field, container, null, Configuration["MAXDATE"]);
                            tp.Add(tables);
                            tables = null;

                            break;
                        case "TOT_CON":
                            brand = container.CategoryMap[container.Items.IndexOf(field)];
                            if (!(InvalidBrandNames.Contains(brand.Name)))
                            {
                                table = new DimTOT_CONDetailTable(language, context, "Consideration/Preference  - [BRAND] (All respondents)", field, container);
                                tp.Add(table);
                                table = null;
                            }
                            if (container != null && (((BoundField)container.Items.Last()).Equals(field)))
                            {

                                ////We need a unique question name for Preference, so using CON1.The data should match the data in the next table for TOT_CON.
                                //table = new DimTOT_CONSummaryTable(language, context, "Consideration/Preference – First or Second Choice (All respondents)", field, container, topCats: 2);
                                //((DimTOT_CONSummaryTable)table).AddQTypeMod("CON1");
                                //tp.Add(table);
                                //((DimTOT_CONSummaryTable)table).AppendToFunnel(((DimTOT_CONSummaryTable)table).FunnelRows, "Prefer 1/2");
                                //table = null;

                                table = new DimTOT_CONSummaryTable(language, context, "Consideration/Preference – First or Second Choice (All respondents)", field, container, topCats: 2);
                                tp.Add(table);

                                table = new DimTOT_CONBenchmarkTable(((DimAggregatedTable)table).Elements, language, context, "Consideration/Preference – First or Second Choice - Benchmark (All respondents)", field, container);
                                tp.Add(table);
                                table = null;

                                table = new DimTOT_CONSummaryTable(language, context, "Consideration/Preference – Total Consideration (All respondents)", field, container, topCats: 3);
                                tp.Add(table);

                                ((DimTOT_CONSummaryTable)table).AppendToFunnel(((DimTOT_CONSummaryTable)table).FunnelRows, "Consideration");
                                table = null;

                                Container CON_1 = (Container)container.Document.Find("CON_1_GRID");
                                BoundField f = (BoundField)CON_1.Items.Last();
                                table = new DimCON_1SummaryTable(language, context, "Consideration/Preference – First or Second Choice (All respondents)", f, CON_1, topCats: 2);
                                tp.Add(table);
                                ((DimCON_1SummaryTable)table).AppendToFunnel(((DimCON_1SummaryTable)table).FunnelRows, "Prefer 1/2");
                                table = null;
                            }

                            break;
                        case "CON_1":
                            /* "Used to populate and check Purchase Funnel.  
                             * We need a unique question name for Preference, so using CON1.  
                             * This will match TOT_CONSummary table of all brands ""First or second choice""(1, 2)."
                            */
                            // This is actually the last table produced in the TOT_CON section, above.

                            break;
                        case "USEMOST":
                        case "USEMOST_T":
                            // this is a grid but requires only a "standard table" 
                            foreach (BoundField itm in container.Items)
                            {
                                table = new DimUSEMOST_TSummaryTable(language, context, "[SINGLE] used most often", itm, container, null);
                                //((DimUSEMOST_TSummaryTable)table).AppendToFunnel(((DimUSEMOST_TSummaryTable)table).FunnelRows, "Use most");
                                tp.Add(table);
                                table = null;

                            }

                            break;
                        case "HIDDEN_VERSION":
                            table = new DimDetailTable(language, context, "Version", field, container, filter: null);
                            tp.Add(table);
                            table = null;
                            break;
                        case "BRPEN":
                            table = new DimBRPENDetailTable(language, context, "", field, null, null);
                            ((DimDetailTable)table).AppendToFunnel(((DimBRPENDetailTable)table).FunnelRows, "FMCG: Brand Penetration");
                            tp.Add(table);
                            table = null;
                            break;
                        case "PURLAST_T":
                        case "PURLAST":
                            // creates a simple table using side axis from the field only
                            // description is empty, we'll try to figure that out later in the base class.
                            table = new DimDetailTable(language, context, "", field, null, null);
                            //((DimDetailTable)table).Apply(field, pattern, language, context);
                            tp.Add(table);
                            table = null;
                            break;
                        case "SW1_T":

                            tables = DimCustomerHarmonicsController.SW1_TTables(field);
                            tp.Add(tables);
                            tables = null;
                            break;
                        case "SW2_T":

                            tables = DimCustomerHarmonicsController.SW2_TTables(field);
                            tp.Add(tables);
                            tables = null;
                            break;
                        case "SW3_M":
                            if (container.Items.Last().Equals(field))
                            {
                                tables = DimCustomerHarmonicsController.SW3_MTables(container);
                                tp.Add(tables);
                                tables = null;
                            }
                            break;
                        case "SW4_P":
                        case "SW4":
                            tables = DimCustomerHarmonicsController.SW4_PTables(field);
                            tp.Add(tables);
                            tables = null;
                            break;
                        case "STPUL_T_M":
                            if (container.Items.Last().Equals(field))
                            {
                                tables = DimCustomerHarmonicsController.STPUL_T_MTables(container);
                                tp.Add(tables);
                                tables = null;
                            }
                            break;
                        case "HARMONICSDURABILITYCELL":
                            tables = DimCustomerHarmonicsController.STPULL_CELLTables(field);
                            tp.Add(tables);
                            tables = null;
                            break;
                        case "RELATIONSHIPDURABILITYSEGMENT":
                            // DURSEG variable
                            tables = DimCustomerHarmonicsController.DURSEG_Tables(field);
                            tp.Add(tables);
                            tables = null;

                            break;
                        case "PURFREQ":
                        case "PURVOL":
                        case "RETPUR_1":
                        case "RETMO_2":
                        
                            // creates a simple table using side axis from the field only
                            table = new DimDetailTable(language, context, field.Name, field, null, null);
                            //((DimDetailTable)table).Apply(field, pattern, language, context);
                            tp.Add(table);
                            table = null;
                            break;
                        
                        case "":
                        default:
                            //// log a warning
                            //Console.WriteLine("\tNo GfKQType property exists for " + field.Name);
                            ////pattern = PatternFactory.BoundFieldDetailTablePattern(field, language, context);
                            if (!(DimQueryData.ColumnIsEmpty(field)))
                            {
                                try
                                {
                                    if (container is null)
                                    { table = new DimDefaultDetailTable(language, context, field.Name, field, filter: null); }
                                    else
                                    { table = new DimDefaultDetailTable(language, context, field.Name, field, container, filter: null); }
                                    //table = new DimDetailTable(language, context, field.Name, field, container, null);
                                    tp.Add(table);
                                    table = null;
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("!");
                                }
                                
                            }
                            
                            ////((DimDetailTable)table).Apply(field, pattern, language, context);
                            break;
                    }
                }
                else 
                {



                }
            }
            else
            {
                /*
                     * // not implementing tables for non-GfK Question types.
                     * 
                table = new DimDetailTable(language, context, field.Name, field, container);
                ((DimDetailTable)table).Apply(field, pattern, language, context);
                */
            }
            if (table != null)
            {
                tp.Add(table);
            }
        }

        static public void Process(UnboundField field, DimTabPlan tp, string language, string context, Container container)
        {
            if(field.Name.Contains("ConX_Loop"))
            {
                Console.WriteLine(field.Name);
            }
            IScriptableItem table = null;
            GfK.Automata.Pattern.Model.Pattern pattern = PatternFactory.Find(field, language, context);
            if (pattern is null)
            {
                if (_processField(field, container) == true)
                {
                    string propVal = DimTabPlan.GetQType(field, container);
                    List<IScriptableItem> tables = null;
                    switch (propVal)
                    {
                        case "ADCONX3_M":
                            if (DimAE_LoopController.CreatedAELoopTables) { return; }
                            tables = DimAE_LoopController.CreateAE_LoopTables(container);
                            tp.Add(tables);
                            break;
                        case "SBAW1":
                            DimSBAWController.CreateSBAW_Tables((Field)field, Filters);
                            tables = DimSBAWController.SBAW1DetailTables();
                            tp.Add(tables);
                            tables = DimSBAWController.SBAWTOTTable();
                            tp.Add(tables);
                            tables = null;
                            break;
                        case "SPEND":
                            tables = DimSpendTables.CreateSpendTables(container, language, context, Filters);
                            tp.Add(tables);
                            tables = null;
                            break;
                        case "RBX2":
                            if (DimRBXController.CreatedRBXTables) { break; }
                            tables = DimRBXController.CreateRBX_Tables(container);
                            tp.Add(tables);
                            tables = null;
                            break;
                        case "AGE":

                            table = new DimDetailTable(language, context, field.Label(context, language, field.Document), field, container, null);
                            ((DimDetailTable)table).Apply(field, pattern, language, context);
                            tp.Add(table);
                            table = null;
                            break;
                        case "NUMOWN_T":
                            // grid
                            if (container != null && (((UnboundField)container.Items.Last()).Equals(field)))
                            {
                                table = new DimDetailTable(language, context, "Number of [PROD_M] owned</br>", field, container, null);
                                tp.Add(table);
                                table = null;
                                //((DimDetailTable)table).Apply(field, pattern, language, context);

                                // NUMOWN_T.NUM
                                table = new DimNumOwn_TDetailTable(language, context, "Brands owned", field,container, null);
                                ((DimDetailTable)table).AppendToFunnel(((DimDetailTable)table).FunnelRows, "TECH: Own brand");
                                tp.Add(table);
                                table = null;

                            }
                            break;
                        case "USEMOST_T":
                        case "USEMOST":
                            Console.WriteLine(field.Name);
                            if (container != null && (((UnboundField)container.Items.Last()).Equals(field)))
                            {
                                table = new DimDetailTable(language, context, "Number of [PROD_M] owned</br>", field, container, null);
                                tp.Add(table);
                                table = null;
                                //((DimDetailTable)table).Apply(field, pattern, language, context);
                            }
                            break;
                        case "SOP":

                            if (container != null && (((UnboundField)container.Items.Last()).Equals(field)))
                            {

                                tables = DimSOPTables.CreateSOPTables(container, field, language, context, null);
                                tp.Add(tables);
                                tables = null;
                            }

                            break;
                        case "":
                        default:
                            if (!(DimQueryData.ColumnIsEmpty(field)))
                            {
                                ////pattern = PatternFactory.BoundFieldDetailTablePattern(field, language, context);
                                table = new DimDefaultDetailTable(language, context, field.Name, field, filter:null);
                                tp.Add(table);
                                ////((DimDetailTable)table).Apply(field, pattern, language, context);
                                table = null;
                            }
                            break;
                    }
                }
                else
                {

                }
            }
            else
            {
                /*
                 * // not implementing tables for non-GfK Question types.
                 * 
                table = new DimDetailTable(language, context, "", field, container);
                ((DimDetailTable)table).Apply(field, pattern, language, context);
                tp.Add(table);
                */
                table = null;
            }
            if (table != null)
            {
                tp.Add(table);
            }
        }

        private static bool _processField(UnboundField field, Container c)
        {
            if (c != null)
            {
                string[] varParts = c.Name.Replace("[..]", "").Split('.');
                foreach (string part in varParts)
                {
                    if (SpecialFields.Contains(part))
                    {
                        Console.WriteLine("IGNORING Special Field: " + c.Name + "\t" + field.Name);
                        return false;
                    }
                }
                if (SpecialFields.Contains(c.Name))
                {
                    // we don't have a way to deal with this yet
                    Console.WriteLine("IGNORING Special Field: " + c.Name + "\t" + field.Name);
                    return false;
                }
                Console.WriteLine("Processing: " + c.Name + "\t" + field.Name);
                return true;
            }
            else
            {
                // c is not passed, so use field only
                Console.WriteLine("Processing: " + field.Name);
                return field.Properties.ContainsKey(GfKProperties.GfKQType);
            }
        }
        private static bool _processContainer(Container container)
        {
            if ((container != null) && container.Properties.ContainsKey("TemplateItem"))
            {
                if (container.Properties["TemplateItem"] ==  true)
                { return false; }
            }
            return true;
        }
        private static bool _processField(BoundField field, Container container)
        {
            //field.Properties.ContainsKey(GfKQType) || ((container != null) && container.Name.ToUpper() == "AE_LOOP")
            //
            //  Determines whether to process an individual BoundField object
            //  AE_Loop is special case as a Container<Container> which must be traversed differently
            // 
            if ((container != null) && (SpecialFields.Contains(container.Name)))
            {
                // we have exceptional class like DimAE_LoopController to handle these.
                Console.WriteLine("Processing Special Field: " + container.Name + "\t" + field.Name);
                return true;
            }
            else if ( container != null )
            {
                Console.WriteLine("Processing: " + container.Name + "\t" + field.Name);
                
                return true;
            }

            Console.WriteLine("Processing:" + field.Name);
            return true;

            //throw (new NotImplementedException());
            
        }

    }
}