﻿using System;
using System.Collections.Generic;
using GfK.Automata.Model;
using MDMLib;

namespace GfK.Automata.Tabulation.Service
{
    public class MDMReader
    {
        static MetadataDocument _document;
        static MetadataSourceType _type;

        static MDMReader()
        {
        }

        public static MetadataDocument Read(string path, string version, MetadataSourceType type)
        {
            _type = type;
            _document = new MetadataDocument(_type);
            _load(path, version);
            return _document;
        }

        static BoundField _getBoundField(VariableInstance variable, DataType type)
        {
            BoundField fld = new BoundField(variable.FullName, type, variable.EffectiveMinValue, variable.EffectiveMaxValue, null, variable.HasCaseData, false, variable.IsReference, variable.IsSystem, variable.MinValue, variable.MaxValue);
            return fld;
        }

        static Category _getCategory(IElement element, MetadataDocument document)
        {
            Category category = new Category(element.Name);
            _loadLabels((MDMLabeledObject)element, category, document);
            _loadProperties(element.Properties, category);
            return category;
        }

        static DataType _getDataType(VariableInstance variable)
        {
            switch (variable.DataType)
            {
                case DataTypeConstants.mtBoolean:
                    return DataType.dtBoolean;
                case DataTypeConstants.mtCategorical:
                    return DataType.dtCategorical;
                case DataTypeConstants.mtDate:
                    return DataType.dtDate;
                case DataTypeConstants.mtDouble:
                    return DataType.dtDouble;
                case DataTypeConstants.mtLong:
                    return DataType.dtLong;
                case DataTypeConstants.mtNone:
                    return DataType.dtNone;
                case DataTypeConstants.mtObject:
                    return DataType.dtObject;
                case DataTypeConstants.mtText:
                    return DataType.dtText;
                default:
                    throw new NotImplementedException();
            }
        }

        static List<Category> _getFieldCategoryMap(IDocument d, Categories categories, MetadataDocument document)
        {
            List<Category> list = new List<Category>();
            int count = categories.Count;
            Contexts c = d.Contexts;
            Languages l = d.Languages;
            for (int i = 0; i < categories.Count; i++)
            {
                Category category = new Category(categories[i].Name);
                category.IsReference = categories[i].IsReference;
                for (int j = 0; j < c.Count; j++)
                {
                    Context context = c[j];
                    d.Contexts.Current = context.Name;
                    for (int k = 0; k < l.Count; k++)
                    {
                        Language language = l[k];
                        d.Languages.Current = language.Name;
                        string text = categories[i].Label;
                        if (text.Trim().Length > 0)
                            category.AddLabel(context.Name, language.Name, text, document);
                    }
                }
                d.Contexts.Current = "Question";
                IProperties p = categories[i].Properties;
                int count2 = p.Count;
                for (int j = 0; j < count2; j++)
                {
                    category.Properties.Add(p.Name[j], p[j]);
                }
                list.Add(category);
            }
            return list;
        }

        static UnboundField _getUnboundField(VariableInstance variable, DataType type)
        {
            UnboundField fld = new UnboundField(variable.FullName, type, variable.EffectiveMinValue, variable.EffectiveMaxValue, null, variable.HasCaseData, false, variable.IsReference, variable.IsSystem, variable.MinValue, variable.MaxValue);
            return fld;
        }

        static void _load(string path, string version)
        {
            switch (_type)
            {
                case MetadataSourceType.stDimensionsADO:
                case MetadataSourceType.stDimensionsCSV:
                case MetadataSourceType.stDimensionsLog:
                case MetadataSourceType.stDimensionsQC:
                case MetadataSourceType.stDimensionsQV:
                case MetadataSourceType.stDimensionsSample:
                case MetadataSourceType.stDimensionsSAV:
                case MetadataSourceType.stDimensionsSC:
                case MetadataSourceType.stDimensionsSSS:
                case MetadataSourceType.stCSV:
                case MetadataSourceType.stExcel:
                case MetadataSourceType.stGAMApplication:
                case MetadataSourceType.stSAV:
                    throw new NotImplementedException();
                case MetadataSourceType.stDimensionsMDD:
                    _loadDimensionsMDD(path, version);
                    break;
            }
        }

        static void _loadContexts(Document MDM)
        {
            Contexts contexts = MDM.Contexts;
            for (int i = 0; i < contexts.Count; i++)
            {
                Context context = contexts[i];
                _document.Contexts.Add(context.Name);
            }
            _document.SetContext(MDM.Contexts.Base);
        }

        static void _loadDimensionsMDD(string path, string version = "{..}")
        {
            Document MDM = new Document();
            MDM.Open(path, version, openConstants.oREAD);
            _document.Path = path;
            _loadContexts(MDM);
            _loadLanguages(MDM);
            _loadTypes(MDM.Types, _document);
            _loadFields(MDM.Fields, _document);
            MDM.Close();
        }

        static void _loadField(VariableInstance variable, MetadataObject parent)
        {
            DataType dt = _getDataType(variable);
            switch (variable.DataType)
            {
                case DataTypeConstants.mtNone:
                case DataTypeConstants.mtObject:
                    // Do not add to metadata
                    break;
                case DataTypeConstants.mtBoolean:
                    BoundField boolf = _getBoundField(variable, dt);
                    parent.Add(boolf);
                    _loadLabels((MDMLabeledObject)variable, boolf, parent.Document);
                    boolf.Add(new Category("True"));
                    boolf.Add(new Category("False"));
                    _loadProperties(variable.Document.Fields[variable.FullName].Properties, boolf);
                    _loadParentProperties(parent.Properties, boolf);
                    break;
                case DataTypeConstants.mtCategorical:
                    BoundField bf = _getBoundField(variable, dt);
                    parent.Add(bf);
                    _loadLabels((MDMLabeledObject)variable, bf, parent.Document);
                    _loadElements(variable.Elements, bf);
                    _loadProperties(variable.Document.Fields[variable.FullName].Properties, bf);
                    _loadParentProperties(parent.Properties, bf);
                    break;
                case DataTypeConstants.mtDate:
                case DataTypeConstants.mtDouble:
                case DataTypeConstants.mtLong:
                case DataTypeConstants.mtText:
                    UnboundField uf = _getUnboundField(variable, dt);
                    parent.Add(uf);
                    _loadLabels((MDMLabeledObject)variable, uf, parent.Document);
                    _loadProperties(variable.Document.Fields[variable.FullName].Properties, uf);
                    _loadParentProperties(parent.Properties, uf);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        static void _loadFields(ITypes fields, MetadataObject parent)
        {
            int count = fields.Count;
            IMDMField field;
            for (int i = 0; i < count; i++)
            {
                field = fields[i];
                switch (field.ObjectTypeValue)
                {
                    case ObjectTypesConstants.mtVariable:
                    case ObjectTypesConstants.mtVariableInstance:
                        Variables v = field.Variables;
                        int varcount = v.Count;
                        string lastname = field.FullName;
                        for (int j = 0; j < varcount; j++)
                        {
                            VariableInstance vi = v[j];
                            if (!vi.FullName.Contains(@".."))
                            {
                                _loadField(vi, parent);
                            }
                        }
                        break;
                        case ObjectTypesConstants.mtArray:
                        case ObjectTypesConstants.mtGrid:
                            Container c = new Container(field.FullName);
                            Console.WriteLine(field.FullName);
                            _loadProperties(field.Properties, c);
                            _loadParentProperties(parent.Properties, c);
                            parent.Add(c);
                            c.SetCategoryMap(_getFieldCategoryMap(field.Document, field.Categories, parent.Document));
                            _loadFields(field.Fields, c);
                            break;
                        case ObjectTypesConstants.mtClass:
                        case ObjectTypesConstants.mtFields:
                            Container c2 = new Container(field.FullName);
                            Console.WriteLine(field.FullName);
                            _loadProperties(field.Properties, c2);
                            _loadParentProperties(parent.Properties, c2);
                            parent.Add(c2);
                            _loadFields(field.Fields, c2);
                        break;
                    default:
                        break;
                }
            }
        }

        static void _loadLabels(MDMLabeledObject mdmObject, ILabeledMetadataObject metadataObject, MetadataDocument document)
        {
            Document d = (Document)mdmObject.Document;
            Contexts c = d.Contexts;
            Languages l = d.Languages;
            for (int i = 0; i < c.Count; i++)
            {
                Context context = c[i];
                d.Contexts.Current = context.Name;
                for (int j = 0; j < l.Count; j++)
                {
                    Language language = l[j];
                    d.Languages.Current = language.Name;
                    string text = mdmObject.Label;
                    if (text.Trim().Length == 0)
                        text = string.Empty;
                    metadataObject.AddLabel(context.Name, language.Name, text, document);
                }
            }
        }

        static void _loadLanguages(Document MDM)
        {
            Languages languages = MDM.Languages;
            for (int i = 0; i < languages.Count; i++)
            {
                Language language = languages[i];
                _document.Languages.Add(language.Name);
            }
            _document.SetLanguage(MDM.Languages.Base);
        }

        static void _loadTypes(ITypes Types, MetadataDocument document)
        {
            Document d = (Document)Types.Parent;
            Contexts c = d.Contexts;
            Languages l = d.Languages;
            int count = Types.Count;
            for (int i = 0; i < count; i++)
            {
                DefinedList dl = new DefinedList(Types[i].Name);
                document.Add(dl);
                IMDMObject obj = (IMDMObject)Types[i];
                IElements elements = (IElements)obj;
                _loadElements(elements, dl);
                _loadProperties(Types[i].Properties, dl);
            }
        }

        static void _loadElements(IElements elements, MetadataObject parent)
        {
            int count = elements.Elements.Count;
            IElement e;
            for (int i = 0; i < count; i++)
            {
                e = elements.Elements[i];
                switch (e.ObjectTypeValue)
                {
                    case ObjectTypesConstants.mtElement:
                        Category category = _getCategory(e, parent.Document);
                        parent.Add(category);
                        break;
                    case ObjectTypesConstants.mtElements:
                        Elements thisElements = (Elements)e;
                        if (thisElements.IsReference)
                        {
                            parent.Add(new Reference(thisElements.Name, parent.Document));
                        }
                        else
                        {
                            _loadElements(thisElements, parent);
                        }
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        static void _loadParentProperties(Dictionary<string, dynamic> parentProperties, MetadataObject metadataObject)
        {
            foreach (string key in parentProperties.Keys)
            {
                if (!metadataObject.Properties.ContainsKey(key))
                    metadataObject.Properties.Add(key, parentProperties[key]);
            }
        }

        static void _loadProperties(IProperties properties, MetadataObject metadataObject)
        {
            foreach (Context context in properties.Document.Contexts)
            {
                properties.Document.Contexts.Current = context.Name;
                int count = properties.Count;
                for (int i = 0; i < count; i++)
                {
                    if (metadataObject.Properties.ContainsKey(properties.Name[i]))
                    {
                        if (properties[i] != metadataObject.Properties[properties.Name[i]])
                        {
                            Console.WriteLine("WARNING:  Duplicate property detected - skipping");
                            Console.WriteLine("                    PropertyName: " + properties.Name[i]);
                            Console.WriteLine("               New PropertyValue: " + properties[i]);
                            Console.WriteLine("          Existing PropertyValue: " + metadataObject.Properties[properties.Name[i]]);
                        }
                    }
                    else
                    {
                        metadataObject.Properties.Add(properties.Name[i], properties[i]);
                    }
                }
            }
        }
    }
}
