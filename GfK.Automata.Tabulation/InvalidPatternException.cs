﻿using System;

namespace GfK.Automata.Tabulation
{
    public class InvalidPatternException : Exception
    {
        public InvalidPatternException()
        {
        }

        public InvalidPatternException(string message): base(message)
        {
        }
    }
}
