﻿namespace GfK.Automata.Tabulation
{
    public interface IPersistablePattern
    {
        void DecrementStatistics();
        bool Exists();
        int GetTableCount();
        void IncrementStatistics();
        bool Save();
        //bool Save(Table table);
        //bool Validate(Table table);

    }
}
