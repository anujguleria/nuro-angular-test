﻿namespace GfK.Automata.Tabulation
{
    public enum CellItemType
    {
        itCount,
        itColPercent,
        itRowPercent,
        itTotalPercent,
        itMean,
        itSum,
        itMinimum,
        itMaximum,
        itUnweightedCount,
        itCumColPercent,
        itCumRowPercent,
        itRange,
        itMode,
        itMedian,
        itPercentile,
        itStdDev,
        itStdErr,
        itVariance,
        itResiduals,
        itAdjustedResiduals,
        itExpectedValues,
        itValidN,
        itIndices,
        itColPropResults,
        itColBase,
        itRowBase,
        itUnweightedColBase,
        itUnweightedRowBase,
        itProfileResult,
        itCellChiSquare,
        itColRanks,
        itRowRanks
    }
    public class CellItem
    {
        public double CutOffValue { get; set; }
        public int Decimals { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public CellItemType Type { get; set; }
        public GfK.Automata.Model.IMetadataObject Variable { get; set; }
    }
}
