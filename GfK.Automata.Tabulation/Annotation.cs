﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GfK.Automata.Tabulation
{
    public enum AnnotationPosition
    {
        apTitleHeader,
        apLeftHeader,
        apCenterHeader,
        apRightHeader,
        apTitleFooter,
        apLeftFooter,
        apCenterFooter,
        apRightFooter
    }

    public class Annotation
    {
        public string Text { get; set; }
        public AnnotationPosition Position { get; set; }
        public Annotation( string text, AnnotationPosition position )
        {
            Text = text;
            Position = position;
        }
    }
}
